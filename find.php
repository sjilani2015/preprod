<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

$monUrl = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
if (isset($_SESSION['data_login'])) {
    $users = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));
    $ma_liste_colonne = explode(",", $users['colonne']);
} else {
    $ma_liste_colonne = array("6,7,8,9,10,11,12,13,14,15,16");
}
 
function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}

$users = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));
$ma_liste_colonne = explode(";", $users['colonne']);



$keyword_searchsql = addslashes($_POST['keyword_search1']);

$regionsql = $_POST['region'];
$villesql = $_POST['ville'];
$cpsql = $_POST['cp'];


$min_anneesql = addslashes($_POST['creation_deb']);
$max_anneesql = addslashes($_POST['creation_fin']);
$sirensql = addslashes($_POST['siret']);
$cilble_B2B = addslashes($_POST['cilble_B2B']);
$cilble_B2C = addslashes($_POST['cilble_B2C']);
$cilble_C2C = addslashes($_POST['cilble_C2C']);

$forme_EURL = addslashes($_POST['forme_EURL']);
$forme_SAS = addslashes($_POST['forme_SAS']);
$forme_SA = addslashes($_POST['forme_SA']);
$forme_SARL = addslashes($_POST['forme_SARL']);

$rang1 = addslashes($_POST['rang1']);
$rang2 = addslashes($_POST['rang2']);
$rang3 = addslashes($_POST['rang3']);
$rang4 = addslashes($_POST['rang4']);
$rang5 = addslashes($_POST['rang5']);
$nafsql = addslashes($_POST['naf']);
$sous_nafsql = addslashes($_POST['sous_naf']);
$prenomsql = addslashes($_POST['prenom']);
$nomsql = addslashes($_POST['nom']);
$formations = addslashes((str_replace(array("é", "è", "ê", "ç", "î", "ï"), array("e", "e", "e", "c", "i", "i"), $_POST['formations'])));
$doc_legal = addslashes($_POST['doc_legal']);
$parution = addslashes($_POST['parution']);
$min_effectifsql = addslashes($_POST['effectif_min']);
$max_effectifsql = addslashes($_POST['effectif_max']);



$debut_creation = addslashes($_POST['debut_creation']);
$fin_creation = addslashes($_POST['fin_creation']);
$date_l1 = addslashes($_POST['date_l1']);
$date_l2 = addslashes($_POST['date_l2']);


$nbr_lf1 = addslashes($_POST['nbr_lf1']);
$nbr_lf2 = addslashes($_POST['nbr_lf2']);
$nbr_lf3 = addslashes($_POST['nbr_lf3']);
$nbr_lf4 = addslashes($_POST['nbr_lf4']);
$nbr_lf5 = addslashes($_POST['nbr_lf5']);
$nbr_lf6 = addslashes($_POST['nbr_lf6']);

$montant_l1 = addslashes($_POST['montant_l1']);
$montant_l2 = addslashes($_POST['montant_l2']);
$montant_l3 = addslashes($_POST['montant_l3']);
$montant_l4 = addslashes($_POST['montant_l4']);

$chiff_a1 = addslashes($_POST['chiff_a1']);
$chiff_a2 = addslashes($_POST['chiff_a2']);
$chiff_a3 = addslashes($_POST['chiff_a3']);

$min_cs = addslashes($_POST['min_cs']);
$max_cs = addslashes($_POST['max_cs']);
$resultat_min = addslashes($_POST['resultat_min']);
$resultat_max = addslashes($_POST['resultat_max']);
$experience = addslashes((str_replace(array("é", "è", "ê", "ç", "î", "ï"), array("e", "e", "e", "c", "i", "i"), $_POST['experience'])));
$skills = addslashes((str_replace(array("é", "è", "ê", "ç", "î", "ï"), array("e", "e", "e", "c", "i", "i"), $_POST['skills'])));

$rachat = addslashes($_POST['rachat']);
$ipo = addslashes($_POST['ipo']);
$depot_bilan = addslashes($_POST['depot_bilan']);




$bool = 0;
$bool1 = 0;
$bool2 = 0;
$sql_sectors = "";
$tags_sector = "";
$list_secteurs1 = getListSecteurTotal();
if (!empty($list_secteurs1)) {
    foreach ($list_secteurs1 as $secteurses) {

        $val = "secteur_" . $secteurses['id'];
        if ($_POST[$val] != '') {
            $bool = 1;
            $sql_sectors .= " or activite.secteur =" . $secteurses['id'] . " ";
            $tags_sector .= ' <a href="' . URL . '/etude-secteur/' . generate_id($secteurses["id"]) . '/' . urlWriting(strtolower($secteurses["nom_secteur"])) . '" title="" class="tags__el tags__el--pinky"> <span class="ico-chart"></span>' . utf8_encode($secteurses["nom_secteur"]) . '</a>';
        }
    }
}
$sql_ssectors = "";
$list_ssecteurs1 = getListSousSecteurTotal();
if (!empty($list_ssecteurs1)) {
    foreach ($list_ssecteurs1 as $ssecteurses) {

        $val = "sous_secteur_" . $ssecteurses['id'];
        if ($_POST[$val] != '') {
            $bool2 = 1;
            $sql_ssectors .= " or activite.sous_secteur =" . $ssecteurses['id'] . " ";
        }
    }
}
$list_activite1 = getListActiviteTotal();
if (!empty($list_activite1)) {
    foreach ($list_activite1 as $activites) {

        $val = "activite_" . $activites['id'];
        if ($_POST[$val] != '') {
            $bool1 = 1;
            $sql_activite .= " or activite.activite =" . $activites['id'] . " ";
        }
    }
}



if ($bool == 1) {
    $chaine_market = " and ( activite.secteur =1000 " . $sql_sectors . " ) ";
} else {
    $chaine_market = " ";
}
if ($bool2 == 1) {
    $chaine_market3 = " and ( activite.sous_secteur =1000 " . $sql_ssectors . " ) ";
    $chaine_market = " ";
} else {
    $chaine_market3 = " ";
}
if ($bool1 == 1) {
    $chaine_market2 = "  and ( activite.activite=1000  " . $sql_activite . " )";
} else {
    $chaine_market2 = " ";
}








if ($date_l1 != "") {
    $sql_date_funds1 = " and year(lf.date_ajout) >= '" . $date_l1 . "' ";
}
if ($date_l2 != "") {
    $sql_date_funds2 = " and year(lf.date_ajout) <= '" . $date_l2 . "' ";
}



if ($debut_creation != "") {
    $sql_date_creation_deb = " and date(startup.date_complete) >= '" . $debut_creation . "' ";
}
if ($fin_creation != "") {
    $sql_date_creation_fin = " and date(startup.date_complete) <= '" . $fin_creation . "' ";
}


$roundlink = " ";
$var_rachat = " ";
if (($sql_montant_funds != '') || ($montant_l1 != '') || ($montant_l2 != '') || ($montant_l3 != '') || ($montant_l4 != '') || ($date_l1 != '') || ($date_l2 != '') || ($rachat != '') || ($ipo != '')) {
    $roundlink = " inner join lf on lf.id_startup=startup.id ";
    if (($rachat != "") || ($ipo != "")) {
        $var_rachat = "  ";
    } else {
        $var_rachat = " and lf.rachat=0 ";
    }
}

if ($cilble_B2B != '')
    $gg .= " or startup.cible like '%" . $cilble_B2B . "%' ";
if ($cilble_B2C != '')
    $gg .= " or startup.cible like '%" . $cilble_B2C . "%' ";
if ($cilble_C2C != '')
    $gg .= " or startup.cible like '%" . $cilble_C2C . "%' ";


if (($cilble_B2B != "") || ($cilble_B2C != "") || ($cilble_C2C != ""))
    $chaine_b = " and (startup.cible='BBB' $gg ) ";
else {
    $chaine_b = " ";
}



if ($nbr_lf1 != '')
    $sql_raised_funds_A = " or startup.tour_serie ='1' ";
if ($nbr_lf2 != '')
    $sql_raised_funds_B = " or startup.tour_serie ='2' ";
if ($nbr_lf3 != '')
    $sql_raised_funds_C = " or startup.tour_serie ='3' ";
if ($nbr_lf4 != '')
    $sql_raised_funds_D = " or startup.tour_serie ='4' ";
if ($nbr_lf5 != '')
    $sql_raised_funds_E = " or startup.tour_serie ='5' ";
if ($nbr_lf6 != '')
    $sql_raised_funds_F = "  or startup.tour_serie ='6' or startup.tour_serie='7' or startup.tour_serie='8' or startup.tour_serie='9' or startup.tour_serie='10' or startup.tour_serie='11'  or startup.tour_serie='12' ";

if (($nbr_lf1 != '') || ($nbr_lf2 != '') || ($nbr_lf3 != '') || ($nbr_lf4 != '') || ($nbr_lf5 != '') || ($nbr_lf6 != '')) {
    $chaine_raised_fund = " and ( startup.tour_serie ='100' " . $sql_raised_funds_A . " " . $sql_raised_funds_B . " " . $sql_raised_funds_C . " " . $sql_raised_funds_D . " " . $sql_raised_funds_E . " " . $sql_raised_funds_F . " )";
} else {
    $chaine_raised_fund = " ";
}

if (($cilble_B2B != "") || ($cilble_B2C != "") || ($cilble_C2C != ""))
    $chaine_b = " and (startup.cible='BBB' $gg ) ";
else {
    $chaine_b = " ";
}

/**
 * Forme juridique
 */
if ($forme_EURL != '')
    $ff .= " or startup.forme_jur like '" . $forme_EURL . "' ";
if ($forme_SAS != '')
    $ff .= " or startup.forme_jur like '" . $forme_SAS . "' ";
if ($forme_SARL != '')
    $ff .= " or startup.forme_jur like '" . $forme_SARL . "' ";
if ($forme_SA != '')
    $ff .= " or startup.forme_jur like '" . $forme_SA . "' ";


if (($forme_EURL != "") || ($forme_SAS != "") || ($forme_SARL != "") || ($forme_SA != ""))
    $chaine_ff = " and (startup.forme_jur='BBB' $ff ) ";
else {
    $chaine_ff = " ";
}


if ($rang1 != '')
    $cc .= " or startup.rang_effectif = 1 ";
if ($rang2 != '')
    $cc .= " or (startup.rang_effectif >= 2 and startup.rang_effectif<=10) ";
if ($rang3 != '')
    $cc .= " or (startup.rang_effectif >= 11 and startup.rang_effectif<=50) ";
if ($rang4 != '')
    $cc .= " or (startup.rang_effectif >= 51 and startup.rang_effectif<=100) ";
if ($rang5 != '')
    $cc .= " or (startup.rang_effectif >= 101) ";

if (($rang1 != "") || ($rang2 != "") || ($rang3 != "") || ($rang4 != "") || ($rang5 != ""))
    $chaine_cc = " and (startup.rang_effectif>100000 $cc ) ";
else {
    $chaine_cc = " ";
}


/**
 * NAF
 */
if ($nafsql != "") {
    $naflist = " and startup.label_naf like '" . $nafsql . "' ";
} else {
    $naflist = "";
}
/**
 * SOUS-NAF
 */
if ($sous_nafsql != "") {
    $sous_nafsqllist = " and startup.naf like '" . $sous_nafsql . "' ";
} else {
    $sous_nafsqllist = "";
}
/**
 * ROUND
 */
if ($roundsql != "") {
    $sql_raised_funds_A = " and startup.serie ='" . $roundsql . "' ";
} else {
    $sql_raised_funds_A = "";
}
/**
 * DATE DE
 */
if ($min_anneesql != "") {
    $cearition_deblist = " and startup.creation >= '" . $min_anneesql . "' ";
} else {
    $cearition_deblist = "";
}
/**
 * DATE A
 */
if ($max_anneesql != "") {
    $cearition_finlist = " and startup.creation <= '" . $max_anneesql . "' ";
} else {
    $cearition_finlist = "";
}
/**
 * EFFECTIF DE
 */
if ($min_effectifsql != "") {
    $effectiflist_min = " and startup.rang_effectif >= " . $min_effectifsql . " ";
} else {
    $effectiflist_min = "";
}
/**
 * EFFECTIF A
 */
if ($max_effectifsql != "") {
    $effectiflist_max = " and startup.rang_effectif <= " . $max_effectifsql . " ";
} else {
    $effectiflist_max = "";
}



if ($min_cs != "") {
    $capitalist_min = " and startup_capital.capital >= " . $min_cs . " ";
} else {
    $capitalist_min = "";
}
/**
 * EFFECTIF A
 */
if ($max_cs != "") {
    $capitallist_max = " and startup_capital.capital <= " . $max_cs . " ";
} else {
    $capitallist_max = "";
}

if (($max_cs != '') || ($min_cs != '')) {
    $doc_capitallink = " inner join startup_capital on startup_capital.id_startup=startup.id ";
} else {
    $doc_capitallink = " ";
}
/**
 * Benefice A
 */
if ($resultat_min != "") {
    $sql_resultat_min = " and startup_benefice.benefice >= " . $resultat_min . " ";
} else {
    $sql_resultat_min = "";
}
if ($resultat_max != "") {
    $sql_resultat_max = " and startup_benefice.benefice <= " . $resultat_max . " ";
} else {
    $sql_resultat_max = "";
}

if (($resultat_min != '') || ($resultat_max != '')) {
    $doc_beneficelink = " inner join startup_benefice on startup_benefice.id_startup=startup.id ";
} else {
    $doc_beneficelink = " ";
}


/**
 * Region
 */
if ($regionsql != "") {
    $ma_region = mysqli_fetch_array(mysqli_query($link, "select * from region_new where region_new='" . (addslashes($regionsql) . "'")));
    $regionlist = " and (startup.region_new like '%" . (addslashes($regionsql)) . "%' or startup.ville like '%" . (addslashes($regionsql)) . "%' or startup.cp like '%" . (addslashes($regionsql)) . "%' or startup.adresse like '%" . (addslashes($regionsql)) . "%' ) ";
    $tags_region = '  <a href="' . URL . '/region/' . generate_id($ma_region['id']) . '/' . urlWriting(strtolower($ma_region['region_new'])) . '" title="" class="tags__el tags__el--pinky"><span class="ico-chart"></span>' . utf8_encode(str_replace("__", " ", utf8_decode($regionsql))) . ' </a>';
} else {
    $regionlist = "";
}
/**
 * Ville
 */
$tag_ville = "";
if ($villesql != "") {
    $villelist = " and (startup.region_new like '%" . (addslashes($villesql)) . "%' or startup.ville like '%" . (addslashes($villesql)) . "%' or startup.cp like '%" . (addslashes($villesql)) . "%' or startup.adresse like '%" . (addslashes($villesql)) . "%' ) ";
    $tag_ville = '<a href="" title="" class="tags__el"><span class="ico-tag"></span>' . utf8_decode($villesql) . '</a>';
} else {
    $villelist = "";
}
/**
 * CP
 */
if ($cpsql != "") {
    $cplist = " and (startup.region_new like '%" . utf8_decode($cpsql) . "%' or startup.ville like '%" . utf8_decode($cpsql) . "%' or startup.cp like '%" . utf8_decode($cpsql) . "%' or startup.adresse like '%" . utf8_decode($cpsql) . "%' ) ";
} else {
    $cplist = "";
}

/**
 * SIRET
 */
if ($sirensql != "") {
    $sirenlist = " and startup.siret like '%" . utf8_decode($sirensql) . "%'";
} else {
    $sirenlist = "";
}
/**
 * Keyword
 */
if ($keyword_searchsql != "") {
    $keyword_searchlist = " and (startup.nom like '%" . $keyword_searchsql . "%' or startup.juridique like '%" . $keyword_searchsql . "%' or startup.short_fr like '%" . $keyword_searchsql . "%' or startup.long_fr like '%" . $keyword_searchsql . "%' or startup.concat_tags like '%" . $keyword_searchsql . "%') ";
} else {
    $keyword_searchlist = "";
}
/**
 * Prénom personnes
 */
if ($prenomsql != "") {
    $prenomlist = " and personnes.prenom like '%" . $prenomsql . "%'  ";
} else {
    $prenomlist = "";
}
if ($nomsql != "") {
    $nomlist = " and personnes.nom like '%" . $nomsql . "%'  ";
} else {
    $nomlist = "";
}

if (($prenomsql != '') || ($nomsql != '') || ($formations != '') || ($experience != '') || ($skills != '')) {
    $personneslink = " inner join personnes on personnes.id_startup=startup.id ";
} else {
    $personneslink = " ";
}

if ($formations != "") {
    $formationlist = " and biblio_ecole.sous_famille like '" . $formations . "' ";
    $formatiolink = " inner join personnes_ecole on personnes.id=personnes_ecole.id_personne inner join biblio_ecole on biblio_ecole.id=personnes_ecole.id_ecole ";
} else {
    $formationlist = " ";
    $formatiolink = " ";
}
if ($experience != "") {
    $experiencelist = " and biblio_experience.experience like '" . $experience . "' ";
    $explink = " inner join personnes_experience on personnes.id=personnes_experience.id_personne inner join biblio_experience on biblio_experience.id=personnes_experience.id_experience ";
} else {
    $experiencelist = " ";
    $explink = " ";
}
if ($skills != "") {
    $skillslist = " and biblio_competence.skills like '" . $skills . "' ";
    $skillslink = " inner join personnes_competence on personnes.id=personnes_competence.id_personne inner join biblio_competence on biblio_competence.id=personnes_competence.id_skills ";
} else {
    $skillslist = " ";
    $skillslink = " ";
}


if (($doc_legal != '') || ($parution != '')) {
    $doc_legallink = " inner join startup_depot on startup_depot.id_startup=startup.id ";
} else {
    $doc_legallink = " ";
}


if ($doc_legal != "") {
    $doc_legallist = " and startup_depot.depot like '%" . $doc_legal . "%' ";
} else {
    $doc_legallist = " ";
}
if ($parution != "") {

    $parutionlist = " and startup_depot.dt like '" . $parution . "' ";
} else {
    $parutionlist = " ";
}


if ($montant_l1 != "") {
    $sql_montant_funds = " and lf.montant<'1000' ";
}
if ($montant_l2 != "") {
    $sql_montant_funds = " and lf.montant>='1000' and lf.montant <10000 ";
}
if ($montant_l3 != "") {
    $sql_montant_funds = " and lf.montant>='10000' and lf.montant <100000 ";
}
if ($montant_l4 != "") {
    $sql_montant_funds = " and lf.montant>='100000' ";
}



if ($rachat != "") {
    $sqlrachat = " and lf.rachat=1  ";
} else {
    $sqlrachat = " ";
}
if ($ipo != "") {
    $sqlipo = " and lf.rachat=2  ";
} else {
    $sqlipo = " ";
}
if ($depot_bilan == 1) {
    $sqldepotbilan = " and startup.sup_radie=1  ";
} else {
    $sqldepotbilan = " ";
}


if ($chiff_a1 != "") {
    $sqlca = " and startup_ca.ca<=1000000  ";
} else {
    $sqlca = " ";
}
if ($chiff_a2 != "") {
    $sqlca2 = " and startup_ca.ca>1000000 and startup_ca.ca<=10000000 ";
} else {
    $sqlca2 = " ";
}
if ($chiff_a3 != "") {
    $sqlca3 = " and startup_ca.ca>=10000000  ";
} else {
    $sqlca3 = " ";
}
if (($chiff_a1 != '') || ($chiff_a2 != '') || ($chiff_a3 != '')) {
    $sqllinkca = " inner join startup_ca on startup_ca.id_startup=startup.id ";
} else {
    $sqllinkca = " ";
}

if (isset($_SESSION['data_login'])) {
    if ($users['premium'] == 0) {
        $quota = "10";
    }
    if ($users['premium'] == 1) {
        $quota = "100";
    }
} else {
    $quota = 10;
}



$_SESSION['ma_base_url'] = $monUrl;
unset($_SESSION['ma_requete']);

$totas = mysqli_num_rows(mysqli_query($link, "select startup.id from startup inner join activite on activite.id_startup=startup.id $roundlink $typelink $personneslink $formatiolink $doc_legallink $doc_beneficelink $sqllinkca $doc_capitallink $explink $skillslink where status=1 $prenomlist $sqlca $sqlca2 $sqlca3 $nomlist $global $cplist $villelist $doc_legallist $var_rachat $experiencelist $sql_resultat_min $sql_resultat_max $capitalist_min $capitallist_max $parutionlist $keyword_searchlist $chaine_cc $chaine_market $chaine_market3 $sql_date_funds1 $sql_date_funds2 $chaine_market2 $naflist $chaine_b $chaine_ff $chaine_raised_fund $skillslist $regionlist $sirenlist $sous_nafsqllist $typelist  $effectiflist_max $formationlist $sql_date_creation_deb $sql_date_creation_fin  $effectiflist_min $cearition_finlist $cearition_deblist $sql_raised_funds_A $sqlrachat $sqlipo $sqldepotbilan $sql_montant_funds $sql_date_deb $sql_date_fin group by startup.id"));
$_SESSION['ma_requete'] = "select startup.tour_serie,startup.sup_radie,startup.url,startup.date_add,startup.concat_tags,startup.domain, startup.status,startup.short_fr,startup.region_new,startup.rang_effectif, startup.id,startup.nom,startup.logo,startup.creation,startup.ville,startup.cible from startup inner join activite on activite.id_startup=startup.id $roundlink $typelink $personneslink $formatiolink $doc_legallink $doc_beneficelink $sqllinkca $doc_capitallink $explink $skillslink  where status=1  $prenomlist $sqlca $sqlca2 $sqlca3 $nomlist $global $cplist $villelist $experiencelist $doc_legallist $var_rachat $sql_resultat_min $sql_resultat_max $capitalist_min $capitallist_max $parutionlist $keyword_searchlist $chaine_cc $chaine_market $chaine_market3 $sql_date_funds1 $sql_date_funds2 $chaine_market2 $naflist $chaine_b $chaine_ff $sql_date_creation_deb $sql_date_creation_fin $sqlrachat $sqlipo $chaine_raised_fund $sqldepotbilan $regionlist $sirenlist $sous_nafsqllist $typelist $skillslist $effectiflist_max $formationlist  $effectiflist_min $cearition_finlist $cearition_deblist $sql_raised_funds_A $sql_montant_funds $sql_date_deb $sql_date_fin group by startup.id order by id desc limit $quota";

$sql100 = mysqli_query($link, "select startup.tour_serie,startup.sup_radie,startup.url,startup.date_add,startup.concat_tags,startup.domain, startup.status,startup.short_fr,startup.region_new,startup.rang_effectif, startup.id,startup.nom,startup.logo,startup.creation,startup.ville,startup.cible from startup inner join activite on activite.id_startup=startup.id $roundlink $typelink $personneslink $formatiolink $doc_legallink $doc_beneficelink $sqllinkca $doc_capitallink $explink $skillslink  where status=1  $prenomlist $sqlca $sqlca2 $sqlca3 $nomlist $global $cplist $villelist $experiencelist $doc_legallist $var_rachat $sql_resultat_min $sql_resultat_max $capitalist_min $capitallist_max $parutionlist $keyword_searchlist $chaine_cc $chaine_market $sql_date_creation_deb $sql_date_creation_fin $chaine_market3 $sql_date_funds1 $sql_date_funds2 $chaine_market2 $naflist $chaine_b $chaine_ff $sqlrachat $sqlipo $chaine_raised_fund $sqldepotbilan $regionlist $sirenlist $sous_nafsqllist $typelist $skillslist $effectiflist_max $formationlist  $effectiflist_min $cearition_finlist $cearition_deblist $sql_raised_funds_A $sql_montant_funds $sql_date_deb $sql_date_fin group by startup.id order by id desc limit $quota");
$sql1000 = mysqli_query($link, "select startup.tour_serie,startup.sup_radie,startup.url,startup.date_add,startup.concat_tags,startup.domain, startup.status,startup.short_fr,startup.region_new,startup.rang_effectif, startup.id,startup.nom,startup.logo,startup.creation,startup.ville,startup.cible from startup inner join activite on activite.id_startup=startup.id $roundlink $typelink $personneslink $formatiolink $doc_legallink $doc_beneficelink $sqllinkca $doc_capitallink $explink $skillslink  where status=1 $prenomlist $sqlca $sqlca2 $sqlca3 $nomlist $global $cplist $villelist $experiencelist $doc_legallist $var_rachat $sql_resultat_min $sql_resultat_max $capitalist_min $capitallist_max $parutionlist $keyword_searchlist $chaine_cc $chaine_market $sql_date_creation_deb $sql_date_creation_fin $chaine_market3 $sql_date_funds1 $sql_date_funds2 $chaine_market2 $naflist $chaine_b $chaine_ff $sqlrachat $sqlipo $chaine_raised_fund $sqldepotbilan $regionlist $sirenlist $sous_nafsqllist $typelist $skillslist $effectiflist_max $formationlist  $effectiflist_min $cearition_finlist $cearition_deblist $sql_raised_funds_A $sql_montant_funds $sql_date_deb $sql_date_fin group by startup.id");
$nbr_result_limit = mysqli_num_rows($sql100);
$nbr_result = mysqli_num_rows($sql1000);
$_SESSION['nbr_result'] = $nbr_result;
$_SESSION['list_id'] = "";

//echo "select startup.tour_serie,startup.sup_radie,startup.url,startup.date_add,startup.concat_tags,startup.domain, startup.status,startup.short_fr,startup.region_new,startup.rang_effectif, startup.id,startup.nom,startup.logo,startup.creation,startup.ville,startup.cible from startup inner join activite on activite.id_startup=startup.id $roundlink $typelink $personneslink $formatiolink $doc_legallink $doc_beneficelink $sqllinkca $doc_capitallink $explink $skillslink  where status=1 and sup_radie=0 $prenomlist $sqlca $sqlca2 $sqlca3 $nomlist $global $cplist $villelist $experiencelist $doc_legallist $var_rachat $sql_resultat_min $sql_resultat_max $capitalist_min $capitallist_max $parutionlist $keyword_searchlist $chaine_cc $chaine_market $chaine_market3 $sql_date_funds1 $sql_date_funds2 $chaine_market2 $naflist $chaine_b $chaine_ff $sqlrachat $sqlipo $chaine_raised_fund $sqldepotbilan $regionlist $sirenlist $sous_nafsqllist $typelist $skillslist $effectiflist_max $formationlist  $effectiflist_min $cearition_finlist $cearition_deblist $sql_raised_funds_A $sql_montant_funds $sql_date_deb $sql_date_fin group by startup.id order by id desc limit $quota";
?>
<div class="searchHeader">
    <div class="searchHeader__title"><?php echo $nbr_result_limit ?> startups affichées / <?php echo $totas ?> startups sélectionnées</div>
    <div class="searchHeader__actions">
        <button class="btn btn-sm btn-bordered btn-searchfilters btn-filter">Filtrer</button>
        <?php
        if (isset($_SESSION['data_login'])) {
            ?>
            <button class="btn btn-sm btn-primary btn-bookmark btn-saveSearch">Enregistrer</button>
            <?php
        }
        ?>
        <a href="<?php echo URL ?>/recherche-startups" class="btn btn-sm btn-primary btn-reset">Réinitialiser</a>
    </div> 
</div>
<div class="searchFilters">
    <div class="searchFilters__tags tags tags--left tags--hascross">
        <?php echo $tags_sector . " " . $tags_region . " " . $tag_ville; ?>
    </div>
    <div class="searchFilters__tableOpt">
        <?php
        if (isset($_SESSION['data_login'])) {
            ?>
            <ul>
                <li class="columns">
                    <a href="#" title="#" class="btn-searchcolumns"><span class="ico-columns"></span> Voir plus de colonnes</a>
                </li>
            </ul>
        <?php } ?>
    </div>
</div>
<div class="table-search-wrapper">
    <table class="table table-search" id="mine">
        <thead>
            <tr>
                <?php foreach ($searchColumns as $col) { ?>
                    <th data-column="<?= $col['id']; ?>" class="<?= $col['class']; ?>" data-sortable="<?= $col['sortable']; ?>"><?= $col['name']; ?></th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $count = 1;
            while ($sups = mysqli_fetch_array($sql100)) {

                $secteurs = mysqli_fetch_array(mysqli_query($link, "Select  secteur.nom_secteur,secteur.id,secteur.nom_secteur_en From   secteur Inner Join  activite    On activite.secteur = secteur.id Where  activite.id_startup =" . $sups['id']));
                $ssecteurs = mysqli_fetch_array(mysqli_query($link, "Select  sous_secteur.nom_sous_secteur,sous_secteur.id,sous_secteur.nom_sous_secteur_en From   sous_secteur Inner Join  activite    On activite.sous_secteur = sous_secteur.id Where  activite.id_startup =" . $sups['id']));
                $activite = mysqli_fetch_array(mysqli_query($link, "Select last_activite.nom_activite,last_activite.id From   last_activite Inner Join  activite    On activite.activite = last_activite.id Where  activite.id_startup =" . $sups['id']));
                $sactivite = mysqli_fetch_array(mysqli_query($link, "Select  last_sous_activite.nom_sous_activite,last_sous_activite.id From   last_sous_activite Inner Join  activite    On activite.sous_activite = last_sous_activite.id Where  activite.id_startup =" . $sups['id']));

                $tags_list = mysqli_fetch_array(mysqli_query($link, "Select  activite.tags From  activite  Where  activite.id_startup =" . $sups['id']));
                $lf_somme = mysqli_fetch_array(mysqli_query($link, "Select  sum(montant) as somme From  lf inner join startup on lf.id_startup=startup.id  Where startup.status=1 and lf.rachat=0 and startup.id =" . $sups['id']));
                $lf_nb = mysqli_fetch_array(mysqli_query($link, "Select  count(*) as nb From  lf inner join startup on lf.id_startup=startup.id  Where startup.status=1 and lf.rachat=0 and startup.id =" . $sups['id']));
                $rachat_nb = mysqli_num_rows(mysqli_query($link, "Select  id From  lf inner join startup on lf.id_startup=startup.id  Where startup.status=1 and lf.rachat=1 and startup.id =" . $sups['id']));
                $ipo_nb = mysqli_num_rows(mysqli_query($link, "Select id From  lf inner join startup on lf.id_startup=startup.id  Where startup.status=1 and lf.rachat=2 and startup.id =" . $sups['id']));
                $sup_ca = mysqli_fetch_array(mysqli_query($link, "Select  startup_ca.ca,startup_ca.annee From  startup_ca inner join startup on startup_ca.id_startup=startup.id  Where startup.status=1 and  startup.id =" . $sups['id'] . " order by annee desc limit 1"));
                $ch = "";
                $chiffre = mysqli_fetch_array(mysqli_query($link, "select * from startup_score where id_startup=" . $sups['id']));



                if ($sactivite['id'] == "")
                    $idsousactivite = 0;
                else
                    $idsousactivite = $sactivite['id'];

                if ($ssecteurs['id'] == "")
                    $idsoussecteur = 0;
                else
                    $idsoussecteur = $ssecteurs['id'];

                if ($activite['id'] == "")
                    $idactivite = 0;
                else
                    $idactivite = $activite['id'];



                $ligne_secteur_activite = mysqli_fetch_array(mysqli_query($link, "select * from secteur_activite where id_secteur=" . $secteurs['id'] . " and id_sous_secteur=" . $idsoussecteur . " and id_activite=" . $idactivite . " and id_sous_activite=" . $idsousactivite));



                $somme_global_competition = mysqli_fetch_array(mysqli_query($link, "select sum(competition_secteur) as somme from secteur_activite"));
                $nb_global_competition = mysqli_num_rows(mysqli_query($link, "select competition_secteur from secteur_activite where competition_secteur!=0"));
                $moy_competition_secteur = $somme_global_competition['somme'] / $nb_global_competition;

                $_SESSION['list_id'] = $_SESSION['list_id'] . $sups['id'] . ",";

                $tabs = explode(",", $sups['concat_tags']);
                $nbrs = count($tabs);
                for ($i = 0; $i < 6; $i++) {
                    if (trim($tabs[$i]) != '') {
                        $ch = $ch . ' <div style="background-color: #D1ECF0;color: #000;padding: 5px;border-radius: 5px;font-size: 10px;margin-bottom:5px;float: left;margin-right: 5px;">' . $tabs[$i] . '</div>';
                    }
                }
                if ($sups['status'] == 1 && $sups['bilan'] == 0) {
                    $status = 'Active';
                }
                if ($sups['status'] == 1 && $sups['bilan'] == 1) {
                    $status = 'Fermée';
                }
                if ($rachat_nb > 0) {
                    $status = 'Rachetée';
                }
                if ($ipo_nb > 0) {
                    $status = 'En bourse';
                }
                $ssh = '';
                $tabs1 = ($sups['short_fr']);
                $bnrs = strlen(($sups['short_fr']));
                for ($o = 0; $o < 50; $o++) {
                    $ssh .= $tabs1[$o];
                }
                if ($bnrs >= 50) {
                    $ssh = $ssh . "...";
                }

                $tabs22 = explode(",", $sups['concat_tags']);
                $nbs_tag = count($tabs22);
                $tagss = "";
                for ($j = 0; $j < 3; $j++) {

                    $val = trim(str_replace(array(" ", "-", "é", "è", "ê"), array("_", "__", "e", "e", "e"), trim(utf8_encode($tabs22[$j]))));
                    $tagss .= '<div  class="table-search__link"><a id="' . addslashes(utf8_encode($tabs22[$j])) . '" onclick="afficher_par_tag(\'' . str_replace(array(" ", "-", "é", "è", "ê"), array("_", "__", "e", "e", "e"), utf8_encode($val)) . '\')">' . addslashes(($tabs22[$j])) . '</a></div> ';
                }
                $tab_site = explode("/", str_replace(array("https://", "http://", "www."), array("", "", ""), $sups['url']));
                ?>
                <?php include("body.php"); ?>
                <?php
                $count++;
            }
            ?>


        </tbody>
    </table>
</div>
