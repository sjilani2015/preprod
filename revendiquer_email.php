
<div style="text-align:center;">
    <h2 class="sub_title" style="line-height: 24px;font-size: 2.6rem;text-align: center;margin-top: 8px;margin-bottom: 32px;">Bravo ! Vérifiez vos emails<br>et validez votre demande de liaison</h2>
    <button type="button" style="width:261px;height: 40px;background: #D4EDDA;border: 1px solid #D4EDDA; color:#155724;opacity: 1;font-family: Roboto;font-size: 1.2rem;line-height: 16px;padding-left: 0px;margin-left: 160px;padding-right: 0px;margin-right: 160px;padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px;">Un code vient de vous être envoyé par email</button>
    <div class="colgridform">
        <form>
            <div class="gridformtext" style="margin-top: 16px;">
                <div style="margin-left: 128px;">
                    <label>Votre code de vérification</label>
                    <input type="text" name="code" id="code" value="" autocomplete="off" style="text-align:center" placeholder="" >
                </div>
                <div>
                    <input type="submit" style="background: #00CCFF;border-radius: 4px;padding: 8px;width: 89px;font-family: 'Roboto';height: 38px;font-size: 12px;line-height: 12px;margin-right: 0px;margin-bottom: 0px;margin-top: 24px;" name="Valider" value="Valider" />
                </div>
            </div>
        </form>

        <h5 style="text-align:left;font-size:16px;color: #425466;font-family: 'EuclidBold';line-height: 24px;margin-top: 16px;margin-bottom: 16px;">Prochaines étapes</h5>
        <div style="margin-top: 16px;">
            <p style="font-family: 'Roboto';line-height: 16px!important">Si votre demande est approuvée : vous en serez informé par e-mail et vous pourrez gérer le profil de l’établissement dans votre dashboard<br><br> Si votre demande est refusée : vous en serez informé par e-mail et vous pourrez toujours contacter notre service de modération et, dans certains cas, faire appel de ce refus.
        </div>
        <h5 style="text-align:left;font-size:16px;color: #425466;font-family: 'EuclidBold';line-height: 24px;margin-top: 16px;margin-bottom: 16px;">Contacter notre service de modération</h5>
        <div style="margin-top: 16px;">
            <p style="font-family: 'Roboto';line-height: 16px!important">Sed lobortis viverra libero sed vehicula. Nunc vestibulum condimentum turpis sed convallis. Etiam hendrerit purus hendrerit, semper odio eu, aliquet felis. Nulla blandit maximus urna, et dictum orci congue et. Nulla varius eros et urna auctor vehicula. Duis volutpat quis ipsum eget varius. Morb
        </div>                                  
        <div style="text-align:center;margin-top: 16px;margin-left: 224px;margin-right: 224px;height: 38px;margin-bottom: 32px;">
            <input type="submit" style="background: #00CCFF;border-radius: 4px;padding: 8px;width: 89px;font-family: 'Roboto';height: 38px;font-size: 12px;line-height: 12px;margin-right: 0px;margin-bottom: 0px;" data-dismiss="modal" name="Fermer" value="Fermer" />
        </div>
    </div>
</div>

