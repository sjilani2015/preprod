<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');


$monUrl = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
/* $_SESSION['utm_source'] = $monUrl;

  $users = mysqli_fetch_array(mysqli_query($link, "select premium,id from user where email='" . $_SESSION['data_login'] . "'"));

  /*
 * Test de quota
 */
/*
  if ($users['premium'] == 0) {
  mysqli_query($link, "update user set vu=vu+1 where id=" . $users['id']);
  mysqli_query($link, "insert into user_vu(user,dt)values(" . $users['id'] . ",'" . date('Y-m-d') . "')");
  $verif_vu = mysqli_num_rows(mysqli_query($link, "select id from user_vu where user=" . $users['id'] . " and dt='" . date('Y-m-d') . "'"));
  if ($verif_vu > 10) {
  echo '<script>window.location="' . url . 'expiration-offre"</script>';
  }
  } */

/*
 * Récupérer id Startup
 */
$_GET["code"] = 44449;
//$_GET["code"] = 285349;
/* if (isset($_GET["startup"]) && $_GET["startup"] != '') {
  $startup_id = $_GET["startup"];
  $code = $_GET["code"];
  } else
  header('location:' . url . $_SESSION["myfs_language"] . '/404');
 */


$code = $_GET["code"];
$id_startup = degenerate_id($code);

$_SESSION['id_sup'] = $id_startup;

$startup_capital = getStartupCapital($id_startup);
$nb_evolution = mysqli_num_rows(mysqli_query($link, "select id from startup_capital where id_startup=" . $id_startup));


$startup_ca = getStartupCA($id_startup);


$ca1 = str_replace(array("€", " ", ","), array("", "", "."), $startup_ca[0]["ca"]);
$ca2 = str_replace(array("€", " ", ","), array("", "", "."), $startup_ca[1]["ca"]);
$anneeca1 = $startup_ca[0]["annee"];
$anneeca2 = $startup_ca[1]["annee"];

$row_fondateur_startup = getFondateurByIdStartup($id_startup);

$startup = getStartupById($id_startup);

$_SESSION['startup_nom_revendiquer'] = $startup[0]['nom'];
$_SESSION['startup_id_revendiquer'] = $startup[0]['id'];

// echo strlen(substr($descriptionfr, 0, 155));
$row_nom_secteur = getSecteurByIdStartup($startup[0]['id']);
if ($row_nom_secteur[0]['id'] != "") {
    $row_nom_sous_secteur = getSousSecteurByIdStartup($startup[0]['id']);
}


$id_secteur = mysqli_fetch_array(mysqli_query($link, "Select  secteur.id,secteur.nom_secteur,secteur.nom_secteur_en From secteur inner join activite on activite.secteur=secteur.id Where  activite.id_startup =" . $startup[0]['id']));
$id_sous_secteur = mysqli_fetch_array(mysqli_query($link, "Select  sous_secteur.id,sous_secteur.nom_sous_secteur,sous_secteur.nom_sous_secteur_en From  sous_secteur inner join activite on activite.sous_secteur=sous_secteur.id Where  activite.id_startup =" . $startup[0]['id']));
$id_sous_activite = mysqli_fetch_array(mysqli_query($link, "Select  last_sous_activite.id,last_sous_activite.nom_sous_activite From  last_sous_activite inner join activite on activite.sous_activite=last_sous_activite.id Where  activite.id_startup =" . $startup[0]['id']));
$id_activite = mysqli_fetch_array(mysqli_query($link, "Select  last_activite.id,last_activite.nom_activite,last_activite.nom_activite_en From  last_activite inner join activite on activite.activite=last_activite.id Where  activite.id_startup =" . $startup[0]['id']));

if ($id_sous_activite['id'] == "")
    $idsousactivite = 0;
else
    $idsousactivite = $id_sous_activite['id'];

if ($id_sous_secteur['id'] == "")
    $idsoussecteur = 0;
else
    $idsoussecteur = $id_sous_secteur['id'];

if ($id_activite['id'] == "")
    $idactivite = 0;
else
    $idactivite = $id_activite['id'];


$ligne_secteur_activite = mysqli_fetch_array(mysqli_query($link, "select * from secteur_activite where id_secteur=" . $id_secteur['id'] . " and id_sous_secteur=" . $idsoussecteur . " and id_activite=" . $idactivite . " and id_sous_activite=" . $idsousactivite));
$ligne_secteur_secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur_secteur where id_secteur=" . $id_secteur['id']));
$ligne_france = mysqli_fetch_array(mysqli_query($link, "select * from secteur_france where 1"));

$somme_global_competition = mysqli_fetch_array(mysqli_query($link, "select sum(competition_secteur) as somme from secteur_activite"));
$nb_global_competition = mysqli_num_rows(mysqli_query($link, "select competition_secteur from secteur_activite where competition_secteur!=0"));
$moy_competition_secteur = $somme_global_competition['somme'] / $nb_global_competition;

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}

$chiffre = mysqli_fetch_array(mysqli_query($link, "select * from startup_score where id_startup=" . $startup[0]['id']));
if ($chiffre['maturite'] != "") {
    $maturite = $chiffre['maturite'];
    $matirute_courbe = $chiffre['maturite'];
} else {
    $maturite = "-";
    $matirute_courbe = 0;
}
if ($chiffre['anciennete'] != "") {
    $anciennete = $chiffre['anciennete'];
    $anciennete_courbe = $chiffre['anciennete'];
} else {
    $anciennete = "-";
    $anciennete_courbe = 0;
}
if ($chiffre['traction_likedin'] != "") {
    $traction = $chiffre['traction_likedin'];
    $traction_courbe = $chiffre['traction_likedin'];
} else {
    $traction = "-";
    $traction_courbe = 0;
}


$menu = 2;
?>
<?php
$tab_somme_total_lf = getTotalLfByStartup($startup[0]["id"]);


$tab_last_deal = getLastDealByStartup($startup[0]["id"]);
$row_total_investisseur = getTotalInvestisseurByStartup($startup[0]["id"]);
if (!empty($row_total_investisseur)) {
    $ch_invest = "";
    foreach ($row_total_investisseur as $deal) {
        $ch_invest .= stripslashes($deal['de']) . ",";
    }
}

$nb_founder = mysqli_num_rows(mysqli_query($link, "select id from personnes where id_startup=" . $startup[0]["id"]));

$tab_invest = explode(",", $ch_invest);

$verif_rachat = mysqli_num_rows(mysqli_query($link, "select id from lf where id_startup=" . $startup[0]["id"] . " and rachat=1 "));
$verif_ipo = mysqli_num_rows(mysqli_query($link, "select id from lf where id_startup=" . $startup[0]["id"] . " and rachat=2 "));

$tot_leve_france = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf inner join startup on startup.id=lf.id_startup inner join activite on startup.id=activite.id_startup  where startup.status=1 and lf.rachat=0 and activite.secteur=" . $row_nom_secteur[0]['id'] . ""));
$tot_nb_france = mysqli_fetch_array(mysqli_query($link, "select count(*) as nb from lf inner join startup on startup.id=lf.id_startup inner join activite on startup.id=activite.id_startup  where startup.status=1 and lf.rachat=0 and montant>0 and montant!='NC' and activite.secteur=" . $row_nom_secteur[0]['id'] . ""));

$moy_france = $tot_leve_france['somme'] / $tot_nb_france['nb'];

$purcentage_lf = ($tab_somme_total_lf[0]["somme"] * 100) / $moy_france;

$descriptionfr = $startup[0]['long_fr'];


$sql_list_personnes = mysqli_query($link, "select * from personnes where id_startup=" . $startup[0]['id'] . " and nom !='' and etat=1 order by personnes.id");
$nb_personnes = mysqli_num_rows($sql_list_personnes);
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">

        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script>am4core.addLicense("CH303325752");</script>

        <script>
            const   themeColorBlue = am4core.color('#00bff0'),
                    themeColorFill = am4core.color('#E1F2FA'),
                    themeColorGrey = am4core.color('#dadada'),
                    themeColorLightGrey = am4core.color('#F5F5F5'),
                    themeColorDarkgrey = am4core.color("#33333A"),
                    themeColorLine = am4core.color("#87D6E3"),
                    themeColorLineRed = am4core.color("#FF7C7C"),
                    themeColorLineGreen = am4core.color("#21D59B"),
                    themeColorColumns = am4core.color("#E1F2FA"),
                    labelColor = am4core.color('#798a9a');
        </script>
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">

                    <section class="module">
                        <div class="module__title"><?php echo ($startup[0]['nom']); ?></div>
                        <div class="tags">
                            <a href="<?php echo URL ?>/etude-secteur/<?php echo generate_id($row_nom_secteur[0]['id']) . "/" . urlWriting($row_nom_secteur[0]['nom_secteur']) ?>" title="" class="tags__el tags__el--pinky">
                                <span class="ico-chart"></span> <?php echo ($row_nom_secteur[0]["nom_secteur"]); ?>
                            </a>
                            <?php
                            if ($startup[0]['region_new'] != '') {
                                $get_id_region = mysqli_fetch_array(mysqli_query($link, "select * from region_new where region_new='" . addslashes($startup[0]['region_new']) . "'"));
                                ?>
                                <a href="<?php echo URL . '/region/' . generate_id($get_id_region['id']) . "/" . urlWriting(strtolower($get_id_region['region_new'])) ?>" title="" target="_blank" class="tags__el tags__el--pinky">
                                    <span class="ico-chart"></span> <?php echo ($startup[0]['region_new']) ?>
                                </a>
                                <?php
                            }
                            ?>
                            <?php
                            if ($startup[0]['ville'] != '') {
                                ?>
                                <a href="#" title="" class="tags__el">
                                    <span class="ico-tag"></span> <?php echo ($startup[0]['ville']); ?>
                                </a>
                                <?php
                            }
                            ?>
                            <?php
                            if ($id_sous_secteur["nom_sous_secteur"] != "") {
                                ?>
                                <a href="#" title="" class="tags__el">
                                    <span class="ico-tag"></span> <?php echo ($id_sous_secteur["nom_sous_secteur"]); ?>
                                </a>
                                <?php
                            }
                            ?>
                            <?php
                            if ($id_activite["nom_activite"] != "") {
                                ?>
                                <a href="#" title="" class="tags__el">
                                    <span class="ico-tag"></span> <?php echo ($id_activite["nom_activite"]); ?>
                                </a>
                                <?php
                            }
                            ?>
                            <?php
                            if ($id_sous_activite["nom_sous_activite"] != "") {
                                ?>
                                <a href="#" title="" class="tags__el">
                                    <span class="ico-tag"></span> <?php echo ($id_sous_activite["nom_sous_activite"]); ?>
                                </a>
                                <?php
                            }
                            ?>

                            <?php
                            $concat_tag_tab = explode(",", $startup[0]['concat_tags']);
                            $nb_tagss = count($concat_tag_tab);

                            for ($ii = 0; $ii < $nb_tagss; $ii++) {
                                if ($concat_tag_tab[$ii] != "") {
                                    ?>
                                    <a href="<?php echo URL; ?>/recherche-startups/tags/<?php echo addslashes(str_replace(array("-", " ", "&", "é", "è", "ê", "î", "ï", "ô"), array("__", "_", "..", "e", "e", "e", "i", "i", "o"), $concat_tag_tab[$ii])); ?>" target="_blank" title="" class="tags__el">
                                        <span class="ico-tag"></span> <?php echo ($concat_tag_tab[$ii]); ?>
                                    </a>
                                    <?php
                                }
                            }
                            ?>
                        </div>

                        <div class="ctg ctg--1_3">
                            <aside class="bloc text-center">
                                <div class="companyLogo">
                                    <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $startup[0]['logo']) ?>" alt="<?php echo $startup[0]['nom']; ?>" />
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Total des fonds levés</div>
                                    <div class="datasbloc__val"><?php
                                        if ($tab_somme_total_lf[0]["somme"] != 0) {
                                            echo str_replace(",0", "", number_format($tab_somme_total_lf[0]["somme"] / 1000, 1, ',', ' ')) . " M€";
                                        } else
                                            echo "-";
                                        ?></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Effectifs</div>
                                    <div class="datasbloc__val"><?php
                                        if ($startup[0]['rang_effectif'] == 1)
                                            echo "1";
                                        if (($startup[0]['rang_effectif'] > 1) && ($startup[0]['rang_effectif'] <= 10))
                                            echo "2 à 10";
                                        if (($startup[0]['rang_effectif'] > 10) && ($startup[0]['rang_effectif'] <= 50))
                                            echo "11 à 50";
                                        if (($startup[0]['rang_effectif'] > 51) && ($startup[0]['rang_effectif'] <= 100))
                                            echo "51 à 100";
                                        if ($startup[0]['rang_effectif'] > 100)
                                            echo ">100";
                                        ?></div>
                                </div>
                                <ul class="companyInfos">
                                    <li>Création : <?php echo $startup[0]['creation'] ?></li>
                                    <li>Capital : <?php
                                        $capital1 = str_replace(array("€", " "), array("", ""), $startup_capital[0]["capital"]);
                                        if ($capital1 != "") {
                                            ?>


                                            <?php echo str_replace(",0", "", number_format(($capital1 / 1000), 1, ",", " ")) ?> K€

                                            <?php
                                        } else {
                                            ?>
                                            -
                                            <?php
                                        }
                                        ?></li>
                                    <li><?php
                                        if ($nb_founder == 1)
                                            echo $nb_founder . " fondateur";
                                        else
                                            echo $nb_founder . " fondateurs";
                                        ?></li>
                                </ul>
                                <div class="bloc-actions bloc-actions--darkgrey">
                                    <div class="bloc-actions__ico">
                                        <a href="#" title="">
                                            <span class="icon-wrapper">
                                                <span class="ico-growup"></span>
                                            </span>
                                            Revendiquer cette startup
                                        </a>
                                    </div>
                                </div>
                            </aside>

                            <div class="bloc content_radar">
                                <div class="ctg ctg--2_2 ctg--fullheight">
                                    <div class="startupIntro">
                                        <?php
                                        $bloc_tab = explode(" ", strip_tags($startup[0]['long_fr']));
                                        $nbdd = count($bloc_tab);
                                        $bloc1 = "";
                                        $bloc2 = "";
                                        for ($p = 0; $p < 30; $p++) {
                                            $bloc1 .= $bloc_tab[$p] . " ";
                                        }
                                        for ($pp = 30; $pp < $nbdd; $pp++) {
                                            $bloc2 .= $bloc_tab[$pp] . " ";
                                        }
                                        ?>
                                        <h2><?php echo (stripslashes($startup[0]['short_fr'])); ?></h2>
                                        <p class="intro-short"><?php echo ($bloc1); ?> <span class="intro-seeMore">voir plus</span></p>
                                        <p class="intro-long"><?php echo ($bloc1 . $bloc2); ?> <span class="intro-seeLess">voir moins</span></p>

                                    </div>
                                    <div class="chart chart--radar">
                                        <div id="chart-radar"></div>
                                    </div>
                                    <script>
                                        am4core.ready(function () {


                                            /* Create chart instance */
                                            var chart = am4core.create("chart-radar", am4charts.RadarChart);
                                            /* Add data */
                                            chart.data = [{
                                                    "date": "A",
                                                    "value": <?php echo $ligne_secteur_activite['competition_secteur'] ?>,
                                                    "value2": <?php echo number_format($ligne_france['indice_competition_secteur'], 1, ".", "") ?>
                                                }, {
                                                    "date": "B",
                                                    "value": <?php echo $matirute_courbe; ?>,
                                                    "value2": <?php echo $ligne_secteur_activite['moy_maturite'] ?>
                                                }, {
                                                    "date": "C",
                                                    "value": <?php echo $chiffre["indice_transparence"]; ?>,
                                                    "value2": <?php echo $ligne_secteur_secteur["indice_transparence"] ?>
                                                }, {
                                                    "date": "D",
                                                    "value": <?php echo $chiffre['tours'] ?>,
                                                    "value2": <?php echo $ligne_secteur_secteur['moy_nbr_levee'] ?>
                                                }, {
                                                    "date": "E",
                                                    "value": <?php echo $anciennete_courbe; ?>,
                                                    "value2": <?php echo $ligne_secteur_activite['moy_anciennete']; ?>
                                                }, {
                                                    "date": "F",
                                                    "value": <?php echo $traction_courbe; ?>,
                                                    "value2": <?php echo $ligne_secteur_activite['moy_traction_likedin'] ?>
                                                }];
                                            chart.paddingTop = 0;
                                            chart.paddingBottom = 0;
                                            chart.paddingLeft = 0;
                                            chart.paddingRight = 0;
                                            /* Create axes */
                                            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                            categoryAxis.dataFields.category = "date";
                                            categoryAxis.renderer.ticks.template.strokeWidth = 2;
                                            categoryAxis.renderer.labels.template.fontSize = 11;
                                            categoryAxis.renderer.labels.template.fill = labelColor;
                                            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                            valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
                                            valueAxis.renderer.axisFills.template.fillOpacity = 0.05;
                                            valueAxis.renderer.labels.template.fontSize = 11;
                                            valueAxis.renderer.gridType = "polygons"


                                            /* Create and configure series */
                                            function createSeries(field, name, themeColor) {
                                                var series = chart.series.push(new am4charts.RadarSeries());
                                                series.dataFields.valueY = field;
                                                series.dataFields.categoryX = "date";
                                                series.strokeWidth = 2;
                                                series.fill = themeColor;
                                                series.fillOpacity = 0.3;
                                                series.stroke = themeColor;
                                                series.name = name;
                                                // Show bullets ?
                                                //let circleBullet = series.bullets.push(new am4charts.CircleBullet());
                                                //circleBullet.circle.stroke = am4core.color('#fff');
                                                //circleBullet.circle.strokeWidth = 2;
                                            }


                                            createSeries("value", "Startup", themeColorBlue);
                                            createSeries("value2", "Secteur", themeColorGrey);
                                            chart.legend = new am4charts.Legend();
                                            chart.legend.position = "top";
                                            chart.legend.useDefaultMarker = true;
                                            chart.legend.fontSize = "11";
                                            chart.legend.color = themeColorGrey;
                                            chart.legend.labels.template.fill = labelColor;
                                            chart.legend.labels.template.textDecoration = "none";
                                            chart.legend.valueLabels.template.textDecoration = "none";
                                            let as = chart.legend.labels.template.states.getKey("active");
                                            as.properties.textDecoration = "line-through";
                                            as.properties.fill = themeColorDarkgrey;
                                            let marker = chart.legend.markers.template.children.getIndex(0);
                                            marker.cornerRadius(12, 12, 12, 12);
                                            marker.width = 20;
                                            marker.height = 20;
                                        });
                                    </script>
                                    <div class="datagrid">
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">A :</span> Compétition secteur
                                            </div>
                                            <div class="datagrid__val"><?php
                                                if ($ligne_secteur_activite['competition_secteur'] != '')
                                                    echo str_replace(".", ",", $ligne_secteur_activite['competition_secteur']);
                                                else
                                                    echo "-";
                                                ?> <span class="ico-decrease"></span></div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['indice_competition_secteur'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">B :</span> Maturité
                                            </div>
                                            <div class="datagrid__val"><?php
                                                if ($chiffre['maturite'] != '')
                                                    echo str_replace(".", ",", $chiffre['maturite']);
                                                else
                                                    echo "-";
                                                ?> <span class="ico-decrease"></span></div>
                                            <div class="datagrid__subtitle">Moy. secteur <?php echo str_replace(".", ",", $ligne_secteur_activite['moy_maturite']); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">C :</span> Capacité à délivrer
                                            </div>
                                            <div class="datagrid__val"><?php
                                                if ($chiffre["indice_transparence"] != "")
                                                    echo $chiffre["indice_transparence"];
                                                else
                                                    echo "-";
                                                ?> <span class="ico-decrease"></span></div>
                                            <div class="datagrid__subtitle">Moy. secteur <?php echo str_replace(".", ",", $ligne_secteur_secteur['indice_transparence']) ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">D :</span> Nombre de levées
                                            </div>
                                            <div class="datagrid__val"><?php
                                                if ($chiffre['tours'] != "")
                                                    echo str_replace(".", ",", $chiffre['tours']);
                                                else
                                                    echo "-";
                                                ?></div>
                                            <div class="datagrid__subtitle">Moy. secteur <?php echo str_replace(".", ",", $ligne_secteur_secteur['moy_nbr_levee']); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">E :</span> Ancienneté
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $anciennete); ?></div>
                                            <div class="datagrid__subtitle">Moy. secteur <?php echo str_replace(".", ",", $ligne_secteur_activite['moy_anciennete']); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">F :</span> Traction LinkedIn
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $traction); ?> <span class="ico-decrease"></span></div>
                                            <div class="datagrid__subtitle">Moy. secteur <?php echo str_replace(".", ",", $ligne_secteur_activite['moy_traction_likedin']); ?></div>
                                        </div>
                                    </div>

                                    <div class="bloc-actions">
                                        <div class="bloc-actions__ico">
                                            <a style="cursor: pointer" onclick="memoriser(<?php echo $startup[0]['id']; ?>)" title="">
                                                <span class="icon-wrapper">
                                                    <span class="ico-bookmark"></span>
                                                </span>
                                                Mémoriser
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="bloc_radar"></div>

                        </div>

                    </section>


                    <section class="module module--finances content_finance">
                        <div class="module__title">Finances : capitaux et levées de fonds</div>
                        <div class="bloc">
                            <div class="row">
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Montant global levé</div>
                                    <div class="datasbloc__val"><?php
                                        if ($tab_somme_total_lf[0]["somme"] != 0) {
                                            echo str_replace(",0", "", number_format($tab_somme_total_lf[0]["somme"] / 1000, 1, ',', ' ')) . " M€";
                                        } else
                                            echo "-";
                                        ?></div>
                                    <div class="datasbloc__subtitle"><?php
                                        if ($tab_somme_total_lf[0]["somme"] != 0) {
                                            if ($purcentage_lf > 0) {

                                                echo "+" . number_format($purcentage_lf, 0, ",", " ") . "% / moy. secteur";
                                            }
                                            if ($purcentage_lf < 0) {
                                                echo "-" . number_format($purcentage_lf, 0, ",", " ") . "% / moy. secteur";
                                            }
                                            if ($purcentage_lf == 0) {
                                                echo "-";
                                            }
                                        } else {
                                            echo "";
                                        }
                                        ?></div>
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Nombre de levées</div>
                                    <div class="datasbloc__val"><?php echo $chiffre['tours'] ?></div>
                                    <div class="datasbloc__subtitle"><?php
                                        if ($chiffre['nb_investisseur'] != '')
                                            echo $chiffre['nb_investisseur'] . " VC";
                                        else
                                            echo "-";
                                        ?></div>
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Fréquence</div>
                                    <div class="datasbloc__val"><?php
                                        if (($chiffre['frequence_levee'] != '') && ($chiffre['frequence_levee'] != 1))
                                            echo str_replace(".", ",", $chiffre['frequence_levee']) . " mois";
                                        else
                                            echo "-";
                                        ?></div>
                                    <div class="datasbloc__subtitle" style="white-space: nowrap;">Délai moyen entre 2 levées</div>
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key"style="white-space: nowrap;">Probabilité de future levée</div>
                                    <div class="datasbloc__val"><?php
                                        if ($chiffre['prob_levee'] != "")
                                            echo $chiffre['prob_levee'];
                                        else
                                            echo "Faible";
                                        ?></div>
                                    <div class="datasbloc__subtitle">à 6 mois</div>
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Capital social</div>
                                    <?php
                                    $capital2 = str_replace(array("€", " "), array("", ""), $startup_capital[0]["capital"]);


                                    if ($capital2 != "") {
                                        ?>
                                        <div class="datasbloc__val"><?php echo str_replace(",0", "", number_format(($capital2 / 1000), 1, ",", " ")) ?> K€</div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="datasbloc__val">-</div>
                                    <?php } ?>
                                    <div class="datasbloc__subtitle"><?php echo $nb_evolution; ?> évolutions</div>
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">CA</div>
                                    <div class="datasbloc__val"><?php
                                        if ($ca1 != 0)
                                            echo str_replace(',00', '', number_format($ca1 / 1000000, 2, ",", " ")) . " M€";
                                        else
                                            echo "-";
                                        ?></div>
                                    <div class="datasbloc__subtitle"><?php
                                        $ca1 = str_replace(array("€", " ", ","), array("", "", "."), $startup_ca[0]["ca"]);
                                        $ca2 = str_replace(array("€", " ", ","), array("", "", "."), $startup_ca[1]["ca"]);
                                        $anneeca1 = $startup_ca[0]["annee"];
                                        $anneeca2 = $startup_ca[1]["annee"];

                                        if ($ca2 != '') {
                                            $diff = trim($ca1) - trim($ca2);
                                            $pourcentage = ($diff * 100) / $ca2;
                                            if ($diff > 0) {

                                                echo "<img src='" . URL . "/images/arrow-up.svg' alt=''> " . number_format($pourcentage, 0, ",", " ") . "%";
                                            }
                                            if ($diff < 0) {
                                                echo "<img src='" . URL . "/images/arrow-down.svg' alt=''> " . number_format($pourcentage, 0, ",", " ") . "%";
                                            }
                                            if ($diff == 0) {
                                                echo "<img src='" . URL . "/images/arrow-fix.svg' alt=''> ";
                                            }
                                        } else {
                                            echo "&nbsp;";
                                        }
                                        ?></div>
                                </div>
                            </div>
                            <?php
                            if ($chiffre['tours'] > 0) {
                                ?>
                                <div class="bloc__title mtop">Détails des levées de fonds de <?php echo $startup[0]['nom'] ?></div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Montant</th>
                                                <th class="text-center">Date</th>
                                                <th>Investisseurs</th>
                                            </tr>
                                        </thead>
                                        <?php
                                        $row_last_lf_all_startup = getAllLFByStartup($startup[0]['id']);
                                        if (!empty($row_last_lf_all_startup)) {
                                            ?>
                                            <tbody>
                                                <?php
                                                $sql_rachat = mysqli_query($link, "select * from lf where rachat=1 and id_startup=" . $startup[0]['id']);
                                                $nb_rachat = mysqli_num_rows($sql_rachat);
                                                if ($nb_rachat > 0) {
                                                    while ($all_rachat = mysqli_fetch_array($sql_rachat)) {
                                                        ?>
                                                        <tr>
                                                            <td class="text-center moneyCell"><?php
                                                                if ($all_rachat['montant'] != 0) {
                                                                    echo str_replace(",0", "", number_format($all_rachat['montant'] / 1000, 1, ",", ".")) . " M€";
                                                                } else
                                                                    echo "NC";
                                                                ?> Rachat</td>
                                                            <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($all_rachat['date_ajout']); ?></td>
                                                            <td class="">
                                                                <div class="labels">
                                                                    <?php
                                                                    $sql_list_invest = mysqli_query($link, "select * from startup_investisseur where id_lf=" . $all_rachat['id']);
                                                                    while ($mon_invest = mysqli_fetch_array($sql_list_invest)) {
                                                                        if ($mon_invest['id_investisseur'] != 0) {
                                                                            $name_invest = mysqli_fetch_array(mysqli_query($link, "select * from new_name_list where id=" . $mon_invest['id_investisseur']));
                                                                            ?>
                                                                            <a href="<?php echo URL; ?>/startups-investisseur/<?php generate_id($mon_invest['id_investisseur']) ?> '/' <?php urlWriting(strtolower($name_invest['new_name'])) ?> '" class="label"><?php echo $name_invest['new_name']; ?></a>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <span class="label"> <?php echo ($mon_invest['group_name']); ?></span>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }

                                                $ii = 1;
                                                foreach ($row_last_lf_all_startup as $all_lf) {
                                                    if ($ii == 1)
                                                        $lettre = "1";
                                                    if ($ii == 2)
                                                        $lettre = "2";
                                                    if ($ii == 3)
                                                        $lettre = "3";
                                                    if ($ii == 4)
                                                        $lettre = "4";
                                                    if ($ii == 5)
                                                        $lettre = "5";
                                                    if ($ii == 6)
                                                        $lettre = "6";
                                                    if ($ii == 7)
                                                        $lettre = "7";
                                                    if ($ii == 8)
                                                        $lettre = "8";
                                                    if ($ii == 9)
                                                        $lettre = "9";
                                                    ?>

                                                    <tr>
                                                        <td class="text-center moneyCell"><?php
                                                            if ($all_lf['montant'] != 0) {
                                                                echo str_replace(",0", "", number_format($all_lf['montant'] / 1000, 1, ",", ".")) . " M€";
                                                            } else
                                                                echo "NC";
                                                            ?></td>
                                                        <td class="text-center dateCell"><?php echo change_date_fr_chaine($all_lf['date_ajout']); ?></td>
                                                        <td class="">
                                                            <div class="labels">
                                                                <?php
                                                                $sql_list_invest = mysqli_query($link, "select * from startup_investisseur where id_lf=" . $all_lf['id']);
                                                                while ($mon_invest = mysqli_fetch_array($sql_list_invest)) {
                                                                    if ($mon_invest['id_investisseur'] != 0) {
                                                                        $name_invest = mysqli_fetch_array(mysqli_query($link, "select * from new_name_list where id=" . $mon_invest['id_investisseur']));
                                                                        ?>
                                                                        <a href="<?php echo URL; ?>/startups-investisseur/<?php echo generate_id($mon_invest['id_investisseur']) ?>/<?php echo urlWriting(strtolower($name_invest['new_name'])) ?>" class="label"><?php echo $name_invest['new_name']; ?></a>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <span class="label"> <?php echo ($mon_invest['group_name']); ?></span>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $ii++;
                                                }
                                                ?>
                                            </tbody>
                                            <?php
                                        }
                                        ?>
                                    </table>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </section>
                    <div id="bloc_finance"></div>
                    <section class="module">
                        <div class="module__title">Marché & secteur</div>
                        <div class="ctg ctg--1_2_1">
                            <div class="bloc text-center">
                                <!--<div class="bloc__title">Compétition secteur</div>-->
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Compétition secteur <a target="_blank" href="<?php echo URL ?>/etude-secteur/<?php echo generate_id($row_nom_secteur[0]['id']) . "/" . urlWriting($row_nom_secteur[0]['nom_secteur']) ?>" class="datasbloc__key__ico"><span class="ico-chart"></span></a></div>
                                    <div class="datasbloc__val"><?php echo str_replace(".", ",", $ligne_secteur_activite['competition_secteur']); ?> 
                                        <?php if ($ligne_secteur_secteur['indice_competition_secteur'] > $ligne_secteur_secteur['indice_competition_secteur_12']) { ?>
                                            <span class="ico-raise"></span>
                                        <?php } ?>
                                        <?php if ($ligne_secteur_secteur['indice_competition_secteur'] == $ligne_secteur_secteur['indice_competition_secteur_12']) { ?>
                                            <span class="ico-stable"></span>
                                        <?php } ?>
                                        <?php if ($ligne_secteur_secteur['indice_competition_secteur'] < $ligne_secteur_secteur['indice_competition_secteur_12']) { ?>
                                            <span class="ico-decrease"></span>
                                        <?php } ?>
                                    </div>
                                    <div class="datasbloc__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['indice_competition_secteur'], 1, ",", "")); ?> </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Création secteur</div>
                                    <div class="datasbloc__val"><?php echo str_replace(".", ",", $ligne_secteur_secteur['taux_creation']) . "%" ?></div>
                                    <div class="datasbloc__subtitle">Moy. France <?php echo str_replace(',0', '', number_format($ligne_france['taux_creation'], 1, ",", "")); ?></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Code Naf</div>
                                    <div class="datasbloc__val"><?php
                                        if ($startup[0]['naf'] != "")
                                            echo $startup[0]['naf'];
                                        else
                                            echo "-";
                                        ?></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Modèle</div>
                                    <div class="datasbloc__val"><?php
                                        if ($startup[0]['cible'] != "")
                                            echo $startup[0]['cible'];
                                        else
                                            echo "-";
                                        ?></div>
                                </div>
                            </div>
                            <div class="bloc content_maturite">
                                <div class="bloc__title">Maturité des startups du secteur</div>
                                <div class="chart chart--bubbles">
                                    <div id="chart-bubbles-startups"></div>
                                </div>
                                <script>
                                    am4core.ready(function () {


                                        var chart = am4core.create("chart-bubbles-startups", am4charts.XYChart);
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 15;
                                        chart.numberFormatter.numberFormat = "#.";

                                        var valueAxisX = chart.xAxes.push(new am4charts.ValueAxis());
                                        valueAxisX.renderer.ticks.template.disabled = true;
                                        valueAxisX.renderer.axisFills.template.disabled = true;
                                        valueAxisX.renderer.fontSize = 11;
                                        valueAxisX.renderer.color = labelColor;
                                        valueAxisX.renderer.labels.template.fill = labelColor;

                                        // Title left
                                        valueAxisX.title.text = "Années";
                                        valueAxisX.title.align = "right";

                                        //valueAxisX.title.valign = "top";
                                        valueAxisX.title.dy = 0;
                                        valueAxisX.title.fontSize = 11;
                                        valueAxisX.title.fill = labelColor;

                                        var valueAxisY = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxisY.renderer.ticks.template.disabled = true;
                                        valueAxisY.renderer.axisFills.template.disabled = true;

                                        // Title left
                                        valueAxisY.title.text = "Score de maturité";
                                        valueAxisY.title.rotation = -90;
                                        valueAxisY.title.align = "left";
                                        valueAxisY.title.valign = "top";
                                        valueAxisY.title.dy = 0;
                                        valueAxisY.title.fontSize = 11;
                                        valueAxisY.title.fill = labelColor;
                                        valueAxisY.renderer.fontSize = 11;
                                        valueAxisY.renderer.color = labelColor;
                                        valueAxisY.renderer.labels.template.fill = labelColor;

                                        var series = chart.series.push(new am4charts.LineSeries());
                                        series.dataFields.valueX = "x";
                                        series.dataFields.valueY = "y";
                                        series.dataFields.value = "value";
                                        series.strokeOpacity = 0;
                                        series.sequencedInterpolation = true;

                                        series.tooltip.pointerOrientation = "vertical";
                                        series.tooltip.getFillFromObject = false;
                                        series.tooltip.background.fill = am4core.color("#ffffff");
                                        series.tooltip.background.stroke = themeColorLightGrey;
                                        series.tooltip.label.fontSize = 13;
                                        series.tooltip.label.fill = themeColorDarkgrey;


                                        var bullet = series.bullets.push(new am4core.Circle());
                                        bullet.fill = themeColorLightGrey;
                                        bullet.propertyFields.fill = "color";
                                        bullet.strokeOpacity = 0;
                                        bullet.strokeWidth = 2;
                                        bullet.fillOpacity = 0.5;
                                        bullet.stroke = am4core.color("#ffffff");
                                        bullet.hiddenState.properties.opacity = 0;
                                        bullet.tooltipText = "[bold]{title}[/]\nAnnée: {valueX.value}\nMaturité : {valueY.value}";

                                        var shadow = series.tooltip.background.filters.getIndex(0);
                                        shadow.dx = 0;
                                        shadow.dy = 0;
                                        shadow.blur = 0;



                                        var outline = chart.plotContainer.createChild(am4core.Circle);
                                        outline.fillOpacity = 0;
                                        outline.strokeOpacity = 0.8;
                                        outline.stroke = themeColorBlue;
                                        outline.strokeWidth = 2;
                                        outline.hide(0);

                                        var blurFilter = new am4core.BlurFilter();
                                        outline.filters.push(blurFilter);
                                        bullet.events.on("over", function (event) {
                                            var target = event.target;
                                            outline.radius = target.pixelRadius + 2;
                                            outline.x = target.pixelX;
                                            outline.y = target.pixelY;
                                            outline.show();
                                        })

                                        bullet.events.on("out", function (event) {
                                            outline.hide();
                                        })

                                        var hoverState = bullet.states.create("hover");
                                        hoverState.properties.fillOpacity = 1;
                                        hoverState.properties.strokeOpacity = 1;
                                        series.heatRules.push({target: bullet, min: 2, max: 10, property: "radius"});
                                        bullet.adapter.add("tooltipY", function (tooltipY, target) {
                                            return -target.radius;
                                        })

                                        chart.data = [
<?php
$sql_startup_ssecteur = mysqli_query($link, "select startup.rang_effectif,startup.intervalle_effectif,startup_score.tours,startup_score.id_startup,startup_score.maturite,startup.creation,startup.nom from startup_score inner join activite on startup_score.id_startup=activite.id_startup inner join startup on startup.id=startup_score.id_startup where startup.status=1 and startup.creation!='' and startup.creation!=0 and startup.intervalle_effectif!=''  and creation>2008 and activite.secteur=" . $id_secteur['id'] . " and activite.sous_secteur=" . $idsoussecteur . " and activite.activite=" . $idactivite . " and activite.sous_activite=" . $idsousactivite)or die(mysqli_error($link));

while ($mat_sql = mysqli_fetch_array($sql_startup_ssecteur)) {
    ?>
                                                {
                                                    "title": "<?php echo addslashes(($mat_sql['nom'])); ?>",
                                                    "color": <?php
    if ($mat_sql['id_startup'] != $startup[0]['id'])
        echo 'themeColorGrey';
    else
        echo 'themeColorBlue';
    ?>,
                                                    "x": "<?php echo $mat_sql['creation']; ?>",
                                                    "y": <?php echo $mat_sql['maturite']; ?>,
                                                    "value": 60
                                                },
    <?php
}
?>

                                        ];
                                    });
                                </script>
                            </div>
                            <div id="bloc_maturite"></div>

                            <div class="bloc">
                                <div class="bloc__title">Startups à suivre</div>
                                <div class="tablebloc">
                                    <table class="table table--condensed">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th>Création</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $row_startup_autre = mysqli_query($link, "select * from startup_similar where id_startup=" . $startup[0]['id'] . " order by score limit 10");
                                            while ($autre_data = mysqli_fetch_array($row_startup_autre)) {
                                                $autre_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,date_complete from startup where id=" . $autre_data["id_similar"]));
                                                $cch = "";
                                                $tabs = $autre_startup['nom'];
                                                $nbe = count($tabs);
                                                for ($j = 0; $j < 15; $j++) {
                                                    $cch = $cch . $tabs[$j];
                                                }
                                                if ($nbe > 15) {
                                                    $cch = $cch . "...";
                                                }
                                                ?>
                                                <tr>
                                                    <td>
                                                        <a href="<?php echo URL . '/fr/startup-france/' . generate_id($autre_startup['id']) . "/" . urlWriting(strtolower($autre_startup["nom"])) ?>" title="" class="companyNameCell"><span><?php echo ($cch); ?></span></a>
                                                    </td>
                                                    <td><?php
                                                        if ($autre_startup['date_complete'] != "")
                                                            echo change_date_fr_chaine_relateds($autre_startup['date_complete']);
                                                        else
                                                            echo "NC";
                                                        ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module module--fondateurs">
                        <div class="module__title">Fondateurs & team</div>

                        <div class="ctg ctg--3_1">
                            <div class="bloc" id="bloc_autre_founder">
                                <div class="bloc__title">Fondateurs</div>
                                <div class="tablebloc tablebloc--light">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Fondateur</th>
                                                <th class="text-center">Âge</th>
                                                <th class="text-center">École</th>
                                                <th class="text-center">Diplôme</th>
                                                <th class="text-center" width="40%">Compétences</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $ligne_personne = mysqli_fetch_array(mysqli_query($link, "select * from personnes where id_startup=" . $startup[0]['id'] . " limit 1"));

                                            $cch_skils = "";
                                            $lign_exp = mysqli_fetch_array(mysqli_query($link, "select * from personnes_phantombuster where id_personnes=" . $ligne_personne['id']));
                                            $fonction_ligne = mysqli_fetch_array(mysqli_query($link, "select * from fonction where id=" . $ligne_personne["fonction"]));
                                            $ecole_ligne = mysqli_fetch_array(mysqli_query($link, "select * from ecole_personnes where idpersonne=" . $ligne_personne["id"] . " order by a desc limit 1"));
                                            $list_competances = mysqli_query($link, "select * from personnes_competence where id_personne=" . $ligne_personne["id"] . " limit 4");
                                            $ch_skils = explode(',', $lign_exp['allSkills']);
                                            $nb_comm = count($ch_skils);
                                            if ($nb_comm > 0) {
                                                for ($i = 0; $i < 5; $i++) {
                                                    if ($ch_skils[$i] != '') {
                                                        $cch_skils .= ($ch_skils[$i]) . ", ";
                                                    }
                                                }
                                            } else {
                                                $cch_skils = "-";
                                            }
                                            ?>
                                            <tr>
                                                <td class="text-center">
                                                    <div class="avatar">
                                                        <i class="ico-avatar"></i>
                                                    </div>
                                                    <strong><?php echo stripslashes(($ligne_personne['prenom'] . " " . $ligne_personne['nom'])); ?></strong>
                                                    <div class="fonction"><?php echo stripslashes($fonction_ligne['nom_fr']); ?></div>
                                                </td>
                                                <td class="text-center"><?php
                                                    if ($ligne_personne['age'] != "")
                                                        echo $ligne_personne['age'];
                                                    else
                                                        echo "-";
                                                    ?></td>
                                                <td class="text-center"><?php
                                                    if ($lign_exp['schoolName_3'] != "") {
                                                        echo ($lign_exp['schoolName_3']);
                                                    } else if ($lign_exp['schoolName_2'] != "") {
                                                        echo ($lign_exp['schoolName_2']);
                                                    } else if ($lign_exp['schoolName_1'] != "") {
                                                        echo ($lign_exp['schoolName_1']);
                                                    } else {
                                                        echo "-";
                                                    }
                                                    ?></td>
                                                <td class="text-center"><?php
                                                    if ($lign_exp['degree_3'] != "") {
                                                        echo ($lign_exp['degree_3']);
                                                    } else if ($lign_exp['degree_2'] != "") {
                                                        echo ($lign_exp['degree_2']);
                                                    } else if ($lign_exp['degree_1'] != "") {
                                                        echo ($lign_exp['degree_1']);
                                                    } else {
                                                        echo "-";
                                                    }
                                                    ?></td>
                                                <td class="text-center"><?php echo $cch_skils; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="historyList">
                                    <?php
                                    if ($lign_exp['jobTitle_1'] != "") {
                                        ?>
                                        <div class="historyList__item">
                                            <div class="historyList__item__date"><?php echo ($lign_exp['dateRange_job_1']) ?></div>
                                            <div class="historyList__item__infos"><?php echo ($lign_exp['jobTitle_1']) ?> chez <?php echo ($lign_exp['companyName_1']) ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                    if ($lign_exp['jobTitle_2'] != "") {
                                        ?>
                                        <div class="historyList__item">
                                            <div class="historyList__item__date"><?php echo ($lign_exp['dateRange_job_2']) ?></div>
                                            <div class="historyList__item__infos"><?php echo ($lign_exp['jobTitle_2']) ?> chez <?php echo ($lign_exp['companyName_2']) ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                    if ($lign_exp['jobTitle_3'] != "") {
                                        ?>
                                        <div class="historyList__item">
                                            <div class="historyList__item__date"><?php echo ($lign_exp['dateRange_job_3']) ?></div>
                                            <div class="historyList__item__infos"><?php echo ($lign_exp['jobTitle_3']) ?> chez <?php echo ($lign_exp['companyName_3']) ?></div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Fondateurs & Team</div>
                                <div class="bloc-team">
                                    <?php
                                    $row_total_fondateur = getListFondateurByIdStartup($startup[0]["id"]);
                                    foreach ($row_total_fondateur as $personne) {
                                        $fonction = mysqli_fetch_array(mysqli_query($link, "select * from fonction where id=" . $personne["fonction"]));
                                        ?>
                                        <div style="cursor: pointer" class="bloc-team__people" onclick="getinfofounder(<?php echo $personne["id"]; ?>,<?php echo $startup[0]['id']; ?>)">
                                            <div class="bloc-team__people__avatar">
                                                <div class="avatar">
                                                    <i class="ico-avatar-white"></i>
                                                </div>
                                            </div>
                                            <div class="bloc-team__people__name">
                                                <?php echo stripslashes(($personne['prenom'] . " " . $personne['nom'])); ?>
                                                <span class="fonction"><?php echo stripslashes($fonction['nom_fr']); ?></span>
                                            </div>
                                            <div class="bloc-team__people__link">
                                                <?php if ($personne['linkedin'] != "") { ?>
                                                    <a href="https://<?php echo str_replace(array("https://"), array(""), $personne['linkedin']) ?>" title=""><i class="ico-linkedin"></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module module--fondateurs">
                        <div class="module__title">Localisation</div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc jc text-center">
                                <div class="datasbloc">
                                    <div class="datasbloc__val">
                                        <div class="tags">
                                            <a target="_blank" href="<?php echo URL . '/region/' . generate_id($get_id_region['id']) . "/" . urlWriting(strtolower($get_id_region['region_new'])) ?>" title="" class="tags__el tags__el--pinky">
                                                <span class="ico-chart"></span>  <?php echo ($startup[0]['region_new']) ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Adresse</div>
                                    <div class="datasbloc__val small">
                                        <?php if ($startup[0]['adresse'] != '') { ?>
                                            <?php echo ($startup[0]['adresse']) . ", " . $startup[0]['cp'] . " " . ($startup[0]['ville']); ?>
                                        <?php } else { ?>
                                            -
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Tél</div>
                                    <div class="datasbloc__val small">
                                        <?php
                                        if (isset($_SESSION['data_login'])) {
                                            if ($startup[0]['tel'] != "")
                                                echo $startup[0]['tel'];
                                            else
                                                echo "-";
                                        }else {
                                            ?>
                                            <a href="" class="link-not-connected">Connectez-vous</a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Website</div>
                                    <div class="datasbloc__val small">
                                        <a href="<?php echo $startup[0]['url'] ?>" target="_blank"><?php echo $startup[0]['url'] ?></a>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Email</div>
                                    <div class="datasbloc__val small">
                                        <?php
                                        if (isset($_SESSION['data_login'])) {
                                            echo $startup[0]['email'];
                                        } else {
                                            ?>
                                            <a href="" class="link-not-connected">Connectez-vous</a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Réseaux sociaux</div>
                                    <div class="datasbloc__val small">
                                        <ul class="list list--socialnetworks">
                                            <?php if ($startup[0]['linkedin'] != "") { ?> <li><a href="<?php echo $startup[0]['linkedin']; ?>" title="LinkedIn"><span class="ico-linkedin"></span></a></li><?php } ?>
                                            <?php if ($startup[0]['facebook'] != "") { ?>  <li><a href="<?php echo $startup[0]['facebook']; ?>" title="Facebook"><span class="ico-facebook"></span></a></li><?php } ?>
                                            <?php if ($startup[0]['twitter'] != "") { ?> <li><a href="<?php echo $startup[0]['twitter']; ?>" title="Twitter"><span class="ico-twitter"></span></a></li><?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Startups du secteur par département <a href="">Voir la liste &raquo;</a></div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th class="iconCell">Évol.</th>
                                                <th>Départements</th>
                                                <th class="text-center">Startups</th>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <?php
                                            $date1 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d') - 1, date('y')));
                                            $date2 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d') - 30, date('y')));

                                            $sql_dep = mysqli_query($link, "select count(*) as nb, startup.departement from startup inner join activite on activite.id_startup=startup.id where startup.status=1 and startup.departement!='' and activite.secteur='" . $row_nom_secteur[0]['id'] . "' and  startup.region_new='" . (addslashes($startup[0]['region_new']) ) . "'  group by startup.departement order by nb desc limit 10");

                                            while ($data_dep = mysqli_fetch_array($sql_dep)) {
                                                $sql_dep_tot = mysqli_num_rows(mysqli_query($link, "select startup.id from startup inner join activite on activite.id_startup=startup.id where startup.status=1 and startup.departement!='' and activite.secteur=" . $row_nom_secteur[0]['id'] . "  and  startup.region_new='" . addslashes($startup[0]['region_new']) . "' and departement='" . (addslashes($data_dep['departement'])) . "'"));
                                                $sql_dep_tot2 = mysqli_num_rows(mysqli_query($link, "select startup.id from startup inner join activite on activite.id_startup=startup.id where startup.status=1 and startup.departement!='' and activite.secteur=" . $row_nom_secteur[0]['id'] . "  and  startup.region_new='" . addslashes($startup[0]['region_new']) . "' and departement='" . (addslashes($data_dep['departement'])) . "' and date(startup.date_add)>'" . $date1 . "'"));
                                                $sql_dep2 = mysqli_fetch_array(mysqli_query($link, "select count(*) as nb from startup inner join activite on activite.id_startup=startup.id where startup.status=1 and departement='" . (addslashes($data_dep['departement'])) . "' and activite.secteur=" . $row_nom_secteur[0]['id'] . " and date(startup.date_add)>'" . $date2 . "'  group by startup.departement"));
                                                $nom_dep = mysqli_fetch_array(mysqli_query($link, "select * from departement where code='" . (addslashes($data_dep['departement'])) . "'"));


                                                if ($sql_dep_tot2 != 0) {
                                                    $diff1 = $sql_dep_tot2 - $sql_dep2['nb'];
                                                    $pourcentage1 = ($diff1 * 100) / $sql_dep_tot2;
                                                } else {
                                                    $diff1 = 0;
                                                    $pourcentage1 = 0;
                                                }
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        if ($diff1 < 0) {
                                                            ?>
                                                            <span class="ico-decrease"></span>
                                                        <?php } ?>
                                                        <?php
                                                        if ($diff1 > 0) {
                                                            ?>
                                                            <span class="ico-raise"></span>
                                                        <?php } ?>
                                                        <?php
                                                        if ($diff1 == 0) {
                                                            ?>
                                                            <span class="ico-stable"></span>
                                                        <?php } ?>
                                                    </td>
                                                    <td><?php
                                                        if ($nom_dep['nom'] != '')
                                                            echo ($nom_dep['nom']);
                                                        else
                                                            echo "NC";
                                                        ?></td>
                                                    <td class="text-center"><?php echo $sql_dep_tot; ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module module--legals">
                        <div class="module__title">Legals & tech stack</div>
                        <div class="ctg ctg--1_3">
                            <div class="bloc text-center">
                                <div class="bloc__title text-center">Légales</div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Raison sociale</div>
                                    <div class="datasbloc__val small"><?php
                                        if ($startup[0]['societe'] != "")
                                            echo ($startup[0]['societe']);
                                        else
                                            echo "-";
                                        ?></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Siret</div>
                                    <div class="datasbloc__val small"><?php
                                        if ($startup[0]['siret'] != "")
                                            echo $startup[0]['siret'];
                                        else
                                            echo "-";
                                        ?></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Forme juridique</div>
                                    <div class="datasbloc__val small"><?php
                                        if ($startup[0]['juridique'] != "")
                                            echo ($startup[0]['juridique']);
                                        else
                                            echo "-";
                                        ?></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Code Naf</div>
                                    <div class="datasbloc__val small"><?php
                                        if ($startup[0]['naf'] != "")
                                            echo $startup[0]['naf'];
                                        else
                                            echo "-";
                                        ?></div>
                                </div>
                                <?php
                                $dernier = mysqli_fetch_array(mysqli_query($link, "select * from startup_depot where id_startup=" . $startup[0]['id'] . " order by dt desc limit 1"));
                                ?>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Dernier mouvement Bodacc</div>
                                    <div class="datasbloc__val small"> <?php if ($dernier['depot'] != "") { ?>
                                            <?php echo ($dernier['depot']); ?><br><?php echo change_date_fr_chaine_related($dernier['dt']); ?>
                                        <?php } else echo "-"; ?></div>
                                </div>
                            </div>

                            <div class="bloc text-center">
                                <div class="bloc__title text-center">Tech Stack</div>
                                <?php
                                $nb_stack = mysqli_query($link, "select * from startup_builtwith where id_startup=" . $startup[0]['id']);
                                $startup_builtwith = mysqli_fetch_array($nb_stack);
                                ?>

                                <?php
                                if ($startup_builtwith['Analytics_and_Tracking_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Analytics and Tracking</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Analytics_and_Tracking_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Widgets_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Widgets</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Widgets_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['eCommerce_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">eCommerce</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['eCommerce_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Frameworks_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Frameworks</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Frameworks_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Content_Delivery_Network_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Content Delivery Network</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Content_Delivery_Network_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Mobile_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Mobile</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Mobile_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Payment_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Payment</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Payment_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Audio_Video_Media_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Audio Video Media</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Audio_Video_Media_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Content_Management_System_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Content Management System</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Content_Management_System_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['JavaScript_Libraries_and_Functions_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">JavaScript Libraries and Functions</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['JavaScript_Libraries_and_Functions_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Advertising_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Advertising</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Advertising_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Verified_Link_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Verified Link</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Verified_Link_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Shipping_Providers_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Shipping Providers</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Shipping_Providers_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Email_Hosting_Providers_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Email Hosting Providers</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Email_Hosting_Providers_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Name_Servers_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Name Servers</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Name_Servers_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Web_Hosting_Providers_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Web Hosting Providers</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Web_Hosting_Providers_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['SSL_Certificates_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">SSL Certificates</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['SSL_Certificates_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Operating_Systems_and_Servers_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Operating Systems and Servers</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Operating_Systems_and_Servers_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Web_Servers_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Web Servers</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Web_Servers_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Verified_CDN_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Verified CDN</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Verified_CDN_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Web_Master_Registration_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Web Master Registration</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Web_Master_Registration_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Domain_Parking_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Domain Parking</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Domain_Parking_items'] ?></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>

        <script src="https://assets.poool.fr/audit.min.js"></script>
        <script src="https://assets.poool.fr/access.min.js"></script>
        <script>
                                        window.addEventListener('load', () => {
                                            Audit
                                                    .init('UB0IB-OS245-ZNO3H-EH4IZ')
                                                    .config({cookies_enabled: true})
                                                    .sendEvent('page-view', {type: 'premium'});
                                            const access = Access.init('UB0IB-OS245-ZNO3H-EH4IZ');
                                            access.config({
                                                cookies_enabled: true,
                                            })
                                                    .on('release', function () {
                                                        access.destroy();
                                                        document.querySelector('#bloc_maturite').remove();
                                                        document.querySelector('#bloc_radar').remove();
                                                    })
                                                    .createPaywall({
                                                        target: '#bloc_maturite',
                                                        content: '.content_maturite',
                                                        mode: 'custom',
                                                        pageType: 'premium',
                                                    })

                                            Access
                                                    .init('UB0IB-OS245-ZNO3H-EH4IZ')
                                                    .config({
                                                        context: 'radar',
                                                        cookies_enabled: true,
                                                    })
                                                    .createPaywall({
                                                        target: '#bloc_radar',
                                                        content: '.content_radar',
                                                        mode: 'custom',
                                                        pageType: 'premium',
                                                    })
                                            Access
                                                    .init('UB0IB-OS245-ZNO3H-EH4IZ')
                                                    .config({
                                                        context: 'maturite',
                                                        cookies_enabled: true,
                                                    })
                                                    .createPaywall({
                                                        target: '#bloc_maturite',
                                                        content: '.content_maturite',
                                                        mode: 'custom',
                                                        pageType: 'premium',
                                                    })

                                        }
                                        )

        </script>



        <script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>
            function getinfofounder(id, startup) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL ?>/bloc_founder.php?id=" + id + "&startup=" + startup,
                    success: function (o) {
                        document.getElementById("bloc_autre_founder").innerHTML = o;
                    }

                }
                );
            }
            function revendiquer() {
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL ?>/revendiquer_sup.php",
                    success: function (o) {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo URL ?>/revendique_email.php",
                            success: function (o) {
                                document.getElementById("grid").innerHTML = o;
                            }

                        }
                        );
                    }

                }
                );
            }


            $(document).ready(function () {
                $("#voir_plus").click(function () {
                    $("#long_fr_bloc2").show();
                    $("#voir_plus").hide();
                });
                $("#voir_moins").click(function () {
                    $("#long_fr_bloc2").hide();
                    $("#voir_plus").show();
                });
            });


        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>