<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (isset($_POST['email_news'])) {
    $email = addslashes($_POST['email_news']);
    $verif = mysqli_num_rows(mysqli_query($link, "select * from newsletter where email='" . $email . "'"));
    if ($verif == 0) {
        mysqli_query($link, "insert into newsletter(email,dt,ip)values('" . $email . "','" . date('Y-m-d H:i:s') . "','" . $_SERVER['REMOTE_ADDR'] . "')");
    }
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Inscription Newsletter</title>
        <meta name="description" content="<?= METADESC; ?>">
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>

        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">            
                    Vous êtes désormais inscrit à notre newsletter. Vous pouvez dès maintenant être notifier des offres et des news myFrenchstartup.<br>

                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');

            function checkEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            function connexion() {
                document.getElementById("msg_error_mail").style.display = "none";
                document.getElementById("msg_error_user").style.display = "none";
                document.getElementById("msg_error_user_block").style.display = "none";
                var login = document.getElementById("login_connect").value;
                var pwd = document.getElementById("pwd").value;
                if (checkEmail(login)) {

                    $.ajax({
                        type: "POST",
                        url: "<?php echo URL ?>/verif_user.php?login=" + login + "&pwd=" + pwd,

                        success: function (data) {
                            var t = eval(data);
                            if (t[0].result == 0) {
                                document.getElementById("msg_error_user").style.display = "block";
                            } else {
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo URL ?>/verif_user_block.php?login=" + login + "&pwd=" + pwd,

                                    success: function (data1) {
                                        var t = eval(data1);
                                        if (t[0].result == 0) {
                                            document.getElementById("msg_error_user_block").style.display = "block";
                                        } else {
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo URL ?>/redirection.php?login=" + login + "&pwd=" + pwd,

                                                success: function (data2) {

                                                    window.location = data2;


                                                }
                                            });
                                        }


                                    }
                                });
                            }


                        }
                    });






                } else {
                    document.getElementById("msg_error_mail").style.display = "block";
                }

            }
        </script>
    </body>
</html>