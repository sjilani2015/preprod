/**
 * Check Devices
 * @returns {string|boolean}
 */
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (
            isMobile.Android() ||
            isMobile.BlackBerry() ||
            isMobile.iOS() ||
            isMobile.Opera() ||
            isMobile.Windows()
        );
    }
};

// Variables
let menuPath = "https://www.myfrenchstartup.com/navigation/mainmenu.php";

/**
 Toggle Main Menu for mobile
 **/
let toggleMainMenu  = document.getElementById('toggleMainMenu'),
    mainmenu      = document.querySelector('.mainmenu__wrapper'),
    menuLaunched    = document.querySelector('.mainmenu__shutter'),
    mainHeader   = document.getElementById('header'), 
    mainHeaderW  = document.getElementById('header-wrapper');



function showMenu() {
    document.body.classList.add('has-mainmenu');
    toggleMainMenu.classList.add('opened');
}

function hideMenu() {
    document.body.classList.remove("has-mainmenu"); 
    toggleMainMenu.classList.remove("opened");
}


function initAjaxMenu() {

    var xhr = new XMLHttpRequest();

    xhr.onload = function () {

        if (this.status >= 200 && this.status < 300 || this.status === 304) {

            showMenu();

            mainmenu.innerHTML = xhr.responseText;


            //I'm using "click" but it works with any event
            document.addEventListener('click', function(event) {
                var isClickInside = mainmenu.contains(event.target);

                if (!isClickInside) {
                    hideMenu();
                }
            });
        }
    };

    xhr.open("GET", menuPath, true);
    xhr.send('null');
}

if (toggleMainMenu !== null) {
    toggleMainMenu.addEventListener('click', function () {
        hideHeaderSearchbar();
        
        if(document.body.classList.contains('has-mainmenu')) {
            hideMenu();
            return false;
        }
        
        if (menuLaunched) {
            showMenu();
        } else {
            initAjaxMenu();
        }
    });
}


function initHeader() {
    // const
    const upScroll   = 1;
    const downScroll = 2;
    
    // let
    let curScroll;
    let prevDir     = 0;
    let direction   = 0;
    let prevScroll  = window.scrollY || window.pageYOffset || document.scrollTop;

    const checkScroll = () => {
        // Define current direction
        curScroll = window.scrollY || window.pageYOffset || document.scrollTop;
        direction = (curScroll > prevScroll) ? downScroll : upScroll;
        
        // Changing direction
        if (direction !== prevDir) {
            toggleHeader(direction, curScroll);
        } else {
            if (window.pageYOffset < 1) {
                mainHeader.classList.add('switching');
                setTimeout(function () {
                    mainHeader.classList.remove("pinned");
                    mainHeader.classList.remove('switching');
                }, 100);
            }
        }
        
        prevScroll = curScroll;
    };

    const toggleHeader = (direction, curScroll) => {
        // Get headers height
        const mainHeaderH   = mainHeader ? mainHeaderW.offsetHeight : 0;

        
        
        // Manage headers display
        if (direction === downScroll && curScroll > mainHeaderH) {
            if (mainHeader) {
                mainHeader.classList.add('switching');
                setTimeout(function () {
                    mainHeader.classList.remove("pinned");
                    mainHeader.classList.remove('switching');
                }, 100);
            }
            prevDir = direction;
        }
        
        else if (direction === upScroll) {
            if (mainHeader) {
                mainHeader.classList.add('switching');
                setTimeout(function () {
                    mainHeader.classList.add("pinned");
                    mainHeader.classList.remove('switching');
                }, 100);
            }
            prevDir = direction;
        }
    };

    window.addEventListener('scroll', checkScroll);
}
initHeader();




/** Preloader **/
window.onload = function(e){
    document.body.classList.remove ('preload');
}


/**
 * Dropdowns
 * @type {NodeListOf<Element>}
 */
const dropdowns = document.querySelectorAll("[data-dropdown]");
function initDropDowns() {
    dropdowns && dropdowns.forEach(e => {
        let t = e.getAttribute("data-dropdown"),
            n = document.getElementById(t);
        e.addEventListener("click", e => {
            n.style.display = "block"
        }), document.addEventListener("click", function(t) {
            e.contains(t.target) || (n.style.display = "none")
        })
    })
}

initDropDowns();


/*
    Searchbar for header
*/

let toggleSearchbar = document.getElementById('toggleSearchbar');

toggleSearchbar.addEventListener('click', function() {
    showHeaderSearchbar();

    //I'm using "click" but it works with any event
    document.addEventListener('click', function(event) {
        var isClickInside = document.querySelector('.header').contains(event.target);

        if (!isClickInside) {
            hideHeaderSearchbar();
        }
    });
});

function showHeaderSearchbar() {
    document.querySelector('.header').classList.toggle('isSearch');
    document.querySelector('.header .searchbar .form-control').focus();
}

function hideHeaderSearchbar() {
    document.querySelector('.header').classList.remove('isSearch');
}



/** See more for intro **/
let seemorebtn = document.querySelector('.intro-seeMore'),
    seelessbtn = document.querySelector('.intro-seeLess'),
    introShort = document.querySelector('.intro-short'), 
    introLong = document.querySelector('.intro-long');

if (seemorebtn !== null) {
    seemorebtn.addEventListener('click', function() {
        introShort.style.display = "none";
        introLong.style.display = "block";
    });
}

if (seelessbtn !== null) {
    seelessbtn.addEventListener('click', function() {
        introShort.style.display = "block";
        introLong.style.display = "none";
    });
}


/**
 * Show tab content
 * @param id
 */
function showFilterTab(id) {
    let filterTabId = document.getElementById('filterTab-' + id);
    
    let isActive = filterTabId.classList.contains('active');
    if (!isActive) {
        let tabs = document.querySelectorAll('.filters-right__tab');
        if (tabs !== null) {
            tabs.forEach(e => {
                e.classList.remove("active");
            });
        }
        filterTabId.classList.add('active');
    }
}


/**
 * Open filters for search modal
 */
function openSearchFilters() {
    let searchFiltersBtn = document.querySelector('.btn-searchfilters'),
        modalSearchFilters = document.getElementById('modal-searchfilters');

    if (modalSearchFilters !== null && searchFiltersBtn !== null) {
        searchFiltersBtn.addEventListener('click', function () {
            document.body.classList.add("has-door");
            modalSearchFilters.classList.add("shown");
        });

        var tablinks = document.querySelectorAll('.nav-vertical__list a');
        if (tablinks !== null) {
            tablinks.forEach(e => {
                e.addEventListener('click', function (e) {
                    let tab = parseInt(this.getAttribute("data-target-tab"));
                    e.preventDefault();
                    showFilterTab(tab);

                    let isActive = this.parentNode.classList.contains('active');
                    if (!isActive) {
                        tablinks.forEach(e => {
                            e.parentNode.classList.remove('active');
                        });
                        this.parentNode.classList.add('active');
                    }
                })
            });
        }
    }

    /**
     * Open filters for column modal
     */
    let searchColumnsBtn = document.querySelector('.btn-searchcolumns'),
        modalSearchColumns = document.getElementById('modal-columns');

    if (modalSearchColumns !== null && searchColumnsBtn !== null) {
        searchColumnsBtn.addEventListener('click', function () {
            document.body.classList.add ("has-door");
            modalSearchColumns.classList.add ("shown");
        });
    }

    /**
     * Open searches for save search modal
     */
    let searchSaveBtn = document.querySelectorAll('.btn-saveSearch'),
        modalSaveSearch = document.getElementById('modal-saveSearch');

    if (modalSaveSearch !== null || searchSaveBtn !== null) {
        searchSaveBtn.forEach(e => {

            e.addEventListener('click', function (e) {
                e.preventDefault();
                document.body.classList.add("has-door");
                modalSaveSearch.classList.add("shown");
            });
        });
    }
}



/**
 * Close btn Modal
*/
var closeBtnModal = document.querySelectorAll('.modal__closeBtn');
if (closeBtnModal !== null) {
    closeBtnModal.forEach(e => {
        e.addEventListener('click', function(e) {
            e.preventDefault();
            closeModal();
        })
    });
}

function closeModal() {
    var modals = document.querySelectorAll('.modal');
    document.body.classList.remove ('has-door');

    // Close all modals
    if (modals !== null) {
        modals.forEach(e => {
            e.classList.remove ("shown");
        });
    }
}

/**
 * Sidebar menu on search modal
*/
const sidebar = document.querySelector('#modal-searchfilters .nav-vertical__list ul');
if (sidebar) {
    sidebar.addEventListener('click', (e) => {
        e.preventDefault();
        sidebar.classList.toggle('opened');
    });
}

/**
 * Signin
 * 
 */
let loginTabLink = document.querySelector('.loginTab'),
    signinTab = document.querySelector('.signin__login'),
    registerTab = document.querySelector('.signin__register'),
    registerTabLink = document.querySelector('.registerTab');

if (loginTabLink) {
    loginTabLink.addEventListener('click', function() {
        registerTabLink.classList.remove('active');
        this.classList.add('active');
        signinTab.classList.add('shown');
        registerTab.classList.remove('shown');
    });
}

if (registerTabLink) {
    registerTabLink.addEventListener('click', function() {
        loginTabLink.classList.remove('active');
        this.classList.add('active');
        signinTab.classList.remove('shown');
        registerTab.classList.add('shown');
    });
}

/** See more for account register **/
let seemoreRegisterbtn = document.querySelector('.btn-seemoreRegister'),
    registerLegals = document.querySelector('.register-mentions .hide');

if (seemoreRegisterbtn !== null) {
    seemoreRegisterbtn.addEventListener('click', function() {
        registerLegals.classList.remove('hide');
        this.style.display = "none";
    });
}


/* Remove all alerts */
function removeAlerts() {
    let alerts = document.querySelectorAll('.alert');
    
    if (alerts !== null) {
        alerts.forEach(e => {
            e.remove();
        });
    }
}

/**
 * Btn Alert
**/
let searchAlert = document.querySelector('.btn-searchAlert');
if (searchAlert !== null) {
    searchAlert.addEventListener('click', function(e) {
        e.preventDefault();
        removeAlerts();
        
        let alertContainer = document.createElement('div'),
            searchHeader = document.querySelector('.searchHeader');
        
        
        alertContainer.classList.add ('alert');
        alertContainer.classList.add ('alert--error');
        //alertContainer.classList.add ('alert--success'); // cas de succès
        
        alertContainer.innerHTML = 'Vous devez <a href="/signin.php">créer un compte</a> pour pouvoir créer une alerte';
        searchHeader.parentNode.insertBefore(alertContainer, searchHeader);
    });
}
