<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (isset($_SESSION['data_login'])) {
    $users = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));
}


$sql100 = mysqli_query($link, "select * from new_name_list");
$sql1000 = mysqli_num_rows($sql100);

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}
?>
<div class="searchHeader">
    <div class="searchHeader__title">10 investisseurs affichés / <?php echo $sql1000 ?> investisseurs selectionnés</div>

    <div class="searchHeader__actions">
        <button class="btn btn-sm btn-bordered btn-searchfilters btn-filter">Filtrer</button>

        <a href="<?php echo URL ?>/liste-investisseurs" class="btn btn-sm btn-primary btn-reset">Réinitialiser</a>
    </div>
</div>


<div class="searchFilters">
    <div class="searchFilters__tags tags tags--left"></div>
    <div class="searchFilters__tableOpt"></div>
</div>
<div class="table-responsive">
    <table class="table table-search" id="mine3">
        <thead>
            <tr>
                <th class="table-search__startup">Investisseur</th>
                <th class="table-search__ranking text-center">Ranking France</th>
                <th class="table-search__participations text-center">Participations</th>
                <th class="table-search__yearInvests text-center">Investissements par an</th>
                <th class="table-search__ticketmoy text-center">Ticket moyen</th>
                <th class="table-search__secteursInvests text-center">Principaux secteurs</th>
                <th class="table-search__secteursInvests text-center">Principales régions</th>

            </tr>
        </thead>
        <tbody>
            <?php
            $_SESSION['ma_base_url'] = $monUrl;
            $_SESSION['ma_requete'] = "select * from new_name_list order by nbr_invest desc limit 10";
            $sql101 = mysqli_query($link, "select * from new_name_list order by nbr_invest desc limit 10");
            $row_invest_france = mysqli_fetch_array(mysqli_query($link, "select * from new_name_list_fr where id=1"));


            while ($sups = mysqli_fetch_array($sql101)) {


                $mains = "";
                $mains_reg="";
                $principaux_secteurs = explode(',', $sups['principaux_secteurs']);
                for ($i = 0; $i < 2; $i++) {
                    $secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur where id=" . $principaux_secteurs[$i]));
                    $mains .= "<a href='" . URL . "/etude-secteur/" . generate_id($secteur['id']) . "/" . urlWriting(strtolower($secteur['nom_secteur'])) . "' style='color:#425466' target='_blank'>" . $secteur['nom_secteur'] . "</a> <br>";
                }
                $principales_regions = explode(',', $sups['principales_regions']);
                for ($j = 0; $j < 2; $j++) {
                    $regs = mysqli_fetch_array(mysqli_query($link, "select * from region_new where id=" . $principales_regions[$j]));
                    $mains_reg .= "<a href='" . URL . "/region/" . generate_id($regs['id']) . "/" . urlWriting(strtolower($regs['region_new'])) . "' style='color:#425466' target='_blank'>" . $regs['region_new'] . "</a> <br>";
                }
                ?>


                <tr>
                    <th class="table-search__startup">
                        <div class="startupInfos">
                            <div class="startupInfos__desc">
                                <div class="companyName"><a href="<?php echo URL . '/startups-investisseur/' . generate_id($sups['id']) . "/" . urlWriting(strtolower($sups["new_name"])) ?>"  target="_blank"><?php echo (stripslashes(strtoupper($sups['new_name']))); ?></a></div>
                            </div>
                        </div>
                    </th>
                    <td class="table-search__ranking text-center">
                        <?php echo $sups['ranking']; ?>
                        <?php
                        if ($sups['ranking'] < $row_invest_france['ranking_fr']) {
                            ?>
                            <span class="ico-raise"></span>
                            <?php
                        }
                        if ($sups['ranking'] > $row_invest_france['ranking_fr']) {
                            ?>
                            <span class="ico-decrease"></span>
                            <?php
                        }
                        if ($sups['ranking'] == $row_invest_france['ranking_fr']) {
                            ?>
                            <span class="ico-stable"></span>
                            <?php
                        }
                        ?>
                    </td>
                    <td class="table-search__participations text-center"><?php echo $sups['participation']; ?></td>
                    <td class="table-search__yearInvests text-center"><?php echo str_replace(",0","",number_format($sups['invest_par_an'],1,","," ")); ?></td>
                    <td class="table-search__ticketmoy text-center"><?php echo str_replace(",0","",number_format($sups['moyenne']/1000,1,","," ")); ?> M€</td>
                    <td class="table-search__secteursInvests text-center"><?php echo $mains; ?></td>
                    <td class="table-search__siege text-center"><?php echo $mains_reg; ?></td>

                </tr>

                <?php
            }
            ?>
        </tbody>
    </table>
</div>