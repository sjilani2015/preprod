<?php

class Product {

    // database connection and table name
    private $conn;
    private $table_name = "startup";
    // object properties
    public $id;
    public $nom;
    public $short_fr;
    public $creation;
    public $date_add;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    // read products
    function read() {

        // select all query
        $query = "SELECT
                s.nom, s.id, s.creation, s.date_add,s.short_fr
            FROM
                " . $this->table_name . " s
                
            ORDER BY
                s.date_add DESC limit 5";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }

    // used when filling up the update product form
    function readOne() {

        // query to read single record
        $query = "SELECT
                 s.nom, s.id, s.creation, s.date_add,s.short_fr
            FROM
                " . $this->table_name . " s
                
            WHERE
                s.id = ?
            LIMIT
                0,1";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // bind id of product to be updated
        $stmt->bindParam(1, $this->id);

        // execute query
        $stmt->execute();

        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set values to object properties
        $this->nom = $row['nom'];
        $this->short_fr = $row['short_fr'];
        $this->id = $row['id'];
    }

    // search products
    function search($keywords) {

        // select all query
        $query = "SELECT
                 s.nom, s.id, s.creation, s.date_add,s.short_fr
            FROM
                " . $this->table_name . " s
                
            WHERE
                s.nom LIKE ? OR s.short_fr LIKE ?
            ORDER BY
                s.date_add DESC";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // sanitize
        $keywords = htmlspecialchars(strip_tags($keywords));
        $keywords = "%{$keywords}%";

        // bind
        $stmt->bindParam(1, $keywords);
        $stmt->bindParam(2, $keywords);
        $stmt->bindParam(3, $keywords);

        // execute query
        $stmt->execute();

        return $stmt;
    }

}

?>