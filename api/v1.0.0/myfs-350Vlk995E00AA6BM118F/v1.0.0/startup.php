<?php

// Connect to database
//include("db_connect.php");
$request_method = $_SERVER["REQUEST_METHOD"];

function getProducts() {




    $dbroot = 'localhost';
    $dbuser = 'myfrenchuser';
    $dbpass = 'My#2fsDb3';
    $dbname = 'myfrenchdata';

    $link = mysqli_connect($dbroot, $dbuser, $dbpass) or die('<h1>Impossible de trouver le serveur</h1>');
    mysqli_select_db($link, $dbname) or die('<h1>Impossible de trouver la base de données</h1>');

    $query = "SELECT startup.rang_effectif,lf.de,lf.date_ajout,startup.id,startup.nom,startup.cp,startup.ville,startup.creation,startup.url,startup.tel,startup.email,startup.short_fr,startup.serie,activite.secteur,activite.sous_secteur FROM startup inner join activite on startup.id=activite.id_startup inner join lf on startup.id=lf.id_startup where startup.status=1 and lf.rachat=0 order by startup.id desc limit 20";

    $response = array();
    $resultat = array();
    $result = mysqli_query($link, $query);
    while ($row = mysqli_fetch_array($result)) {

        $oo = mysqli_query($link, "select nom_secteur from secteur where id=" . $row['secteur']);
        $sql_nom_secteur = mysqli_fetch_array($oo);
        $nom_secteur = $sql_nom_secteur['nom_secteur'];

        $pp = mysqli_query($link, "select nom_sous_secteur from sous_secteur where id=" . $row['sous_secteur']);
        $sql_nom_sous_secteur = mysqli_fetch_array($pp);
        $nom_sous_secteur = $sql_nom_sous_secteur['nom_sous_secteur'];

        $ii = mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $row['id']);
        $total_levee = mysqli_fetch_array($ii);

        $ss = mysqli_query($link, "select * from personnes where id_startup=" . $row['id'] . " limit 1");
        $personnes = mysqli_fetch_array($ss);
        $fondateur = $personnes['prenom'] . " " . $personnes['nom'];

        $resultat = [
            'nom' => utf8_encode($row['nom']),
            'cp' => $row['cp'],
            'creation' => $row['creation'],
            'url' => $row['url'],
            'ville' => utf8_encode($row['ville']),
            'tel' => $row['tel'],
            'email' => $row['email'],
            'description' => utf8_encode($row['ch']),
            'serie' => $row['serie'],
            'total_levee' => $total_levee['somme'],
            'fondateur' => utf8_encode($fondateur),
            'secteur' => utf8_encode($nom_secteur),
            'effectif' => utf8_encode($row['rang_effectif']),
            'sous_secteur' => utf8_encode($nom_sous_secteur),
            'investisseur' => utf8_encode($row['de']),
            'date_levee' => utf8_encode($row['date_ajout']),
        ];

        $response[] = $resultat;
    }
    header('Content-Type: application/json');
    //echo  json_encode($response,JSON_FORCE_OBJECT);
    echo json_encode($response, JSON_PRETTY_PRINT);
}

switch ($request_method) {

    case 'POST':


        getProducts();

        break;
    default:
        // Invalid Request Method
        header("HTTP/1.0 405 Method Not Allowed");
        break;
}
?>