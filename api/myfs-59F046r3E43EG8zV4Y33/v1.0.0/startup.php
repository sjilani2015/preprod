<?php

// Connect to database
include("db_connect.php");
$request_method = $_SERVER["REQUEST_METHOD"];

function getProducts() {




    $dbroot = 'mysql51-109.bdb';
    $dbuser = 'myfrenchdata';
    $dbpass = '02Datawayp';
    $dbname = 'myfrenchdata';


    $link = mysqli_connect($dbroot, $dbuser, $dbpass) or die('<h1>Impossible de trouver le serveur</h1>');
    mysqli_select_db($link, $dbname) or die('<h1>Impossible de trouver la base de données</h1>');

    $query = "SELECT startup.id,startup.nom,startup.cp,startup.ville,startup.tel,startup.email,startup.short_fr,startup.serie,activite.secteur,activite.sous_secteur FROM startup inner join activite on startup.id=activite.id_startup where startup.status=1 limit 100";

    $response = array();
    $resultat = array();
    $result = mysqli_query($link, $query);
    while ($row = mysqli_fetch_array($result)) {

        $oo = mysqli_query($link, "select nom_secteur from secteur where id=" . $row['secteur']);
        $sql_nom_secteur = mysqli_fetch_array($oo);
        $nom_secteur = $sql_nom_secteur['nom_secteur'];


        $pp = mysqli_query($link, "select nom_sous_secteur from sous_secteur where id=" . $row['sous_secteur']);
        $sql_nom_sous_secteur = mysqli_fetch_array($pp);
        $nom_sous_secteur = $sql_nom_sous_secteur['nom_sous_secteur'];


        $ii = mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $row['id']);
        $total_levee = mysqli_fetch_array($ii);


        $ss = mysqli_query($link, "select * from personnes where id_startup=" . $row['id'] . " limit 1");
        $personnes = mysqli_fetch_array($ss);
        $fondateur = $personnes['prenom'] . " " . $personnes['nom'];

        $resultat = [
            'nom' => utf8_encode($row['nom']),
            'cp' => $row['cp'],
            'ville' => utf8_encode($row['ville']),
            'tel' => $row['tel'],
            'email' => $row['email'],
            'description' => utf8_encode($row['ch']),
            'serie' => $row['serie'],
            'total_levee' => $total_levee['somme'],
            'fondateur' => utf8_encode($fondateur),
            'secteur' => utf8_encode($nom_secteur),
            'effectif' => utf8_encode($row['effectif']),
            'sous_secteur' => utf8_encode($nom_sous_secteur),
        ];


        $response[] = $resultat;
    }
    header('Content-Type: application/json');
    echo json_encode($response, JSON_FORCE_OBJECT);
    //echo  json_encode($response,JSON_PRETTY_PRINT);
}

function getProduct($nom, $secteur, $creation, $levee, $keyword) {

    $dbroot = 'mysql51-109.bdb';
    $dbuser = 'myfrenchdata';
    $dbpass = '02Datawayp';
    $dbname = 'myfrenchdata';


    $link = mysqli_connect($dbroot, $dbuser, $dbpass) or die('<h1>Impossible de trouver le serveur</h1>');
    mysqli_select_db($link, $dbname) or die('<h1>Impossible de trouver la base de données</h1>');

    $query = "SELECT startup.id,startup.nom,startup.cp,startup.ville,startup.tel,startup.effectif,startup.email,startup.short_fr as ch,startup.serie,activite.secteur,activite.sous_secteur FROM startup inner join activite on startup.id=activite.id_startup";

    $query .= " WHERE status=1 ";

    if ($nom != "") {
        $query .= " and startup.nom like '%" . $nom . "%' ";
    }
    if ($secteur != "") {
        $query .= " and activite.secteur = " . $secteur . " ";
    }
    if ($creation != "") {
        $query .= " and startup.creation = " . $creation . " ";
    }
    if ($levee == "0") {
        $query .= " and startup.serie = '' ";
    }
    if ($levee == "1") {
        $query .= " and startup.serie != '' ";
    }
    if ($keyword != "") {
        $query .= " and (startup.nom like '%" . $keyword . "%' or startup.short_fr like '%" . $keyword . "%' or startup.long_fr like '%" . $keyword . "%' or startup.technologie like '%" . $keyword . "%' )  ";
    }

    $query .= "  limit 10 ";


    $response = array();
    $resultat = array();
    $result = mysqli_query($link, $query);
    while ($row = mysqli_fetch_array($result)) {

        $oo = mysqli_query($link, "select nom_secteur from secteur where id=" . $row['secteur']);
        $sql_nom_secteur = mysqli_fetch_array($oo);
        $nom_secteur = $sql_nom_secteur['nom_secteur'];


        $pp = mysqli_query($link, "select nom_sous_secteur from sous_secteur where id=" . $row['sous_secteur']);
        $sql_nom_sous_secteur = mysqli_fetch_array($pp);
        $nom_sous_secteur = $sql_nom_sous_secteur['nom_sous_secteur'];


        $ii = mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $row['id']);
        $total_levee = mysqli_fetch_array($ii);


        $ss = mysqli_query($link, "select * from personnes where id_startup=" . $row['id'] . " limit 1");
        $personnes = mysqli_fetch_array($ss);
        $fondateur = $personnes['prenom'] . " " . $personnes['nom'];

        $resultat = [
            'nom' => utf8_encode($row['nom']),
            'cp' => $row['cp'],
            'ville' => utf8_encode($row['ville']),
            'tel' => $row['tel'],
            'email' => $row['email'],
            'description' => utf8_encode($row['ch']),
            'serie' => $row['serie'],
            'total_levee' => $total_levee['somme'],
            'fondateur' => utf8_encode($fondateur),
            'secteur' => utf8_encode($nom_secteur),
            'effectif' => utf8_encode($row['effectif']),
            'sous_secteur' => utf8_encode($nom_sous_secteur),
        ];


        $response[] = $resultat;
    }
    header('Content-Type: application/json');
    echo json_encode($response, JSON_FORCE_OBJECT);
    //echo  json_encode($response,JSON_PRETTY_PRINT);
}

function getProduct2($nom) {

    $dbroot = 'mysql51-109.bdb';
    $dbuser = 'myfrenchdata';
    $dbpass = '02Datawayp';
    $dbname = 'myfrenchdata';


    $link = mysqli_connect($dbroot, $dbuser, $dbpass) or die('<h1>Impossible de trouver le serveur</h1>');
    mysqli_select_db($link, $dbname) or die('<h1>Impossible de trouver la base de données</h1>');

    $query = "SELECT startup.id,startup.nom,startup.cp,startup.ville,startup.tel,startup.email,startup.short_fr,startup.serie,activite.secteur,activite.sous_secteur FROM startup inner join activite on startup.id=activite.id_startup";

    $query .= " WHERE startup.nom like '%" . $nom . "%' limit 100";



    $response = array();
    $resultat = array();
    $result = mysqli_query($link, $query);
    while ($row = mysqli_fetch_array($result)) {

        $oo = mysqli_query($link, "select nom_secteur from secteur where id=" . $row['secteur']);
        $sql_nom_secteur = mysqli_fetch_array($oo);
        $nom_secteur = $sql_nom_secteur['nom_secteur'];


        $pp = mysqli_query($link, "select nom_sous_secteur from sous_secteur where id=" . $row['sous_secteur']);
        $sql_nom_sous_secteur = mysqli_fetch_array($pp);
        $nom_sous_secteur = $sql_nom_sous_secteur['nom_sous_secteur'];


        $ii = mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $row['id']);
        $total_levee = mysqli_fetch_array($ii);


        $ss = mysqli_query($link, "select * from personnes where id_startup=" . $row['id'] . " limit 1");
        $personnes = mysqli_fetch_array($ss);
        $fondateur = $personnes['prenom'] . " " . $personnes['nom'];

        $resultat = [
            'nom' => utf8_encode($row['nom']),
            'cp' => $row['cp'],
            'ville' => utf8_encode($row['ville']),
            'tel' => $row['tel'],
            'email' => $row['email'],
            'description' => utf8_encode($row['ch']),
            'serie' => $row['serie'],
            'total_levee' => $total_levee['somme'],
            'fondateur' => utf8_encode($fondateur),
            'secteur' => utf8_encode($nom_secteur),
            'effectif' => utf8_encode($row['effectif']),
            'sous_secteur' => utf8_encode($nom_sous_secteur),
        ];


        $response[] = $resultat;
    }
    header('Content-Type: application/json');
    echo json_encode($response, JSON_FORCE_OBJECT);
    //echo  json_encode($response,JSON_PRETTY_PRINT);
}

function getProduct3($secteur) {

    $dbroot = 'mysql51-109.bdb';
    $dbuser = 'myfrenchdata';
    $dbpass = '02Datawayp';
    $dbname = 'myfrenchdata';


    $link = mysqli_connect($dbroot, $dbuser, $dbpass) or die('<h1>Impossible de trouver le serveur</h1>');
    mysqli_select_db($link, $dbname) or die('<h1>Impossible de trouver la base de données</h1>');

    $query = "SELECT startup.id,startup.nom,startup.cp,startup.ville,startup.tel,startup.email,startup.short_fr,startup.serie,activite.secteur,activite.sous_secteur FROM startup inner join activite on startup.id=activite.id_startup";


    $query .= " WHERE activite.secteur = " . $secteur . "";


    $response = array();
    $resultat = array();
    $result = mysqli_query($link, $query);
    while ($row = mysqli_fetch_array($result)) {

        $oo = mysqli_query($link, "select nom_secteur from secteur where id=" . $row['secteur']);
        $sql_nom_secteur = mysqli_fetch_array($oo);
        $nom_secteur = $sql_nom_secteur['nom_secteur'];


        $pp = mysqli_query($link, "select nom_sous_secteur from sous_secteur where id=" . $row['sous_secteur']);
        $sql_nom_sous_secteur = mysqli_fetch_array($pp);
        $nom_sous_secteur = $sql_nom_sous_secteur['nom_sous_secteur'];


        $ii = mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $row['id']);
        $total_levee = mysqli_fetch_array($ii);


        $ss = mysqli_query($link, "select * from personnes where id_startup=" . $row['id'] . " limit 1");
        $personnes = mysqli_fetch_array($ss);
        $fondateur = $personnes['prenom'] . " " . $personnes['nom'];

        $resultat = [
            'nom' => utf8_encode($row['nom']),
            'cp' => $row['cp'],
            'ville' => utf8_encode($row['ville']),
            'tel' => $row['tel'],
            'email' => $row['email'],
            'description' => utf8_encode($row['ch']),
            'serie' => $row['serie'],
            'total_levee' => $total_levee['somme'],
            'fondateur' => utf8_encode($fondateur),
            'secteur' => utf8_encode($nom_secteur),
            'effectif' => utf8_encode($row['effectif']),
            'sous_secteur' => utf8_encode($nom_sous_secteur),
        ];


        $response[] = $resultat;
    }
    header('Content-Type: application/json');
    echo json_encode($response, JSON_FORCE_OBJECT);
    //echo  json_encode($response,JSON_PRETTY_PRINT);
}

switch ($request_method) {

    case 'POST':

        if (($_POST["nom"] == "") && ($_POST["secteur"] == "") && ($_POST["creation"] == "") && ($_POST["levee"] == "") && ($_POST["keyword"] == "")) {


            getProducts();
        } else {
            $nom = $_POST["nom"];
            $secteur = $_POST["secteur"];
            $creation = $_POST["creation"];
            $levee = $_POST["levee"];
            $keyword = $_POST["keyword"];

            getProduct($nom, $secteur, $creation, $levee, $keyword);
        }

        break;
    default:
        // Invalid Request Method
        header("HTTP/1.0 405 Method Not Allowed");
        break;
}
?>