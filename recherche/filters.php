<div class="modal" id="modal-searchfilters">
    <div class="modal__wrapper">
        <div class="modal__content">
            <div class="filters-content">
                <div class="filters-aside">
                    <div class="nav-vertical">
                        <div class="nav-vertical__title">
                            <span class="title">Filtres avancés</span>
                        </div>
                        <div class="nav-vertical__list">
                            <ul>
                                <li class="active"><a data-target-tab="1" href="" title="">Recherches fréquentes</a></li>
                                <li><a data-target-tab="2" href="" title="">Secteur</a></li>
                                <li><a data-target-tab="3" href="" title="">Informations légales</a></li>
                                <li><a data-target-tab="4" href="" title="">Localisation</a></li>
                                <li><a data-target-tab="5" href="" title="">Âge</a></li>
                                <li><a data-target-tab="6" href="" title="">Taille</a></li>
                                <li><a data-target-tab="7" href="" title="">Levées de fonds</a></li>
                                <li><a data-target-tab="8" href="" title="">Informations financières</a></li>
                                <li><a data-target-tab="9" href="" title="">Entrepreneurs</a></li>
                                <li><a data-target-tab="10" href="" title="">Type</a></li>
                                <li><a data-target-tab="11" href="" title="">Mes recherches sauvegardées</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="filters-right">
                    <span class="modal__closeBtn"></span>
                    <div class="filters-right__tab active" id="filterTab-1">
                        <div class="filters-right__tabcontent">
                            <div class="form-group">
                                <div class="form-label">Recherche par mot clé</div>
                                <div class="searchbar">
                                    <input class="searchbar__field form-control" type="text" value="" placeholder="Trouver une startup, un investisseur..." autocomplete="off" name="keyword_search1" id="keyword_search1">
                                    <span class="searchbar__btn"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.6 44.3"><path d="M24.3 38.6C13.6 38.6 5 29.9 5 19.3 5 8.7 13.6 0 24.3 0c10.6 0 19.3 8.7 19.3 19.3C43.6 29.9 34.9 38.6 24.3 38.6zM24.3 5.2c-7.8 0-14.1 6.3-14.1 14.1 0 7.8 6.3 14.1 14.1 14.1 7.8 0 14.1-6.3 14.1-14.1C38.4 11.5 32 5.2 24.3 5.2z"></path><path d="M3.2 44.3c-0.8 0-1.6-0.3-2.3-0.9 -1.3-1.3-1.3-3.3 0-4.5l8.2-8.2c1.3-1.3 3.3-1.3 4.5 0 1.3 1.3 1.3 3.3 0 4.5l-8.2 8.2C4.9 44 4 44.3 3.2 44.3z"></path></svg></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-label">Recherche par tag</div>
                                <div class="tags tags--left">
                                    <a href="#" title="" class="tags__el" data-dismiss="modal" onclick="next400()">
                                        <span class="ico-tag"></span> Next40
                                    </a>
                                    <a href="#" title="" class="tags__el" data-dismiss="modal" onclick="ft120()">
                                        <span class="ico-tag"></span> FT120
                                    </a>
                                    <a href="#" title="" class="tags__el" data-dismiss="modal" onclick="licorne()">
                                        <span class="ico-tag"></span> Licorne
                                    </a>
                                    <a href="#" title="" id="Fintech" class="tags__el" data-dismiss="modal" onclick="remplir_keyword(this);rechercher_startup()">
                                        <span class="ico-tag"></span> Fintech
                                    </a>
                                    <a href="#" title="" id="Chatbot" class="tags__el" data-dismiss="modal" onclick="remplir_keyword(this);rechercher_startup()">
                                        <span class="ico-tag"></span> Chatbot
                                    </a>
                                    <a href="#" title="" id="HealthTech" class="tags__el" data-dismiss="modal" onclick="remplir_keyword(this);rechercher_startup()">
                                        <span class="ico-tag"></span> HealthTech
                                    </a>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-label">Business modèle</div>

                                <div class="formgrid">
                                    <?php
                                    $sqlacti = mysqli_query($link, "Select  * From  last_activite  order by nom_activite");
                                    if (!empty($sqlacti)) {
                                        while ($activitedata = mysqli_fetch_array($sqlacti)) {
                                            ?>

                                            <div class="formgrid__item">
                                                <input class="inp-cbx" id="activite_<?php echo $activitedata['id']; ?>" type="checkbox" value="<?php echo $activitedata['id']; ?>" style="display: none" />
                                                <label class="cbx" for="activite_<?php echo $activitedata['id']; ?>">
                                                    <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    <span><?php echo ($activitedata['nom_activite']); ?></span>
                                                </label>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-label">Cible client</div>
                                <div class="formgrid">

                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="cible_B2B" type="checkbox" value="B2B" style="display: none" />
                                        <label class="cbx" for="cible_B2B">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>B2B</span>
                                        </label>
                                    </div>
                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="cible_B2C" type="checkbox" value="B2C" style="display: none" />
                                        <label class="cbx" for="cible_B2C">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>B2C</span>
                                        </label>
                                    </div>
                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="cible_C2C" type="checkbox" value="C2C" style="display: none" />
                                        <label class="cbx" for="cible_C2C">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>C2C</span>
                                        </label>
                                    </div>



                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="filters-right__tab" id="filterTab-2">
                        <div class="form-group">
                            <div class="form-label">Secteur</div>

                            <div class="formgrid">
                                <?php
                                $list_secteurs = getListSecteurTotal();
                                if (!empty($list_secteurs)) {
                                    foreach ($list_secteurs as $secteur_activite) {
                                        ?>
                                        <input type="hidden" name="" id="id_sec" value="<?php echo $secteur_activite['id']; ?>">
                                        <div class="formgrid__item">
                                            <input class="inp-cbx" id="secteur_<?php echo $secteur_activite['id']; ?>" type="checkbox" style="display: none" />
                                            <label onclick="get_sous_secteur(<?php echo $secteur_activite['id']; ?>)" class="cbx" for="secteur_<?php echo $secteur_activite['id']; ?>">
                                                <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                <span><?php echo utf8_encode($secteur_activite['nom_secteur']); ?></span>
                                            </label>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                        $list_ssecteurs10 = getListSecteurTotal();
                        if (isset($list_ssecteurs10) && !empty($list_ssecteurs10)) {
                            $t = 0;
                            foreach ($list_ssecteurs10 as $ssecteur_activite1) {
                                $nb_sec = mysqli_num_rows(mysqli_query($link, " select * from sous_secteur where id_secteur= " . $ssecteur_activite1['id']));
                                $t = $t + $nb_sec;
                            }
                           // echo $t;
                            if ($t > 0) {
                                ?>

                                <!-- à conditionner -->
                                <div class="form-group soussecteurs" id="bloc_sous_secteur" style="display: none;">
                                    <div class="form-label" id="lib_sous_secteur">Sous secteur</div>
                                    <div class="formgrid formgrid--secteurs">
                                        <?php
                                        foreach ($list_ssecteurs10 as $ssecteur_activite) {
                                            ?>
                                            <div id="bloc_sous_ssecteur_<?php echo $ssecteur_activite['id'] ?>" class="formgrid__secteurs" style="display: none;">
                                                <?php
                                                $sous_sec = mysqli_query($link, " select* from sous_secteur where id_secteur= " . $ssecteur_activite['id']);
                                                while ($datasous_sec = mysqli_fetch_array($sous_sec)) {
                                                    ?>
                                                    <input type="hidden" name="" id="id_sous_sec" value="<?php echo $datasous_sec['id']; ?>">
                                                    <div class="formgrid__item">
                                                        <input class="inp-cbx" id="sous_secteur_<?php echo $datasous_sec['id']; ?>" type="checkbox" style="display: none" />
                                                        <label class="cbx" for="sous_secteur_<?php echo $datasous_sec['id']; ?>">
                                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                            <span><?php echo ($datasous_sec['nom_sous_secteur']); ?></span>
                                                        </label>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>

                    </div>
                    <div class="filters-right__tab" id="filterTab-3">
                        <div class="filters-right__tabcontent">
                            <div class="form-group">
                                <div class="form-label">Forme juridique</div>
                                <div class="formgrid">

                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="forme_EURL" type="checkbox" value="EURL" style="display: none" />
                                        <label class="cbx" for="forme_EURL">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>EURL</span>
                                        </label>
                                    </div>
                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="forme_SAS" type="checkbox" value="SAS" style="display: none" />
                                        <label class="cbx" for="forme_SAS">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>SAS</span>
                                        </label>
                                    </div>
                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="forme_SARL" type="checkbox" value="SARL" style="display: none" />
                                        <label class="cbx" for="forme_SARL">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>SARL</span>
                                        </label>
                                    </div>
                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="forme_SA" type="checkbox" value="SA" style="display: none" />
                                        <label class="cbx" for="forme_SA">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>SA</span>
                                        </label>
                                    </div>


                                </div>
                            </div>
                            <div class="form-group form-group--half">
                                <div class="form-label">Code NAF</div>
                                <div class="custom-select">
                                    <select class="form-control" data-placeholder="Type" name="naf" id="naf"  onchange="getsousnaf(this)">
                                        <option value="">NAF - Libellé NAF</option>
                                        <?php
                                        $sql5 = mysqli_query($link, "Select  * From  startup_naf  order by label");

                                        if (!empty($sql5)) {
                                            while ($nafitem = mysqli_fetch_array($sql5)) {
                                                ?>
                                                <option value="<?php echo ($nafitem['label']) ?>"><?php echo ($nafitem['label']); ?> - <?php echo ($nafitem['label_fr']); ?></option>

                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group--half">
                                <div class="form-label">Sous NAF</div>
                                <div class="custom-select" id="bloc_sous_naf">
                                    <select data-placeholder="Type" class="form-control" name="sous_naf" id="sous_naf">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group--half">
                                <div class="form-label">Siren / Siret</div>
                                <input class="form-control" type="text" value="" placeholder="" name="siret" id="siret">
                            </div>

                            <div class="form-group">
                                <div class="formgrid">

                                    <div class="form-group">
                                        <div class="form-label">Document légal</div>
                                        <input class="form-control" type="text" value="" placeholder="" name="doc_legal" id="doc_legal">
                                    </div>

                                    <div class="formgrid__item">
                                        <div class="form-label">Date</div>
                                        <input class="form-control" type="date" value="" placeholder="" name="parution" id="parution">
                                    </div>
                                </div>
                                <div class="formgrid__item"></div>
                            </div>


                        </div>
                    </div>
                    <div class="filters-right__tab" id="filterTab-4">
                        <div class="form-group">
                            <div class="formgrid">
                                <div class="formgrid__item">
                                    <div class="form-label">Ville</div>
                                    <input class="form-control" type="text" value="" placeholder="" name="ville" id="ville">
                                </div>
                                <div class="formgrid__item">
                                    <div class="form-label">Code postal</div>
                                    <input class="form-control" type="text" value="" placeholder="" name="cp" id="cp">
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-group--half">
                            <div class="form-label">Région</div>
                            <div class="custom-select">
                                <select class="form-control" data-placeholder="" id="region" name="region">
                                    <?php
                                    if (isset($_GET['region']) && $_GET['region'] != '') {
                                        ?>
                                        <option value="<?php echo $_GET['region']; ?>"><?php echo $_GET['region']; ?></option>
                                        <?php
                                    } else {
                                        ?>
                                        <option value="">Région</option>
                                        <?php
                                    }
                                    ?>
                                    <option value="Auvergne-Rhône-Alpes">Auvergne-Rhône-Alpes</option>
                                    <option value="Bourgogne-Franche-Comté">Bourgogne-Franche-Comté</option>
                                    <option value="Bretagne">Bretagne</option>
                                    <option value="Centre-Val de Loire">Centre-Val de Loire</option>
                                    <option value="Corse">Corse</option>
                                    <option value="Grand Est">Grand Est</option>
                                    <option value="Hauts-de-France">Hauts-de-France</option>
                                    <option value="Île-de-France">Île-de-France</option>
                                    <option value="Métropole">Métropole</option>
                                    <option value="Normandie">Normandie</option>
                                    <option value="Nouvelle-Aquitaine">Nouvelle-Aquitaine</option>
                                    <option value="Occitanie">Occitanie</option>
                                    <option value="Pays de la Loire">Pays de la Loire</option>
                                    <option value="Provence-Alpes-Côte d'Azur">Provence-Alpes-Côte d'Azur</option>

                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="filters-right__tab" id="filterTab-5">
                        <div class="form-group">
                            <div class="form-group__title">Année de création</div>
                            <div class="formgrid">
                                <div class="formgrid__item">
                                    <div class="form-label form-label--muted">de</div>
                                    <div class="custom-select">
                                        <select class="form-control" data-placeholder="" name="creation_deb" id="creation_deb">
                                            <option value=""></option>
                                            <?php
                                            for ($i = date("Y"); $i >= 1998 + 1; $i--) {
                                                ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="formgrid__item">
                                    <div class="form-label form-label--muted">à</div>
                                    <div class="custom-select">
                                        <select class="form-control" data-placeholder="" name="creation_fin" id="creation_fin">
                                            <option value=""></option>

                                            <?php
                                            for ($i = date("Y"); $i >= 1998 + 1; $i--) {
                                                ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group__title">Date précise de création</div>
                            <div class="formgrid">
                                <div class="formgrid__item">
                                    <div class="form-label form-label--muted">de</div>
                                    <input type="date" id="debut_creation" name="debut_creation" class="form-control">
                                </div>
                                <div class="formgrid__item">
                                    <div class="form-label form-label--muted">à</div>
                                    <input type="date" id="fin_creation" name="fin_creation" class="form-control">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="filters-right__tab" id="filterTab-6">
                        <div class="form-group">
                            <div class="form-group__title">Effectif</div>
                            <div class="formgrid">
                                <div class="formgrid__item">
                                    <div class="form-label form-label--muted">de</div>
                                    <input type="number" id="effectif_min" class="form-control" placeholder="1">
                                </div>
                                <div class="formgrid__item">
                                    <div class="form-label form-label--muted">à</div>
                                    <input type="number" id="effectif_max" class="form-control" placeholder="100">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label">Rang effectifs</div>
                            <?php
                            $cb_effectif = array(
                                '1 personne',
                                '2 à 10 personnes',
                                '11 à 50 personnes',
                                '51 à 100 personnes',
                                'Supérieur à 100 personnes'
                            );
                            ?>
                            <div class="formgrid">
                                <?php
                                $i = 1;
                                foreach ($cb_effectif as $effectif) {
                                    ?>
                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="rang<?= $i ?>" value="<?php echo $i; ?>" type="checkbox" style="display: none" />
                                        <label class="cbx" for="rang<?= $i ?>">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span><?= $effectif ?></span>
                                        </label>
                                    </div>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </div>
                        </div>

                    </div>
                    <div class="filters-right__tab" id="filterTab-7">
                        <div class="form-group">
                            <div class="form-label">Levées de fonds</div>
                            <div class="formgrid">
                                <div class="formgrid__item">
                                    <input class="inp-cbx" id="check-a7" name="leve" onclick="affiche()" value="0" type="checkbox" style="display: none" />
                                    <label class="cbx" for="check-a7">
                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                        <span>A levé des fonds</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div id="blc" style="display:none">
                            <div class="form-group">
                                <div class="form-label">Nombre de levées de fonds</div>
                                <?php
                                $cb_leveesdefonds = array(
                                    '1 levée de fonds',
                                    '2 levées de fonds',
                                    '3 levées de fonds',
                                    '4 levées de fonds',
                                    '5 levées de fonds',
                                    '+5 levées de fonds'
                                );
                                ?>
                                <div class="formgrid">

                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="check-a" value="1" type="checkbox" style="display: none" />
                                        <label class="cbx" for="check-a">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>1 levée de fonds</span>
                                        </label>
                                    </div>
                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="check-b" value="2" type="checkbox" style="display: none" />
                                        <label class="cbx" for="check-b">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>2 levées de fonds</span>
                                        </label>
                                    </div>
                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="check-c" value="3" type="checkbox" style="display: none" />
                                        <label class="cbx" for="check-c">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>3 levées de fonds</span>
                                        </label>
                                    </div>
                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="check-d" value="4" type="checkbox" style="display: none" />
                                        <label class="cbx" for="check-d">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>4 levées de fonds</span>
                                        </label>
                                    </div>
                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="check-e" value="5" type="checkbox" style="display: none" />
                                        <label class="cbx" for="check-e">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>5 levées de fonds</span>
                                        </label>
                                    </div>
                                    <div class="formgrid__item">
                                        <input class="inp-cbx" id="check-f" value="+5" type="checkbox" style="display: none" />
                                        <label class="cbx" for="check-f">
                                            <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                            <span>+5 levées de fonds</span>
                                        </label>
                                    </div>


                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-label">Montant levé</div>
                                <?php
                                $cb_montantleveesdefonds = array(
                                    'inférieur à 1m€',
                                    'entre 1 m€ et 10 m€',
                                    'entre 10 m€ et 100 m€',
                                    'supérieur à 100 m€',
                                );
                                ?>
                                <div class="formgrid">
                                    <?php
                                    $i = 0;
                                    foreach ($cb_montantleveesdefonds as $mldf) {
                                        ?>
                                        <div class="formgrid__item">
                                            <input class="inp-cbx" name="lfs" id="check_lf<?= $i; ?>" type="checkbox" style="display: none" />
                                            <label class="cbx" for="check_lf<?= $i ?>">
                                                <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                <span><?= $mldf ?></span>
                                            </label>
                                        </div>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Date de levée de fonds</div>
                                <div class="formgrid">
                                    <div class="formgrid__item">
                                        <div class="form-label form-label--muted">de</div>
                                        <input type="date" name="creation_deb" id="date_l1" class="form-control">
                                    </div>
                                    <div class="formgrid__item">
                                        <div class="form-label form-label--muted">à</div>
                                        <input type="date" class="form-control"  name="creation_fin" id="date_l2">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="filters-right__tab" id="filterTab-8">
                        <div class="form-group">
                            <div class="form-label">Chiffre d'affaires</div>
                            <div class="formgrid formgrid--3col">
                                <div class="formgrid__item">
                                    <label class="custom-radio">
                                        Inférieur à 1 m€
                                        <input type="radio" checked="checked" name="cas" value="0">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="formgrid__item">
                                    <label class="custom-radio">
                                        Entre 1 m€ et 10 m€
                                        <input type="radio" checked="checked" name="cas"  value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="formgrid__item">
                                    <label class="custom-radio">
                                        Supérieur à 10 m€
                                        <input type="radio" checked="checked" name="cas"  value="2">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group__title">Capital social</div>
                            <div class="formgrid">
                                <div class="formgrid__item">
                                    <div class="form-label form-label--muted">de</div>
                                    <input type="text" class="form-control" name="min_cs" id="min_cs" value="" placeholder="m€">
                                </div>
                                <div class="formgrid__item">
                                    <div class="form-label form-label--muted">à</div>
                                    <input type="text" class="form-control" name="max_cs" id="max_cs" value="" placeholder="m€">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group__title">Résultat</div>
                            <div class="formgrid">
                                <div class="formgrid__item">
                                    <div class="form-label form-label--muted">de</div>
                                    <input type="text" class="form-control" name="resultat_min" id="resultat_min" value="" placeholder="m€">
                                </div>
                                <div class="formgrid__item">
                                    <div class="form-label form-label--muted">à</div>
                                    <input type="text" class="form-control" name="resultat_max" id="resultat_max" value="" placeholder="m€">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="filters-right__tab" id="filterTab-9">
                        <div class="form-group">
                            <div class="form-group__title">Recherche par nom</div>
                            <div class="formgrid">
                                <div class="formgrid__item">
                                    <div class="form-label form-label--muted">Prénom</div>
                                    <input type="text" class="form-control" value="" name="prenom" id="prenom_e" placeholder="">
                                </div>
                                <div class="formgrid__item">
                                    <div class="form-label form-label--muted">Nom</div>
                                    <input type="text" name="nom" id="nom_e"  class="form-control" value="" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-label">Recherche par formation</div>
                            <input type="text" class="form-control" id="autocomplete-ajax" placeholder="Exemple : Ecole Centrale Paris" autocomplete="off" value="" placeholder="">
                            <div id="suggesstion-box-formation" style="background: rgb(243 243 243);font-weight: 100;border: 1px solid rgb(140 140 140);border-radius: 5px;padding-right: 25px;display: none;top: 51px;width: 100%; z-index: 99999"></div>
                        </div>
                        <div class="form-group">
                            <div class="form-label">Recherche par expériences</div>
                            <input type="text" class="form-control" name="experienceses" placeholder="Exemple : Ingénieur" id="autocomplete-ajax1" autocomplete="off" value="" placeholder="">
                            <div id="suggesstion-box-exp"  style="background: rgb(243 243 243);font-weight: 100;border: 1px solid rgb(140 140 140);border-radius: 5px;padding-right: 25px;display: none;top: 51px;width: 100%; z-index: 99999"></div>
                        </div>
                        <div class="form-group">
                            <div class="form-label">Recherche par compétences</div>
                            <input type="text" class="form-control" name="skill" placeholder="Exemple : Gestion de projets"  id="autocomplete-ajax2" autocomplete="off" value="" placeholder="">
                            <div id="suggesstion-box-skills" style="background: rgb(243 243 243);font-weight: 100;border: 1px solid rgb(140 140 140);border-radius: 5px;padding-right: 25px;display: none;top: 51px;width: 100%; z-index: 99999"></div>
                        </div>

                    </div>
                    <div class="filters-right__tab" id="filterTab-10">
                        <div class="form-group">
                            <div class="form-label">Type</div>
                            <label class="custom-radio">
                                Racheté
                                <input type="radio"  name="rachat" id="rachat" value="1">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="custom-radio">
                                En bourse
                                <input type="radio"  name="rachat" id="ipo" value="2">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="custom-radio">
                                Liquidée
                                <input type="radio"  name="rachat" id="depot_bilan" value="3">
                                <span class="checkmark"></span>
                            </label>
                        </div>

                    </div>

                    <div class="filters-right__tab" id="filterTab-11">
                        <div class="form-label">Mes recherches sauvegardées</div>
                        <div class="savedsearches">
                            <div class="savedsearches__item">
                                <div class="savedsearches__item__name">
                                    <div class="name"><a href="#">AGENCE WEB</a></div>
                                    <div class="date">Recherche du 2021-09-13 15:20:21</div>
                                </div>
                                <div class="savedsearches__item__actions">
                                    <a href="" title=""><span class="ico-dashboard"></span></a>
                                    <a href="" title=""><span class="ico-eye"></span></a>
                                    <a href="" title=""><span class="ico-trash"></span></a>
                                </div>
                            </div>
                            <div class="savedsearches__item">
                                <div class="savedsearches__item__name">
                                    <div class="name"><a href="#">AGENCE WEB</a></div>
                                    <div class="date">Recherche du 2021-09-13 15:20:21</div>
                                </div>
                                <div class="savedsearches__item__actions">
                                    <a href="" title=""><span class="ico-dashboard"></span></a>
                                    <a href="" title=""><span class="ico-eye"></span></a>
                                    <a href="" title=""><span class="ico-trash"></span></a>
                                </div>
                            </div>
                            <div class="savedsearches__item">
                                <div class="savedsearches__item__name">
                                    <div class="name"><a href="#">AGENCE WEB</a></div>
                                    <div class="date">Recherche du 2021-09-13 15:20:21</div>
                                </div>
                                <div class="savedsearches__item__actions">
                                    <a href="" title=""><span class="ico-dashboard"></span></a>
                                    <a href="" title=""><span class="ico-eye"></span></a>
                                    <a href="" title=""><span class="ico-trash"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center;margin-right: 15px; margin-top: 20px">
                        <button type="button" class="btn btn-primary" onclick="rechercher_startup()" data-dismiss="modal">Valider</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
