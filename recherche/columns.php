<?php
if (isset($_SESSION['data_login'])) {
    $users = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));
    $ma_liste_colonne = explode(",", $users['colonne']);
} else {
    $ma_liste_colonne = array("6,7,8,9,10,11,12,13,14,15,16");
}
?>
<div id="modal-columns" class="modal">
    <div class="modal__wrapper">
        <div class="modal__content">
            <div class="modal__header">
                <span class="title">Personnalisez la vue de vos résultats et affichez la data qui vous intéresse</span>
                <span class="modal__closeBtn"></span>
            </div>
            <div class="modal__body">

                <div class="columnsFilter">
                    <?php
                    // ID to exclude
                    //$exclude = array(6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16); // 0 = logo
                    $exclude = $ma_liste_colonne; // 0 = logo

                    foreach ($searchColumns as $col) {
                        ?>
                        <?php
                        if (!in_array($col['id'], $exclude)) {
                            // toggle-selected : nouvelle class à définir pour les colonnes affichées
                            ?>
                            <div  onclick="update_colonne(<?= $col['id']; ?>)" class="columnsFilter__item toggle-vis toggle-selected" data-columnId="<?= $col['id']; ?>"><?= $col['name']; ?></div>
                            <?php
                        } else {
                            ?>

                            <div onclick="update_colonne(<?= $col['id']; ?>)" class="columnsFilter__item toggle-vis" data-columnId="<?= $col['id']; ?>"><?= $col['name']; ?></div>

                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
