<div id="modal-saveSearch" class="modal">
    <div class="modal__wrapper">
        <div class="modal__content">
            <div class="modal__header">
                <span class="title">Enregistrer votre recherche
                    <p class="subtitle">Enregistrez votre recherche et recevez une notification à chaque mise à jour d'une startup de votre liste</p>
                </span>
                <span class="modal__closeBtn"></span>
            </div>
            <div class="modal__body">
                <div class="form-group">
                    <div class="form-label">Nom de la recherche</div>
                    <div class="form-group-btn">
                        <input class="form-control" id="list_name" type="text" value="" placeholder="" name="">
                        <button type="button" onclick="add_to_list_myfs()" class="btn btn-primary">Valider</button>
                    </div>
                    <div id="attente_save" style="display: none; text-align: center; width: 100%;"><img src="https://www.myfrenchstartup.com/loader.gif" /></div>
                </div>

                <div class="form-label">Mes recherches sauvegardées</div>
                <div class="savedsearches" id="bloc_list_table1">
                     <?php
                        $sql_list = mysqli_query($link, "select * from user_save_list where user=" . $users['id'] . " order by dt desc");
                        while ($data_list_user = mysqli_fetch_array($sql_list)) {
                            ?>
                    <div class="savedsearches__item">
                        <div class="savedsearches__item__name">
                            <div class="name"><?php echo $data_list_user['nom']; ?></div>
                            <div class="date">Recherche du <?php echo $data_list_user['dt']; ?></div>
                        </div>
                        <div class="savedsearches__item__actions">
                            <a href="<?php echo URL ?>/dashboard/<?php echo generate_id($data_list_user['id']) ?>/<?php echo urlWriting($data_list_user['nom']); ?>" title=""><span class="ico-dashboard"></span></a>
                            <a href="" title=""><span class="ico-eye"></span></a>
                            <a onclick="delete_list(<?php echo $data_list_user['id']; ?>)" style="cursor: pointer" title=""><span class="ico-trash"></span></a>
                        </div>
                    </div>
                     <?php
                        }
                        ?>
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>
