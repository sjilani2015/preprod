<?php
include("include/db.php");
include("functions/functions.php");
include('config.php');
$monUrl = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$_SESSION['utm_source'] = $monUrl;
 if (!isset($_SESSION['data_login'])) {
 
  } else {
  $oo = mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'")or die(mysqli_error($link));
  $us = mysqli_fetch_array($oo);
  
  } 
$monUrl = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$_SESSION['utm_source'] = $monUrl;

function formatDateLf($date) {
    if ($date != '') {
        if (strlen($date) < 11) {
            $date = explode('-', $date);
        } else {
            $date = explode(' ', $date);
            $date = explode('-', $date[0]);
        }
        return $date[2] . '/' . $date[1] . '/' . $date[0];
    } else
        return '';
}

$total_sqm = mysqli_query($link, "select * from startup_deal_flow inner join startup on startup.id=startup_deal_flow.id_startup  where startup.status=1 and  startup_deal_flow.montant!=''  and startup_deal_flow.dt>='" . date('Y-m-d') . "' and startup_deal_flow.etat=1 order by startup.nom");
$nb_total = mysqli_num_rows($total_sqm);

$total_somme = mysqli_fetch_array(mysqli_query($link, "select sum(startup_deal_flow.montant) as somme from startup_deal_flow inner join startup on startup.id=startup_deal_flow.id_startup  where startup.status=1   and startup_deal_flow.dt>='" . date('Y-m-d') . "' and startup_deal_flow.montant!='' and startup_deal_flow.etat=1 order by startup.nom"));

$kpi = mysqli_fetch_array(mysqli_query($link, "select * from startup_deal_flow_stats limit 1"));
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Deal Flow - Startups françaises en cours de levées de fonds</title>
        <meta name="description" content="<?= METADESC; ?>">

        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/maps.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/franceLow.min.js"></script>

        <script>
            am4core.addLicense("CH303325752");
            am4core.addLicense("MP307346529");
        </script>
        <script>
            const   themeColorBlue = am4core.color('#00bff0'),
                    themeColorFill = am4core.color('#E1F2FA'),
                    themeColorGrey = am4core.color('#dadada'),
                    themeColorLightGrey = am4core.color('#F5F5F5'),
                    themeColorDarkgrey = am4core.color("#33333A"),
                    themeColorLine = am4core.color("#ff7a7a"),
                    themeColorLineRed = am4core.color("#FF7C7C"),
                    themeColorLineGreen = am4core.color("#21D59B"),
                    themeColorColumns = am4core.color("#E1F2FA"),
                    labelColor = am4core.color('#798a9a');
        </script>
        <style>
            .ctg--4_4{
                grid-template-columns: 1fr 1fr 1fr 1fr;
            }
            .ctg--2_1{
                grid-template-columns: 2fr 1fr;
            }
            @media screen and (max-width: 500px){
                .ctg--4_4 {
                    grid-template-columns: 1fr;
                }
                .ctg--2_1 {
                    grid-template-columns: 1fr;
                }
            }
            .parent{
                /*   position:relative;*/
            }
            .child1{
                z-index: 0;
                position: absolute;
            }
            .child2{
                z-index: 1;
                position: relative;
            }
        </style>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">




                    <section class="module">
                        <div class="module__title">Deal Flow - Startups françaises en cours de levées de fonds</div>
                        <div class="ctg ctg--2_1">
                            <div class="bloc">

                                <p><strong><?php echo $nb_total; ?> Startups françaises</strong> à la recherche de fonds, via notre plateforme myFrenchStartup.Le montant global qu’elles souhaitent lever dépasse les <strong><?php echo number_format((floor($total_somme['somme'] / 1000)), 0, ".", "."); ?> millions d'euros</strong>.</p>
                                <p>Nous avons questionné ces startups sur les montants recherchés, l'objectif de leur levée de fonds ainsi que le type d'investisseurs recherchés. Mais nous leur avons surtout posé des questions concernant leur traction en demandant des informations sur leurs clients, leurs subventions, les concours qu'ils ont gagné, le type d'innovation proposé (usage, modèle économique, ...) et les protections industrielles mises en place (brevet, soleau,...)</p>


                            </div>
                            <div class="bloc">
                                <img src="images/deal-flow-banner.jpg" style="width:100%" />
                            </div>
                        </div>
                    </section>
                    <section class="module">

                        <div class="ctg ctg--3_3">
                            <div class="bloc" style="background: #9ADCFF">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:#fff">Startups à la recherche de fonds</div>
                                    <div class="datasbloc__val" style="color:#fff"><?php echo $nb_total; ?></div>
                                </div>
                            </div>
                            <div class="bloc" style="background: #FF8AAE">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:#fff">Montant global recherché</div>
                                    <div class="datasbloc__val" style="color:#fff"><?php echo number_format(($total_somme['somme']), 3, " ", " "); ?> €</div>
                                </div>
                            </div>
                            <div class="bloc" style="background: #B4CFB0">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:#fff">Ticket moyen par levée de fonds</div>
                                    <div class="datasbloc__val" style="color:#fff"><?php echo number_format(($total_somme['somme'] / $nb_total), 3, " ", " "); ?> €</div>
                                </div>
                            </div>

                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">La liste des startups à la recherche de fonds</div>
                        <div class="bloc">
                            <div class="tablebloc">
                                <?php
                                if ($us['premium'] == 1) {
                                    ?>
                                    <table class="table table-search " id="mine">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="text-center nowrap">Activité</th>
                                                <th class="text-center">Date de clôture</th>
                                                <th class="text-center">Montant (€)</th>
                                                <th class="text-center">Type d'investisseurs ciblés</th>
                                                <th class="text-center">Objectifs</th>

                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $sql = mysqli_query($link, "select startup.logo,startup.id,startup.nom,startup.short_fr,startup.short_en,startup.cible,startup.date_complete,startup.effectif,startup.logo,startup_deal_flow.montant,startup_deal_flow.dt,startup_deal_flow.date_add,startup_deal_flow.secteur, startup_deal_flow.type_invest,startup_deal_flow.objectif,startup_deal_flow.business,startup_deal_flow.innovation from startup_deal_flow inner join startup on startup.id=startup_deal_flow.id_startup  where startup.status=1 and  startup_deal_flow.montant!='' and startup_deal_flow.etat=1 and startup_deal_flow.dt>='" . date('Y-m-d') . "' order by startup_deal_flow.id desc");

                                            while ($data = mysqli_fetch_array($sql)) {

                                                $secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur inner join activite on activite.secteur=secteur.id where activite.id_startup=" . $data['id']));
                                                ?>
                                                <tr>
                                                    <td class="table-search__startup">
                                                        <div class="startupInfos">
                                                            <div class="startupInfos__image">
                                                                <a href="<?php echo URL . '/fr/startup-deal-flow/' . generate_id($data['id']) . "/" . urlWriting(strtolower($data["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $data['logo']) ?>" alt=""></a>
                                                            </div>
                                                            <div class="startupInfos__desc">
                                                                <div class="companyName"><a href="<?php echo URL . '/fr/startup-deal-flow/' . generate_id($data['id']) . "/" . urlWriting(strtolower($data["nom"])) ?>" target="_blank"><?php echo (stripslashes($data['nom'])); ?></a></div>

                                                            </div>
                                                        </div>
                                                    </td>


                                                    <td><?php
                                                        if ($_SESSION['myfs_language'] == "fr")
                                                            echo $secteur['nom_secteur'];
                                                        else
                                                            echo $secteur['nom_secteur_en'];
                                                        ?></td>
                                                    <td>

                                                        <?php echo formatDateLf($data['dt']); ?>

                                                    </td>
                                                    <td  style="text-align: center"><h6 class="text-semibold"><b><?php echo number_format(($data['montant'] * 1000), 0, "", ",") ?></b></h6></td>

                                                    <td><?php
                                                        $tab_inves = explode(",", $data['type_invest']);
                                                        $nb = count($tab_inves);
                                                        for ($i = 0;
                                                                $i < $nb;
                                                                $i++) {
                                                            if ($tab_inves[$i] != '') {
                                                                echo $tab_inves[$i] . ', ';
                                                            }
                                                        }
                                                        ?></td>

                                                    <td><?php echo stripslashes(($data['objectif'])) ?></td>

                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                } else {
                                    ?>
                                    <div class="parent">
                                        <div class="child1"><a href="https://www.myfrenchstartup.com/nos-offres"><img src="flow.jpg" alt="" style="max-width: 95%;" class="img-fluid"></a></div>
                                        <div class="child2">
                                            <div style="text-align: center;padding-top: 7rem;margin-bottom: 4rem;"><img src="https://cdn.poool.fr/uploads/6197d747b48dc40034d2ed71/logo-myfs217x064.svg" style="width:250px"></div>
                                            <div style="color:#FFF;text-align: center;font-size: 2rem;margin-bottom: 4rem;">Pour visualiser la liste exclusive des startups à la recherche de fonds, merci de passer en mode premium</div>
                                            <div style="text-align: center;margin-bottom: 7rem;">
                                                <a class="btn btn-primary" href="https://www.myfrenchstartup.com/nos-offres">Découvrir</a>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </section>


                    <section class="module">
                        <div class="bloc">
                            <div class="bloc__title">Détails des Startups à la recherche de fonds par Secteurs d'activités</div>
                            <div class="chart chart--columns">
                                <div id="chart-historique-secteur-fr"></div>
                            </div>

                            <script>
                                // Demo
                                // https://jsfiddle.net/api/post/library/pure/
                                // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                am4core.ready(function () {

                                var chart = am4core.create("chart-historique-secteur-fr", am4charts.XYChart);
                                chart.paddingBottom = 0;
                                chart.paddingLeft = 0;
                                chart.paddingRight = 0;
                                // will use this to store colors of the same items
                                var colors = {};
                                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                categoryAxis.dataFields.category = "category";
                                categoryAxis.renderer.minGridDistance = 10;
                                categoryAxis.renderer.fontSize = "10px";
                                categoryAxis.renderer.labels.template.fill = labelColor;
                                categoryAxis.dataItems.template.text = "{realName}";
                                categoryAxis.renderer.grid.disabled = true;
                                // Setting up label rotation
                                categoryAxis.renderer.labels.template.rotation = 90;
                                // Title Bottom
                                categoryAxis.title.text = "Secteurs";
                                categoryAxis.title.rotation = 0;
                                categoryAxis.title.align = "center";
                                categoryAxis.title.valign = "bottom";
                                categoryAxis.title.dy = - 5;
                                categoryAxis.title.fontSize = 11;
                                categoryAxis.title.fill = labelColor;
                                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                valueAxis.renderer.labels.template.fontSize = 11;
                                valueAxis.renderer.labels.template.fill = labelColor;
                                // Title left
                                valueAxis.title.text = "Nombre";
                                valueAxis.title.rotation = - 90;
                                valueAxis.title.align = "left";
                                valueAxis.title.valign = "top";
                                valueAxis.title.dy = 0;
                                valueAxis.title.fontSize = 11;
                                valueAxis.title.fill = labelColor;
                                // single column series for all data
                                var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                columnSeries.fill = themeColorColumns;
                                columnSeries.stroke = 0;
                                columnSeries.columns.template.width = am4core.percent(80);
                                columnSeries.dataFields.categoryX = "category";
                                columnSeries.dataFields.valueY = "value";
                                // second value axis for quantity
                                var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                                valueAxis2.renderer.opposite = true;
                                valueAxis2.syncWithAxis = valueAxis;
                                valueAxis2.tooltip.disabled = true;
                                valueAxis2.renderer.labels.template.fontSize = 11;
                                valueAxis2.renderer.labels.template.fill = labelColor;
                                // Title right
                                valueAxis2.title.text = "Montant recherché (m€)";
                                valueAxis2.title.rotation = - 90;
                                valueAxis2.title.align = "left";
                                valueAxis2.title.valign = "top";
                                valueAxis2.title.dy = 0;
                                valueAxis2.title.fontSize = 11;
                                valueAxis2.title.fill = labelColor;
                                // quantity line series
                                var lineSeries = chart.series.push(new am4charts.LineSeries());
                                lineSeries.dataFields.categoryX = "category";
                                lineSeries.dataFields.valueY = "financement";
                                lineSeries.yAxis = valueAxis2;
                                lineSeries.bullets.push(new am4charts.CircleBullet());
                                lineSeries.fill = themeColorLine;
                                lineSeries.strokeWidth = 2;
                                lineSeries.fillOpacity = 0;
                                lineSeries.stroke = themeColorLine;
                                // when data validated, adjust location of data item based on count
                                lineSeries.events.on("datavalidated", function () {
                                lineSeries.dataItems.each(function (dataItem) {
                                // if count divides by two, location is 0 (on the grid)
                                if (dataItem.dataContext.count / 2 == Math.round(dataItem.dataContext.count / 2)) {
                                dataItem.setLocation("categoryX", 0);
                                }
                                // otherwise location is 0.5 (middle)
                                else {
                                dataItem.setLocation("categoryX", 0.5);
                                }
                                })
                                })

                                        ///// DATA
                                        var chartData = [];
                                var lineSeriesData = [];
                                chart.numberFormatter.numberFormat = "#.#";
                                var data =
                                {
<?php
$sql_courbe = mysqli_query($link, "select * from startup_deal_flow_courbe");
while ($data_courbe = mysqli_fetch_array($sql_courbe)) {
    ?>
                                    "<?php echo $data_courbe['secteur']; ?>": {
                                    "<?php echo $data_courbe['secteur']; ?>": <?php echo $data_courbe['nbr']; ?>,
                                            "financement": <?php echo ceil($data_courbe['montant'] / 1000); ?>
                                    },
    <?php
}
?>

                                }

                                // process data and prepare it for the chart
                                for (var providerName in data) {
                                var providerData = data[providerName];
                                // add data of one provider to temp array
                                var tempArray = [];
                                var count = 0;
                                // add items
                                for (var itemName in providerData) {
                                if (itemName != "financement") {
                                count++;
                                // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                }
                                }
                                // sort temp array
                                tempArray.sort(function (a, b) {
                                if (a.value > b.value) {
                                return 1;
                                } else if (a.value < b.value) {
                                return - 1
                                } else {
                                return 0;
                                }
                                })

                                        // add quantity and count to middle data item (line series uses it)
                                        var lineSeriesDataIndex = Math.floor(count / 2);
                                tempArray[lineSeriesDataIndex].financement = providerData.financement;
                                tempArray[lineSeriesDataIndex].count = count;
                                // push to the final data
                                am4core.array.each(tempArray, function (item) {
                                chartData.push(item);
                                })
                                }

                                valueAxis.renderer.grid.template.strokeWidth = 0;
                                //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                categoryAxis.renderer.grid.template.strokeWidth = 0;
                                chart.data = chartData;
                                }); // end am4core.ready()
                            </script>
                        </div>





                    </section>

                    <section class="module">
                        <div class="module__title">Typologie des Startups</div>

                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <p>Les Startups qui nous ont communiqué leur recherche de fonds, souhaitent lever des fonds en early-stage. Il s'agit de leur toute première levée de fonds.</p>

                                <p>La grande majorité a moins de 5 ans d'âge et leur effectif dépasse à peine les 5 salariés.</p>

                                <p>L’Île de France attire comme toujours une grande partie des investissements, suivie par la région Auvergne-Rhône Alpes et Occitanie.</p>

                                <p>Plusieurs startups ciblent les marchés BtoB et BtoC en même temps, avec une prédominance du secteur BtoB.</p>
                                <div class="ctg ctg--3_3">
                                    <div class="bloc" style="border: 1px solid #F06292;padding: 1em;color: #F06292;">
                                        <div class="datasbloc">
                                            <div class="datasbloc__key" style="color: #F06292;">BtoB</div>
                                            <div class="datasbloc__val" style="color: #F06292;"><?php echo $kpi['BtoB']; ?></div>
                                        </div>
                                    </div>
                                    <div class="bloc" style="border: 1px solid #66BB6A;padding: 1em;color: #66BB6A;">
                                        <div class="datasbloc">
                                            <div class="datasbloc__key" style="color: #66BB6A;">BtoC</div>
                                            <div class="datasbloc__val" style="color: #66BB6A;"><?php echo $kpi['BtoC']; ?></div>
                                        </div>
                                    </div>
                                    <div class="bloc" style="border: 1px solid #7986CB;padding: 1em;color: #7986CB;">
                                        <div class="datasbloc">
                                            <div class="datasbloc__key" style="color: #7986CB;">CtoC</div>
                                            <div class="datasbloc__val" style="color: #7986CB;"><?php echo $kpi['CtoC']; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Répartition par année de création</div>
                                <div class="chart chart--columns">
                                    <div id="chart-annee-creation"></div>
                                </div>
                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                    var chart = am4core.create("chart-annee-creation", am4charts.XYChart);
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "category";
                                    categoryAxis.renderer.minGridDistance = 10;
                                    categoryAxis.renderer.fontSize = "10px";
                                    categoryAxis.renderer.labels.template.fill = labelColor;
                                    categoryAxis.dataItems.template.text = "{realName}";
                                    categoryAxis.renderer.grid.disabled = true;
                                    // Title Bottom
                                    categoryAxis.title.text = "Année de création";
                                    categoryAxis.title.rotation = 0;
                                    categoryAxis.title.align = "center";
                                    categoryAxis.title.valign = "bottom";
                                    categoryAxis.title.dy = - 5;
                                    categoryAxis.title.fontSize = 11;
                                    categoryAxis.title.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    // Title left
                                    valueAxis.title.text = "Nombre de startups";
                                    valueAxis.title.rotation = - 90;
                                    valueAxis.title.align = "left";
                                    valueAxis.title.valign = "top";
                                    valueAxis.title.dy = 0;
                                    valueAxis.title.fontSize = 11;
                                    valueAxis.title.fill = labelColor;
                                    // single column series for all data
                                    var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                    columnSeries.fill = themeColorColumns;
                                    columnSeries.stroke = 0;
                                    columnSeries.columns.template.width = am4core.percent(80);
                                    columnSeries.dataFields.categoryX = "category";
                                    columnSeries.dataFields.valueY = "value";
                                    ///// DATA
                                    var chartData = [];
                                    var data =
                                    {
<?php
$tab_list_creation = explode(";", $kpi['deal_flow_annee']);
$tab_list_nbr_creation = explode(";", $kpi['deal_flow_annee_nbr']);

$b = count($tab_list_creation);
for ($i = 0; $i < $b; $i++) {
    ?>

                                        "<?php echo $tab_list_creation[$i]; ?>": {
                                        "<?php echo $tab_list_creation[$i]; ?>": <?php echo $tab_list_nbr_creation[$i]; ?>,
                                        },
    <?php
}
?>
                                    }

                                    // process data and prepare it for the chart
                                    for (var providerName in data) {
                                    var providerData = data[providerName];
                                    // add data of one provider to temp array
                                    var tempArray = [];
                                    var count = 0;
                                    // add items
                                    for (var itemName in providerData) {
                                    if (itemName != "financement") {
                                    count++;
                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                    }
                                    }
                                    // sort temp array
                                    tempArray.sort(function (a, b) {
                                    if (a.value > b.value) {
                                    return 1;
                                    } else if (a.value < b.value) {
                                    return - 1
                                    } else {
                                    return 0;
                                    }
                                    })


                                            // push to the final data
                                            am4core.array.each(tempArray, function (item) {
                                            chartData.push(item);
                                            })
                                    }

                                    valueAxis.renderer.grid.template.strokeWidth = 0;
                                    //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                    categoryAxis.renderer.grid.template.strokeWidth = 0;
                                    chart.data = chartData;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Répartition par effectifs</div>
                                <div class="chart chart--columns">
                                    <div id="chart-effectifs"></div>
                                </div>
                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                    var chart = am4core.create("chart-effectifs", am4charts.XYChart);
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "category";
                                    categoryAxis.renderer.minGridDistance = 10;
                                    categoryAxis.renderer.fontSize = "10px";
                                    categoryAxis.renderer.labels.template.fill = labelColor;
                                    categoryAxis.dataItems.template.text = "{realName}";
                                    categoryAxis.renderer.grid.disabled = true;
                                    // Title Bottom
                                    categoryAxis.title.text = "Tranches d'effectifs";
                                    categoryAxis.title.rotation = 0;
                                    categoryAxis.title.align = "center";
                                    categoryAxis.title.valign = "bottom";
                                    categoryAxis.title.dy = - 5;
                                    categoryAxis.title.fontSize = 11;
                                    categoryAxis.title.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    // Title left
                                    valueAxis.title.text = "Nombre de startups";
                                    valueAxis.title.rotation = - 90;
                                    valueAxis.title.align = "left";
                                    valueAxis.title.valign = "top";
                                    valueAxis.title.dy = 0;
                                    valueAxis.title.fontSize = 11;
                                    valueAxis.title.fill = labelColor;
                                    // single column series for all data
                                    var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                    columnSeries.fill = themeColorColumns;
                                    columnSeries.stroke = 0;
                                    columnSeries.columns.template.width = am4core.percent(80);
                                    columnSeries.dataFields.categoryX = "category";
                                    columnSeries.dataFields.valueY = "value";
                                    ///// DATA
                                    var chartData = [];
                                    var data =
                                    {
<?php
$tab_list_effectif = explode(";", $kpi['deal_flow_effectif']);
$tab_list_nbr_effectif = explode(";", $kpi['deal_flow_effectif_nbr']);

$b = count($tab_list_effectif);
for ($i = 0; $i < $b; $i++) {
    ?>

                                        "<?php echo $tab_list_effectif[$i]; ?>": {
                                        "<?php echo $tab_list_effectif[$i]; ?>": <?php echo $tab_list_nbr_effectif[$i]; ?>,
                                        },
    <?php
}
?>
                                    }


                                    // process data and prepare it for the chart
                                    for (var providerName in data) {
                                    var providerData = data[providerName];
                                    // add data of one provider to temp array
                                    var tempArray = [];
                                    var count = 0;
                                    // add items
                                    for (var itemName in providerData) {
                                    if (itemName != "financement") {
                                    count++;
                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                    }
                                    }
                                    // sort temp array
                                    tempArray.sort(function (a, b) {
                                    if (a.value > b.value) {
                                    return 1;
                                    } else if (a.value < b.value) {
                                    return - 1
                                    } else {
                                    return 0;
                                    }
                                    })


                                            // push to the final data
                                            am4core.array.each(tempArray, function (item) {
                                            chartData.push(item);
                                            })
                                    }

                                    valueAxis.renderer.grid.template.strokeWidth = 0;
                                    //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                    categoryAxis.renderer.grid.template.strokeWidth = 0;
                                    chart.data = chartData;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Répartition par région</div>
                                <div class="tablebloc">
                                    <table class="table table--geographie">
                                        <thead>
                                            <tr>
                                                <th>Région</th>
                                                <th class="moneyCell text-center">Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $tab_list_region = explode(";", $kpi['deal_flow_region']);
                                            $tab_list_nbr_region = explode(";", $kpi['deal_flow_region_nbr']);

                                            $b = count($tab_list_region);
                                            for ($i = 0; $i < $b; $i++) {
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo addslashes($tab_list_region[$i]); ?>
                                                    </td>
                                                    <td class="text-center"><?php echo $tab_list_nbr_region[$i]; ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            <?php
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </section>



                    <section class="module">
                        <div class="module__title">Analyse des Startups à la recherche de fonds</div>
                        <div class="bloc">
                            <div class="bloc__title">Objectifs des fonds recherchés</div>
                            <div class="bloc">
                                <div class="chart chart--columns">
                                    <div id="chart-objectifs"></div>
                                </div>
                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                    var chart = am4core.create("chart-objectifs", am4charts.XYChart);
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "category";
                                    categoryAxis.renderer.minGridDistance = 10;
                                    categoryAxis.renderer.fontSize = "10px";
                                    categoryAxis.renderer.labels.template.fill = labelColor;
                                    categoryAxis.dataItems.template.text = "{realName}";
                                    categoryAxis.renderer.grid.disabled = true;
                                    // Title Bottom
                                    categoryAxis.title.text = "";
                                    categoryAxis.title.rotation = 0;
                                    categoryAxis.title.align = "center";
                                    categoryAxis.title.valign = "bottom";
                                    categoryAxis.title.dy = - 5;
                                    categoryAxis.title.fontSize = 11;
                                    categoryAxis.title.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    // Title left
                                    valueAxis.title.text = "Nombre de startups";
                                    valueAxis.title.rotation = - 90;
                                    valueAxis.title.align = "left";
                                    valueAxis.title.valign = "top";
                                    valueAxis.title.dy = 0;
                                    valueAxis.title.fontSize = 11;
                                    valueAxis.title.fill = labelColor;
                                    // single column series for all data
                                    var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                    columnSeries.fill = themeColorColumns;
                                    columnSeries.stroke = 0;
                                    columnSeries.columns.template.width = am4core.percent(80);
                                    columnSeries.dataFields.categoryX = "category";
                                    columnSeries.dataFields.valueY = "value";
                                    ///// DATA
                                    var chartData = [];
                                    var data =
                                    {
<?php
$tab_list_objectif = explode(";", $kpi['deal_flow_objectif']);
$tab_list_nbr_objectif = explode(";", $kpi['deal_flow_objectif_nbr']);

$b = count($tab_list_objectif);
for ($i = 0; $i < $b; $i++) {
    ?>

                                        "<?php echo $tab_list_objectif[$i]; ?>": {
                                        "<?php echo $tab_list_objectif[$i]; ?>": <?php echo $tab_list_nbr_objectif[$i]; ?>,
                                        },
    <?php
}
?>
                                    }


                                    // process data and prepare it for the chart
                                    for (var providerName in data) {
                                    var providerData = data[providerName];
                                    // add data of one provider to temp array
                                    var tempArray = [];
                                    var count = 0;
                                    // add items
                                    for (var itemName in providerData) {
                                    if (itemName != "financement") {
                                    count++;
                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                    }
                                    }
                                    // sort temp array
                                    tempArray.sort(function (a, b) {
                                    if (a.value > b.value) {
                                    return 1;
                                    } else if (a.value < b.value) {
                                    return - 1
                                    } else {
                                    return 0;
                                    }
                                    })


                                            // push to the final data
                                            am4core.array.each(tempArray, function (item) {
                                            chartData.push(item);
                                            })
                                    }

                                    valueAxis.renderer.grid.template.strokeWidth = 0;
                                    //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                    categoryAxis.renderer.grid.template.strokeWidth = 0;
                                    chart.data = chartData;
                                    }); // end am4core.ready()
                                </script>

                            </div>
                        </div>
                        <div class="bloc__title" style="margin-top:2rem"><h3>Traction : <span style="font-weight: normal; font-size: 13px;">Indicateurs et signaux (Premiers utilisateurs, premiers partenariats, concours et récompenses). </span></h3></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc" style="border: 1px solid #F06292; padding: 1em; color:#F06292">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:#F06292">Dispose de clients payants</div>
                                    <div class="datasbloc__val" style="color:#F06292"><?php echo $kpi['payant'] ?></div>
                                </div>
                            </div>
                            <div class="bloc" style="border: 1px solid #66BB6A; padding: 1em; color:#66BB6A">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:#66BB6A">A bénéficié d'une Subvention</div>
                                    <div class="datasbloc__val" style="color:#66BB6A"><?php echo $kpi['subventions'] ?></div>
                                </div>
                            </div>
                            <div class="bloc" style="border: 1px solid #7986CB; padding: 1em; color:#7986CB">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:#7986CB">A gagné un concours</div>
                                    <div class="datasbloc__val" style="color:#7986CB"><?php echo $kpi['concours'] ?></div>
                                </div>
                            </div>
                            <div class="bloc" style="border: 1px solid orange; padding: 1em; color:orange">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:orange">Collabore avec des Grands Comptes</div>
                                    <div class="datasbloc__val" style="color:orange"><?php echo $kpi['grand_compte'] ?></div>
                                </div>
                            </div>
                        </div>

                        <div class="bloc__title" style="margin-top:2rem"><h3>Propriété industrielle et protection : <span style="font-weight: normal; font-size: 13px;">Les protections et brevets mis en place pour protéger leur innovation. </span></h3></div>
                        <div class="ctg ctg--3_3">
                            <div class="bloc" style="border: 1px solid #7986CB; padding: 1em; color:#7986CB">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:#7986CB">Brevet</div>
                                    <div class="datasbloc__val" style="color:#7986CB"><?php echo $kpi['Brevet'] ?></div>
                                </div>
                            </div>
                            <div class="bloc" style="border: 1px solid orange; padding: 1em; color:orange">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:orange">Dessins et modèles</div>
                                    <div class="datasbloc__val" style="color:orange"><?php echo $kpi['Dessins'] ?></div>
                                </div>
                            </div>
                            <div class="bloc" style="border: 1px solid #F06292; padding: 1em; color:#F06292">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:#F06292">Soleau</div>
                                    <div class="datasbloc__val" style="color:#F06292"><?php echo $kpi['Soleau'] ?></div>
                                </div>
                            </div>

                        </div>
                        <div class="bloc__title" style="margin-top:2rem"><h3>Type d'innovation : <span style="font-weight: normal; font-size: 13px;">Les différentes formes d'innovation mises en place pour se démarquer des concurrents. </span></h3></div>
                        <div class="ctg ctg--4_4">
                            <div class="bloc" style="border: 1px solid #66BB6A; padding: 1em; color:#66BB6A">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:#66BB6A">Modèle économique</div>
                                    <div class="datasbloc__val" style="color:#66BB6A"><?php echo $kpi['economique'] ?></div>
                                </div>
                            </div>
                            <div class="bloc" style="border: 1px solid #66BB6A; padding: 1em; color:#66BB6A">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:#66BB6A">Positionnement</div>
                                    <div class="datasbloc__val" style="color:#66BB6A"><?php echo $kpi['Positionnement'] ?></div>
                                </div>
                            </div>
                            <div class="bloc" style="border: 1px solid #66BB6A; padding: 1em; color:#66BB6A">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:#66BB6A">Technologique</div>
                                    <div class="datasbloc__val" style="color:#66BB6A"><?php echo $kpi['Technologique'] ?></div>
                                </div>
                            </div>
                            <div class="bloc" style="border: 1px solid #66BB6A; padding: 1em; color:#66BB6A">
                                <div class="datasbloc">
                                    <div class="datasbloc__key" style="color:#66BB6A">Usage</div>
                                    <div class="datasbloc__val" style="color:#66BB6A"><?php echo $kpi['Usages'] ?></div>
                                </div>
                            </div>

                        </div>
                    </section>







                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>jquery.dataTables.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>datatables.bootstrap.min.js?<?= time(); ?>"></script>

        <script>
                                    var table = jQuery('#table-marches-secteurs').DataTable({
                                    "searching": false,
                                            "order": [[3, "desc"]],
                                            "nextPrev": false,
                                            "bLengthChange": false,
                                            "bInfo": false,
                                            "bPaginate": false,
                                            "autoWidth": false,
                                            //"fixedColumns": false,
                                            initComplete: (settings, json) => {
                                    $('#paginateTable').empty();
                                    $('#mine_paginate').appendTo('#paginateTable');
                                    },
                                            "language": {
                                            "paginate": {
                                            "previous": "Précédent",
                                                    "next": "Suivant"
                                            }
                                            }
                                    });
        </script>




        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>


        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
                                    (function (i, s, o, g, r, a, m) {
                                    i['GoogleAnalyticsObject'] = r;
                                    i[r] = i[r] || function () {
                                    (i[r].q = i[r].q || []).push(arguments)
                                    }, i[r].l = 1 * new Date();
                                    a = s.createElement(o),
                                            m = s.getElementsByTagName(o)[0];
                                    a.async = 1;
                                    a.src = g;
                                    m.parentNode.insertBefore(a, m)
                                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                    ga('create', 'UA-36251023-1', 'auto');
                                    ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
            var $ = jQuery;
            var valeur = document.getElementById("search-box").value;
            $.ajax({
            type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                    $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#search-box").css("background", "#FFF");
                    }
            });
            }

            function selectCountry(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }

            var table = $('#mine').DataTable({
            "searching": false,
                    "nextPrev": false,
                    "bLengthChange": false,
                    "bInfo": false,
                    "bPaginate": true,
                    "autoWidth": false,
                    //"fixedColumns": false,
                    initComplete: (settings, json) => {
            $('#paginateTable').empty();
            $('#mine_paginate').appendTo('#paginateTable');
            },
                    "language": {
                    "paginate": {
                    "previous": "Précédent",
                            "next": "Suivant"
                    }
                    }

            });
        </script>
    </body>
</html>