<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Mentions légales - <?= SITENAME; ?></title>
        <meta name="description" content="Foire aux questions <?= SITENAME; ?>">
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="edito">

                    <div class="page-title"><h1>Mentions légales</h1></div>

                    <h2>A propos de MYFRENCHSTARTUP</h2>
                    <p>myfrenchstartup.com est un service de qualification et de diffusion d'informations sur les startups françaises. Vous trouverez sur le site les informations suivantes :</p>

                    <h3 class="sub_title">Informations gratuites</h3>
                    <div>
                        <p>
                            <b>Les informations générales</b> (sources privées).<br>
                            Les données sont collectées et traitées par l’équipe MYFRENCHSTARTUP.<br>
                            Le service de distribution est assuré par MYFRENCHSTARTUP.<br>
                            Les éléments diffusés permettent l'identification des startups françaises : activité, présentation, adresse, téléphone, dirigeants, effectif, année de création, site web, réseaux sociaux,...<br>
                            La mise à jour est quotidienne.<br>
                            <b>Les 5 derniers deals des startups </b>(sources privées).<br>
                            Les données sont collectées et traitées par l’équipe MYFRENCHSTARTUP.<br>
                            Le service de distribution est assuré par MYFRENCHSTARTUP.<br>
                            Les éléments diffusés permettent l'identification des dernières levées de fonds, acquisitions et entrée en bourse des startups françaises.<br>
                            La mise à jour est quotidienne.
                        </p>
                    </div>

                    <h3 class="sub_title">Informations payantes</h3>
                    <div>
                        <p>De nombreuses informations (détails des levée de fonds, liste des investisseurs, montant levés,…) et services (recherches personnalisées de startups ou toutes autres données) sont disponibles en achat à l'acte ou sur abonnement en utilisant les différents moyens de paiement proposés. Toute commande implique l'acceptation des conditions de vente ainsi que la connaissance des moyens techniques de livraison. Les services proposés par nos partenaires sont facturés par ces derniers selon leurs propres conditions générales de vente.</p>
                    </div>
                    <h3>Société éditrice du site</h3>
                    <p> myfrenchstartup, société par action simplifiée, au capital de 2.000€, sise au 26-28, rue Danielle Casanova 75002 Paris et enregistrée au RCS de Paris sous le numéro 798 203 774.
                        <br>Numéro de TVA intracommunautaire : FR76798203774
                    </p>
                    <h3>Coordonnées de la société éditrice</h3>
                    <p>myfrenchstartup SAS, 26-28, rue Danielle Casanova - 75002 Paris.
                        <br>Email : contact@myfrenchstartup.com
                    </p>
                    <h3 class="sub_title">Directeur de la publication  </h3>
                    <p>Directeur de la publication Nicolas BEAUMONT - Président</p>

                    <h3>Hébergeur</h3>
                    <p>OVH<br>
                        SAS au capital de 10 069 020 €<br>
                        RCS Lille Métropole 424 761 419 00045<br>
                        Code APE 2620Z<br>
                        N° TVA : FR 22 424 761 419<br>
                        Siège social : 2 rue Kellermann - 59100 Roubaix - France</p>

                    <h3>Consultation</h3>
                    <p>La consultation ou la réception de documents n'entraîne aucun transfert de droit de propriété intellectuelle en faveur de l'utilisateur. Ce dernier s'engage à ne pas rediffuser ou à reproduire les données fournies autrement que pour son usage personnel. Les données transmises sont traitées en conformité avec les usages en vigueur. L'éditeur fournit l'information et les services associés en l'état et ne saurait accorder une garantie quelconque notamment pour la fiabilité, l'actualité, l'exhaustivité des données. L'utilisateur recherche, sélectionne et interprète les données sous sa propre responsabilité.
                        <br>Toutes les informations sont données à titre indicatif.<br>
                        Les éventuelles erreurs figurant dans les fichiers peuvent être signalées avec le formulaire à cet effet sur la page <a href="contact.php">«<u>Nous contacter</u>»</a></p>

                    <h3>Confidentialité</h3>
                    <p>myfrenchstartup.com et ses mandants, se réservent le droit de contacter toute personne ayant volontairement donné ses coordonnées au sujet d'opérations ou de promotions sur le site. myfrenchstartup.com pourra procéder à des analyses statistiques sans que celles-ci soit nominatives et pourra en informer des tiers (organismes d'évaluation de fréquentation) sous une forme résumée et non nominative</p>

                    <h3>Utilisation de cookies</h3>
                    <p>La gestion des commandes nécessite l'utilisation de cookies. Des informations non personnelles sont enregistrées par ce système de cookies (fichiers texte utilisés pour reconnaître un utilisateur et ainsi faciliter son utilisation du site). Ceux-ci n'ont aucune signification en dehors de leur utilisation sur le site myfrenchstartup.com.</p>

                    <h3>Conditions d'utilisation</h3>
                    <div>
                        <p>Les données contenues sur le site myfrenchstartup.com sont protégées par la loi du 1er juillet 1998 sur les bases de données.<br>
                            En accédant ou en utilisant le site, vous reconnaissez vous conformer aux dispositions de la loi, et notamment en vous interdisant l'extraction, le transfert, le stockage, la reproduction de tout ou partie qualitativement ou quantitativement substantielle du contenu des bases de données figurant sur le site.</p>
                        <p><b>La reproduction, la rediffusion ou l'extraction automatique par tout moyen d'informations figurant sur myfrenchstartup.com est interdite. L'emploi de robots, programmes permettant l'extraction directe de données est rigoureusement interdit</b></p>
                        <p>
                            MYFRENCHSTARTUP SAS se réserve le droit d'entamer toute action visant à faire cesser le préjudice.<br>
                            MYFRENCHSTARTUP SAS propose un service de vente de données en format fichier.<br>
                            Le site myfrenchstartup.com a été déclaré à la CNIL en application de l'article 16 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés (Récépissé N° 580653).



                        </p>
                    </div>

                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/animated.min.js"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>