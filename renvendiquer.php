<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Revendiquer ma startup - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <?php
            if (isset($_POST["nom"])) {




                mysqli_query($link, "INSERT INTO contact (prenom, nom, email, tel, message, sujet, date) VALUES ( '" . addslashes($_POST["prenom"]) . "', '" . addslashes($_POST["nom"]) . "', '" . addslashes($_POST["email"]) . "', '" . addslashes($_POST["tel"]) . "', '" . addslashes($_POST["story"]) . "', '" . addslashes($_POST["sujet"]) . "', '" . date('Y-m-d H:i:s') . "');");
                $headers = "From: \"" . $_POST['lastname_contact'] . "\"<" . $_POST['email'] . ">\n";
                $headers .= "Reply-To: " . $_POST['email'] . "\n";
                $headers .= "Content-Type: text/html; charset=\"utf-8\"";
                $message = 'Prénom :' . $_POST["prenom"] . "<br>
		Nom :" . $_POST["nom"] . "<br>
		Email :" . $_POST["email"] . "<br>
		Tél :" . $_POST["tel"] . "<br>
		Message :" . $_POST["story"] . "<br>
		Date :" . date('Y-m-d H:i:s') . "<br>";

                require 'phpmailer/PHPMailerAutoload.php';

                $mail = new PHPMailer();

                $mail->IsSMTP();
                $mail->Host = 'ssl0.ovh.net';  // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'info@myfrenchstartup.com';                 // SMTP username
                $mail->Password = '04Betterway';                           // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                               // TCP port to connect to

                $mail->setFrom('info@myfrenchstartup.com');
                $mail->addAddress('contact@myfrenchstartup.com');     // Add a recipient
                // $mail->addBCC('contact@myfrenchstartup.com');


                $mail->isHTML(true);                                  // Set email format to HTML

                $mail->Subject = "Demande de contact";

                $mail->Body = utf8_decode($message);

                if (!$mail->send()) {
                    echo 'Message could not be sent.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                } else {
                    
                }

                echo "<script>alert('Votre message a été envoyé avec succès.')</script>";
                echo "<script>window.location='contact'</script>";
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="formpage">
                    <form method="post"action="">
                        <div id="grid" class="formpage__content">
                            <div class="formpage__title">
                                <h1>Pour nous écrire,<br />veuillez remplir le formulaire ci-dessous.</h1>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Informations personnelles</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>Email</label>
                                        <input value="<?php echo $_SESSION['data_login']; ?>" class="form-control" type="email" name="email" id="email" required="" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Startup</label>
                                        <input type="text" name="societe" required=""  class="form-control" id="societe" value="<?php echo $_SESSION['startup_nom_revendiquer']; ?>" />
                                    </div>


                                </div>

                            </div>

                            <p class="small muted">
                            <h5 style="text-align:left;font-size:16px;color: #425466;line-height: 24px;margin-top: 16px;margin-bottom: 16px;">Demande de liaison</h5>
                            <div style="margin-top: 16px;">
                                <p style=line-height: 16px!important"><strong>Utilisez une adresse e-mail de votre établissement</strong> : lorsque vous indiquez une adresse e-mail de votre établissement au moment de votre demande de liaison, vos données sont vérifiées rapidement.
                                    <br><br>Si vous ne disposez pas d’une adresse e-mail de l’établissement ou si votre adresse ne peut être vérifiée, des justificatifs supplémentaires pourront vous être demandés.</p>

                            </div>
                            <h5 style="text-align:left;font-size:16px;color: #425466;line-height: 24px;margin-top: 16px;margin-bottom: 16px;">Les prochaines étapes</h5>
                            <p style="line-height: 24px!important">
                                1. Sélectionnez votre startup et saisissez votre adresse e-mail professionnelle<br> 
                                2. Vous recevez un e-mail de confirmation (sous 24/48h) avec un lien vers l’espace de mise à jour startup<br> 
                                3. Retrouvez votre startup dans votre Dashboard, éditez votre fiche et accédez à vos statistiques.<br> 
                            </p>                                     
                            <div style="text-align:center;margin-top: 16px;margin-left: 224px;margin-right: 224px;height: 38px;margin-bottom: 32px;">
                                <input type="button" onclick="revendiquer()" class="btn btn-primary" name="register" value="Valider" />
                            </div>   
                            </p>

                            
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
                                    (function (i, s, o, g, r, a, m) {
                                        i['GoogleAnalyticsObject'] = r;
                                        i[r] = i[r] || function () {
                                            (i[r].q = i[r].q || []).push(arguments)
                                        }, i[r].l = 1 * new Date();
                                        a = s.createElement(o),
                                                m = s.getElementsByTagName(o)[0];
                                        a.async = 1;
                                        a.src = g;
                                        m.parentNode.insertBefore(a, m)
                                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                                    ga('create', 'UA-36251023-1', 'auto');
                                    ga('send', 'pageview');
        </script>

        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function revendiquer() {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo URL ?>/revendiquer_sup.php",
                                                    success: function (o) {
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "<?php echo URL ?>/revendique_email.php",
                                                            success: function (o) {
                                                                document.getElementById("grid").innerHTML = o;
                                                            }

                                                        }
                                                        );
                                                    }

                                                }
                                                );
                                            }
        </script>
    </body>
</html>