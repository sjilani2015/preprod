<?php
include("include/db.php");
include("functions/functions.php");
include("config.php");

if (isset($_POST['startup'])) {
    $startup = addslashes($_POST['startup']);
    $id_startup = $_POST['id'];
    $juridique = addslashes($_POST['juridique']);
    $accroche = addslashes($_POST['accroche']);

    $siret = addslashes($_POST['siret']);
    if ($siret != '') {
        $siret1 = trim(str_replace(" ", "", $siret));
        $siret2 = substr($siret1, 0, 9);
        $siret_link = "http://www.verif.com/societe/" . $siret2;
    } else {
        $siret_link = "";
    }

    $email = addslashes($_POST['email']);
    $tel = addslashes($_POST['tel']);
    $creation = addslashes($_POST['creation']);
    if ($creation != '') {
        $tab_creation = explode("-", $creation);
        $date_creation = $creation;
        $annee_creation = $tab_creation[0];
    } else {
        $date_creation = "";
        $annee_creation = "";
    }
    $website = addslashes($_POST['website']);
    $facebook = addslashes($_POST['facebook']);
    $twitter = addslashes($_POST['twitter']);
    $linkedin = addslashes($_POST['linkedin']);
    $youtube = addslashes($_POST['youtube']);

    $secteur_val = addslashes($_POST['list_sector']);
    $marche = addslashes($_POST['list_marche']);

    $description = addslashes($_POST['long_fr']);
    $tags = addslashes($_POST['tags']);
    $adresse = addslashes($_POST['adresse']);
    $cp = addslashes($_POST['postal_code']);
    $ville = addslashes($_POST['locality']);
    $lat = addslashes($_POST['lat']);
    $lng = addslashes($_POST['lng']);
    $region = addslashes($_POST['administrative_area_level_1']);

    $target_path = "";

    if ($_FILES['logo']['name'] != '') {
        $target_path = "logo/";
        $favicon_path = "favicon/";
        /////
        $uid = uniqid();
        $tf = basename($_FILES['logo']['name']);

        $target_path = $target_path . $uid . str_replace(' ', '_', $startup) . '.' . pathinfo($tf, PATHINFO_EXTENSION);
        $favicon_path = $favicon_path . $uid . str_replace(' ', '_', $startup) . '.' . pathinfo($tf, PATHINFO_EXTENSION);
        // $chemin = "../logo/" .$uid.str_replace(' ','_',$startup).'.'.pathinfo($tf, PATHINFO_EXTENSION);

        if (move_uploaded_file($_FILES['logo']['tmp_name'], $target_path)) {
            mysqli_query($link, "update startup set logo='" . $target_path . "',favicon='" . $favicon_path . "' where id=" . $id_startup);
        } else {
            echo "";
        }
    }
    ///////////////

    mysqli_query($link, "update startup set "
            . "nom='" . $startup . "',"
            . "societe='" . $juridique . "',"
            . "url='" . $website . "',"
            . "creation='" . $annee_creation . "',"
            . "date_complete='" . $date_creation . "',"
            . "email='" . $email . "',"
            . "tel='" . $tel . "',"
            . "siret='" . $siret . "',"
            . "adresse='" . $adresse . "',"
            . "cp='" . $cp . "',"
            . "ville='" . $ville . "',"
            . "lat='" . $lat . "',"
            . "lng='" . $lng . "',"
            . "short_fr='" . $accroche . "',"
            . "long_fr='" . $description . "',"
            . "twitter='" . $twitter . "',"
            . "youtube='" . $youtube . "',"
            . "facebook='" . $facebook . "',"
            . "linkedin='" . $linkedin . "',"
            . "region='" . $region . "',"
            . "verif_link='" . $siret_link . "' "
            . "  where id=" . $id_startup);

    mysqli_query($link, "insert into historique(dt,user,module,id_startup)values('" . date('Y-m-d H:i:s') . "','" . $_SESSION['data_login'] . "','Startup modifiée du Front'," . $id_startup . ")");

    $verif_secteur = mysqli_num_rows(mysqli_query($link, "select * from activite where id_startup=" . $id_startup));
    if ($verif_secteur > 0) {
        mysqli_query($link, "update  activite set secteur=" . $secteur_val . ", activite=" . $marche . " where id_startup=" . $id_startup);
    } else {
        mysqli_query($link, "insert into activite(id_startup,secteur,activite)values(" . $id_startup . "," . $secteur_val . "," . $marche . ")");
    }

    $prenoms_list = $_POST["prenom_founder"];
    $noms_list = $_POST["nom_founder"];
    $fonction_list = $_POST["fonction"];
    $linkedin_list = $_POST["linkedin_founder"];
    $id_list = $_POST["id_founder"];
    $email_list = $_POST["email_founder"];
    $tel_list = $_POST["tel_founder"];

    foreach ($noms_list as $key => $investitem) {
        $prenom_co = addslashes($prenoms_list[$key]);
        $nom_co = addslashes($noms_list[$key]);
        $fct_co = addslashes($fonction_list[$key]);
        $linkedin_co = addslashes($linkedin_list[$key]);
        $email_co = addslashes($email_list[$key]);
        $tel_co = addslashes($tel_list[$key]);
        $id_co = addslashes($id_list[$key]);

        if (($nom_co != '') || ($fct_co != '') || ($linkedin_co != '')) {
            mysqli_query($link, "update personnes set nom='" . $nom_co . "',prenom='" . $prenom_co . "',fonction='" . $fct_co . "',linkedin='" . $linkedin_co . "',email='" . $email_co . "',tel='" . $tel_co . "' where id=" . $id_co)or die(mysqli_error($link));
        }
    }
} else {

    echo "<script>window.location='" . url . "nos-offres'</script>";
    exit();
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Startup modifiée - <?= SITENAME; ?></title>
        <meta name="description" content="">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="edito">


                    <h2>Votre startup a été modifiée</h2>
                    <p> Merci pour votre collaboration.<br>
                        Afin de garantir la fiabilité des données, toutes les startups ajoutées sont soumises à la validation d'un Data Manager.<br>Votre Startup sera validée d'ici 24/48 h.<br><br>Merci pour votre compréhension,<br>L'équipe MyFrenchStartup</div>
                </p>

            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/animated.min.js"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>