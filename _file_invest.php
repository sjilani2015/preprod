<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');


//if(!isset($_SESSION['data_login'])){
//  echo "<script>window.location='".url."nos-offres'</script>";
//}
$monUrl = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$_SESSION['utm_source'] = $monUrl;


$code = $_GET["id"];
$id_invest = degenerate_id($code);
$row_invest_info = mysqli_fetch_array(mysqli_query($link, "select * from new_name_list where id=" . $id_invest));
$row_invest_france = mysqli_fetch_array(mysqli_query($link, "select * from new_name_list_fr where id=1"));

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}

$date1 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d'), date('y')));
$date2 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d') - 30, date('y')));
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">

        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/maps.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/franceLow.min.js"></script>
        <script>am4core.addLicense("CH303325752");</script>
        <script>
            const   themeColorBlue = am4core.color('#00bff0'),
                    themeColorFill = am4core.color('#E1F2FA'),
                    themeColorGrey = am4core.color('#dadada'),
                    themeColorLightGrey = am4core.color('#F5F5F5'),
                    themeColorDarkgrey = am4core.color("#33333A"),
                    themeColorLine = am4core.color("#87D6E3"),
                    themeColorLineRed = am4core.color("#FF7C7C"),
                    themeColorLineGreen = am4core.color("#21D59B"),
                    themeColorColumns = am4core.color("#E1F2FA"),
                    labelColor = am4core.color('#798a9a');
        </script>
        <style>
            a{
                color:#323239;
            }
        </style>
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">
                    <div class="banner-partner">
                        <a href="https://www.myfrenchstartup.com/channel/" target="_blank"> <img src="https://www.myfrenchstartup.com/images/banner.png" alt=""></a>
                    </div>

                    <section class="module">
                        <div class="module__title"><?php echo utf8_encode(stripslashes(strtoupper($row_invest_info['new_name']))); ?></div>
                        <div class="ctg ctg--1_3">
                            <aside class="bloc text-center">
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Total investi</div>
                                    <div class="datasbloc__val">
                                        <?php echo str_replace(",0", "", number_format($row_invest_info['totale'] / 1000, 1, ",", " ")); ?> M€
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Principaux secteurs</div>
                                    <div class="datasbloc__val">
                                        <?php
                                        $principaux_secteurs = explode(',', $row_invest_info['principaux_secteurs']);
                                        for ($i = 0; $i < 2; $i++) {
                                            $secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur where id=" . $principaux_secteurs[$i]));
                                            ?>
                                            <?php echo ($secteur['nom_secteur']); ?><br>                      
                                            <?php
                                        }
                                        ?>

                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Principales régions d'investissement </div>
                                    <div class="datasbloc__val">
                                        <?php
                                        $principaux_regions = explode(',', $row_invest_info['principales_regions']);
                                        for ($i = 0; $i < 2; $i++) {
                                            $reg = mysqli_fetch_array(mysqli_query($link, "select * from region_new where id=" . $principaux_regions[$i]));
                                            ?>
                                            <?php echo ($reg['region_new']); ?><br>                      
                                            <?php
                                        }
                                        ?>

                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Investissement moyen</div>
                                    <div class="datasbloc__val"><?php echo str_replace(',0', '', number_format($row_invest_info['moyenne'] / 1000, 1, ",", " ")) ?> M€ </div>
                                </div>

                            </aside>

                            <div class="bloc">
                                <div class="ctg ctg--2_2 ctg--fullheight">
                                    <div class="chart chart--radar">
                                        <div id="chart-radar"></div>
                                    </div>                                    
                                    <script>
                                        am4core.ready(function () {


                                        /* Create chart instance */
                                        var chart = am4core.create("chart-radar", am4charts.RadarChart);
                                        /* Add data */
                                        chart.data = [{
                                        "date": "A",
                                                "value": <?php echo $row_invest_info['ranking_radar']; ?>,
                                                "value2": <?php echo number_format($row_invest_france['ranking_fr_radar'], 1, ".", ""); ?>
                                        }, {
                                        "date": "B",
                                                "value":  <?php echo $row_invest_info['moyenne_radar']; ?>,
                                                "value2":<?php echo number_format($row_invest_france['moyenne_fr_radar'], 1, ".", ""); ?>
                                        }, {
                                        "date": "C",
                                                "value": <?php echo $row_invest_info['participation_radar']; ?>,
                                                "value2": <?php echo number_format($row_invest_france['participation_fr_radar'], 1, ".", ""); ?>
                                        }, {
                                        "date": "D",
                                                "value": <?php echo $row_invest_info['invest_par_an_radar']; ?>,
                                                "value2": <?php echo number_format($row_invest_france['invest_par_an_fr_radar'], 1, ".", ""); ?>
                                        }, {
                                        "date": "E",
                                                "value": <?php echo $row_invest_info['co_invest_radar']; ?>,
                                                "value2": <?php echo number_format($row_invest_france['co_invest_fr_radar'], 1, ".", ""); ?>
                                        }, {
                                        "date": "F",
                                                "value": <?php echo $row_invest_info['maturite_startup_radar']; ?>,
                                                "value2": <?php echo number_format($row_invest_france['maturite_startup_fr_radar'], 1, ".", ""); ?>
                                        }];
                                        chart.paddingTop = 0;
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;
                                        /* Create axes */
                                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.dataFields.category = "date";
                                        categoryAxis.renderer.ticks.template.strokeWidth = 2;
                                        categoryAxis.renderer.labels.template.fontSize = 11;
                                        categoryAxis.renderer.labels.template.fill = labelColor;
                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
                                        valueAxis.renderer.axisFills.template.fillOpacity = 0.05;
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.renderer.gridType = "polygons"


                                                /* Create and configure series */
                                                        function createSeries(field, name, themeColor) {
                                                        var series = chart.series.push(new am4charts.RadarSeries());
                                                        series.dataFields.valueY = field;
                                                        series.dataFields.categoryX = "date";
                                                        series.strokeWidth = 2;
                                                        series.fill = themeColor;
                                                        series.fillOpacity = 0.3;
                                                        series.stroke = themeColor;
                                                        series.name = name;
                                                        // Show bullets ?
                                                        //let circleBullet = series.bullets.push(new am4charts.CircleBullet());
                                                        //circleBullet.circle.stroke = am4core.color('#fff');
                                                        //circleBullet.circle.strokeWidth = 2;
                                                        }


                                                createSeries("value", "Investisseur", themeColorBlue);
                                                createSeries("value2", "France", themeColorGrey);
                                                chart.legend = new am4charts.Legend();
                                                chart.legend.position = "top";
                                                chart.legend.useDefaultMarker = true;
                                                chart.legend.fontSize = "11";
                                                chart.legend.color = themeColorGrey;
                                                chart.legend.labels.template.fill = labelColor;
                                                chart.legend.labels.template.textDecoration = "none";
                                                chart.legend.valueLabels.template.textDecoration = "none";
                                                let as = chart.legend.labels.template.states.getKey("active");
                                                as.properties.textDecoration = "line-through";
                                                as.properties.fill = themeColorDarkgrey;
                                                let marker = chart.legend.markers.template.children.getIndex(0);
                                                marker.cornerRadius(12, 12, 12, 12);
                                                marker.width = 20;
                                                marker.height = 20;
                                                });
                                    </script>
                                    <div class="datagrid">
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">A :</span> Ranking investisseur
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $row_invest_info['ranking']); ?> 
                                                <?php
                                                if ($row_invest_info['ranking'] < $row_invest_france['ranking_fr']) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($row_invest_info['ranking'] > $row_invest_france['ranking_fr']) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($row_invest_info['ranking'] == $row_invest_france['ranking_fr']) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Médiane France <?php echo str_replace(",0", "", number_format($row_invest_france['ranking_fr'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">B :</span> Ticket moyen
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(",0", "", number_format($row_invest_info['moyenne'] / 1000, 1, ",", " ")); ?>  M€
                                                <?php
                                                if ($row_invest_info['moyenne'] > $row_invest_france['moyenne_fr']) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($row_invest_info['moyenne'] < $row_invest_france['moyenne_fr']) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($row_invest_info['moyenne'] == $row_invest_france['moyenne_fr']) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($row_invest_france['moyenne_fr'] / 1000, 1, ",", "")); ?> M€</div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">C :</span> Participations
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $row_invest_info['participation']); ?> 
                                                <?php
                                                if ($row_invest_info['participation'] > $row_invest_france['participation_fr']) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($row_invest_info['participation'] < $row_invest_france['participation_fr']) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($row_invest_info['participation'] == $row_invest_france['participation_fr']) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo number_format($row_invest_france['participation_fr'], 1, ",", ""); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">D :</span> Invest. par an
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $row_invest_info['invest_par_an']); ?> 
                                                <?php
                                                if ($row_invest_info['invest_par_an'] > $row_invest_france['invest_par_an_fr']) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($row_invest_info['invest_par_an'] < $row_invest_france['invest_par_an_fr']) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($row_invest_info['invest_par_an'] == $row_invest_france['invest_par_an_fr']) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo $row_invest_france['invest_par_an_fr']; ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">E :</span> Co-investissement
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $row_invest_info['co_invest']); ?> %
                                                <?php
                                                if ($row_invest_info['co_invest'] > $row_invest_france['co_invest_fr']) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($row_invest_info['co_invest'] < $row_invest_france['co_invest_fr']) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($row_invest_info['co_invest'] == $row_invest_france['co_invest_fr']) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(',0', '', number_format($row_invest_france['co_invest_fr'], 1, ",", "")); ?> %</div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">F :</span> Maturité startups
                                            </div>
                                            <div class="datagrid__val"><?php echo $row_invest_info['maturite_startup']; ?> 
                                                <?php
                                                if ($row_invest_info['maturite_startup'] > $row_invest_france['maturite_startup_fr']) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($row_invest_info['maturite_startup'] < $row_invest_france['maturite_startup_fr']) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($row_invest_info['maturite_startup'] == $row_invest_france['maturite_startup_fr']) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo number_format($row_invest_france['maturite_startup_fr'], 1, ",", ""); ?></div>
                                        </div>
                                    </div>

                                    <div class="bloc-actions">
                                        <div class="bloc-actions__ico">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Historique des financements</div>
                                <div class="chart chart--columns">
                                    <div id="chart-financement"></div>
                                </div>
                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                    var chart = am4core.create("chart-financement", am4charts.XYChart);
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    // will use this to store colors of the same items
                                    var colors = {};
                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "category";
                                    categoryAxis.renderer.minGridDistance = 10;
                                    categoryAxis.renderer.fontSize = "10px";
                                    categoryAxis.renderer.labels.template.fill = labelColor;
                                    categoryAxis.dataItems.template.text = "{realName}";
                                    categoryAxis.renderer.grid.disabled = true;
                                    // Title Bottom
                                    categoryAxis.title.text = "Années";
                                    categoryAxis.title.rotation = 0;
                                    categoryAxis.title.align = "center";
                                    categoryAxis.title.valign = "bottom";
                                    categoryAxis.title.dy = - 5;
                                    categoryAxis.title.fontSize = 11;
                                    categoryAxis.title.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    // Title left
                                    valueAxis.title.text = "Nombre de startups financées";
                                    valueAxis.title.rotation = - 90;
                                    valueAxis.title.align = "left";
                                    valueAxis.title.valign = "top";
                                    valueAxis.title.dy = 0;
                                    valueAxis.title.fontSize = 11;
                                    valueAxis.title.fill = labelColor;
                                    // single column series for all data
                                    var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                    columnSeries.fill = themeColorColumns;
                                    columnSeries.stroke = 0;
                                    columnSeries.columns.template.width = am4core.percent(80);
                                    columnSeries.dataFields.categoryX = "category";
                                    columnSeries.dataFields.valueY = "value";
                                    // second value axis for quantity
                                    var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis2.renderer.opposite = true;
                                    valueAxis2.syncWithAxis = valueAxis;
                                    valueAxis2.tooltip.disabled = true;
                                    valueAxis2.renderer.labels.template.fontSize = 11;
                                    valueAxis2.renderer.labels.template.fill = labelColor;
                                    // Title right
                                    valueAxis2.title.text = "Financement total (M€)";
                                    valueAxis2.title.rotation = - 90;
                                    valueAxis2.title.align = "left";
                                    valueAxis2.title.valign = "top";
                                    valueAxis2.title.dy = 0;
                                    valueAxis2.title.fontSize = 11;
                                    valueAxis2.title.fill = labelColor;
                                    // quantity line series
                                    var lineSeries = chart.series.push(new am4charts.LineSeries());
                                    lineSeries.dataFields.categoryX = "category";
                                    lineSeries.dataFields.valueY = "financement";
                                    lineSeries.yAxis = valueAxis2;
                                    lineSeries.bullets.push(new am4charts.CircleBullet());
                                    lineSeries.fill = themeColorLine;
                                    lineSeries.strokeWidth = 2;
                                    lineSeries.fillOpacity = 0;
                                    lineSeries.stroke = themeColorLine;
                                    // when data validated, adjust location of data item based on count
                                    lineSeries.events.on("datavalidated", function () {
                                    lineSeries.dataItems.each(function (dataItem) {
                                    // if count divides by two, location is 0 (on the grid)
                                    if (dataItem.dataContext.count / 2 == Math.round(dataItem.dataContext.count / 2)) {
                                    dataItem.setLocation("categoryX", 0);
                                    }
                                    // otherwise location is 0.5 (middle)
                                    else {
                                    dataItem.setLocation("categoryX", 0.5);
                                    }
                                    })
                                    })

                                            ///// DATA
                                            var chartData = [];
                                    var lineSeriesData = [];
                                    var data =
                                    {
<?php
for ($i = 2014; $i <= date('Y'); $i++) {
    $nbr_somme = 0;
    $nbr_tot = 0;
    $sql = mysqli_fetch_array(mysqli_query($link, "select investisseur from de_list where new_name ='" . $row_invest_info['new_name'] . "' limit 1"));
    $verif_somme = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf where de like '%" . $sql['investisseur'] . "%' and rachat=0 and year(date_ajout)='" . $i . "'"));
    $nbr_somme = $nbr_somme + $verif_somme['somme'];

    $verif_nb = mysqli_num_rows(mysqli_query($link, "select id from lf where de like '%" . $sql['investisseur'] . "%' and rachat=0 and year(date_ajout)='" . $i . "'"));
    $nbr_tot = $nbr_tot + $verif_nb;
    ?>
                                        "<?php echo $i; ?>": {
                                        "<?php echo $i; ?>": <?php echo $nbr_tot; ?>,
                                                "financement": <?php echo $nbr_somme / 1000; ?>
                                        },
    <?php
}
?>

                                    }

                                    // process data and prepare it for the chart
                                    for (var providerName in data) {
                                    var providerData = data[providerName];
                                    // add data of one provider to temp array
                                    var tempArray = [];
                                    var count = 0;
                                    // add items
                                    for (var itemName in providerData) {
                                    if (itemName != "financement") {
                                    count++;
                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                    }
                                    }
                                    // sort temp array
                                    tempArray.sort(function (a, b) {
                                    if (a.value > b.value) {
                                    return 1;
                                    } else if (a.value < b.value) {
                                    return - 1
                                    } else {
                                    return 0;
                                    }
                                    })

                                            // add quantity and count to middle data item (line series uses it)
                                            var lineSeriesDataIndex = Math.floor(count / 2);
                                    tempArray[lineSeriesDataIndex].financement = providerData.financement;
                                    tempArray[lineSeriesDataIndex].count = count;
                                    // push to the final data
                                    am4core.array.each(tempArray, function (item) {
                                    chartData.push(item);
                                    })
                                    }

                                    valueAxis.renderer.grid.template.strokeWidth = 0;
                                    //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                    categoryAxis.renderer.grid.template.strokeWidth = 0;
                                    chart.data = chartData;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Maturité des startups financées</div>
                                <div class="chart chart--columns">
                                    <div id="chart-maturite"></div>
                                </div>
                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                    var chart = am4core.create("chart-maturite", am4charts.XYChart);
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    // will use this to store colors of the same items
                                    var colors = {};
                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "category";
                                    categoryAxis.renderer.minGridDistance = 10;
                                    categoryAxis.renderer.fontSize = "10px";
                                    categoryAxis.renderer.labels.template.fill = labelColor;
                                    categoryAxis.dataItems.template.text = "{realName}";
                                    categoryAxis.renderer.grid.disabled = true;
                                    // Title Bottom
                                    categoryAxis.title.text = "Années";
                                    categoryAxis.title.rotation = 0;
                                    categoryAxis.title.align = "center";
                                    categoryAxis.title.valign = "bottom";
                                    categoryAxis.title.dy = - 5;
                                    categoryAxis.title.fontSize = 11;
                                    categoryAxis.title.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    // Title left
                                    valueAxis.title.text = "Nombre de startups financées";
                                    valueAxis.title.rotation = - 90;
                                    valueAxis.title.align = "left";
                                    valueAxis.title.valign = "top";
                                    valueAxis.title.dy = 0;
                                    valueAxis.title.fontSize = 11;
                                    valueAxis.title.fill = labelColor;
                                    // single column series for all data
                                    var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                    columnSeries.fill = themeColorColumns;
                                    columnSeries.stroke = 0;
                                    columnSeries.columns.template.width = am4core.percent(80);
                                    columnSeries.dataFields.categoryX = "category";
                                    columnSeries.dataFields.valueY = "value";
                                    // second value axis for quantity
                                    var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis2.renderer.opposite = true;
                                    valueAxis2.syncWithAxis = valueAxis;
                                    valueAxis2.tooltip.disabled = true;
                                    valueAxis2.renderer.labels.template.fontSize = 11;
                                    valueAxis2.renderer.labels.template.fill = labelColor;
                                    // Title right
                                    valueAxis2.title.text = "Financement total (M€)";
                                    valueAxis2.title.rotation = - 90;
                                    valueAxis2.title.align = "left";
                                    valueAxis2.title.valign = "top";
                                    valueAxis2.title.dy = 0;
                                    valueAxis2.title.fontSize = 11;
                                    valueAxis2.title.fill = labelColor;
                                    // quantity line series
                                    var lineSeries = chart.series.push(new am4charts.LineSeries());
                                    lineSeries.dataFields.categoryX = "category";
                                    lineSeries.dataFields.valueY = "financement";
                                    lineSeries.yAxis = valueAxis2;
                                    lineSeries.bullets.push(new am4charts.CircleBullet());
                                    lineSeries.fill = themeColorLine;
                                    lineSeries.strokeWidth = 2;
                                    lineSeries.fillOpacity = 0;
                                    lineSeries.stroke = themeColorLine;
                                    // when data validated, adjust location of data item based on count
                                    lineSeries.events.on("datavalidated", function () {
                                    lineSeries.dataItems.each(function (dataItem) {
                                    // if count divides by two, location is 0 (on the grid)
                                    if (dataItem.dataContext.count / 2 == Math.round(dataItem.dataContext.count / 2)) {
                                    dataItem.setLocation("categoryX", 0);
                                    }
                                    // otherwise location is 0.5 (middle)
                                    else {
                                    dataItem.setLocation("categoryX", 0.5);
                                    }
                                    })
                                    })

                                            ///// DATA
                                            var chartData = [];
                                    var lineSeriesData = [];
                                    var data =
                                    {

                                        "1 an": {
                                        "1 an": <?php echo $row_invest_info['nbr_sub_1']; ?>,
                                                "financement": <?php echo $row_invest_info['total_sub_1'] / 1000; ?>
                                        },
                                        "2 ans": {
                                        "2 ans": <?php echo $row_invest_info['nbr_sub_2']; ?>,
                                                "financement": <?php echo $row_invest_info['total_sub_2'] / 1000; ?>
                                        },
                                        "3 ans": {
                                        "3 ans": <?php echo $row_invest_info['nbr_sub_3']; ?>,
                                                "financement": <?php echo $row_invest_info['total_sub_3'] / 1000; ?>
                                        },
                                        "4 ans": {
                                        "4 ans": <?php echo $row_invest_info['nbr_sub_4']; ?>,
                                                "financement": <?php echo $row_invest_info['total_sub_4'] / 1000; ?>
                                        },
                                        "5 ans": {
                                        "5 ans": <?php echo $row_invest_info['nbr_sub_5']; ?>,
                                                "financement": <?php echo $row_invest_info['total_sub_5'] / 1000; ?>
                                        },
                                        "6 ans": {
                                        "6 ans": <?php echo $row_invest_info['nbr_sub_6']; ?>,
                                                "financement": <?php echo $row_invest_info['total_sub_6'] / 1000; ?>
                                        },
                                        "7 ans": {
                                        "7 ans": <?php echo $row_invest_info['nbr_sub_7']; ?>,
                                                "financement": <?php echo $row_invest_info['total_sub_7'] / 1000; ?>
                                        },
                                        "8 ans": {
                                        "8 ans": <?php echo $row_invest_info['nbr_sub_8']; ?>,
                                                "financement": <?php echo $row_invest_info['total_sub_8'] / 1000; ?>
                                        },
                                        "9 ans": {
                                        "9 ans": <?php echo $row_invest_info['nbr_sub_9']; ?>,
                                                "financement": <?php echo $row_invest_info['total_sub_9'] / 1000; ?>
                                        },
                                        "10 ans": {
                                        "10 ans": <?php echo $row_invest_info['nbr_sub_10']; ?>,
                                                "financement": <?php echo $row_invest_info['total_sub_10'] / 1000; ?>
                                        },


                                    }

                                    // process data and prepare it for the chart
                                    for (var providerName in data) {
                                    var providerData = data[providerName];
                                    // add data of one provider to temp array
                                    var tempArray = [];
                                    var count = 0;
                                    // add items
                                    for (var itemName in providerData) {
                                    if (itemName != "financement") {
                                    count++;
                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                    }
                                    }
                                    // sort temp array
                                    tempArray.sort(function (a, b) {
                                    if (a.value > b.value) {
                                    return 1;
                                    } else if (a.value < b.value) {
                                    return - 1
                                    } else {
                                    return 0;
                                    }
                                    })

                                            // add quantity and count to middle data item (line series uses it)
                                            var lineSeriesDataIndex = Math.floor(count / 2);
                                    tempArray[lineSeriesDataIndex].financement = providerData.financement;
                                    tempArray[lineSeriesDataIndex].count = count;
                                    // push to the final data
                                    am4core.array.each(tempArray, function (item) {
                                    chartData.push(item);
                                    })
                                    }

                                    valueAxis.renderer.grid.template.strokeWidth = 0;
                                    //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                    categoryAxis.renderer.grid.template.strokeWidth = 0;
                                    chart.data = chartData;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                        </div>
                    </section>
                    <section class="module">

                        <div class="ctg ctg--3_3">
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Participation seul</div>
                                        <div class="datasbloc__key">Périmètre d’investissement </div>
                                        <div class="datasbloc__val nowrap">Moy. <?php echo str_replace(",0", "", number_format($row_invest_info['moy_seul'] / 1000, 1, ",", " ")) ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round(100 - $row_invest_info['co_invest']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($row_invest_info['min_seul'] / 1000, 1, ",", " ")) ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($row_invest_info['max_seul'] / 1000, 1, ",", " ")) ?> M€ </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Co-investissements</div>
                                        <div class="datasbloc__key">Dimension des deals</div>
                                        <div class="datasbloc__val nowrap">Moy. <?php echo str_replace(",0", "", number_format($row_invest_info['moy_co_invest'] / 1000, 1, ",", " ")) ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($row_invest_info['co_invest']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($row_invest_info['min_co_invest'] / 1000, 1, ",", " ")) ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($row_invest_info['max_co_invest'] / 1000, 1, ",", " ")) ?> M€</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Nombre d’investissements</div>
                                        <div class="datasbloc__key">Par an</div>
                                        <div class="datasbloc__val nowrap">Moy. <?php echo str_replace(",0", "", number_format($row_invest_info['moy_annee_invest'], 1, ",", " ")) ?> par an</div> 
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($row_invest_info['nb_3']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($row_invest_info['min_annee_invest'], 1, ",", " ")) ?> </div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($row_invest_info['max_annee_invest'], 1, ",", " ")) ?> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Investissements par secteur</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="moneyCell text-center">Invest.<br>Réalisés</th>
                                                <th class="dateCell text-center">% portefeuille</th>
                                                <th class="dateCell text-center">% invest.<br>France</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_secteur = mysqli_query($link, "select * from new_name_list_secteur where id_new_name_list=" . $id_invest . " order by id");
                                            while ($principaux_secteurs1 = mysqli_fetch_array($sql_secteur)) {
                                                $secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur where id=" . $principaux_secteurs1['id_secteur']));
                                                ?>
                                                <tr>
                                                    <td class=" moneyCell">
                                                        <?php echo $secteur['nom_secteur']; ?>
                                                    </td>
                                                    <td class="text-center moneyCell"><?php echo $principaux_secteurs1['investissements'] ?></td>
                                                    <td class="text-center dateCell"><?php echo str_replace(",0", "", number_format($principaux_secteurs1['portefeuille'], 1, ",", "")) ?>%</td>
                                                    <td class="text-center dateCell"><?php echo str_replace(",0", "", number_format($principaux_secteurs1['perc_france'], 1, ",", "")) ?>%</td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Investissements par région</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="moneyCell text-center">Invest.<br>Réalisés</th>
                                                <th class="dateCell text-center">% portefeuille</th>
                                                <th class="dateCell text-center">% invest.<br>France</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_regionn = mysqli_query($link, "select * from new_name_list_region where id_new_name_list=" . $id_invest . " order by id");
                                            while ($principaux_reg1 = mysqli_fetch_array($sql_regionn)) {
                                                $reg1 = mysqli_fetch_array(mysqli_query($link, "select * from region_new where id=" . $principaux_reg1['id_region']));
                                                ?>
                                                <tr>
                                                    <td class=" moneyCell">
                                                        <?php echo $reg1['region_new']; ?>
                                                    </td>
                                                    <td class="text-center moneyCell"><?php echo $principaux_reg1['investissements'] ?></td>
                                                    <td class="text-center dateCell"><?php echo str_replace(",0", "", number_format($principaux_reg1['portefeuille'], 1, ",", "")) ?>%</td>
                                                    <td class="text-center dateCell"><?php echo str_replace(",0", "", number_format($principaux_reg1['perc_france'], 1, ",", "")) ?>%</td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="bloc">
                            <div class="bloc__title">Derniers financements <?php echo utf8_encode(stripslashes(strtoupper($row_invest_info['new_name']))); ?></div>
                            <div class="table-responsive">
                                <table class="table table-search">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Startups</th>
                                            <th>Région</th>
                                            <th>Secteur</th>
                                            <th>Montant (M€)</th>
                                            <th>Date</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $row_startup_investisseur = mysqli_query($link, "select * from startup_investisseur where id_investisseur=" . $row_invest_info['id'] . " order by date_ajout desc limit 20");
                                        while ($startup_invest = mysqli_fetch_array($row_startup_investisseur)) {
                                            $id_startup = $startup_invest["id_startup"];
                                            $startup = mysqli_fetch_array(mysqli_query($link, "select * from startup  where startup.id=" . $id_startup));
                                            $secteurs = mysqli_fetch_array(mysqli_query($link, "Select  secteur.nom_secteur,secteur.id,secteur.nom_secteur_en From   secteur Inner Join  activite    On activite.secteur = secteur.id Where  activite.id_startup =" . $id_startup));
                                            $ssecteurs = mysqli_fetch_array(mysqli_query($link, "Select  sous_secteur.nom_sous_secteur,sous_secteur.id,sous_secteur.nom_sous_secteur_en From   sous_secteur Inner Join  activite    On activite.sous_secteur = sous_secteur.id Where  activite.id_startup =" . $id_startup));
                                            ?>
                                            <tr>
                                                <td class="table-search__startup">

                                                    <div class="startupInfos__image">
                                                        <a href="<?php echo URL . 'fr/startup-france/' . generate_id($startup['id']) . "/" . urlWriting(strtolower($startup["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $startup['logo']) ?>" alt="<?php echo "Startup " . $startup["nom"]; ?>"></a>
                                                    </div>


                                                </td>
                                                <td class="table-search__position center">

                                                    <div class="startupInfos__desc">
                                                        <div class="companyName"><a href="<?php echo URL . 'fr/startup-france/' . generate_id($startup['id']) . "/" . urlWriting(strtolower($startup["nom"])) ?>" target="_blank"><?php echo stripslashes($startup['nom']); ?></a></div>
                                                        <div class="companyLabel"><?php echo stripslashes($startup['short_fr']); ?></div>
                                                    </div>

                                                </td>
                                                <td class="table-search__position center"><?php echo str_replace(array("Paris - ", "\\"), array("", ""), ($startup['region_new'])); ?></td>
                                                <td class="table-search__position center"><a style="cursor: pointer" onclick="afficher_par_secteur(<?php echo $secteurs['id'] ?>, '<?php echo nottoie($secteurs['nom_secteur']) ?>')"><?php echo ($secteurs['nom_secteur']) ?></a></td>

                                                <td class="table-search__totalfondsleves center"><?php
                                                    if ($startup_invest['montant'] == "0")
                                                        echo 'NC';
                                                    else
                                                        echo str_replace(",0", "", number_format($startup_invest['montant'] / 1000, 1, ',', ' '));
                                                    ?></td>

                                                <td class="table-search__datedernierelevee center"><?php echo change_date_fr_chaine_related($startup_invest['date_ajout']); ?></td>

                                            </tr>

                                            <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="bloc">
                            <div class="bloc__title">Co-investisseurs <?php echo utf8_encode(stripslashes(strtoupper($row_invest_info['new_name']))); ?></div>
                            <div class="table-responsive">
                                <table class="table table-search">
                                    <thead>
                                        <tr>
                                            <th>Investisseurs</th>
                                            <th>Nombre de participations</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $verif_co1 = mysqli_query($link, "select  id_co_investisseur,nbr_co_invest from investisseur_co_investisseur where id_investisseur=" . $id_invest . " order by nbr_co_invest desc limit 20");
                                        while ($data_invest = mysqli_fetch_array($verif_co1)) {
                                            $iii = mysqli_fetch_array(mysqli_query($link, "select * from new_name_list where id='" . $data_invest["id_co_investisseur"] . "'"));
                                            ?>
                                            <tr>

                                                <td class="table-search__position center"><a href="<?php echo url; ?>startups-investisseur/<?php echo generate_id($iii['id']) ?>/<?php echo urlWriting($iii['new_name']) ?>" target="_blank"><?php echo strtoupper($iii["new_name"]); ?></a></td>
                                                <td class="table-search__position center"><?php echo $data_invest['nbr_co_invest']; ?></td>


                                            </tr>

                                            <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>



                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script type="module">
            // https://github.com/fiduswriter/Simple-DataTables/wiki/columns
            import {DataTable} from "<?= JS_PATH; ?>tablefilter.min.js"
            const table = new DataTable("table", {
            "searchable": false,
            "nextPrev": false,
            "paging": false,
            //"fixedColumns": false,

            });

            let columns = table.columns();


        </script>
        <script>
                                                (function (i, s, o, g, r, a, m) {
                                                i['GoogleAnalyticsObject'] = r;
                                                i[r] = i[r] || function () {
                                                (i[r].q = i[r].q || []).push(arguments)
                                                }, i[r].l = 1 * new Date();
                                                a = s.createElement(o),
                                                        m = s.getElementsByTagName(o)[0];
                                                a.async = 1;
                                                a.src = g;
                                                m.parentNode.insertBefore(a, m)
                                                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                                ga('create', 'UA-36251023-1', 'auto');
                                                ga('send', 'pageview');
        </script>
    </body>
</html>