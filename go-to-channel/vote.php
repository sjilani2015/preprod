<?php
include("../include/db.php");
include("../functions/functions.php");
$ip = $_SERVER['REMOTE_ADDR'];

$_SESSION['source'] = $_GET['utm_source'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Votez pour la meilleure startup dans 4 catégories</title>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="https://www.myfrenchstartup.com/myfs.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </head>
    <body background="#fdfcf8" style="background-color:#fdfcf8 ">
        <style type="text/css">

            b{
                margin-bottom: 5px !important;
            }
            .bg-bleu{
                background-color:  #3836ce;
            }
            .bg-gris{
                background-color:  #f2f2f2;
            }

            .text-noir{
                color: #040707;
            }
            .text-bleu{
                color: #3837d1;
            }
            .text-gris{
                color: #a0a0a0;
            }
            .cnt {
                position: relative;
                text-align: center;
                color: white;
            }
            h1{
                color:  #040707;
                font-size: 25px;
                line-height: 32px;
                font-weight: bold;
                padding-top: 20px
            }
            h2{
                font-size: 30px;
                line-height: 34px;
                font-weight: bold;
            }
            h3{
                font-size: 20px;
                line-height: 24px;
                font-weight: bold;
            }
            p{
                font-size: 15px;
                line-height: 19px;
            }
            .bt{
                background: #3837d1;
                color: white;
                font-size: 25px;
                line-height: 29px;
                padding: 10px 30px;
            }

            a:hover {
                color: #fff;
                text-decoration: none;

            }

            @media (min-width: 992px){

                .m-740{
                    max-width: 740px !important;
                }


                .bottom-centered {
                    position: absolute;
                    bottom: 20px;
                    left: 50%;
                    transform: translate(-50%, -50%);
                }

                .centered {
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                }

                h1{
                    color: #fff;
                    text-align: right;
                }
            }




            /*  
             * Rating styles
             */
            .rating {
                width: 100%;
                /* margin: 0 auto 0em;*/
                font-size: 30px;
                overflow:hidden;
            }
            .rating a {
                float:right;
                color: #aaa;
                text-decoration: none;
                -webkit-transition: color .4s;
                -moz-transition: color .4s;
                -o-transition: color .4s;
                transition: color .4s;
            }
            .rating a:hover,
            .rating a:hover ~ a
            {
                color: orange !important;
                cursor: pointer;
            }
            .rating2 {
                direction: rtl;
            }
            .rating2 a {
                float:none
            }
        </style>
        <header>
            <div class="container-fluid">
                <div class="cnt">
                    <img src="images/bg.jpg" alt="GoToChannel 2021" style="width:100%;">
                    <div class="centered"><h1  ><b>2021</b> <br> Programme Startups IT - 1ère édition</h1></div>
                    <h3 class="text-bleu bottom-centered" >Votez pour la meilleure startup dans 4 catégories </h3>
                </div>
            </div>
        </header>

        <div class="container-fluid" >
            <div class="row justify-content-center">
                <div class="col-md-2 m-2">
                    <img class="mx-auto d-block" src="images/part1.png">
                </div>
                <div class="col-md-2 m-2">
                    <img class="mx-auto d-block" src="images/part2.png">
                </div>
                <div class="col-md-2 m-2">
                    <img class="mx-auto d-block" src="images/part3.png">
                </div>
                <div class="col-md-2 m-2">
                    <img class="mx-auto d-block" src="images/part4.png">
                </div>
                <div class="col-md-2 m-2">
                    <img class="mx-auto d-block" src="images/part5.png">
                </div>
            </div>
        </div>



        <main>
            <section class="bg-bleu p-5 mt-2 bg-gris"  >
                <div class="container">
                    <div class="row">
                        <div class="col-12"> 
                            <p class="text-center text-noir" style="color: #3837d1;font-size: 2.5em;font-weight: bold;line-height: 70px;">
                                Catégorie DATA
                            </p>

                        </div>
                    </div>
                </div>
            </section> 
            <section class="p-5 mt-5">
                <div class="">
                    <div class="row">
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/DATACOOK.png" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>datacook.io</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">DATACOOK</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Plateforme de segmentation clients, compatible Salesforce et autres CRM. </span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating1_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=1 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="1_5" <?php if ($rating1_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="1_4"  <?php if ($rating1_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="1_3"  <?php if ($rating1_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="1_2"  <?php if ($rating1_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="1_1"  <?php if ($rating1_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal1" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">DATACOOK</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société :</b> DATACOOK (raison sociale : FIDELIZ) <br>
                                                        <b>Solution concernée :</b> Plateforme de segmentation clients, compatible Salesforce et autres CRM. Elle permet de piloter au ROI les actions marketing en priorisant les cibles et investissements. Propose des bilans concrets, chiffrés et efficaces des opérations menées, pour motiver les équipes, les challenger et renforcer l’agilité de leur organisation. 
                                                        <br><b>Fondateurs :</b> Baptiste PIGAUX 
                                                        <br> <b>Site web :</b> www.datacook.io
                                                        <br> <b>Modèle :</b> licence annuelle SaaS 
                                                        <br> <b>Description :</b> Prochaine étape de développement : nouvelle version de la solution incluant la fonction automatisée de Next Best Action. 
                                                        <br> <b>Maturité :</b> Nous avons déjà des clients Grands Comptes
                                                        <br> <b>Lien Youtube : </b>
                                                        <iframe width="70%" height="315" src="https://www.youtube.com/embed/ux198qi0xGM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/2_Logo-800x800.jpg" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>woofer.io</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">WOOFER</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Une application pour une expérience collaborateur unique intégrant qualité de vie au travail, communication interne, et partage de savoirs. </span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating2_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=2 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="2_5" <?php if ($rating2_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="2_4"  <?php if ($rating2_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="2_3"  <?php if ($rating2_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="2_2"  <?php if ($rating2_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="2_1"  <?php if ($rating2_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal2" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">WOOFER</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société :</b> Woofer 
                                                        <br><b>Solution concernée :</b> Une application pour une expérience collaborateur unique intégrant qualité de vie au travail, communication interne, et partage de savoirs. 
                                                        <br><b>Les fonctionnalités :</b> Baromètre social, eNPs, enquêtes, feedback, signalement harcèlement/discrimination, bibliothèque de contenus inspirants et engagés. 
                                                        <br><b>Fondateurs :</b> Elyes BOUTELDJA, Stéphanie LANGLOIS, Black Meridian 
                                                        <br><b>Site web :</b> https://www.woofer.io/ 
                                                        <br><b>Modèle :</b> Abonnement et services de conseil
                                                        <br><b>Description :</b> Lancement réalisé début mai. Objectif : disposer d’une visibilité accrue, développement de partenariats distributeurs et intégrateurs.  
                                                        <br><b>Maturité :</b> inbound et outbound 

                                                        <br> <b>Lien Youtube : </b>
                                                        <iframe width="70%" height="315" src="https://www.youtube.com/embed/OgCGyksjCEo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>





                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/logo_black_purple.jpg" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>feedier.com/fr</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading" style="text-transform: uppercase">FEEDIER</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Technologie qui permet de mesurer la qualité de l’expérience, d’analyser les feedbacks et de créer des actions personnalisées pour transformer les retours en un avantage compétitif.</span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating8_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=8 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="8_5" <?php if ($rating8_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="8_4"  <?php if ($rating8_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="8_3"  <?php if ($rating8_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="8_2"  <?php if ($rating8_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="8_1"  <?php if ($rating8_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal8" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal8" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel" style="text-transform: uppercase">FEEDIER</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b> Nom société :</b> FEEDIER 
                                                        <br><b>Solution concernée :</b> Technologie qui permet de mesurer la qualité de l’expérience, d’analyser les feedbacks et de créer des actions personnalisées pour transformer les retours des clients, utilisateurs et employés en un avantage compétitif. 
                                                        <br><b>Fondateurs :</b> François Forest 
                                                        <br><b>Site web :</b> feedier.com/fr
                                                        <br><b>Modèle :</b> B2B SaaS avec engagements annuels 
                                                        <br><b>Description :</b> Amélioration du nombre de connecteurs (Salesforce, Microsoft Dynamics, SAP, etc.) Amélioration du module d’analyse sémantique (NLP) grâce au machine learning. Ajout d’un module de data governance pour l’accès aux feedbacks. 
                                                        <br><b>Maturité :</b> Vente directe mis en place (inbound et outbound) 
                                                        <br><b>  Lien Youtube :</b> 
                                                        <iframe width="70%"  height="315" src="https://www.youtube.com/embed/yANzAX5WY6s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                                        ·    
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/Dastra-Logo.jpg" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>dastra.eu</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading" style="text-transform: uppercase">Dastra</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Dastra, une startup LegalTech qui aide les délégués à la protection des données (ou Data Protection Officer, DPO) </span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating9_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=9 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="9_5" <?php if ($rating9_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="9_4"  <?php if ($rating9_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="9_3"  <?php if ($rating9_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="9_2"  <?php if ($rating9_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="9_1"  <?php if ($rating9_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal9" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal9" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel" style="text-transform: uppercase">Dastra</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b> Nom société :</b> Dastra 
                                                        <br><b> Solution concernée :</b> Dastra, une startup LegalTech qui aide les délégués à la protection des données (ou Data Protection Officer, DPO) dans les entreprises à simplifier la mise en œuvre et le suivi de la conformité RGPD à travers un logiciel SaaS collaboratif disponible en plug-play et pouvant être testé avec une offre gratuite.
                                                        <br><b>  Fondateurs :</b> Paul-Emmanuel Bidault 
                                                        <br><b>   Site web :</b> www.dastra.eu 
                                                        <br><b>   Modèle :</b></b> SaaS B2B 
                                                        <br><b>   Description :</b> Accélérer les ventes en recrutant davantage de commerciaux et des partenaires revendeurs. Faire grandir l’équipe et atteindre la maturité produit – services en recrutant des développeurs et des juristes. Préparer l’évolutivité de la plateforme. Se lancer à l’international. 
                                                        <br><b>   Maturité :</b> Bien mature d’un point de vue commercial à la fois en ventes directe et indirecte, Dastra cherche des partenaires pour renforcer ses canaux de vente indirect déjà existant 
                                                        <br><b>   Lien Youtube :</b>
                                                        <iframe width="70%"  height="315" src="https://www.youtube.com/embed/Wry0ohs0zTA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>F
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/Logo-Retail-Shake-PNG.jpg" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>retailshake.com</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading" style="text-transform: uppercase">Retail Shake</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Retail Shake est un outil de veille à 360°, incluant de la veille tarifaire, comparaison des produits </span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating10_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=10 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="10_5" <?php if ($rating10_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="10_4"  <?php if ($rating10_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="10_3"  <?php if ($rating10_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="10_2"  <?php if ($rating10_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="10_1"  <?php if ($rating10_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal10" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal10" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel" style="text-transform: uppercase">Retail Shake</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b> Nom société :</b> Retail Shake 
                                                        <br><b> Solution concernée : </b>Retail Shake est un outil de veille à 360°, incluant de la veille tarifaire, comparaison des produits, benchmark prix ou surveillance des stocks géolocalisés. Le but de Retail Shake est d’optimiser les stocks de leurs clients, leur plan merchandising et leur stratégie marketing, tout en permettant de suivre les marques et les enseignes concurrentielles. 
                                                        <br><b>  Fondateurs : </b>Irwan Djoehana 
                                                        <br><b>   Site web :</b> www.retailshake.com 
                                                        <br><b>   Modèle : </b>B2B, SaaS, abonnements 
                                                        <br><b>   Description : </b>Nos perspectives de développement à 2-3 ans : Remporter de nouveaux appels d’offres nationaux, accélérer à l’international (Europe puis Amérique du Nord), automatiser la vente de nos abonnements et recruter dans toutes les équipes (Commerciale, marketing, technique). Objectif de CA fin 2022 : 1,9 M euro. 
                                                        <br><b>   Maturité :</b> déjà des clients nationaux + expansion Europe en cours avec country manager. 
                                                        <br><b>  Lien Youtube :</b>
                                                        <iframe width="70%"  height="315" src="https://www.youtube.com/embed/kw_o7nl30Lc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>F
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </section>
            <section class="bg-bleu p-5 mt-2 bg-gris"  >
                <div class="container">
                    <div class="row">
                        <div class="col-12"> 
                            <p class="text-center text-noir" style="color: #3837d1;font-size: 2.5em;font-weight: bold;line-height: 70px;">
                                Catégorie IA et IOT
                            </p>

                        </div>
                    </div>
                </div>
            </section>


            <section class="p-5 mt-5">
                <div class="">
                    <div class="row">
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/MAbSilico.png" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>mabsilico.com</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">MAbSilico</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Société française de deeptech qui conçoit et met en œuvre des solutions informatiques </span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating12_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=12 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="12_5" <?php if ($rating12_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="12_4"  <?php if ($rating12_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="12_3"  <?php if ($rating12_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="12_2"  <?php if ($rating12_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="12_1"  <?php if ($rating12_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal12" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal12" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">MAbSilico</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société :</b> MAbSilico 
                                                        <br><b>Solution concernée :</b> Société française de deeptech qui conçoit et met en œuvre des solutions informatiques pour la découverte et le développement d'anticorps thérapeutiques et de biomédicaments. 
                                                        <br><b> Fondateurs :</b>  Vincent Puard 
                                                        <br><b> Site web :</b>  https://www.mabsilico.com/ 
                                                        <br><b> Modèle :</b>  Saas / B2B 
                                                        <br><b>  Description :</b>  Société deeptech permettant d'accélérer et sécuriser la conception de médicaments anticorps contre le cancer en utilisant des solutions d'IA et de ML commercialisées aux sociétés biotech et pharmaceutiques
                                                        <br><b>  Maturité :</b>  Le niveau de maturité commerciale de MAbSilico est faible. La force commerciale et les process sont naissants et en cours de structuration.
                                                        <br><b>  Lien Youtube :</b>           
                                                        <iframe width="70%"  height="315" src="https://www.youtube.com/embed/iOQP9GPETYg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/logo150x80_0015_Calque-19 (1).png" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>ponicode.com</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">Ponicode</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Solution qui fournit une analyse détaillée de la qualité des codes et une feuille de route spécifique pour le renforcer. </span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating13_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=13 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="13_5" <?php if ($rating13_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="13_4"  <?php if ($rating13_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="13_3"  <?php if ($rating13_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="13_2"  <?php if ($rating13_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="13_1"  <?php if ($rating13_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal13" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal13" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Ponicode</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société :</b> Ponicode 
                                                        <br><b>Solution concernée : </b>Solution qui fournit une analyse détaillée de la qualité des codes et une feuille de route spécifique pour le renforcer. Cette offre est soutenue par une IA de détection de défauts de qualité innovante. 
                                                        <br><b> Fondateurs : </b>Patrick Joubert 
                                                        <br><b> Site web :</b> www.ponicode.com
                                                        <br><b> Modèle :</b> SaaS 
                                                        <br><b> Description :</b> Plus de 10,000 utilisateurs à travers le monde. Ponicode se consacre donc désormais au développement de son support vers de nouveaux langages et de nouveaux environnements de développement ainsi qu’à l’accélération de son effort commercial.  Notre startup profite de son avancée technologique pour enrichir sa plateforme. 
                                                        <br><b> Maturité :</b> Nous avons décroché nos premiers clients et nous travaillons à l’amélioration et l’accélération de notre force commerciale. 
                                                        <br> <b>Lien Youtube : </b>
                                                        <iframe width="70%" height="315" src="https://www.youtube.com/embed/c5uL2E_WV-k" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/logo-4-connectinnov.jpg" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>connectinnov.com</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">CONNECTINNOV</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Solutions digitales (création d’applications, supports tactiles, table tactile enfant) pour aménager les points de vente.  </span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating14_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=14 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="14_5" <?php if ($rating14_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="14_4"  <?php if ($rating14_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="14_3"  <?php if ($rating14_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="14_2"  <?php if ($rating14_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="14_1"  <?php if ($rating14_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal14" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal14" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">CONNECTINNOV</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société :</b> CONNECTINNOV 
                                                        <br><b>Solution concernée :</b> Solutions digitales (création d’applications, supports tactiles, table tactile enfant) pour aménager les points de vente.  
                                                        <br><b> Fondateurs :</b> Michel MILLUL 
                                                        <br><b> Site web : </b>www.connectinnov.com 
                                                        <br><b> Modèle : </b>B2B 
                                                        <br><b> Description :</b> En phase de recrutement et dev commercial. 
                                                        <br><b> Maturité : </b>VENTE SUR INTERNET  
                                                        <br><b> Lien YouTube : </b> 
                                                        <iframe width="70%"  height="315" src="https://www.youtube.com/embed/C0av8wPTMtg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/logo - Talansoft.jpg" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>talansoft.com</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">Talansoft </h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Logiciel de création 3D no code, permettant de créer des applications interactives en AR & 3D sans coder</span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating17_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=17 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="17_5" <?php if ($rating17_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="17_4"  <?php if ($rating17_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="17_3"  <?php if ($rating17_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="17_2"  <?php if ($rating17_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="17_1"  <?php if ($rating17_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal17" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal17" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Talansoft</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société : </b>Talansoft 
                                                        <br><b> Solution concernée :</b> Logiciel de création 3D no code, permettant de créer des applications interactives en AR & 3D sans coder, 20 fois plus vite que la concurrence. Téléchargez nos modèles d'application gratuitement sur notre site !
                                                        <br><b>  Fondateurs :</b> Pierre, Jim, Louis, Guillaume 
                                                        <br><b>  Site web :</b> https://www.talansoft.com/ 
                                                        <br><b>  Modèle : </b>licence 
                                                        <br><b>  Description :</b> En phase de product-market fit et nous travaillons avec nos premiers clients entreprise.  
                                                        <br><b>  Maturité :</b> ventes B2B, publications en ligne sur sites spécialisés, salons 
                                                        <br> <b>Lien Youtube : </b>
                                                        <iframe width="70%" height="315" src="https://www.youtube.com/embed/N5WRKeGKykQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/Redirection.png" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>redirection.io</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">Redirection io </h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Outil parfait pour gérer les redirections HTTP pour les entreprises, le marketing et le référencement.</span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating18_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=18 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="18_5" <?php if ($rating18_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="18_4"  <?php if ($rating18_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="18_3"  <?php if ($rating18_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="18_2"  <?php if ($rating18_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="18_1"  <?php if ($rating18_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal18" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal18" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Redirection io</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société :</b> Redirection io 
                                                        <br><b>Solution concernée :</b> Outil parfait pour gérer les redirections HTTP pour les entreprises, le marketing et le référencement. Leur service permet aux visiteurs de sites internet de ne plus jamais rencontrer d'erreur 404. Chaque jour, redirection.io permet à ses clients d'effectuer plus de 100M de redirections. 
                                                        <br><b>Fondateurs :</b> Xavier Lacot 
                                                        <br><b>Site web :</b> https://redirection.io 
                                                        <br><b>Modèle :</b> B2B principalement, SaaS à abonnements. Lancement d’une offre on-premise prévue en fin d’année.
                                                        <br><b>Description :</b> Nous sommes en phase de consolidation de partenariats avec des agences SEO utilisatrices de notre solution. Nous développons en ce moment notre équipe commerciale naissante. Nos prochains axes de développement : commercialisation / marketing / chasse de prospects / lancement de l’offre hébergée avec auto-SSL, lancement du crawler SEO, lancement de l’autopilote, notre automate intelligent capable de fournir des conseils de règles de gestion de trafic à mettre en place pour optimiser le SEO.
                                                        <br><b>Maturité :</b> La technologie est mature et unique, et issue de 5 ans de R&D. Nous entamons nos premiers efforts marketing et commerciaux, nos clients payants actuels provenant principalement du bouche à oreille ou du référencement naturel. Notre panel de clients inclut des startups comme de très grands groupes (de luxe, notamment) présents sur les 5 continents. 
                                                        <br><b> Lien Youtube :</b>
                                                        <iframe width="70%"  height="315" src="https://www.youtube.com/embed/Glr3TN174vw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="bg-bleu p-5 mt-2 bg-gris"  >
                <div class="container">
                    <div class="row">
                        <div class="col-12"> 
                            <p class="text-center text-noir" style="color: #3837d1;font-size: 2.5em;font-weight: bold;line-height: 70px;">
                                Catégorie CLOUD
                            </p>

                        </div>
                    </div>
                </div>
            </section>
            <section class="p-5 mt-5">
                <div class="">
                    <div class="row">
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/Logo-Artifakt-LightBack-Horizontal (1).jpg" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>artifakt.com</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">Artifakt</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Plateforme permettant aux développeurs d’automatiser le déploiement, l’hébergement, la scalabilité et la gestion de leurs applications web sur le cloud</span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating19_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=19 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="19_5" <?php if ($rating19_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="19_4"  <?php if ($rating19_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="19_3"  <?php if ($rating19_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="19_2"  <?php if ($rating19_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="19_1"  <?php if ($rating19_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal19" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal19" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Artifakt</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société :</b> Artifakt 
                                                        <br><b>Solution concernée :</b> Artifakt est une plateforme permettant aux développeurs d’automatiser le déploiement, l’hébergement, la scalabilité et la gestion de leurs applications web sur le cloud. Notre console ergonomique simplifie les processus devops en offrant aux développeurs une totale autonomie pour mettre leurs projets en ligne. A cela s’ajoute, l'accès au monitoring des applications et des serveurs, aux logs, à une gestion des membres et la possibilité de modifier son infrastructure en fonction du trafic web attendu. 
                                                        <br><b>Fondateurs :</b> Aymeric Aitamer 
                                                        <br><b>Site web :</b> https://www.artifakt.com/ 
                                                        <br><b>Modèle :</b> B2B 
                                                        <br><b>Description :</b> En plein développement de notre Business à l’International et particulièrement sur les marchés européens, notre prochain temps fort est l’ouverture d’un accès gratuit à notre plateforme. Artifakt s’adresse aux équipes de développement qui veulent faciliter leur workflow et mieux travailler ensemble, en leur apportant une vraie autonomie et une plus grande agilité grâce à notre plateforme.
                                                        <br><b>Maturité :</b> Série A - 4.9 millions 
                                                        <br> <b>Lien Youtube : </b>
                                                        <iframe width="70%" height="315" src="https://www.youtube.com/embed/RIZbpxi6RYk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                         
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/Logo.jpg" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>urbest.io</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">Urbest</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Plateforme collaborative de suivi de maintenance et service entre demandeurs, intervenants et superviseurs.</span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating21_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=21 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="21_5" <?php if ($rating21_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="21_4"  <?php if ($rating21_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="21_3"  <?php if ($rating21_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="21_2"  <?php if ($rating21_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="21_1"  <?php if ($rating21_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal21" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal21" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Urbest</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société :</b> Urbest 
                                                        <br><b>Solution concernée :</b> Plateforme collaborative de suivi de maintenance et service entre demandeurs, intervenants et superviseurs. Urbest permet à des gestionnaires immobiliers ou des responsables de services d’organiser la charge de travail, de suivre et de communiquer de manière transparente sur l'ensemble des tâches planifiées ou à faire dans la semaine.
                                                        <br><b>Fondateurs :</b> Hugo Gervais 
                                                        <br><b>Site web :</b> www.urbest.io 
                                                        <br><b>Modèle :</b> abonnement B2B 
                                                        <br><b>Description :</b> Les besoins financiers d'Urbest sont destinées à 2 commerciaux et 2 développeurs ainsi que la mise en place de partenariat avec des distributeurs Saas comme avec Winpeo en Belgique. 
                                                        <br><b>Maturité :</b> Phoning, Linkedin, Capterra, email, salon / Canaux : Linkedin, Capterra, email 
                                                        <br> <b>Lien Youtube : </b>
                                                        <iframe width="70%" height="315" src="https://www.youtube.com/embed/zyq1LWQm0ns" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                         




                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/Welcomr.png" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>welcomr.com</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">Welcomr</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Welcomr s'impose comme la solution de contrôle d'accès des entreprises qui font entrer leurs bâtiments</span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating23_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=23 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="23_5" <?php if ($rating23_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="23_4"  <?php if ($rating23_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="23_3"  <?php if ($rating23_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="23_2"  <?php if ($rating23_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="23_1"  <?php if ($rating23_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal23" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal23" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Welcomr</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société :</b> Welcomr 
                                                        <br><b>   Solution concernée :</b> Welcomr s'impose comme la solution de contrôle d'accès des entreprises qui font entrer leurs bâtiments dans l'ère de la transformation digitale. C'est une plateforme de contrôle d'accès multimodale : smartphone, badge, QR code, codes PIN individuels, lecteur de plaques minéralogiques, ouverture à distance... 
                                                        <br><b> Fondateurs :</b> Alexis Gollain 
                                                        <br><b>  Site web :</b> www.welcomr.com 
                                                        <br><b>  Modèle :</b> SaaS B2B 
                                                        <br><b>   Description: </b>Welcomr recrute à tous les postes pour engager et animer un réseau de partenaires intégrateurs dans toute la France et au-delà.
                                                        <br><b>    Maturité :</b> Forte 
                                                        <br> <b>Lien Youtube : </b>
                                                        <iframe width="70%" height="315" src="https://www.youtube.com/embed/RbekL1Z15Ms" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/UBIKAP LOGO.jpg" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>ubikap.com</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">Ubikap</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Ubikap permet aux avocats, experts-comptables et juristes de dématérialiser le secrétariat juridique des sociétés </span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating24_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=24 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="24_5" <?php if ($rating24_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="24_4"  <?php if ($rating24_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="24_3"  <?php if ($rating24_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="24_2"  <?php if ($rating24_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="24_1"  <?php if ($rating24_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal24" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal24" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Ubikap</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société :</b> Ubikap 
                                                        <br><b>Solution concernée :</b> Ubikap permet aux avocats, experts-comptables et juristes de dématérialiser le secrétariat juridique des sociétés : assemblées générales, registres sur blockchain et gestion des dossiers.
                                                        <br><b>Fondateurs :</b> Thomas Malvoisin 
                                                        <br><b>Site web :</b> www.ubikap.com 
                                                        <br><b>Modèle :</b> SaaS B2B 
                                                        <br><b>Description :</b> Accélérer notre croissance et passer rapidement la barre des 100 clients. Recrutement d'une équipe constituée de business developer et customer success dans les prochains mois. Gagner en visibilité et développer différents partenariats stratégiques avec des solutions complémentaires et/ou des distributeurs. Intégrer quelques nouvelles fonctionnalités importantes à la première version qui nous permettront d’ouvrir de nouveaux marchés d’ici la fin de l’année. Maturité : Partenariat établi avec le Barreau de Lyon, un autre avec l'Ordre des Experts-Comptables de Lyon, plus de 300 utilisateurs aujourd’hui.
                                                        <br><b> Lien Youtube : </b>
                                                        <iframe width="70%"  height="315" src="https://www.youtube.com/embed/O5qBqAS57UE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/eBrigade.png" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>ebrigade.app </span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">eBrigade</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Cette solution permet aux entreprises d’organiser leurs activités, de suivre l’évolution du personnel et gérer leurs communautés. </span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating27_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=27 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="27_5" <?php if ($rating27_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="27_4"  <?php if ($rating27_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="27_3"  <?php if ($rating27_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="27_2"  <?php if ($rating27_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="27_1"  <?php if ($rating27_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal27" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal27" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">eBrigade</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">

                                                        <b>Nom société :</b> eBrigade 
                                                        <br><b>  Solution concernée : </b>Cette solution permet aux entreprises d’organiser leurs activités, de suivre l’évolution du personnel et gérer leurs communautés. 
                                                        <br><b> Fondateurs :</b> Joseph Nahed 
                                                        <br><b>  Site web :</b> www.ebrigade.app 
                                                        <br><b>  Modèle :</b> B2B 
                                                        <br><b>  Description :</b> Levée de fond. Croissance commerciale rapide. Internationalisation du produit. 
                                                        <br><b>  Maturité :</b> Nous avons déjà plus de 40 clients et rentrons entre 4 et 7 clients par mois (abonnement) 
                                                        <br> <b>Lien Youtube : </b>
                                                        <iframe width="70%" height="315" src="https://www.youtube.com/embed/Xd-xoan1rNU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="bg-bleu p-5 mt-2 bg-gris"  >
                <div class="container">
                    <div class="row">
                        <div class="col-12"> 
                            <p class="text-center text-noir" style="color: #3837d1;font-size: 2.5em;font-weight: bold;line-height: 70px;">
                                Catégorie Cybersécurité
                            </p>

                        </div>
                    </div>
                </div>
            </section>
            <section class="p-5 mt-5">
                <div class="">
                    <div class="row">
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/OGO_LOGO_BLEU-F.jpg" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>ogosecurity.com</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">OGO Security</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">OGO propose une solution de protection web instantané sans installation </span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating28_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=28 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="28_5" <?php if ($rating28_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="28_4"  <?php if ($rating28_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="28_3"  <?php if ($rating28_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="28_2"  <?php if ($rating28_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="28_1"  <?php if ($rating28_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal28" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal28" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">OGO Security</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société :</b> OGO Security 
                                                        <br><b> Solution concernée :</b> OGO propose une solution de protection web instantané sans installations, sans gestions quotidienne, sans gestion management, sans besoins de ressources en cybersécurité ou de ressources techniques, le tout pour un coût divisé par trois. 
                                                        <br><b> Fondateurs : </b>Olivier Arous 
                                                        <br><b> Site web :</b> www.ogosecurity.com 
                                                        <br><b> Modèle :</b> B2B 
                                                        <br><b> Description :</b> Le produit est vendu en direct depuis Octobre 2020 (10 clients) et nous souhaitons ouvrir le canal indirect fin 2021, ou début 2022 avec des hébergeurs et fournisseurs de solution de cybersécurité. 
                                                        <br><b>  Maturité :</b> Vente directe en place. Vente indirecte en recrutement 
                                                        <br><b>  Lien Youtube :</b>  
                                                        <iframe width="70%"  height="315" src="https://www.youtube.com/embed/lWv9LhLMYrM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/CrowdSec-DEF.jpg" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>crowdsec.net</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">CrowdSec</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Cette solution est un IPS open source et collaboratif qui permet d’analyser les comportements et de répondre aux attaques pour renforcer gratuitement la sécurité de vos environnements IT.</span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating29_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=29 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="29_5" <?php if ($rating29_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="29_4"  <?php if ($rating29_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="29_3"  <?php if ($rating29_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="29_2"  <?php if ($rating29_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="29_1"  <?php if ($rating29_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal29" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal29" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">CrowdSec</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société :</b> CrowdSec 
                                                        <br><b>Solution concernée :</b> Cette solution est un IPS open source et collaboratif qui permet d’analyser les comportements et de répondre aux attaques pour renforcer gratuitement la sécurité de vos environnements IT.
                                                        <br><b>Site web :</b> crowdsec.net
                                                        <br><b>Modèle :</b> B2B & SaaS 
                                                        <br><b>Description :</b> Installé plus de 20 000 fois dans 106 pays dans le monde, CrowdSec vient de sortir sa version 1.2 qui permet une compatibilité à toutes les plus grandes distributions Linux. Elle voit également sa capacité d'élimination des faux positifs et des tentatives d'empoisonnement renforcée par son nouveau système de consensus. L’équipe, qui a récemment levé 4M en Seed Round, 630K de subventions via le Grand Défi Cyber et 2M auprès de la BPI, se concentre désormais au développement d’offres Premium B2B qui seront disponibles fin 2021. La solution est à ce jour utilisée par des PME, grands groupes, instituions gouvernementales, forces armées, universités etc.
                                                        <br><b>Lien Youtube : </b>
                                                        <iframe width="70%" height="315" src="https://www.youtube.com/embed/tG-e2UxZmXc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                                          

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/Parcoor.png" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>parcoor.com</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">Parcoor</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Logiciel de sécurité pour bloquer les cyberattaques. </span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating30_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=30 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="30_5" <?php if ($rating30_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="30_4"  <?php if ($rating30_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="30_3"  <?php if ($rating30_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="30_2"  <?php if ($rating30_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="30_1"  <?php if ($rating30_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal30" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal30" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Parcoor</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b> Nom société :</b> Parcoor 
                                                        <br><b>  Solution concernée : </b>Logiciel de sécurité pour bloquer les cyberattaques. Parcoor développe en permanence des solutions innovantes de détection des menaces. Ces solutions sont basées sur une nouvelle approche combinant les données centrales du système surveillées en temps réel + des algorithmes d'apprentissage automatique de pointe, légers, rapides et explicables. 
                                                        <br><b> Fondateurs :</b> Manuel Capel 
                                                        <br><b> Site web :</b> https://parcoor.com 
                                                        <br><b>  Modèle : </b>B2B (licences + contrat de maintenance) 
                                                        <br><b>  Description :</b> Le but est de conclure des contrats pour des prototypes in-situ qui pourront à court-terme (6 mois) aboutir à des contrats de déploiement à large échelle de notre solution sur des flottes d’appareils embarqués. 
                                                        <br><b>  Maturité : </b>Encore précoce bien qu’elle s’étoffe (contrat avec Thalès) Nous comptons principalement sur les salons pour présenter et commercialiser notre solution. 
                                                        <br><b> Lien Youtube : </b>
                                                        <iframe width="70%"  height="315" src="https://www.youtube.com/embed/TdOAteC9Vcc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3" style="margin-top: 20px">
                            <div class="card p-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex flex-row align-items-center">
                                        <div class="icon"> <i class="bx bxl-mailchimp"></i> </div>
                                        <div class="ms-2 c-details">
                                            <h6 class="mb-0"><img src="images/Astrachain.png" /></h6>
                                        </div>
                                    </div>
                                    <div class="badge"> <span>astrachain.com</span> </div>
                                </div>
                                <div class="mt-5" style="margin-top: 5px !important;">
                                    <h3 class="heading">Astrachain</h3>
                                    <div class="mt-3" style="height: 120px"> <span class="text1" style="font-size: 14px">Astrachain commercialise un service nommé Dot & Split. </span> </div>
                                    <div class="rating">
                                        <?php
                                        $rating31_1 = mysqli_fetch_array(mysqli_query($link, "select * from vote where startup=31 and ip='" . $ip . "'"));
                                        ?>
                                        <a id="31_5" <?php if ($rating31_1['note'] >= 5) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 5 stars">☆</a>
                                        <a id="31_4"  <?php if ($rating31_1['note'] >= 4) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 4 stars">☆</a>
                                        <a id="31_3"  <?php if ($rating31_1['note'] >= 3) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 3 stars">☆</a>
                                        <a id="31_2"  <?php if ($rating31_1['note'] >= 2) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 2 stars">☆</a>
                                        <a id="31_1"  <?php if ($rating31_1['note'] >= 1) echo 'style="color:orange"'; ?> onclick="voter11(this)" title="Give 1 star">☆</a>
                                    </div>

                                    <div class="mt-5" style="text-align: center;">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal31" style="width: 160px;">Fiche</button>
                                        <div class="modal fade" id="exampleModal31" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Astrachain</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align: left;font-size: 14px; line-height: 23px">
                                                        <b>Nom société : </b>Astrachain 
                                                        <br><b>   Solution concernée :</b> Astrachain commercialise un service nommé Dot & Split. C’est une technologie de protection des données, cloud native, basée sur une blockchain privée et un chiffrement de pointe. Les données sont chiffrées et divisées, puis stockées simultanément chez plusieurs fournisseurs cloud. De plus, ils ont développé une application clé en main nommée DOT. Cet outil de gestion de documents et de collaboration permet de bénéficier d’une interface intuitive et fluide. 
                                                        <br><b>  Fondateurs :</b> Yosra Jarraya 
                                                        <br><b>  Site web :</b> astrachain.com
                                                        <br><b>  Modèle :</b> B2B, abonnements, as-a-Service 
                                                        <br><b>  Description : </b>Recrutements, Commercialisation (Go live produit le 07/09). 
                                                        <br><b>  Maturité :</b> Canal direct uniquement dans l'immédiat, partenariats à développer 
                                                        <br><b> Lien Youtube</b>
                                                        <iframe width="70%"  height="315" src="https://www.youtube.com/embed/4yH-eFvzUV0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </main>
        <footer>
            <section class="bg-bleu p-5 mt-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 ">
                            <p class="text-center text-white">
                                <span class="font-weight-bold">MyFrenchStartup © 2013 - 2021</span>
                                <br>La reproduction, la rediffusion ou l'extraction automatique par tout moyen d'informations figurant 
                                <br>sur myfrenchstartup.com est interdite. L'emploi de robots, programmes permettant l'extraction directe 
                                <br>de données est rigoureusement interdit.
                            </p>
                        </div>
                    </div>
                </div>        
            </section>
        </footer>
        <script>
            function voter11(el) {
                var id = el.id;

                $.ajax({
                    url: 'save.php?id=' + id,
                    success: function (data) {

                        var words = id.split('_');
                        for (var i = 1; i <= 5; i++) {
                            var val = words[0] + "_" + i;
                            document.getElementById(val).style.color = "black";
                        }

                        for (var i = 1; i <= words[1]; i++) {
                            var val = words[0] + "_" + i;
                            document.getElementById(val).style.color = "orange";
                        }


                    }

                });
            }

        </script>
    </body>
</html>
