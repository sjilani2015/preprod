<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}

if (!isset($_SESSION['data_login'])) {
    echo '<script>window.location="' . URL . '/connexion"</script>';
}

if (!isset($_GET["code"])) {
    header('location:' . URL . '/404');
}
$code = $_GET["code"];
if ($code == '') {
    header('location:' . URL . '/404');
}

$id_startup = degenerate_id($code);
$users_actif = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));

$verif_user = mysqli_num_rows(mysqli_query($link, "select id from user_startup where iduser=" . $users_actif['id'] . " and id_startup=" . $id_startup));

if ($verif_user == 0) {
    header('location:' . URL . '/404');
}

$startup = getStartupById($id_startup);
$id_secteur = mysqli_fetch_array(mysqli_query($link, "Select  secteur.id,secteur.nom_secteur,secteur.nom_secteur_en From secteur inner join activite on activite.secteur=secteur.id Where  activite.id_startup =" . $startup[0]['id']));
$id_sous_secteur = mysqli_fetch_array(mysqli_query($link, "Select  sous_secteur.id,sous_secteur.nom_sous_secteur,sous_secteur.nom_sous_secteur_en From  sous_secteur inner join activite on activite.sous_secteur=sous_secteur.id Where  activite.id_startup =" . $startup[0]['id']));
$id_sous_activite = mysqli_fetch_array(mysqli_query($link, "Select  last_sous_activite.id,last_sous_activite.nom_sous_activite From  last_sous_activite inner join activite on activite.sous_activite=last_sous_activite.id Where  activite.id_startup =" . $startup[0]['id']));
$id_activite = mysqli_fetch_array(mysqli_query($link, "Select  last_activite.id,last_activite.nom_activite,last_activite.nom_activite_en From  last_activite inner join activite on activite.activite=last_activite.id Where  activite.id_startup =" . $startup[0]['id']));
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Modifier ma startup - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="formpage">
                    <div class="formpage__content">
                        <form class="stepy-basic" action="<?php echo URL ?>/startup-modifie" method="post" enctype="multipart/form-data">
                            <div class="formpage__title">
                                <h1>Pour modifier votre startup,<br />veuillez remplir le formulaire ci-dessous.</h1>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Informations générales</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>* Nom commercial</label>
                                        <input type="text" value="<?php echo $startup[0]['nom']; ?>" required="" name="startup"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Nom juridique de votre startup</label>
                                        <input type="text" name="juridique" value="<?php echo $startup[0]['societe']; ?>" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Accroche</label>
                                        <input type="text"  name="accroche" required="" value="<?php echo utf8_encode($startup[0]['short_fr']); ?>" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>SIRET</label>
                                        <input type="text" name="siret" value="<?php echo $startup[0]['siret']; ?>" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Date de création</label>
                                        <input type="date"  name="creation" required="" value="<?php echo $startup[0]['date_complete']; ?>" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Adresse</label>
                                        <div class="map_canvas"></div>
                                        <input type="hidden" value="<?php echo $startup[0]['id']; ?>" name="id" />
                                        <input type="hidden" class="ll_input" value="<?php echo $startup[0]['cp']; ?>" name="postal_code" />
                                        <input type="hidden" class="ll_input" value="<?php echo $startup[0]['ville']; ?>" name="locality"  />
                                        <input type="hidden" class="ll_input" value="<?php echo $startup[0]['lat']; ?>" name="lat"/>
                                        <input type="hidden" class="ll_input" value="<?php echo $startup[0]['lng']; ?>" name="lng"/>
                                        <input type="hidden" class="ll_input" value="<?php echo $startup[0]['region']; ?>" name="administrative_area_level_1"/>
                                        <input type="text" required="" value="<?php echo $startup[0]['adresse']; ?>" id="geocomplete" name="adresse" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Email</label>
                                        <input type="email"  name="email" required="" value="<?php echo $startup[0]['email']; ?>" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Téléphone</label>
                                        <input type="phone" name="tel"  required="" value="<?php echo $startup[0]['tel']; ?>" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Réseaux sociaux</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>* Site web</label>
                                        <input type="text"  required="" name="website" value="<?php echo $startup[0]['url']; ?>"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Modifier votre logo</label>
                                        <input type="file"  name="logo"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Facebook</label>
                                        <input type="text" placeholder="https://www.facebook.com/company" value="<?php echo $startup[0]['facebook']; ?>" name="facebook"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Twitter</label>
                                        <input type="text" placeholder="@company" value="<?php echo $startup[0]['twitter']; ?>" name="twitter"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* LinkedIn</label>
                                        <input placeholder="https://www.linkedin.com/company"  required="" type="text"  value="<?php echo $startup[0]['linkedin']; ?>" name="linkedin"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Vidéo Youtube</label>
                                        <input type="text" placeholder="https://" value="<?php echo $startup[0]['youtube']; ?>" name="youtube"  class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Activité</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>* Marché</label>
                                        <div class="custom-select">
                                            <select class="form-control" required="" data-placeholder="" name="list_sector">
                                                <option value="<?php echo $id_secteur['id'] ?>"><?php echo $id_secteur['nom_secteur'] ?></option>
                                                <?php
                                                $row_list_secteur = getListSecteurTotal();
                                                if (!empty($row_list_secteur)) {
                                                    foreach ($row_list_secteur as $secteur) {
                                                        ?>
                                                        <option value="<?php echo $secteur['id']; ?>"><?php echo utf8_encode($secteur['nom_secteur']); ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Activité</label>
                                        <div class="custom-select">
                                            <select class="form-control" required="" data-placeholder="" name="list_marche">
                                                <option value="<?php echo $id_activite['id'] ?>"><?php echo $id_activite['nom_activite'] ?></option>
                                                <?php
                                                $sql_activite = mysqli_query($link, "Select  * From  last_activite  order by nom_activite");
                                                while ($marche = mysqli_fetch_array($sql_activite)) {
                                                    ?>
                                                    <option value="<?php echo $marche['id']; ?>"><?php echo ($marche['nom_activite']); ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Tags</label>
                                        <input type="text" value="Startup" name="tags" required="" value="<?php echo $startup[0]['concat_tags']; ?>" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Présentation de votre startup (200 à 1000 caractères)</label>
                                        <textarea type="text" value="Startup" name="long_fr" required="" class="form-control" placeholder="Description détaillée de votre Startup" rows="5"><?php echo strip_tags(utf8_encode($startup[0]['long_fr'])); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Fondateur</div>
                                <?php
                                $n = 1;
                                $sql_founders = mysqli_query($link, "select * from personnes where id_startup=" . $startup[0]["id"]);
                                while ($founders = mysqli_fetch_array($sql_founders)) {
                                    $sql_fonction1 = mysqli_fetch_array(mysqli_query($link, "select * from fonction where id=" . $founders['fonction']));
                                    ?>
                                    <div style="font-weight: bold;margin-top: 24px;margin-bottom: 8px;">Fondateur <?php echo $n ?></div>
                                    <div class="formgrid formgrid--2col">

                                        <div class="formgrid__item">
                                            <label>Prénom</label>
                                            <input type="hidden" required="" name="id_founder[]" value="<?php echo $founders['id']; ?>" />
                                            <input placeholder="" type="text" value="<?php echo $founders['prenom']; ?>" name="prenom_founder[]"  class="form-control" />
                                        </div>
                                        <div class="formgrid__item">
                                            <label>Nom</label>
                                            <input placeholder="" required="" type="text" value="<?php echo $founders['nom']; ?>" name="nom_founder[]"  class="form-control" />
                                        </div>
                                        <div class="formgrid__item">
                                            <label>* Fonction</label>
                                            <div class="custom-select">
                                                <select class="form-control" required="" data-placeholder="" name="fonction[]">
                                                    <option value="<?php echo $founders['fonction']; ?>"><?php echo $sql_fonction1['nom_fr']; ?></option>
                                                    <?php
                                                    $sql_fonction = mysqli_query($link, "select * from fonction where valide=1");
                                                    while ($fonction = mysqli_fetch_array($sql_fonction)) {
                                                        ?>


                                                        <option value="<?php echo $fonction['id'] ?>" >
                                                            <?php
                                                            echo $fonction['nom_fr'];
                                                            ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="formgrid__item">
                                            <label>Email</label>
                                            <input placeholder="" type="text" value="<?php echo $founders['email']; ?>" name="email_founder[]"  class="form-control" />
                                        </div>
                                        <div class="formgrid__item">
                                            <label>Téléphone</label>
                                            <input placeholder="" type="text" value="<?php echo $founders['tel']; ?>" name="tel_founder[]"  class="form-control" />
                                        </div>
                                        <div class="formgrid__item">
                                            <label>LinkedIn</label>
                                            <input placeholder="Lien vers profil linkedin" required="" type="text" value="<?php echo $founders['linkedin']; ?>" name="linkedin_founder[]"  class="form-control" />
                                        </div>
                                    </div>
                                    <?php
                                    $n++;
                                }
                                ?>
                            </div>

                            <div class="formpage__actions">
                                <button type="submit" class="btn btn-primary">Valider</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>