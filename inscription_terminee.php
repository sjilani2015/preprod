<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
if (isset($_SESSION['data_login'])) {
    header('location:' . URL . '/recherche-startups');
    exit();
}


if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}

function generateRandomString($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

if (isset($_POST['f1_civilite'])) {
    $civilite = addslashes($_POST['f1_civilite']);
    $prenom = addslashes($_POST['prenom']);
    $nom = addslashes($_POST['nom']);
    $email = addslashes($_POST['email']);
    $societe = addslashes($_POST['societe']);
    $type_user = addslashes($_POST['f1_type']);
    $fonction = addslashes($_POST['fonction']);
    $tel = addslashes($_POST['tel']);
    $ville = addslashes($_POST['ville']);

    $pwd = addslashes($_POST['password1']);

    $pwd1 = md5($pwd);
    $token = uniqid();
    $password = generateRandomString(6);

    $_SESSION["inscrit"] = $email;

    $verif_user = mysqli_num_rows(mysqli_query($link, "select * from user where email='" . $email . "'"));

    if ($verif_user == 0) {
        mysqli_query($link, "insert into user(type,email,password,date_add,ip,status,token,civilite,prenom,nom,societe,fonction,tel,ville,colonne)values('" . $type_user . "','" . $email . "','" . $pwd1 . "','" . date('Y-m-d H:i:s') . "','" . $_SERVER['REMOTE_ADDR'] . "',0,'" . $token . "','" . $civilite . "','" . $prenom . "','" . $nom . "','" . $societe . "','" . $fonction . "','" . $tel . "','" . $ville . "','6,7,8,9,10,11,12,13,14,15,16,17,18') ");

        $id_users = mysqli_insert_id($link);

        /* $req_licorne = "select * from startup  where  licorne=1";
          $sql_licorne = mysqli_query($link, "select * from startup  where  licorne=1");
          $id_licorne = "";
          while ($dt_licorne = mysqli_fetch_array($sql_licorne)) {
          $id_licorne .= $dt_licorne['id'] . ",";
          }

          $req_ft = "select * from startup  where  ft120=1";
          $sql_ft = mysqli_query($link, "select * from startup  where  ft120=1");
          $id_ft = "";
          while ($dt_ft = mysqli_fetch_array($sql_ft)) {
          $id_ft .= $dt_ft['id'] . ",";
          }

          $req_next = "select * from startup  where  next40=1";
          $sql_next = mysqli_query($link, "select * from startup  where  next40=1");
          $id_next = "";
          while ($dt_next = mysqli_fetch_array($sql_next)) {
          $id_next .= $dt_next['id'] . ",";
          }

          $suplin = 'Startups Licornes';
          $supft = 'FT 120';
          $supnext = 'NEXT 40';

          mysqli_query($link, "insert into user_save_list(user,nom,req,list,dt)values(" . $id_users . ",'" . $suplin . "','" . $req_licorne . "','" . $id_licorne . "','" . date('Y-m-d H:i:s') . "')");
          mysqli_query($link, "insert into user_save_list(user,nom,req,list,dt)values(" . $id_users . ",'" . $supft . "','" . $req_ft . "','" . $id_ft . "','" . date('Y-m-d H:i:s') . "')");
          mysqli_query($link, "insert into user_save_list(user,nom,req,list,dt)values(" . $id_users . ",'" . $supnext . "','" . $req_next . "','" . $id_next . "','" . date('Y-m-d H:i:s') . "')"); */

        $pp = mysqli_query($link, "select * from user where email='" . $email . "' limit 1");
        $mon_user = mysqli_fetch_array($pp);

        if ($type_user == 0)
            $mytype1 = "Entrepreneur";
        if ($type_user == 1)
            $mytype1 = "Investisseur";
        if ($type_user == 3)
            $mytype1 = "Media";
        if ($type_user == 4)
            $mytype1 = "Institutionnel";
        if ($type_user == 5)
            $mytype1 = "Prestataire";
        if ($type_user == 6)
            $mytype1 = "Etudiant";
        if ($type_user == 7)
            $mytype1 = "Enseignant";
        if ($type_user == 10)
            $mytype1 = "Corporate";


        $body = '<html>
    <head>
        <title>MyFrenchStartup</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        
        <style type="text/css">
            table {
                border-collapse: separate;
            }
            a,
            a:link,
            a:visited {
                text-decoration: none;
                color: #000000;
            }
            a:hover {
                text-decoration: underline;
            }
            .ExternalClass p,
            .ExternalClass p,
            .ExternalClass font,
            .ExternalClass td {
                line-height: 100%
            }
            .ExternalClass {
                width: 100%;
            }
            .hmfix img {
                width: 10px !important;
                height: 10px !important;
            }
            @media only screen and (max-width: 460px){
                .resp_tab
                {
                    width: 100% !important;
                }
                .text-respo{
                    text-align:center !important;
                }
                .resp_img
                {
                    width: 100% !important;
                }
                .resp_btn
                {
                    width: 80% !important;
                }
                .resp_col{
                    display: block !important;
                    width: 100% !important;
                    /*text-align: center !important;*/
                }
            }
        </style>
    </head>
    <body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

        <table align="center" width="600" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_01.jpg" width="600" style="height: 100%; display: block" alt=""></td>
            </tr>
            <tr>
                <td>
                    <table width="600" style="height: 42px"  border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_02_01.jpg" width="227" style="height: 100%; display: block" alt=""></td>
                            <td>
                                <a href="https://www.myfrenchstartup.com/fr/" target="_blank"><img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_02_02.jpg" width="145" style="height: 100%; display: block" alt=""></a></td>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_02_03.jpg" width="228" style="height: 100%; display: block" alt=""></td>
                        </tr>
                    </table>    
                </td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_03.jpg" width="600" style="height: 100%; display: block" alt=""></td>
            </tr>
            <tr>
                <td>
                    <table id="Tableau_01" width="600" style="height:28px" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_04_01.jpg" width="36" style="height: 100%; display: block" alt=""></td>
                            <td style="width:564px; background: #ffffff">
                                <p style="font-family:Helvetica;padding:0;Margin:0;text-align: left;font-size: 22px;color:#212121;font-weight: bold;" >
                                    Bonjour ' . $prenom . ',
                                </p>       
                            </td>
                        </tr>
                    </table>    
                </td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_05.jpg" width="600" style="height: 100%; display: block" alt=""></td>
            </tr>
            <tr>
                <td>
                    <table id="Tableau_01" width="600" style="height: 45px" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_06_01.jpg" width="36" style="height: 100%; display: block" alt=""></td>
                            <td style="width:535px; background: #ffffff">
                                <p style="font-family:Helvetica;padding:0;Margin:0;text-align: left;font-size: 16px;color:#808080;line-height: 22px;font-weight: 100;" >
                                    Bienvenue sur myFrenchStartup. Plus qu\'une étape pour pouvoir bénéficier de toute la data de l\'écosystème des startups françaises.
                                </p>      
                            </td>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_06_03.jpg" width="29" style="height: 100%; display: block" alt=""></td>
                        </tr>
                    </table>    
                </td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_07.jpg" width="600" style="height: 100%; display: block" alt=""></td>
            </tr>
            <tr>
                <td>
                    <table id="Tableau_01" width="600" style="height: 21px" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_08_01.jpg" width="35" style="height: 100%; display: block" alt=""></td>
                            <td style="width:565px; background: #ffffff">
                                <p style="font-family:Helvetica;padding:0;Margin:0;text-align: left;font-size: 16px;color:#808080;line-height: 20px;font-weight: 100;" >
                                    Merci de valider votre compte en cliquant sur ce lien :
                                </p>      
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_09.jpg" width="600" style="height: 100%; display: block" alt=""></td>
            </tr>
            <tr>
                <td>
                    <table id="Tableau_01" width="600" style="height: 48px" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_10_01.jpg" width="185" style="height: 100%; display: block" alt=""></td>
                            <td>
                                <a href="' . URL . '/valider.php?num=' . $token . '" target="_blank"><img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_10_02.jpg" width="202" style="height: 100%; display: block" alt=""></a></td>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_10_03.jpg" width="213" style="height: 100%; display: block" alt=""></td>
                        </tr>
                    </table>    
                </td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_11.jpg" width="600" style="height: 100%; display: block" alt=""></td>
            </tr>
            <tr>
                <td>
                    <table id="Tableau_01" width="600" style="height: 21px" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_08_01.jpg" width="65" style="height: 100%; display: block" alt=""></td>
                            <td style="width:535px; background: #ffffff">
                                <p style="font-family:Helvetica;padding:0;Margin:0;text-align: left;font-size: 16px;color:#808080;line-height: 20px;font-weight: 100; font-style: italic" >
                                    Ce lien restera valable durant 30 jours.
                                </p>      
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_13.jpg" width="600" style="height: 100%; display: block" alt=""></td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_14.jpg" width="600" style="height: 100%; display: block" alt=""></td>
            </tr>
            <tr>
                <td style="background:#333333">
                    <table id="Tableau_01" width="600" style="height: 44px" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_15_01.jpg" width="111" style="height: 100%; display: block" alt=""></td>
                            <td style="width:381px; background: #333333">
                                <p style="font-family:Helvetica;padding:0;Margin:0;text-align: center;font-size: 11px;color:#ffffff;line-height: 20px;font-weight: 100;" >
                                    Copyright © 2022 myFrenchStartup, Tous droits réservés.<br>
                                    Vous recevez cet email car vous êtes inscrits au site myFrenchStartup
                                </p>      
                            </td>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_15_03.jpg" width="108" style="height: 100%; display: block" alt=""></td>
                        </tr>
                    </table>    
                </td>
            </tr>
            <tr>
                <td  style="background:#333333">
                    <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_16.jpg" width="600" style="height: 100%; display: block" alt=""></td>
            </tr>
            <tr>
                <td style="background:#333333">
                    <table id="Tableau_01" width="600" style="height: 27px" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_17_01.jpg" width="234" style="height: 100%; display: block" alt=""></td>
                            <td>
                                <a href="https://www.facebook.com/MyFrenchStartup" target="_blank"><img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_17_02.jpg" width="25" style="height: 100%; display: block" alt=""></a></td>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_17_03.jpg" width="25" style="height: 100%; display: block" alt=""></td>
                            <td>
                                <a href="https://twitter.com/myfrenchstartup" target="_blank"> <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_17_04.jpg" width="29" style="height: 100%; display: block" alt=""></a></td>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_17_05.jpg" width="27" style="height: 100%; display: block" alt=""></td>
                            <td>
                                <a href="https://www.myfrenchstartup.com/fr/" target="_blank"><img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_17_06.jpg" width="27" style="height: 100%; display: block" alt=""></a></td>
                            <td>
                                <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_17_07.jpg" width="233" style="height: 100%; display: block" alt=""></td>
                        </tr>
                    </table>    
                </td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/emailing/confirmation/images/index_18.jpg" width="600" style="height: 100%; display: block" alt=""></td>
            </tr>
        </table>

    </body>
</html>';

       

        require 'phpmailer/PHPMailerAutoload.php';

        $mail = new PHPMailer();

        $mail->IsSMTP();
        $mail->Host = 'ssl0.ovh.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'inscription@myfrenchstartup.com';                 // SMTP username
        $mail->Password = '04Betterway#p';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                               // TCP port to connect to

        $mail->setFrom('inscription@myfrenchstartup.com','myFrenchStartup');
        $mail->addAddress($email);     // Add a recipient
        $mail->addBCC('contact@myfrenchstartup.com');
        //$mail->addBCC('s.jilani@agencebetterway.com');

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = "Confirmez votre inscription !";

        $mail->Body = utf8_decode($body);

        if (!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            
        }


        // exec("Rscript /var/www/html/cron_myfs/calcul_startup_list_last_add.r");
    } else {
        echo "<script>alert('Email existe déjà !')</script>";
        echo "<script>window.location='" . URL . "/connexion'</script>";
    }
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>

        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container" style="text-align: center">            
                    Un email de confirmation vient d'être envoyé.<br>Merci de cliquer sur le lien de confirmation afin de valider votre inscription<br>

                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');

            function checkEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            function connexion() {
                document.getElementById("msg_error_mail").style.display = "none";
                document.getElementById("msg_error_user").style.display = "none";
                document.getElementById("msg_error_user_block").style.display = "none";
                var login = document.getElementById("login_connect").value;
                var pwd = document.getElementById("pwd").value;
                if (checkEmail(login)) {

                    $.ajax({
                        type: "POST",
                        url: "<?php echo URL ?>/verif_user.php?login=" + login + "&pwd=" + pwd,

                        success: function (data) {
                            var t = eval(data);
                            if (t[0].result == 0) {
                                document.getElementById("msg_error_user").style.display = "block";
                            } else {
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo URL ?>/verif_user_block.php?login=" + login + "&pwd=" + pwd,

                                    success: function (data1) {
                                        var t = eval(data1);
                                        if (t[0].result == 0) {
                                            document.getElementById("msg_error_user_block").style.display = "block";
                                        } else {
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo URL ?>/redirection.php?login=" + login + "&pwd=" + pwd,

                                                success: function (data2) {

                                                    window.location = data2;


                                                }
                                            });
                                        }


                                    }
                                });
                            }


                        }
                    });






                } else {
                    document.getElementById("msg_error_mail").style.display = "block";
                }

            }
        </script>
    </body>
</html>