<?php
//include("include/db.php");
include("functions/functions.php");
include ('config.php');
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Offre payante - <?= SITENAME; ?></title>
        <meta name="description" content="Foire aux questions <?= SITENAME; ?>">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php include ('layout/header-simple.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">
                    <div class="section-title section-title--fat">
                        Pourquoi une offre payante ?
                    </div>
                    <div class="offre-payante">
                        <div class="box">
                            <p>Les médias proposant des accès « gratuits » fonctionnent tous sur le même modèle : la vente d’espaces publicitaires. Ces dernières années, ce modèle s’est enrichi de fonctionnalités permettant aux lecteurs de bénéficier d’offres adaptées à leurs intérêts et de publicités directement en lien avec leur activité. Ces offres nécessitent la plupart du temps, le partage des données confiées par nos lecteurs avec nos partenaires commerciaux. Nos offres ont fait l’objet d’un audit de conformité avec le Règlement Général sur la Protection des Données, mais nous comprenons que certains lecteurs souhaitent que leurs données soient utilisées exclusivement pour gérer leurs abonnements. Ce souhait est évidemment totalement légitime, mais nous prive de recettes publicitaires indispensables à la survie de nos médias.</p>
                            <h2>Abonnement</h2>
                            A ce titre, nous proposons à celles et ceux de nos lecteurs qui ne souhaitent pas que leurs données soient partagées avec nos partenaires, d’accéder à tous nos contenus dans le cadre d’un abonnement.
                        </div>
                        <div class="formules">
                            <div class="formules__item">
                                <div class="formules__item__title">
                                    Offre payante
                                </div>
                                <div class="formules__item__pricing">
                                    98<span class="curr">€</span>
                                </div>
                                <div class="formules__item__pricingDetails">Formule 12 mois / abonné</div>
                                <div class="formules__item__headline">
                                    Abonnement "non partage des données"
                                </div>
                                <ul class="formules__item__arg">
                                    <li>Abonnez-vous à l'offre payante de 98 euros TTC par an, pour profiter des services du MyFrenchStartup sans partager vos données avec nos partenaires.</li>
                                </ul>


                                <div class="formules__item__cta">
                                    <a href="<?php echo URL ?>" class="btn btn-primary">S'abonner</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="offre-copyrights">
                        <p>MyFrenchStartup SAS, 26-28 rue Danielle Casanova - 75002 Paris, intervient en qualité de responsable de traitement des données collectées via le formulaire d’abonnement. Ces données sont nécessaires pour gérer votre abonnement. Sans elles, nous ne pourrons faire suite à votre demande.</p>
                        <p>Dans le cadre de l'abonnement payant souscrit, vous accédez aux mêmes contenus que la formule gratuite, mais LE MyFrenchStartup SAS ne partage aucune donnée avec ses partenaires commerciaux. Les seuls destinataires de vos données sont les personnes en charge de la gestion des abonnements au sein de notre société.</p>
                        <p>Conformément à notre politique générale en matière de données personnelles, vous disposez d’un droit d’accès vous permettant à tout moment de connaître la nature des données collectées vous concernant, de demander leur rectification ou leur effacement. Ce droit s’exerce par simple envoi d’un email à privacy@lemoci.com. Vos données sont conservées pour la durée de votre abonnement. Sauf indication contraire, nous conservons votre email pour vous adresser nos newsletters. Si vous ne souhaitez pas recevoir notre newsletter, vous pouvez nous l’indiquer par envoi d’un simple email à desinscription@lemoci.com. Vous pourrez également vous désabonner à tout moment en cliquant sur le lien adéquat dans nos newsletters. En cas de difficulté en lien avec la gestion de vos données personnelles, vous avez le droit d’introduire une réclamation auprès de la CNIL. Pour tout savoir sur la manière dont LE MyFrenchStartup SAS gère les données personnelles, vous pouvez vous reporter à notre Charte sur les Données Personnelles.</p>
                    </div>

                    <div class="offre-footer">
                        <div class="box">
                            <h3>Nous contacter</h3>
                            Vous pouvez contacter notre service clients par email ou par téléphone au 01 01 01 01 01 du lundi au vendredi.
                        </div>
                        <div class="box">
                            <h3>Paiement sécurisé</h3>
                            Vous pouvez régler votre abonnement en toute sécurité par carte bancaire (Visa, Carte Bleue et Mastercard) ou par virement bancaire.
                        </div>
                        <div class="box">
                            <h3>Retrouvez toute l'actualité</h3>
                            Avec l’abonnement retrouvez toute l’actualité et recevez le magazine papier au bureau ou à la maison. (sauf offre web)
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/animated.min.js"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>