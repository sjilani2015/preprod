<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
$_SESSION["myfs_language"]='fr';
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Liste Startups France, Plus grand annuaire de startups francaises</title>
        <meta name="description" content="<?= METADESC; ?>">
        
        <link rel="image_src" href="<?php echo URL; ?>/images/logo-myfs-300x300.jpg"/>
        <meta name="title" content="Liste Startups France, Plus grand annuaire de startups francaises"/>
        <meta name="Description" content="myFrenchStartup, annuaire Startups France, base de données qui liste 30000 startups françaises, entrepreneurs et investisseurs avec les levées de fonds" />
        <meta name="Keywords" content="annuaire startup, startups françaises, startup france, liste de startups, base de données de startups, entrepreneurs" />


        <meta property="og:title" content="Liste Startups France, Plus grand annuaire de startups francaises" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="<?php echo URL; ?>" />
        <meta property="og:image" content="<?php echo URL; ?>/images/logo-myfs-300x300.jpg" />
        <meta property="og:description" content="myFrenchStartup, annuaire Startups France, base de données qui liste 30000 startups françaises, entrepreneurs et investisseurs avec les levées de fonds" />
        <meta property="og:site_name" content="MyFrenchStartup" />
        <meta property="og:locale" content="<?php echo strtolower($_SESSION["myfs_language"]) . '_' . strtoupper($_SESSION["myfs_language"]); ?>" />
        <meta property="fb:page_id" content="384072774988229" />

        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content="@myfrenchstartup">
        <meta name="twitter:title" content="Liste Startups France, Plus grand annuaire de startups francaises">
        <meta name="twitter:description" content="myFrenchStartup, annuaire Startups France, base de données qui liste 30000 startups françaises, entrepreneurs et investisseurs avec les levées de fonds">
        <meta name="twitter:image" content="<?php echo URL; ?>/images/logo-myfs-300x300.jpg">

        <meta itemprop="inLanguage" content="<?php echo strtolower($_SESSION["myfs_language"]) . '_' . strtoupper($_SESSION["myfs_language"]); ?>">
        <meta itemprop="copyrightHolder" content="MyFrenchStartup" />
        <meta itemprop="copyrightYear" content="<?php echo date('Y'); ?>" />
        <meta itemprop="provider" content="MyFrenchStartup">
        
        
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
        <!-- End Google Tag Manager -->
        <style>
            .cards__bloc__raise__iconUp:after{
            background-image: none !important;
            }
        </style>
    </head>
    <body class="preload">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MR2VSZB"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <?php include ('layout/home/overview.php'); ?>
                <?php include ('layout/home/fundraising.php'); ?>
                <?php include ('layout/home/laststartups.php'); ?>
                <?php include ('layout/home/trends.php'); ?>
                <?php include ('layout/home/services.php'); ?>
                <?php include ('layout/home/demo.php'); ?>
                <?php //include ('layout/home/analyze.php');  ?>
                <?php include ('layout/home/peoplemap.php'); ?>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/animated.min.js"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectRegion(val) {
                const words = val.split('/');
                // $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
        <script type="text/javascript">window.gdprAppliesGlobally = true;
            (function () {
                function a(e) {
                    if (!window.frames[e]) {
                        if (document.body && document.body.firstChild) {
                            var t = document.body;
                            var n = document.createElement("iframe");
                            n.style.display = "none";
                            n.name = e;
                            n.title = e;
                            t.insertBefore(n, t.firstChild)
                        } else {
                            setTimeout(function () {
                                a(e)
                            }, 5)
                        }
                    }
                }
                function e(n, r, o, c, s) {
                    function e(e, t, n, a) {
                        if (typeof n !== "function") {
                            return
                        }
                        if (!window[r]) {
                            window[r] = []
                        }
                        var i = false;
                        if (s) {
                            i = s(e, t, n)
                        }
                        if (!i) {
                            window[r].push({command: e, parameter: t, callback: n, version: a})
                        }
                    }
                    e.stub = true;
                    function t(a) {
                        if (!window[n] || window[n].stub !== true) {
                            return
                        }
                        if (!a.data) {
                            return
                        }
                        var i = typeof a.data === "string";
                        var e;
                        try {
                            e = i ? JSON.parse(a.data) : a.data
                        } catch (t) {
                            return
                        }
                        if (e[o]) {
                            var r = e[o];
                            window[n](r.command, r.parameter, function (e, t) {
                                var n = {};
                                n[c] = {returnValue: e, success: t, callId: r.callId};
                                a.source.postMessage(i ? JSON.stringify(n) : n, "*")
                            }, r.version)
                        }
                    }
                    if (typeof window[n] !== "function") {
                        window[n] = e;
                        if (window.addEventListener) {
                            window.addEventListener("message", t, false)
                        } else {
                            window.attachEvent("onmessage", t)
                        }
                    }
                }
                e("__tcfapi", "__tcfapiBuffer", "__tcfapiCall", "__tcfapiReturn");
                a("__tcfapiLocator");
                (function (e) {
                    var t = document.createElement("script");
                    t.id = "spcloader";
                    t.type = "text/javascript";
                    t.async = true;
                    t.src = "https://sdk.privacy-center.org/" + e + "/loader.js?target=" + document.location.hostname;
                    t.charset = "utf-8";
                    var n = document.getElementsByTagName("script")[0];
                    n.parentNode.insertBefore(t, n)
                })("f0c52c20-b8cf-485b-a4b4-c222da28676d")
            })();</script>
    </body>
</html>