<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (!isset($_SESSION['data_login'])) {
    echo "<script>window.location='" . url . "nos-offres'</script>";
}
$monUrl = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$_SESSION['utm_source'] = $monUrl;
$id_secteur = degenerate_id($_GET['id']);
//$id_secteur = degenerate_id(5234);
$mon_secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur where id=" . $id_secteur));
$ligne_secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur_secteur where id_secteur=" . $id_secteur));
$ligne_france = mysqli_fetch_array(mysqli_query($link, "select * from secteur_france where 1"));

$year = date("Y");
$previousyear = $year - 1;

$users = mysqli_fetch_array(mysqli_query($link, "select premium,id from user where email='" . $_SESSION['data_login'] . "'"));
mysqli_query($link, "insert into user_vu_secteur(user,email,secteur,dt)values(" . $users['id'] . ",'".$_SESSION['data_login']."'," . $id_secteur . ",'" . date('Y-m-d H:i:s') . "')");

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}

$date1 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d'), date('y')));
$date2 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d') - 30, date('y')));
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">

        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/maps.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/franceLow.min.js"></script>
        <script>
            am4core.addLicense("CH303325752");
            am4core.addLicense("MP307346529");
        </script>
        <script>
            const   themeColorBlue = am4core.color('#00bff0'),
                    themeColorFill = am4core.color('#E1F2FA'),
                    themeColorGrey = am4core.color('#dadada'),
                    themeColorLightGrey = am4core.color('#F5F5F5'),
                    themeColorDarkgrey = am4core.color("#33333A"),
                    themeColorLine = am4core.color("#87D6E3"),
                    themeColorLineRed = am4core.color("#FF7C7C"),
                    themeColorLineGreen = am4core.color("#21D59B"),
                    themeColorColumns = am4core.color("#E1F2FA"),
                    labelColor = am4core.color('#798a9a');
        </script>
        <style>
            a{
                color:#323239;
            }
        </style>
        <script>
            (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({'gtm.start':
                    new Date().getTime(), event: 'gtm.js'});
            var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">
                    <section class="module">
                        <div class="module__title">Étude secteur <?php echo ($mon_secteur['nom_secteur']) ?></div>
                        <div class="ctg ctg--1_3">
                            <aside class="bloc text-center">
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Startups du secteur</div>
                                    <div class="datasbloc__val">
                                        <?php echo number_format($ligne_secteur['nbr_startups'], 0, ".", " "); ?>
                                    </div>
                                    <div class="datasbloc__subtitle">
                                        <?php echo str_replace(',0', '', number_format($ligne_secteur['startup_france'], 1, ",", " ")) ?>% 
                                        <?php
                                        if ($ligne_secteur['evo_startup_france'] == 0) {
                                            ?>
                                            <span class="ico-stable"></span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if ($ligne_secteur['evo_startup_france'] == 1) {
                                            ?>
                                            <span class="ico-raise"></span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if ($ligne_secteur['evo_startup_france'] == -1) {
                                            ?>
                                            <span class="ico-decrease"></span>
                                            <?php
                                        }
                                        ?>
                                        France
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Emplois créés</div>
                                    <div class="datasbloc__val"><?php
                                        echo number_format($ligne_secteur['effectif'], 0, ".", " ");
                                        ?></div>
                                    <div class="datasbloc__subtitle">
                                        <!--todo --> <?php echo str_replace(",0", "", number_format($ligne_secteur['effectif_france'], 1, ",", "")) ?>% 
                                        <?php
                                        if ($ligne_secteur['evo_effectif'] == 0) {
                                            ?>
                                            <span class="ico-stable"></span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if ($ligne_secteur['evo_effectif'] == 1) {
                                            ?>
                                            <span class="ico-raise"></span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if ($ligne_secteur['evo_effectif'] == -1) {
                                            ?>
                                            <span class="ico-decrease"></span>
                                            <?php
                                        }
                                        ?>
                                        France
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Total fonds levés</div>
                                    <div class="datasbloc__val"><?php echo number_format(($ligne_secteur['total_invest'] / 1000), 1, ",", " ") ?> M€</div>
                                    <div class="datasbloc__subtitle">
                                        <?php echo str_replace(',0', '', number_format($ligne_secteur['invest_france'], 1, ",", " ")) ?>% 
                                        <?php
                                        if ($ligne_secteur['evo_invest_france'] == 0) {
                                            ?>
                                            <span class="ico-stable"></span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if ($ligne_secteur['evo_invest_france'] == 1) {
                                            ?>
                                            <span class="ico-raise"></span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if ($ligne_secteur['evo_invest_france'] == -1) {
                                            ?>
                                            <span class="ico-decrease"></span>
                                            <?php
                                        }
                                        ?>

                                        France
                                    </div>
                                </div>
                            </aside>

                            <div class="bloc">
                                <div class="ctg ctg--2_2 ctg--fullheight">
                                    <div class="chart chart--radar">
                                        <div id="chart-radar"></div>
                                    </div>
                                    <script>
                                        am4core.ready(function () {


                                        /* Create chart instance */
                                        var chart = am4core.create("chart-radar", am4charts.RadarChart);
                                        /* Add data */
                                        chart.data = [{
                                        "date": "A",
                                                "value": <?php echo $ligne_secteur['indice_competition_secteur']; ?>,
                                                "value2": <?php echo number_format($ligne_france['indice_competition_secteur'], 1, ".", ""); ?>
                                        }, {
                                        "date": "B",
                                                "value": <?php echo $ligne_secteur['indice_maturite'] ?>,
                                                "value2": <?php echo number_format($ligne_france['indice_maturite'], 1, ".", ""); ?>
                                        }, {
                                        "date": "C",
                                                "value": <?php echo $ligne_secteur['indice_transparence'] ?>,
                                                "value2": <?php echo number_format($ligne_france['indice_transparence'], 1, ".", ""); ?>
                                        }, {
                                        "date": "D",
                                                "value": <?php echo $ligne_secteur['indice_anciennete'] ?>,
                                                "value2": <?php echo number_format($ligne_france['indice_anciennete'], 1, ".", ""); ?>
                                        }, {
                                        "date": "E",
                                                "value": <?php echo $ligne_secteur['indice_montant_investissement'] ?>,
                                                "value2": <?php echo number_format($ligne_france['indice_montant_investissement'], 1, ".", ""); ?>
                                        }, {
                                        "date": "F",
                                                "value": <?php echo $ligne_secteur['intensite_financement']; ?>,
                                                "value2": <?php echo number_format($ligne_france['intensite_financement'], 1, ".", ""); ?>
                                        }];
                                        chart.paddingTop = 0;
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;
                                        /* Create axes */
                                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.dataFields.category = "date";
                                        categoryAxis.renderer.ticks.template.strokeWidth = 2;
                                        categoryAxis.renderer.labels.template.fontSize = 11;
                                        categoryAxis.renderer.labels.template.fill = labelColor;
                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
                                        valueAxis.renderer.axisFills.template.fillOpacity = 0.05;
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.events.on("ready", function (ev) {
                                        ev.target.min = 0;
                                        ev.target.max = 10;
                                        })

                                                valueAxis.renderer.gridType = "polygons"


                                                /* Create and configure series */
                                                        function createSeries(field, name, themeColor) {
                                                        var series = chart.series.push(new am4charts.RadarSeries());
                                                        series.dataFields.valueY = field;
                                                        series.dataFields.categoryX = "date";
                                                        series.strokeWidth = 2;
                                                        series.fill = themeColor;
                                                        series.fillOpacity = 0.3;
                                                        series.stroke = themeColor;
                                                        series.name = name;
                                                        // Show bullets ?
                                                        //let circleBullet = series.bullets.push(new am4charts.CircleBullet());
                                                        //circleBullet.circle.stroke = am4core.color('#fff');
                                                        //circleBullet.circle.strokeWidth = 2;
                                                        }


                                                createSeries("value", "Secteur", themeColorBlue);
                                                createSeries("value2", "France", themeColorGrey);
                                                chart.legend = new am4charts.Legend();
                                                chart.legend.position = "top";
                                                chart.legend.useDefaultMarker = true;
                                                chart.legend.fontSize = "11";
                                                chart.legend.color = themeColorGrey;
                                                chart.legend.labels.template.fill = labelColor;
                                                chart.legend.labels.template.textDecoration = "none";
                                                chart.legend.valueLabels.template.textDecoration = "none";
                                                let as = chart.legend.labels.template.states.getKey("active");
                                                as.properties.textDecoration = "line-through";
                                                as.properties.fill = themeColorDarkgrey;
                                                let marker = chart.legend.markers.template.children.getIndex(0);
                                                marker.cornerRadius(12, 12, 12, 12);
                                                marker.width = 20;
                                                marker.height = 20;
                                                });
                                    </script>
                                    <div class="datagrid">
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">A :</span> Compétition
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_secteur['indice_competition_secteur']); ?> 
                                                <?php
                                                if ($ligne_secteur['evo_competition_secteur'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_secteur['evo_competition_secteur'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_secteur['evo_competition_secteur'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['indice_competition_secteur'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">B :</span> Maturité
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_secteur['indice_maturite']); ?> 
                                                <?php
                                                if ($ligne_secteur['evo_maturite'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_secteur['evo_maturite'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_secteur['evo_maturite'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['indice_maturite'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">C :</span> Transparence
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_secteur['indice_transparence']); ?> 
                                                <?php
                                                if ($ligne_secteur['evo_transparence'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_secteur['evo_transparence'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_secteur['evo_transparence'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo number_format($ligne_france['indice_transparence'], 1, ",", ""); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">D :</span> Ancienneté
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(",0", '', str_replace(".", ",", $ligne_secteur['indice_anciennete'])); ?>
                                                <?php
                                                if ($ligne_secteur['evo_anciennete'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_secteur['evo_anciennete'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_secteur['evo_anciennete'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", '', number_format($ligne_france['indice_anciennete'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">E :</span> Montant moyen des financements
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_secteur['indice_montant_investissement']); ?> 
                                                <?php
                                                if ($ligne_secteur['evo_montant_investissement'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_secteur['evo_montant_investissement'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_secteur['evo_montant_investissement'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(',0', '', number_format($ligne_france['indice_montant_investissement'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">F :</span> Intensité des financements
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(",0", '', number_format($ligne_secteur['intensite_financement'], 1, ",", "")); ?> 
                                                <?php
                                                if ($ligne_secteur['evo_financement'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_secteur['evo_financement'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_secteur['evo_financement'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo number_format($ligne_france['intensite_financement'], 1, ",", ""); ?></div>
                                        </div>
                                    </div>

                                    <div class="bloc-actions">
                                        <div class="bloc-actions__ico">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Financement secteur <?php echo ($mon_secteur['nom_secteur']) ?></div>

                        <div class="ctg ctg--3_3">
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financements Seed</div>
                                        <div class="datasbloc__val nowrap">Moy. <?php echo str_replace(",0", "", number_format($ligne_secteur['moy_1'] / 1000, 1, ",", " ")) ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($ligne_secteur['nb_1']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($ligne_secteur['min_1'] / 1000, 1, ",", " ")) ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($ligne_secteur['max_1'] / 1000, 1, ",", " ")) ?> M€ </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financements Croissance</div>
                                        <div class="datasbloc__val nowrap">Moy. <?php echo str_replace(",0", "", number_format($ligne_secteur['moy_2'] / 1000, 1, ",", " ")) ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($ligne_secteur['nb_2']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($ligne_secteur['min_2'] / 1000, 1, ",", " ")) ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($ligne_secteur['max_2'] / 1000, 1, ",", " ")) ?> M€</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financements Développement</div>
                                        <div class="datasbloc__val nowrap">Moy. <?php echo str_replace(",0", "", number_format($ligne_secteur['moy_3'] / 1000, 1, ",", " ")) ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($ligne_secteur['nb_3']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($ligne_secteur['min_3'] / 1000, 1, ",", " ")) ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($ligne_secteur['max_3'] / 1000, 1, ",", " ")) ?> M€</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Historique financements du secteur</div>
                                <div class="chart chart--columns">
                                    <div id="chart-financement"></div>
                                </div>
                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                    var chart = am4core.create("chart-financement", am4charts.XYChart);
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    // will use this to store colors of the same items
                                    var colors = {};
                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "category";
                                    categoryAxis.renderer.minGridDistance = 10;
                                    categoryAxis.renderer.fontSize = "10px";
                                    categoryAxis.renderer.labels.template.fill = labelColor;
                                    categoryAxis.dataItems.template.text = "{realName}";
                                    categoryAxis.renderer.grid.disabled = true;
                                    // Title Bottom
                                    categoryAxis.title.text = "Années";
                                    categoryAxis.title.rotation = 0;
                                    categoryAxis.title.align = "center";
                                    categoryAxis.title.valign = "bottom";
                                    categoryAxis.title.dy = - 5;
                                    categoryAxis.title.fontSize = 11;
                                    categoryAxis.title.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    // Title left
                                    valueAxis.title.text = "Nombre de startups financées";
                                    valueAxis.title.rotation = - 90;
                                    valueAxis.title.align = "left";
                                    valueAxis.title.valign = "top";
                                    valueAxis.title.dy = 0;
                                    valueAxis.title.fontSize = 11;
                                    valueAxis.title.fill = labelColor;
                                    // single column series for all data
                                    var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                    columnSeries.fill = themeColorColumns;
                                    columnSeries.stroke = 0;
                                    columnSeries.columns.template.width = am4core.percent(80);
                                    columnSeries.dataFields.categoryX = "category";
                                    columnSeries.dataFields.valueY = "value";
                                    // second value axis for quantity
                                    var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis2.renderer.opposite = true;
                                    valueAxis2.syncWithAxis = valueAxis;
                                    valueAxis2.tooltip.disabled = true;
                                    valueAxis2.renderer.labels.template.fontSize = 11;
                                    valueAxis2.renderer.labels.template.fill = labelColor;
                                    // Title right
                                    valueAxis2.title.text = "Financement total (M€)";
                                    valueAxis2.title.rotation = - 90;
                                    valueAxis2.title.align = "left";
                                    valueAxis2.title.valign = "top";
                                    valueAxis2.title.dy = 0;
                                    valueAxis2.title.fontSize = 11;
                                    valueAxis2.title.fill = labelColor;
                                    // quantity line series
                                    var lineSeries = chart.series.push(new am4charts.LineSeries());
                                    lineSeries.dataFields.categoryX = "category";
                                    lineSeries.dataFields.valueY = "financement";
                                    lineSeries.yAxis = valueAxis2;
                                    lineSeries.bullets.push(new am4charts.CircleBullet());
                                    lineSeries.fill = themeColorLine;
                                    lineSeries.strokeWidth = 2;
                                    lineSeries.fillOpacity = 0;
                                    lineSeries.stroke = themeColorLine;
                                    // when data validated, adjust location of data item based on count
                                    lineSeries.events.on("datavalidated", function () {
                                    lineSeries.dataItems.each(function (dataItem) {
                                    // if count divides by two, location is 0 (on the grid)
                                    if (dataItem.dataContext.count / 2 == Math.round(dataItem.dataContext.count / 2)) {
                                    dataItem.setLocation("categoryX", 0);
                                    }
                                    // otherwise location is 0.5 (middle)
                                    else {
                                    dataItem.setLocation("categoryX", 0.5);
                                    }
                                    })
                                    })

                                            ///// DATA
                                            var chartData = [];
                                    var lineSeriesData = [];
                                    var data =
                                    {
<?php
for ($i = 2014; $i <= date('Y'); $i++) {
    $nbr_somme = 0;
    $nbr_tot = 0;
    $verif_somme = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf inner join activite on activite.id_startup=lf.id_startup where rachat=0 and year(date_ajout)='" . $i . "' and activite.secteur=" . $id_secteur));
    $nbr_somme = $nbr_somme + $verif_somme['somme'];

    $verif_nb = mysqli_num_rows(mysqli_query($link, "select lf.id from lf inner join activite on activite.id_startup=lf.id_startup where rachat=0 and year(date_ajout)='" . $i . "' and activite.secteur=" . $id_secteur));
    $nbr_tot = $nbr_tot + $verif_nb;
    ?>
                                        "<?php echo $i; ?>": {
                                        "<?php echo $i; ?>": <?php echo $nbr_tot; ?>,
                                                "financement": <?php echo $nbr_somme / 1000; ?>
                                        },
    <?php
}
?>

                                    }

                                    // process data and prepare it for the chart
                                    for (var providerName in data) {
                                    var providerData = data[providerName];
                                    // add data of one provider to temp array
                                    var tempArray = [];
                                    var count = 0;
                                    // add items
                                    for (var itemName in providerData) {
                                    if (itemName != "financement") {
                                    count++;
                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                    }
                                    }
                                    // sort temp array
                                    tempArray.sort(function (a, b) {
                                    if (a.value > b.value) {
                                    return 1;
                                    } else if (a.value < b.value) {
                                    return - 1
                                    } else {
                                    return 0;
                                    }
                                    })

                                            // add quantity and count to middle data item (line series uses it)
                                            var lineSeriesDataIndex = Math.floor(count / 2);
                                    tempArray[lineSeriesDataIndex].financement = providerData.financement;
                                    tempArray[lineSeriesDataIndex].count = count;
                                    // push to the final data
                                    am4core.array.each(tempArray, function (item) {
                                    chartData.push(item);
                                    })
                                    }

                                    valueAxis.renderer.grid.template.strokeWidth = 0;
                                    //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                    categoryAxis.renderer.grid.template.strokeWidth = 0;
                                    chart.data = chartData;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Maturité des startups financées</div>
                                <div class="chart chart--columns">
                                    <div id="chart-maturite"></div>
                                </div>
                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                    var chart = am4core.create("chart-maturite", am4charts.XYChart);
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    // will use this to store colors of the same items
                                    var colors = {};
                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "category";
                                    categoryAxis.renderer.minGridDistance = 10;
                                    categoryAxis.renderer.fontSize = "10px";
                                    categoryAxis.renderer.labels.template.fill = labelColor;
                                    categoryAxis.dataItems.template.text = "{realName}";
                                    categoryAxis.renderer.grid.disabled = true;
                                    // Title Bottom
                                    categoryAxis.title.text = "Ancienneté des startups au moment de l’investissement";
                                    categoryAxis.title.rotation = 0;
                                    categoryAxis.title.align = "center";
                                    categoryAxis.title.valign = "bottom";
                                    categoryAxis.title.dy = - 5;
                                    categoryAxis.title.fontSize = 11;
                                    categoryAxis.title.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    // Title left
                                    valueAxis.title.text = "Nombre de startups financées";
                                    valueAxis.title.rotation = - 90;
                                    valueAxis.title.align = "left";
                                    valueAxis.title.valign = "top";
                                    valueAxis.title.dy = 0;
                                    valueAxis.title.fontSize = 11;
                                    valueAxis.title.fill = labelColor;
                                    // single column series for all data
                                    var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                    columnSeries.fill = themeColorColumns;
                                    columnSeries.stroke = 0;
                                    columnSeries.columns.template.width = am4core.percent(80);
                                    columnSeries.dataFields.categoryX = "category";
                                    columnSeries.dataFields.valueY = "value";
                                    // second value axis for quantity
                                    var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis2.renderer.opposite = true;
                                    valueAxis2.syncWithAxis = valueAxis;
                                    valueAxis2.tooltip.disabled = true;
                                    valueAxis2.renderer.labels.template.fontSize = 11;
                                    valueAxis2.renderer.labels.template.fill = labelColor;
                                    // Title right
                                    valueAxis2.title.text = "Financement total (M€)";
                                    valueAxis2.title.rotation = - 90;
                                    valueAxis2.title.align = "left";
                                    valueAxis2.title.valign = "top";
                                    valueAxis2.title.dy = 0;
                                    valueAxis2.title.fontSize = 11;
                                    valueAxis2.title.fill = labelColor;
                                    // quantity line series
                                    var lineSeries = chart.series.push(new am4charts.LineSeries());
                                    lineSeries.dataFields.categoryX = "category";
                                    lineSeries.dataFields.valueY = "financement";
                                    lineSeries.yAxis = valueAxis2;
                                    lineSeries.bullets.push(new am4charts.CircleBullet());
                                    lineSeries.fill = themeColorLine;
                                    lineSeries.strokeWidth = 2;
                                    lineSeries.fillOpacity = 0;
                                    lineSeries.stroke = themeColorLine;
                                    // when data validated, adjust location of data item based on count
                                    lineSeries.events.on("datavalidated", function () {
                                    lineSeries.dataItems.each(function (dataItem) {
                                    // if count divides by two, location is 0 (on the grid)
                                    if (dataItem.dataContext.count / 2 == Math.round(dataItem.dataContext.count / 2)) {
                                    dataItem.setLocation("categoryX", 0);
                                    }
                                    // otherwise location is 0.5 (middle)
                                    else {
                                    dataItem.setLocation("categoryX", 0.5);
                                    }
                                    })
                                    })

                                            ///// DATA
                                            var chartData = [];
                                    var lineSeriesData = [];
                                    var data =
                                    {
<?php
for ($i = date('Y'); $i >= 2014; $i--) {
    $nbr_somme = 0;
    $nbr_tot = 0;
    $verif_somme = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf inner join activite on activite.id_startup=lf.id_startup inner join startup on startup.id=lf.id_startup where rachat=0 and year(startup.date_complete)='" . $i . "' and activite.secteur=" . $id_secteur));
    $nbr_somme = $nbr_somme + $verif_somme['somme'];

    $verif_nb = mysqli_num_rows(mysqli_query($link, "select lf.id from lf inner join activite on activite.id_startup=lf.id_startup inner join startup on startup.id=lf.id_startup where rachat=0 and year(startup.date_complete)='" . $i . "' and activite.secteur=" . $id_secteur));
    ?>
                                        "<?php echo 2021 - $i + 1; ?> ans": {
                                        "<?php echo 2021 - $i + 1; ?> ans": <?php echo $verif_nb; ?>,
                                                "financement": <?php echo $nbr_somme / 1000; ?>
                                        },
    <?php
}
?>

                                    }

                                    // process data and prepare it for the chart
                                    for (var providerName in data) {
                                    var providerData = data[providerName];
                                    // add data of one provider to temp array
                                    var tempArray = [];
                                    var count = 0;
                                    // add items
                                    for (var itemName in providerData) {
                                    if (itemName != "financement") {
                                    count++;
                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                    }
                                    }
                                    // sort temp array
                                    tempArray.sort(function (a, b) {
                                    if (a.value > b.value) {
                                    return 1;
                                    } else if (a.value < b.value) {
                                    return - 1
                                    } else {
                                    return 0;
                                    }
                                    })

                                            // add quantity and count to middle data item (line series uses it)
                                            var lineSeriesDataIndex = Math.floor(count / 2);
                                    tempArray[lineSeriesDataIndex].financement = providerData.financement;
                                    tempArray[lineSeriesDataIndex].count = count;
                                    // push to the final data
                                    am4core.array.each(tempArray, function (item) {
                                    chartData.push(item);
                                    })
                                    }

                                    valueAxis.renderer.grid.template.strokeWidth = 0;
                                    //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                    categoryAxis.renderer.grid.template.strokeWidth = 0;
                                    chart.data = chartData;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Startups du secteur en recherche de fonds</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="moneyCell text-center">Montant recherché</th>
                                                <th class="dateCell text-center">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $list_prob = explode(",", $ligne_secteur['last_deal']);
                                            $nbrs = count($list_prob);
                                            for ($i = 0; $i < $nbrs; $i++) {

                                                $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation,date_complete from startup where id=" . $list_prob[$i]));
                                                $data_secteur4 = mysqli_fetch_array(mysqli_query($link, "SELECT * FROM  startup_deal_flow  where id_startup=" . $list_prob[$i] . " order by startup_deal_flow.dt desc limit 1"));
                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="logoNameCell">
                                                            <div class="logo">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt="<?php echo ($ma_startup['nom']); ?>" /></a>
                                                            </div>
                                                            <div class="name"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo ($ma_startup['nom']); ?></a></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center moneyCell"><?php echo str_replace(",0", "", number_format($data_secteur4['montant'] / 1000, 1, ",", " ")); ?> M€</td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($data_secteur4['dt']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Top 10 probabilité levée de fonds à 6 mois</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="dateCell text-center">Création</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $list_prob = explode(",", $ligne_secteur['last_prob']);
                                            $nbrs = count($list_prob);
                                            for ($i = 0; $i < $nbrs; $i++) {

                                                $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation,date_complete from startup where id=" . $list_prob[$i]));
                                                $tot_lev_prob = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $list_prob['id']));
                                                ?>


                                                <tr>
                                                    <td>
                                                        <div class="logoNameCell">
                                                            <div class="logo">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt="<?php echo ($ma_startup['nom']); ?>" /></a>
                                                            </div>
                                                            <div class="name"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo ($ma_startup['nom']); ?></a></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($ma_startup['date_complete']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Marché et activités secteur <?php echo ($mon_secteur['nom_secteur']) ?></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Dynamiques du secteur </div>
                                <div class="chart chart--xy">
                                    <div id="chart-dynamiquesecteur"></div>
                                </div>
                                <script>
                                    am4core.ready(function () {
                                    var chart = am4core.create("chart-dynamiquesecteur", am4charts.XYChart);
                                    chart.paddingTop = 0;
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    chart.data = [

                                    {date: new Date(2014, 5, 12), value1: <?php echo $ligne_secteur['cree_2014']; ?>, value2: <?php echo $ligne_secteur['defail_2014']; ?>, value3: <?php echo $ligne_secteur['aqui_ipo_2014']; ?>, previousDate: new Date(2014, 5, 5)},
                                    {date: new Date(2015, 5, 12), value1: <?php echo $ligne_secteur['cree_2015']; ?>, value2: <?php echo $ligne_secteur['defail_2015']; ?>, value3: <?php echo $ligne_secteur['aqui_ipo_2015']; ?>, previousDate: new Date(2015, 5, 5)},
                                    {date: new Date(2016, 5, 12), value1: <?php echo $ligne_secteur['cree_2016']; ?>, value2: <?php echo $ligne_secteur['defail_2016']; ?>, value3: <?php echo $ligne_secteur['aqui_ipo_2016']; ?>, previousDate: new Date(2016, 5, 5)},
                                    {date: new Date(2017, 5, 12), value1: <?php echo $ligne_secteur['cree_2017']; ?>, value2: <?php echo $ligne_secteur['defail_2017']; ?>, value3: <?php echo $ligne_secteur['aqui_ipo_2017']; ?>, previousDate: new Date(2017, 5, 5)},
                                    {date: new Date(2018, 5, 12), value1: <?php echo $ligne_secteur['cree_2018']; ?>, value2: <?php echo $ligne_secteur['defail_2018']; ?>, value3: <?php echo $ligne_secteur['aqui_ipo_2018']; ?>, previousDate: new Date(2018, 5, 5)},
                                    {date: new Date(2019, 5, 12), value1: <?php echo $ligne_secteur['cree_2019']; ?>, value2: <?php echo $ligne_secteur['defail_2019']; ?>, value3: <?php echo $ligne_secteur['aqui_ipo_2019']; ?>, previousDate: new Date(2019, 5, 5)},
                                    {date: new Date(2020, 5, 12), value1: <?php echo $ligne_secteur['cree_2020']; ?>, value2: <?php echo $ligne_secteur['defail_2020']; ?>, value3: <?php echo $ligne_secteur['aqui_ipo_2020']; ?>, previousDate: new Date(2020, 5, 5)},
                                    {date: new Date(2021, 5, 12), value1: <?php echo $ligne_secteur['cree_2021']; ?>, value2: <?php echo $ligne_secteur['defail_2021']; ?>, value3: <?php echo $ligne_secteur['aqui_ipo_2021']; ?>, previousDate: new Date(2021, 5, 5)},
                                    {date: new Date(2022, 5, 12), value1: <?php echo $ligne_secteur['cree_2022']; ?>, value2: <?php echo $ligne_secteur['defail_2022']; ?>, value3: <?php echo $ligne_secteur['aqui_ipo_2022']; ?>, previousDate: new Date(2022, 5, 5)},
                                    ]

                                            // Create axes
                                            var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                                    dateAxis.renderer.minGridDistance = 50;
                                    dateAxis.renderer.labels.template.fontSize = 11;
                                    dateAxis.renderer.labels.template.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    /* Create and configure series */
                                    function createSeries(field, name, themeColor) {
                                    var series = chart.series.push(new am4charts.LineSeries());
                                    series.dataFields.valueY = field;
                                    series.dataFields.dateX = "date";
                                    series.strokeWidth = 2;
                                    series.stroke = themeColor;
                                    series.name = name;
                                    series.tensionX = 0.77;
                                    series.fill = themeColor;
                                    }


                                    createSeries("value1", "Créations", themeColorBlue);
                                    createSeries("value2", "Défaillances", themeColorLineGreen);
                                    createSeries("value3", "Acquisitions - IPO", themeColorLineRed);
                                    chart.zoomOutButton.disabled = true;
                                    chart.legend = new am4charts.Legend();
                                    chart.legend.position = "top";
                                    chart.legend.useDefaultMarker = true;
                                    chart.legend.fontSize = "11";
                                    chart.legend.color = themeColorGrey;
                                    chart.legend.labels.template.fill = labelColor;
                                    chart.legend.labels.template.textDecoration = "none";
                                    chart.legend.valueLabels.template.textDecoration = "none";
                                    let as = chart.legend.labels.template.states.getKey("active");
                                    as.properties.textDecoration = "line-through";
                                    as.properties.fill = themeColorDarkgrey;
                                    let marker = chart.legend.markers.template.children.getIndex(0);
                                    marker.cornerRadius(12, 12, 12, 12);
                                    marker.width = 20;
                                    marker.height = 20;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                            <div class="bloc">

                                <?php
                                $sql_region = mysqli_query($link, "select count(*) as nb, activite.sous_secteur from startup inner join activite on activite.id_startup=startup.id where startup.status=1 and sup_radie=0 and activite.sous_secteur!=0 and activite.secteur=" . $id_secteur . " group by activite.sous_secteur order by nb desc limit 10");
                                $nbs = mysqli_num_rows($sql_region);
                                if ($nbs > 0) {
                                    ?>
                                    <div class="bloc__title">Répartition des startups par sous-secteurs</div>
                                    <div class="tablebloc">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th class="iconCell">Évol.</th>
                                                    <th>Sous secteur</th>
                                                    <th class="text-center">Startups</th>
                                                </tr>
                                            </tbody>
                                            <tbody>
                                                <?php
                                                $tab_list_deps = explode(";", $ligne_secteur['list_sous_secteur']);
                                                $tab_list_nbr_deps = explode(";", $ligne_secteur['nbr_sous_secteur']);
                                                $tab_list_evo_nbr_dep = explode(";", $ligne_secteur['evo_sous_secteur']);
                                                $b = count($tab_list_deps);
                                                for ($i = 0; $i < 10; $i++) {
                                                    if ($tab_list_deps[$i] != "") {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                if ($tab_list_evo_nbr_dep[$i] == -1) {
                                                                    ?>
                                                                    <span class="ico-decrease"></span>
                                                                <?php } ?>
                                                                <?php
                                                                if ($tab_list_evo_nbr_dep[$i] == 1) {
                                                                    ?>
                                                                    <span class="ico-raise"></span>
                                                                <?php } ?>
                                                                <?php
                                                                if ($tab_list_evo_nbr_dep[$i] == 0) {
                                                                    ?>
                                                                    <span class="ico-stable"></span>
                                                                <?php } ?>
                                                            </td>
                                                            <td><?php
                                                                if ($tab_list_deps[$i] != '')
                                                                    echo ($tab_list_deps[$i]);
                                                                else
                                                                    echo "NC";
                                                                ?></td>
                                                            <td class="text-center"><?php echo $tab_list_nbr_deps[$i]; ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="bloc__title">Répartition des startups par sous-secteurs</div>

                                    <div class="alert alert--infos">Pas de sous secteurs.</div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Dernières créations de startups <?php echo ($mon_secteur['nom_secteur']) ?></div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="dateCell text-center">Création</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_naf = mysqli_query($link, "select startup.logo,startup.nom,startup.creation,activite.activite,startup.date_complete,startup.id from startup inner join activite on activite.id_startup=startup.id where startup.status=1 and sup_radie=0 and activite.secteur=" . $id_secteur . " order by date_complete desc limit 5");
                                            while ($data_naf = mysqli_fetch_array($sql_naf)) {
                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="logoNameCell">
                                                            <div class="logo">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($data_naf['id']) . "/" . urlWriting(strtolower($data_naf["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $data_naf['logo']) ?>" alt="<?php echo ($data_naf['nom']); ?>" /></a>
                                                            </div>
                                                            <div class="name"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($data_naf['id']) . "/" . urlWriting(strtolower($data_naf["nom"])) ?>" target="_blank"><?php echo ($data_naf['nom']); ?></a></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($data_naf['date_complete']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Dernières acquisitions de startups <?php echo ($mon_secteur['nom_secteur']) ?></div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="moneyCell text-center">Montant</th>
                                                <th class="dateCell text-center">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_secteur2 = mysqli_query($link, "SELECT startup.logo,startup.id,startup.nom,lf.date_ajout,lf.montant FROM `startup` inner join activite on startup.id=activite.id_startup inner join lf on lf.id_startup=startup.id where startup.status=1 and sup_radie=0 and activite.secteur=" . $id_secteur . " and lf.rachat=1 order by date_ajout desc limit 5");
                                            while ($data_secteur2 = mysqli_fetch_array($sql_secteur2)) {
                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="logoNameCell">
                                                            <div class="logo">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($data_secteur2['id']) . "/" . urlWriting(strtolower($data_secteur2["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $data_secteur2['logo']) ?>" alt="<?php echo ($data_secteur2['nom']); ?>" /></a>
                                                            </div>
                                                            <div class="name"> <a href="<?php echo URL . '/fr/startup-france/' . generate_id($data_secteur2['id']) . "/" . urlWriting(strtolower($data_secteur2["nom"])) ?>" target="_blank"><?php echo ($data_secteur2['nom']); ?></a></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center moneyCell"><?php
                                                        if ($data_secteur2['montant'] != 0)
                                                            echo str_replace(",0", "", number_format($data_secteur2['montant'] / 1000, 1, ",", " ")) . " M€";
                                                        else
                                                            echo 'NC';
                                                        ?></td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($data_secteur2['date_ajout']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Géographie du secteur</div>

                                <div class="chart chart--map">
                                    <div id="chart-map"></div>
                                </div>

                                <script>

                                    // Create map instance
                                    var chart = am4core.create("chart-map", am4maps.MapChart);
                                    chart.paddingTop = 0;
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    // Set map definition
                                    chart.geodata = am4geodata_franceLow;
                                    // Set projection
                                    chart.projection = new am4maps.projections.Miller();
                                    // Create map polygon series
                                    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
                                    //Set min/max fill color for each area
                                    polygonSeries.heatRules.push({
                                    property: "fill",
                                            target: polygonSeries.mapPolygons.template,
                                            //min: chart.colors.getIndex(1).brighten(1),
                                            //max: chart.colors.getIndex(1).brighten(-0.3),
                                            logarithmic: true
                                    });
                                    // Make map load polygon data (state shapes and names) from GeoJSON
                                    polygonSeries.useGeodata = true;
                                    // Set heatmap values for each state
<?php
$region_Bretagne = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=17 and id_secteur=" . $id_secteur));
$region_PaysdelaLoire = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=12 and id_secteur=" . $id_secteur));
$region_GrandEst = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=18 and id_secteur=" . $id_secteur));
$region_Nouvelle_Aquitaine = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=1 and id_secteur=" . $id_secteur));
$region_Hauts_de_France = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=7 and id_secteur=" . $id_secteur));
$region_azur = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=2 and id_secteur=" . $id_secteur));
$region_Occitanie = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=4 and id_secteur=" . $id_secteur));
$region_Auvergne_Rhone_Alpes = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=5 and id_secteur=" . $id_secteur));
$region_Corse = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=3 and id_secteur=" . $id_secteur));
$region_Bourgogne_Franche_Comte = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=11 and id_secteur=" . $id_secteur));
$region_ile_e_France = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=8 and id_secteur=" . $id_secteur));
$region_Centre_Val_Loire = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=19 and id_secteur=" . $id_secteur));
$region_Normandie = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=15 and id_secteur=" . $id_secteur));
?>

                                    // Bleu 1 le + clair : #E2F4FC
                                    // Bleu 2 : #CEEAF7
                                    // Bleu 3 : #BEE7F8
                                    // Bleu 4 le + foncé : #ACDDF2
<?php

function valueFillColor($val) {
    if ($val < 40) {
        return "#E2F4FC";
    } elseif ($val >= 40 && $val <= 60) {
        return "#CEEAF7";
    } elseif ($val > 60 && $val <= 110) {
        return "#BEE7F8";
    } elseif ($val > 110) {
        return "#ACDDF2";
    }
}
?>

                                    polygonSeries.data = [
                                    {
                                    "id": "FR-BRE",
                                            "value": <?php echo $region_Bretagne['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_Bretagne['nbr_startups']); ?>")
                                            //"fill": am4core.color("#ff0000"),
                                    },
                                    {
                                    "id": "FR-IDF",
                                            "value": <?php echo $region_ile_e_France['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_ile_e_France['nbr_startups']); ?>")
                                    },
                                    {
                                    "id": "FR-HDF",
                                            "value": <?php echo $region_Hauts_de_France['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_Hauts_de_France['nbr_startups']); ?>")

                                    },
                                    {
                                    "id": "FR-GES",
                                            "value": <?php echo $region_GrandEst['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_GrandEst['nbr_startups']); ?>")

                                    },
                                    {
                                    "id": "FR-NOR",
                                            "value": <?php echo $region_Normandie['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_Normandie['nbr_startups']); ?>")

                                    }, {
                                    "id": "FR-PDL",
                                            "value": <?php echo $region_PaysdelaLoire['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_PaysdelaLoire['nbr_startups']); ?>")

                                    }, {
                                    "id": "FR-CVL",
                                            "value": <?php echo $region_Centre_Val_Loire['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_Centre_Val_Loire['nbr_startups']); ?>")

                                    }, {
                                    "id": "FR-BFC",
                                            "value": <?php echo $region_Bourgogne_Franche_Comte['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_Bourgogne_Franche_Comte['nbr_startups']); ?>")

                                    }, {
                                    "id": "FR-ARA",
                                            "value": <?php echo $region_Auvergne_Rhone_Alpes['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_Auvergne_Rhone_Alpes['nbr_startups']); ?>")

                                    }, {
                                    "id": "FR-PAC",
                                            "value": <?php echo $region_azur['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_azur['nbr_startups']); ?>")

                                    }, {
                                    "id": "FR-NAQ",
                                            "value": <?php echo $region_Nouvelle_Aquitaine['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_Nouvelle_Aquitaine['nbr_startups']); ?>")

                                    }, {
                                    "id": "FR-OCC",
                                            "value": <?php echo $region_Occitanie['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_Occitanie['nbr_startups']); ?>")

                                    }, {
                                    "id": "FR-COR",
                                            "value": <?php echo $region_Corse['nbr_startups'] ?>,
                                            "fill": am4core.color("<?php echo valueFillColor($region_Corse['nbr_startups']); ?>")

                                    },
                                    ];
                                    // Configure series tooltip
                                    var polygonTemplate = polygonSeries.mapPolygons.template;
                                    polygonTemplate.tooltipText = "[#fff font-size: 11px]{name}: {value}[/]";
                                    polygonTemplate.nonScalingStroke = true;
                                    polygonTemplate.strokeWidth = 1.5;
                                    polygonTemplate.propertyFields.fill = "fill";
                                    polygonTemplate.stroke = themeColorGrey;
                                    polygonTemplate.properties.fill = am4core.color("#E0E0E0");
                                    // Create hover state and set alternative fill color
                                    var hs = polygonTemplate.states.create("hover");
                                    hs.properties.fill = themeColorBlue;
                                    hs.properties.color = am4core.color("#ff0000");
                                    chart.seriesContainer.draggable = false;
                                    chart.seriesContainer.resizable = false;
                                    chart.maxZoomLevel = 1;
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Startups du secteur par département</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th class="iconCell">Évol.</th>
                                                <th>Sous secteur</th>
                                                <th class="text-center">Startups</th>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <?php
                                            $tab_list_deps = explode(";", $ligne_secteur['list_dep']);
                                            $tab_list_nbr_deps = explode(";", $ligne_secteur['nbr_dep']);
                                            $tab_list_evo_nbr_dep = explode(";", $ligne_secteur['evo_dep']);
                                            $b = count($tab_list_deps);
                                            for ($i = 0; $i < 10; $i++) {
                                                if ($tab_list_deps[$i] != "") {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php
                                                            if ($tab_list_evo_nbr_dep[$i] == -1) {
                                                                ?>
                                                                <span class="ico-decrease"></span>
                                                            <?php } ?>
                                                            <?php
                                                            if ($tab_list_evo_nbr_dep[$i] == 1) {
                                                                ?>
                                                                <span class="ico-raise"></span>
                                                            <?php } ?>
                                                            <?php
                                                            if ($tab_list_evo_nbr_dep[$i] == 0) {
                                                                ?>
                                                                <span class="ico-stable"></span>
                                                            <?php } ?>
                                                        </td>
                                                        <td><?php
                                                            if ($tab_list_deps[$i] != '')
                                                                echo ($tab_list_deps[$i]);
                                                            else
                                                                echo "NC";
                                                            ?></td>
                                                        <td class="text-center"><?php echo $tab_list_nbr_deps[$i]; ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="bloc">
                            <div class="bloc__title">Dernières levées de fonds du secteur <?php echo ($mon_secteur['nom_secteur']) ?></div>
                            <div class="table-responsive">
                                <table id="mine2" class="table table-search">
                                    <thead>
                                        <tr>
                                            <th class="table-search__startup">Startups</th>
                                            <th class="table-search__datedernierelevee text-center">Dernière levée de fonds (M€)</th>
                                            <th class="table-search__date text-center">Date dernière levée</th>
                                            <th class="table-search__region text-center">Région</th>
                                            <th class="table-search__maturite text-center">Maturité</th>
                                            <th class="table-search__date text-center">Création</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $list_last_lf = explode(",", $ligne_secteur['last_lf']);
                                        $nbrs = count($list_last_lf);
                                        for ($i = 0; $i < $nbrs; $i++) {

                                            $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation,date_complete,region_new from startup where id=" . $list_last_lf[$i]));
                                            $data_last_lf = mysqli_fetch_array(mysqli_query($link, "select lf.montant,lf.date_ajout from lf  where id_startup=" . $list_last_lf[$i]));
                                            $score = mysqli_fetch_array(mysqli_query($link, "Select  * From   startup_score where  id_startup=" . $ma_startup['id']));
                                            ?>
                                            <tr>
                                                <th class="table-search__startup">
                                                    <div class="startupInfos">
                                                        <div class="startupInfos__image">
                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt="<?php echo "Startup " . $ma_startup["nom"]; ?>"></a>
                                                        </div>
                                                        <div class="startupInfos__desc">
                                                            <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo stripslashes($ma_startup['nom']); ?></a></div>
                                                            <div class="companyLabel"><?php echo stripslashes($ma_startup['short_fr']); ?></div>
                                                        </div>
                                                    </div>
                                                </th>


                                                <td class="table-search__datedernierelevee text-center"><?php echo str_replace(",0", "", number_format($data_last_lf['montant'] / 1000, 1, ",", " ")); ?></td>
                                                <td class="table-search__date text-center">
                                                    <?php echo change_date_fr_chaine_related($data_last_lf['date_ajout']); ?> 
                                                </td>
                                                <td class="table-search__region text-center"><?php echo ($ma_startup['region_new']); ?></td>
                                                <td class="table-search__maturite text-center"> <?php echo $score['maturite']; ?> 
                                                    <?php if ($score['maturite'] == $ligne_secteur['indice_maturite']) { ?>
                                                        <span class="ico-stable"></span> 
                                                    <?php } ?>
                                                    <?php if ($score['maturite'] > $ligne_secteur['indice_maturite']) { ?>
                                                        <span class="ico-raise"></span> 
                                                    <?php } ?>
                                                    <?php if ($score['maturite'] < $ligne_secteur['indice_maturite']) { ?>
                                                        <span class="ico-decrease"></span> 
                                                    <?php } ?>
                                                </td>
                                                <td  class="table-search__date text-center">
                                                    <?php echo change_date_fr_chaine_related($ma_startup['date_complete']); ?> 
                                                </td>
                                            </tr>

                                            <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="bloc">
                            <div class="bloc__title">Dernières augmentations de capital secteur <?php echo ($mon_secteur['nom_secteur']) ?></div>
                            <div class="table-responsive">
                                <table id="mine" class="table table-search">
                                    <thead>
                                        <tr>
                                            <th class="table-search__startup">Startups</th>
                                            <th class="table-search__capaciteadelivrer text-center">Nouveau capital social (k€)</th>
                                            <th class="table-search__date text-center">Date modification de capital</th>
                                            <th class="table-search__region text-center">Région</th>
                                            <th class="table-search__maturite text-center">Maturité</th>
                                            <th class="table-search__date text-center">Création</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $list_capital_last = explode(",", $ligne_secteur['last_capital']);
                                        $nbrs_list = count($list_capital_last);
                                        for ($i = 0; $i < $nbrs_list; $i++) {

                                            $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation,date_complete,region_new from startup where id=" . $list_capital_last[$i]));
                                            $dernier_capital = mysqli_fetch_array(mysqli_query($link, "select * from startup_capital where id_startup=" . $ma_startup['id'] . " order by dt desc limit 1"));
                                            $score = mysqli_fetch_array(mysqli_query($link, "Select  * From   startup_score where  id_startup=" . $ma_startup['id']));
                                            ?>
                                            <tr>
                                                <th class="table-search__startup">
                                                    <div class="startupInfos">
                                                        <div class="startupInfos__image">
                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt="<?php echo "Startup " . $ma_startup["nom"]; ?>"></a>
                                                        </div>
                                                        <div class="startupInfos__desc">
                                                            <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo stripslashes($ma_startup['nom']); ?></a></div>
                                                            <div class="companyLabel"><?php echo stripslashes($ma_startup['short_fr']); ?></div>
                                                        </div>
                                                    </div>
                                                </th>

                                                <td class="table-search__capaciteadelivrer text-center"><?php echo str_replace(",0", "", number_format($dernier_capital['capital'] / 1000, 1, ",", " ")); ?></td>
                                                <td class="table-search__date text-center"><?php echo change_date_fr_chaine_related($dernier_capital['dt']); ?></td>
                                                <td class="table-search__region text-center"><?php echo ($ma_startup['region_new']); ?></td>
                                                <td class="table-search__maturite text-center"> <?php echo $score['maturite']; ?> 
                                                    <?php if ($score['maturite'] == $ligne_secteur['indice_maturite']) { ?>
                                                        <span class="ico-stable"></span> 
                                                    <?php } ?>
                                                    <?php if ($score['maturite'] > $ligne_secteur['indice_maturite']) { ?>
                                                        <span class="ico-raise"></span> 
                                                    <?php } ?>
                                                    <?php if ($score['maturite'] < $ligne_secteur['indice_maturite']) { ?>
                                                        <span class="ico-decrease"></span> 
                                                    <?php } ?>
                                                </td>
                                                <td class="table-search__date text-center">
                                                    <?php echo change_date_fr_chaine_related($ma_startup['date_complete']); ?> 
                                                </td>
                                            </tr>

                                            <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>





                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>jquery.dataTables.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>datatables.bootstrap.min.js?<?= time(); ?>"></script>

        <script>
                                    var table = jQuery('#mine').DataTable({
                                    "searching": false,
                                            "nextPrev": false,
                                            "bLengthChange": false,
                                            "bInfo": false,
                                            "bPaginate": true,
                                            "autoWidth": false,
                                            //"fixedColumns": false,
                                            initComplete: (settings, json) => {
                                    $('#paginateTable').empty();
                                    $('#mine_paginate').appendTo('#paginateTable');
                                    },
                                            "language": {
                                            "paginate": {
                                            "previous": "Précédent",
                                                    "next": "Suivant"
                                            }
                                            }
                                    });
                                    var table2 = jQuery('#mine2').DataTable({
                                    "searching": false,
                                            "nextPrev": false,
                                            "bLengthChange": false,
                                            "bInfo": false,
                                            "bPaginate": true,
                                            "autoWidth": false,
                                            //"fixedColumns": false,
                                            initComplete: (settings, json) => {
                                    $('#paginateTable2').empty();
                                    $('#mine2_paginate').appendTo('#paginateTable2');
                                    },
                                            "language": {
                                            "paginate": {
                                            "previous": "Précédent",
                                                    "next": "Suivant"
                                            }
                                            }
                                    });
        </script>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
                                    (function (i, s, o, g, r, a, m) {
                                    i['GoogleAnalyticsObject'] = r;
                                    i[r] = i[r] || function () {
                                    (i[r].q = i[r].q || []).push(arguments)
                                    }, i[r].l = 1 * new Date();
                                    a = s.createElement(o),
                                            m = s.getElementsByTagName(o)[0];
                                    a.async = 1;
                                    a.src = g;
                                    m.parentNode.insertBefore(a, m)
                                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                    ga('create', 'UA-36251023-1', 'auto');
                                    ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
            var $ = jQuery;
            var valeur = document.getElementById("search-box").value;
            $.ajax({
            type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                    $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#search-box").css("background", "#FFF");
                    }
            });
            }

            function selectCountry(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>