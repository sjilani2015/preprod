<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

$monUrl = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$_SESSION['utm_source'] = $monUrl;

if (!isset($_SESSION['data_login'])) {
    header("location:" . URL . "/nos-offres");
    exit();
} else {
    $oo = mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'");
    $us = mysqli_fetch_array($oo);
    if ($us['premium'] == 0) {
        header("location:" . URL . "/nos-offres");
        exit();
    }
}



/*
 * Récupérer id Startup
 */
//$_GET["code"] = 44449;
//$_GET["code"] = 285349;
if (isset($_GET["startup"]) && $_GET["startup"] != '') {
    $startup_id = $_GET["startup"];
    $code = $_GET["code"];
} else
    header('location:' . URL . '/404');


$code = $_GET["code"];
$id_startup = degenerate_id($code);
$verifs = mysqli_num_rows(mysqli_query($link, "select id from startup where id=" . $id_startup . " and status=1"));
if ($verifs != 1) {
    header('location:' . URL . '/404');
}

$_SESSION['id_sup'] = $id_startup;

$startup_capital = getStartupCapital($id_startup);
$nb_evolution = mysqli_num_rows(mysqli_query($link, "select id from startup_capital where id_startup=" . $id_startup));

$startup_ca = getStartupCA($id_startup);

$ca1 = str_replace(array("€", " ", ","), array("", "", "."), $startup_ca[0]["ca"]);
$ca2 = str_replace(array("€", " ", ","), array("", "", "."), $startup_ca[1]["ca"]);
$anneeca1 = $startup_ca[0]["annee"];
$anneeca2 = $startup_ca[1]["annee"];

$row_fondateur_startup = getFondateurByIdStartup($id_startup);

$startup = getStartupById($id_startup);

$_SESSION['startup_nom_revendiquer'] = $startup[0]['nom'];
$_SESSION['startup_id_revendiquer'] = $startup[0]['id'];

// echo strlen(substr($descriptionfr, 0, 155));
$row_nom_secteur = getSecteurByIdStartup($startup[0]['id']);
if ($row_nom_secteur[0]['id'] != "") {
    $row_nom_sous_secteur = getSousSecteurByIdStartup($startup[0]['id']);
}


$id_secteur = mysqli_fetch_array(mysqli_query($link, "Select  secteur.id,secteur.nom_secteur,secteur.nom_secteur_en From secteur inner join activite on activite.secteur=secteur.id Where  activite.id_startup =" . $startup[0]['id']));
$id_sous_secteur = mysqli_fetch_array(mysqli_query($link, "Select  sous_secteur.id,sous_secteur.nom_sous_secteur,sous_secteur.nom_sous_secteur_en From  sous_secteur inner join activite on activite.sous_secteur=sous_secteur.id Where  activite.id_startup =" . $startup[0]['id']));
$id_sous_activite = mysqli_fetch_array(mysqli_query($link, "Select  last_sous_activite.id,last_sous_activite.nom_sous_activite From  last_sous_activite inner join activite on activite.sous_activite=last_sous_activite.id Where  activite.id_startup =" . $startup[0]['id']));
$id_activite = mysqli_fetch_array(mysqli_query($link, "Select  last_activite.id,last_activite.nom_activite,last_activite.nom_activite_en From  last_activite inner join activite on activite.activite=last_activite.id Where  activite.id_startup =" . $startup[0]['id']));

if ($id_sous_activite['id'] == "")
    $idsousactivite = 0;
else
    $idsousactivite = $id_sous_activite['id'];

if ($id_sous_secteur['id'] == "")
    $idsoussecteur = 0;
else
    $idsoussecteur = $id_sous_secteur['id'];

if ($id_activite['id'] == "")
    $idactivite = 0;
else
    $idactivite = $id_activite['id'];


$ligne_secteur_activite = mysqli_fetch_array(mysqli_query($link, "select * from secteur_activite where id_secteur=" . $id_secteur['id'] . " and id_sous_secteur=" . $idsoussecteur . " and id_activite=" . $idactivite . " and id_sous_activite=" . $idsousactivite));
$ligne_secteur_secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur_secteur where id_secteur=" . $id_secteur['id']));
$ligne_france = mysqli_fetch_array(mysqli_query($link, "select * from secteur_france where 1"));

$somme_global_competition = mysqli_fetch_array(mysqli_query($link, "select sum(competition_secteur) as somme from secteur_activite"));
$nb_global_competition = mysqli_num_rows(mysqli_query($link, "select competition_secteur from secteur_activite where competition_secteur!=0"));
$moy_competition_secteur = $somme_global_competition['somme'] / $nb_global_competition;

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}

$chiffre = mysqli_fetch_array(mysqli_query($link, "select * from startup_score where id_startup=" . $startup[0]['id']));
if ($chiffre['maturite'] != "") {
    $maturite = $chiffre['maturite'];
    $matirute_courbe = $chiffre['maturite'];
} else {
    $maturite = "-";
    $matirute_courbe = 0;
}
if ($chiffre['anciennete'] != "") {
    $anciennete = $chiffre['anciennete'];
    $anciennete_courbe = $chiffre['anciennete'];
} else {
    $anciennete = "-";
    $anciennete_courbe = 0;
}
if ($chiffre['traction_likedin'] != "") {
    $traction = $chiffre['traction_likedin'];
    $traction_courbe = $chiffre['traction_likedin'];
} else {
    $traction = "-";
    $traction_courbe = 0;
}


$menu = 2;
?>
<?php
$tab_somme_total_lf = getTotalLfByStartup($startup[0]["id"]);

$tab_last_deal = getLastDealByStartup($startup[0]["id"]);
$row_total_investisseur = getTotalInvestisseurByStartup($startup[0]["id"]);
if (!empty($row_total_investisseur)) {
    $ch_invest = "";
    foreach ($row_total_investisseur as $deal) {
        $ch_invest .= stripslashes($deal['de']) . ",";
    }
}

$nb_founder = mysqli_num_rows(mysqli_query($link, "select id from personnes where id_startup=" . $startup[0]["id"]));

$tab_invest = explode(",", $ch_invest);

$verif_rachat = mysqli_num_rows(mysqli_query($link, "select id from lf where id_startup=" . $startup[0]["id"] . " and rachat=1 "));
$verif_ipo = mysqli_num_rows(mysqli_query($link, "select id from lf where id_startup=" . $startup[0]["id"] . " and rachat=2 "));

$tot_leve_france = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf inner join startup on startup.id=lf.id_startup inner join activite on startup.id=activite.id_startup  where startup.status=1 and lf.rachat=0 and activite.secteur=" . $row_nom_secteur[0]['id'] . ""));
$tot_nb_france = mysqli_fetch_array(mysqli_query($link, "select count(*) as nb from lf inner join startup on startup.id=lf.id_startup inner join activite on startup.id=activite.id_startup  where startup.status=1 and lf.rachat=0 and montant>0 and montant!='NC' and activite.secteur=" . $row_nom_secteur[0]['id'] . ""));

$moy_france = $tot_leve_france['somme'] / $tot_nb_france['nb'];

$purcentage_lf = ($tab_somme_total_lf[0]["somme"] * 100) / $moy_france;

$descriptionfr = $startup[0]['long_fr'];

$sql_list_personnes = mysqli_query($link, "select * from personnes where id_startup=" . $startup[0]['id'] . " and nom !='' and etat=1 order by personnes.id");
$nb_personnes = mysqli_num_rows($sql_list_personnes);

$startup_flow = mysqli_fetch_array(mysqli_query($link, "select * from startup_deal_flow where id_startup=" . $startup[0]["id"]));
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">

        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script>am4core.addLicense("CH303325752");</script>

        <script>
            const   themeColorBlue = am4core.color('#00bff0'),
                    themeColorFill = am4core.color('#E1F2FA'),
                    themeColorGrey = am4core.color('#dadada'),
                    themeColorLightGrey = am4core.color('#F5F5F5'),
                    themeColorDarkgrey = am4core.color("#33333A"),
                    themeColorLine = am4core.color("#87D6E3"),
                    themeColorLineRed = am4core.color("#FF7C7C"),
                    themeColorLineGreen = am4core.color("#21D59B"),
                    themeColorColumns = am4core.color("#E1F2FA"),
                    labelColor = am4core.color('#798a9a');
        </script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
        <style>
            #poool-widget {
                position: relative;
            }

            #poool-widget::before {
                position: absolute;
                background: transparent;
                background-image: -webkit-gradient(linear,left top,left bottom,from(rgba(255,255,255,0.05)),to(rgba(255,255,255,1)));
                background-image: linear-gradient(to bottom,rgba(255,255,255,0.05),rgba(255,255,255,1));
                background-position: center top;
                top: -110px;
                content: '';
                width: calc(100% + 10px);
                height: 110px;
            }
        </style>
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <?php
    if (isset($_POST["nom"])) {

        $nom = addslashes($_POST['nom']);
        $prenom = addslashes($_POST['prenom']);
        $tel = addslashes($_POST['tel']);
        $email = addslashes($_POST['email']);
        $societe = addslashes($_POST['societe']);
        $fonction = addslashes($_POST['fonction']);
        $story = addslashes($_POST['story']);

        mysqli_query($link, "INSERT INTO startup_deal_flow_contact (prenom, nom, email, tel, msg, date_add,societe,fonction,user,id_startup) VALUES"
                . " ( '" . $prenom . "', '" . $nom . "', '" . $email . "', '" . $tel . "', '" . $story . "',  '" . date('Y-m-d H:i:s') . "', '" . $societe . "', '" . $fonction . "', " . $us['id'] . ", " . $id_startup . ");");
        $headers = "From: \"" . $nom . "\"<" . $email . ">\n";
        $headers .= "Reply-To: " . $email . "\n";
        $headers .= "Content-Type: text/html; charset=\"utf-8\"";
        $message = 'Prénom :' . $prenom . "<br>
		Nom :" . $nom . "<br>
		Email :" . $email . "<br>
		Tél :" . $tel . "<br>
		Société :" . $societe . "<br>
		Fonction :" . $fonction . "<br>
		Message :" . $story . "<br>
		Date :" . date('Y-m-d H:i:s') . "<br>";

        require 'phpmailer/PHPMailerAutoload.php';

        $mail = new PHPMailer();

        $mail->IsSMTP();
        $mail->Host = 'ssl0.ovh.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'info@myfrenchstartup.com';                 // SMTP username
        $mail->Password = '04Betterway';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                               // TCP port to connect to

        $mail->setFrom('info@myfrenchstartup.com');
      //  $mail->addAddress($startup_flow['email']);     // Add a recipient
        $mail->addBCC('contact@myfrenchstartup.com');
        $mail->addBCC('samijilani@live.com');

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = "Myfrenchstartup - Demande de mise en relation";

        $mail->Body = utf8_decode($message);

        if (!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            
        }

        echo "<script>alert('Votre message a été envoyé avec succès.')</script>";
        echo "<script>window.location='" . $monUrl . "'</script>";
    }
    ?>
    <body class="preload page">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MR2VSZB"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">

                    <section class="module">



                        <div class="ctg ctg--1_3">
                            <aside class="bloc text-center">
                                <div class="module__title" > <a style="color:#00ccff" href="<?php echo URL ?>/fr/startup-france/<?php echo generate_id($startup[0]['id']) ?>/<?php echo urlWriting(strtolower($startup[0]['nom'])) ?>"><?php echo ($startup[0]['nom']); ?></a></div>
                                <div class="companyLogo">
                                    <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $startup[0]['logo']) ?>" alt="<?php echo $startup[0]['nom']; ?>" />
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Création</div>
                                    <div class="datasbloc__val"><?php
            echo $startup[0]['creation'];
            ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Effectif</div>
                                    <div class="datasbloc__val"><?php
                                        echo $startup[0]['rang_effectif'];
            ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Secteur</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;"><?php
                                        echo $id_secteur["nom_secteur"];
            ?>
                                    </div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;"><?php
                                        echo $id_sous_secteur["nom_sous_secteur"];
            ?>
                                    </div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;"><?php
                                        echo $id_activite["nom_activite"];
            ?>
                                    </div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;"><?php
                                        echo $id_sous_a["nom_sous_activite"];
            ?>
                                    </div>
                                </div>



                                <div>  <a class="btn btn-block btn-primary" href="<?php echo URL ?>/fr/startup-france/<?php echo generate_id($startup[0]['id']) ?>/<?php echo urlWriting(strtolower($startup[0]['nom'])) ?>" target="_blank">   Fiche complète   </a></div>
                            </aside>
                            <div>
                                <div class="bloc">
                                    <div class="ctg ctg--fullheight">
                                        <div class="">

                                            <div class="row">
                                                <div class="col datasbloc text-center">
                                                    <div class="datasbloc__key">Montant recherché</div>
                                                    <div class="datasbloc__val">
                                                        <?php
                                                        echo str_replace(",0", "", number_format($startup_flow['montant'] / 1000, 1, ",", "")) . " m€";
                                                        ?>
                                                    </div>        
                                                </div>
                                                <div class="col datasbloc text-center">
                                                    <div class="datasbloc__key">Date clôture</div>
                                                    <div class="datasbloc__val">
                                                        <?php
                                                        echo change_date_fr_chaine($startup_flow['dt']);
                                                        ?>
                                                    </div> 
                                                </div>

                                                <div class="col datasbloc text-center">
                                                    <div class="datasbloc__key">1<sup>ère</sup> levée de fonds</div>
                                                    <div class="datasbloc__val">
                                                        <?php
                                                        $verif_premier = mysqli_num_rows(mysqli_query($link, "select * from lf where id_startup=" . $startup[0]['id'] . "  and rachat=0"));
                                                        if ($verif_premier == 0) {
                                                            echo "Oui";
                                                        } else {
                                                            echo "Non";
                                                        }
                                                        ?>
                                                    </div> 
                                                </div>





                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="bloc" style="margin-top: 16px;">
                                    <div class="ctg ctg--fullheight">
                                        <div class="">
                                            <div class="datasbloc__key">Présentation</div>
                                            <div class="ctg ctg--fullheight">

                                                <div class="startupIntro">

                                                    <div class="row">

                                                        <?php
                                                        $bloc_tab = explode(" ", strip_tags($startup[0]['long_fr']));
                                                        $nbdd = count($bloc_tab);
                                                        $bloc1 = "";
                                                        $bloc2 = "";
                                                        for ($p = 0; $p < 90; $p++) {
                                                            $bloc1 .= $bloc_tab[$p] . " ";
                                                        }
                                                        for ($pp = 90; $pp < $nbdd; $pp++) {
                                                            $bloc2 .= $bloc_tab[$pp] . " ";
                                                        }
                                                        ?>
                                                        <div style="margin-bottom: 24px;font-size: 15px;font-weight: bold;color: #425466;"><?php echo utf8_encode(stripslashes($startup[0]['short_fr'])); ?></div>
                                                        <div class="intro-short" style="margin-top: 0px;"><?php echo utf8_encode($startup[0]['long_fr']); ?> </div>





                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                    </section>


                    <section class="module module--finances">
                        <div class="bloc">

                            <div class="row">
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Propriété industrielle et protection</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        $rest = substr($startup_flow['protection'], 0, -1);  // retourne "abcde"
                                        echo $rest;
                                        ?>
                                    </div>        
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Type d'innovation</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        $rest2 = substr($startup_flow['innovation'], 0, -1);  // retourne "abcde"
                                        echo $rest2;
                                        ?>
                                    </div> 
                                </div>

                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Client cible</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        $rest3 = substr($startup_flow['cible'], 0, -1);  // retourne "abcde"
                                        echo $rest3;
                                        ?>
                                    </div> 
                                </div>

                            </div>

                        </div>
                    </section>
                    <section class="module module--fondateurs">
                        <div class="module__title">Fondateurs & team</div>

                        <div class="ctg ctg--3_1">
                            <div class="bloc" id="bloc_autre_founder">
                                <div class="bloc__title">Fondateurs</div>
                                <div class="tablebloc tablebloc--light">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Fondateur</th>
                                                <th class="text-center">Âge</th>
                                                <th class="text-center">École</th>
                                                <th class="text-center">Diplôme</th>
                                                <th class="text-center" width="40%">Compétences</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $ligne_personne = mysqli_fetch_array(mysqli_query($link, "select * from personnes where id_startup=" . $startup[0]['id'] . " limit 1"));

                                            $cch_skils = "";
                                            $lign_exp = mysqli_fetch_array(mysqli_query($link, "select * from personnes_phantombuster where id_personnes=" . $ligne_personne['id']));
                                            $fonction_ligne = mysqli_fetch_array(mysqli_query($link, "select * from fonction where id=" . $ligne_personne["fonction"]));
                                            $ecole_ligne = mysqli_fetch_array(mysqli_query($link, "select * from ecole_personnes where idpersonne=" . $ligne_personne["id"] . " order by a desc limit 1"));
                                            $list_competances = mysqli_query($link, "select * from personnes_competence where id_personne=" . $ligne_personne["id"] . " limit 4");
                                            $ch_skils = explode(',', $lign_exp['allSkills']);
                                            $nb_comm = count($ch_skils);
                                            if ($nb_comm > 0) {
                                                for ($i = 0; $i < 5; $i++) {
                                                    if ($ch_skils[$i] != '') {
                                                        $cch_skils .= ($ch_skils[$i]) . ", ";
                                                    }
                                                }
                                            } else {
                                                $cch_skils = "-";
                                            }
                                            ?>
                                            <tr>
                                                <td class="text-center">
                                                    <div class="avatar">
                                                        <i class="ico-avatar"></i>
                                                    </div>
                                                    <strong><?php echo stripslashes(($ligne_personne['prenom'] . " " . $ligne_personne['nom'])); ?></strong>
                                                    <div class="fonction"><?php echo stripslashes($fonction_ligne['nom_fr']); ?></div>
                                                </td>
                                                <td class="text-center"><?php
                                            if ($ligne_personne['age'] != "")
                                                echo $ligne_personne['age'];
                                            else
                                                echo "-";
                                            ?></td>
                                                <td class="text-center"><?php
                                                    if ($lign_exp['schoolName_3'] != "") {
                                                        echo ($lign_exp['schoolName_3']);
                                                    } else if ($lign_exp['schoolName_2'] != "") {
                                                        echo ($lign_exp['schoolName_2']);
                                                    } else if ($lign_exp['schoolName_1'] != "") {
                                                        echo ($lign_exp['schoolName_1']);
                                                    } else {
                                                        echo "-";
                                                    }
                                            ?></td>
                                                <td class="text-center"><?php
                                                    if ($lign_exp['degree_3'] != "") {
                                                        echo ($lign_exp['degree_3']);
                                                    } else if ($lign_exp['degree_2'] != "") {
                                                        echo ($lign_exp['degree_2']);
                                                    } else if ($lign_exp['degree_1'] != "") {
                                                        echo ($lign_exp['degree_1']);
                                                    } else {
                                                        echo "-";
                                                    }
                                            ?></td>
                                                <td class="text-center"><?php echo $cch_skils; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="historyList">
                                    <?php
                                    if ($lign_exp['jobTitle_1'] != "") {
                                        ?>
                                        <div class="historyList__item">
                                            <div class="historyList__item__date"><?php echo ($lign_exp['dateRange_job_1']) ?></div>
                                            <div class="historyList__item__infos"><?php echo ($lign_exp['jobTitle_1']) ?> chez <?php echo ($lign_exp['companyName_1']) ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                    if ($lign_exp['jobTitle_2'] != "") {
                                        ?>
                                        <div class="historyList__item">
                                            <div class="historyList__item__date"><?php echo ($lign_exp['dateRange_job_2']) ?></div>
                                            <div class="historyList__item__infos"><?php echo ($lign_exp['jobTitle_2']) ?> chez <?php echo ($lign_exp['companyName_2']) ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                    if ($lign_exp['jobTitle_3'] != "") {
                                        ?>
                                        <div class="historyList__item">
                                            <div class="historyList__item__date"><?php echo ($lign_exp['dateRange_job_3']) ?></div>
                                            <div class="historyList__item__infos"><?php echo ($lign_exp['jobTitle_3']) ?> chez <?php echo ($lign_exp['companyName_3']) ?></div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Fondateurs & Team</div>
                                <div class="bloc-team">
                                    <?php
                                    $row_total_fondateur = getListFondateurByIdStartup($startup[0]["id"]);
                                    foreach ($row_total_fondateur as $personne) {
                                        $fonction = mysqli_fetch_array(mysqli_query($link, "select * from fonction where id=" . $personne["fonction"]));
                                        ?>
                                        <div style="cursor: pointer" class="bloc-team__people" onclick="getinfofounder(<?php echo $personne["id"]; ?>,<?php echo $startup[0]['id']; ?>)">
                                            <div class="bloc-team__people__avatar">
                                                <div class="avatar">
                                                    <i class="ico-avatar-white"></i>
                                                </div>
                                            </div>
                                            <div class="bloc-team__people__name">
                                                <?php echo stripslashes(utf8_encode($personne['prenom'] . " " . $personne['nom'])); ?>
                                                <span class="fonction"><?php echo stripslashes($fonction['nom_fr']); ?></span>
                                            </div>
                                            <div class="bloc-team__people__link">
                                                <?php if ($personne['linkedin'] != "") { ?>
                                                    <a target="_blank" href="https://<?php echo str_replace(array("https://"), array(""), $personne['linkedin']) ?>" title=""><i class="ico-linkedin"></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="module module--finances">
                        <div class="bloc">

                            <div class="row">
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Objectif de la levée</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        $rest5 = explode(",", $startup_flow['objectif']);
                                        $nb = count($rest5);
                                        for ($l = 0; $l < $nb; $l++) {
                                            if ($rest5[$l] != "") {
                                                ?>
                                                <div>- <?php echo $rest5[$l]; ?></div>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <?php
                                        if ($startup_flow['autre_objectif'] != '') {
                                            ?>
                                            <div>- <?php echo $startup_flow['autre_objectif']; ?></div>
                                            <?php
                                        }
                                        ?>
                                    </div>        
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Type investisseurs</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        $rest6 = explode(",", $startup_flow['type_invest']);
                                        $nb2 = count($rest6);
                                        for ($l = 0; $l < $nb2; $l++) {
                                            if ($rest6[$l] != "") {
                                                ?>
                                                <div>- <?php echo $rest6[$l]; ?></div>
                                                <?php
                                            }
                                        }
                                        ?>

                                    </div>        
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Business Modèle</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        $rest7 = explode(",", $startup_flow['business']);
                                        $nb = count($rest7);
                                        for ($l = 0; $l < $nb; $l++) {
                                            if ($rest7[$l] != "") {
                                                ?>
                                                <div>- <?php echo $rest7[$l]; ?></div>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <?php
                                        if ($startup_flow['autre_bm'] != '') {
                                            ?>
                                            <div>- <?php echo $startup_flow['autre_bm']; ?></div>
                                            <?php
                                        }
                                        ?>
                                    </div>        
                                </div>


                            </div>

                        </div>
                    </section>
                    <section class="module module--finances">
                        <div class="bloc">

                            <div class="row">
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Clients payants</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        $payant = $startup_flow['objectif'];
                                        if ($payant == "Non") {
                                            ?>
                                            <div>Non</div>
                                            <?php
                                        } else {
                                            ?>
                                            <div>Oui</div> 

                                            <?php
                                        }
                                        ?>

                                    </div>        
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Partenaires Grand Compte</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        $grand_compte = $startup_flow['grand_compte'];
                                        if ($grand_compte == "Non") {
                                            ?>
                                            <div>Non</div>
                                            <?php
                                        } else {
                                            if ($startup_flow['nom_grand_compte'] == "") {
                                                ?>
                                                <div>Oui</div> 
                                                <?php
                                            } else {
                                                ?>
                                                <div><?php echo $startup_flow['nom_grand_compte'] ?></div> 

                                                <?php
                                            }
                                            ?>
                                            <?php
                                        }
                                        ?>

                                    </div>        
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Concours</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        $concours = $startup_flow['concours'];
                                        if ($concours == "Non") {
                                            ?>
                                            <div>Non</div>
                                            <?php
                                        } else {
                                            if ($startup_flow['nom_concours'] == "") {
                                                ?>
                                                <div>Oui</div> 
                                                <?php
                                            } else {
                                                ?>
                                                <div><?php echo $startup_flow['nom_concours'] ?></div> 

                                                <?php
                                            }
                                            ?>
                                            <?php
                                        }
                                        ?>

                                    </div>        
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Subventions</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        $subventions = $startup_flow['subventions'];
                                        if ($subventions == "Non") {
                                            ?>
                                            <div>Non</div>
                                            <?php
                                        } else {
                                            if ($startup_flow['nom_subvention'] == "") {
                                                ?>
                                                <div>Oui</div> 
                                                <?php
                                            } else {
                                                ?>
                                                <div><?php echo $startup_flow['nom_subvention'] ?></div> 

                                                <?php
                                            }
                                            ?>
                                            <?php
                                        }
                                        ?>

                                    </div>        
                                </div>




                            </div>

                        </div>
                    </section>
                    <section class="module module--finances">
                        <div class="bloc">

                            <div class="row">
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Nom du marché</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        if ($startup_flow['nom_marche'] == "") {
                                            ?>
                                            <div>-</div>
                                            <?php
                                        } else {
                                            ?>
                                            <div><?php echo $startup_flow['nom_marche'] ?></div> 

                                            <?php
                                        }
                                        ?>

                                    </div>        
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Taille du marché</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        if ($startup_flow['taillem'] == "") {
                                            ?>
                                            <div>-</div>
                                            <?php
                                        } else {
                                            ?>
                                            <div><?php echo $startup_flow['taillem'] ?></div> 

                                            <?php
                                        }
                                        ?>

                                    </div>   
                                </div>
                                <div class="col datasbloc text-center">
                                    <div class="datasbloc__key">Source</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <?php
                                        if ($startup_flow['source'] == "") {
                                            ?>
                                            <div>-</div>
                                            <?php
                                        } else {
                                            ?>
                                            <div><?php echo $startup_flow['source'] ?></div> 

                                            <?php
                                        }
                                        ?>

                                    </div>      
                                </div>

                            </div>

                        </div>
                    </section>

                    <section class="module module--finances">
                        <div class="bloc">

                            <div class="row">
                                <div class="col datasbloc text-left">
                                    <div class="datasbloc__key">Contacter directement le fondateur</div>
                                    <div class="datasbloc__val" style="font-size: 16px !important;">
                                        <form method="post"action="">

                                            <div class="form-group">
                                                <div class="formgrid formgrid--3col">
                                                    <div class="formgrid__item">
                                                        <label>Nom</label>
                                                        <input type="text" value="<?php echo $us['nom']; ?>" name="nom"  required="" class="form-control" />
                                                    </div>
                                                    <div class="formgrid__item">
                                                        <label>Prénom</label>
                                                        <input type="text" value="<?php echo $us['prenom']; ?>" name="prenom" required="" class="form-control" />
                                                    </div>
                                                    <div class="formgrid__item">
                                                        <label>Email</label>
                                                        <input type="email" value="<?php echo $us['email']; ?>" name="email" required="" class="form-control" />
                                                    </div> 
                                                    <div class="formgrid__item">
                                                        <label>Téléphone</label>
                                                        <input type="text" value="<?php echo $us['tel']; ?>" name="tel" required="" class="form-control" />
                                                    </div> 
                                                    <div class="formgrid__item">
                                                        <label>Société</label>
                                                        <input type="text" value="<?php echo $us['societe']; ?>" name="societe" required="" class="form-control" />
                                                    </div> 
                                                    <div class="formgrid__item">
                                                        <label>Fonction</label>
                                                        <input type="text" value="<?php echo $us['fonction']; ?>" name="fonction" required="" class="form-control" />
                                                    </div> 

                                                </div>
                                                <div class="formgrid">
                                                    <div class="formgrid__item">
                                                        <label>Indiquez votre demande</label>
                                                        <textarea type="text" value="Startup" required="" name="story"class="form-control" placeholder="" rows="10"></textarea>
                                                    </div>
                                                </div> 

                                            </div>
                                            <div class="formpage__actions">
                                                <button type="submit" class="btn btn-primary">Valider</button>
                                            </div>
                                        </form>
                                    </div>        
                                </div>


                            </div>

                        </div>
                    </section>

                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>

        <script src="https://assets.poool.fr/audit.min.js"></script>
        <script src="https://assets.poool.fr/access.min.js"></script>
        <script>
                                        window.addEventListener('load', () => {
                                            Audit
                                                    .init('UB0IB-OS245-ZNO3H-EH4IZ')
                                                    .config({cookies_enabled: true}, {gtm_auto_tracking_enabled: true})
                                                    .sendEvent('page-view', {type: 'premium'});
                                            const access = Access.init('UB0IB-OS245-ZNO3H-EH4IZ');
                                            access.config({
                                                cookies_enabled: true,
                                            })
                                                    .on('release', function () {
                                                        access.destroy();
                                                        document.querySelector('#fiche_startup').remove();
                                                        document.querySelector('#bloc_radar').remove();
                                                        document.querySelector('#bloc_maturite').remove();
                                                    })



                                            Access
                                                    .init('UB0IB-OS245-ZNO3H-EH4IZ')
                                                    .config({
                                                        context: 'radar',
                                                        cookies_enabled: true,
                                                    })
                                                    .createPaywall({
                                                        target: '#bloc_radar',
                                                        content: '.content_radar',
                                                        mode: 'hide',
                                                        percent: 100,
                                                        pageType: 'premium',
                                                        cookies_enabled: true,
                                                    });
                                            Access
                                                    .init('UB0IB-OS245-ZNO3H-EH4IZ')
                                                    .config({
                                                        context: 'maturite',
                                                        cookies_enabled: true,
                                                    })
                                                    .createPaywall({
                                                        target: '#bloc_maturite',
                                                        content: '.content_maturite',
                                                        mode: 'hide',
                                                        percent: 100,
                                                        pageType: 'premium',
                                                        cookies_enabled: true,
                                                    });
                                            Access
                                                    .init('UB0IB-OS245-ZNO3H-EH4IZ')
                                                    .config({
                                                        context: 'finance',
                                                        cookies_enabled: true,
                                                    })
                                                    .createPaywall({
                                                        target: '#fiche_startup',
                                                        content: '.content_finance',
                                                        mode: 'hide',
                                                        percent: 80,
                                                        pageType: 'premium',
                                                        cookies_enabled: true,
                                                    });





                                        }
                                        )

        </script>



        <script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>
            function getinfofounder(id, startup) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL ?>/bloc_founder.php?id=" + id + "&startup=" + startup,
                    success: function (o) {
                        document.getElementById("bloc_autre_founder").innerHTML = o;
                    }

                }
                );
            }
            function revendiquer() {
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL ?>/revendiquer_sup.php",
                    success: function (o) {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo URL ?>/revendique_email.php",
                            success: function (o) {
                                document.getElementById("grid").innerHTML = o;
                            }

                        }
                        );
                    }

                }
                );
            }


            $(document).ready(function () {
                $("#voir_plus").click(function () {
                    $("#long_fr_bloc2").show();
                    $("#voir_plus").hide();
                });
                $("#voir_moins").click(function () {
                    $("#long_fr_bloc2").hide();
                    $("#voir_plus").show();
                });
            });


        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function memoriser(id) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL ?>/memoriser.php",
                    data: 'id=' + id,

                    success: function (data) {
                        alert("Votre startup est mémorisée dans votre liste favoris");
                    }
                });
            }
        </script>
        <div class="modal fade" id="exampleModal_filtre" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document" style="width: 800px !important;max-width: 604px !important;min-height: 400px;">
                <div class="modal-content" style="font-size: 14px;border-radius: 5px;width: 100%;min-height: 400px;">
                    <div id="grid">
                        <div class="zoom-anim-dialog" >




                            <!-- Login -->
                            <div class="tab-content" id="tab1" style="border-top-width: 0px;padding: 8px;">
                                <div style="text-align:center;">
                                    <h2 class="sub_title" style="line-height: 24px;font-size: 2.6rem;text-align: center;margin-top: 8px;margin-bottom: 16px;">Revendiquer<br>gratuitement cette startup</h2>
                                    <h5 style="text-align:center;font-size:16px;color: #425466;font-family: 'Euclid';line-height: 24px;margin-top: 24px;margin-bottom: 24px;">Prenez la main sur votre fiche startup et valorisez votre image auprès de toutes l’écosystème</h5>
                                </div>
                                <form method="post" action="<?php echo url ?>revendiquer-startup-validation" class="register">

                                    <div class="colgridform">
                                        <div class="gridformtext" style="margin-top: 8px;">
                                            <div>
                                                <label>Email</label>
                                                <input type="email" name="email" id="email" required="" value="<?php echo $_SESSION['data_login']; ?>" >
                                            </div>
                                            <div>
                                                <label>Startup</label>
                                                <input type="text" name="societe" required="" id="societe" value="<?php echo $_SESSION['startup_nom_revendiquer']; ?>" >
                                            </div>
                                        </div>
                                        <h5 style="text-align:left;font-size:16px;color: #425466;font-family: 'EuclidBold';line-height: 24px;margin-top: 16px;margin-bottom: 16px;">Demande de liaison</h5>
                                        <div style="margin-top: 16px;">
                                            <p style="font-family: 'EuclidLight';line-height: 16px!important"><strong>Utilisez une adresse e-mail de votre établissement</strong> : lorsque vous indiquez une adresse e-mail de votre établissement au moment de votre demande de liaison, vos données sont vérifiées rapidement.
                                                <br><br>Si vous ne disposez pas d’une adresse e-mail de l’établissement ou si votre adresse ne peut être vérifiée, des justificatifs supplémentaires pourront vous être demandés.</p>

                                        </div>
                                        <h5 style="text-align:left;font-size:16px;color: #425466;font-family: 'EuclidBold';line-height: 24px;margin-top: 16px;margin-bottom: 16px;">Les prochaines étapes</h5>
                                        <p style="font-family: 'EuclidLight';line-height: 24px!important">
                                            1. Sélectionnez votre startup et saisissez votre adresse e-mail professionnelle<br> 
                                            2. Vous recevez un e-mail de confirmation (sous 24/48h) avec un lien vers l’espace de mise à jour startup<br> 
                                            3. Retrouvez votre startup dans votre Dashboard, éditez votre fiche et accédez à vos statistiques.<br> 
                                        </p>                                     
                                        <div style="text-align:center;margin-top: 16px;margin-left: 224px;margin-right: 224px;height: 38px;margin-bottom: 32px;">
                                            <input type="button" onclick="revendiquer()" style="background: #00CCFF;border-radius: 4px;padding: 8px;width: 89px;font-family: 'Roboto';height: 38px;font-size: 12px;line-height: 12px;margin-right: 0px;margin-bottom: 0px;" name="register" value="Valider" />
                                        </div>
                                    </div>
                                </form>
                            </div>



                        </div>
                    </div>


                </div>
            </div>
        </div>
        <script type="text/javascript">window.gdprAppliesGlobally = true;
            (function () {
                function a(e) {
                    if (!window.frames[e]) {
                        if (document.body && document.body.firstChild) {
                            var t = document.body;
                            var n = document.createElement("iframe");
                            n.style.display = "none";
                            n.name = e;
                            n.title = e;
                            t.insertBefore(n, t.firstChild)
                        } else {
                            setTimeout(function () {
                                a(e)
                            }, 5)
                        }
                    }
                }
                function e(n, r, o, c, s) {
                    function e(e, t, n, a) {
                        if (typeof n !== "function") {
                            return
                        }
                        if (!window[r]) {
                            window[r] = []
                        }
                        var i = false;
                        if (s) {
                            i = s(e, t, n)
                        }
                        if (!i) {
                            window[r].push({command: e, parameter: t, callback: n, version: a})
                        }
                    }
                    e.stub = true;
                    function t(a) {
                        if (!window[n] || window[n].stub !== true) {
                            return
                        }
                        if (!a.data) {
                            return
                        }
                        var i = typeof a.data === "string";
                        var e;
                        try {
                            e = i ? JSON.parse(a.data) : a.data
                        } catch (t) {
                            return
                        }
                        if (e[o]) {
                            var r = e[o];
                            window[n](r.command, r.parameter, function (e, t) {
                                var n = {};
                                n[c] = {returnValue: e, success: t, callId: r.callId};
                                a.source.postMessage(i ? JSON.stringify(n) : n, "*")
                            }, r.version)
                        }
                    }
                    if (typeof window[n] !== "function") {
                        window[n] = e;
                        if (window.addEventListener) {
                            window.addEventListener("message", t, false)
                        } else {
                            window.attachEvent("onmessage", t)
                        }
                    }
                }
                e("__tcfapi", "__tcfapiBuffer", "__tcfapiCall", "__tcfapiReturn");
                a("__tcfapiLocator");
                (function (e) {
                    var t = document.createElement("script");
                    t.id = "spcloader";
                    t.type = "text/javascript";
                    t.async = true;
                    t.src = "https://sdk.privacy-center.org/" + e + "/loader.js?target=" + document.location.hostname;
                    t.charset = "utf-8";
                    var n = document.getElementsByTagName("script")[0];
                    n.parentNode.insertBefore(t, n)
                })("f0c52c20-b8cf-485b-a4b4-c222da28676d")
            })();</script>
    </body>
</html>