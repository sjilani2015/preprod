﻿<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

$url_base = "https://www.myfrenchstartup.com/deal-flow/";
mysqli_query($link, "insert into study_tracking(action,ip,date_add)values('Page Add Deal Flow','" . $_SERVER['REMOTE_ADDR'] . "','" . date('Y-m-d H:i:s') . "')");
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Deal Flow</title>

        <meta name="title" content="Deal Flow"/>
        <meta name="Description" content="Deal Flow | Ajouter votre demande de levées de fonds " />
        <!-- Global stylesheets -->
        <link rel="shortcut icon" href="https://www.myfrenchstartup.com/myfs.ico">

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo $url_base; ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $url_base; ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $url_base; ?>assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $url_base; ?>assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $url_base; ?>assets/css/colors.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $url_base; ?>assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo $url_base; ?>js/datepicker.css" />
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/loaders/blockui.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/ui/nicescroll.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/ui/drilldown.js"></script>


        <!-- /core JS files -->

        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/ui/moment/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/pickers/daterangepicker.js"></script>

        <!-- /core JS files -->

        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/core/app.js"></script>

        <script type="text/javascript" src="<?php echo $url_base; ?>starters/assets/js/navbar_fixed_secondary.js"></script>

        <style>
            .sep_bloc{font-size:20px; text-transform: uppercase}
            ul#follow_us {
                list-style: none;
                padding-top: 30px;
                padding-left: 0px;
                padding-right: 0px;
                padding-bottom: 0px;
                margin: 10px 0 15px 0;
            }
            .style_check  input{height: 22px; width: 22px}
        </style>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');

        </script>
    </head>

    <body style="background-color:#F6F9FC !important">

        <!-- Main navbar -->
        <div class="navbar navbar-inverse" style="background: #fff; height: 62px">
            <div class="navbar-header">
                <a class="navbar-brand" style="padding-top: 5px;" href="https://www.myfrenchstartup.com">
                    <img src="https://www.myfrenchstartup.com/static/images/logos/logo.svg?1" style="width: 155px" alt="">

                </a>

                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">

            </div>
        </div>

        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title"></div>
            </div>
        </div>
        <!-- /page header -->
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main content -->
                <div class="content-wrapper">
                    <div class="container">
                        <!-- Line and bar combination -->
                        <form method="post" action="">
                            <div class="col-lg-8 col-lg-offset-2">

                                <!-- Sales stats -->
                                <div class="panel panel-flat">
                                    <div class="panel-heading" style="margin-bottom: 10px;">
                                        <div>
                                            <h5 class="panel-title pull-left" style="color: #00CCFF; margin-bottom: 10px;">Informations générales</h5>

                                        </div>
                                        <div class="heading-elements">
                                        </div>

                                        <div class="row" style="margin-top: 40px;">
                                            <div class="col-lg-6"><br>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-rocket" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="text"  class="form-control" name="societe" placeholder="Nom commercial de votre Startup" required="">
                                                </div>
                                            </div>
                                            <div class="col-lg-6"><br>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-legal" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="text"  class="form-control" name="juridique" placeholder="Nom juridique">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6"><br>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-legal" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="text"  class="form-control" name="siret" placeholder="SIRET / SIREN">
                                                </div>
                                            </div>
                                            <div class="col-lg-6"><br>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-globe" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="text"  class="form-control" name="website" placeholder="Site web" required="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-users" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="text"  class="form-control" name="effectif" placeholder="Nombre d'employés">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-calendar" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="text" class="datepicker1 form-control" id="dp3"  name="date_creation" placeholder="Date de création" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-6"><label class="text-semibold" style="margin-top: 10px;">Secteur d'activité</label><br>
                                                <div class="input-group">
                                                    <select class="form-control" name="secteur" id="secteur_select" onchange="reload_sous_secteur_bloc()">
                                                        <option selected=""></option>

                                                        <?php
                                                        $req_select_secteur = "select * from secteur order by nom_secteur";
                                                        $res_select_secteur = mysqli_query($link, $req_select_secteur);
                                                        while ($data_secteur = mysqli_fetch_array($res_select_secteur)) {
                                                            ?>
                                                            <option <?php
                                                            if ($data_secteur['id'] == $data['secteur']) {
                                                                echo ' selected="selected" ';
                                                            }
                                                            ?> value="<?php echo $data_secteur['id'] ?>"> <?php echo utf8_encode($data_secteur['nom_secteur']) ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6" id="bloc_sous_secteur_affiche" style="display: none"><label class="text-semibold" style="margin-top: 10px;">Sous Secteur</label><br>
                                                <div id="sous_secteur_bloc" class="input-group">
                                                    <select  class="form-control" name="sous_secteur"  >
                                                        <option value=""></option>
                                                        <?php
                                                        if ($data['secteur'] != '') {
                                                            $req_select_sous_secteur = "select * from sous_secteur where id_secteur=" . $data['secteur'] . " order by nom_sous_secteur";
                                                        }

                                                        $res_select_sous_secteur = mysqli_query($link, $req_select_sous_secteur);
                                                        while ($data_sous_secteur = mysqli_fetch_array($res_select_sous_secteur)) {
                                                            ?>
                                                            <option <?php
                                                            if ($data_sous_secteur['id'] == $data['sous_secteur']) {
                                                                echo ' selected="selected" ';
                                                            }
                                                            ?> value="<?php echo $data_sous_secteur['id'] ?>"> <?php echo utf8_encode($data_sous_secteur['nom_sous_secteur']) ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>





                            <div class="col-lg-8 col-lg-offset-2">
                                <div class="panel panel-flat">
                                    <div class="panel-heading" id="chiffres_cles">

                                        <h5 class="panel-title" style="color: #00CCFF">Votre levée de fonds</h5>
                                        <div class="heading-elements">
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-6"><label class="text-semibold" style="margin-top: 10px;">Montant recherché en k€</label><br>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-eur" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="text"  class="form-control" name="montant_rech" placeholder="Montant recherché en K&euro;" required="">
                                                </div>
                                            </div>
                                            <div class="col-lg-6"><label class="text-semibold" style="margin-top: 10px;">Date de clôture de votre levée de fonds</label><br>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-calendar" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="text" class="datepicker form-control" id="dp2"  name="date_rech" placeholder="Date de clôture" required value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-md-12">
                                                <div><label class="text-semibold" style="margin-top: 10px;">Objectif de ce tour de table</label></div>
                                                <div class="table-responsive">
                                                    <table class="table text-nowrap table-bordered" style="font-size: 13px;">
                                                        <tbody>
                                                            <tr style="text-align: center">

                                                                <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Marketing &<br>communication</td>
                                                                <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Recrutement<br>Développeurs</td>
                                                                <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Recrutement<br>Commerciaux</td>
                                                                <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">R&D</td>
                                                                <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Acquisition<br>Matériels</td>
                                                                <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Internationalisation</td>
                                                                <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Autres</td>
                                                            </tr>
                                                            <tr class="style_check" style="text-align: center">

                                                                <td><input type="checkbox" name="objectif[]" value="Marketing & communication" ></td>
                                                                <td><input type="checkbox" name="objectif[]" value="Recrutement Développeurs" ></td>
                                                                <td><input type="checkbox" value="Recrutement Commerciaux" name="objectif[]"/></td>
                                                                <td><input type="checkbox" value="R&D" name="objectif[]" /></td>
                                                                <td><input type="checkbox" value="Acquisition matériels" name="objectif[]" /></td>
                                                                <td><input type="checkbox" value="Internationalisation" name="objectif[]" /></td>
                                                                <td><input type="checkbox" value="Autres" name="objectif[]" id="objectif_autre" onclick="ajout_autre()" /></td>
                                                            </tr>

                                                        </tbody>

                                                    </table>
                                                    <input type="text" name="autre_objectif" id="autre_objectif" placeholder="Merci de préciser" style="display: none; margin-top: 15px;" class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-md-12">
                                                <div><label class="text-semibold" style="margin-top: 10px;">Types d'Investisseurs recherchés</label> </div>
                                                <div class="table-responsive">
                                                    <table class="table text-nowrap table-bordered" style="font-size: 13px;">
                                                        <tbody>
                                                            <tr style="text-align: center">
                                                                <td class="col-md-3">Business Angels</td>
                                                                <td class="col-md-3">VC / Corporate VC</td>
                                                                <td class="col-md-3">Crowdfunding</td>
                                                                <td class="col-md-3">Fonds Publics</td>
                                                            </tr>
                                                            <tr class="style_check" style="text-align: center">
                                                                <td><input type="checkbox" value="Business Angels" name="rech[]" /></td>
                                                                <td><input type="checkbox" value="VC / Corporate VC" name="rech[]"/></td>
                                                                <td><input type="checkbox" value="Crowdfunding" name="rech[]" /></td>
                                                                <td><input type="checkbox" value="Fonds publics" name="rech[]" /></td>
                                                            </tr>
                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- /line and bar combination -->
                            <div class="col-lg-8 col-lg-offset-2">

                                <!-- Sales stats -->
                                <div class="panel panel-flat">
                                    <div class="panel-heading" style="margin-bottom: 45px;">
                                        <div>
                                            <h5 class="panel-title pull-left" style="color: #00CCFF; margin-bottom: 20px;">Votre Marché</h5>
                                            <i class="fa fa-plus-circle pull-right fa-2x" style="cursor:pointer" id="action_marche"></i>
                                        </div>
                                        <div class="heading-elements">

                                        </div>
                                        <div id="bloc_marche" style="display: none">
                                            <div class="row" style="margin-top: 55px;">
                                                <div class="col-lg-4"><label class="text-semibold">Business Modèle</label></div>
                                                <div class="col-lg-12">
                                                    <div class="table-responsive">
                                                        <table class="table text-nowrap table-bordered" style="font-size: 13px;">
                                                            <tbody>
                                                                <tr style="text-align: center">
                                                                    <td style="padding-left: 5px; padding-right: 5px;padding-top: 2px;padding-bottom: 2px;">Gratuit</td>
                                                                    <td style="padding-left: 5px; padding-right: 5px;padding-top: 2px;padding-bottom: 2px;">Premium/<br>Abonnement</td>
                                                                    <td style="padding-left: 5px; padding-right: 5px;padding-top: 2px;padding-bottom: 2px;">Publicité</td>
                                                                    <td style="padding-left: 5px; padding-right: 5px;padding-top: 2px;padding-bottom: 2px;">Vente</td>
                                                                    <td style="padding-left: 5px; padding-right: 5px;padding-top: 2px;padding-bottom: 2px;">Commissions</td>
                                                                    <td style="padding-left: 5px; padding-right: 5px;padding-top: 2px;padding-bottom: 2px;">Mise en relation</td>
                                                                    <td style="padding-left: 5px; padding-right: 5px;padding-top: 2px;padding-bottom: 2px;">Revenus de<br>Licences</td>
                                                                    <td style="padding-left: 5px; padding-right: 5px;padding-top: 2px;padding-bottom: 2px;">Autres</td>
                                                                </tr>
                                                                <tr class="style_check" style="text-align: center">
                                                                    <td><input type="checkbox" name="business[]" value="Gratuit" style="padding-left: 5px; padding-right: 5px;padding-top: 2px;padding-bottom: 2px;padding-top: 5px;padding-bottom: 5px;"  /></td>
                                                                    <td><input type="checkbox" name="business[]" value="Premium/Abonnement" style="padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;" /></td>
                                                                    <td><input type="checkbox" name="business[]" value="Publicité" style="padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;" /></td>
                                                                    <td><input type="checkbox" name="business[]" value="Ventes" style="padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;"/></td>
                                                                    <td><input type="checkbox" name="business[]" value="Commissions" style="padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;"  /></td>
                                                                    <td><input type="checkbox" name="business[]" value="Mise en relation" style="padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;" /></td>
                                                                    <td><input type="checkbox" name="business[]" value="Revenus de licences" style="padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;" /></td>
                                                                    <td><input type="checkbox" name="business[]" value="Autres" id="bm_autre" onclick="ajout_bm()" style="padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;" /></td>
                                                                </tr>
                                                            </tbody>

                                                        </table>
                                                        <input type="text" name="autre_bm" id="autre_bm" placeholder="Merci de préciser" style="display: none; margin-top: 15px;" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 25px;">
                                                <div class="col-lg-4"><label class="text-semibold">Type d'innovation</label></div>
                                                <div class="col-lg-12">
                                                    <div class="table-responsive">
                                                        <table class="table text-nowrap table-bordered" style="font-size: 13px;">
                                                            <tbody>
                                                                <tr style="text-align: center">
                                                                    <td class="col-md-3">Usage</td>
                                                                    <td class="col-md-3">Technologique</td>
                                                                    <td class="col-md-3">Modèle &Eacute;conomique</td>
                                                                    <td class="col-md-3">Positionnement</td>
                                                                </tr>
                                                                <tr class="style_check" style="text-align: center">
                                                                    <td><input type="checkbox" name="innovation[]" value="Usage" style="margin-top: -1px; margin-right: 2px;" /></td>
                                                                    <td><input type="checkbox" name="innovation[]" value="Technologique" style="margin-top: -1px; margin-right: 2px;" /></td>
                                                                    <td><input type="checkbox" name="innovation[]" value="Modèle économique" style="margin-top: -1px; margin-right: 2px;" /></td>
                                                                    <td><input type="checkbox" name="innovation[]" value="Positionnement" style="margin-top: -1px; margin-right: 2px;" /></td>
                                                                </tr>
                                                            </tbody>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 25px;">

                                                <div class="col-lg-12"><label class="text-semibold">Protection industrielle</label></div>
                                                <div class="col-lg-12">
                                                    <div class="table-responsive">
                                                        <table class="table text-nowrap table-bordered" style="font-size: 13px;">
                                                            <tbody>
                                                                <tr>

                                                                    <td class="col-md-3">Brevet</td>
                                                                    <td class="col-md-3">Dessins & Modèles</td>
                                                                    <td class="col-md-3">Soleau</td>

                                                                </tr>
                                                                <tr class="style_check">

                                                                    <td><input type="checkbox" name="protection[]" value="Brevet" style="margin-top: -2px; margin-right: 2px;" /></td>
                                                                    <td><input type="checkbox" name="protection[]" value="Dessins & Modèles"  style="margin-top: -2px; margin-right: 2px;" /></td>
                                                                    <td><input type="checkbox" name="protection[]" value="Soleau" style="margin-top: -2px; margin-right: 2px;" /></td>
                                                                </tr>

                                                            </tbody>

                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row" style="margin-top: 25px;">
                                                <div class="col-lg-12"><label class="text-semibold">Votre cible</label></div>
                                                <div class="col-lg-12">
                                                    <div class="table-responsive">
                                                        <table class="table text-nowrap table-bordered" style="font-size: 13px;">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="col-md-3">BtoB</td>
                                                                    <td class="col-md-3">BtoC</td>
                                                                    <td class="col-md-3">CtoC</td>
                                                                </tr>
                                                                <tr class="style_check">
                                                                    <td><input type="checkbox" name="cible[]" value="BtoB" style="margin-top: -2px; margin-right: 2px;" /></td>
                                                                    <td><input type="checkbox" name="cible[]" value="BtoC"  style="margin-top: -2px; margin-right: 2px;" /></td>
                                                                    <td><input type="checkbox" name="cible[]" value="CtoC" style="margin-top: -2px; margin-right: 2px;" /></td>
                                                                </tr>

                                                            </tbody>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 25px;">
                                                <div class="col-lg-12"><label class="text-semibold" style="margin-top: 10px;">Nom de votre marché</label><br>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-line-chart" style="color: rgb(64, 131, 196);"></i></div>
                                                        <input type="text"  class="form-control" name="nom_marche" placeholder="Fintech, Edtech, Ads, Biotech,...">
                                                    </div>
                                                </div>
                                                <div class="col-lg-9"><label class="text-semibold" style="margin-top: 10px;">Taille du marché</label><br>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-cubes" style="color: rgb(64, 131, 196);"></i></div>
                                                        <input type="text"  class="form-control" name="taillem" placeholder="Taille du marché">
                                                    </div>
                                                </div>
                                                <div class="col-lg-3" style="margin-top: 8px;">

                                                    <label class="text-semibold" style="margin-top: 10px;"></label><br>
                                                    <div class="checkbox checkbox-switch">
                                                        <label class="style_check">
                                                            <input type="checkbox" name="volume" data-on-color="success" data-off-color="primary" data-on-text="Montant" data-off-text="Volume" class="switch1" checked="checked">

                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 10px;">
                                                <div class="col-lg-12"><label class="text-semibold" style="margin-top: 10px;">Source</label><br>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-info" style="color: rgb(64, 131, 196);"></i></div>
                                                        <input type="text"  class="form-control" name="sourcemarche" placeholder="Merci d'indiquer la source de ces données">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /sales stats -->

                            </div>
                            <div class="col-lg-8 col-lg-offset-2">

                                <!-- Sales stats -->
                                <div class="panel panel-flat">
                                    <div class="panel-heading" style="margin-bottom: 45px;">

                                        <div>
                                            <h5 class="panel-title pull-left" style="color: #00CCFF; margin-bottom: 20px;">Maturité</h5>
                                            <i class="fa fa-plus-circle pull-right fa-2x" style="cursor:pointer" id="action_info"></i>
                                        </div>
                                        <div class="heading-elements">
                                        </div>

                                        <div class="row" style="margin-top: 55px; display: none" id="bloc_info">

                                            <div class="col-lg-7" style="margin-top: 10px;"><label class="text-semibold" style="margin-top: 30px; font-weight: normal; font-size: 16px;">Avez-vous un ou plusieurs clients payants?</label></div>
                                            <div class="col-lg-5" style="margin-top: 25px;">
                                                <div class="checkbox checkbox-switch">
                                                    <label>
                                                        <input type="checkbox" name="payant" data-on-color="success" data-off-color="danger" data-on-text="Oui" data-off-text="Non" class="switch1">

                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-7" style="margin-top: 10px;"><label class="text-semibold" style="margin-top: 30px; font-weight: normal; font-size: 16px;">Avez-vous un ou plusieurs partenaires Grand Compte?</label></div>
                                            <div class="col-lg-5" style="margin-top: 25px;" >
                                                <div class="checkbox checkbox-switch">
                                                    <label>
                                                        <input type="checkbox" name="partenaire" id="partenaire" data-on-color="success" data-off-color="danger" data-on-text="Oui" data-off-text="Non" class="switch_partenaire">
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12" style="margin-top: 10px;">
                                                <input type="text" class="form-control" name="nom_grand_compte" placeholder="Qui sont vos partenaires?" id="nom_grand_compte" style="display: none">
                                            </div>

                                            <div class="col-lg-7" style="margin-top: 10px;"><label class="text-semibold" style="margin-top: 30px; font-weight: normal; font-size: 16px;">Avez-vous remporté des concours et/ou des distinctions?</label></div>
                                            <div class="col-lg-5" style="margin-top: 25px;">
                                                <div class="checkbox checkbox-switch">
                                                    <label>
                                                        <input type="checkbox" name="concours" id="concours" data-on-color="success" data-off-color="danger" data-on-text="Oui" data-off-text="Non" class="switch_concours">

                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12" style="margin-top: 10px;">
                                                <input type="text" class="form-control" name="nom_concours" placeholder="Nom des concours" id="nom_concours" style="display: none">
                                            </div>
                                            <div class="col-lg-7" style="margin-top: 10px;"><label class="text-semibold" style="margin-top: 30px; font-weight: normal; font-size: 16px;">Avez vous bénéficié de subventions?</label></div>
                                            <div class="col-lg-5" style="margin-top: 25px;">
                                                <div class="checkbox checkbox-switch">
                                                    <label>
                                                        <input type="checkbox" name="subventions" id="subventions" data-on-color="success" data-off-color="danger" data-on-text="Oui" data-off-text="Non" class="switch_subvention">

                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12" style="margin-top: 10px;">
                                                <input type="text" class="form-control" name="nom_subvention" placeholder="Nom de l'organisme" id="nom_subvention" style="display: none">
                                            </div>
                                        </div>





                                    </div>
                                </div>
                                <!-- /sales stats -->

                            </div>
                            <div class="col-lg-8 col-lg-offset-2">

                                <!-- Sales stats -->
                                <div class="panel panel-flat">
                                    <div class="panel-heading" style="margin-bottom: 45px;">
                                        <div>
                                            <h5 class="panel-title pull-left" style="color: #00CCFF; margin-bottom: 20px;">Personne à contacter</h5>
                                            <i class="fa fa-plus-circle pull-right fa-2x" style="cursor:pointer" id="action_personnes"></i>
                                        </div>
                                        <div class="heading-elements">
                                        </div>

                                        <div class="row" style="margin-top: 55px" id="bloc_personnes">
                                            <div class="col-lg-6"><label class="text-semibold" style="margin-top: 10px;">Prénom</label><br>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-user" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="text"  class="form-control" name="prenom" placeholder="Prénom">
                                                </div>
                                            </div>
                                            <div class="col-lg-6"><label class="text-semibold" style="margin-top: 10px;">Nom</label><br>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-user" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="text"  class="form-control" name="nom" placeholder="Nom">
                                                </div>
                                            </div>

                                            <div class="col-lg-6"><label class="text-semibold" style="margin-top: 10px;">Fonction</label><br>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-suitcase" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="text"  class="form-control" name="fonction" placeholder="Fonction">
                                                </div>
                                            </div>
                                            <div class="col-lg-6"><label class="text-semibold" style="margin-top: 10px;">Téléphone</label><br>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-phone" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="text"  class="form-control" name="tel" placeholder="Téléphone">
                                                </div>
                                            </div>

                                            <div class="col-lg-6"><label class="text-semibold" style="margin-top: 10px;">Email</label><br>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-at" style="color: rgb(64, 131, 196);"></i></div>
                                                    <input type="email"  class="form-control" name="email_contact" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">  <hr></div>
                                            <div class="col-lg-12"><label class="text-semibold" style="margin-top: 10px;">Autres co-fondateurs </label></div>
                                            <?php $co = 1; ?>
                                            <div id="bloc_founder">
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-user" style="color: rgb(64, 131, 196);"></i></div>
                                                        <input type="text"  class="form-control" name="prenom_co[]" placeholder="Prénom">
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-user" style="color: rgb(64, 131, 196);"></i></div>
                                                        <input type="text"  class="form-control" name="nom_co[]" placeholder="Nom">
                                                    </div>
                                                </div>
                                                <div class="col-lg-3" style="margin-bottom: 20px;">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-suitcase" style="color: rgb(64, 131, 196);"></i></div>
                                                        <input type="text"  class="form-control" name="fonction_co[]" placeholder="Fonction">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="addnbco">
                                                <div class="col-lg-3">
                                                    <a class="" onclick="add_co_founder(<?php echo $co + 1; ?>);">
                                                        <i class="fa fa-plus-circle fa-2x"></i>
                                                    </a>
                                                </div>
                                            </div>

                                        </div>





                                    </div>
                                </div>
                                <!-- /sales stats -->

                            </div>

                            <div class="col-lg-8 col-lg-offset-2">

                                <!-- Sales stats -->
                                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <div class="heading-elements">
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6 col-lg-offset-3"><label class="text-semibold" style="margin-top: 10px;"></label><br>
                                                <div class="input-group col-md-12">
                                                    <button type="submit" class="btn btn-primary col-md-12" name="ajout">Valider</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <!-- /main content -->
            </div>
            <!-- /page content -->
            <!-- Footer -->

        </div>
        <!-- /page container -->
        <script src="<?php echo $url_base; ?>js/bootstrap-datepicker.js"></script>
        <script>

                                                        $(function () {
                                                            window.prettyPrint && prettyPrint();

                                                            refrech_datepicker();
                                                            refrech_datepicker1();

                                                        });


                                                        function refrech_datepicker() {
                                                            $(".datepicker").each(function () {
                                                                $(this).datepicker({
                                                                    format: 'yyyy/mm/dd'
                                                                });


                                                            });

                                                        }
                                                        function refrech_datepicker1() {
                                                            $(".datepicker1").each(function () {
                                                                $(this).datepicker({
                                                                    format: 'yyyy/mm/dd'
                                                                });


                                                            });

                                                        }
        </script>


        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/plugins/forms/styling/switch.min.js"></script>
        <script type="text/javascript" src="<?php echo $url_base; ?>assets/js/core/app.js"></script>
        <script>

                                                        $(function () {

                                                            // Bootstrap switch
                                                            // ------------------------------

                                                            $(".switch1").bootstrapSwitch();
                                                            $(".switch_partenaire").bootstrapSwitch();
                                                            $(".switch_concours").bootstrapSwitch();
                                                            $(".switch_subvention").bootstrapSwitch();

                                                        });


                                                        function add_co_founder(id) {
                                                            $("#bloc_founder").append('<div style="clear:both" class="capital" id="capital_' + id + '"><hr><div class="col-lg-3"><div class="input-group"><div class="input-group-addon"><i class="fa fa-user" style="color: rgb(64, 131, 196);"></i></div><input type="text"  class="form-control" name="prenom_co[]" placeholder="Prénom"></div></div><div class="col-lg-3"><div class="input-group"><div class="input-group-addon"><i class="fa fa-user" style="color: rgb(64, 131, 196);"></i></div><input type="text"  class="form-control" name="nom_co[]" placeholder="Nom"></div></div><div class="col-lg-3"><div class="input-group"><div class="input-group-addon"><i class="fa fa-suitcase" style="color: rgb(64, 131, 196);"></i></div><input type="text"  class="form-control" name="fonction_co[]" placeholder="Fonction"></div></div><div class="col-md-3"><a onClick="del_co_founfer(' + id + ');" class=""><i style="color:#EC407A" class="fa fa-minus-circle fa-2x"></i> </a></div></div>');
                                                            $("#addnbco").html('<div style="margin-top: 10px; margin-bottom: 10px; clear: both" class="col-md-12"><a class="" onclick="add_co_founder(' + (id + 1) + ');"><i class="fa fa-plus-circle fa-2x"></i></a><div class="clr"></div></div>');
                                                        }
                                                        function del_co_founfer(id)
                                                        {
                                                            $("#capital_" + id).remove();
                                                        }

                                                        $(document).ready(function () {
                                                            $("#action_marche").click(function () {
                                                                $("#bloc_marche").toggle("slow", function () {
                                                                    // Animation complete.
                                                                });
                                                            });
                                                            $("#action_info").click(function () {
                                                                $("#bloc_info").toggle("slow", function () {
                                                                    // Animation complete.
                                                                });
                                                            });
                                                            $("#action_personnes").click(function () {
                                                                $("#bloc_personnes").toggle("slow", function () {
                                                                    // Animation complete.
                                                                });
                                                            });
                                                        });
                                                        function ajout_autre() {

                                                            if (document.getElementById("objectif_autre").checked == true) {
                                                                document.getElementById("autre_objectif").style.display = "block";
                                                            } else {
                                                                document.getElementById("autre_objectif").style.display = "none";
                                                            }
                                                        }
                                                        function ajout_bm() {

                                                            if (document.getElementById("bm_autre").checked == true) {
                                                                document.getElementById("autre_bm").style.display = "block";
                                                            } else {
                                                                document.getElementById("autre_bm").style.display = "none";
                                                            }
                                                        }
                                                        function appeller_modal() {
                                                            $("#foo").trigger("click");
                                                        }

        </script>
        <!-- Button trigger modal -->
        <button id="foo" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" style="width: 0px; height: 0px; padding: 0px; border: 0px">

        </button>
        <?php
        if (isset($_POST["societe"])) {

            // Startup
            $societe = addslashes(utf8_decode(($_POST["societe"])));
            $juridique = addslashes(utf8_decode(($_POST["juridique"])));
            $siret = addslashes(utf8_decode(($_POST["siret"])));

            $siteweb = addslashes(utf8_decode(($_POST["website"])));
            $nom_marche = addslashes(utf8_decode(($_POST["nom_marche"])));
            $secteur = addslashes(utf8_decode(($_POST["secteur"])));
            $sous_secteur = addslashes(utf8_decode(($_POST["sous_secteur"])));
            $concours = addslashes(utf8_decode(($_POST["concours"])));
            $subventions = addslashes(utf8_decode(($_POST["subventions"])));
            $nom_subvention = addslashes(utf8_decode(($_POST["nom_subvention"])));
            $nom_concours = addslashes(utf8_decode(($_POST["nom_concours"])));
            $nom_grand_compte = addslashes(utf8_decode(($_POST["nom_grand_compte"])));

            // Votre levée de fonds
            $montant = addslashes(utf8_decode(($_POST["montant_rech"])));
            $date_rech = addslashes(utf8_decode($_POST["date_rech"]));
            $split = explode("/", $date_rech);
            $annee = $split[0];
            $mois = $split[1];
            $jour = $split[2];
            $creation = $annee . "-" . $mois . "-" . $jour;

            $objectifs = $_POST["objectif"];
            $objectif = "";
            for ($i = 0; $i < count($objectifs); $i++) {
                $objectif .= " " . $objectifs[$i] . ",";
            }

            $typerech = $_POST["rech"];
            $rechs = "";
            for ($i = 0; $i < count($typerech); $i++) {
                $rechs .= " " . $typerech[$i] . ",";
            }



            //Marché
            $business1 = $_POST["business"];
            $business = "";
            for ($i = 0; $i < count($business1); $i++) {
                $business .= " " . $business1[$i] . ",";
            }
            $innovation1 = $_POST["innovation"];
            $innovation = "";
            for ($i = 0; $i < count($innovation1); $i++) {
                $innovation .= " " . $innovation1[$i] . ",";
            }
            $protection1 = $_POST["protection"];
            $protection = "";
            for ($i = 0; $i < count($protection1); $i++) {
                $protection .= " " . $protection1[$i] . ",";
            }
            $cible1 = $_POST["cible"];
            $cible = "";
            for ($i = 0; $i < count($cible1); $i++) {
                $cible .= " " . $cible1[$i] . ",";
            }

            $taillem = addslashes($_POST["taillem"]);
            $sourcemarche = addslashes($_POST["sourcemarche"]);
            $volume = addslashes($_POST["volume"]);

            //Informations générales

            $effectif = addslashes(utf8_decode(($_POST["effectif"])));
            $date_creation = addslashes($_POST["date_creation"]);
            $split1 = explode("/", $date_creation);
            $annee1 = $split1[0];
            $mois1 = $split1[1];
            $jour1 = $split1[2];
            $creation_dt = $annee1 . "-" . $mois1 . "-" . $jour1;


            $payant = addslashes($_POST['payant']);
            $grand_compte = addslashes($_POST['partenaire']);
            $autre_objectif = addslashes($_POST['autre_objectif']);
            $autre_bm = addslashes($_POST['autre_bm']);


//Personne à contacter
            $prenom = addslashes(utf8_decode(($_POST['prenom'])));
            $nom = addslashes(utf8_decode(($_POST['nom'])));
            $fonction = addslashes(utf8_decode(($_POST['fonction'])));
            $tel = addslashes(utf8_decode(($_POST['tel'])));
            $email = addslashes(utf8_decode(($_POST['email_contact'])));

            mysqli_query($link, "INSERT INTO `data_enquete` ( `email`, `societe`,`juridique`,`siret`, `montant`, `dt`, `type_invest`, `objectif`, `business`, `innovation`, `protection`, `taillem`, `volume`, `source`, `date_add`, `cible`, `effectif`, `date_creation`, `payant`, `grand_compte`, `nom`, `prenom`, `fonction`, `tel`, `autre_objectif`, `autre_bm`, `secteur`, `sous_secteur`, `nom_marche`, `siteweb`, `concours`, `subventions`,`nom_subvention`,`nom_concours`,`nom_grand_compte`) VALUES('" . $email . "', '" . $societe . "','" . $juridique . "','" . $siret . "', '" . $montant . "', '" . $creation . "', '" . addslashes($rechs) . "', '" . $objectif . "', '" . $business . "', '" . $innovation . "', '" . $protection . "', '" . $taillem . "', '" . $volume . "', '" . $sourcemarche . "', '" . date('Y-m-d H:i:s') . "', '" . $cible . "', '" . $effectif . "', '" . $creation_dt . "', '" . $payant . "', '" . $grand_compte . "', '" . $nom . "', '" . $prenom . "', '" . $fonction . "', '" . $tel . "', '" . $autre_objectif . "','" . $autre_bm . "','" . $secteur . "','" . $sous_secteur . "','" . $nom_marche . "','" . $siteweb . "','" . $concours . "','" . $subventions . "','" . $nom_subvention . "','" . $nom_concours . "','" . $nom_grand_compte . "');");
            $id_req = mysqli_insert_id($link);
            $prenoms_list = $_POST["prenom_co"];
            $noms_list = $_POST["nom_co"];
            $fonctions_list = $_POST["fonction_co"];
            foreach ($prenoms_list as $key => $investitem) {
                $prenom_co = addslashes($prenoms_list[$key]);
                $nom_co = addslashes($noms_list[$key]);
                $fonction_co = addslashes($fonctions_list[$key]);
                if ($prenom_co != '') {
                    mysqli_query($link, "INSERT INTO `data_enquete_founder` ( `id_enquete`, `nom`, `prenom`, `fonction`) VALUES ( " . $id_req . ", '" . $nom_co . "', '" . $prenom_co . "', '" . $fonction_co . "');");
                }
            }
            echo '<script>$( document ).ready(function() {
   appeller_modal();
});</script>';
        } // Fin isset
        ?>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Validation</h4>
                    </div>
                    <div class="modal-body">
                        <p>Merci pour votre collaboration.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:window.location = 'https://www.myfrenchstartup.com/fr/'">Fermer</button>

                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </body>
    <script>
        function reload_sous_secteur_bloc()
        {
            document.getElementById("bloc_sous_secteur_affiche").style.display = 'block';
            id_secteur = $('#secteur_select').val();


            $.ajax({
                method: "POST",
                url: "ajax_load_sous_secteur.php",
                data: {id_secteur: id_secteur}
            })
                    .done(function (data) {
                        $('#sous_secteur_bloc').html(data);
                    });




        }
        $(document).ready(function () {
            $(".bootstrap-switch-id-partenaire").click(function () {
                if (document.getElementById("partenaire").checked == true) {
                    document.getElementById("nom_grand_compte").style.display = "block";
                } else {
                    document.getElementById("nom_grand_compte").style.display = "none";
                    document.getElementById("nom_grand_compte").value = "";
                }
            });

        });
        $(document).ready(function () {
            $(".bootstrap-switch-id-concours").click(function () {
                if (document.getElementById("concours").checked == true) {
                    document.getElementById("nom_concours").style.display = "block";
                } else {
                    document.getElementById("nom_concours").style.display = "none";
                    document.getElementById("nom_concours").value = "";
                }
            });
        });

        $(document).ready(function () {
            $(".bootstrap-switch-id-subventions").click(function () {
                if (document.getElementById("subventions").checked == true) {
                    document.getElementById("nom_subvention").style.display = "block";
                } else {
                    document.getElementById("nom_subvention").style.display = "none";
                    document.getElementById("nom_subvention").value = "";
                }
            });
        });

    </script>
</html>
