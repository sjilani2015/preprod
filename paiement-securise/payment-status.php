<?php
// Include the configuration file  
require_once 'config.php';

// Include the database connection file  
require_once 'dbConnect.php';



include("../include/db.php");
include("../functions/functions.php");
include ('../config.php');

$payment_id = $statusMsg = '';
$status = 'error';

// Check whether the subscription ID is not empty 
if (!empty($_GET['sid'])) {
    $subscr_id = base64_decode($_GET['sid']);

    // Fetch subscription info from the database 
    $sqlQ = "SELECT S.*, P.name as plan_name, P.price as plan_amount, U.first_name, U.last_name, U.email FROM user_subscriptions as S LEFT JOIN users_premium as U On U.id = S.user_id LEFT JOIN plans as P On P.id = S.plan_id WHERE S.id = ?";
    $stmt = $db->prepare($sqlQ);
    $stmt->bind_param("i", $db_id);
    $db_id = $subscr_id;
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        // Subscription and transaction details 
        $subscrData = $result->fetch_assoc();
        $stripe_subscription_id = $subscrData['stripe_subscription_id'];
        $paid_amount = $subscrData['paid_amount'];
        $paid_amount_currency = $subscrData['paid_amount_currency'];
        $plan_interval = $subscrData['plan_interval'];
        $plan_period_start = $subscrData['plan_period_start'];
        $plan_period_end = $subscrData['plan_period_end'];
        $subscr_status = $subscrData['status'];

        $plan_name = $subscrData['plan_name'];
        $plan_amount = $subscrData['plan_amount'];

        $customer_name = $subscrData['first_name'] . ' ' . $subscrData['last_name'];
        $customer_email = $subscrData['payer_email'];

        $status = 'success';
        $statusMsg = 'Transaction réussie';
    } else {
        $statusMsg = "Transaction has been failed!";
    }
} else {
    header("Location: index.php");
    exit;
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('../metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
        
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
    </head>

    <body>
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('../layout/header-simple.php');
            } else {
                include ('../layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">
        <?php if (!empty($subscr_id)) { ?>
            <h1 class="<?php echo $status; ?>"><?php echo $statusMsg; ?></h1>

            <h4>Informations de paiement</h4>
            <p><b>Référence :</b> <?php echo $subscr_id; ?></p>
            <p><b>Subscription ID :</b> <?php echo $stripe_subscription_id; ?></p>
            <p><b>Montant payé :</b> <?php echo $paid_amount . ' ' . $paid_amount_currency; ?>
            </p>
            <p><b>Status:</b> <?php echo $subscr_status; ?></p>

            <h4>Subscription Information</h4>
            <p><b>Plan :</b> <?php echo $plan_name; ?></p>
            <p><b>Montant :</b> <?php echo $plan_amount . ' ' . STRIPE_CURRENCY; ?></p>
            <p><b>Plan Interval:</b> <?php echo $plan_interval; ?></p>
            <p><b>Du:</b> <?php echo $plan_period_start; ?></p>
            <p><b>A:</b> <?php echo $plan_period_end; ?></p>

            <h4>Client</h4>
            <p><b>Nom :</b> <?php echo $customer_name; ?></p>
            <p><b>Email :</b> <?php echo $customer_email; ?></p>
        <?php } else { ?>
            <h1 class="error">Your Transaction been failed!</h1>
            <p class="error"><?php echo $statusMsg; ?></p>
        <?php } ?>
                </div>
            </div>
        </div>
    </body>
</html>