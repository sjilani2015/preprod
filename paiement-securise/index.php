<?php 
 
// Include configuration file  
require_once 'config.php'; 
 
// Include the database connection file 
include_once 'dbConnect.php'; 
 
// Fetch plans from the database 
$sqlQ = "SELECT * FROM plans"; 
$stmt = $db->prepare($sqlQ); 
$stmt->execute(); 
$result = $stmt->get_result(); 
 
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Mes factures - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
        <link rel="stylesheet" href="css/style.css">
        <script src="https://js.stripe.com/v3/"></script>
    </head>
    <body class="preload page">
        
        <div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Paiement sécurisé via Stripe</h3>
        
        <!-- Plan Info -->
        <div>
            <b>Select Plan:</b>
            <select id="subscr_plan" class="form-control">
                <?php 
                if($result->num_rows > 0){ 
                    while($row = $result->fetch_assoc()){ 
                ?>
                    <option value="<?php echo $row['id']; ?>"><?php echo $row['name'].' [$'.$row['price'].'/'.$row['interval'].']'; ?></option>
                <?php 
                    } 
                } 
                ?>
            </select>
        </div>
    </div>
    <div class="panel-body">
        <!-- Display status message -->
        <div id="paymentResponse" class="hidden"></div>
        
        <!-- Display a subscription form -->
        <form id="subscrFrm">
            <div class="form-group">
                <label>Titulaire de la carte</label>
                <input type="text" id="name" class="form-control" placeholder="Titulaire de la carte" required="" autofocus="">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" id="email" class="form-control" placeholder="Email" required="">
            </div>
            
            <div class="form-group">
                <label>Informations de la carte</label>
                <div id="card-element">
                    <!-- Stripe.js will create card input elements here -->
                </div>
            </div>
            
            <!-- Form submit button -->
            <button id="submitBtn" class="btn btn-success">
                <div class="spinner hidden" id="spinner"></div>
                <span id="buttonText">Procéder au paiement (89 €)</span>
            </button>
        </form>
        
        <!-- Display processing notification -->
        <div id="frmProcess" class="hidden">
            <span class="ring"></span> Processing...
        </div>
    </div>
</div>
        
        
        
        
        
        
        
        <script src="js/checkout.js" STRIPE_PUBLISHABLE_KEY="<?php echo STRIPE_PUBLISHABLE_KEY; ?>" defer></script>
        
        
        
    </body>
</html>