<?php
include("include/db.php");
include("functions/functions.php");

function generateRandomString($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$password = generateRandomString(8);
$token = "MYFS" . $password;
mysqli_query($link, "update user set  donnees=1 where email='" . $_SESSION['data_login'] . "'");
mysqli_query($link, "insert into factures(user,dt,montant)values('".$_SESSION['data_login']."','".date('Y-m-d H:i:s')."','98')");

?>
<!DOCTYPE html>
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>Contact</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css" rel="stylesheet"/>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo url; ?>css/style.css">
    <link rel="stylesheet" href="<?php echo url; ?>css/main-color.css" id="colors">
    <link rel="stylesheet" href="<?php echo url; ?>css/design_system.css" id="colors">
    <link rel="stylesheet" href="<?php echo url; ?>file.css" id="colors">


    <style>

        .colgrid1-2-1{
            display: grid;
            grid-template-columns: 255px 600px 255px;
            grid-gap: 0px;
            grid-auto-rows:519px;
        }
        label{
            font-size: 1.2rem !important;
            font-family: Roboto !important;
            color: #798999 !important;
            margin-bottom: 8px !important;
            height: 14px !important;
            width: 226px !important;
            text-align: left !important;
            letter-spacing: 0px !important;
            opacity: 1 !important;
        }
        /* input[type='radio']:checked:after {
             top: 239px;
             left: 303px;
             width: 12px;
             height: 12px;
             background:padding-box;
             background: #D8E2ED;
             opacity: 1;
         }*/
        input[type='text']{
            top: 283px;
            left: 300px;
            width: 255px;
            height: 40px;
            background: var(--arrière-plan) 0% 0% no-repeat padding-box;
            border: 1px solid var(--background-1);
            background: #F6F9FC 0% 0% no-repeat padding-box;
            border: 1px solid #D8E2ED;
            opacity: 1;
        }
        input[type='email']{
            width: 350px !important;
            height: 40px !important;
            border: 1px !important;
            background: #F6F9FC !important;
            border: 1px solid #D8E2ED !important;
            border-radius: 4px !important;
            opacity: 1 !important;
        }
        .gridformtext{
            display: grid;
            grid-template-columns: 255px 253px;
            grid-gap: 32px;
            grid-auto-rows:62px;
        }
        textarea{
            width: 540px!important;
            height: 120px!important;
            background: #F6F9FC!important;
            border: 1px solid #D7E9EC!important;
            border-radius: 4px!important;
        }
        .pricing-h1{
            text-align: center;
            font-family: 'EuclidBold';
            font-size:4.8rem;
            line-height: 4.8rem;
            margin-top: 40px;
            margin-bottom: 24px;
            color:#2D3B48;
        }




    </style> 


</head>

<body style="background:#F6F9FC ; font-family: 'EuclidLight';">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header Container
        ================================================== -->
<?php include('header.php'); ?>
        <div class="wrapper" style="padding-left:0px;padding-right: 0px">
            <div class="colgrid1-2-1" style="margin-bottom:32px">
                <div></div>
                <div style="margin-top:16px;background-color: #FFFFFF;">
                    <div style="text-align: center" class="pricing-h1">Transaction réussie</div>
                    <div style="text-align: center;line-height: 30px;">Merci<br>
                        Paiement effectué avec succès<br>
                        Veuillez noter le N° de la transaction : <?php echo $token;  ?><br>
                        Votre transaction est terminée et vous allez recevoir par email un avis accusant réception de votre achat. Vous pouvez connecter à votre compte sur myfrenchstartup.com/mon-compte pour consulter les détails de cette transaction.</div>




                </div>
            </div>
            <div><?php include('footer1.php'); ?></div>
        </div>
    </div>
    <script src="<?php echo url; ?>jquery.min.js"></script>

    <script src="<?php echo url; ?>bootstrap.min.js"></script>
    <script src="<?php echo url; ?>jquery.knob.js"></script>
    <!-- Sparkline -->

    <script>
        $(document).ready(function () {
            $("#search-box").keyup(function () {
                $.ajax({
                    type: "POST",
                    url: "<?php echo url ?>readCountry.php",
                    data: 'keyword=' + $(this).val(),
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            });
        });
        function selectCountry(val) {
            const words = val.split('/');
            $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo url ?>' + val;
        }
        function selectInvest(val) {
            const words = val.split('/');
            $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo url ?>' + val;
        }
        function selectEntrepreneur(val) {
            const words = val.split('/');
            $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo url ?>' + val;
        }
        function selectTags(val) {
            const words = val.split('/');
            $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo url ?>' + val;
        }
        function selectRegion(val) {
            const words = val.split('/');
            $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo url ?>' + val;
        }




    </script>
</body>
</html>