<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

mysqli_query($link, "insert into study_tracking(action,ip,date_add)values('Page Add Deal Flow','" . $_SERVER['REMOTE_ADDR'] . "','" . date('Y-m-d H:i:s') . "')");
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Ajouter une startup - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="formpage">
                    <div class="formpage__content">
                        <form class="stepy-basic" action="<?php echo URL ?>/startup-ajoutee" method="post" enctype="multipart/form-data">
                            <div class="formpage__title">
                                <h1>Pour ajouter votre Deal Flow,<br />veuillez remplir le formulaire ci-dessous.</h1>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Informations générales</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>* Nom commercial de votre startup</label>
                                        <input type="text" value=""  name="societe"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Nom juridique</label>
                                        <input type="text" name="juridique" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>SIRET / SIREN</label>
                                        <input type="text" value="" name="siret"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Site web</label>
                                        <input type="text" name="website" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Nombre d'employés</label>
                                        <input type="text" name="effectif" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Date de création</label>
                                        <input type="date" value="" name="date_creation"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Secteur d'activité</label>
                                        <div class="custom-select">
                                            <select class="form-control" data-placeholder="" name="secteur" id="secteur_select" onchange="reload_sous_secteur_bloc()">
                                                <option></option>
                                                <?php
                                                $row_list_secteur = getListSecteurTotal();
                                                if (!empty($row_list_secteur)) {
                                                    foreach ($row_list_secteur as $secteur) {
                                                        ?>
                                                        <option value="<?php echo $secteur['id']; ?>"><?php echo utf8_encode($secteur['nom_secteur']); ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="formgrid__item" id="bloc_sous_secteur_affiche" style="display: none">
                                        <label>* Sous secteur d'activité</label>
                                        <div class="custom-select" id="sous_secteur_bloc">
                                            <select class="form-control" name="sous_secteur"  >
                                                <option></option>

                                            </select>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Votre levée de fonds</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>Montant recherché en k€</label>
                                        <input type="text" value="" name="montant_rech"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Date de clôture de votre levée de fonds</label>
                                        <input type="date" value="" name="date_rech"  class="form-control" />
                                    </div>

                                    <div class="formgrid__item">
                                        <label>Objectif de ce tour de table</label>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table text-nowrap table-bordered" style="font-size: 13px;">
                                            <tbody>
                                                <tr style="text-align: center">

                                                    <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Marketing &<br>communication</td>
                                                    <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Recrutement<br>Développeurs</td>
                                                    <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Recrutement<br>Commerciaux</td>
                                                    <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">R&D</td>
                                                    <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Acquisition<br>Matériels</td>
                                                    <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Internationalisation</td>
                                                    <td style="width: 127px; padding-left: 5px; padding-right: 5px;padding-top: 5px;padding-bottom: 5px;">Autres</td>
                                                </tr>
                                                <tr class="style_check" style="text-align: center">

                                                    <td><input type="checkbox" name="objectif[]" value="Marketing & communication" ></td>
                                                    <td><input type="checkbox" name="objectif[]" value="Recrutement Développeurs" ></td>
                                                    <td><input type="checkbox" value="Recrutement Commerciaux" name="objectif[]"/></td>
                                                    <td><input type="checkbox" value="R&D" name="objectif[]" /></td>
                                                    <td><input type="checkbox" value="Acquisition matériels" name="objectif[]" /></td>
                                                    <td><input type="checkbox" value="Internationalisation" name="objectif[]" /></td>
                                                    <td><input type="checkbox" value="Autres" name="objectif[]" id="objectif_autre" onclick="ajout_autre()" /></td>
                                                </tr>

                                            </tbody>

                                        </table>
                                        <input type="text" name="autre_objectif" id="autre_objectif" placeholder="Merci de préciser" style="display: none; margin-top: 15px;" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Twitter</label>
                                        <input type="text" placeholder="@company" value="" name="twitter"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>LinkedIn</label>
                                        <input placeholder="https://www.linkedin.com/company" type="text" value="" name="linkedin"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Vidéo Youtube</label>
                                        <input type="text" placeholder="https://" value="" name="youtube"  class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Activité</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>* Marché</label>
                                        <div class="custom-select">
                                            <select class="form-control" data-placeholder="" name="list_sector">
                                                <option></option>
                                                <?php
                                                $row_list_secteur = getListSecteurTotal();
                                                if (!empty($row_list_secteur)) {
                                                    foreach ($row_list_secteur as $secteur) {
                                                        ?>
                                                        <option value="<?php echo $secteur['id']; ?>"><?php echo utf8_encode($secteur['nom_secteur']); ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Activité</label>
                                        <div class="custom-select">
                                            <select class="form-control" data-placeholder="" name="list_marche">
                                                <option></option>
                                                <?php
                                                $sql_activite = mysqli_query($link, "Select  * From  last_activite  order by nom_activite");
                                                while ($marche = mysqli_fetch_array($sql_activite)) {
                                                    ?>
                                                    <option value="<?php echo $marche['id']; ?>"><?php echo utf8_encode($marche['nom_activite']); ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Tags</label>
                                        <input type="text" value="Startup" name=""  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Présentation de votre startup (200 à 1000 caractères)</label>
                                        <textarea type="text" value="Startup" name="tags"class="form-control" placeholder="Description détaillée de votre Startup" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Fondateur</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>Prénom</label>
                                        <input placeholder="" type="text" value="" name="prenom_founder"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Nom</label>
                                        <input placeholder="" type="text" value="" name="nom_founder"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Activité</label>
                                        <div class="custom-select">
                                            <select class="form-control" data-placeholder="" name="fonction">
                                                <?php
                                                $sql_fonction = mysqli_query($link, "select * from fonction where valide=1");
                                                while ($fonction = mysqli_fetch_array($sql_fonction)) {
                                                    ?>


                                                    <option value="<?php echo $fonction['id'] ?>" >
                                                        <?php
                                                        echo $fonction['nom_fr'];
                                                        ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="formgrid__item">
                                        <label>LinkedIn</label>
                                        <input placeholder="Lien vers votre profil linkedin" type="text" value="" name="linkedin_founder"  class="form-control" />
                                    </div>
                                </div>
                            </div>

                            <div class="formpage__actions">
                                <button type="submit" class="btn btn-primary">Valider</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
                                                        (function (i, s, o, g, r, a, m) {
                                                            i['GoogleAnalyticsObject'] = r;
                                                            i[r] = i[r] || function () {
                                                                (i[r].q = i[r].q || []).push(arguments)
                                                            }, i[r].l = 1 * new Date();
                                                            a = s.createElement(o),
                                                                    m = s.getElementsByTagName(o)[0];
                                                            a.async = 1;
                                                            a.src = g;
                                                            m.parentNode.insertBefore(a, m)
                                                        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                                                        ga('create', 'UA-36251023-1', 'auto');
                                                        ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>