<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
if (!isset($_SESSION['data_login'])) {
    header("location:" . URL . "/connexion");
    exit();
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Ajouter une startup - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
        <style type="text/css" media="screen">
            .map_canvas {
                float: left;
            }
            .text-wrap{
                position: relative !important
            }
            .big_titre{
                padding: 10px;
                font-size: 20px;
                cursor: pointer;
                margin-bottom: 10px;
                margin-top: 10px;
            }
            .btn-primary{
                width:120px
            }
        </style>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="formpage">
                    <div class="formpage__content">
                        <form class="stepy-basic" action="<?php echo URL ?>/startup-ajoutee" method="post" enctype="multipart/form-data">
                            <div class="formpage__title">
                                <h1>Pour ajouter votre startup,<br />veuillez remplir le formulaire ci-dessous.</h1>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Informations générales</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>* Nom commercial</label>
                                        <input type="text" value=""  name="startup" required="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Nom juridique de votre startup</label>
                                        <input type="text" name="juridique" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Accroche</label>
                                        <input type="text" value="" name="accroche" required="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>SIRET</label>
                                        <input type="text" name="siret" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Date de création</label>
                                        <input type="date" value="" name="creation" required="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Adresse</label>
                                        <div class="map_canvas"></div>
                                        <input type="hidden" class="ll_input" name="postal_code" />
                                        <input type="hidden" class="ll_input" name="locality"  />
                                        <input type="hidden" class="ll_input" name="lat"/>
                                        <input type="hidden" class="ll_input" name="lng"/>
                                        <input type="hidden" class="ll_input" name="administrative_area_level_1"/>
                                        <input type="text" required="" id="geocomplete" name="adresse" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Email</label>
                                        <input type="email" value="" name="email" required="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Téléphone</label>
                                        <input type="phone" name="tel" required="" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Réseaux sociaux</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>* Site web</label>
                                        <input type="text" value="" name="website" required="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Ajouter votre logo</label>
                                        <input type="file" value="" name="logo"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Facebook</label>
                                        <input type="text" placeholder="https://www.facebook.com/company" value="" name="facebook"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Twitter</label>
                                        <input type="text" placeholder="@company" value="" name="twitter"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* LinkedIn</label>
                                        <input placeholder="https://www.linkedin.com/company" type="text" required="" value="" name="linkedin"  class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Vidéo Youtube</label>
                                        <input type="text" placeholder="https://" value="" name="youtube"  class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Activité</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>* Marché</label>
                                        <div class="custom-select">
                                            <select class="form-control" required="" data-placeholder="" name="list_sector">
                                                <option></option>
                                                <?php
                                                $row_list_secteur = getListSecteurTotal();
                                                if (!empty($row_list_secteur)) {
                                                    foreach ($row_list_secteur as $secteur) {
                                                        ?>
                                                        <option value="<?php echo $secteur['id']; ?>"><?php echo utf8_encode($secteur['nom_secteur']); ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Activité</label>
                                        <div class="custom-select">
                                            <select class="form-control" required="" data-placeholder="" name="list_marche">
                                                <option></option>
                                                <?php
                                                $sql_activite = mysqli_query($link, "Select  * From  last_activite  order by nom_activite");
                                                while ($marche = mysqli_fetch_array($sql_activite)) {
                                                    ?>
                                                    <option value="<?php echo $marche['id']; ?>"><?php echo utf8_encode($marche['nom_activite']); ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Tags</label>
                                        <input type="text" value="Startup" name="tags" required="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* Présentation de votre startup (200 à 1000 caractères)</label>
                                        <textarea type="text" value="Startup" name="description"class="form-control" placeholder="Description détaillée de votre Startup" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Fondateur</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>Prénom</label>
                                        <input placeholder="" type="text" value="" name="prenom_founder" required="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Nom</label>
                                        <input placeholder="" type="text" value="" name="nom_founder" required="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Activité</label>
                                        <div class="custom-select">
                                            <select class="form-control" data-placeholder="" name="fonction">
                                                <?php
                                                $sql_fonction = mysqli_query($link, "select * from fonction where valide=1");
                                                while ($fonction = mysqli_fetch_array($sql_fonction)) {
                                                    ?>


                                                    <option value="<?php echo $fonction['id'] ?>" >
                                                        <?php
                                                        echo $fonction['nom_fr'];
                                                        ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="formgrid__item">
                                        <label>* LinkedIn</label>
                                        <input placeholder="Lien vers votre profil linkedin" required="" type="text" value="" name="linkedin_founder"  class="form-control" />
                                    </div>
                                </div>
                            </div>

                            <div class="formpage__actions">
                                <button type="submit" class="btn btn-primary">Valider</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcCIjgD7FA1zVC6EQwS5V6I-6xuUuwMiQ&sensor=false&amp;libraries=places"></script>
        <script src="<?= JS_PATH; ?>jquery.geocomplete.js"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }

            $(function () {
                $("#geocomplete").geocomplete({
                    map: ".map_canvas",
                    details: "form",
                    types: ["geocode", "establishment"],
                });

                $("#find").click(function () {
                    $("#geocomplete").trigger("geocode");
                });
            });
        </script>
    </body>
</html>