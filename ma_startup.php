<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
if (!isset($_SESSION['data_login'])) {
    header("location:" . URL . "/connexion");
    exit();
}
$user_compte = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}

$currentPage = "startups";

function change_date_fr_chaine_related($date) {
    $split1 = explode(" ", $date);
    $split = explode("-", $split1[0]);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Mes startups - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
        <style>
            .account__right{
                padding-top: 35px;
                background: #f7fafc !important;
                border: 1px solid #eef2f6 !important;
            }
            .form-control{
                background-color: #fff !important;
                border-color: #eef2f6 !important;
            }
        </style>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>

        <div class="page-wrapper">
            <?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">            
                    <div class="account">
                        <div class="account__aside">
                            <div class="nav-vertical">
                                <div class="nav-vertical__title">
                                    <span class="title">Mon profil</span>
                                </div>
                                <div class="nav-vertical__list">
                                    <?php include ('account/menu.php'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="account__right" style="padding-top: 35px;">

                            <?php
                            $sql = mysqli_query($link, "select * from user_startup where iduser=" . $user_compte['id'] . " group by id_startup");
                            $nb_sup = mysqli_num_rows($sql);
                            if ($nb_sup > 0) {
                                ?>
                                <div style="font-family: 'EuclidBold';margin-bottom: 32px;color: #8694A1;">Mes startups</div>
                                <div class="table-responsive">
                                    <table class="invoices table">
                                        <thead>
                                            <tr>
                                                <th class="invoices__date">Startup</th>
                                                <th class="invoices__date">Ajouté le</th>
                                                <th class="invoices__date">Nombre de vues</th>
                                                <th class="invoices__date">Modifier</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            while ($data = mysqli_fetch_array($sql)) {
                                                $ma_sup = mysqli_fetch_array(mysqli_query($link, "select * from startup where id=" . $data['id_startup']));
                                                ?>
                                                <tr>
                                                    <td><?php echo $ma_sup['nom']; ?></td>
                                                    <td><?php echo change_date_fr_chaine_related($ma_sup['date_add']); ?></td>
                                                    <td><?php echo $ma_sup['vu']; ?></td>
                                                    <td><a href="<?php echo URL ?>/modifier-ma-startup/<?php echo generate_id($ma_sup['id']) ?>">Modifier</a></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="form-group form-group--btn" style="text-align: right">
                                    <a href="https://www.myfrenchstartup.com/ajouter-ma-startup" style="padding-left: 88px;padding-right: 88px;padding-top: 8px;padding-bottom: 8px;font-size: 16px;" target="_blank" class="btn btn-primary">Ajouter ma startup</a>
                                </div>
                                <div>Si votre startup ne figure pas dans notre base, veuillez la rajouter et prendre la main dessus.</div>

                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>