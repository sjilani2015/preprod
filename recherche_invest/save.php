<div id="modal-saveSearch" class="modal">
    <div class="modal__wrapper">
        <div class="modal__content">
            <div class="modal__header">
                <span class="title">Enregistrer votre recherche
                    <p class="subtitle">Enregistrez votre recherche et recevez une notification à chaque mise à jour d'une startup de votre liste</p>
                </span>
                <span class="modal__closeBtn"></span>
            </div>
            <div class="modal__body">
                <div class="form-group">
                    <div class="form-label">Nom de la recherche</div>
                    <div class="form-group-btn">
                        <input class="form-control" type="text" value="" placeholder="" name="">
                        <button class="btn btn-primary">Valider</button>
                    </div>
                </div>

                <div class="form-label">Mes recherches sauvegardées</div>
                <div class="savedsearches">
                    <div class="savedsearches__item">
                        <div class="savedsearches__item__name">
                            <div class="name">AGENCE WEB</div>
                            <div class="date">Recherche du 2021-09-13 15:20:21</div>
                        </div>
                        <div class="savedsearches__item__actions">
                            <a href="" title=""><span class="ico-dashboard"></span></a>
                            <a href="" title=""><span class="ico-eye"></span></a>
                            <a href="" title=""><span class="ico-trash"></span></a>
                        </div>
                    </div>
                    <div class="savedsearches__item">
                        <div class="savedsearches__item__name">
                            <div class="name">AGENCE WEB</div>
                            <div class="date">Recherche du 2021-09-13 15:20:21</div>
                        </div>
                        <div class="savedsearches__item__actions">
                            <a href="" title=""><span class="ico-dashboard"></span></a>
                            <a href="" title=""><span class="ico-eye"></span></a>
                            <a href="" title=""><span class="ico-trash"></span></a>
                        </div>
                    </div>
                    <div class="savedsearches__item">
                        <div class="savedsearches__item__name">
                            <div class="name">AGENCE WEB</div>
                            <div class="date">Recherche du 2021-09-13 15:20:21</div>
                        </div>
                        <div class="savedsearches__item__actions">
                            <a href="" title=""><span class="ico-dashboard"></span></a>
                            <a href="" title=""><span class="ico-eye"></span></a>
                            <a href="" title=""><span class="ico-trash"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
