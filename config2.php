<?php
//include("include/db.php");
//define ('URL', 'http://local.myfrenchstartup.com');
define('URL', 'https://www.myfrenchstartup.com');

// Path css à modifier également dans globals.scss
error_reporting(E_ALL);
ini_set('display_errors', '1');


define('JS_PATH', URL . '/static/js/');
define('CSS_PATH', URL . '/static/css/');
define('IMG_PATH', URL . '/static/images/');
define('VIDEOS_PATH', URL . '/static/videos/');

define('SITENAME', "MyFrenchStartup");
define('METADESC', "MyFrenchStartup est la solution SaaS de référence pour l’écosystème des startups en France. Nous déployons une gamme de services sans équivalent pour faciliter l’essor de l’innovation française, en général, et les relations entrepreneurs, investisseurs, corporate et institutionnels, en particulier. Data actualisées en temps réel, analyses dynamiques de la startup jusqu’aux secteurs, market place pour les rencontres de talents et les partenariats… Impossible n’est pas MyFrenchStartup.");

/**
 * Navigation links
 * Main menu
 */
if (!isset($_SESSION['data_login'])) {
    $mainmenu = array(
        array(
            'id' => 1,
            'pagename' => 'dashboard',
            'link' => 'mon-compte',
            'title' => 'Mon tableau de bord',
        ),
        array(
            'id' => 2,
            'pagename' => 'startups',
            'link' => 'recherche-startups',
            'title' => 'Startups',
        ),
        array(
            'id' => 3,
            'pagename' => 'investisseurs',
            'link' => 'liste-investisseurs',
            'title' => 'Investisseurs',
        ),
        array(
            'id' => 4,
            'pagename' => 'levees-de-fonds',
            'link' => 'levees-fonds',
            'title' => 'Levées de fonds',
        ),
        array(
            'id' => 5,
            'pagename' => 'investisseurs',
            'link' => 'ecosysteme-startup-france',
            'title' => 'Ecosystème',
        ),
        array(
                'id' => 6,
                'pagename' => 'dealflow',
                'link' => 'etude-dealflow',
                'title' => 'Deal Flow',
            ),
            
    );
} else {
    
    $oo=mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'")or die(mysqli_error($link));
    $us = mysqli_fetch_array($oo);
    if ($us['premium'] == 1) {
        $mainmenu = array(
            array(
                'id' => 1,
                'pagename' => 'dashboard',
                'link' => 'mon-dashboard',
                'title' => 'Mon tableau de bord',
            ),
         
            array(
                'id' => 3,
                'pagename' => 'investisseurs',
                'link' => 'liste-investisseurs',
                'title' => 'Investisseurs',
            ),
            array(
                'id' => 4,
                'pagename' => 'levees-de-fonds',
                'link' => 'levees-fonds',
                'title' => 'Levées de fonds',
            ),
            array(
                'id' => 5,
                'pagename' => 'investisseurs',
                'link' => 'ecosysteme-startup-france',
                'title' => 'Ecosystème',
            ),
            array(
                'id' => 6,
                'pagename' => 'dealflow',
                'link' => 'etude-dealflow',
                'title' => 'Deal Flow',
            )
        );
    } else {
        $mainmenu = array(
            array(
                'id' => 1,
                'pagename' => 'dashboard',
                'link' => 'mon-dashboard',
                'title' => 'Mon tableau de bord',
            ),
            array(
                'id' => 2,
                'pagename' => 'startups',
                'link' => 'recherche-startups',
                'title' => 'Startups',
            ),
            array(
                'id' => 3,
                'pagename' => 'investisseurs',
                'link' => 'liste-investisseurs',
                'title' => 'Investisseurs',
            ),
            array(
                'id' => 4,
                'pagename' => 'levees-de-fonds',
                'link' => 'levees-fonds',
                'title' => 'Levées de fonds',
            ),
            array(
                'id' => 5,
                'pagename' => 'investisseurs',
                'link' => 'ecosysteme-startup-france',
                'title' => 'Ecosystème',
            ),
            array(
                'id' => 6,
                'pagename' => 'dealflow',
                'link' => 'etude-dealflow',
                'title' => 'Deal Flow',
            ),
           
        );
    }
}


/*
 * Search columns
 */

$searchColumns = array(
    array(
        'id' => '0',
        'class' => 'table-search__startup',
        'name' => 'Startups',
        'sortable' => "true",
    ),
    array(
        'id' => '1',
        'class' => 'table-search__marche',
        'name' => 'Marché',
        'sortable' => "true",
    ),
    array(
        'id' => '2',
        'class' => 'table-search__maturite center',
        'name' => 'Maturité',
        'sortable' => "true",
    ),
    array(
        'id' => '3',
        'class' => 'table-search__totalfondsleves center',
        'name' => 'Total fonds levés',
        'sortable' => "true",
    ),
    array(
        'id' => '4',
        'class' => 'table-search__region center',
        'name' => 'Région',
        'sortable' => "true",
    ),
    array(
        'id' => '5',
        'class' => 'table-search__traction center',
        'name' => 'Traction',
        'sortable' => "true",
    ),
    // default Hidden columns
    array(
        'id' => '6',
        'class' => 'table-search__date center',
        'name' => 'Création',
        'sortable' => "true",
    ),
    array(
        'id' => '7',
        'class' => 'table-search__ville center',
        'name' => 'Ville',
        'sortable' => "true",
    ),
    array(
        'id' => '8',
        'class' => 'table-search__position center',
        'name' => 'Position',
        'sortable' => "true",
    ),
    array(
        'id' => '9',
        'class' => 'table-search__nblevees center',
        'name' => 'Nombre de levées',
        'sortable' => "true",
    ),
    array(
        'id' => '10',
        'class' => 'table-search__totaldernierelevee center',
        'name' => 'Montant dernière levée',
        'sortable' => "true",
    ),
    array(
        'id' => '11',
        'class' => 'table-search__datedernierelevee center',
        'name' => 'Date dernière levée',
        'sortable' => "true",
    ),
    array(
        'id' => '12',
        'class' => 'table-search__datedernieremodificationcapital center',
        'name' => 'Dernière modif. de capital',
        'sortable' => "true",
    ),
    array(
        'id' => '13',
        'class' => 'table-search__effectif center',
        'name' => 'Effectifs',
        'sortable' => "true",
    ),
    array(
        'id' => '14',
        'class' => 'table-search__competitionsecteur center',
        'name' => 'Compétition secteur',
        'sortable' => "true",
    ),
    array(
        'id' => '15',
        'class' => 'table-search__modeleeconomique center',
        'name' => 'Modèle économique',
        'sortable' => "true",
    ),
    array(
        'id' => '16',
        'class' => 'table-search__capaciteadelivrer center',
        'name' => 'Capacité à délivrer',
        'sortable' => "true",
    ),
    array(
        'id' => '17',
        'class' => 'table-search__derniereactualite center',
        'name' => 'Dernière actualité',
        'sortable' => "true",
    ),
    array(
        'id' => '18',
        'class' => 'table-search__tags',
        'name' => 'Tags',
        'sortable' => "true",
    )
);
