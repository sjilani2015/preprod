<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (!isset($_SESSION['data_login'])) {
    header("location:" . URL . "/connexion");
    exit(); 
}
$mon_user = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));

mysqli_query($link, "insert into user_dashboard(user,dt)values(" . $mon_user['id'] . ",'" . date('Y-m-d H:i:s') . "')");

$ssslq = mysqli_query($link, "select * from user_favoris where user=" . $mon_user['id']);
$nbrese = mysqli_num_rows($ssslq);
if ($nbrese > 0) {
    $dataas = mysqli_fetch_array($ssslq);
    $details = explode(",", $dataas['liste']);
    $nbs = count($details);
} else {
    $nbs = 0;
}

$nb_segment = mysqli_num_rows(mysqli_query($link, "select id from user_save_list where user=" . $mon_user['id']));

$date1 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d') - 30, date('y')));
$sql_notif = mysqli_query($link, "select * from notifications where user=" . $mon_user['id'] . " and date(dt)>='" . $date1 . "'");
$tots = 0;
while ($ddd = mysqli_fetch_array($sql_notif)) {
    $tab_sup = explode(",", $ddd['list_sup']);
    $nb_sup = count($tab_sup);
    $tab_lf = explode(",", $ddd['list_lf']);
    $nb_lf = count($tab_sup);
    $tab_verif = explode(",", $ddd['list_verif']);
    $nb_verif = count($tab_sup);

    $tots = $tots + $nb_sup + $nb_lf + $nb_verif;
}

function change_date_fr_chaine_related($date) {
    $split2 = explode(" ", $date);
    $split = explode("-", $split2[0]);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">

        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/maps.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/franceLow.min.js"></script>
        <script>am4core.addLicense("CH303325752");</script>
        <script>
            const   themeColorBlue = am4core.color('#00bff0'),
                    themeColorFill = am4core.color('#E1F2FA'),
                    themeColorGrey = am4core.color('#dadada'),
                    themeColorLightGrey = am4core.color('#F5F5F5'),
                    themeColorDarkgrey = am4core.color("#33333A"),
                    themeColorLine = am4core.color("#87D6E3"),
                    themeColorLineRed = am4core.color("#FF7C7C"),
                    themeColorLineGreen = am4core.color("#21D59B"),
                    themeColorColumns = am4core.color("#E1F2FA"),
                    labelColor = am4core.color('#798a9a');
        </script>
        <style>
            a{
                color:#323239;
            }
        </style>
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">
                    <section class="module">
                        <div class="module__title">Mon tableau de bord</div>
                        <div class="ctg ctg--1_3">
                            <aside class="bloc text-center">
                                <div class="datasbloc">
                                    <div class="datasbloc__key">
                                        Nombre de startups sélectionnées
                                    </div>
                                    <div class="datasbloc__val">
                                        <?php
                                        echo number_format($nbs, 0, ".", " ");
                                        ?> 
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">
                                        Nombre de segments
                                    </div>
                                    <div class="datasbloc__val">
                                        <?php
                                        echo number_format($nb_segment + 3, 0, ".", " ");
                                        ?> 
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">
                                        Nombre de notifications sur 30 jours
                                    </div>
                                    <div class="datasbloc__val">
                                        <?php
                                        echo number_format($tots, 0, ".", " ");
                                        ?> 
                                    </div>
                                </div>



                            </aside>

                            <div class="bloc">
                                <div class="bloc__title">Mes segments de recherche enregistrés</div>

                                <div class="tablebloc">
                                    <table id="mine3" class="table table-search">
                                        <thead>
                                            <tr style="max-height: unset; min-height: unset; height: unset !important;">
                                                <th class="text-center">Détails segment</th>
                                                <th class="dateCell text-center">Date segment</th>
                                                <th>Nom segment</th>
                                                <th class="text-center">Modifier</th>
                                                <th class="text-center">Supprimer</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            $sql_list = mysqli_query($link, "select date(dt) as ddt,nom,id from user_save_list where user=" . $mon_user['id'] . " order by dt desc");
                                            while ($data_list = mysqli_fetch_array($sql_list)) {
                                                ?>
                                                <tr style="max-height: unset; min-height: unset; height: unset !important;">
                                                    <td class="text-center">
                                                        <a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id($data_list['id']) ?>/<?php echo urlWriting($data_list['nom']); ?>">
                                                            <span style="background: #c6d1dd; display: inline-block; text-align: center; border-radius: 100px; padding: 10px;"><span class="ico-chart" style="margin-right: 0;"></span></span>
                                                        </a>
                                                    </td>
                                                    <td class="dateCell text-center"><?php echo change_date_fr_chaine_related($data_list['ddt']); ?></td>
                                                    <td><a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id($data_list['id']) ?>/<?php echo urlWriting($data_list['nom']); ?>"><?php echo $data_list['nom'] ?></a></td>
                                                    <td class="text-center">
                                                        <a href="<?php echo URL; ?>/recherche-startups/list/<?php echo generate_id($data_list['id']); ?>">
                                                            <span class="ico-edit"></span>
                                                        </a>
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="">
                                                            <span class="ico-trash"></span>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                            <tr style="max-height: unset; min-height: unset; height: unset !important;">
                                                <td class="text-center">
                                                    <a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id(38) ?>/NEXT_40">
                                                        <span style="background: #c6d1dd; display: inline-block; text-align: center; border-radius: 100px; padding: 10px;"><span class="ico-chart" style="margin-right: 0;"></span></span>
                                                    </a>
                                                </td>
                                                <td class="dateCell text-center"><?php echo change_date_fr_chaine_related($mon_user['date_add']); ?></td>
                                                <td><a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id(38) ?>/NEXT_40">NEXT 40</a></td>
                                                <td class="text-center">

                                                </td>
                                                <td class="text-center">

                                                </td>
                                            </tr>
                                            <tr style="max-height: unset; min-height: unset; height: unset !important;">
                                                <td class="text-center">
                                                    <a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id(35) ?>/FT_120">
                                                        <span style="background: #c6d1dd; display: inline-block; text-align: center; border-radius: 100px; padding: 10px;"><span class="ico-chart" style="margin-right: 0;"></span></span>
                                                    </a>
                                                </td>
                                                <td class="dateCell text-center"><?php echo change_date_fr_chaine_related($mon_user['date_add']); ?></td>
                                                <td><a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id(35) ?>/FT_120">FT 120</a></td>
                                                <td class="text-center">

                                                </td>
                                                <td class="text-center">

                                                </td>
                                            </tr>
                                            <tr style="max-height: unset; min-height: unset; height: unset !important;">
                                                <td class="text-center">
                                                    <a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id(31) ?>/startup_licornes">
                                                        <span style="background: #c6d1dd; display: inline-block; text-align: center; border-radius: 100px; padding: 10px;"><span class="ico-chart" style="margin-right: 0;"></span></span>
                                                    </a>
                                                </td>
                                                <td class="dateCell text-center"><?php echo change_date_fr_chaine_related($mon_user['date_add']); ?></td>
                                                <td><a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id(31) ?>/startup_licornes">Startups Licornes</a></td>
                                                <td class="text-center">

                                                </td>
                                                <td class="text-center">

                                                </td>
                                            </tr>



                                        </tbody>
                                    </table>

                                </div>





                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="bloc">
                            <div class="bloc__title">Mes startups</div>
                            <div class="table-responsive">
                                <table id="mine2" class="table table-search">
                                    <thead>
                                        <tr>
                                            <th>Startup</th>
                                            <th>Marché</th>
                                            <th>Maturité</th>
                                            <th>Total fonds levés</th>
                                            <th>Région</th>
                                            <th>Traction</th>
                                            <th>Création</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $dataas12 = mysqli_query($link, "select * from user_startup where iduser=" . $mon_user['id'] . " and etat=1 group by id_startup");
                                        while ($details = mysqli_fetch_array($dataas12)) {

                                            $favoris = mysqli_fetch_array(mysqli_query($link, "select * from startup where id=" . $details['id_startup']));
                                            $secteurs = mysqli_fetch_array(mysqli_query($link, "Select secteur.nom_secteur,secteur.id,secteur.nom_secteur_en From   secteur Inner Join  activite    On activite.secteur = secteur.id Where  activite.id_startup =" . $favoris['id']));
                                            $score = mysqli_fetch_array(mysqli_query($link, "Select  * From   startup_score where  id_startup=" . $favoris['id']));
                                            $tot_lf = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $favoris['id']));

                                            $ssecteurs = mysqli_fetch_array(mysqli_query($link, "Select  * From  activite  Where  activite.id_startup =" . $favoris['id']));
                                            $ligne_secteur_activite = mysqli_fetch_array(mysqli_query($link, "select * from secteur_activite where id_secteur=" . $secteurs['id'] . " and id_sous_secteur=" . $idsoussecteur . " and id_activite=" . $idactivite . " and id_sous_activite=" . $idsousactivite));

                                            if ($ssecteurs['sous_activite'] == "")
                                                $idsousactivite = 0;
                                            else
                                                $idsousactivite = $ssecteurs['sous_activite'];

                                            if ($ssecteurs['sous_secteur'] == "")
                                                $idsoussecteur = 0;
                                            else
                                                $idsoussecteur = $ssecteurs['sous_secteur'];

                                            if ($ssecteurs['activite'] == "")
                                                $idactivite = 0;
                                            else
                                                $idactivite = $ssecteurs['activite'];
                                            ?>
                                            <tr>
                                                <th class="table-search__startup">
                                                    <div class="startupInfos">
                                                        <div class="startupInfos__image">
                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($favoris['id']) . "/" . urlWriting(strtolower($favoris["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $favoris['logo']) ?>" alt="<?php echo "Startup " . $favoris["nom"]; ?>"></a>
                                                        </div>
                                                        <div class="startupInfos__desc">
                                                            <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($favoris['id']) . "/" . urlWriting(strtolower($favoris["nom"])) ?>" target="_blank"><?php echo stripslashes($favoris['nom']); ?></a></div>
                                                            <div class="companyLabel"><?php echo stripslashes($favoris['short_fr']); ?></div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <td class="table-search__position center">
                                                    <?php echo ($secteurs['nom_secteur']); ?>
                                                </td>
                                                <td class="table-search__position center">
                                                    <?php echo $score['maturite']; ?> 
                                                    <?php if ($score['maturite'] == $ligne_secteur_activite['moy_maturite']) { ?>
                                                        <span class="ico-stable"></span> 
                                                    <?php } ?>
                                                    <?php if ($score['maturite'] > $ligne_secteur_activite['moy_maturite']) { ?>
                                                        <span class="ico-raise"></span> 
                                                    <?php } ?>
                                                    <?php if ($score['maturite'] < $ligne_secteur_activite['moy_maturite']) { ?>
                                                        <span class="ico-decrease"></span> 
                                                    <?php } ?>
                                                </td>
                                                <td class="table-search__position center"><?php echo str_replace(",0", "", number_format(($tot_lf['somme'] / 1000), 1, ",", " ")); ?></td>
                                                <td class="table-search__position center"><?php echo ($favoris['region_new']); ?></td>
                                                <td class="table-search__position center"><?php echo ($score['traction_likedin']); ?></td>
                                                <td class="table-search__position center"><?php echo change_date_fr_chaine_related($favoris['date_complete']); ?></td>
                                                <td class="text-center">
                                                    <a href="<?php echo URL; ?>/modifier-ma-startup/<?php echo generate_id($favoris['id']); ?>">
                                                        <span class="ico-edit"></span>
                                                    </a>
                                                </td>
                                            </tr>

                                            <?php
                                            $l++;
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="bloc">
                            <div class="bloc__title">Mes favoris</div>
                            <div class="table-responsive">
                                <table id="mine2" class="table table-search">
                                    <thead>
                                        <tr>
                                            <th>Startup</th>
                                            <th>Marché</th>
                                            <th>Maturité</th>
                                            <th>Total fonds levés</th>
                                            <th>Région</th>
                                            <th>Traction</th>
                                            <th>Création</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $dataas = mysqli_fetch_array(mysqli_query($link, "select * from user_favoris where user=" . $mon_user['id']));
                                        $details = explode(",", $dataas['liste']);
                                        $nbs = count($details);
                                        for ($i = 0; $i < $nbs; $i++) {
                                            if ($details[$i] != '') {
                                                $favoris = mysqli_fetch_array(mysqli_query($link, "select * from startup where id=" . $details[$i]));
                                                $secteurs = mysqli_fetch_array(mysqli_query($link, "Select secteur.nom_secteur,secteur.id,secteur.nom_secteur_en From   secteur Inner Join  activite    On activite.secteur = secteur.id Where  activite.id_startup =" . $favoris['id']));
                                                $score = mysqli_fetch_array(mysqli_query($link, "Select  * From   startup_score where  id_startup=" . $favoris['id']));
                                                $tot_lf = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $favoris['id']));

                                                $ssecteurs = mysqli_fetch_array(mysqli_query($link, "Select  * From  activite  Where  activite.id_startup =" . $favoris['id']));
                                                $ligne_secteur_activite = mysqli_fetch_array(mysqli_query($link, "select * from secteur_activite where id_secteur=" . $secteurs['id'] . " and id_sous_secteur=" . $idsoussecteur . " and id_activite=" . $idactivite . " and id_sous_activite=" . $idsousactivite));

                                                if ($ssecteurs['sous_activite'] == "")
                                                    $idsousactivite = 0;
                                                else
                                                    $idsousactivite = $ssecteurs['sous_activite'];

                                                if ($ssecteurs['sous_secteur'] == "")
                                                    $idsoussecteur = 0;
                                                else
                                                    $idsoussecteur = $ssecteurs['sous_secteur'];

                                                if ($ssecteurs['activite'] == "")
                                                    $idactivite = 0;
                                                else
                                                    $idactivite = $ssecteurs['activite'];
                                                ?>
                                                <tr>
                                                    <th class="table-search__startup">
                                                        <div class="startupInfos">
                                                            <div class="startupInfos__image">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($favoris['id']) . "/" . urlWriting(strtolower($favoris["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $favoris['logo']) ?>" alt="<?php echo "Startup " . $favoris["nom"]; ?>"></a>
                                                            </div>
                                                            <div class="startupInfos__desc">
                                                                <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($favoris['id']) . "/" . urlWriting(strtolower($favoris["nom"])) ?>" target="_blank"><?php echo stripslashes($favoris['nom']); ?></a></div>
                                                                <div class="companyLabel"><?php echo stripslashes($favoris['short_fr']); ?></div>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <td class="table-search__position center">
                                                        <?php echo ($secteurs['nom_secteur']); ?>
                                                    </td>
                                                    <td class="table-search__position center">
                                                        <?php echo $score['maturite']; ?> 
                                                        <?php if ($score['maturite'] == $ligne_secteur_activite['moy_maturite']) { ?>
                                                            <span class="ico-stable"></span> 
                                                        <?php } ?>
                                                        <?php if ($score['maturite'] > $ligne_secteur_activite['moy_maturite']) { ?>
                                                            <span class="ico-raise"></span> 
                                                        <?php } ?>
                                                        <?php if ($score['maturite'] < $ligne_secteur_activite['moy_maturite']) { ?>
                                                            <span class="ico-decrease"></span> 
                                                        <?php } ?>
                                                    </td>
                                                    <td class="table-search__position center"><?php echo str_replace(",0", "", number_format(($tot_lf['somme'] / 1000), 1, ",", " ")); ?></td>
                                                    <td class="table-search__position center"><?php echo ($favoris['region_new']); ?></td>
                                                    <td class="table-search__position center"><?php echo ($score['traction_likedin']); ?></td>
                                                    <td class="table-search__position center"><?php echo change_date_fr_chaine_related($favoris['date_complete']); ?></td>

                                                </tr>

                                                <?php
                                                $l++;
                                            }
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="bloc">
                            <div class="bloc__title">Mes notifications</div>
                            <?php $sql_notif = mysqli_query($link, "select * from notifications where user=" . $mon_user['id'] . " order by dt desc limit 10"); ?>
                            <?php $totalNotifs = mysqli_num_rows($sql_notif); ?>
                            <?php if ($totalNotifs == 0) { ?>
                                <div class="alert alert--infos">Pas de notification envoyée à ce jour.</div>
                            <?php } else { ?>
                                <div class="table-responsive">
                                    <table id="mine2" class="table table-search">
                                        <thead>
                                            <tr>
                                                <th class="table-search__startup">Startup</th>
                                                <th class="table-search__date text-center">Date</th>
                                                <th class="table-search__tags">Notification</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_notif = mysqli_query($link, "select * from notifications where user=" . $mon_user['id'] . " order by dt desc limit 10");
                                            while ($data_notifs = mysqli_fetch_array($sql_notif)) {
                                                $list_sup = $data_notifs['list_sup'];
                                                $list_lf = $data_notifs['list_lf'];
                                                $list_verif = $data_notifs['list_verif'];
                                                ?>
                                                <?php
                                                if ($list_sup != '') {
                                                    $tabs_sup = explode(",", $list_sup);
                                                    $nb_list_sup = count($tabs_sup);
                                                    for ($i = 0; $i < 5; $i++) {
                                                        if ($tabs_sup[$i] != "") {
                                                            $startups = mysqli_fetch_array(mysqli_query($link, "Select * from startup Where id =" . $tabs_sup[$i]));
                                                            ?>
                                                            <tr>
                                                                <th class="table-search__startup">
                                                                    <div class="startupInfos">
                                                                        <div class="startupInfos__image">
                                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank">  
                                                                                <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $startups['logo']) ?>" alt="<?php echo "Startup " . $startups["nom"]; ?>">
                                                                            </a>
                                                                        </div>
                                                                        <div class="startupInfos__desc">
                                                                            <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a></div>
                                                                            <div class="companyLabel"><?php echo stripslashes($startups['short_fr']); ?></div>
                                                                        </div>
                                                                    </div>
                                                                </th>
                                                                <td class="table-search__date text-center">
                                                                    <?php echo change_date_fr_chaine_related($data_notifs['dt']); ?>
                                                                </td>

                                                                <td class="table-search__tags text-center">Nouvelle startup ajoutée à votre liste <?php echo $data_notifs['nom']; ?>.</td>

                                                            </tr>

                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                                <?php
                                                if ($list_lf != '') {
                                                    $tabs_sup1 = explode(",", $list_lf);
                                                    $nb_list_sup = count($tabs_sup1);
                                                    for ($i = 0; $i < 5; $i++) {
                                                        if ($tabs_sup1[$i] != "") {
                                                            $startups = mysqli_fetch_array(mysqli_query($link, "Select * from startup Where id =" . $tabs_sup1[$i]));
                                                            ?>
                                                            <tr>
                                                                <th class="table-search__startup">
                                                                    <div class="startupInfos">
                                                                        <div class="startupInfos__image">
                                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $startups['logo']) ?>" alt="<?php echo "Startup " . $startups["nom"]; ?>"></a>
                                                                        </div>
                                                                        <div class="startupInfos__desc">
                                                                            <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a></div>
                                                                            <div class="companyLabel"><?php echo stripslashes($startups['short_fr']); ?></div>
                                                                        </div>
                                                                    </div>
                                                                </th>
                                                                <td class="table-search__position center">
                                                                    <?php echo change_date_fr_chaine_related($data_notifs['dt']); ?>
                                                                </td>

                                                                <td class="table-search__position center">Nouvelle levée de fonds ajoutée à votre liste <?php echo $data_notifs['nom']; ?>.</td>

                                                            </tr>

                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                                <?php
                                                if ($list_verif != '') {
                                                    $tabs_sup2 = explode(",", $list_verif);
                                                    $nb_list_sup = count($tabs_sup1);
                                                    for ($i = 0; $i < 5; $i++) {
                                                        if ($tabs_sup2[$i] != "") {
                                                            $startups = mysqli_fetch_array(mysqli_query($link, "Select * from startup Where id =" . $tabs_sup2[$i]));
                                                            ?>
                                                            <tr>
                                                                <th class="table-search__startup">
                                                                    <div class="startupInfos">
                                                                        <div class="startupInfos__image">
                                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $startups['logo']) ?>" alt="<?php echo "Startup " . $startups["nom"]; ?>"></a>
                                                                        </div>
                                                                        <div class="startupInfos__desc">
                                                                            <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a></div>
                                                                            <div class="companyLabel"><?php echo stripslashes($startups['short_fr']); ?></div>
                                                                        </div>
                                                                    </div>
                                                                </th>
                                                                <td class="table-search__position center">
                                                                    <?php echo change_date_fr_chaine_related($data_notifs['dt']); ?>
                                                                </td>

                                                                <td class="table-search__position center">Nouveau mouvement Bodacc ajouté à votre liste <?php echo $data_notifs['nom']; ?>.</td>

                                                            </tr>

                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>

                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            <?php } ?>
                        </div>
                    </section>





                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>jquery.dataTables.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>datatables.bootstrap.min.js?<?= time(); ?>"></script>

        <script>
            var table = jQuery('#mine').DataTable({
                "searching": false,
                "nextPrev": false,
                "bLengthChange": false,
                "bInfo": false,
                "bPaginate": true,
                "autoWidth": false,
                //"fixedColumns": false,
                initComplete: (settings, json) => {
                    $('#paginateTable').empty();
                    $('#mine_paginate').appendTo('#paginateTable');
                },
                "language": {
                    "paginate": {
                        "previous": "Précédent",
                        "next": "Suivant"
                    }
                }
            });
            var table2 = jQuery('#mine2').DataTable({
                "searching": false,
                "nextPrev": false,
                "bLengthChange": false,
                "bInfo": false,
                "bPaginate": true,
                "autoWidth": false,
                //"fixedColumns": false,
                initComplete: (settings, json) => {
                    $('#paginateTable').empty();
                    $('#mine2_paginate').appendTo('#paginateTable2');
                },
                "language": {
                    "paginate": {
                        "previous": "Précédent",
                        "next": "Suivant"
                    }
                }
            });
            var table2 = jQuery('#mine3').DataTable({
                "searching": false,
                "nextPrev": false,
                "bLengthChange": false,
                "bInfo": false,
                "bPaginate": true,
                "autoWidth": false,
                //"fixedColumns": false,
                "aoColumnDefs": [
                    {"targets": 4,
                        "orderable": false
                    },
                    {"targets": 3,
                        "orderable": false
                    }
                ],
                initComplete: (settings, json) => {
                    $('#paginateTable3').empty();
                    $('#mine3_paginate').appendTo('#paginateTable3');
                },
                "language": {
                    "paginate": {
                        "previous": "Précédent",
                        "next": "Suivant"
                    }
                }
            });
        </script>
        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>