<?php
include("include/db.php");
include("functions/functions.php");

?>
<div class="bloc__title">Fondateurs</div>
<div class="tablebloc tablebloc--light">
    <table class="table">
        <thead> 
            <tr>
                <th class="text-center">Fondateur</th>
                <th class="text-center">Âge</th>
                <th class="text-center">École</th>
                <th class="text-center">Diplôme</th>
                <th class="text-center" width="40%">Compétences</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $ligne_personne = mysqli_fetch_array(mysqli_query($link, "select * from personnes where id=" . $_GET['id']));

            $cch_skils = "";
            $lign_exp = mysqli_fetch_array(mysqli_query($link, "select * from personnes_phantombuster where id_personnes=" . $ligne_personne['id']));
            $fonction_ligne = mysqli_fetch_array(mysqli_query($link, "select * from fonction where id=" . $ligne_personne["fonction"]));
            $ecole_ligne = mysqli_fetch_array(mysqli_query($link, "select * from ecole_personnes where idpersonne=" . $ligne_personne["id"] . " order by a desc limit 1"));
            $list_competances = mysqli_query($link, "select * from personnes_competence where id_personne=" . $ligne_personne["id"] . " limit 4");
            $ch_skils = explode(',', $lign_exp['allSkills']);
            $nb_comm = count($ch_skils);
            if ($nb_comm > 0) {
                for ($i = 0; $i < 5; $i++) {
                    if ($ch_skils[$i] != '') {
                        $cch_skils .= ($ch_skils[$i]) . ", ";
                    }
                }
            } else {
                $cch_skils = "-";
            }
            ?>
            <tr>
                <td class="text-center">
                    <div class="avatar">
                        <i class="ico-avatar"></i>
                    </div>
                    <strong><?php echo stripslashes(($ligne_personne['prenom'] . " " . $ligne_personne['nom'])); ?></strong>
                    <div class="fonction"><?php echo stripslashes($fonction_ligne['nom_fr']); ?></div>
                </td>
                <td class="text-center"><?php
                    if ($ligne_personne['age'] != "")
                        echo $ligne_personne['age'];
                    else
                        echo "-";
                    ?></td>
                <td class="text-center"><?php
                    if ($lign_exp['schoolName_3'] != "") {
                        echo ($lign_exp['schoolName_3']);
                    } else if ($lign_exp['schoolName_2'] != "") {
                        echo ($lign_exp['schoolName_2']);
                    } else if ($lign_exp['schoolName_1'] != "") {
                        echo ($lign_exp['schoolName_1']);
                    } else {
                        echo "-";
                    }
                    ?></td>
                <td class="text-center"><?php
                    if ($lign_exp['degree_3'] != "") {
                        echo ($lign_exp['degree_3']);
                    } else if ($lign_exp['degree_2'] != "") {
                        echo ($lign_exp['degree_2']);
                    } else if ($lign_exp['degree_1'] != "") {
                        echo ($lign_exp['degree_1']);
                    } else {
                        echo "-";
                    }
                    ?></td>
                <td class="text-center"><?php echo $cch_skils; ?></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="historyList">
    <?php
    if ($lign_exp['jobTitle_1'] != "") {
        ?>
        <div class="historyList__item">
            <div class="historyList__item__date"><?php echo ($lign_exp['dateRange_job_1']) ?></div>
            <div class="historyList__item__infos"><?php echo ($lign_exp['jobTitle_1']) ?> chez <?php echo ($lign_exp['companyName_1']) ?></div>
        </div>
    <?php } ?>
    <?php
    if ($lign_exp['jobTitle_2'] != "") {
        ?>
        <div class="historyList__item">
            <div class="historyList__item__date"><?php echo ($lign_exp['dateRange_job_2']) ?></div>
            <div class="historyList__item__infos"><?php echo ($lign_exp['jobTitle_2']) ?> chez <?php echo ($lign_exp['companyName_2']) ?></div>
        </div>
    <?php } ?>
    <?php
    if ($lign_exp['jobTitle_3'] != "") {
        ?>
        <div class="historyList__item">
            <div class="historyList__item__date"><?php echo ($lign_exp['dateRange_job_3']) ?></div>
            <div class="historyList__item__infos"><?php echo ($lign_exp['jobTitle_3']) ?> chez <?php echo ($lign_exp['companyName_3']) ?></div>
        </div>
    <?php } ?>

</div>