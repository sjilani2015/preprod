<?php

/**
 *
 * Fonction qui permet de générer le slug d'un nom afin de l'écrire dans l'url
 * @category None
 * @package  None
 * @author   Hamadi Lanouar <h.lanouar@myfrenchstartup.com>
 * @link     https://www.myfrenchstartup.com
 * @param string $str
 * @param boolean $utf8
 * @return string
 */
function urlWriting($str, $utf8 = true) {
    $str = (string) $str;
    if (is_null($utf8)) {
        if (!function_exists('mb_detect_encoding')) {
            $utf8 = (strtolower(mb_detect_encoding($str)) == 'utf-8');
        } else {
            $length = strlen($str);
            $utf8 = true;
            for ($i = 0; $i < $length; $i++) {
                $c = ord($str[$i]);
                if ($c < 0x80)
                    $n = 0;# 0bbbbbbb
                elseif (($c & 0xE0) == 0xC0)
                    $n = 1;# 110bbbbb
                elseif (($c & 0xF0) == 0xE0)
                    $n = 2;# 1110bbbb
                elseif (($c & 0xF8) == 0xF0)
                    $n = 3;# 11110bbb
                elseif (($c & 0xFC) == 0xF8)
                    $n = 4;# 111110bb
                elseif (($c & 0xFE) == 0xFC)
                    $n = 5;# 1111110b
                else
                    return false;# Does not match any model
                for ($j = 0; $j < $n; $j++) { # n bytes matching 10bbbbbb follow ?
                    if (( ++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80)) {
                        $utf8 = false;
                        break;
                    }
                }
            }
        }
    }

    if (!$utf8)
        $str = utf8_encode($str);

    $transliteration = array(
        'Ĳ' => 'I', 'Ö' => 'O', 'Œ' => 'O', 'Ü' => 'U', 'ä' => 'a', 'æ' => 'a',
        'ĳ' => 'i', 'ö' => 'o', 'œ' => 'o', 'ü' => 'u', 'ß' => 's', 'ſ' => 's',
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A',
        'Æ' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Ç' => 'C', 'Ć' => 'C',
        'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D', 'È' => 'E',
        'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E', 'Ę' => 'E', 'Ě' => 'E',
        'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G', 'Ġ' => 'G', 'Ģ' => 'G',
        'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
        'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I', 'İ' => 'I', 'Ĵ' => 'J',
        'Ķ' => 'K', 'Ľ' => 'K', 'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ł' => 'L',
        'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N', 'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O',
        'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O',
        'Ŏ' => 'O', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Ş' => 'S',
        'Ŝ' => 'S', 'Ș' => 'S', 'Š' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
        'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ū' => 'U', 'Ů' => 'U',
        'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U', 'Ŵ' => 'W', 'Ŷ' => 'Y',
        'Ÿ' => 'Y', 'Ý' => 'Y', 'Ź' => 'Z', 'Ż' => 'Z', 'Ž' => 'Z', 'à' => 'a',
        'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
        'å' => 'a', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
        'ď' => 'd', 'đ' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
        'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e', 'ƒ' => 'f',
        'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h', 'ħ' => 'h',
        'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i', 'ĩ' => 'i',
        'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĵ' => 'j', 'ķ' => 'k', 'ĸ' => 'k',
        'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l', 'ŀ' => 'l', 'ñ' => 'n',
        'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n', 'ŋ' => 'n', 'ò' => 'o',
        'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o',
        'ŏ' => 'o', 'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'ś' => 's', 'š' => 's',
        'ť' => 't', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ū' => 'u', 'ů' => 'u',
        'ű' => 'u', 'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ÿ' => 'y',
        'ý' => 'y', 'ŷ' => 'y', 'ż' => 'z', 'ź' => 'z', 'ž' => 'z', 'Α' => 'A',
        'Ά' => 'A', 'Ἀ' => 'A', 'Ἁ' => 'A', 'Ἂ' => 'A', 'Ἃ' => 'A', 'Ἄ' => 'A',
        'Ἅ' => 'A', 'Ἆ' => 'A', 'Ἇ' => 'A', 'ᾈ' => 'A', 'ᾉ' => 'A', 'ᾊ' => 'A',
        'ᾋ' => 'A', 'ᾌ' => 'A', 'ᾍ' => 'A', 'ᾎ' => 'A', 'ᾏ' => 'A', 'Ᾰ' => 'A',
        'Ᾱ' => 'A', 'Ὰ' => 'A', 'ᾼ' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D',
        'Ε' => 'E', 'Έ' => 'E', 'Ἐ' => 'E', 'Ἑ' => 'E', 'Ἒ' => 'E', 'Ἓ' => 'E',
        'Ἔ' => 'E', 'Ἕ' => 'E', 'Ὲ' => 'E', 'Ζ' => 'Z', 'Η' => 'I', 'Ή' => 'I',
        'Ἠ' => 'I', 'Ἡ' => 'I', 'Ἢ' => 'I', 'Ἣ' => 'I', 'Ἤ' => 'I', 'Ἥ' => 'I',
        'Ἦ' => 'I', 'Ἧ' => 'I', 'ᾘ' => 'I', 'ᾙ' => 'I', 'ᾚ' => 'I', 'ᾛ' => 'I',
        'ᾜ' => 'I', 'ᾝ' => 'I', 'ᾞ' => 'I', 'ᾟ' => 'I', 'Ὴ' => 'I', 'ῌ' => 'I',
        'Θ' => 'T', 'Ι' => 'I', 'Ί' => 'I', 'Ϊ' => 'I', 'Ἰ' => 'I', 'Ἱ' => 'I',
        'Ἲ' => 'I', 'Ἳ' => 'I', 'Ἴ' => 'I', 'Ἵ' => 'I', 'Ἶ' => 'I', 'Ἷ' => 'I',
        'Ῐ' => 'I', 'Ῑ' => 'I', 'Ὶ' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M',
        'Ν' => 'N', 'Ξ' => 'K', 'Ο' => 'O', 'Ό' => 'O', 'Ὀ' => 'O', 'Ὁ' => 'O',
        'Ὂ' => 'O', 'Ὃ' => 'O', 'Ὄ' => 'O', 'Ὅ' => 'O', 'Ὸ' => 'O', 'Π' => 'P',
        'Ρ' => 'R', 'Ῥ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Ύ' => 'Y',
        'Ϋ' => 'Y', 'Ὑ' => 'Y', 'Ὓ' => 'Y', 'Ὕ' => 'Y', 'Ὗ' => 'Y', 'Ῠ' => 'Y',
        'Ῡ' => 'Y', 'Ὺ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'P', 'Ω' => 'O',
        'Ώ' => 'O', 'Ὠ' => 'O', 'Ὡ' => 'O', 'Ὢ' => 'O', 'Ὣ' => 'O', 'Ὤ' => 'O',
        'Ὥ' => 'O', 'Ὦ' => 'O', 'Ὧ' => 'O', 'ᾨ' => 'O', 'ᾩ' => 'O', 'ᾪ' => 'O',
        'ᾫ' => 'O', 'ᾬ' => 'O', 'ᾭ' => 'O', 'ᾮ' => 'O', 'ᾯ' => 'O', 'Ὼ' => 'O',
        'ῼ' => 'O', 'α' => 'a', 'ά' => 'a', 'ἀ' => 'a', 'ἁ' => 'a', 'ἂ' => 'a',
        'ἃ' => 'a', 'ἄ' => 'a', 'ἅ' => 'a', 'ἆ' => 'a', 'ἇ' => 'a', 'ᾀ' => 'a',
        'ᾁ' => 'a', 'ᾂ' => 'a', 'ᾃ' => 'a', 'ᾄ' => 'a', 'ᾅ' => 'a', 'ᾆ' => 'a',
        'ᾇ' => 'a', 'ὰ' => 'a', 'ᾰ' => 'a', 'ᾱ' => 'a', 'ᾲ' => 'a', 'ᾳ' => 'a',
        'ᾴ' => 'a', 'ᾶ' => 'a', 'ᾷ' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd',
        'ε' => 'e', 'έ' => 'e', 'ἐ' => 'e', 'ἑ' => 'e', 'ἒ' => 'e', 'ἓ' => 'e',
        'ἔ' => 'e', 'ἕ' => 'e', 'ὲ' => 'e', 'ζ' => 'z', 'η' => 'i', 'ή' => 'i',
        'ἠ' => 'i', 'ἡ' => 'i', 'ἢ' => 'i', 'ἣ' => 'i', 'ἤ' => 'i', 'ἥ' => 'i',
        'ἦ' => 'i', 'ἧ' => 'i', 'ᾐ' => 'i', 'ᾑ' => 'i', 'ᾒ' => 'i', 'ᾓ' => 'i',
        'ᾔ' => 'i', 'ᾕ' => 'i', 'ᾖ' => 'i', 'ᾗ' => 'i', 'ὴ' => 'i', 'ῂ' => 'i',
        'ῃ' => 'i', 'ῄ' => 'i', 'ῆ' => 'i', 'ῇ' => 'i', 'θ' => 't', 'ι' => 'i',
        'ί' => 'i', 'ϊ' => 'i', 'ΐ' => 'i', 'ἰ' => 'i', 'ἱ' => 'i', 'ἲ' => 'i',
        'ἳ' => 'i', 'ἴ' => 'i', 'ἵ' => 'i', 'ἶ' => 'i', 'ἷ' => 'i', 'ὶ' => 'i',
        'ῐ' => 'i', 'ῑ' => 'i', 'ῒ' => 'i', 'ῖ' => 'i', 'ῗ' => 'i', 'κ' => 'k',
        'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => 'k', 'ο' => 'o', 'ό' => 'o',
        'ὀ' => 'o', 'ὁ' => 'o', 'ὂ' => 'o', 'ὃ' => 'o', 'ὄ' => 'o', 'ὅ' => 'o',
        'ὸ' => 'o', 'π' => 'p', 'ρ' => 'r', 'ῤ' => 'r', 'ῥ' => 'r', 'σ' => 's',
        'ς' => 's', 'τ' => 't', 'υ' => 'y', 'ύ' => 'y', 'ϋ' => 'y', 'ΰ' => 'y',
        'ὐ' => 'y', 'ὑ' => 'y', 'ὒ' => 'y', 'ὓ' => 'y', 'ὔ' => 'y', 'ὕ' => 'y',
        'ὖ' => 'y', 'ὗ' => 'y', 'ὺ' => 'y', 'ῠ' => 'y', 'ῡ' => 'y', 'ῢ' => 'y',
        'ῦ' => 'y', 'ῧ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'p', 'ω' => 'o',
        'ώ' => 'o', 'ὠ' => 'o', 'ὡ' => 'o', 'ὢ' => 'o', 'ὣ' => 'o', 'ὤ' => 'o',
        'ὥ' => 'o', 'ὦ' => 'o', 'ὧ' => 'o', 'ᾠ' => 'o', 'ᾡ' => 'o', 'ᾢ' => 'o',
        'ᾣ' => 'o', 'ᾤ' => 'o', 'ᾥ' => 'o', 'ᾦ' => 'o', 'ᾧ' => 'o', 'ὼ' => 'o',
        'ῲ' => 'o', 'ῳ' => 'o', 'ῴ' => 'o', 'ῶ' => 'o', 'ῷ' => 'o', 'А' => 'A',
        'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E',
        'Ж' => 'Z', 'З' => 'Z', 'И' => 'I', 'Й' => 'I', 'К' => 'K', 'Л' => 'L',
        'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S',
        'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'K', 'Ц' => 'T', 'Ч' => 'C',
        'Ш' => 'S', 'Щ' => 'S', 'Ы' => 'Y', 'Э' => 'E', 'Ю' => 'Y', 'Я' => 'Y',
        'а' => 'A', 'б' => 'B', 'в' => 'V', 'г' => 'G', 'д' => 'D', 'е' => 'E',
        'ё' => 'E', 'ж' => 'Z', 'з' => 'Z', 'и' => 'I', 'й' => 'I', 'к' => 'K',
        'л' => 'L', 'м' => 'M', 'н' => 'N', 'о' => 'O', 'п' => 'P', 'р' => 'R',
        'с' => 'S', 'т' => 'T', 'у' => 'U', 'ф' => 'F', 'х' => 'K', 'ц' => 'T',
        'ч' => 'C', 'ш' => 'S', 'щ' => 'S', 'ы' => 'Y', 'э' => 'E', 'ю' => 'Y',
        'я' => 'Y', 'ð' => 'd', 'Ð' => 'D', 'þ' => 't', 'Þ' => 'T', 'ა' => 'a',
        'ბ' => 'b', 'გ' => 'g', 'დ' => 'd', 'ე' => 'e', 'ვ' => 'v', 'ზ' => 'z',
        'თ' => 't', 'ი' => 'i', 'კ' => 'k', 'ლ' => 'l', 'მ' => 'm', 'ნ' => 'n',
        'ო' => 'o', 'პ' => 'p', 'ჟ' => 'z', 'რ' => 'r', 'ს' => 's', 'ტ' => 't',
        'უ' => 'u', 'ფ' => 'p', 'ქ' => 'k', 'ღ' => 'g', 'ყ' => 'q', 'შ' => 's',
        'ჩ' => 'c', 'ც' => 't', 'ძ' => 'd', 'წ' => 't', 'ჭ' => 'c', 'ხ' => 'k',
        'ჯ' => 'j', 'ჰ' => 'h'
    );
    $str = str_replace(array_keys($transliteration), array_values($transliteration), $str);
    $str = trim(preg_replace('/([^ a-z0-9]+)/i', '', $str));

    $str = str_replace(' ', '_', $str);

    return $str;
}

/**
 *
 * Fonction qui permet de générer un nombre à partir de l'id startup
 * @category None
 * @package  None
 * @author   Hamadi Lanouar <h.lanouar@myfrenchstartup.com>
 * @link     https://www.myfrenchstartup.com
 * @param int $id
 * @return int
 */
function generate_id($id) {
    $nb = ($id * 11) + 5124;
    return $nb;
}

/**
 *
 * Fonction qui permet de regénérer l'id startup à partir d'un nombre donné
 * @category None
 * @package  None
 * @author   Hamadi Lanouar <h.lanouar@myfrenchstartup.com>
 * @link     https://www.myfrenchstartup.com
 * @param int $id
 * @return int
 */
function degenerate_id($id) {
    $nb = ($id - 5124) / 11;
    return $nb;
}

function change_date_fr_chaine($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "janvier";
    if (($mois == "02"))
        $mm = "février";
    if (($mois == "03"))
        $mm = "mars";
    if (($mois == "04"))
        $mm = "avril";
    if (($mois == "05"))
        $mm = "mai";
    if (($mois == "06"))
        $mm = "juin";
    if (($mois == "07"))
        $mm = "juillet";
    if (($mois == "08"))
        $mm = "août";
    if (($mois == "09"))
        $mm = "septembre";
    if (($mois == "10"))
        $mm = "octobre";
    if (($mois == "11"))
        $mm = "novembre";
    if ($mois == "12")
        $mm = "décembre";

    $creation = $jour . " " . $mm . " " . $annee;
    return $creation;
}
function change_date_fr_chaine_relateds($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "jan";
    if (($mois == "02"))
        $mm = "fév";
    if (($mois == "03"))
        $mm = "mar";
    if (($mois == "04"))
        $mm = "avr";
    if (($mois == "05"))
        $mm = "mai";
    if (($mois == "06"))
        $mm = "jui";
    if (($mois == "07"))
        $mm = "jul";
    if (($mois == "08"))
        $mm = "aoû";
    if (($mois == "09"))
        $mm = "sep";
    if (($mois == "10"))
        $mm = "oct";
    if (($mois == "11"))
        $mm = "nov";
    if ($mois == "12")
        $mm = "déc";

    $creation =  $mm . " " . $annee;
    return $creation;
}

function requete_base($requete) {
    $dbroot = 'localhost';
    $dbuser = 'myfrenchuser';
    $dbpass = 'My#2fsDb3';
    $dbname = 'myfrenchdata';

    $link = mysqli_connect($dbroot, $dbuser, $dbpass) or die('<h1>Impossible de trouver le serveur</h1>');
    mysqli_select_db($link, $dbname) or die('<h1>Impossible de trouver la base de données</h1>');
    $resultat = array();
    $res = mysqli_query($link, $requete)or die(mysqli_error($link));
    $nb = mysqli_num_rows($res);
    if ($nb > 0) {
        $i = 0;
        while ($val = mysqli_fetch_array($res)) {
            $resultat[$i] = $val;
            $i++;
        }
    }
    return $resultat;
}

function getListSecteurTotal() {
    return requete_base("Select  * From  secteur  order by secteur.nom_secteur");
}

function getListSousSecteurTotal() {
    return requete_base("Select  * From  sous_secteur  order by sous_secteur.nom_sous_secteur");
}

function getListActiviteTotal() {
    return requete_base("Select  * From  last_activite  order by last_activite.nom_activite");
}

function getLastNewsDeal($nb) {
    return requete_base("select lf.id,startup.nom,lf.date_ajout,startup.logo,startup.technologie,startup.region_new,startup.couverture, lf.id_startup,lf.montant,lf.rachat,lf.de  from lf inner join startup on startup.id=lf.id_startup where startup.status=1 and lf.rachat=0 order by date_ajout desc limit " . $nb);
}

function getSecteurByIdStartup($id) {
    return requete_base("Select  secteur.nom_secteur,secteur.id,secteur.nom_secteur_en From   secteur Inner Join  activite    On activite.secteur = secteur.id Where  activite.id_startup =" . $id);
}

function getStartupById($id) {
    return requete_base("SELECT * FROM startup WHERE id=" . $id);
}

function getFondateurByIdStartup($id) {
    return requete_base("select * from personnes where id_startup=" . $id . " and nom !='' and (fonction=1 or fonction=2 or fonction=6 or fonction =7) and etat=1 order by personnes.id desc limit 1");
}

function getActiviteByIdStartup($id) {
    return requete_base("Select  last_activite.nom_activite,last_activite.nom_activite_en,  last_activite.id From  last_activite Inner Join  activite    On last_activite.id = activite.activite Where  activite.id_startup =" . $id);
}

function getTagsByIdStartup($id) {
    return requete_base("Select  activite.tags From  activite  Where  activite.id_startup id=" . $id . " ");
}

function getSousSecteurByIdStartup2($id) {
    return requete_base("Select  sous_secteur.nom_sous_secteur,sous_secteur.id,sous_secteur.nom_sous_secteur_en From   sous_secteur Inner Join  activite    On activite.sous_secteur = sous_secteur.id Where  activite.id_startup =" . $id);
}

function getSousSecteurByIdStartup($id) {
    return requete_base("Select  sous_secteur.id,sous_secteur.nom_sous_secteur,sous_secteur.nom_sous_secteur_en From  sous_secteur inner join activite on activite.sous_secteur=sous_secteur.id Where  activite.id_startup =" . $id);
}
function getStartupByIdSecteur($id) {
    return requete_base("Select  startup.related From   startup     Where  startup.id=" . $id);
}
function getSousActiviteByIdActivite($id) {
    return requete_base("Select  last_sous_activite.nom_sous_activite,last_sous_activite.nom_sous_activite_en From  last_sous_activite inner join activite on activite.sous_activite=last_sous_activite.id Where  activite.id_startup = " . $id);
}

function getStartupEffectif($id) {
    return requete_base("select * from startup_effectif where id_startup=" . $id . " order by annee desc limit 2");
}

function getStartupCapital($id) {
    return requete_base("select capital, year(dt) as annee from startup_capital where id_startup=" . $id . " order by dt desc limit 2");
}

function getStartupBenefice($id) {
    return requete_base("select * from startup_benefice where id_startup=" . $id . " order by annee desc limit 2");
}

function getStartupCA($id) {
    return requete_base("select * from startup_ca where id_startup=" . $id . " order by annee desc limit 2");
}

function getTotalLfByStartup($id) {
    return requete_base("select sum(montant) as somme from lf where rachat=0 and id_startup=" . $id);
}

function getTotalInvestisseurByStartup($id) {
    return requete_base("select de  from lf where rachat=0 and id_startup=" . $id);
}

function getLastDealByStartup($id) {
    return requete_base("select *  from lf where rachat=0 and id_startup=" . $id . " order by date_ajout desc limit 1");
}

function verif_has_lf($id) {
    return requete_base("select count(*) as nb from lf where (rachat=0) and  id_startup=" . $id);
}

function verif_has_lf_only($id) {
    return requete_base("select count(*) as nb from lf where rachat=0 and  id_startup=" . $id);
}

function getAllLFByStartup($id) {
    return requete_base("select *  from lf where rachat=0 and id_startup=" . $id . " order by date_ajout desc");
}

function getLastExpByPersonne($id) {
    return requete_base("Select  societe_personnes.societe, societe_personnes.de, societe_personnes.a,  societe_personnes.fonction,  societe_personnes.idpersonne From  societe_personnes Where  societe_personnes.idpersonne =" . $id . " order by societe_personnes.de desc");
}

function getListFondateurByIdStartup($id) {
    return requete_base("select * from personnes where id_startup=" . $id . " and nom !='' and etat=1 order by personnes.id limit 4");
}

function getLegalInformationStartup($id) {
    return requete_base("select * from startup_depot where id_startup=" . $id . " order by dt desc");
}

function getLegalInformationStartupl5($id) {
    return requete_base("select * from startup_depot where id_startup=" . $id . " order by dt desc limit 5");
}

function getNbFondateurByIdStartup($id) {
    return requete_base("select count(*) as nb from personnes where id_startup=" . $id . " and (fonction=1 or fonction=2 or fonction=6 or fonction =7) and etat=1 and nom !=''");
}
function getListCEOByIdStartup($id) {
    return requete_base("select * from personnes where id_startup=" . $id . " and nom !='' and (fonction=1 or fonction=2 or fonction=6 or fonction =7) and etat=1");
}