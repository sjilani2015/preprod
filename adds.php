<?php
include("include/db.php");
include("functions/functions.php");
include("config.php");
if (!isset($_SESSION['data_login'])) {
    header("location:" . URL . "/connexion");
    exit();
}


if (isset($_POST['startup'])) {
    $startup = addslashes($_POST['startup']);

    $juridique = addslashes($_POST['juridique']);
    $accroche = addslashes($_POST['accroche']);

    $siret = addslashes($_POST['siret']);
    if ($siret != '') {
        $siret1 = trim(str_replace(" ", "", $siret));
        $siret2 = substr($siret1, 0, 9);
        $siret_link = "http://www.verif.com/societe/" . $siret2;
    } else {
        $siret_link = "";
    }

    $email = addslashes($_POST['email']);
    $tel = addslashes($_POST['tel']);
    $creation = addslashes($_POST['creation']);
    if ($creation != '') {
        $tab_creation = explode("/", $creation);
        $date_creation = $tab_creation[2] . "-" . $tab_creation[1] . "-" . $tab_creation[0];
        $annee_creation = $tab_creation[2];
    } else {
        $date_creation = "";
        $annee_creation = "";
    }
    $website = addslashes($_POST['website']);
    $facebook = addslashes($_POST['facebook']);
    $twitter = addslashes($_POST['twitter']);
    $linkedin = addslashes($_POST['linkedin']);
    $youtube = addslashes($_POST['youtube']);

    $secteur_val = addslashes($_POST['list_sector']);
    $sous_secteur = addslashes($_POST['list_sous_sector']);
    $marche = addslashes($_POST['list_marche']);
    $sous_marche = addslashes($_POST['list_sous_marche']);

    $description = addslashes($_POST['description']);
    $tags = addslashes($_POST['tags']);
    $adresse = addslashes($_POST['adresse']);
    $cp = addslashes($_POST['postal_code']);
    $ville = addslashes($_POST['locality']);
    $lat = addslashes($_POST['lat']);
    $lng = addslashes($_POST['lng']);
    $region = addslashes($_POST['administrative_area_level_1']);

    // $target_path = "";
    // if ($_FILES['logo']['name'] != '') {
    //     $target_path = "logo/";
    //     $target_path = $target_path . uniqid() . basename($_FILES['logo']['name']);
    //     if (move_uploaded_file($_FILES['logo']['tmp_name'], $target_path)) {
    //         echo "";
    //     } else {
    //         echo "";
    //     }
    // }
    //////////////
    $target_path = "";

    if ($_FILES['logo']['name'] != '') {
        $target_path = "logo/";
        $favicon_path = "favicon/";
        /////
        $uid = uniqid();
        $tf = basename($_FILES['logo']['name']);

        $target_path = $target_path . $uid . str_replace(' ', '_', $startup) . '.' . pathinfo($tf, PATHINFO_EXTENSION);
        $favicon_path = $favicon_path . $uid . str_replace(' ', '_', $startup) . '.' . pathinfo($tf, PATHINFO_EXTENSION);
        // $chemin = "../logo/" .$uid.str_replace(' ','_',$startup).'.'.pathinfo($tf, PATHINFO_EXTENSION);

        if (move_uploaded_file($_FILES['logo']['tmp_name'], $target_path)) {
            
        } else {
            echo "";
        }
    }
    ///////////////



    mysqli_query($link, "insert into startup("
            . "nom,"
            . "societe,"
            . "url,"
            . "creation,"
            . "date_complete,"
            . "email,"
            . "tel,"
            . "siret,"
            . "adresse,"
            . "cp,"
            . "ville,"
            . "lat,"
            . "lng,"
            . "short_fr,"
            . "long_fr,"
            . "twitter,"
            . "youtube,"
            . "facebook,"
            . "linkedin,"
            . "logo,"
            . "favicon,"
            . "status,"
            . "date_add,"
            . "region,"
            . "verif_link) "
            . "values ( "
            . " '" . $startup . "',"
            . " '" . $juridique . "',"
            . " '" . $website . "',"
            . " '" . $annee_creation . "',"
            . " '" . $date_creation . "',"
            . " '" . $email . "',"
            . " '" . $tel . "',"
            . " '" . $siret . "',"
            . " '" . $adresse . "',"
            . " '" . $cp . "',"
            . " '" . $ville . "',"
            . " '" . $lat . "',"
            . " '" . $lng . "',"
            . " '" . $accroche . "',"
            . " '" . $description . "',"
            . " '" . $twitter . "',"
            . " '" . $youtube . "',"
            . " '" . $facebook . "',"
            . " '" . $linkedin . "',"
            . " '" . $target_path . "',"
            . " '" . $favicon_path . "',"
            . " '2',"
            . " '" . date("Y-m-d H:i:s") . "',"
            . " '" . $region . "',"
            . " '" . $siret_link . "')");
    $id_startup = mysqli_insert_id($link);

    mysqli_query($link, "insert into historique(dt,user,module,id_startup)values('" . date('Y-m-d H:i:s') . "','" . $_SESSION['data_login'] . "','Startup ajoutée du Front'," . $id_startup . ")");
    // user statup : source:1 ajout from add_startup_by_user
    // user statup : source:0 ajout from admin ( demande association )
    mysqli_query($link, "insert into user_startup("
            . "iduser,"
            . "id_startup,"
            . "dt,"
            . "etat,"
            . "source) "
            . "values("
            . " '" . $id_user . "',"
            . " '" . $id_startup . "' ,"
            . " '" . date('Y-m-d H:i:s') . "',"
            . " '1',"
            . " '1')");

    //mysql_query("insert into new_activite(id_startup,tags)values(" . $id_startup . ",'" . $tags . "')")or die(mysql_error());


    mysqli_query($link, "insert into activite ("
            . "id_startup,"
            . "secteur,"
            . "sous_secteur,"
            . "activite,"
            . "sous_activite,"
            . "tags) "
            . "values ( "
            . " '" . $id_startup . "',"
            . " '" . $secteur_val . "',"
            . " '" . $sous_secteur . "',"
            . " '" . $marche . "',"
            . " '" . $sous_marche . "',"
            . " '" . $tags . "' )");

    $prenoms_list = $_POST["prenom_founder"];
    $noms_list = $_POST["nom_founder"];
    $fonction_list = $_POST["fonction"];
    $linkedin_list = $_POST["linkedin_founder"];

    foreach ($noms_list as $key => $investitem) {
        $prenom_co = addslashes($prenoms_list[$key]);
        $nom_co = addslashes($noms_list[$key]);
        $fct_co = addslashes($fonction_list[$key]);
        $linkedin_co = addslashes($linkedin_list[$key]);

        if (($nom_co != '') || ($fct_co != '') || ($linkedin_co != '')) {
            mysqli_query($link, "INSERT INTO personnes ( "
                    . "id_startup,"
                    . "prenom,"
                    . "nom,"
                    . "fonction,"
                    . "linkedin) VALUES ( "
                    . " '" . $id_startup . "',"
                    . " '" . $prenom_co . "',"
                    . " '" . $nom_co . "',"
                    . " '" . $fct_co . "',"
                    . " '" . $linkedin_co . "');");
        }
    }









    /* ------------------------------------------------------
     * -----------------------DEBUT ENVOI MAIL------------
     * ---------------------------------------------------
     */



    $headers = "From: \"myfrenchstartup\"<contact@myfrenchstartup.com>\n";
    $headers .= "Reply-To: contact@myfrenchstartup.com\n";
    $headers .= "Content-Type: text/html; charset=\"utf-8\"";

    $message = '<html>
    <head>
        <title>MyfrenchStartup Add New Startup </title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>
    <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

        <table align="center" id="Table_01" width="600" height="394" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/mailing/images/myfs_01.jpg" width="600" height="81" alt=""></td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/mailing/images/myfs_02.jpg" width="600" height="30" alt=""></td>
            </tr>
            <tr>
                <td width="600" bgcolor="#ffffff">';

    $message .= 'Bonjour,<br /><br />Une nouvelle startup a été ajoutée a MyFrenchStartup';
    $message .= '<br />Le: ' . date('Y-m-d H:i:s');
    $message .= '<br />De la part de: ' . $_SESSION['data_login'];
    $message .= '<br />Nom de la startup: ' . $startup;

    $message .= '</td></tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/mailing/images/myfs_04.jpg" width="600" height="59" alt=""></td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/mailing/images/myfs_05.jpg" width="600" height="48" alt=""></td>
            </tr>
        </table>
    </body>
</html>';

    require 'phpmailer/PHPMailerAutoload.php';

    $mail = new PHPMailer();

    $mail->IsSMTP();
    $mail->Host = 'ssl0.ovh.net';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'info@myfrenchstartup.com';                 // SMTP username
    $mail->Password = '04Betterway';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                               // TCP port to connect to

    $mail->setFrom('info@myfrenchstartup.com');
    $mail->addAddress('contact@myfrenchstartup.com');     // Add a recipient
    $mail->addBCC('s.jilani@agencebetterway.com');

    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = "Nouvelle Startup inscrite MyFrenchStartup";

    $mail->Body = utf8_decode($message);

    if (!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        
    }

    /* ------------------------------------------------------
     * -----------------------FIN ENVOI MAIL------------
     * ---------------------------------------------------
     */
} else {

    echo "<script>window.location='" . url . "nos-offres'</script>";
    exit();
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
<?php include ('metaheaders.php'); ?>
        <title>Conditions générales de vente - <?= SITENAME; ?></title>
        <meta name="description" content="">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
<?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="edito">


                    <h2>Votre startup a été ajoutée</h2>


                    <p> Merci pour votre collaboration.<br>
                        Afin de garantir la fiabilité des données, toutes les startups ajoutées sont soumises à la validation d'un Data Manager.<br>Votre Startup sera validée d'ici 24/48 h.<br><br>Merci pour votre compréhension,<br>L'équipe MyFrenchStartup</div>
                </p>

            </div>
        </div>

<?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/animated.min.js"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>