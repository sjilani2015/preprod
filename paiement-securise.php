<?php
include("include/db.php");
include("functions/functions.php");
if (!isset($_SESSION['data_login'])) {
    header('Location: ' . url . 'connexion?utm_source=nos-offres');
    exit();
}
// Include configuration file  
require_once 'paiement-securise/config.php';

// Include the database connection file 
include_once 'paiement-securise/dbConnect.php';

// Fetch plans from the database 
$sqlQ = "SELECT * FROM plans";
$stmt = $db->prepare($sqlQ);
$stmt->execute();
$result = $stmt->get_result();
?>
<!DOCTYPE html>
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>Pricing</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    
    ================================================== -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="paiement-securise/css/style.css">
    <link rel="stylesheet" href="css/main-color.css" id="colors">
    <link rel="stylesheet" href="css/design_system.css" id="colors">
    <link rel="stylesheet" href="file.css" id="colors">

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <style>
        th{
            background: #F6F9FC;
            color: #7E84A3;
            font-size: 1.2rem;
            line-height: 16px;
            border: none !important;
        }
        td{

            font-size: 1.2rem;
            text-align: left;
            line-height: 1.6rem;
            font-family: 'Roboto';
            border: none !important;

        }
        .table-ditance{
            margin-top: 24px;
            margin-left: 8px;
        }
        .pricing-title{
            font-size: 2.6rem;
            line-height: 2.8rem;
            color: #C5D1DC;
            font-family: 'EuclidLight';
            text-align: center;
            margin-bottom: 1.6rem;

        }
        .pricing-h3{
            text-align: center;
            font-size: 1.6rem;
            line-height: 2.4rem;
            color:#2D3B48;
            margin-top: 0px;
            margin-bottom: 48px;
            font-family: 'Euclid';
        }
        .pricing-h1{
            text-align: center;
            font-family: 'EuclidBold';
            font-size:4.8rem;
            line-height: 4.8rem;
            margin-top: 40px;
            margin-bottom: 24px;
            color:#2D3B48;
        }
        .pricing-montant{
            text-align: center;
            font-family: 'EuclidBold';
            font-size: 6.4rem;

            color:#2D3B48;
        }
        .pricing-devise{
            font-family: 'EuclidLight';
        }
        .pricing-cart{
            font-family: 'EuclidBold';
            font-size: 1.2rem;
            line-height: 1.6rem;
            color: #425466;
            text-align: center;
        }
        .pricing-text{
            text-align: center;
            font-family: 'Roboto';
            font-size: 1.6rem;
            line-height: 2rem;
            color: #425466;
            margin-top: 28px;
        }
        .pricing-puce{
            margin-left: 5em;
            text-align: left;
            margin-top: 16px;
            margin-bottom: 24px;
            color: #425466;
            font-size: 1.2rem;
            font-family: 'Roboto';
            font-weight: 100;
            line-height: 24px;
            height: 144px;
        }
        .pricing-check{
            width: 1.2rem;
            margin-right: 8px;
        }
        .pricing-btn{

            background-color: #00CCFF;
            font-weight: 300;
            font-family: 'Roboto';
            border-radius: 5px;
            color: #FFFFFF;
            padding-left: 1.6rem;
            padding-right: 1.6rem;
            padding-top: 1.2rem;
            padding-bottom: 1.2rem;
            font-size: 1.2rem;
            line-height: 1.6rem;
        }
        .bloc-contact{
            padding: 24px;
        }
        .contact-text{
            text-align: center;
            color: #425466;
            font-size: 1.2rem;
            line-height: 1.6rem;
            font-family: 'Roboto';
        }
        .bloc-information{
            background: #EDF1F6;
            margin-top: 40px;
            margin-bottom: 72px;
        }
        .contact-text-span{
            font-family: 'EuclidBold';
            font-size: 1.2rem;
        }
        .contact-grand-question{
            text-align: center;
            padding-top: 24px;
            font-size: 3.2rem;
            color: #2D3B48;
            line-height: 2.4rem;
            font-family: 'EuclidBold';
            margin-bottom: 40px;
            input[type='text']{
                top: 283px;
                left: 300px;
                width: 255px;
                height: 40px;
                background: var(--arrière-plan) 0% 0% no-repeat padding-box;
                border: 1px solid var(--background-1);
                background: #F6F9FC 0% 0% no-repeat padding-box;
                border: 1px solid #D8E2ED;
                opacity: 1;
            }

        </style>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
    </head>

    <body style="background: #F6F9FC;
          font-family: 'Roboto';">

        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Header Container
            ================================================== -->
            <?php include('header.php'); ?>
            <div class="wrapper">

                <div class="clearfix"></div>

                <h1 class="pricing-h1">Premium</h1>

                <h3 class="pricing-h3">Une offre simple qui grandit en même temps que vous</h3>



                <div class="blob2-courbe" style="margin-bottom: 30px">
                        <div class="espace-bloc-interieur">
                            <div class="titre-overview" style="text-align: left;">Récapitualitif de la commande</div>
                        <div>
                            <div class="montant-overview">89 €</div>
                            <div class="montant-overview" style="font-size:11px">Par mois / Par utilisateur</div>
                            </div>
                            <div style="text-align: center; padding: 15px;font-weight: bold">Ou</div>
                        <div>
                            <div class="montant-overview">900 €</div>
                            <div class="montant-overview" style="font-size:11px">Par an / Par utilisateur</div>
                            </div>
                            <div class="pricing-text">Une offre simple qui grandit en même<br>temps que vous</div>
                            <div class="pricing-puce" style="margin-bottom: 12px;">
                            <div>
                                <div><span><img class="pricing-check" src="assets/images/check.svg"></span> <span class="tout_text_index">Dashboard personnalisé</span> </div>
                                <div><span><img class="pricing-check" src="assets/images/check.svg"></span> <span class="tout_text_index">Recherches illimitées</span> </div>
                                <div><span><img class="pricing-check" src="assets/images/check.svg"></span> <span class="tout_text_index">100 résultats par recherche</span></div>
                                <div><span><img class="pricing-check" src="assets/images/check.svg"></span> <span class="tout_text_index">Alertes personnalisées</span></div>
                                <div><span><img class="pricing-check" src="assets/images/check.svg"></span> <span class="tout_text_index">Accès dealflow</span></div>
                                <div><span><img class="pricing-check" src="assets/images/check.svg"></span> <span class="tout_text_index">Abonnement individuel</span> </div>
                            </div>
                        </div>
                            <div style=" margin-top: 38px;">Pour toute information, merci d'envoyer un email à <a href="mailto:contact@myfrenchstartup.com">contact@myfrenchstartup.com</a></div>
                    </div>
                    <div class="espace-bloc-interieur">
                        <div class="titre-overview" style="text-align: left;">Paiement sécurisé via Stripe</div>
                        <div class="">
                            <div class="panel-heading">

                                <!-- Plan Info -->
                                <div>
                                    <b>Sélectionnez un abonnement mensuel ou annuel</b>
                                    <select id="subscr_plan">
                                        <?php
                                        if ($result->num_rows > 0) {
                                            while ($row = $result->fetch_assoc()) {
                                                ?>
                                                <option value="<?php echo $row['id']; ?>"><?php echo $row['name'] . ' [' . $row['price'] . '€/' . $row['interval'] . ']'; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="panel-body">
                                <!-- Display status message -->
                                <div id="paymentResponse" class="hidden"></div>

                                <!-- Display a subscription form -->
                                <form id="subscrFrm">
                                    <div class="form-group">
                                        <label>Titulaire de la carte</label>
                                        <input type="text" id="name" class="form-control" placeholder="Titulaire de la carte" required="" autofocus="">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" id="email" class="form-control" placeholder="Email" required="">
                                    </div>

                                    <div class="form-group">
                                        <label>Informations de la carte</label>
                                        <div id="card-element">
                                            <!-- Stripe.js will create card input elements here -->
                                        </div>
                                    </div>

                                    <!-- Form submit button -->
                                    <button id="submitBtn" class="btn pricing-btn" style="margin-top: 24px">
                                            <div class="spinner hidden" id="spinner"></div>
                                            <span id="buttonText">Procéder au paiement (89 €)</span>
                                        </button>
                                    </form>

                                    <!-- Display processing notification -->
                                    <div id="frmProcess" class="hidden">
                                        <span class="ring"></span> Processing...
                                    </div>
                                </div>
                            </div>
                            <div style="text-align: center;
                            margin-top: 30px;"><img src="images/logo-stripe-secure.png" alt="" style="width: 170px;"></div>
                    </div>
                </div>



                <div class="bloc-chart">




                </div>









                <div id="backtotop"><a href="#"></a></div>
                    <?php include('footer1.php'); ?>
            </div>

        </div>
        <!-- Wrapper / End -->












        <script src="js/scripts.js"></script>
        <script src="jquery.min.js"></script>

        <script src="bootstrap.min.js"></script>
        <script src="jquery.knob.js"></script>
        <script>
            $(document).ready(function () {
                $("#search-box").keyup(function () {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo url ?>readCountry.php",
                        data: 'keyword=' + $(this).val(),
                        beforeSend: function () {
                            $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function (data) {
                            $("#suggesstion-box").show();
                            $("#suggesstion-box").html(data);
                            $("#search-box").css("background", "#FFF");
                        }
                    });
                });


            });

            function selectCountry(val) {
                const words = val.split('/');
                $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }
            function selectRegion(val) {
                const words = val.split('/');
                $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }




        </script>

        <script src="paiement-securise/js/checkout.js" STRIPE_PUBLISHABLE_KEY="<?php echo STRIPE_PUBLISHABLE_KEY; ?>" defer></script>
        <?php
        mysqli_query($link, "insert into acces_paiement(user,dt)values('" . $_SESSION['data_login'] . "','" . date('Y-m-d H:i:s') . "')");
        ?>
    </body>
</html>