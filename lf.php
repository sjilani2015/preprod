<?php
include("include/db.php");
include("functions/functions.php");
include('config.php');

if (isset($_SESSION['data_login'])) {
    $users = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));
    $ma_liste_colonne = explode(";", $users['colonne']);
}

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include('metaheaders.php'); ?>
        <title>Recherche de startups - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
        <script>
            (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({'gtm.start':
                    new Date().getTime(), event: 'gtm.js'});
            var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
    </head>

    <body class="preload page">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MR2VSZB"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <div style="text-align: center;padding-top: 30px;"><a href="https://www.myfrenchstartup.com/etude-dealflow" target="_blank"><img src="martech.png" alt="" /></a></div>

            <?php
            if (!isset($_SESSION['data_login'])) {
                include('layout/header-simple.php');
            } else {
                include('layout/header-connected.php');
            }
            ?>
            <?php include('recherche_lf/filters.php'); ?>

            <div class="page-content" id="page-content">
                <div class="container-fluid">
                    <div id="mon_bloc_tab">
                        <div id="loaderDiv" style="display: none; text-align: center; width: 100%;"><img src="loader.gif" /></div>
                    </div>

                    <div class="search-actions">
                        <!--<a href="" class="btn btn-sm btn-primary btn-alarm btn-searchAlert">Créer une alerte</a>-->
                        <!-- <a href="" class="btn btn-sm btn-saveSearch btn-primary btn-bookmark">Enregistrer la recherche</a>-->
                    </div>
                    <div id="paginateTable"></div>
                </div>
            </div>
        </div>

        <?php include('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>jquery.dataTables.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>datatables.bootstrap.min.js?<?= time(); ?>"></script>

        <script>
            function ajouter_etude(count, id) {
            $("#" + count).show();
            }

            function enlever(count) {
            $("#" + count).hide();
            }

            function alerter(id, nom) {

            window.location = "<?php echo URL ?>/etude-secteur/" + id + "/" + nom;
            }

            function similar_sup(id) {
            window.location = "<?php echo URL ?>/recherche-startups/similar-startup/" + id;
            }

            function afficher_par_tag(tags) {
            window.location = "<?php echo URL ?>/recherche-startups/tags/" + tags;
            }

            function afficher_par_secteur(id, nom) {
            window.location = "<?php echo URL ?>/recherche-startups/secteur-activite/" + id + "/" + nom;
            }

            function afficher_par_ssecteur(id, nom) {
            window.location = "<?php echo URL ?>/recherche-startups/sous-secteur-activite/" + id + "/" + nom;
            }

            function afficher_cible(nom) {
            window.location = "<?php echo URL ?>/recherche-startups/cible/" + nom;
            }

            function afficher_par_ville(id, nom) {
            window.location = "<?php echo URL ?>/recherche-startups/location/" + id + "/" + nom;
            }

            $(document).ready(function () {

<?php if (isset($_GET['similar'])) { ?>
                similar_sups(<?php echo $_GET['similar']; ?>)
<?php } else if (isset($_GET['tags'])) {
    ?>
                import_tags('<?php echo $_GET['tags']; ?>');
    <?php
} else if (isset($_GET['secteur'])) {
    ?>
                import_secteur('<?php echo $_GET['secteur']; ?>');
    <?php
} else if (isset($_GET['ssecteur'])) {
    ?>
                import_ssecteur('<?php echo $_GET['ssecteur']; ?>');
    <?php
} else if (isset($_GET['cible'])) {
    ?>
                import_cible('<?php echo $_GET['cible']; ?>');
    <?php
} else if (isset($_GET['ville'])) {
    ?>
                import_ville('<?php echo $_GET['region']; ?>');
    <?php
} else if (isset($_GET['list'])) {
    ?>
                import_list('<?php echo $_GET['list']; ?>');
    <?php
} else if (isset($_GET['next'])) {
    ?>
                next400('<?php echo $_GET['next']; ?>');
    <?php
} else {
    ?>
                import_table();
    <?php
}
?>
            });
            function get_sous_secteur(id) {

            if (document.getElementById("secteur_" + id).checked == true) {
            document.getElementById('secteur_' + id).value = 0;
            } else {
            document.getElementById('secteur_' + id).value = 1;
            }
            if (document.getElementById('secteur_' + id).value == 1) {
            document.getElementById("bloc_sous_ssecteur_" + id).style.display = "grid";
            document.getElementById("bloc_sous_secteur").style.display = "contents";
            } else {
            document.getElementById("bloc_sous_ssecteur_" + id).style.display = "none";
            }

            }

            function getsousnaf(el) {
            var id = el.value;
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/getlistsousnaf.php?id=" + id,
                    success: function (o) {
                    document.getElementById("bloc_sous_naf").innerHTML = o;
                    }

            });
            }

            function affiche() {
            if (document.getElementById("check-a7").checked == true) {
            document.getElementById('check-a7').value = 1;
            } else {
            document.getElementById('check-a7').value = 0;
            }

            if (document.getElementById('check-a7').value == 1) {
            document.getElementById("blc").style.display = "contents";
            } else {
            document.getElementById("blc").style.display = "none";
            }


            }
        </script>
        <script>


            function rechercher_lf() {

            var ville = document.getElementById("ville").value;
            var cp = document.getElementById("cp").value;
            var region = document.getElementById("region").value;
            var creation_deb = document.getElementById("creation_deb").value;
            var creation_fin = document.getElementById("creation_fin").value;
            var siret = document.getElementById("siret").value;
            var naf = document.getElementById("naf").value;
            var sous_naf = document.getElementById("sous_naf").value;
            var prenom = document.getElementById("prenom_e").value;
            var nom = document.getElementById("nom_e").value;
            var formations = document.getElementById("autocomplete-ajax").value;
            var experience = document.getElementById("autocomplete-ajax1").value;
            var skills = document.getElementById("autocomplete-ajax2").value;
            var doc_legal = document.getElementById("doc_legal").value;
            var parution = document.getElementById("parution").value;
            var effectif_min = document.getElementById("effectif_min").value;
            var effectif_max = document.getElementById("effectif_max").value;
            var min_cs = document.getElementById("min_cs").value;
            var max_cs = document.getElementById("max_cs").value;
            var resultat_min = document.getElementById("resultat_min").value;
            var resultat_max = document.getElementById("resultat_max").value;
            var date_l1 = document.getElementById("date_l1").value;
            var date_l2 = document.getElementById("date_l2").value;
            var debut_creation = document.getElementById("debut_creation").value;
            var fin_creation = document.getElementById("fin_creation").value;
            if ($('#cilble_B2B').is(':checked')) {
            cilble_B2B = "B2B";
            } else {
            cilble_B2B = '';
            }
            if ($('#cible_B2C').is(':checked')) {
            cilble_B2C = "B2C";
            } else {
            cilble_B2C = '';
            }
            if ($('#cible_C2C').is(':checked')) {
            cilble_C2C = "C2C";
            } else {
            cilble_C2C = '';
            }

            if ($('#forme_EURL').is(':checked')) {
            forme_EURL = "EURL";
            } else {
            forme_EURL = '';
            }
            if ($('#forme_SAS').is(':checked')) {
            forme_SAS = "SAS";
            } else {
            forme_SAS = '';
            }
            if ($('#forme_SARL').is(':checked')) {
            forme_SARL = "SARL";
            } else {
            forme_SARL = '';
            }
            if ($('#forme_SARL').is(':checked')) {
            forme_SA = "SA";
            } else {
            forme_SA = '';
            }


            if ($('#rang1').is(':checked')) {
            rang1 = "1";
            } else {
            rang1 = '';
            }
            if ($('#rang2').is(':checked')) {
            rang2 = "2";
            } else {
            rang2 = '';
            }
            if ($('#rang3').is(':checked')) {
            rang3 = "3";
            } else {
            rang3 = '';
            }
            if ($('#rang4').is(':checked')) {
            rang4 = "4";
            } else {
            rang4 = '';
            }
            if ($('#rang5').is(':checked')) {
            rang5 = "5";
            } else {
            rang5 = '';
            }
            if ($('#check-ca1').is(':checked')) {
            chiff_a1 = "1";
            } else {
            chiff_a1 = '';
            }

            if ($('#check-ca2').is(':checked')) {
            chiff_a2 = "2";
            } else {
            chiff_a2 = '';
            }
            if ($('#check-ca3').is(':checked')) {
            chiff_a3 = "3";
            } else {
            chiff_a3 = '';
            }
            if ($('#check-a').is(':checked')) {
            nbr_lf1 = "1";
            } else {
            nbr_lf1 = '';
            }
            if ($('#check-b').is(':checked')) {
            nbr_lf2 = "2";
            } else {
            nbr_lf2 = '';
            }
            if ($('#check-c').is(':checked')) {
            nbr_lf3 = "3";
            } else {
            nbr_lf3 = '';
            }
            if ($('#check-d').is(':checked')) {
            nbr_lf4 = "4";
            } else {
            nbr_lf4 = '';
            }
            if ($('#check-e').is(':checked')) {
            nbr_lf5 = "5";
            } else {
            nbr_lf5 = '';
            }
            if ($('#check-f').is(':checked')) {
            nbr_lf6 = "6";
            } else {
            nbr_lf6 = '';
            }
            if ($('#check_lf1').is(':checked')) {
            montant_l1 = "1";
            } else {
            montant_l1 = '';
            }
            if ($('#check_lf2').is(':checked')) {
            montant_l2 = "2";
            } else {
            montant_l2 = '';
            }
            if ($('#check_lf3').is(':checked')) {
            montant_l3 = "3";
            } else {
            montant_l3 = '';
            }
            if ($('#check_lf4').is(':checked')) {
            montant_l4 = "4";
            } else {
            montant_l4 = '';
            }


            if ($('#rachat').is(':checked')) {
            rachat = "1";
            } else {
            rachat = '';
            }
            if ($('#ipo').is(':checked')) {
            ipo = "2";
            } else {
            ipo = '';
            }
            if ($('#depot_bilan').is(':checked')) {
            depot_bilan = "1";
            } else {
            depot_bilan = '';
            }






<?php
$list_secteurs1 = getListSecteurTotal();
if (!empty($list_secteurs1)) {
    foreach ($list_secteurs1 as $secteurses) {
        ?>
                    secteur_<?php echo $secteurses['id'] ?> = '';
        <?php
    }
}
?>
<?php
$list_secteurs2 = getListSecteurTotal();
if (!empty($list_secteurs2)) {
    foreach ($list_secteurs2 as $secteurse) {
        ?>
                    if ($('#secteur_<?php echo $secteurse['id'] ?>').is(':checked')) {
                    secteur_<?php echo $secteurse['id'] ?> = <?php echo $secteurse['id'] ?>;
                    } else {
                    secteur_<?php echo $secteurse['id'] ?> = '';
                    }
        <?php
    }
}
?>
<?php
$list_activite1 = getListActiviteTotal();
if (!empty($list_activite1)) {
    foreach ($list_activite1 as $activitees) {
        ?>
                    activite_<?php echo $activitees['id'] ?> = '';
        <?php
    }
}
?>
<?php
$list_activite2 = getListActiviteTotal();
if (!empty($list_activite2)) {
    foreach ($list_activite2 as $activitee) {
        ?>
                    if ($('#activite_<?php echo $activitee['id'] ?>').is(':checked')) {
                    activite_<?php echo $activitee['id'] ?> = <?php echo $activitee['id'] ?>;
                    } else {
                    activite_<?php echo $activitee['id'] ?> = '';
                    }
        <?php
    }
}

$list_ssecteurs1 = getListSousSecteurTotal();
if (!empty($list_ssecteurs1)) {
    foreach ($list_ssecteurs1 as $ssecteurses) {
        ?>
                    sous_secteur_<?php echo $ssecteurses['id'] ?> = '';
        <?php
    }
}

$list_ssecteurs2 = getListSousSecteurTotal();
if (!empty($list_ssecteurs2)) {
    foreach ($list_ssecteurs2 as $ssecteurse) {
        ?>
                    if ($('#sous_secteur_<?php echo $ssecteurse['id'] ?>').is(':checked')) {
                    sous_secteur_<?php echo $ssecteurse['id'] ?> = <?php echo $ssecteurse['id'] ?>;
                    } else {
                    sous_secteur_<?php echo $ssecteurse['id'] ?> = '';
                    }
        <?php
    }
}
?>
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/find_lf.php",
                    data: {

                    ville: ville,
                            cp: cp,
                            region: region,
                            creation_deb: creation_deb,
                            creation_fin: creation_fin,
                            siret: siret,
                            cilble_B2B: cilble_B2B,
                            cilble_B2C: cilble_B2C,
                            cilble_C2C: cilble_C2C,
                            forme_EURL: forme_EURL,
                            forme_SAS: forme_SAS,
                            forme_SA: forme_SA,
                            forme_SARL: forme_SARL,
                            rang1: rang1,
                            rang2: rang2,
                            rang3: rang3,
                            rang4: rang4,
                            rang5: rang5,
                            naf: naf,
                            sous_naf: sous_naf,
                            prenom: prenom,
                            nom: nom,
                            formations: formations,
                            doc_legal: doc_legal,
                            parution: parution,
                            effectif_min: effectif_min,
                            effectif_max: effectif_max,
                            date_l1: date_l1,
                            date_l2: date_l2,
                            nbr_lf1: nbr_lf1,
                            nbr_lf2: nbr_lf2,
                            nbr_lf3: nbr_lf3,
                            nbr_lf4: nbr_lf4,
                            nbr_lf5: nbr_lf5,
                            nbr_lf6: nbr_lf6,
                            montant_l1: montant_l1,
                            montant_l2: montant_l2,
                            montant_l3: montant_l3,
                            montant_l4: montant_l4,
                            chiff_a1: chiff_a1,
                            chiff_a2: chiff_a2,
                            chiff_a3: chiff_a3,
                            min_cs: min_cs,
                            max_cs: max_cs,
                            resultat_min: resultat_min,
                            resultat_max: resultat_max,
                            experience: experience,
                            skills: skills,
                            rachat: rachat,
                            ipo: ipo,
                            depot_bilan: depot_bilan,
                            debut_creation: debut_creation,
                            fin_creation: fin_creation,
<?php
$list_secteurs2 = getListSecteurTotal();
if (!empty($list_secteurs2)) {
    foreach ($list_secteurs2 as $secteurse) {
        ?>
                            secteur_<?php echo $secteurse['id'] ?>: secteur_<?php echo $secteurse['id'] ?>,
        <?php
    }
}
?>
<?php
$list_activte2 = getListActiviteTotal();
if (!empty($list_activte2)) {
    foreach ($list_activte2 as $activiitee) {
        ?>
                            activite_<?php echo $activiitee['id'] ?>: activite_<?php echo $activiitee['id'] ?>,
        <?php
    }
}
?>
<?php
$list_ssecteurs22 = getListSousSecteurTotal();
if (!empty($list_ssecteurs22)) {
    foreach ($list_ssecteurs22 as $sssecteurse) {
        ?>
                            sous_secteur_<?php echo $sssecteurse['id'] ?>: sous_secteur_<?php echo $sssecteurse['id'] ?>,
        <?php
    }
}
?>

                    },
                    beforeSend: function () {
                    document.getElementById("mon_bloc_tab").innerHTML = '<div id="loaderDiv" style="display: none; text-align: center; width: 100%;">' +
                            '<img src="<?php echo URL ?>/loader.gif" /></div>';
                    jQuery("#loaderDiv").show();
                    },
                    success: function (o) {

                    document.getElementById("mon_bloc_tab").innerHTML = o;
                    var table = jQuery('#mine').DataTable({
                    "searching": false,
                            "nextPrev": false,
                            "bLengthChange": false,
                            "bInfo": false,
                            "bPaginate": true,
                            "autoWidth": false,
                            //"fixedColumns": false,
                            initComplete: (settings, json) => {
                    $('#paginateTable').empty();
                    $('#mine_paginate').appendTo('#paginateTable');
                    },
                            "language": {
                            "paginate": {
                            "previous": "Précédent",
                                    "next": "Suivant"
                            }
                            }

                    });
                    openSearchFilters();
<?php
if (isset($_SESSION['data_login'])) {
    $colonnes = explode(",", $users['colonne']);
    $nb_colonne = count($colonnes);
    for ($u = 0; $u < $nb_colonne; $u++) {
        if ($colonnes[$u] != '') {
            ?>
                                table.column(<?php echo $colonnes[$u]; ?>).visible(false);
            <?php
        }
    }
} else {

    for ($p = 7; $p <= 18; $p++) {
        ?>
                            table.column(<?php echo $p; ?>).visible(false);
        <?php
    }
}
?>
                    //table.column(0).visible(true);
                    $('div.toggle-vis').on('click', function (e) {
                    e.preventDefault();
                    // Get the column API object
                    var column = table.column($(this).attr('data-columnId'));
                    // Toggle the visibility
                    column.visible(!column.visible());
                    if ($(this).hasClass('toggle-selected')) {
                    $(this).addClass('toggle-selected');
                    } else {
                    $(this).removeClass("toggle-selected");
                    }
                    });
                    }
            });
            closeModal();
            }

            function import_table() {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/default_lf.php",
                    beforeSend: function () {
                    jQuery("#loaderDiv").show();
                    },
                    success: function (o) {
                    document.getElementById("mon_bloc_tab").innerHTML = o;
                    var table = jQuery('#mine').DataTable({
                    "searching": false,
                            "nextPrev": false,
                            "bLengthChange": false,
                            "bInfo": false,
                            "bPaginate": true,
                            "autoWidth": false,
                            //"fixedColumns": false,

                            initComplete: (settings, json) => {
                    $('#paginateTable').empty();
                    $('#mine_paginate').appendTo('#paginateTable');
                    },
                            "language": {
                            "paginate": {
                            "previous": "Précédent",
                                    "next": "Suivant"
                            }
                            }
                    });
                    openSearchFilters();
<?php
if (isset($_SESSION['data_login'])) {
    $colonnes = explode(",", $users['colonne']);
    $nb_colonne = count($colonnes);
    for ($u = 0; $u < $nb_colonne; $u++) {
        if ($colonnes[$u] != '') {
            ?>
                                table.column(<?php echo $colonnes[$u]; ?>).visible(false);
            <?php
        }
    }
} else {

    for ($p = 7; $p <= 18; $p++) {
        ?>
                            table.column(<?php echo $p; ?>).visible(false);
        <?php
    }
}
?>
                    //table.column(0).visible(true);
                    $('div.toggle-vis').on('click', function (e) {
                    e.preventDefault();
                    // Get the column API object
                    var column = table.column($(this).attr('data-columnId'));
                    // Toggle the visibility
                    column.visible(!column.visible());
                    if ($(this).hasClass('toggle-selected')) {
                    $(this).removeClass("toggle-selected");
                    } else {
                    $(this).addClass('toggle-selected');
                    }
                    });
                    }
            });
            }

            function next400(id) {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/liste40.php",
                    beforeSend: function () {
                    document.getElementById("mon_bloc_tab").innerHTML = '<div id="loaderDiv" style="display: none; text-align: center; width: 100%;">' +
                            '<img src="<?php echo URL ?>/loader.gif" /></div>';
                    jQuery("#loaderDiv").show();
                    },
                    success: function (o) {
                    document.getElementById("mon_bloc_tab").innerHTML = o;
                    var table = jQuery('#mine').DataTable({
                    "searching": false,
                            "nextPrev": false,
                            "bLengthChange": false,
                            "bInfo": false,
                            "bPaginate": true,
                            "autoWidth": false,
                            //"fixedColumns": false,
                            initComplete: (settings, json) => {
                    $('#paginateTable').empty();
                    $('#mine_paginate').appendTo('#paginateTable');
                    },
                            "language": {
                            "paginate": {
                            "previous": "Précédent",
                                    "next": "Suivant"
                            }
                            }
                    });
                    openSearchFilters();
<?php
if (isset($_SESSION['data_login'])) {
    $colonnes = explode(",", $users['colonne']);
    $nb_colonne = count($colonnes);
    for ($u = 0; $u < $nb_colonne; $u++) {
        if ($colonnes[$u] != '') {
            ?>
                                table.column(<?php echo $colonnes[$u]; ?>).visible(false);
            <?php
        }
    }
} else {

    for ($p = 7; $p <= 18; $p++) {
        ?>
                            table.column(<?php echo $p; ?>).visible(false);
        <?php
    }
}
?>
                    //table.column(0).visible(true);
                    $('div.toggle-vis').on('click', function (e) {
                    e.preventDefault();
                    // Get the column API object
                    var column = table.column($(this).attr('data-columnId'));
                    // Toggle the visibility
                    column.visible(!column.visible());
                    if ($(this).hasClass('toggle-selected')) {
                    $(this).addClass('toggle-selected');
                    } else {
                    $(this).removeClass("toggle-selected");
                    }
                    });
                    }
            });
            closeModal();
            }

            function ft120() {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/ft120.php",
                    beforeSend: function () {
                    document.getElementById("mon_bloc_tab").innerHTML = '<div id="loaderDiv" style="display: none; text-align: center; width: 100%;">' +
                            '<img src="<?php echo URL ?>/loader.gif" /></div>';
                    jQuery("#loaderDiv").show();
                    },
                    success: function (o) {

                    document.getElementById("mon_bloc_tab").innerHTML = o;
                    var table = jQuery('#mine').DataTable({
                    "searching": false,
                            "nextPrev": false,
                            "bLengthChange": false,
                            "bInfo": false,
                            "bPaginate": true,
                            "autoWidth": false,
                            //"fixedColumns": false,
                            initComplete: (settings, json) => {
                    $('#paginateTable').empty();
                    $('#mine_paginate').appendTo('#paginateTable');
                    },
                            "language": {
                            "paginate": {
                            "previous": "Précédent",
                                    "next": "Suivant"
                            }
                            }

                    });
                    openSearchFilters();
<?php
if (isset($_SESSION['data_login'])) {
    $colonnes = explode(",", $users['colonne']);
    $nb_colonne = count($colonnes);
    for ($u = 0; $u < $nb_colonne; $u++) {
        if ($colonnes[$u] != '') {
            ?>
                                table.column(<?php echo $colonnes[$u]; ?>).visible(false);
            <?php
        }
    }
} else {

    for ($p = 7; $p <= 18; $p++) {
        ?>
                            table.column(<?php echo $p; ?>).visible(false);
        <?php
    }
}
?>
                    //table.column(0).visible(true);
                    $('div.toggle-vis').on('click', function (e) {
                    e.preventDefault();
                    // Get the column API object
                    var column = table.column($(this).attr('data-columnId'));
                    // Toggle the visibility
                    column.visible(!column.visible());
                    if ($(this).hasClass('toggle-selected')) {
                    $(this).addClass('toggle-selected');
                    } else {
                    $(this).removeClass("toggle-selected");
                    }
                    });
                    }
            });
            closeModal();
            }

            function licorne() {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/licorne.php",
                    beforeSend: function () {
                    document.getElementById("mon_bloc_tab").innerHTML = '<div id="loaderDiv" style="display: none; text-align: center; width: 100%;">' +
                            '<img src="<?php echo URL ?>/loader.gif" /></div>';
                    jQuery("#loaderDiv").show();
                    },
                    success: function (o) {
                    document.getElementById("mon_bloc_tab").innerHTML = o;
                    var table = jQuery('#mine').DataTable({
                    "searching": false,
                            "nextPrev": false,
                            "bLengthChange": false,
                            "bInfo": false,
                            "bPaginate": true,
                            "autoWidth": false,
                            //"fixedColumns": false,
                            initComplete: (settings, json) => {
                    $('#paginateTable').empty();
                    $('#mine_paginate').appendTo('#paginateTable');
                    },
                            "language": {
                            "paginate": {
                            "previous": "Précédent",
                                    "next": "Suivant"
                            }
                            }

                    });
                    openSearchFilters();
<?php
if (isset($_SESSION['data_login'])) {
    $colonnes = explode(",", $users['colonne']);
    $nb_colonne = count($colonnes);
    for ($u = 0; $u < $nb_colonne; $u++) {
        if ($colonnes[$u] != '') {
            ?>
                                table.column(<?php echo $colonnes[$u]; ?>).visible(false);
            <?php
        }
    }
} else {

    for ($p = 7; $p <= 18; $p++) {
        ?>
                            table.column(<?php echo $p; ?>).visible(false);
        <?php
    }
}
?>
                    //table.column(0).visible(true);
                    $('div.toggle-vis').on('click', function (e) {
                    e.preventDefault();
                    // Get the column API object
                    var column = table.column($(this).attr('data-columnId'));
                    // Toggle the visibility
                    column.visible(!column.visible());
                    if ($(this).hasClass('toggle-selected')) {
                    $(this).addClass('toggle-selected');
                    } else {
                    $(this).removeClass("toggle-selected");
                    }
                    });
                    }
            });
            closeModal();
            }

            function import_secteur(id) {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/secteurs.php?secteur=" + id,
                    beforeSend: function () {
                    document.getElementById("mon_bloc_tab").innerHTML = '<div id="loaderDiv" style="display: none; text-align: center; width: 100%;">' +
                            '<img src="<?php echo URL ?>/loader.gif" /></div>';
                    jQuery("#loaderDiv").show();
                    },
                    success: function (o) {

                    document.getElementById("mon_bloc_tab").innerHTML = o;
                    var table = jQuery('#mine').DataTable({
                    "searching": false,
                            "nextPrev": false,
                            "bLengthChange": false,
                            "bInfo": false,
                            "bPaginate": true,
                            "autoWidth": false,
                            ////"fixedColumns": false,

                    });
                    openSearchFilters();
<?php
if (isset($_SESSION['data_login'])) {
    $colonnes = explode(",", $users['colonne']);
    $nb_colonne = count($colonnes);
    for ($u = 0; $u < $nb_colonne; $u++) {
        if ($colonnes[$u] != '') {
            ?>
                                table.column(<?php echo $colonnes[$u]; ?>).visible(false);
            <?php
        }
    }
} else {

    for ($p = 7; $p <= 18; $p++) {
        ?>
                            table.column(<?php echo $p; ?>).visible(false);
        <?php
    }
}
?>
                    //table.column(0).visible(true);
                    $('div.toggle-vis').on('click', function (e) {
                    e.preventDefault();
                    // Get the column API object
                    var column = table.column($(this).attr('data-columnId'));
                    // Toggle the visibility
                    column.visible(!column.visible());
                    if ($(this).hasClass('toggle-selected')) {
                    $(this).removeClass("toggle-selected");
                    } else {
                    $(this).addClass('toggle-selected');
                    }
                    });
                    }
            });
            }

            function import_ssecteur(id) {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/ssecteur.php?ssecteur=" + id,
                    beforeSend: function () {
                    document.getElementById("mon_bloc_tab").innerHTML = '<div id="loaderDiv" style="display: none; text-align: center; width: 100%;">' +
                            '<img src="<?php echo URL ?>/loader.gif" /></div>';
                    jQuery("#loaderDiv").show();
                    },
                    success: function (o) {

                    document.getElementById("mon_bloc_tab").innerHTML = o;
                    var table = jQuery('#mine').DataTable({
                    "searching": false,
                            "nextPrev": false,
                            "bLengthChange": false,
                            "bInfo": false,
                            "bPaginate": true,
                            "autoWidth": false,
                            //"fixedColumns": false,
                            initComplete: (settings, json) => {
                    $('#paginateTable').empty();
                    $('#mine_paginate').appendTo('#paginateTable');
                    },
                            "language": {
                            "paginate": {
                            "previous": "Précédent",
                                    "next": "Suivant"
                            }
                            }

                    });
                    openSearchFilters();
<?php
if (isset($_SESSION['data_login'])) {
    $colonnes = explode(",", $users['colonne']);
    $nb_colonne = count($colonnes);
    for ($u = 0; $u < $nb_colonne; $u++) {
        if ($colonnes[$u] != '') {
            ?>
                                table.column(<?php echo $colonnes[$u]; ?>).visible(false);
            <?php
        }
    }
} else {

    for ($p = 7; $p <= 18; $p++) {
        ?>
                            table.column(<?php echo $p; ?>).visible(false);
        <?php
    }
}
?>
                    //table.column(0).visible(true);
                    $('div.toggle-vis').on('click', function (e) {
                    e.preventDefault();
                    // Get the column API object
                    var column = table.column($(this).attr('data-columnId'));
                    // Toggle the visibility
                    column.visible(!column.visible());
                    if ($(this).hasClass('toggle-selected')) {
                    $(this).removeClass("toggle-selected");
                    } else {
                    $(this).addClass('toggle-selected');
                    }
                    });
                    }
            });
            }

            function import_ville(id) {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/ville.php?ville=" + id,
                    beforeSend: function () {
                    document.getElementById("mon_bloc_tab").innerHTML = '<div id="loaderDiv" style="display: none; text-align: center; width: 100%;">' +
                            '<img src="<?php echo URL ?>/loader.gif" /></div>';
                    jQuery("#loaderDiv").show();
                    },
                    success: function (o) {
                    document.getElementById("mon_bloc_tab").innerHTML = o;
                    var table = jQuery('#mine').DataTable({
                    "searching": false,
                            "nextPrev": false,
                            "bLengthChange": false,
                            "bInfo": false,
                            "bPaginate": true,
                            "autoWidth": false,
                            //"fixedColumns": false,
                            initComplete: (settings, json) => {
                    $('#paginateTable').empty();
                    $('#mine_paginate').appendTo('#paginateTable');
                    },
                            "language": {
                            "paginate": {
                            "previous": "Précédent",
                                    "next": "Suivant"
                            }
                            }

                    });
                    openSearchFilters();
<?php
if (isset($_SESSION['data_login'])) {
    $colonnes = explode(",", $users['colonne']);
    $nb_colonne = count($colonnes);
    for ($u = 0; $u < $nb_colonne; $u++) {
        if ($colonnes[$u] != '') {
            ?>
                                table.column(<?php echo $colonnes[$u]; ?>).visible(false);
            <?php
        }
    }
} else {

    for ($p = 7; $p <= 18; $p++) {
        ?>
                            table.column(<?php echo $p; ?>).visible(false);
        <?php
    }
}
?>
                    //table.column(0).visible(true);
                    $('div.toggle-vis').on('click', function (e) {
                    e.preventDefault();
                    // Get the column API object
                    var column = table.column($(this).attr('data-columnId'));
                    // Toggle the visibility
                    column.visible(!column.visible());
                    if ($(this).hasClass('toggle-selected')) {
                    $(this).removeClass("toggle-selected");
                    } else {
                    $(this).addClass('toggle-selected');
                    }
                    });
                    }
            });
            }


            function import_cible(id) {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/cible.php?cible=" + id,
                    beforeSend: function () {
                    document.getElementById("mon_bloc_tab").innerHTML = '<div id="loaderDiv" style="display: none; text-align: center; width: 100%;">' +
                            '<img src="<?php echo URL ?>/loader.gif" /></div>';
                    jQuery("#loaderDiv").show();
                    },
                    success: function (o) {

                    document.getElementById("mon_bloc_tab").innerHTML = o;
                    var table = jQuery('#mine').DataTable({
                    "searching": false,
                            "nextPrev": false,
                            "bLengthChange": false,
                            "bInfo": false,
                            "bPaginate": true,
                            "autoWidth": false,
                            ////"fixedColumns": false,

                    });
                    openSearchFilters();
<?php
if (isset($_SESSION['data_login'])) {
    $colonnes = explode(",", $users['colonne']);
    $nb_colonne = count($colonnes);
    for ($u = 0; $u < $nb_colonne; $u++) {
        if ($colonnes[$u] != '') {
            ?>
                                table.column(<?php echo $colonnes[$u]; ?>).visible(false);
            <?php
        }
    }
} else {

    for ($p = 7; $p <= 18; $p++) {
        ?>
                            table.column(<?php echo $p; ?>).visible(false);
        <?php
    }
}
?>
                    //table.column(0).visible(true);
                    $('div.toggle-vis').on('click', function (e) {
                    e.preventDefault();
                    // Get the column API object
                    var column = table.column($(this).attr('data-columnId'));
                    // Toggle the visibility
                    column.visible(!column.visible());
                    if ($(this).hasClass('toggle-selected')) {
                    $(this).removeClass("toggle-selected");
                    } else {
                    $(this).addClass('toggle-selected');
                    }
                    });
                    }
            });
            closeModal();
            }
            function import_list(id) {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/lists.php?list=" + id,
                    beforeSend: function () {
                    document.getElementById("mon_bloc_tab").innerHTML = '<div id="loaderDiv" style="display: none; text-align: center; width: 100%;">' +
                            '<img src="<?php echo URL ?>/loader.gif" /></div>';
                    jQuery("#loaderDiv").show();
                    },
                    success: function (o) {

                    document.getElementById("mon_bloc_tab").innerHTML = o;
                    var table = jQuery('#mine').DataTable({
                    "searching": false,
                            "nextPrev": false,
                            "bLengthChange": false,
                            "bInfo": false,
                            "bPaginate": true,
                            "autoWidth": false,
                            ////"fixedColumns": false,

                    });
                    openSearchFilters();
<?php
if (isset($_SESSION['data_login'])) {
    $colonnes = explode(",", $users['colonne']);
    $nb_colonne = count($colonnes);
    for ($u = 0; $u < $nb_colonne; $u++) {
        if ($colonnes[$u] != '') {
            ?>
                                table.column(<?php echo $colonnes[$u]; ?>).visible(false);
            <?php
        }
    }
} else {

    for ($p = 7; $p <= 18; $p++) {
        ?>
                            table.column(<?php echo $p; ?>).visible(false);
        <?php
    }
}
?>
                    //table.column(0).visible(true);
                    $('div.toggle-vis').on('click', function (e) {
                    e.preventDefault();
                    // Get the column API object
                    var column = table.column($(this).attr('data-columnId'));
                    // Toggle the visibility
                    column.visible(!column.visible());
                    if ($(this).hasClass('toggle-selected')) {
                    $(this).removeClass("toggle-selected");
                    } else {
                    $(this).addClass('toggle-selected');
                    }
                    });
                    }
            });
            closeModal();
            }

            function import_tags(id) {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/tags.php?tags=" + id,
                    beforeSend: function () {
                    document.getElementById("mon_bloc_tab").innerHTML = '<div id="loaderDiv" style="display: none; text-align: center; width: 100%;">' +
                            '<img src="<?php echo URL ?>/loader.gif" /></div>';
                    jQuery("#loaderDiv").show();
                    },
                    success: function (o) {

                    document.getElementById("mon_bloc_tab").innerHTML = o;
                    var table = jQuery('#mine').DataTable({
                    "searching": false,
                            "nextPrev": false,
                            "bLengthChange": false,
                            "bInfo": false,
                            "bPaginate": true,
                            "autoWidth": false,
                            //"fixedColumns": false,
                            initComplete: (settings, json) => {
                    $('#paginateTable').empty();
                    $('#mine_paginate').appendTo('#paginateTable');
                    },
                            "language": {
                            "paginate": {
                            "previous": "Précédent",
                                    "next": "Suivant"
                            }
                            }

                    });
                    openSearchFilters();
<?php
if (isset($_SESSION['data_login'])) {
    $colonnes = explode(",", $users['colonne']);
    $nb_colonne = count($colonnes);
    for ($u = 0; $u < $nb_colonne; $u++) {
        if ($colonnes[$u] != '') {
            ?>
                                table.column(<?php echo $colonnes[$u]; ?>).visible(false);
            <?php
        }
    }
} else {

    for ($p = 7; $p <= 18; $p++) {
        ?>
                            table.column(<?php echo $p; ?>).visible(false);
        <?php
    }
}
?>
                    //table.column(0).visible(true);
                    $('div.toggle-vis').on('click', function (e) {
                    e.preventDefault();
                    // Get the column API object
                    var column = table.column($(this).attr('data-columnId'));
                    // Toggle the visibility
                    column.visible(!column.visible());
                    if ($(this).hasClass('toggle-selected')) {
                    $(this).removeClass("toggle-selected");
                    } else {
                    $(this).addClass('toggle-selected');
                    }
                    });
                    }
            });
            closeModal();
            }



        </script>


        <script>
            $(document).ready(function () {
            $("#search-box").keyup(function () {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/readCountry.php",
                    data: 'keyword=' + $(this).val(),
                    beforeSend: function () {
                    $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#search-box").css("background", "#FFF");
                    }
            });
            });
            $("#autocomplete-ajax").keyup(function () {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/readFormation.php",
                    data: 'keyword=' + $(this).val(),
                    beforeSend: function () {
                    $("#autocomplete-ajax").css("background", "#FFF url(<?php echo URL ?>/LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                    $("#suggesstion-box-formation").show();
                    $("#suggesstion-box-formation").html(data);
                    $("#autocomplete-ajax").css("background", "#FFF");
                    }
            });
            });
            $("#autocomplete-ajax1").keyup(function () {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/readExperience.php",
                    data: 'keyword=' + $(this).val(),
                    beforeSend: function () {
                    $("#autocomplete-ajax1").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                    $("#suggesstion-box-exp").show();
                    $("#suggesstion-box-exp").html(data);
                    $("#autocomplete-ajax1").css("background", "#FFF");
                    }
            });
            });
            $("#autocomplete-ajax2").keyup(function () {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/readSkills.php",
                    data: 'keyword=' + $(this).val(),
                    beforeSend: function () {
                    $("#autocomplete-ajax2").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                    $("#suggesstion-box-skills").show();
                    $("#suggesstion-box-skills").html(data);
                    $("#autocomplete-ajax2").css("background", "#FFF");
                    }
            });
            });
            });
            function selectFormation(val) {

            $("#autocomplete-ajax").val(val);
            $("#suggesstion-box-formation").hide();
            }

            function selectExperience(val) {

            $("#autocomplete-ajax1").val(val);
            $("#suggesstion-box-exp").hide();
            }

            function selectSkills(val) {

            $("#autocomplete-ajax2").val(val);
            $("#suggesstion-box-skills").hide();
            }

            function selectCountry(val) {
            const words = val.split('/');
            //  $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }

            function selectInvest(val) {
            const words = val.split('/');
            // $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }

            function selectEntrepreneur(val) {
            const words = val.split('/');
            //  $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }

            function selectTags(val) {
            const words = val.split('/');
            // $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }

            function selectRegion(val) {
            const words = val.split('/');
            // $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }



            function add_to_list_myfs() {
            var list = document.getElementById('list_name').value;
            $.ajax({
            type: "POST",
                    url: "<?php echo url ?>save_list_user_myfs.php?list=" + list,
                    success: function (o) {

                    document.getElementById("bloc_list_table1").innerHTML = o;
                    }
            });
            }
            function delete_list(id) {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/delete_list.php?id=" + id,
                    success: function (o) {
                    alert("Liste supprimée!");
                    document.getElementById("bloc_list_table1").innerHTML = o;
                    }

            }
            );
            }

            function delete_list2(id) {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/delete_list.php?id=" + id,
                    success: function (o) {
                    alert("Liste supprimée!");
                    document.getElementById("bloc_list_table1").innerHTML = o;
                    }

            }
            );
            }

            function affiche_table() {
            document.getElementById("affichage_list").style.display = "none";
            document.getElementById("affichage_table").style.display = "grid";
            }

            function affiche_liste() {
            document.getElementById("affichage_table").style.display = "none";
            document.getElementById("affichage_list").style.display = "grid";
            }

            function afficher_menu_burger() {

            document.getElementById("bloc_mon_menu").style.display = "block";
            }

            function hider_menu() {

            document.getElementById("bloc_mon_menu").style.display = "none";
            }

            function update_colonne(nb) {
            $.ajax({
            type: "POST",
                    url: "<?php echo URL ?>/update_colonne.php?nb=" + nb,
                    success: function (o) { }
            });
            }


        </script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>

    </body>
</html>