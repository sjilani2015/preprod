<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
if (!isset($_SESSION['data_login'])) {
    header("location:" . URL . "/connexion");
    exit();
}
$mon_user = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}

$currentPage = "notification";

function change_date_fr_chaine_related($date) {
    $split1 = explode(" ", $date);
    $split = explode("-", $split1[0]);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $jour . " " . $mm . "<br>" . $annee;
    return $creation;
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Mes notifications - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
        <style>
            .account__right{
                padding-top: 35px;
                background: #f7fafc !important;
                border: 1px solid #eef2f6 !important;
            }
            .form-control{
                background-color: #fff !important;
                border-color: #eef2f6 !important;
            }
            .table-search__startup {
                width: auto !important;
                max-width: auto !important;
            }
        </style>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>

        <div class="page-wrapper">
            <?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">            
                    <div class="account">
                        <div class="account__aside">
                            <div class="nav-vertical">
                                <div class="nav-vertical__title">
                                    <span class="title">Mon profil</span>
                                </div>
                                <div class="nav-vertical__list">
                                    <?php include ('account/menu.php'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="account__right">

                            <div style="font-family: 'EuclidBold';margin-bottom: 32px;color: #8694A1;">Mes notifications</div>
                            <form class="stepy-basic" action="" method="post">
                                <div class="form-group">
                                    <div class="formgrid formgrid--4col">
                                        <div class="formgrid__item">
                                            <label>&Agrave; partir</label>
                                            <input type="date" value="<?php echo date('Y-m-d') ?>" name="de"   class="form-control" />
                                        </div>
                                        <div class="formgrid__item">
                                            <label>Jusqu'au</label>
                                            <input type="date" value="<?php echo date('Y-m-d') ?>" name="a"  class="form-control" />
                                        </div>
                                        <div class="formgrid__item">
                                            <label>Listes enregistrées</label>
                                            <div class="custom-select">
                                                <select class="form-control" data-placeholder="" name="list">
                                                    <option></option>
                                                    <?php
                                                    $sql_list = mysqli_query($link, "select date(dt) as ddt,nom,id,total_new from user_save_list where user=" . $mon_user['id'] . " order by nom ");
                                                    while ($data_list = mysqli_fetch_array($sql_list)) {
                                                        ?>
                                                        <option value="<?php echo $data_list['id']; ?>"><?php echo utf8_encode($data_list['nom']); ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="formgrid__item">
                                            <label>Type</label>
                                            <div class="custom-select">
                                                <select class="form-control" data-placeholder="" name="type">
                                                    <option></option>

                                                    <option value="startup">Nouvelle startup</option>
                                                    <option value="lf">Nouvelle levées de fonds</option>
                                                    <option value="bodacc">Nouveau mouvement Bodacc</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="margin-bottom: 40px;margin-top: 20px;">
                                        <button type="submit" class="btn btn-primary">Rechercher</button>
                                    </div>
                                </div>

                            </form>

                            <?php
                            if ($_POST['de']) {
                                $dt1 = $_POST['de'];
                                $dt2 = $_POST['a'];
                                $list = $_POST['list'];
                                $type = $_POST['type'];

                                if ($list != "") {
                                    $list_sql = " and liste=" . $list . " ";
                                } else {
                                    $list_sql = " ";
                                }
                                if ($type != "") {
                                    if ($type == "startup") {
                                        $type_sql = " and list_sup!=''  ";
                                    }
                                    if ($type == "lf") {
                                        $type_sql = " and list_lf!=''  ";
                                    }
                                    if ($type == "bodacc") {
                                        $type_sql = " and list_verif!=''  ";
                                    }
                                } else {
                                    $type_sql = " ";
                                }
                                // echo "select * from notifications where date(dt)>='" . $dt1 . "' and date(dt)<='" . $dt2 . "' and user=" . $mon_user['id'] . " $type_sql $list_sql  order by dt desc limit 10";
                                ?>
                                <div class="tablebloc">
                                    <?php $sql_notif = mysqli_query($link, "select * from notifications where date(dt)>='" . $dt1 . "' and date(dt)<='" . $dt2 . "' and user=" . $mon_user['id'] . " $type_sql $list_sql  order by dt desc"); ?>
                                    <?php $totalNotifs = mysqli_num_rows($sql_notif);
                                    ?>
                                    <?php if ($totalNotifs == 0) { ?>
                                        <div class="alert alert--infos">Pas de notification envoyée à ce jour.</div>
                                    <?php } else { ?>
                                        <div class="table-responsive">
                                            <table id="mine2" class="table table-search">
                                                <thead>
                                                    <tr>
                                                        <th class="table-search__date text-center">Date</th>
                                                        <th class="table-search__tags">Notification</th>
                                                        <th class="table-search__startup">Startup</th>
                                                        <th class="table-search__tags">Liste</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php

                                                    $sql_notif = mysqli_query($link, "select * from notifications where date(dt)>='" . $dt1 . "' and date(dt)<='" . $dt2 . "' and user=" . $mon_user['id'] . " $type_sql $list_sql order by dt desc");
                                                    while ($data_notifs = mysqli_fetch_array($sql_notif)) {
                                                        $list_sup = $data_notifs['list_sup'];
                                                        $list_lf = $data_notifs['list_lf'];
                                                        $list_verif = $data_notifs['list_verif'];
                                                        ?>
                                                        <?php
                                                        if ($list_sup != '') {
                                                            $tabs_sup = explode(",", $list_sup);
                                                            $nb_list_sup = count($tabs_sup);
                                                            for ($i = 0; $i < 5; $i++) {
                                                                if ($tabs_sup[$i] != "") {
                                                                    $startups = mysqli_fetch_array(mysqli_query($link, "Select * from startup Where id =" . $tabs_sup[$i]));
                                                                    ?>
                                                                    <tr>
                                                                        <td class="table-search__date text-center">
                                                                            <?php echo change_date_fr_chaine_related($data_notifs['dt']); ?>
                                                                        </td>
                                                                        <td class="table-search__tags text-center">Nouvelle startup ajoutée</td>
                                                                        <td>
                                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a> 
                                                                        </td>

                                                                        <td class="table-search__tags text-center"><?php echo $data_notifs['nom']; ?></td>


                                                                    </tr>

                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($list_lf != '') {
                                                            $tabs_sup_lf = explode(",", $list_lf);
                                                            $nb_list_sup = count($tabs_sup_lf);
                                                            for ($i = 0; $i < $nb_list_sup; $i++) {
                                                                if ($tabs_sup_lf[$i] != "") {
                                                                    $startups = mysqli_fetch_array(mysqli_query($link, "Select * from startup Where id =" . $tabs_sup_lf[$i]));
                                                                  //  $lfss = mysqli_fetch_array(mysqli_query($link, "Select * from lf Where id_startup =" . $startups['id']." and rachat=0 and date(date_ajout)>='" . $dt1 . "' and date(date_ajout)<='" . $dt2 . "'" ));
                                                                    ?>
                                                                    <tr>
                                                                        <td class="table-search__position center">
                                                                            <?php echo change_date_fr_chaine_related($data_notifs['dt']); ?>
                                                                        </td>
                                                                        <td class="table-search__position center">Nouvelle levée de fonds ajoutée</td>
                                                                        <td>
                                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a>                                                                   
                                                                        </td>


                                                                        <td class="table-search__position center"><?php echo $data_notifs['nom']; ?>.</td>


                                                                    </tr>

                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($list_verif != '') {
                                                            $tabs_sup2 = explode(",", $list_verif);

                                                            $nb_list_verif = count($tabs_sup2);
                                                            for ($i = 0; $i < $nb_list_verif; $i++) {
                                                                if ($tabs_sup2[$i] != "") {
                                                                    $startups = mysqli_fetch_array(mysqli_query($link, "Select * from startup Where id =" . $tabs_sup2[$i]));
                                                                    $startup_depot = mysqli_fetch_array(mysqli_query($link, "select * from startup_depot where id_startup=" . $startups['id']." and date(dt)>='" . $dt1 . "' and date(dt)<='" . $dt2 . "' order by id desc limit 1 "));
                                                                    ?>
                                                                    <tr>
                                                                        <td class="table-search__position center">
                                                                            <?php echo change_date_fr_chaine_related($data_notifs['dt']); ?>
                                                                        </td>
                                                                        <td class="table-search__position center"><?php echo $startup_depot['depot'] ?></td>
                                                                        <td>  
                                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a>                                                                   
                                                                        </td>
                                                                        <td class="table-search__position center"><?php echo $data_notifs['nom']; ?></td>
                                                                    </tr>

                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>

                                                        <?php
                                                    }
                                                    ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>


                                </div>
                            <?php } else { ?>

                                <div class="tablebloc">
                                    <?php $sql_notif = mysqli_query($link, "select * from notifications where user=" . $mon_user['id'] . " order by dt desc limit 10"); ?>
                                    <?php $totalNotifs = mysqli_num_rows($sql_notif); ?>
                                    <?php if ($totalNotifs == 0) { ?>
                                        <div class="alert alert--infos">Pas de notification envoyée à ce jour.</div>
                                    <?php } else { ?>
                                        <div class="table-responsive">
                                            <table id="mine2" class="table table-search">
                                                <thead>
                                                    <tr>
                                                        <th class="table-search__date text-center">Date</th>
                                                        <th class="table-search__tags">Notification</th>
                                                        <th class="table-search__startup">Startup</th>
                                                        <th class="table-search__tags">Liste</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $sql_notif = mysqli_query($link, "select * from notifications where user=" . $mon_user['id'] . " order by dt desc limit 10");
                                                    while ($data_notifs = mysqli_fetch_array($sql_notif)) {
                                                        $list_sup = $data_notifs['list_sup'];
                                                        $list_lf = $data_notifs['list_lf'];
                                                        $list_verif = $data_notifs['list_verif'];
                                                        ?>
                                                        <?php
                                                        if ($list_sup != '') {
                                                            $tabs_sup = explode(",", $list_sup);
                                                            $nb_list_sup = count($tabs_sup);
                                                            for ($i = 0; $i < 5; $i++) {
                                                                if ($tabs_sup[$i] != "") {
                                                                    $startups = mysqli_fetch_array(mysqli_query($link, "Select * from startup Where id =" . $tabs_sup[$i]));
                                                                    ?>
                                                                    <tr>
                                                                        <td class="table-search__date text-center">
                                                                            <?php echo change_date_fr_chaine_related($data_notifs['dt']); ?>
                                                                        </td>
                                                                        <td class="table-search__tags text-center">Nouvelle startup ajoutée</td>
                                                                        <td>
                                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a> 
                                                                        </td>

                                                                        <td class="table-search__tags text-center"><?php echo $data_notifs['nom']; ?></td>


                                                                    </tr>

                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($list_lf != '') {
                                                            $tabs_sup1 = explode(",", $list_lf);
                                                            $nb_list_sup = count($tabs_sup1);
                                                            for ($i = 0; $i < 5; $i++) {
                                                                if ($tabs_sup1[$i] != "") {
                                                                    $startups = mysqli_fetch_array(mysqli_query($link, "Select * from startup Where id =" . $tabs_sup1[$i]));
                                                                    ?>
                                                                    <tr>
                                                                        <td class="table-search__position center">
                                                                            <?php echo change_date_fr_chaine_related($data_notifs['dt']); ?>
                                                                        </td>
                                                                        <td class="table-search__position center">Nouvelle levée de fonds ajoutée</td>
                                                                        <td>
                                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a>                                                                   
                                                                        </td>


                                                                        <td class="table-search__position center"><?php echo $data_notifs['nom']; ?>.</td>


                                                                    </tr>

                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($list_verif != '') {
                                                            $tabs_sup2 = explode(",", $list_verif);

                                                            $nb_list_verif = count($tabs_sup2);
                                                            for ($i = 0; $i < $nb_list_verif; $i++) {
                                                                if ($tabs_sup2[$i] != "") {
                                                                    $startups = mysqli_fetch_array(mysqli_query($link, "Select * from startup Where id =" . $tabs_sup2[$i]));
                                                                    $startup_depot = mysqli_fetch_array(mysqli_query($link, "select * from startup_depot where id_startup=" . $startups['id']." order by id desc limit 1 "));
                                                                    ?>
                                                                    <tr>
                                                                        <td class="table-search__position center">
                                                                            <?php echo change_date_fr_chaine_related($data_notifs['dt']); ?>
                                                                        </td>
                                                                        <td class="table-search__position center"><?php echo $startup_depot['depot'] ?></td>
                                                                        <td>  
                                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a>                                                                   
                                                                        </td>
                                                                        <td><?php echo $data_notifs['nom']; ?></td>
                                                                    </tr>

                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>

                                                        <?php
                                                    }
                                                    ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>


                                </div>

                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>