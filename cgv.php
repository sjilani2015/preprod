<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Conditions générales de vente - <?= SITENAME; ?></title>
        <meta name="description" content="Foire aux questions <?= SITENAME; ?>">
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
           <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="edito">
                    
                        <div class="page-title"><h1>CGV</h1></div>
                        <div class="page-intro">Mis à jour le 13.05.2021</div>
                        <h2>Sommaire</h2>
                        <ul>
                            <li>1. Définitions</li>
                            <li>2. Accès au Site</li>
                            <li>3. Accès aux Services</li>
                            <li>4. Création du Compte</li>
                            <li>5. Partage de données</li>
                            <li>6. Prix et conditions de paiement</li>
                            <li>7. Durée – Non-renouvellement – Résiliation</li>
                            <li>9. Données personnelles et respect de la vie privée</li>
                            <li>10. Propriété intellectuelle</li>
                            <li>11. Modification des Conditions Générales</li>
                            <li>12. Divers</li>
                        </ul>
                        
                        <p>La société <b>myfrenchstartup</b>, société par action simplifiée, au capital de 2.000€, sise au 26-28, rue Danielle Casanova 75002 Paris et enregistrée au RCS de Paris sous le numéro 798 203 774 (ci-après, « <b>la Société Editrice</b> » ou la « <b>Société</b> »), édite un site internet référencé à l’adresse suivante : www.myfrenchstartup.com (ci-après, le « Site »). Les présentes Conditions Générales (ci-après, les « <b>Conditions Générales</b> ») ont pour objet de fixer, à l’exclusion de tout autre document ou conditions, les modalités d’accès aux Services disponibles sur le Site.</p>
                     
                        <h2>1 Définitions</h2>
                        
                        <p>Chaque terme commençant par une majuscule a la signification indiquée dans sa définition, qu'il soit au singulier ou au pluriel. </p>
                        <ul>
                            <li>« <b>Abonnement</b> » désigne toute souscription à une offre sur le Site.</li>
                            <li>« <b>Abonné </b> » désigne toute personne physique ou morale ayant souscrit à un Abonnement.</li>
                            <li>« <b>Compte </b> » désigne le compte utilisé par un Utilisateur pour accéder à certains Services</li>
                            <li>« <b>Contenu </b> » désigne l’ensemble des informations et contenus éditoriaux accessibles sur le Site.</li>
                            <li>« <b>Contribution</b> » désigne le montant annuel payé par les Utilisateurs qui souhaitent accéder aux Services et Contenus sans partage de leurs données personnelles.</li>
                            <li>« <b>Identifiants</b> » désigne l'identifiant de l’Utilisateur et son mot de passe de connexion choisis lors de son inscription, lui permettant de s'identifier et de se connecter à son Compte.</li>
                            <li>« <b>Partenaire</b> » désigne les partenaires commerciaux (startup, fonds, publications spécialisées, etc.) de la Société.</li>
                            <li>« <b>Prestation Payante</b> » désigne l’ensemble des offres de Services et Contenus payants proposées par le Site dans les conditions mentionnées à l’article 3 des présentes.</li>
                            <li>« <b>Service</b> » désigne l’ensemble des services accessibles depuis le Site..</li>
                            <li>« <b>Utilisateur</b> » désigne toute personne physique ou morale ayant créé un Compte.</li>
                            <li>« <b>Visiteur</b> » désigne toute personne accédant au Site.</li>
                        </ul>
                    
                        <h2>2 Accès au Site </h2>
            
                            <p>La Société Editrice est soumise à une obligation de moyens. En conséquence, elle s'engage à faire les meilleurs efforts pour sécuriser l'accès, la consultation et l'utilisation du Site.</p>
                            <p>Les équipements permettant l'accès au Site sont à la charge exclusive du Visiteur, de même que les frais de télécommunication induits par leur utilisation.</p>
                            <p>Il appartient au Visiteur de prendre toute mesure nécessaire pour s'assurer que les caractéristiques techniques de son équipement lui permettent la consultation des Contenus et l’accès aux Services.</p>
                            <p>Le Visiteur reconnait le droit de la Société d’interrompre temporairement l’accès au Site, et donc aux Contenus et aux Services, lorsque celui-ci ou tout serveur lié fait l’objet d’une opération d’actualisation ou de maintenance. Dans cette hypothèse, la Société s’efforcera de limiter l’inconvénient pour les Visiteurs en réduisant le délai d’interruption au temps minimum nécessaire pour réaliser l’opération en cause.</p>

                        <h2>3 Accès aux Services </h2>
                        <h3>3.1 Nature des Services accessibles</h3>

                            <p>Le Site propose plusieurs Services et des Contenus permettant aux personnes intéressées de suivre et de participer aux développements des activités de startups françaises et aux startups d’accéder aux informations nécessaires à leur développement.</p>
                            <p>
                                Le Site propose plusieurs Services et des Contenus permettant aux personnes intéressées de suivre et de participer aux développements des activités de startups françaises et aux startups d’accéder aux informations nécessaires à leur développement.
                                Il permet notamment,<br>

                                •	aux investisseurs et à toute personne intéressée de consulter des informations relatives au développement des startups référencées et d’être tenus informée de l’actualité du secteur par le biais d’alertes, de mises à jour, de newsletters, de publications et d’articles ;<br>
                                •	aux dirigeants de start-up de promouvoir leurs activités auprès d’investisseurs et de professionnels de leur secteur, afin d’organiser des levées de fond, de développer leurs produits et de promouvoir leurs activités ;<br>
                                •	d’obtenir des prestations à la carte, sur des sujets spécifiques.<br>


                                D’autres Services, tels que l’agrégation d’offres d’emploi à destination des personnes intéressées, pourront être proposés.
                            </p>
                        
                        <h3>3.2 Conditions d’accès aux Services et Contenus du Site</h3>
                            <p>L’accès au Site est gratuit, il est réservé à toute personne majeure, agissant à titre professionnel. Toute utilisation du Site emporte acceptation des présentes Conditions générales.</p>
                            <p>Les Services proposés par la Société sont accessibles soit à titre gratuit, soit à titre payant.</p>
                        
                        <h3>3.2.1 Accès aux Services gratuits</h3>
                        <p>L’accès à certains Services et Contenus gratuits nécessite la création d’un Compte afin de, notamment, accéder à certaines informations et Contenus.</p>
                        <p>La création du Compte et le téléchargement de Contenus et Services gratuits nécessitent par ailleurs le partage de certaines informations personnelles avec les Partenaires de la Société</p>
                        <p>Les personnes qui ne souhaitent pas que leurs données personnelles (coordonnées, centres d’intérêts, etc.) soient partagées avec les Partenaires de la société peuvent souscrire un Abonnement spécifique, payant, afin d’accéder aux Services et Contenus normalement proposés à titre gratuit.</p>
                        <p>A cette fin, la Société Editrice a créé une offre permettant d’accéder aux Services et Contenus gratuits, sans partage de données avec ses Partenaires.</p>


                        <h3>3.2.2 Accès aux Services contre paiement</h3>
                        <p>Le Site propose un ensemble de Services et Contenus payants, en complément de ses Services gratuits. Les Prestations Payantes sont indiquées comme telles sur le Site.
                            L’accès aux prestations payantes nécessite la création d’un Compte.
                        </p>
                        
                        <p>Les Prestations Payantes offrent un niveau supérieur d’informations et de suivi du secteur des start-up françaises ainsi que des outils avancés de promotion de leurs activités.</p>

                        <p>La Société Editrice propose plusieurs types de Prestations Payantes :</p>
                        <p>
                            - Un Abonnement à des Services et Contenus spécifiques : développés en collaboration avec les Partenaires de la société Editrice afin de permettre aux start-up de développer leurs activités, de se tenir au courant des actualités du secteur et de promouvoir leurs services après du public et des investisseurs. Les investisseurs souscripteurs bénéficient également d’un accès à des informations complètes sur les start-up dans lesquelles ils souhaitent investir et peuvent recevoir des alertes sur des thèmes choisis par eux ;
                        </p>
                        <p>
                            - L’achat à l’unité de certains Services et Contenus spécifiques : les Utilisateurs intéressés uniquement par certains Contenus ou Services spécifiques peuvent acheter et télécharger des Contenus et Services à l’unité. Les Contenus et Services éligibles sont indiqués comme tels sur le Site et sont exclus des Abonnements. Ils comprennent par exemple la réalisation d’études personnalisées ;
                        </p>
                        
                        <h4>A ces Prestations payantes s’ajoute :</h4>
                        <p>- Le versement de la Contribution afin de souscrire un Abonnement permettant aux utilisateurs de ne pas partager leurs données avec les Partenaires tout en ayant accès aux Services et Contenus développés et proposés par ces mêmes Partenaires, dans les conditions prévues à l’article 5 des présentes</p>
                        <p>La souscription d’un Abonnement ou l’achat d’une Prestation à l’unité est uniquement possible en ligne. La confirmation de paiement vaut acceptation de la demande d’Abonnement ou d’achat d’une Prestation par la Société Editrice, sous réserve de la perception effective du paiement.</p>
                        <p>L’Utilisateur reconnaît que les systèmes d’enregistrement automatique du Site valent preuve de la nature, du contenu et de la date de sa demande. Les données enregistrées par le système de paiement constituent la preuve de la transaction financière.</p>
                        <p>Seul l’Utilisateur ayant souscrit à la Prestation Payante est autorisé à y accéder en s’identifiant grâce à son login et son mot de passe.</p>
                        <p>Par ailleurs, un Abonnement ne peut être ni vendu ni transféré à une autre personne physique ou morale sauf accord exprès de la Société Editrice.</p>
                        <p>La Société Editrice met régulièrement à jour la liste des Prestations accessibles par Abonnement.</p>



                        <h2>4 Création du Compte</h2>
                        <p>Lors de la création du Compte et lorsqu’il complète son profil, l’Utilisateur s’engage à fournir des informations personnelles exactes.</p>
                        <p>Une fois l’inscription finalisée, l’Utilisateur peut télécharger certains Contenus, accéder à certains Services gratuits ou souscrire un Abonnement. Il peut également acheter à l’unité certains Services ou Contenus.</p>
                        <p>Seul le titulaire du Compte est autorisé à se connecter en utilisant son Identifiant et il s’engage à conserver son Identifiant de manière strictement confidentielle.</p>
                        <p>À ce titre, toute utilisation du Compte est réputée avoir été faite par l’Utilisateur lui-même ou avec son autorisation. En cas d’utilisation frauduleuse du Compte, notamment en violation des articles 8 et 10, la Société Editrice se réserve le droit de clôturer le Compte et d’interdire tout accès futur de l’Utilisateur aux Services et aux Contenus, sauf si l’Utilisateur est en mesure de démontrer que son Compte a été piraté ou utilisé par un tiers sans son consentement. </p>
                        <p>Un Compte ne peut être ni vendu ni transféré à un tiers.</p>

                        <h2>5 Partage de données</h2>
                        <p>Certains Contenus du Site ont été construits en coopération avec les Partenaires de la Société Editrice, ou ont été financés via la mise en place de partenariat avec ses fournisseurs.</p>
                        <p>La liste des Partenaires de la Société est accessible en suivant ce lien.</p>
                        <p>En créant un Compte, les Utilisateurs comprennent que les données personnelles renseignées lors de leur utilisation des Services et de leur accès aux Contenus font l’objet d’un partage avec les Partenaires de la société Editrice. </p>
                        <p>A ce titre, lorsque l’accès à un Contenu nécessite l’ouverture d’un Compte ou la fourniture de données personnelles (adresse email, profession, etc.), l’Utilisateur autorise la Société à partager avec les Partenaires les données transmises, afin de recevoir, dans un cadre professionnel, des offres en lien avec les Services choisis. L’accord de l’Utilisateur permet de continuer à lui proposer gratuitement du Contenu de qualité. Lorsque les données sont communiquées aux Partenaires, elles le sont uniquement pour proposer des services en lien avec l’activité professionnelle de l’Utilisateur et ses centres d’intérêts, en relation avec les newsletters et évènements auxquels il s’est inscrit. Aucune communication n’est faite sans son accord exprès et sans qu’il n’en ait préalablement été informé.</p>
                        <p>L’Utilisateur qui ne souhaite pas que ses données soient partagées avec les Partenaires doit l’indiquer au moment de la création de son Compte ou lorsqu’il renseigne les données nécessaires à l’accès à un Contenu ou Service. En contrepartie, il accepte de payer une Contribution annuelle, sous la forme d’un Abonnement dont la durée est détaillée à l’article 6.</p>
                        <p>Le paiement de la Contribution permet à la Société Editrice de financer les Services et Contenus accessibles sur son Site sans partage des données personnelles de la personne concernée avec les Partenaires.</p>
                        <p>Dans ce cas, les seuls destinataires des données mentionnées lors de l’ouverture du Compte, de l’accès aux Contenus et de l’utilisation des Services sont les personnes en charge de la gestion des Abonnements au sein de la Société.</p>


                        <h2>6 Prix et conditions de paiement</h2>
                        
                        <h3>6.1 Prix</h3>
                        <p>Le détail des prix des Abonnements est accessible depuis le Compte ou en suivant le lien suivant.</p>
                        <p>Les prix des Prestations à la carte sont indiqués lorsque vous sélectionnez la Prestation concernée.</p>

                        <h3>6.2 Conditions de paiement</h3>
                        <p>Pour s’abonner ou se réabonner, pour payer la Contribution ou pour payer une Prestation à la carte, l’Utilisateur procède par souscription par carte bancaire sécurisée (Visa, Master Card). Le paiement est assuré par une solution sécurisée qui intègre un procédé de cryptage en mode SSL (Secure Socket Layer) ou tout autre procédé équivalent.</p>
                        <p>L’Utilisateur est seul responsable du paiement par carte bancaire. La Société Editrice se dégage de toute responsabilité face aux difficultés de paiement rencontrées par l’Utilisateur et invite celui-ci à prendre contact avec sa banque le cas échéant.</p>
                        <p>Dans le cadre de la tacite reconduction de l’Abonnement, il appartient à l’Utilisateur dont la durée de validité de la carte a expiré de mettre à jour ses nouvelles coordonnées en se connectant à son Compte.</p>

                        <h2>7 Durée – Non-renouvellement – Résiliation</h2>
                        <h3>7.1 Durée</h3>
                        <p>Les présentes Conditions Générales s’appliquent tant que l’Abonnement souscrit est en vigueur et que l’Utilisateur dispose d’un Compte.</p>
                        <p>Tous les Abonnements proposés par la Société Editrice sont souscrits pour une durée de douze (12) mois, reconductible par tacite reconduction pour des périodes successives de durée identique.</p>
                        <p>Toute souscription engage l’Utilisateur pour la durée mentionnée.</p>

                        <h3>7.2 Non-renouvellement - Résiliation</h3>
                        <p>L’Utilisateur peut suspendre le renouvellement de son Abonnement par l’envoi d’un email à l’adresse : contribution@myfrenchstartup.com :</p>
                        <p>• Quinze (15) jours avant la date du renouvellement pour les Abonnements au mois ;</p>
                        <p>• Trente (30) jours avant la date du renouvellement pour les Abonnements à l’année ;</p>
                        <p>En cas de non-respect des délais précités, l’Abonnement est automatiquement reconduit.</p>
                        <p>En cas de non-paiement des sommes dues, la Société Editrice pourra prononcer la résiliation de plein droit de l’Abonnement, par l’envoi d’une mise en demeure par lettre simple ou e-mail restée sans réponse pendant trente (30) jours. L’Utilisateur restera dans tous les cas redevable des impayés et des frais associés.</p>
                        <p>En cas de manquement de l’Utilisateur aux dispositions des articles 8 et 10 et plus spécifiquement, en cas de détournement à des fins commerciales de la propriété intellectuelle de la Société Editrice, celle-ci se réserve le droit de mettre fin sans notification à l’Abonnement.</p>
                        <p>Par ailleurs, la Société Editrice se réserve le droit d’intenter toute action ou réclamation nécessaire afin d’empêcher, faire cesser et sanctionner toute atteinte à ses droits sur les Contenus et le Site, y compris dans le cadre de poursuites judiciaires, et ce sans mise en demeure préalable.</p>
                        <p>Les Utilisateurs sont informés que toute clôture anticipée de leur Compte suspend immédiatement le bénéfice de leur Abonnement.</p>


                        <h3>7.3 Conséquences de la clôture du Compte</h3>
                        <p>En cas de clôture du Compte, pour quelque raison que ce soit, l’Utilisateur ne peut plus accéder aux Services et Contenus souscrits, qu’il ait ou non résilié son Abonnement.</p>
                        <p>L’Utilisateur qui souhaite conserver le bénéfice des Services et Contenus téléchargés doit par conséquent veiller à procéder à la sauvegarde des données qu’il souhaite conserver.</p>
                        <p>La fermeture du Compte pour quelque cause que ce soit, n’ouvre aucun droit à indemnité au bénéfice de l’Utilisateur.</p>

                        <h2>8 Règles d’usage du Site</h2>
                        <p>Seules les personnes ayant créé un Compte peuvent publier et interagir sur le Site.</p>
                        <p>Lorsque le Site le prévoit expressément, les Utilisateurs ont la possibilité de commenter des Contenus, ou d’envoyer des demandes de contact. En toute hypothèse, les Utilisateurs restent entièrement responsables de leurs commentaires.</p>
                        <p>Les Utilisateurs s’interdisent par ailleurs d’intégrer dans leurs commentaires des contenus contraires ou susceptibles d’être contraires aux bonnes mœurs et à l’ordre public et qui constituent ou sont susceptibles de constituer une violation des dispositions légales et réglementaires en vigueur, notamment si leur nature se rapporte à la drogue, l’alcool, l’incitation à la haine raciale, au crime, au harcèlement, au dénigrement des autres Utilisateurs, des Visiteurs du Site, de la Société Editrice, ou de tout tiers, à l’usurpation de l’identité d’autrui, au piratage informatique (virus, diffusion de copies logiciel, spams, prélèvements abusifs dans la base de données…), ou au non-respect des droits de propriété intellectuelle des tiers, etc.</p>
                        <p>Ils s’interdisent également d’intégrer dans leurs commentaires des contenus ayant un caractère dangereux et/ou irrespectueux pour autrui, notamment, des contenus ayant un caractère violent, vulgaire, ou comprenant des insultes, des moqueries, tout contenu pornographique, etc.</p>
                        <p>La Société Editrice se réserve le droit, à tout moment et sans préavis, de résilier l’Abonnement et de clôturer le Compte de l’Utilisateur en cas de violation des dispositions des présentes.</p>
                        
                        <h2>9 Données personnelles et respect de la vie privée</h2>
                        <p>En complément des informations disponibles à l’article 5, il est rappelé aux Visiteurs et aux Utilisateurs que les informations indiquées comme obligatoires sont indispensables pour ouvrir un Compte, souscrire à une Prestation Payante, envoyer une demande de contact, et, plus généralement, pour accéder aux Contenus et Services.</p>
                        <p>Si l’Utilisateur ne souhaite pas communiquer ces informations, la Société Editrice ne pourra pas lui permettre de recourir aux Services concernés.</p>
                        <p>En toute circonstance, le responsable de traitement des données personnelles au sens de l’article 4 du Règlement Général sur la Protection des Données est la Société Editrice.</p>
                        <p>Les données collectées par la Société Editrice sont collectées, enregistrées et stockées en conformité avec les dispositions de la loi relative à l’informatique, aux fichiers et aux libertés du 6 janvier 1978 dans sa version en vigueur, ainsi qu’avec les dispositions du Règlement Général sur la Protection des Données (RGPD).</p>
                        <p>Conformément à la politique de la Société Editrice en matière de données, les Utilisateurs et les Visiteurs disposent d’un droit d’accès leur permettant à tout moment de connaître la nature des données collectées les concernant, de demander leur rectification ou leur effacement.</p>
                        <p>Ce droit s’exerce par simple envoi d’un email à privacy@myfrenchstartup.com. Pour tout savoir sur la manière dont la Société Editrice gère les données personnelles, les Utilisateurs sont invités à se reporter à notre Charte de confidentialité.</p>
                        <p>Lorsqu’un Utilisateur ouvre un Compte sur le Site, ses données et les données liées à son utilisation du Site sont conservées tant qu’il n’a pas résilié son Compte.</p>
                        <p>Après clôture du Compte, l’adresse email de l’Utilisateur concerné est cependant conservée tant qu’il n’a pas demandé à être désinscrit des listes de diffusion, afin de le tenir informé des évènements et des publications de la Société Editrice. Dans tous les cas, l’Utilisateur peut se désinscrire à tout moment en cliquant sur le lien présent dans les newsletters.</p>
                        <p>L’adresse email et le contenu des messages des personnes qui ont soumis une demande via le Site sont conservés pendant une durée de deux (2) ans à compter de l’envoi du message.</p>
                        <p>Les données relatives aux candidatures ne sont pas collectées par la Société Editrice.</p>
                        <p>En cas de Compte inactif pendant une durée de vingt-quatre mois consécutifs, les données relatives à l’utilisation du Compte par l’Utilisateur sont effacées par la Société Editrice.</p>
                        <p>Les données de fréquentation et de traçabilité des Utilisateurs et des Visiteurs sont conservées pour une durée de 13 mois à compter de la date du dépôt du cookie ou du traceur sur le terminal de la personne concernée.</p>
                        <p>La Société Editrice ne conserve pas les données de paiement.</p>
                        <p>Lorsque l’Utilisateur a consenti au partage de ses données personnelles avec les Partenaires, l’exercice des droits prévus au RGPD se fait directement auprès des Partenaires.</p>
                        <p>En cas de difficulté en lien avec la gestion de ses données personnelles, toute personne concernée a le droit d’introduire une réclamation auprès de la CNIL ou auprès de toute autorité de contrôle compétente.</p>
                        <h2>10 Propriété intellectuelle</h2>
                        <p>L’Utilisateur reconnaît expressément que l’ensemble des droits de propriété intellectuelle afférents au Site, aux Contenus et aux Services est la propriété exclusive de la Société Editrice.</p>
                        <p>Les marques, noms de domaines, produits, logiciels, images, vidéos, textes présents ou relatifs aux Contenus et au Site, et plus généralement, tous les éléments objets de droits de propriété intellectuelle sont et restent la propriété exclusive de la Société Editrice, exception faite des éléments envoyés par les Utilisateurs ou les Partenaires. Aucune cession de droits de propriété intellectuelle n’est réalisée au travers des présentes Conditions Générales. Toute reproduction totale ou partielle, modification ou utilisation des éléments susvisés pour quelque motif que ce soit est strictement interdite.</p>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/animated.min.js"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }
        </script>
    </body>
</html>