<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
if (!isset($_POST["nom"])) {
    $_SESSION['val1'] = random_int(1, 9);
    $_SESSION['val2'] = random_int(1, 9);
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Contact - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <?php
            if (isset($_POST["nom"])) {


                $val1 = $_SESSION['val1'];
                $val2 = $_SESSION['val2'];
                $calcul = $_POST['calcul'];

                $tots = $val1 + $val2;
                if ($tots == $calcul) {
                    mysqli_query($link, "INSERT INTO contact (prenom, nom, email, tel, message, sujet, date,ip) VALUES ( '" . addslashes($_POST["prenom"]) . "', '" . addslashes($_POST["nom"]) . "', '" . addslashes($_POST["email"]) . "', '" . addslashes($_POST["tel"]) . "', '" . addslashes($_POST["story"]) . "', '" . addslashes($_POST["sujet"]) . "', '" . date('Y-m-d H:i:s') . "','" . $_SERVER['REMOTE_ADDR'] . "');");
                    $headers = "From: \"" . $_POST['lastname_contact'] . "\"<" . $_POST['email'] . ">\n";
                    $headers .= "Reply-To: " . $_POST['email'] . "\n";
                    $headers .= "Content-Type: text/html; charset=\"utf-8\"";
                    $message = 'Prénom :' . $_POST["prenom"] . "<br>
		Nom :" . $_POST["nom"] . "<br>
		Email :" . $_POST["email"] . "<br>
		Tél :" . $_POST["tel"] . "<br>
		Sujet :" . $_POST["sujet"] . "<br>
		Message :" . $_POST["story"] . "<br>
		Date :" . date('Y-m-d H:i:s') . "<br>";

                    require 'phpmailer/PHPMailerAutoload.php';

                    $mail = new PHPMailer();

                    $mail->IsSMTP();
                    $mail->Host = 'ssl0.ovh.net';  // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;                               // Enable SMTP authentication
                    $mail->Username = 'inscription@myfrenchstartup.com';                 // SMTP username
                    $mail->Password = '04Betterway#p';                           // SMTP password
                    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = 587;                               // TCP port to connect to

                    $mail->setFrom('inscription@myfrenchstartup.com','myFrenchStartup');
                    $mail->addAddress('contact@myfrenchstartup.com');     // Add a recipient
                    //$mail->addBCC('samijilani@live.com');

                    $mail->isHTML(true);                                  // Set email format to HTML

                    $mail->Subject = "Demande de contact";

                    $mail->Body = utf8_decode($message);

                    if (!$mail->send()) {
                        echo 'Message could not be sent.';
                        echo 'Mailer Error: ' . $mail->ErrorInfo;
                    } else {
                        
                    }

                    echo "<script>alert('Votre message a été envoyé avec succès.')</script>";
                    echo "<script>window.location='contact'</script>";
                } else {
                    echo "<script>alert('Votre message n\'a pas été envoyé !!')</script>";
                    echo "<script>window.location='contact'</script>";
                }
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="formpage">
                    <form method="post"action="">
                        <div class="formpage__content">
                            <div class="formpage__title">
                                <h1>Pour nous écrire,<br />veuillez remplir le formulaire ci-dessous.</h1>
                            </div>
                            <div class="form-group">
                                <div class="form-group__title">Informations personnelles</div>
                                <div class="formgrid formgrid--2col">
                                    <div class="formgrid__item">
                                        <label>Nom</label>
                                        <input type="text" value="" name="nom" required="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Prénom</label>
                                        <input type="text" name="prenom" required="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Email</label>
                                        <input type="email" value="" name="email" required="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Téléphone</label>
                                        <input type="phone" name="tel" required="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Sujet</label>
                                        <div class="custom-select">
                                            <select class="form-control" data-placeholder="" required="" name="sujet">
                                                <option></option>
                                                <option value="demande d'information">Demande d'information</option>
                                                <option <?php
                                                if ((isset($_GET['option'])) && ($_GET['option'] == "demo")) {
                                                    echo "selected";
                                                }
                                                ?> value="demande de démonstration">Demande de démonstration</option>
                                                <option value="problème technique">Problème technique</option>
                                                <option value="problème commercial">Problème commercial</option>
                                                <option value="autre">Autre</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group__title">Message</div>
                                <div class="formgrid">
                                    <div class="formgrid__item">
                                        <label>Indiquez votre demande</label>
                                        <textarea type="text" value="Startup" required="" name="story"class="form-control" placeholder="" rows="10"></textarea>
                                    </div>
                                </div>
                                <div class="formgrid__item">
                                  
                                    <label>Total ? </label>

                                    <input type="text" name="calcul" required="" placeholder="<?php echo $_SESSION['val1'] . " + " . $_SESSION['val2'] . " = ?" ?>" class="form-control" />
                                </div>
                            </div>

                            <p class="small muted">
                                Myfrenchstartup collecte votre nom, prénom, email, téléphone et le contenu de votre message afin de répondre à vos demandes.<br />
                                En cliquant sur « valider », vous reconnaissez avoir lu, compris et accepté nos <a href="https://www.myfrenchstartup.com/conditions-generales-de-ventes" target="_blank">Conditions Générales</a> et notre <a href="https://www.myfrenchstartup.com/charte-de-confidentialite" target="_blank">Charte de confidentialité</a>, et vous consentez au traitement de vos données.<br />
                                Vous disposez d’un droit d’accès vous permettant à tout moment de connaître la nature des données collectées vous concernant, de demander leur rectification ou leur effacement. Ce droit s’exerce par simple envoi d’un email à <a href="mailto:privacy@myfrenchstartup.com">privacy@myfrenchstartup.com</a>.
                            </p>

                            <div class="formpage__actions">
                                <button type="submit" class="btn btn-primary">Valider</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>

        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>