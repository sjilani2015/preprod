<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Test</title>
    </head>
    <body>
        <div class="content">
            <h1 class="title">A Wonderful article behind a paywall</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tortor leo, sollicitudin quis posuere sed, pharetra cursus mauris. Donec ultricies nibh sit amet quam feugiat, vel bibendum nisl pellentesque. In hac habitasse platea dictumst. Sed varius eget ante ac pulvinar. Suspendisse fringilla, quam ac imperdiet consequat, leo massa molestie mi, eget condimentum ligula enim ut mauris. Aliquam egestas malesuada vestibulum. Etiam ut nibh turpis. Fusce mattis blandit bibendum. Vestibulum sodales laoreet lacus ut sollicitudin. Donec tempus iaculis viverra. In congue felis quis sem porta iaculis.</p>
        </div>
        <div id="paywall"></div>


        <script id="poool-access" src="https://assets.poool.fr/access.min.js" async></script>
        <script>
            const script = document.getElementById('poool-access');
            script.addEventListener('load', function () {
                Access
                        .init('UB0IB-OS245-ZNO3H-EH4IZ')
                        .createPaywall({
                            target: '#paywall',
                            content: '.content',
                            mode: 'excerpt',
                            percent: 80,
                            pageType: 'premium',
                        });
            });
        </script>
    </body>
</html>