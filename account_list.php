<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
if (!isset($_SESSION['data_login'])) {
    header("location:" . URL . "/connexion");
    exit();
}
$mon_user = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}

$currentPage = "lists";

function change_date_fr_chaine_related($date) {
    $split1 = explode(" ", $date);
    $split = explode("-", $split1[0]);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Mes listes enregistrées - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
        <style>
            .account__right{
                padding-top: 35px;
                background: #f7fafc !important;
                border: 1px solid #eef2f6 !important;
            }
            .form-control{
                background-color: #fff !important;
                border-color: #eef2f6 !important;
            }
        </style>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>

        <div class="page-wrapper">
            <?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">            
                    <div class="account">
                        <div class="account__aside">
                            <div class="nav-vertical">
                                <div class="nav-vertical__title">
                                    <span class="title">Mon profil</span>
                                </div>
                                <div class="nav-vertical__list">
                                    <?php include ('account/menu.php'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="account__right">

                            <div style="font-family: 'EuclidBold';margin-bottom: 32px;color: #8694A1;">Mes listes enregistrées</div>
                            <div class="tablebloc">
                                <table id="mine3" class="table table-search">
                                    <thead>
                                        <tr style="max-height: unset; min-height: unset; height: unset !important;">
                                            <th class="text-center">Détails segment</th>
                                            <th class="dateCell text-center">Date segment</th>
                                            <th>Nom segment</th>
                                            <th>Notifications</th>
                                            <th class="text-center">Modifier</th>
                                            <th class="text-center">Supprimer</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                        $sql_list = mysqli_query($link, "select date(dt) as ddt,nom,id,total_new from user_save_list where user=" . $mon_user['id'] . " order by dt desc");
                                        while ($data_list = mysqli_fetch_array($sql_list)) {
                                            ?>
                                            <tr style="max-height: unset; min-height: unset; height: unset !important;">
                                                <td class="text-center">
                                                    <a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id($data_list['id']) ?>/<?php echo urlWriting($data_list['nom']); ?>">
                                                        <span style="background: #c6d1dd; display: inline-block; text-align: center; border-radius: 100px; padding: 10px;"><span class="ico-chart" style="margin-right: 0;"></span></span>
                                                    </a>
                                                </td>
                                                <td class="dateCell text-center"><?php echo change_date_fr_chaine_related($data_list['ddt']); ?></td>
                                                <td><a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id($data_list['id']) ?>/<?php echo urlWriting($data_list['nom']); ?>"><?php echo $data_list['nom'] ?></a></td>
                                                <td><?php echo $data_list['total_new'] ?></td>
                                                <td class="text-center">
                                                    <a href="<?php echo URL; ?>/recherche-startups/list/<?php echo generate_id($data_list['id']); ?>">
                                                        <span class="ico-edit"></span>
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    <a style="cursor: pointer" onclick="delete_list(<?php echo $data_list['id']; ?>)">
                                                        <span class="ico-trash"></span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>


                                    </tbody>
                                </table>

                            </div>
                            <div style="font-family: 'EuclidBold';margin-bottom: 32px;color: #8694A1;margin-top: 80px;">Mes listes par défaut</div>
                            <div class="tablebloc">
                                <table id="mine3" class="table table-search">
                                    <thead>
                                        <tr style="max-height: unset; min-height: unset; height: unset !important;">
                                            <th class="text-center">Détails segment</th>
                                            <th class="dateCell text-center">Date segment</th>
                                            <th>Nom segment</th>
                                            <th>Notifications</th>

                                        </tr>
                                    </thead>
                                    <?php
                                    $nb_40 = mysqli_fetch_array(mysqli_query($link, "select total_new from user_save_list where id=38"));
                                    $nb_licorne = mysqli_fetch_array(mysqli_query($link, "select total_new from user_save_list where id=31"));
                                    $nb_120 = mysqli_fetch_array(mysqli_query($link, "select total_new from user_save_list where id=35"));
                                    ?>
                                    <tbody>

                                        <tr style="max-height: unset; min-height: unset; height: unset !important;">
                                            <td class="text-center">
                                                <a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id(38) ?>/NEXT_40">
                                                    <span style="background: #c6d1dd; display: inline-block; text-align: center; border-radius: 100px; padding: 10px;"><span class="ico-chart" style="margin-right: 0;"></span></span>
                                                </a>
                                            </td>
                                            <td class="dateCell text-center"><?php echo change_date_fr_chaine_related($mon_user['date_add']); ?></td>
                                            <td><a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id(38) ?>/NEXT_40">NEXT 40</a></td>
                                            <td><?php echo $nb_40['total_new'] ?></td>

                                        </tr>
                                        <tr style="max-height: unset; min-height: unset; height: unset !important;">
                                            <td class="text-center">
                                                <a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id(35) ?>/FT_120">
                                                    <span style="background: #c6d1dd; display: inline-block; text-align: center; border-radius: 100px; padding: 10px;"><span class="ico-chart" style="margin-right: 0;"></span></span>
                                                </a>
                                            </td>
                                            <td class="dateCell text-center"><?php echo change_date_fr_chaine_related($mon_user['date_add']); ?></td>
                                            <td><a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id(35) ?>/FT_120">FT 120</a></td>
                                            <td><?php echo $nb_120['total_new'] ?></td>

                                        </tr>
                                        <tr style="max-height: unset; min-height: unset; height: unset !important;">
                                            <td class="text-center">
                                                <a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id(31) ?>/startup_licornes">
                                                    <span style="background: #c6d1dd; display: inline-block; text-align: center; border-radius: 100px; padding: 10px;"><span class="ico-chart" style="margin-right: 0;"></span></span>
                                                </a>
                                            </td>
                                            <td class="dateCell text-center"><?php echo change_date_fr_chaine_related($mon_user['date_add']); ?></td>
                                            <td><a target="_blank" href="<?php echo URL ?>/dashboard/<?php echo generate_id(31) ?>/startup_licornes">Startups Licornes</a></td>
                                            <td><?php echo $nb_licorne['total_new'] ?></td>

                                        </tr>



                                    </tbody>
                                </table>

                            </div>
                            <div style="font-family: 'EuclidBold';margin-bottom: 32px;color: #8694A1;margin-top: 80px;">Mes startups favorites</div>
                            <div class="tablebloc">
                                <div class="tablebloc">
                                    <table id="mine2" class="table table-search">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th>Marché</th>
                                                <th>Maturité</th>
                                                <th>Total fonds levés</th>
                                                <th>Région</th>
                                                <th>Traction</th>
                                                <th>Création</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $dataas = mysqli_fetch_array(mysqli_query($link, "select * from user_favoris where user=" . $mon_user['id']));
                                            $details = explode(",", $dataas['liste']);
                                            $nbs = count($details);
                                            for ($i = 0; $i < $nbs; $i++) {
                                                if ($details[$i] != '') {
                                                    $favoris = mysqli_fetch_array(mysqli_query($link, "select * from startup where id=" . $details[$i]));
                                                    $secteurs = mysqli_fetch_array(mysqli_query($link, "Select secteur.nom_secteur,secteur.id,secteur.nom_secteur_en From   secteur Inner Join  activite    On activite.secteur = secteur.id Where  activite.id_startup =" . $favoris['id']));
                                                    $score = mysqli_fetch_array(mysqli_query($link, "Select  * From   startup_score where  id_startup=" . $favoris['id']));
                                                    $tot_lf = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $favoris['id']));

                                                    $ssecteurs = mysqli_fetch_array(mysqli_query($link, "Select  * From  activite  Where  activite.id_startup =" . $favoris['id']));
                                                    $ligne_secteur_activite = mysqli_fetch_array(mysqli_query($link, "select * from secteur_activite where id_secteur=" . $secteurs['id'] . " and id_sous_secteur=" . $idsoussecteur . " and id_activite=" . $idactivite . " and id_sous_activite=" . $idsousactivite));

                                                    if ($ssecteurs['sous_activite'] == "")
                                                        $idsousactivite = 0;
                                                    else
                                                        $idsousactivite = $ssecteurs['sous_activite'];

                                                    if ($ssecteurs['sous_secteur'] == "")
                                                        $idsoussecteur = 0;
                                                    else
                                                        $idsoussecteur = $ssecteurs['sous_secteur'];

                                                    if ($ssecteurs['activite'] == "")
                                                        $idactivite = 0;
                                                    else
                                                        $idactivite = $ssecteurs['activite'];
                                                    ?>
                                                    <tr>
                                                        <th class="table-search__startup">
                                                            <div class="startupInfos">
                                                                <div class="startupInfos__image">
                                                                    <a href="<?php echo URL . '/fr/startup-france/' . generate_id($favoris['id']) . "/" . urlWriting(strtolower($favoris["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $favoris['logo']) ?>" alt="<?php echo "Startup " . $favoris["nom"]; ?>"></a>
                                                                </div>
                                                                <div class="startupInfos__desc">
                                                                    <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($favoris['id']) . "/" . urlWriting(strtolower($favoris["nom"])) ?>" target="_blank"><?php echo stripslashes($favoris['nom']); ?></a></div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <td class="table-search__position center">
                                                            <?php echo ($secteurs['nom_secteur']); ?>
                                                        </td>
                                                        <td class="table-search__position center">
                                                            <?php echo $score['maturite']; ?> 
                                                            <?php if ($score['maturite'] == $ligne_secteur_activite['moy_maturite']) { ?>
                                                                <span class="ico-stable"></span> 
                                                            <?php } ?>
                                                            <?php if ($score['maturite'] > $ligne_secteur_activite['moy_maturite']) { ?>
                                                                <span class="ico-raise"></span> 
                                                            <?php } ?>
                                                            <?php if ($score['maturite'] < $ligne_secteur_activite['moy_maturite']) { ?>
                                                                <span class="ico-decrease"></span> 
                                                            <?php } ?>
                                                        </td>
                                                        <td class="table-search__position center"><?php echo str_replace(",0", "", number_format(($tot_lf['somme'] / 1000), 1, ",", " ")); ?> M€</td>
                                                        <td class="table-search__position center"><?php echo ($favoris['region_new']); ?></td>
                                                        <td class="table-search__position center"><?php echo ($score['traction_likedin']); ?></td>
                                                        <td class="table-search__position center"><?php echo change_date_fr_chaine_related($favoris['date_complete']); ?></td>

                                                    </tr>

                                                    <?php
                                                    $l++;
                                                }
                                            }
                                            ?>

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>