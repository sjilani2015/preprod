<?php
include("../include/db.php");
include("../functions/functions.php");
include("../config.php");

// Include configuration file  
require_once '../paiement-securise/config.php';

// Include the database connection file 
include_once '../paiement-securise/dbConnect.php';
define('type', 'Je suis');
define('type_user_0', 'Entrepreneur');
define('type_user_1', 'Investisseur');
define('type_user_2', 'Autre');
define('type_user_3', 'Media');
define('type_user_4', 'Institutionnel');
define('type_user_5', 'Prestataire');
define('type_user_6', 'Etudiant');
define('type_user_7', 'Enseignant');
define('type_user_10', 'Corporate');
define('type3', 'Autre');
// Fetch plans from the database 
$sqlQ = "SELECT * FROM plans";
$stmt = $db->prepare($sqlQ);
$stmt->execute();
$result = $stmt->get_result();
?>
<!DOCTYPE html>
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>Pricing</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    
    ================================================== -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../paiement-securise/css/style.css">
    <link rel="stylesheet" href="../css/main-color.css" id="colors">
    <link rel="stylesheet" href="../css/design_system.css" id="colors">
    <link rel="stylesheet" href="../file.css" id="colors">

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <style>
        th{
            background: #F6F9FC;
            color: #7E84A3;
            font-size: 1.2rem;
            line-height: 16px;
            border: none !important;
        }
        td{

            font-size: 1.2rem;
            text-align: left;
            line-height: 1.6rem;
            font-family: 'Roboto';
            border: none !important;

        }
        .table-ditance{
            margin-top: 24px;
            margin-left: 8px;
        }
        .pricing-title{
            font-size: 2.6rem;
            line-height: 2.8rem;
            color: #C5D1DC;
            font-family: 'EuclidLight';
            text-align: center;
            margin-bottom: 1.6rem;

        }
        .pricing-h3{
            text-align: center;
            font-size: 1.6rem;
            line-height: 2.4rem;
            color:#2D3B48;
            margin-top: 0px;
            margin-bottom: 48px;
            font-family: 'Euclid';
        }
        .pricing-h1{
            text-align: center;
            font-family: 'EuclidBold';
            font-size:4.8rem;
            line-height: 4.8rem;
            margin-top: 40px;
            margin-bottom: 24px;
            color:#2D3B48;
        }
        .pricing-montant{
            text-align: center;
            font-family: 'EuclidBold';
            font-size: 6.4rem;

            color:#2D3B48;
        }
        .pricing-devise{
            font-family: 'EuclidLight';
        }
        .pricing-cart{
            font-family: 'EuclidBold';
            font-size: 1.2rem;
            line-height: 1.6rem;
            color: #425466;
            text-align: center;
        }
        .pricing-text{
            text-align: center;
            font-family: 'Roboto';
            font-size: 1.6rem;
            line-height: 2rem;
            color: #425466;
            margin-top: 28px;
        }
        .pricing-puce{
            margin-left: 5em;
            text-align: left;
            margin-top: 16px;
            margin-bottom: 24px;
            color: #425466;
            font-size: 1.2rem;
            font-family: 'Roboto';
            font-weight: 100;
            line-height: 24px;
            height: 144px;
        }
        .pricing-check{
            width: 1.2rem;
            margin-right: 8px;
        }
        .pricing-btn{

            background-color: #00CCFF;
            font-weight: 300;
            font-family: 'Roboto';
            border-radius: 5px;
            color: #FFFFFF;
            padding-left: 1.6rem;
            padding-right: 1.6rem;
            padding-top: 1.2rem;
            padding-bottom: 1.2rem;
            font-size: 1.2rem;
            line-height: 1.6rem;
        }
        .bloc-contact{
            padding: 24px;
        }
        .contact-text{
            text-align: center;
            color: #425466;
            font-size: 1.2rem;
            line-height: 1.6rem;
            font-family: 'Roboto';
        }
        .bloc-information{
            background: #EDF1F6;
            margin-top: 40px;
            margin-bottom: 72px;
        }
        .contact-text-span{
            font-family: 'EuclidBold';
            font-size: 1.2rem;
        }
        .contact-grand-question{
            text-align: center;
            padding-top: 24px;
            font-size: 3.2rem;
            color: #2D3B48;
            line-height: 2.4rem;
            font-family: 'EuclidBold';
            margin-bottom: 40px;
            input[type='text']{
                top: 283px;
                left: 300px;
                width: 255px;
                height: 40px;
                background: var(--arrière-plan) 0% 0% no-repeat padding-box;
                border: 1px solid var(--background-1);
                background: #F6F9FC 0% 0% no-repeat padding-box;
                border: 1px solid #D8E2ED;
                opacity: 1;
            }

        </style>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
    </head>

    <body style="background: #F6F9FC;
          font-family: 'Roboto';">


        <?php
        if (isset($_POST['login_connect'])) {

            $login = $_POST['login_connect'];
            $pwd = $_POST['pwd'];

            $nb = mysqli_num_rows(mysqli_query($link, "select * from user where email='" . $login . "' and pwd='" . md5($pwd) . "'"));
            if ($nb == 1) {
                $_SESSION['data_login'] = $login;
            } else {
                mysqli_query($link, "insert into user (email,password,date_add,status,colonne)values('" . $login . "','" . md5($pwd) . "','" . date('Y-m-d H:i:s') . "',1,'6,7,8,9,10,11,12,13,14,15,16,17,18')");
                $_SESSION['data_login'] = $login;
            }
            echo "<script>window.location='https://www.myfrenchstartup.com/myfrenchstartup-premium/investisseur.php'</script>";
        }
        ?>
        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Header Container
            ================================================== -->
            <?php include('header.php'); ?>
            <div class="wrapper">

                <div class="clearfix"></div>

                <h1 class="pricing-h1">Tout l’écosystème des start-ups françaises. Maintenant !</h1>

                <h3 class="pricing-h3">Profitez pleinement du potentiel de MyFrenchStartup en devenant membre Premium.</h3>



                <div class="blob2-courbe" style="margin-bottom: 30px">
                        <div class="espace-bloc-interieur">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td align="left" valign="top" style="padding: 40px 20px 0px 30px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td bgcolor="#ffffff" align="left" style="font-family:Tahoma, Arial, sans-serif;
                                                        font-size: 26px;
                                                        color:#263845;
                                                        padding:0px 0px 20px 0px;
                                                        line-height: 34px; "><strong>Avec l’offre Premium&nbsp;:</strong></td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor="#ffffff" align="left" style="padding:0px 0px 25px 0px;  "><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="25" align="left" valign="top"><img src="https://www.publication.enterprises/FrenchStartUp/Offre_Premium/images/check.jpg" width="23" height="23" alt="" style="display: block"></td>
                                                                        <td bgcolor="#ffffff" align="left" style="font-family:Tahoma, Arial, sans-serif;
                                                                        font-size: 17px;
                                                                        color:#263845;
                                                                        padding:0px 0px 20px 20px;
                                                                        line-height: 25px; "><span style="color: #07CAFB;
                                                                            font-size: 19px;
                                                                            font-weight: bold;">Créez votre tableau de bord personnalisé</span> pour suivre les&nbsp;tendances  et les évolutions de vos marchés cible ou de vos start-ups à suivre</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="25" align="left" valign="top"><img src="https://www.publication.enterprises/FrenchStartUp/Offre_Premium/images/check.jpg" width="23" height="23" alt="" style="display: block"></td>
                                                                        <td bgcolor="#ffffff" align="left" style="font-family:Tahoma, Arial, sans-serif;
                                                                        font-size: 17px;
                                                                        color:#263845;
                                                                        padding:0px 0px 20px 20px;
                                                                        line-height: 25px; "><span style="color: #07CAFB;
                                                                            font-size: 19px;
                                                                            font-weight: bold;">Recevez vos alertes en temps réel </span> sur les mouvements de&nbsp;marché ou l’évolution de votre liste de start-ups préférées</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="25" align="left" valign="top"><img src="https://www.publication.enterprises/FrenchStartUp/Offre_Premium/images/check.jpg" width="23" height="23" alt="" style="display: block"></td>
                                                                        <td bgcolor="#ffffff" align="left" style="font-family:Tahoma, Arial, sans-serif;
                                                                        font-size: 17px;
                                                                        color:#263845;
                                                                        padding:0px 0px 20px 20px;
                                                                        line-height: 25px; "><span style="color: #07CAFB;
                                                                            font-size: 19px;
                                                                            font-weight: bold;">Profitez de recherches illimitées</span> pour découvrir start-up et&nbsp;investisseurs</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="25" align="left" valign="top"><img src="https://www.publication.enterprises/FrenchStartUp/Offre_Premium/images/check.jpg" width="23" height="23" alt="" style="display: block"></td>
                                                                        <td bgcolor="#ffffff" align="left" style="font-family:Tahoma, Arial, sans-serif;
                                                                        font-size: 17px;
                                                                        color:#263845;
                                                                        padding:0px 0px 20px 20px;
                                                                        line-height: 25px; "><span style="color: #07CAFB;
                                                                            font-size: 19px;
                                                                            font-weight: bold;">Accédez au deal flow</span> des start-ups à la recherche de fonds</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="25" align="left" valign="top"><img src="https://www.publication.enterprises/FrenchStartUp/Offre_Premium/images/check.jpg" width="23" height="23" alt="" style="display: block"></td>
                                                                        <td bgcolor="#ffffff" align="left" style="font-family:Tahoma, Arial, sans-serif;
                                                                        font-size: 17px;
                                                                        color:#263845;
                                                                        padding:0px 0px 20px 20px;
                                                                        line-height: 25px; "><span style="color: #07CAFB;
                                                                            font-size: 19px;
                                                                            font-weight: bold;">Identifiez les investisseurs de vos marchés </span>et&nbsp;l’historique de leurs investissements</td>
                                                                </tr>
                                                            </tbody>
                                                        </table></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table>



                        <div style="font-family:Tahoma, Arial, sans-serif;
                             font-size: 17px;
                             color:#263845;
                             padding:0px 0px 20px 0px;
                             line-height: 25px;
                             margin-top: 25px ">Pour plus d’information, <a href="https://www.myfrenchstartup.com/contact" style="color:#07CAFB" target="_blank">contactez-nous</a></div>
                        </div>
                        <div class="espace-bloc-interieur">
                            <?php
                            if (!isset($_SESSION['data_login'])) {
                                ?>
                                <div class="signin">

                                    <div class="signin__content">
                                        <div class="signin__login shown">
                                            <form method="post" action="">
                                                <h3>Merci de finaliser votre inscription pour confirmer votre abonnement</h3>
                                                <div class="alert alert--error" id="msg_error_mail" style="display: none">
                                                    Fomrat mail non valide
                                                </div>
                                                <div class="alert alert--error" id="msg_error_user" style="display: none">
                                                    Veuillez vérifier vos coordonnées !
                                                </div>
                                                <div class="alert alert--error" id="msg_error_user_block" style="display: none">
                                                    Votre compte est inactif !
                                                </div>
                                                <div class="alert alert--success" style="display: none">
                                                    Success
                                                </div>
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input class="form-control" name="login_connect" type="email" value="" required="" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Mot de passe</label>
                                                    <input class="form-control" name="pwd" type="password" value="" required="" />
                                                </div>

                                                <div class="form-group text-center">
                                                    <button class="btn btn-primary" type="submit">Confirmer</button>
                                                </div>

                                            </form>
                                        </div>

                                    </div>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="titre-overview" style="text-align: left;">Paiement sécurisé via Stripe</div>
                            <div class="">
                                <div class="panel-heading">

                                    <!-- Plan Info -->
                                    <div>
                                        <b>Sélectionnez un abonnement mensuel ou annuel</b>
                                        <select id="subscr_plan">
                                            <?php
                                            if ($result->num_rows > 0) {
                                                while ($row = $result->fetch_assoc()) {
                                                    ?>
                                                    <option value="<?php echo $row['id']; ?>"><?php echo $row['name'] . ' [' . $row['price'] . '€/' . $row['interval'] . ']'; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <!-- Display status message -->
                                    <div id="paymentResponse" class="hidden"></div>

                                    <!-- Display a subscription form -->
                                    <form id="subscrFrm">
                                        <div class="form-group">
                                            <label>Titulaire de la carte</label>
                                            <input type="text" id="name" class="form-control" placeholder="Titulaire de la carte" required="" autofocus="">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" id="email" class="form-control" placeholder="Email" required="">
                                        </div>

                                        <div class="form-group">
                                            <label>Informations de la carte</label>
                                            <div id="card-element">
                                                <!-- Stripe.js will create card input elements here -->
                                            </div>
                                        </div>

                                        <!-- Form submit button -->
                                        <button id="submitBtn" class="btn pricing-btn" style="margin-top: 24px">
                                                <div class="spinner hidden" id="spinner"></div>
                                                <span id="buttonText">Procéder au paiement</span>
                                            </button>
                                        </form>

                                        <!-- Display processing notification -->
                                        <div id="frmProcess" class="hidden">
                                            <span class="ring"></span> Processing...
                                        </div>
                                    </div>
                                </div>
                                <div style="text-align: center;
                                margin-top: 30px;"><img src="../images/logo-stripe-secure.png" alt="" style="width: 170px;"></div>
                                <?php
                            }
                            ?>
                    </div>
                </div>



                <div class="bloc-chart">




                </div>









                <div id="backtotop"><a href="#"></a></div>
                    <?php include('footer1.php'); ?>
            </div>

        </div>
        <!-- Wrapper / End -->









        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>



        <script>
                                                    $(document).ready(function () {
                                                        $("#search-box").keyup(function () {
                                                            $.ajax({
                                                                type: "POST",
                                                                url: "<?php echo url ?>readCountry.php",
                                                                data: 'keyword=' + $(this).val(),
                                                                beforeSend: function () {
                                                                    $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                                                                },
                                                                success: function (data) {
                                                                    $("#suggesstion-box").show();
                                                                    $("#suggesstion-box").html(data);
                                                                    $("#search-box").css("background", "#FFF");
                                                                }
                                                            });
                                                        });


                                                    });

                                                    function selectCountry(val) {
                                                        const words = val.split('/');
                                                        $("#search-box").val(words[2]);
                                                        $("#suggesstion-box").hide();
                                                        window.location = '<?php echo url ?>' + val;
                                                    }
                                                    function selectInvest(val) {
                                                        const words = val.split('/');
                                                        $("#search-box").val(words[2]);
                                                        $("#suggesstion-box").hide();
                                                        window.location = '<?php echo url ?>' + val;
                                                    }
                                                    function selectEntrepreneur(val) {
                                                        const words = val.split('/');
                                                        $("#search-box").val(words[2]);
                                                        $("#suggesstion-box").hide();
                                                        window.location = '<?php echo url ?>' + val;
                                                    }
                                                    function selectTags(val) {
                                                        const words = val.split('/');
                                                        $("#search-box").val(words[2]);
                                                        $("#suggesstion-box").hide();
                                                        window.location = '<?php echo url ?>' + val;
                                                    }
                                                    function selectRegion(val) {
                                                        const words = val.split('/');
                                                        $("#search-box").val(words[2]);
                                                        $("#suggesstion-box").hide();
                                                        window.location = '<?php echo url ?>' + val;
                                                    }





        </script>

        <script src="../paiement-securise/js/checkout.js" STRIPE_PUBLISHABLE_KEY="<?php echo STRIPE_PUBLISHABLE_KEY; ?>" defer></script>
        <?php
        mysqli_query($link, "insert into acces_paiement(user,dt)values('" . $_SESSION['data_login'] . "','" . date('Y-m-d H:i:s') . "')");
        ?>
    </body>
</html>