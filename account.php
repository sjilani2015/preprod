<?php
//include("include/db.php");
//include("functions/functions.php");
include ('config.php');

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        
        <div class="page-wrapper">
            <?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">            
                    <div class="account">
                        <div class="account__aside">
                            <div class="nav-vertical">
                                <div class="nav-vertical__title">
                                    <span class="title">Mon profil</span>
                                </div>
                                <div class="nav-vertical__list">
                                    <ul>
                                        <li class="active"><a href="#">Informations personnelles</a></li>
                                        <li><a href="#">Mes factures</a></li>
                                        <li><a href="#">Changer mot de passe</a></li>
                                        <li><a href="#">Mon compte</a></li>
                                        <li><a href="#">Déconnexion</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="account__right">
                            <div class="form-group">
                                <label>Civilité</label>
                                <div class="custom-radio-wrapper">
                                    <label class="custom-radio">
                                        Monsieur
                                        <input type="radio" checked="checked" name="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="custom-radio">
                                        Madame
                                        <input type="radio" checked="checked" name="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="formgrid">
                                    <div class="formgrid__item">
                                        <label>Nom</label>
                                        <input type="text" value="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Prénom</label>
                                        <input type="text" value="" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="formgrid">
                                    <div class="formgrid__item">
                                        <label>Email</label>
                                        <input type="text" value="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Société</label>
                                        <input type="text" value="" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="formgrid">
                                    <div class="formgrid__item">
                                        <label>Fonction</label>
                                        <input type="text" value="" class="form-control" />
                                    </div>
                                    <div class="formgrid__item">
                                        <label>Vous êtes</label>
                                        <input type="text" value="" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label-big">Vos données et notre charte de confidentialité</label>
                                <div><a href="<?php echo URL; ?>/charte-de-confidentialite.php" target="_blank">Voir la charte de confidentialité Myfrenchstartup.com</a></div>
                            </div>
                            <div class="form-group form-group--btn">
                                <button class="btn btn-primary">Enregistrer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
    </body>
</html>