<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
if (isset($_SESSION['data_login'])) {
    header('location:' . URL . '/recherche-startups');
}

function generateRandomString($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>


        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">            
                    <div class="signin">

                        <div class="signin__content">
                            <div class="signin__login shown">
                                <form method="post" action="">
                                    <h1>Mot de passe <br />oublié ?</h1>
                                    <p></p>
                                    <div class="alert alert--error" id="msg_error_mail" style="display: none">
                                        Fomrat mail non valide
                                    </div>
                                    <div class="alert alert--error" id="msg_error_user" style="display: none">
                                        Adresse email inexistante  !
                                    </div>
                                    <div class="alert alert--error" id="msg_error_user_block" style="display: none">
                                        Votre compte est inactif !
                                    </div>
                                    <div class="alert alert--success" id="msg_success_user_email" style="display: none">
                                        Un email contenant votre mot de passe vous a été transmi, merci de vérifier votre boite email.
                                    </div>
                                    <div class="form-group ">
                                        <label>Email</label>
                                        <input class="form-control" name="login_connect" type="email" value="" required="" />
                                    </div>


                                    <div class="form-group text-center">
                                        <button class="btn btn-primary" type="submit" >Envoyer</button>
                                    </div>

                                </form>
                                <?php
                                if (isset($_POST['login_connect'])) {
                                    $email = addslashes($_POST['login_connect']);

                                    $verif = mysqli_query($link, "select * from user where email='" . $email . "'");
                                    $nb = mysqli_num_rows($verif);
                                    if ($nb == 0) {
                                        echo '<script>document.getElementById("msg_error_user").style.display="block";</script>';
                                    } else {

                                        $password = generateRandomString(6);
                                        mysqli_query($link, "update user set token_oublie='" . $password . "',dt_oublie='".date('Y-m-d')."' where email='" . $email . "'");
                                        $user = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $email . "'"));
                                        /*
                                         * Mail
                                         */

                                        $message = '<html>
    <head>
        <title>Mot de pass oublié</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="robots" content="noindex">
    </head>
    <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <!-- Save for Web Slices (myfs.jpg) -->
        <table align="center" id="Table_01" width="600" height="394" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/mailing/images/myfs_01.jpg" width="600" height="81" alt=""></td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/mailing/images/myfs_02.jpg" width="600" height="30" alt=""></td>
            </tr>
            <tr>
                <td width="600" bgcolor="#ffffff">';
                                        $message .= 'Bonjour ' . stripslashes($user['prenom']) . ',<br /><br />Vous avez demandé de modifier votre mot de passe.<br /><br />Pour modifier votre mot de passe, veuillez <a href="' . URL . '/reinit.php?token=' . $password . '">cliquer ici</a> ou bien copier / coller le lien suivant dans votre navigateur :<br />' . URL . '/reinit.php?token=' . $password;

                                        $message .= '</td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/mailing/images/myfs_04.jpg" width="600" height="59" alt=""></td>
            </tr>
            <tr>
                <td>
                    <img src="https://www.myfrenchstartup.com/mailing/images/myfs_05.jpg" width="600" height="48" alt=""></td>
            </tr>
        </table>
    </body>
</html>';

                                        require 'phpmailer/PHPMailerAutoload.php';

                                        $mail = new PHPMailer();

                                        $mail->IsSMTP();
                                        $mail->Host = 'ssl0.ovh.net';  // Specify main and backup SMTP servers
                                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                                        $mail->Username = 'inscription@myfrenchstartup.com';                 // SMTP username
                                        $mail->Password = '04Betterway#p';                           // SMTP password
                                        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                                        $mail->Port = 587;                               // TCP port to connect to

                                        $mail->setFrom('inscription@myfrenchstartup.com','myFrenchStartup');
                                        $mail->addAddress($email);     // Add a recipient


                                        $mail->isHTML(true);                                  // Set email format to HTML

                                        $mail->Subject = "Re-initialisation de mot de passe";

                                        $mail->Body = utf8_decode($message);

                                        if (!$mail->send()) {
                                            echo 'Message could not be sent.';
                                            echo 'Mailer Error: ' . $mail->ErrorInfo;
                                        } else {
                                            
                                        }




                                        echo '<script>
                                            document.getElementById("msg_success_user_email").style.display="block";
                                         </script>';
                                        /*
                                         * Mail
                                         */
                                    }
                                }
                                ?>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');

            function checkEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            function connexion() {
                document.getElementById("msg_error_mail").style.display = "none";
                document.getElementById("msg_error_user").style.display = "none";
                document.getElementById("msg_error_user_block").style.display = "none";
                var login = document.getElementById("login_connect").value;
                var pwd = document.getElementById("pwd").value;
                if (checkEmail(login)) {

                    $.ajax({
                        type: "POST",
                        url: "<?php echo URL ?>/verif_user.php?login=" + login + "&pwd=" + pwd,

                        success: function (data) {
                            var t = eval(data);
                            if (t[0].result == 0) {
                                document.getElementById("msg_error_user").style.display = "block";
                            } else {
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo URL ?>/verif_user_block.php?login=" + login + "&pwd=" + pwd,

                                    success: function (data1) {
                                        var t = eval(data1);
                                        if (t[0].result == 0) {
                                            document.getElementById("msg_error_user_block").style.display = "block";
                                        } else {
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo URL ?>/redirection.php?login=" + login + "&pwd=" + pwd,

                                                success: function (data2) {

                                                    window.location = data2;


                                                }
                                            });
                                        }


                                    }
                                });
                            }


                        }
                    });






                } else {
                    document.getElementById("msg_error_mail").style.display = "block";
                }

            }
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>