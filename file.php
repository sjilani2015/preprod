<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

$monUrl = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$_SESSION['utm_source'] = $monUrl;

$users = mysqli_fetch_array(mysqli_query($link, "select premium,id from user where email='" . $_SESSION['data_login'] . "'"));

/*
 * Test de quota
 */
if (isset($_SESSION['data_login'])) {
    if ($users['premium'] == 0) {
        mysqli_query($link, "update user set vu=vu+1 where id=" . $users['id']);
        mysqli_query($link, "insert into user_vu(user,dt)values(" . $users['id'] . ",'" . date('Y-m-d') . "')");
        $verif_vu = mysqli_num_rows(mysqli_query($link, "select id from user_vu where user=" . $users['id'] . " and dt='" . date('Y-m-d') . "'"));
        if ($verif_vu > 10) {
            echo '<script>window.location="' . URL . '/expiration-offre"</script>';
        }
    }
}
$langage = stripos($_SERVER['SCRIPT_URI'], '/en/');
if ($langage) {
    $lang = "en";
} else
    $lang = "fr";

//require_once("lang/" . $lang . ".php");
$_SESSION["myfs_language"] = $lang;
/*
 * Récupérer id Startup
 */
//$_GET["code"] = 44449;
//$_GET["code"] = 285349;
if (isset($_GET["startup"]) && $_GET["startup"] != '') {
    $startup_id = $_GET["startup"];
    $code = $_GET["code"];
} else
    header('location:' . URL . '/404');


$code = $_GET["code"];
$id_startup = degenerate_id($code);
$verifs = mysqli_num_rows(mysqli_query($link, "select id from startup where id=" . $id_startup . " and status=1"));
if ($verifs != 1) {
    header('location:' . URL . '/404');
}

$_SESSION['id_sup'] = $id_startup;

$startup_capital = getStartupCapital($id_startup);
$nb_evolution = mysqli_num_rows(mysqli_query($link, "select id from startup_capital where id_startup=" . $id_startup));

$startup_ca = getStartupCA($id_startup);

$ca1 = str_replace(array("€", " ", ","), array("", "", "."), $startup_ca[0]["ca"]);
$ca2 = str_replace(array("€", " ", ","), array("", "", "."), $startup_ca[1]["ca"]);
$anneeca1 = $startup_ca[0]["annee"];
$anneeca2 = $startup_ca[1]["annee"];

$row_fondateur_startup = getFondateurByIdStartup($id_startup);

$startup = getStartupById($id_startup);

$_SESSION['startup_nom_revendiquer'] = $startup[0]['nom'];
$_SESSION['startup_id_revendiquer'] = $startup[0]['id'];

// echo strlen(substr($descriptionfr, 0, 155));
$row_nom_secteur = getSecteurByIdStartup($startup[0]['id']);
if ($row_nom_secteur[0]['id'] != "") {
    $row_nom_sous_secteur = getSousSecteurByIdStartup($startup[0]['id']);
}


$id_secteur = mysqli_fetch_array(mysqli_query($link, "Select  secteur.id,secteur.nom_secteur,secteur.nom_secteur_en From secteur inner join activite on activite.secteur=secteur.id Where  activite.id_startup =" . $startup[0]['id']));
$id_sous_secteur = mysqli_fetch_array(mysqli_query($link, "Select  sous_secteur.id,sous_secteur.nom_sous_secteur,sous_secteur.nom_sous_secteur_en From  sous_secteur inner join activite on activite.sous_secteur=sous_secteur.id Where  activite.id_startup =" . $startup[0]['id']));
$id_sous_activite = mysqli_fetch_array(mysqli_query($link, "Select  last_sous_activite.id,last_sous_activite.nom_sous_activite From  last_sous_activite inner join activite on activite.sous_activite=last_sous_activite.id Where  activite.id_startup =" . $startup[0]['id']));
$id_activite = mysqli_fetch_array(mysqli_query($link, "Select  last_activite.id,last_activite.nom_activite,last_activite.nom_activite_en From  last_activite inner join activite on activite.activite=last_activite.id Where  activite.id_startup =" . $startup[0]['id']));

if ($id_sous_activite['id'] == "")
    $idsousactivite = 0;
else
    $idsousactivite = $id_sous_activite['id'];

if ($id_sous_secteur['id'] == "")
    $idsoussecteur = 0;
else
    $idsoussecteur = $id_sous_secteur['id'];

if ($id_activite['id'] == "")
    $idactivite = 0;
else
    $idactivite = $id_activite['id'];


$ligne_secteur_activite = mysqli_fetch_array(mysqli_query($link, "select * from secteur_activite where id_secteur=" . $id_secteur['id'] . " and id_sous_secteur=" . $idsoussecteur . " and id_activite=" . $idactivite . " and id_sous_activite=" . $idsousactivite));
$ligne_secteur_secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur_secteur where id_secteur=" . $id_secteur['id']));
$ligne_france = mysqli_fetch_array(mysqli_query($link, "select * from secteur_france where 1"));

$somme_global_competition = mysqli_fetch_array(mysqli_query($link, "select sum(competition_secteur) as somme from secteur_activite"));
$nb_global_competition = mysqli_num_rows(mysqli_query($link, "select competition_secteur from secteur_activite where competition_secteur!=0"));
$moy_competition_secteur = $somme_global_competition['somme'] / $nb_global_competition;

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}
function change_date_fr_chaine_compl($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $jour." ".$mm . " " . $annee;
    return $creation;
}

$chiffre = mysqli_fetch_array(mysqli_query($link, "select * from startup_score where id_startup=" . $startup[0]['id']));
if ($chiffre['maturite'] != "") {
    $maturite = $chiffre['maturite'];
    $matirute_courbe = $chiffre['maturite'];
} else {
    $maturite = "-";
    $matirute_courbe = 0;
}
if ($chiffre['anciennete'] != "") {
    $anciennete = $chiffre['anciennete'];
    $anciennete_courbe = $chiffre['anciennete'];
} else {
    $anciennete = "-";
    $anciennete_courbe = 0;
}
if ($chiffre['traction_likedin'] != "") {
    $traction = $chiffre['traction_likedin'];
    $traction_courbe = $chiffre['traction_likedin'];
} else {
    $traction = "-";
    $traction_courbe = 0;
}


$menu = 2;
?>
<?php
$tab_somme_total_lf = getTotalLfByStartup($startup[0]["id"]);

$tab_last_deal = getLastDealByStartup($startup[0]["id"]);
$row_total_investisseur = getTotalInvestisseurByStartup($startup[0]["id"]);
if (!empty($row_total_investisseur)) {
    $ch_invest = "";
    foreach ($row_total_investisseur as $deal) {
        $ch_invest .= stripslashes($deal['de']) . ",";
    }
}

$nb_founder = mysqli_num_rows(mysqli_query($link, "select id from personnes where id_startup=" . $startup[0]["id"]));

$tab_invest = explode(",", $ch_invest);

$verif_rachat = mysqli_num_rows(mysqli_query($link, "select id from lf where id_startup=" . $startup[0]["id"] . " and rachat=1 "));
$verif_ipo = mysqli_num_rows(mysqli_query($link, "select id from lf where id_startup=" . $startup[0]["id"] . " and rachat=2 "));

$tot_leve_france = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf inner join startup on startup.id=lf.id_startup inner join activite on startup.id=activite.id_startup  where startup.status=1 and lf.rachat=0 and activite.secteur=" . $row_nom_secteur[0]['id'] . ""));
$tot_nb_france = mysqli_fetch_array(mysqli_query($link, "select count(*) as nb from lf inner join startup on startup.id=lf.id_startup inner join activite on startup.id=activite.id_startup  where startup.status=1 and lf.rachat=0 and montant>0 and montant!='NC' and activite.secteur=" . $row_nom_secteur[0]['id'] . ""));

$moy_france = $tot_leve_france['somme'] / $tot_nb_france['nb'];

$purcentage_lf = ($tab_somme_total_lf[0]["somme"] * 100) / $moy_france;

$descriptionfr = $startup[0]['long_fr'];

$sql_list_personnes = mysqli_query($link, "select * from personnes where id_startup=" . $startup[0]['id'] . " and nom !='' and etat=1 order by personnes.id");
$nb_personnes = mysqli_num_rows($sql_list_personnes);
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>



        <link rel="image_src" href="<?php
        if ($startup[0]['couverture'] != '')
            echo url . 'couverture/' . $startup[0]['couverture'];
        else
            echo url . str_replace("../", "", $startup[0]['logo'])
            ?>">
        <meta property="og: " content="<?php if ($_SESSION['myfs_language'] == "fr") { ?>Startup <?php echo stripslashes($startup[0]['nom']); ?>,  <?php echo stripslashes(utf8_encode($startup[0]['short_fr'])); ?><?php } else { ?>Startup <?php echo stripslashes($startup[0]['nom']); ?>, <?php echo stripslashes($startup[0]['short_en']); ?><?php } ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="<?php echo $monUrl; ?>" />
        <meta property="og:image" content="<?php
        if ($startup[0]['couverture'] != '')
            echo url . 'couverture/' . $startup[0]['couverture'];
        else
            echo url . str_replace("../", "", $startup[0]['logo'])
            ?>" />
              <?php if ($_SESSION['myfs_language'] == "fr") { ?>
            <meta property="og:description" content="<?php echo utf8_decode(strip_tags(substr($descriptionfr, 0, 100))) ?> .Le fondateur de <?php echo utf8_decode(strip_tags($startup[0]['nom'])) ?> est <?php echo utf8_decode(strip_tags($row_fondateur_startup[0]['prenom'])) . ' ' . utf8_decode(strip_tags($row_fondateur_startup[0]['nom'])); ?>" />
        <?php } else { ?>
            <meta property="og:description" content="<?php echo strip_tags(substr($descriptionEn, 0, 100)) . ' .The founder of ' . strip_tags($startup[0]['nom']) . ' is ' . strip_tags($row_fondateur_startup[0]['prenom']) . ' ' . strip_tags($row_fondateur_startup[0]['nom']); ?>" />
        <?php } ?>
        <meta property="og:site_name" content="MyFrenchStartup" />
        <meta property="og:locale" content="<?php echo strtolower($_SESSION["myfs_language"]) . '_' . strtoupper($_SESSION["myfs_language"]); ?>" />
        <meta property="fb:page_id" content="384072774988229" />

        <meta name="twitter:card" content="summary">
        <meta name="twitter:image:alt" content="<?php echo stripslashes($startup[0]['twitter']); ?>">
        <meta name="twitter:site" content="@myfrenchstartup">
        <meta name="twitter:creator" content="<?php echo stripslashes($startup[0]['twitter']); ?>">
        <meta name="twitter:title" content="<?php if ($_SESSION['myfs_language'] == "fr") { ?> Startup <?php echo stripslashes($startup[0]['nom']); ?>,<?php echo stripslashes(utf8_encode($startup[0]['short_fr'])); ?><?php } else { ?>Startup <?php echo stripslashes($startup[0]['nom']); ?>, <?php echo stripslashes($startup[0]['short_en']); ?><?php } ?>"><?php if ($_SESSION['myfs_language'] == "fr") { ?>
            <meta name="twitter:description" content="<?php echo strip_tags(substr($descriptionfr, 0, 100)) ?> .Le fondateur de <?php echo $startup[0]['nom'] ?> est <?php echo $row_fondateur_startup[0]['prenom'] . ' ' . $row_fondateur_startup[0]['nom']; ?>">
            <?php
        } else {
            ?>

            <meta name="twitter:description" content="<?php echo strip_tags(substr($descriptionEn, 0, 100)) ?> .The founder of <?php echo $startup[0]['nom'] ?> is <?php echo $row_fondateur_startup[0]['prenom'] . ' ' . $row_fondateur_startup[0]['nom']; ?>">
            <?php
        }
        ?>
        <meta name="twitter:image" content="<?php
        if ($startup[0]['couverture'] != '')
            echo url . 'couverture/' . $startup[0]['couverture'];
        else
            echo url . str_replace("../", "", $startup[0]['logo'])
            ?>">
        <title><?php if ($_SESSION['myfs_language'] == "fr") { ?>Startup <?php echo stripslashes($startup[0]['nom']); ?> <?php echo stripslashes(utf8_encode($startup[0]['short_fr'])); ?><?php } else { ?>French Startup <?php echo stripslashes($startup[0]['nom']); ?> <?php echo stripslashes($startup[0]['short_en']); ?><?php } ?></title>

        <?php if ($_SESSION['myfs_language'] == "fr") { ?>
            <meta name="Description" content="<?php echo strip_tags(substr($descriptionfr, 0, 100)) ?> .Le fondateur de <?php echo $startup[0]['nom'] ?> est <?php echo $row_fondateur_startup[0]['prenom'] . ' ' . $row_fondateur_startup[0]['nom']; ?>">
        <?php } else { ?>
            <meta name="Description" content="<?php echo strip_tags(substr($descriptionEn, 0, 100)) ?> .The founder of <?php echo $startup[0]['nom'] ?> is <?php echo $row_fondateur_startup[0]['prenom'] . ' ' . $row_fondateur_startup[0]['nom']; ?>">
        <?php } ?>
        <meta name="Keywords" content="<?php echo stripslashes($startup[0]['nom']); ?> <?php echo utf8_encode($startup[0]['short_fr']); ?>" />
        <link rel="icon" type="image/ico" href="https://www.myfrenchstartup.com/static/images/myfs.ico">
        <meta itemprop="inLanguage" content="<?php echo strtolower($_SESSION["myfs_language"]) . '_' . strtoupper($_SESSION["myfs_language"]); ?>">
        <meta itemprop="copyrightHolder" content="MyFrenchStartup" />
        <meta itemprop="copyrightYear" content="<?php echo date('Y'); ?>" />
        <meta itemprop="provider" content="MyFrenchStartup">





        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script>am4core.addLicense("CH303325752");</script>

        <script>
            const   themeColorBlue = am4core.color('#00bff0'),
                    themeColorFill = am4core.color('#E1F2FA'),
                    themeColorGrey = am4core.color('#dadada'),
                    themeColorLightGrey = am4core.color('#F5F5F5'),
                    themeColorDarkgrey = am4core.color("#33333A"),
                    themeColorLine = am4core.color("#87D6E3"),
                    themeColorLineRed = am4core.color("#FF7C7C"),
                    themeColorLineGreen = am4core.color("#21D59B"),
                    themeColorColumns = am4core.color("#E1F2FA"),
                    labelColor = am4core.color('#798a9a');
        </script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
        <style>
            #poool-widget {
                position: relative;
            }

            #poool-widget::before {
                position: absolute;
                background: transparent;
                background-image: -webkit-gradient(linear,left top,left bottom,from(rgba(255,255,255,0.05)),to(rgba(255,255,255,1)));
                background-image: linear-gradient(to bottom,rgba(255,255,255,0.05),rgba(255,255,255,1));
                background-position: center top;
                top: -110px;
                content: '';
                width: calc(100% + 10px);
                height: 110px;
            }
        </style>
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <body class="preload page">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MR2VSZB"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">

                    <section class="module">
                        <div class="module__title"><?php echo ($startup[0]['nom']); ?>
                            <?php if ($startup[0]['sup_radie'] == 1) { ?>
                                <br>  <sub style="color: #ff7a7a;font-size: 12px; padding-left: 10px;">Startup radiée  <?php if ($startup[0]['dt_radie'] != '0000-00-00 00:00:00') echo "depuis " . change_date_fr_chaine_related($startup[0]['dt_radie']); ?></sub>
                            <?php } ?>
                            <?php
                            $sql_rachat = mysqli_query($link, "select * from lf where rachat=1 and id_startup=" . $startup[0]['id'] . " limit 1");
                            $nb_rachat = mysqli_num_rows($sql_rachat);

                            if ($nb_rachat > 0) {
                                $rachete = mysqli_fetch_array($sql_rachat);
                                ?>
                                <br>  <sub style="color: #30cafd;font-size: 12px; padding-left: 10px;">Startup rachetée par <?php echo strtoupper($rachete['de']); ?> depuis <?php echo change_date_fr_chaine_related($rachete['date_ajout']); ?></sub>
                            <?php } ?>
                            <?php
                            $sql_ipo = mysqli_query($link, "select * from lf where rachat=2 and id_startup=" . $startup[0]['id'] . " limit 1");
                            $nb_ipo = mysqli_num_rows($sql_ipo);

                            if ($nb_ipo > 0) {
                                $ipo= mysqli_fetch_array($sql_ipo);
                                ?>
                                <br>  <sub style="color: #30cafd;font-size: 12px; padding-left: 10px;">Entrée en bourse le <?php echo change_date_fr_chaine_compl($ipo['date_ajout']); ?></sub>
                            <?php } ?>
                        </div>
                        <div class="tags">
                            <a href="<?php echo URL ?>/etude-secteur/<?php echo generate_id($row_nom_secteur[0]['id']) . "/" . urlWriting($row_nom_secteur[0]['nom_secteur']) ?>" title="" class="tags__el tags__el--pinky">
                                <span class="ico-chart"></span> <?php echo utf8_encode($row_nom_secteur[0]["nom_secteur"]); ?>
                            </a>
                            <?php
                            if ($startup[0]['region_new'] != '') {
                                $get_id_region = mysqli_fetch_array(mysqli_query($link, "select * from region_new where region_new='" . addslashes(utf8_encode($startup[0]['region_new'])) . "'"));
                                ?>
                                <a href="<?php echo URL . '/region/' . generate_id($get_id_region['id']) . "/" . urlWriting(strtolower($get_id_region['region_new'])) ?>" title="" target="_blank" class="tags__el tags__el--pinky">
                                    <span class="ico-chart"></span> <?php echo utf8_encode($startup[0]['region_new']) ?>
                                </a>
                                <?php
                            }
                            ?>
                            <?php
                            if ($startup[0]['ville'] != '') {
                                ?>
                                <a href="<?php echo URL; ?>/recherche-startups/tags/<?php echo addslashes(str_replace(array("-", " ", "&", "é", "è", "ê", "î", "ï", "ô"), array("__", "_", "..", "e", "e", "e", "i", "i", "o"), utf8_encode($startup[0]['ville']))); ?>" title="" class="tags__el">
                                    <span class="ico-tag"></span> <?php echo utf8_encode($startup[0]['ville']); ?>
                                </a>
                                <?php
                            }
                            ?>
                            <?php
                            if ($id_sous_secteur["nom_sous_secteur"] != "") {
                                ?>
                                <a href="<?php echo URL ?>/recherche-startups/sous-secteur-activite/<?php echo $id_sous_secteur["id"]; ?>/<?php echo urlWriting($id_sous_secteur["nom_sous_secteur"]); ?>" title="" class="tags__el">
                                    <span class="ico-tag"></span> <?php echo ($id_sous_secteur["nom_sous_secteur"]); ?>
                                </a>
                                <?php
                            }
                            ?>
                            <?php /*
                              if ($id_activite["nom_activite"] != "") {
                              ?>
                              <a href="#" title="" class="tags__el">
                              <span class="ico-tag"></span> <?php echo ($id_activite["nom_activite"]); ?>
                              </a>
                              <?php
                              }
                              ?>
                              <?php
                              if ($id_sous_activite["nom_sous_activite"] != "") {
                              ?>
                              <a href="#" title="" class="tags__el">
                              <span class="ico-tag"></span> <?php echo ($id_sous_activite["nom_sous_activite"]); ?>
                              </a>
                              <?php
                              } */
                            ?>

                            <?php
                            $concat_tag_tab = explode(",", $startup[0]['concat_tags']);
                            $nb_tagss = count($concat_tag_tab);

                            for ($ii = 0; $ii < $nb_tagss; $ii++) {
                                if ($concat_tag_tab[$ii] != "") {
                                    ?>
                                    <a href="<?php echo URL; ?>/recherche-startups/tags/<?php echo addslashes(str_replace(array("-", " ", "&", "é", "è", "ê", "î", "ï", "ô"), array("__", "_", "..", "e", "e", "e", "i", "i", "o"), utf8_encode($concat_tag_tab[$ii]))); ?>" target="_blank" title="" class="tags__el">
                                        <span class="ico-tag"></span> <?php echo utf8_encode($concat_tag_tab[$ii]); ?>
                                    </a>
                                    <?php
                                }
                            }
                            ?>
                        </div>

                        <div class="ctg ctg--1_3">
                            <aside class="bloc text-center">
                                <div class="companyLogo">
                                    <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $startup[0]['logo']) ?>" alt="<?php echo $startup[0]['nom']; ?>" />
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Total des fonds levés</div>
                                    <div class="datasbloc__val"><?php
                                        if ($tab_somme_total_lf[0]["somme"] != 0) {
                                            echo str_replace(",0", "", number_format($tab_somme_total_lf[0]["somme"] / 1000, 1, ',', ' ')) . " M€";
                                        } else
                                            echo "-";
                                        ?></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Effectifs</div>
                                    <div class="datasbloc__val"><?php
                                        if ($startup[0]['rang_effectif'] == 1)
                                            echo "1";
                                        if (($startup[0]['rang_effectif'] > 1) && ($startup[0]['rang_effectif'] <= 10))
                                            echo "2 à 10";
                                        if (($startup[0]['rang_effectif'] > 10) && ($startup[0]['rang_effectif'] <= 50))
                                            echo "11 à 50";
                                        if (($startup[0]['rang_effectif'] > 51) && ($startup[0]['rang_effectif'] <= 100))
                                            echo "51 à 100";
                                        if ($startup[0]['rang_effectif'] > 100)
                                            echo ">100";
                                        ?></div>
                                </div>
                                <ul class="companyInfos">
                                    <li>Création : <?php echo $startup[0]['creation'] ?></li>
                                    <li>Capital : <?php
                                        $capital1 = str_replace(array("€", " "), array("", ""), $startup_capital[0]["capital"]);
                                        if ($capital1 != "") {
                                            ?>


                                            <?php echo str_replace(",0", "", number_format(($capital1 / 1000), 1, ",", " ")) ?> K€

                                            <?php
                                        } else {
                                            ?>
                                            -
                                            <?php
                                        }
                                        ?></li>
                                    <li><?php
                                        if ($nb_founder == 1)
                                            echo $nb_founder . " fondateur";
                                        else
                                            echo $nb_founder . " fondateurs";
                                        ?></li>
                                </ul>
                                <div class="bloc-actions bloc-actions--darkgrey">
                                    <div class="bloc-actions__ico">
                                        <a href="#" title="">
                                            <span class="icon-wrapper">
                                                <span class="ico-growup"></span>
                                            </span>                                        
                                        </a>
                                    </div>


                                </div>
                                <?php
                                if (isset($_SESSION['data_login'])) {
                                    ?>
                                    <div>  <a  href="<?php echo URL ?>/revendiquer-ma-startup">   Revendiquer cette startup   </a></div>
                                    <?php
                                }
                                ?>
                            </aside>
                            <div>
                                <div class="bloc <?php
                                if (!isset($_SESSION['data_login'])) {
                                    echo 'content_radar';
                                }
                                ?>">
                                    <div class="ctg ctg--2_2 ctg--fullheight">
                                        <div class="startupIntro">
                                            <?php
                                            $bloc_tab = explode(" ", strip_tags($startup[0]['long_fr']));
                                            $nbdd = count($bloc_tab);
                                            $bloc1 = "";
                                            $bloc2 = "";
                                            for ($p = 0; $p < 30; $p++) {
                                                $bloc1 .= $bloc_tab[$p] . " ";
                                            }
                                            for ($pp = 30; $pp < $nbdd; $pp++) {
                                                $bloc2 .= $bloc_tab[$pp] . " ";
                                            }
                                            ?>
                                            <h2><?php echo utf8_encode(stripslashes($startup[0]['short_fr'])); ?></h2>
                                            <p class="intro-short"><?php echo utf8_encode($bloc1); ?> <span class="intro-seeMore">voir plus</span></p>
                                            <p class="intro-long"><?php echo utf8_encode($bloc1 . $bloc2); ?> <span class="intro-seeLess">voir moins</span></p>

                                        </div>
                                        <?php
                                        if (isset($_SESSION['data_login'])) {
                                            ?>


                                            <div class="chart chart--radar">
                                                <div id="chart-radar"></div>
                                            </div>
                                            <script>
                                                am4core.ready(function () {


                                                    /* Create chart instance */
                                                    var chart = am4core.create("chart-radar", am4charts.RadarChart);
                                                    /* Add data */
                                                    chart.data = [{
                                                            "date": "A",
                                                            "value": <?php echo $ligne_secteur_activite['competition_secteur'] ?>,
                                                            "value2": <?php echo number_format($ligne_france['competition_secteur_activite'], 1, ".", "") ?>
                                                        }, {
                                                            "date": "B",
                                                            "value": <?php echo $matirute_courbe; ?>,
                                                            "value2": <?php echo $ligne_secteur_activite['moy_maturite'] ?>
                                                        }, {
                                                            "date": "C",
                                                            "value": <?php echo $chiffre["indice_transparence"]; ?>,
                                                            "value2": <?php echo $ligne_secteur_activite["moy_transparence"] ?>
                                                        }, {
                                                            "date": "D",
                                                            "value": <?php echo $chiffre['tours'] ?>,
                                                            "value2": <?php echo $ligne_secteur_activite['moy_nbr_levee'] ?>
                                                        }, {
                                                            "date": "E",
                                                            "value": <?php echo $anciennete_courbe; ?>,
                                                            "value2": <?php echo $ligne_secteur_activite['moy_anciennete']; ?>
                                                        }, {
                                                            "date": "F",
                                                            "value": <?php echo $traction_courbe; ?>,
                                                            "value2": <?php echo $ligne_secteur_activite['moy_traction_likedin'] ?>
                                                        }];
                                                    chart.paddingTop = 0;
                                                    chart.paddingBottom = 0;
                                                    chart.paddingLeft = 0;
                                                    chart.paddingRight = 0;
                                                    /* Create axes */
                                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                                    categoryAxis.dataFields.category = "date";
                                                    categoryAxis.renderer.ticks.template.strokeWidth = 2;
                                                    categoryAxis.renderer.labels.template.fontSize = 11;
                                                    categoryAxis.renderer.labels.template.fill = labelColor;


                                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                                    valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
                                                    valueAxis.renderer.axisFills.template.fillOpacity = 0.05;
                                                    valueAxis.renderer.labels.template.fontSize = 11;



                                                    valueAxis.events.on("ready", function (ev) {
                                                        ev.target.min = 0;
                                                        ev.target.max = 10;
                                                    })

                                                    valueAxis.renderer.gridType = "polygons"


                                                    /* Create and configure series */
                                                    function createSeries(field, name, themeColor) {
                                                        var series = chart.series.push(new am4charts.RadarSeries());
                                                        series.dataFields.valueY = field;
                                                        series.dataFields.categoryX = "date";
                                                        series.strokeWidth = 2;
                                                        series.fill = themeColor;
                                                        series.fillOpacity = 0.3;
                                                        series.stroke = themeColor;
                                                        series.name = name;
                                                        // Show bullets ?
                                                        //let circleBullet = series.bullets.push(new am4charts.CircleBullet());
                                                        //circleBullet.circle.stroke = am4core.color('#fff');
                                                        //circleBullet.circle.strokeWidth = 2;
                                                    }


                                                    createSeries("value", "Startup", themeColorBlue);
                                                    createSeries("value2", "Secteur", themeColorGrey);
                                                    chart.legend = new am4charts.Legend();
                                                    chart.legend.position = "top";
                                                    chart.legend.useDefaultMarker = true;
                                                    chart.legend.fontSize = "11";
                                                    chart.legend.color = themeColorGrey;
                                                    chart.legend.labels.template.fill = labelColor;
                                                    chart.legend.labels.template.textDecoration = "none";
                                                    chart.legend.valueLabels.template.textDecoration = "none";
                                                    let as = chart.legend.labels.template.states.getKey("active");
                                                    as.properties.textDecoration = "line-through";
                                                    as.properties.fill = themeColorDarkgrey;
                                                    let marker = chart.legend.markers.template.children.getIndex(0);
                                                    marker.cornerRadius(12, 12, 12, 12);
                                                    marker.width = 20;
                                                    marker.height = 20;
                                                });
                                            </script>
                                            <div class="datagrid">
                                                <div class="datagrid__bloc">
                                                    <div class="datagrid__key">
                                                        <span class="datagrid__key__label">A :</span> Compétition secteur
                                                    </div>
                                                    <div class="datagrid__val"><?php
                                                        if ($ligne_secteur_activite['competition_secteur'] != '')
                                                            echo str_replace(".", ",", $ligne_secteur_activite['competition_secteur']);
                                                        else
                                                            echo "-";
                                                        ?> 
                                                        <?php
                                                        if ($ligne_secteur_activite['competition_secteur'] < $ligne_france['competition_secteur_activite']) {
                                                            ?>
                                                            <span class="ico-decrease"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($ligne_secteur_activite['competition_secteur'] > $ligne_france['competition_secteur_activite']) {
                                                            ?>
                                                            <span class="ico-raise"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($ligne_secteur_activite['competition_secteur'] == $ligne_france['competition_secteur_activite']) {
                                                            ?>
                                                            <span class="ico-stable"></span>
                                                            <?php
                                                        }
                                                        ?>

                                                    </div>
                                                    <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['competition_secteur_activite'], 1, ",", "")); ?></div>
                                                </div>
                                                <div class="datagrid__bloc">
                                                    <div class="datagrid__key">
                                                        <span class="datagrid__key__label">B :</span> Maturité
                                                    </div>
                                                    <div class="datagrid__val"><?php
                                                        if ($chiffre['maturite'] != '')
                                                            echo str_replace(".", ",", $chiffre['maturite']);
                                                        else
                                                            echo "-";
                                                        ?> 
                                                        <?php
                                                        if ($chiffre["maturite"] < $ligne_secteur_activite['moy_maturite']) {
                                                            ?>
                                                            <span class="ico-decrease"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($chiffre["maturite"] > $ligne_secteur_activite['moy_maturite']) {
                                                            ?>
                                                            <span class="ico-raise"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($chiffre["maturite"] == $ligne_secteur_activite['moy_maturite']) {
                                                            ?>
                                                            <span class="ico-stable"></span>
                                                            <?php
                                                        }
                                                        ?>

                                                    </div>
                                                    <div class="datagrid__subtitle">Moy. secteur <?php echo str_replace(".", ",", $ligne_secteur_activite['moy_maturite']); ?></div>
                                                </div>
                                                <div class="datagrid__bloc">
                                                    <div class="datagrid__key">
                                                        <span class="datagrid__key__label">C :</span> Transparence
                                                    </div>
                                                    <div class="datagrid__val"><?php
                                                        if ($chiffre["indice_transparence"] != "")
                                                            echo $chiffre["indice_transparence"];
                                                        else
                                                            echo "-";
                                                        ?> 
                                                        <?php
                                                        if ($chiffre["indice_transparence"] < $ligne_secteur_activite['moy_transparence']) {
                                                            ?>
                                                            <span class="ico-decrease"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($chiffre["indice_transparence"] > $ligne_secteur_activite['moy_transparence']) {
                                                            ?>
                                                            <span class="ico-raise"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($chiffre["indice_transparence"] == $ligne_secteur_activite['moy_transparence']) {
                                                            ?>
                                                            <span class="ico-stable"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="datagrid__subtitle">Moy. secteur <?php echo str_replace(".", ",", $ligne_secteur_activite['moy_transparence']) ?></div>
                                                </div>
                                                <div class="datagrid__bloc">
                                                    <div class="datagrid__key">
                                                        <span class="datagrid__key__label">D :</span> Nombre de levées
                                                    </div>
                                                    <div class="datagrid__val"><?php
                                                        if ($chiffre['tours'] != "")
                                                            echo str_replace(".", ",", $chiffre['tours']);
                                                        else
                                                            echo "-";
                                                        ?>
                                                        <?php
                                                        if ($chiffre["tours"] < $ligne_secteur_activite['moy_nbr_levee']) {
                                                            ?>
                                                            <span class="ico-decrease"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($chiffre["tours"] > $ligne_secteur_activite['moy_nbr_levee']) {
                                                            ?>
                                                            <span class="ico-raise"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($chiffre["tours"] == $ligne_secteur_activite['moy_nbr_levee']) {
                                                            ?>
                                                            <span class="ico-stable"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="datagrid__subtitle">Moy. secteur <?php echo str_replace(".", ",", $ligne_secteur_activite['moy_nbr_levee']); ?></div>
                                                </div>
                                                <div class="datagrid__bloc">
                                                    <div class="datagrid__key">
                                                        <span class="datagrid__key__label">E :</span> Ancienneté
                                                    </div>
                                                    <div class="datagrid__val"><?php echo str_replace(".", ",", $anciennete); ?>
                                                        <?php
                                                        if ($anciennete < $ligne_secteur_activite['moy_anciennete']) {
                                                            ?>
                                                            <span class="ico-decrease"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($anciennete > $ligne_secteur_activite['moy_anciennete']) {
                                                            ?>
                                                            <span class="ico-raise"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($anciennete == $ligne_secteur_activite['moy_anciennete']) {
                                                            ?>
                                                            <span class="ico-stable"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="datagrid__subtitle">Moy. secteur <?php echo str_replace(".", ",", $ligne_secteur_activite['moy_anciennete']); ?></div>
                                                </div>
                                                <div class="datagrid__bloc">
                                                    <div class="datagrid__key">
                                                        <span class="datagrid__key__label">F :</span> Traction LinkedIn
                                                    </div>
                                                    <div class="datagrid__val"><?php echo str_replace(".", ",", $traction); ?> 
                                                        <?php
                                                        if ($traction < $ligne_secteur_activite['moy_traction_likedin']) {
                                                            ?>
                                                            <span class="ico-decrease"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($traction > $ligne_secteur_activite['moy_traction_likedin']) {
                                                            ?>
                                                            <span class="ico-raise"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($traction == $ligne_secteur_activite['moy_traction_likedin']) {
                                                            ?>
                                                            <span class="ico-stable"></span>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="datagrid__subtitle">Moy. secteur <?php echo str_replace(".", ",", $ligne_secteur_activite['moy_traction_likedin']); ?></div>
                                                </div>
                                            </div>

                                            <div class="bloc-actions">
                                                <div class="bloc-actions__ico">
                                                    <a style="cursor: pointer" onclick="memoriser(<?php echo $startup[0]['id']; ?>)" title="">
                                                        <span class="icon-wrapper">
                                                            <span class="ico-bookmark"></span>
                                                        </span>
                                                        Mémoriser
                                                    </a>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                                if (!isset($_SESSION['data_login'])) {
                                    ?>
                                    <div id="bloc_radar"></div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>

                    </section>


                    <section class="module module--finances <?php if (!isset($_SESSION['data_login'])) echo 'content_finance'; ?>">
                        <div class="module__title">Finances : capitaux et levées de fonds</div>
                        <div class="bloc">
                            <?php
                            if (isset($_SESSION['data_login'])) {
                                ?>
                                <div class="row">
                                    <div class="col datasbloc text-center">
                                        <div class="datasbloc__key">Montant global levé</div>
                                        <div class="datasbloc__val"><?php
                                            if ($tab_somme_total_lf[0]["somme"] != 0) {
                                                echo str_replace(",0", "", number_format($tab_somme_total_lf[0]["somme"] / 1000, 1, ',', ' ')) . " M€";
                                            } else
                                                echo "-";
                                            ?></div>
                                        <div class="datasbloc__subtitle"><?php
                                            if ($tab_somme_total_lf[0]["somme"] != 0) {
                                                if ($purcentage_lf > 0) {

                                                    echo "+" . number_format($purcentage_lf, 0, ",", " ") . "% / moy. secteur";
                                                }
                                                if ($purcentage_lf < 0) {
                                                    echo "-" . number_format($purcentage_lf, 0, ",", " ") . "% / moy. secteur";
                                                }
                                                if ($purcentage_lf == 0) {
                                                    echo "-";
                                                }
                                            } else {
                                                echo "";
                                            }
                                            ?></div>
                                    </div>
                                    <div class="col datasbloc text-center">
                                        <div class="datasbloc__key">Nombre de levées</div>
                                        <div class="datasbloc__val"><?php echo $chiffre['tours'] ?></div>
                                        <div class="datasbloc__subtitle"><?php
                                            if ($chiffre['nb_investisseur'] != '')
                                                echo $chiffre['nb_investisseur'] . " VC";
                                            else
                                                echo "-";
                                            ?></div>
                                    </div>
                                    <div class="col datasbloc text-center">
                                        <div class="datasbloc__key">Fréquence</div>
                                        <div class="datasbloc__val"><?php
                                            if (($chiffre['frequence_levee'] != '') && ($chiffre['frequence_levee'] != 1))
                                                echo str_replace(".", ",", $chiffre['frequence_levee']) . " mois";
                                            else
                                                echo "-";
                                            ?></div>
                                        <div class="datasbloc__subtitle" style="white-space: nowrap;">Délai moyen entre 2 levées</div>
                                    </div>
                                    <div class="col datasbloc text-center">
                                        <div class="datasbloc__key"style="white-space: nowrap;">Probabilité de future levée</div>
                                        <div class="datasbloc__val"><?php
                                            if ($chiffre['prob_levee'] != "")
                                                echo $chiffre['prob_levee'];
                                            else
                                                echo "Faible";
                                            ?></div>
                                        <div class="datasbloc__subtitle">à 6 mois</div>
                                    </div>
                                    <div class="col datasbloc text-center">
                                        <div class="datasbloc__key">Capital social</div>
                                        <?php
                                        $capital2 = str_replace(array("€", " "), array("", ""), $startup_capital[0]["capital"]);

                                        if ($capital2 != "") {
                                            ?>
                                            <div class="datasbloc__val"><?php echo str_replace(",0", "", number_format(($capital2 / 1000), 1, ",", " ")) ?> K€</div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="datasbloc__val">-</div>
                                        <?php } ?>
                                        <div class="datasbloc__subtitle"><?php echo $nb_evolution; ?> évolutions</div>
                                    </div>
                                    <div class="col datasbloc text-center">
                                        <div class="datasbloc__key">CA</div>
                                        <div class="datasbloc__val"><?php
                                            $val_ca = explode(';', $startup[0]['evo_ca']);
                                            $nb_val = count($val_ca);
                                            $ca1 = '';
                                            $anneeca1 = '';
                                            $evoca = '';
                                            $pource = '';

                                            if ($nb_val == 4) {
                                                $ca1 = $val_ca[0];
                                                $anneeca1 = $val_ca[1];
                                                $evoca = $val_ca[2];
                                                $pource = $val_ca[3];
                                            }
                                            if ($nb_val == 2) {
                                                $ca1 = $val_ca[0];
                                                $anneeca1 = $val_ca[1];
                                            }

                                            if ($ca1 != '')
                                                echo str_replace(',00', '', number_format($ca1 / 1000000, 2, ",", " ")) . " M€";
                                            else
                                                echo "-";
                                            ?></div>
                                        <div class="datasbloc__subtitle"><?php
                                            echo $anneeca1;
                                            if ($evoca != "") {
                                                if ($evoca == -1) {
                                                    ?>
                                                    <span class="ico-decrease"></span> <?php echo str_replace(",0", "", number_format($pource, 1, ",", "")) ?>%
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($evoca == 1) {
                                                    ?>
                                                    <span class="ico-raise"></span> <?php echo str_replace(",0", "", number_format($pource, 1, ",", "")) ?>%
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if (($chiffre['tours'] > 0) || ($nb_rachat > 0)|| ($nb_ipo > 0)) {
                                    ?>
                                    <div class="bloc__title mtop">Détails des levées de fonds de <?php echo $startup[0]['nom'] ?></div>
                                    <div class="tablebloc">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Montant</th>
                                                    <th class="text-center">Date</th>
                                                    <th>Investisseurs</th>
                                                </tr>
                                            </thead>
                                            <?php
                                            $row_last_lf_all_startup = getAllLFByStartup($startup[0]['id']);
                                            if ((!empty($row_last_lf_all_startup)) || ($nb_rachat > 0)|| ($nb_ipo > 0)) {
                                                ?>
                                                <tbody>
                                                    <?php
                                                    $sql_ipo = mysqli_query($link, "select * from lf where rachat=2 and id_startup=" . $startup[0]['id']);
                                                    $nb_ipo = mysqli_num_rows($sql_ipo);
                                                    if ($nb_ipo > 0) {
                                                        while ($all_ipo = mysqli_fetch_array($sql_ipo)) {
                                                            ?>
                                                            <tr>
                                                                <td class="text-center moneyCell"><?php
                                                                if ($all_ipo['montant'] != 0) {
                                                                    echo str_replace(",0", "", number_format($all_ipo['montant'] / 1000, 1, ",", ".")) . " M€";
                                                                } else
                                                                    echo "NC";
                                                                ?></td>
                                                                <td class="text-center dateCell"><?php echo change_date_fr_chaine_compl($all_ipo['date_ajout']); ?></td>
                                                                <td class="">
                                                                    <div class="labels">
                                                                      
                                                                        <span class="label">Entrée en bourse</span>
                                                                      
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    $sql_rachat = mysqli_query($link, "select * from lf where rachat=1 and id_startup=" . $startup[0]['id']);
                                                    $nb_rachat = mysqli_num_rows($sql_rachat);
                                                    if ($nb_rachat > 0) {
                                                        while ($all_rachat = mysqli_fetch_array($sql_rachat)) {
                                                            ?>
                                                            <tr>
                                                                <td class="text-center moneyCell"> Rachat</td>
                                                                <td class="text-center dateCell"><?php echo change_date_fr_chaine($all_rachat['date_ajout']); ?></td>
                                                                <td class="">
                                                                    <div class="labels">
                                                                        <?php
                                                                        ?>
                                                                        <span class="label"> <?php echo $all_rachat['de']; ?></span>
                                                                        <?php
                                                                        ?>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }

                                                    $ii = 1;
                                                    foreach ($row_last_lf_all_startup as $all_lf) {
                                                        if ($ii == 1)
                                                            $lettre = "1";
                                                        if ($ii == 2)
                                                            $lettre = "2";
                                                        if ($ii == 3)
                                                            $lettre = "3";
                                                        if ($ii == 4)
                                                            $lettre = "4";
                                                        if ($ii == 5)
                                                            $lettre = "5";
                                                        if ($ii == 6)
                                                            $lettre = "6";
                                                        if ($ii == 7)
                                                            $lettre = "7";
                                                        if ($ii == 8)
                                                            $lettre = "8";
                                                        if ($ii == 9)
                                                            $lettre = "9";
                                                        ?>

                                                        <tr>
                                                            <td class="text-center moneyCell"><?php
                                                                if ($all_lf['montant'] != 0) {
                                                                    echo str_replace(",0", "", number_format($all_lf['montant'] / 1000, 1, ",", ".")) . " M€";
                                                                } else
                                                                    echo "NC";
                                                                ?></td>
                                                            <td class="text-center dateCell"><?php echo change_date_fr_chaine($all_lf['date_ajout']); ?></td>
                                                            <td class="">
                                                                <div class="labels">
                                                                    <?php
                                                                    $sql_list_invest = mysqli_query($link, "select * from startup_investisseur where id_lf=" . $all_lf['id']);
                                                                    while ($mon_invest = mysqli_fetch_array($sql_list_invest)) {
                                                                        if ($mon_invest['id_investisseur'] != 0) {
                                                                            $name_invest = mysqli_fetch_array(mysqli_query($link, "select * from new_name_list where id=" . $mon_invest['id_investisseur']));
                                                                            ?>
                                                                            <a href="<?php echo URL; ?>/startups-investisseur/<?php echo generate_id($mon_invest['id_investisseur']) ?>/<?php echo urlWriting(strtolower($name_invest['new_name'])) ?>" class="label"><?php echo $name_invest['new_name']; ?></a>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <span class="label"> <?php echo ($mon_invest['group_name']); ?></span>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $ii++;
                                                    }
                                                    ?>
                                                </tbody>
                                                <?php
                                            }
                                            ?>
                                        </table>
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                            }
                            ?>
                        </div>
                    </section>
                    <?php
                    if (!isset($_SESSION['data_login'])) {
                        ?>
                        <div id="fiche_startup"></div>
                        <?php
                    }
                    ?>
                    <section class="module">
                        <div class="module__title">Marché & secteur</div>
                        <div class="ctg ctg--1_2_1">
                            <div class="bloc text-center">
                                <!--<div class="bloc__title">Compétition secteur</div>-->
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Compétition secteur </div>
                                    <div class="datasbloc__val"><?php echo str_replace(".", ",", $ligne_secteur_secteur['indice_competition_secteur']); ?> 
                                        <?php if ($ligne_secteur_secteur['indice_competition_secteur'] > $ligne_secteur_secteur['indice_competition_secteur_12']) { ?>
                                            <span class="ico-raise"></span>
                                        <?php } ?>
                                        <?php if ($ligne_secteur_secteur['indice_competition_secteur'] == $ligne_secteur_secteur['indice_competition_secteur_12']) { ?>
                                            <span class="ico-stable"></span>
                                        <?php } ?>
                                        <?php if ($ligne_secteur_secteur['indice_competition_secteur'] < $ligne_secteur_secteur['indice_competition_secteur_12']) { ?>
                                            <span class="ico-decrease"></span>
                                        <?php } ?>
                                    </div>
                                    <div class="datasbloc__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['indice_competition_secteur'], 1, ",", "")); ?> </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Création secteur</div>
                                    <div class="datasbloc__val"><?php echo str_replace(".", ",", $ligne_secteur_secteur['taux_creation']) . "%" ?></div>
                                    <div class="datasbloc__subtitle">Moy. France <?php echo str_replace(',0', '', number_format($ligne_france['taux_creation'], 1, ",", "")); ?>%</div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Secteur</div>
                                    <div class="datasbloc__val" style="font-size:15px !important;text-align: center;margin-top: 5px;">
                                        <a style="text-align: center;display: initial;margin-top: 9px;" href="<?php echo URL ?>/etude-secteur/<?php echo generate_id($row_nom_secteur[0]['id']) . "/" . urlWriting($row_nom_secteur[0]['nom_secteur']) ?>" target="_blank" title="" class="tags__el tags__el--pinky">
                                            <span class="ico-chart"></span> <?php echo $id_secteur['nom_secteur']; ?>
                                        </a>

                                    </div>
                                </div>
                                <?php
                                if ($id_sous_secteur['nom_sous_secteur'] != "") {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Sous Secteur</div>
                                        <div class="datasbloc__val" style="font-size:15px !important;text-align: center;margin-top: 5px;">

                                            <a style="text-align: center;display: initial;margin-top: 9px;" href="<?php echo URL; ?>/recherche-startups/tags/<?php echo addslashes(str_replace(array("-", " ", "&", "é", "è", "ê", "î", "ï", "ô",","), array("__", "_", "..", "e", "e", "e", "i", "i", "o",""), utf8_encode($id_sous_secteur['nom_sous_secteur']))); ?>" title="" class="tags__el">
                                                <span class="ico-tag"></span> <?php echo $id_sous_secteur['nom_sous_secteur']; ?> 
                                            </a>                            

                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($id_activite['nom_activite'] != "") {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Activité</div>
                                        <div class="datasbloc__val" style="font-size:15px !important;text-align: center;margin-top: 5px;">

                                            <a style="text-align: center;display: initial;margin-top: 9px;" href="<?php echo URL; ?>/recherche-startups/tags/<?php echo addslashes(str_replace(array("-", " ", "&", "é", "è", "ê", "î", "ï", "ô",","), array("__", "_", "..", "e", "e", "e", "i", "i", "o",""), utf8_encode($id_activite['nom_activite']))); ?>" title="" class="tags__el">
                                                <span class="ico-tag"></span> <?php echo $id_activite['nom_activite']; ?> 
                                            </a>                            

                                        </div>
                                       
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($id_sous_activite['nom_sous_activite'] != "") {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Sous Activité</div>
                                        <div class="datasbloc__val" style="font-size:15px !important;text-align: center;margin-top: 5px;">

                                            <a style="text-align: center;display: initial;margin-top: 9px;" href="<?php echo URL; ?>/recherche-startups/tags/<?php echo addslashes(str_replace(array("-", " ", "&", "é", "è", "ê", "î", "ï", "ô",","), array("__", "_", "..", "e", "e", "e", "i", "i", "o",""), utf8_encode($id_sous_activite['nom_sous_activite']))); ?>" title="" class="tags__el">
                                                <span class="ico-tag"></span> <?php echo $id_sous_activite['nom_sous_activite']; ?> 
                                            </a>                            

                                        </div>
                                        
                                    </div>
                                    <?php
                                }
                                ?>

                                <div class="datasbloc">
                                    <div class="datasbloc__key">Modèle</div>
                                    <div class="datasbloc__val"><?php
                                        if ($startup[0]['cible'] != "")
                                            echo $startup[0]['cible'];
                                        else
                                            echo "-";
                                        ?></div>
                                </div>
                            </div>
                            <div>
                                <div class="bloc <?php
                                if (!isset($_SESSION['data_login'])) {
                                    echo 'content_maturite';
                                }
                                ?>">
                                    <div class="bloc__title">Maturité des startups du secteur</div>
                                    <?php
                                    if (isset($_SESSION['data_login'])) {
                                        ?>
                                        <div class="chart chart--bubbles">
                                            <div id="chart-bubbles-startups"></div>
                                        </div>
                                        <script>
                                            am4core.ready(function () {


                                                var chart = am4core.create("chart-bubbles-startups", am4charts.XYChart);
                                                chart.paddingBottom = 0;
                                                chart.paddingLeft = 0;
                                                chart.paddingRight = 15;
                                                chart.numberFormatter.numberFormat = "#.";

                                                var valueAxisX = chart.xAxes.push(new am4charts.ValueAxis());
                                                valueAxisX.renderer.ticks.template.disabled = true;
                                                valueAxisX.renderer.axisFills.template.disabled = true;
                                                valueAxisX.renderer.fontSize = 11;
                                                valueAxisX.renderer.color = labelColor;
                                                valueAxisX.renderer.labels.template.fill = labelColor;

                                                // Title left
                                                valueAxisX.title.text = "Années";
                                                valueAxisX.title.align = "right";

                                                //valueAxisX.title.valign = "top";
                                                valueAxisX.title.dy = 0;
                                                valueAxisX.title.fontSize = 11;
                                                valueAxisX.title.fill = labelColor;

                                                var valueAxisY = chart.yAxes.push(new am4charts.ValueAxis());
                                                valueAxisY.renderer.ticks.template.disabled = true;
                                                valueAxisY.renderer.axisFills.template.disabled = true;
                                                valueAxisX.min = 2006;
                                                valueAxisX.max = <?php echo date('Y') ?>;

                                                // Title left
                                                valueAxisY.title.text = "Score de maturité";
                                                valueAxisY.title.rotation = -90;
                                                valueAxisY.title.align = "left";
                                                valueAxisY.title.valign = "top";
                                                valueAxisY.title.dy = 0;
                                                valueAxisY.title.fontSize = 11;
                                                valueAxisY.title.fill = labelColor;
                                                valueAxisY.renderer.fontSize = 11;
                                                valueAxisY.renderer.color = labelColor;
                                                valueAxisY.renderer.labels.template.fill = labelColor;

                                                var series = chart.series.push(new am4charts.LineSeries());
                                                series.dataFields.valueX = "x";
                                                series.dataFields.valueY = "y";
                                                series.dataFields.value = "value";
                                                series.strokeOpacity = 0;
                                                series.sequencedInterpolation = true;

                                                series.tooltip.pointerOrientation = "vertical";
                                                series.tooltip.getFillFromObject = false;
                                                series.tooltip.background.fill = am4core.color("#ffffff");
                                                series.tooltip.background.stroke = themeColorLightGrey;
                                                series.tooltip.label.fontSize = 13;
                                                series.tooltip.label.fill = themeColorDarkgrey;


                                                var bullet = series.bullets.push(new am4core.Circle());
                                                bullet.fill = themeColorLightGrey;
                                                bullet.propertyFields.fill = "color";
                                                bullet.strokeOpacity = 0;
                                                bullet.strokeWidth = 2;
                                                bullet.fillOpacity = 0.5;
                                                bullet.stroke = am4core.color("#ffffff");
                                                bullet.hiddenState.properties.opacity = 0;
                                                bullet.tooltipText = "[bold]{title}[/]\nAnnée: {valueX.value}\nMaturité : {valueY.value}";

                                                var shadow = series.tooltip.background.filters.getIndex(0);
                                                shadow.dx = 0;
                                                shadow.dy = 0;
                                                shadow.blur = 0;



                                                var outline = chart.plotContainer.createChild(am4core.Circle);
                                                outline.fillOpacity = 0;
                                                outline.strokeOpacity = 0.8;
                                                outline.stroke = themeColorBlue;
                                                outline.strokeWidth = 2;
                                                outline.hide(0);

                                                var blurFilter = new am4core.BlurFilter();
                                                outline.filters.push(blurFilter);
                                                bullet.events.on("over", function (event) {
                                                    var target = event.target;
                                                    outline.radius = target.pixelRadius + 2;
                                                    outline.x = target.pixelX;
                                                    outline.y = target.pixelY;
                                                    outline.show();
                                                })

                                                bullet.events.on("out", function (event) {
                                                    outline.hide();
                                                })

                                                var hoverState = bullet.states.create("hover");
                                                hoverState.properties.fillOpacity = 1;
                                                hoverState.properties.strokeOpacity = 1;
                                                series.heatRules.push({target: bullet, min: 2, max: 10, property: "radius"});
                                                bullet.adapter.add("tooltipY", function (tooltipY, target) {
                                                    return -target.radius;
                                                })

                                                chart.data = [
    <?php
    $tab_bubble = explode('##', $startup[0]['bubble']);
    $nb_bubble = count($tab_bubble);
    for ($v = 0; $v <= $nb_bubble; $v++) {
        $tab_ligne = explode(";", $tab_bubble[$v]);

        $nom_startup_bubble = $tab_ligne[0];
        $creation_bubble = $tab_ligne[1];
        $maturite_bubble = $tab_ligne[2];
        $id_startup_bubble = $tab_ligne[3];

        if ($nom_startup_bubble != '') {
            ?>
                                                            {
                                                                "title": "<?php echo addslashes($nom_startup_bubble); ?>",
                                                                "color": <?php
            if ($id_startup_bubble != $startup[0]['id'])
                echo 'themeColorGrey';
            else
                echo 'themeColorBlue';
            ?>,
                                                                "x": "<?php echo $creation_bubble; ?>",
                                                                "y": <?php echo $maturite_bubble; ?>,
                                                                "value": <?php
            if ($id_startup_bubble != $startup[0]['id'])
                echo '60';
            else
                echo '60';
            ?>
                                                            },
            <?php
        }
    }
    ?>

                                                ];
                                            });
                                        </script>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                                if (!isset($_SESSION['data_login'])) {
                                    ?>
                                    <div id="bloc_maturite"></div>
                                    <?php
                                }
                                ?>

                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Startups à suivre</div>
                                <div class="tablebloc">
                                    <table class="table table--condensed">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th>Création</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $row_startup_autre = mysqli_query($link, "select * from startup_similar where id_startup=" . $startup[0]['id'] . " order by score limit 10");
                                            while ($autre_data = mysqli_fetch_array($row_startup_autre)) {
                                                $autre_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,date_complete from startup where id=" . $autre_data["id_similar"]));
                                                $cch = "";
                                                $tabs = $autre_startup['nom'];
                                                $nbe = count($tabs);
                                                for ($j = 0; $j < 15; $j++) {
                                                    $cch = $cch . $tabs[$j];
                                                }
                                                if ($nbe > 15) {
                                                    $cch = $cch . "...";
                                                }
                                                ?>
                                                <tr>
                                                    <td>
                                                        <a href="<?php echo URL . '/fr/startup-france/' . generate_id($autre_startup['id']) . "/" . urlWriting(strtolower($autre_startup["nom"])) ?>" title="" class="companyNameCell"><span><?php echo ($cch); ?></span></a>
                                                    </td>
                                                    <td><?php
                                                        if ($autre_startup['date_complete'] != "")
                                                            echo change_date_fr_chaine_related($autre_startup['date_complete']);
                                                        else
                                                            echo "NC";
                                                        ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </section>

                    <section class="module module--fondateurs">
                        <div class="module__title">Fondateurs & team</div>

                        <div class="ctg ctg--3_1">
                            <div class="bloc" id="bloc_autre_founder">
                                <div class="bloc__title">Fondateurs</div>
                                <div class="tablebloc tablebloc--light">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Fondateur</th>
                                                <th class="text-center">Âge</th>
                                                <th class="text-center">École</th>
                                                <th class="text-center">Diplôme</th>
                                                <th class="text-center" width="40%">Compétences</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $ligne_personne = mysqli_fetch_array(mysqli_query($link, "select * from personnes where id_startup=" . $startup[0]['id'] . " limit 1"));

                                            $cch_skils = "";
                                            $lign_exp = mysqli_fetch_array(mysqli_query($link, "select * from personnes_phantombuster where id_personnes=" . $ligne_personne['id']));
                                            $fonction_ligne = mysqli_fetch_array(mysqli_query($link, "select * from fonction where id=" . $ligne_personne["fonction"]));
                                            $ecole_ligne = mysqli_fetch_array(mysqli_query($link, "select * from ecole_personnes where idpersonne=" . $ligne_personne["id"] . " order by a desc limit 1"));
                                            $list_competances = mysqli_query($link, "select * from personnes_competence where id_personne=" . $ligne_personne["id"] . " limit 4");
                                            $ch_skils = explode(',', $lign_exp['allSkills']);
                                            $nb_comm = count($ch_skils);
                                            if ($nb_comm > 0) {
                                                for ($i = 0; $i < 5; $i++) {
                                                    if ($ch_skils[$i] != '') {
                                                        $cch_skils .= ($ch_skils[$i]) . ", ";
                                                    }
                                                }
                                            } else {
                                                $cch_skils = "-";
                                            }
                                            ?>
                                            <tr>
                                                <td class="text-center">
                                                    <div class="avatar">
                                                        <i class="ico-avatar"></i>
                                                    </div>
                                                    <strong><?php echo stripslashes(($ligne_personne['prenom'] . " " . $ligne_personne['nom'])); ?></strong>
                                                    <div class="fonction"><?php echo stripslashes($fonction_ligne['nom_fr']); ?></div>
                                                </td>
                                                <td class="text-center"><?php
                                                    if ($ligne_personne['age'] != "")
                                                        echo $ligne_personne['age'];
                                                    else
                                                        echo "-";
                                                    ?></td>
                                                <td class="text-center"><?php
                                                    if ($lign_exp['schoolName_3'] != "") {
                                                        echo ($lign_exp['schoolName_3']);
                                                    } else if ($lign_exp['schoolName_2'] != "") {
                                                        echo ($lign_exp['schoolName_2']);
                                                    } else if ($lign_exp['schoolName_1'] != "") {
                                                        echo ($lign_exp['schoolName_1']);
                                                    } else {
                                                        echo "-";
                                                    }
                                                    ?></td>
                                                <td class="text-center"><?php
                                                    if ($lign_exp['degree_3'] != "") {
                                                        echo ($lign_exp['degree_3']);
                                                    } else if ($lign_exp['degree_2'] != "") {
                                                        echo ($lign_exp['degree_2']);
                                                    } else if ($lign_exp['degree_1'] != "") {
                                                        echo ($lign_exp['degree_1']);
                                                    } else {
                                                        echo "-";
                                                    }
                                                    ?></td>
                                                <td class="text-center"><?php echo $cch_skils; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="historyList">
                                    <?php
                                    if ($lign_exp['jobTitle_1'] != "") {
                                        ?>
                                        <div class="historyList__item">
                                            <div class="historyList__item__date"><?php echo ($lign_exp['dateRange_job_1']) ?></div>
                                            <div class="historyList__item__infos"><?php echo ($lign_exp['jobTitle_1']) ?> chez <?php echo ($lign_exp['companyName_1']) ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                    if ($lign_exp['jobTitle_2'] != "") {
                                        ?>
                                        <div class="historyList__item">
                                            <div class="historyList__item__date"><?php echo ($lign_exp['dateRange_job_2']) ?></div>
                                            <div class="historyList__item__infos"><?php echo ($lign_exp['jobTitle_2']) ?> chez <?php echo ($lign_exp['companyName_2']) ?></div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                    if ($lign_exp['jobTitle_3'] != "") {
                                        ?>
                                        <div class="historyList__item">
                                            <div class="historyList__item__date"><?php echo ($lign_exp['dateRange_job_3']) ?></div>
                                            <div class="historyList__item__infos"><?php echo ($lign_exp['jobTitle_3']) ?> chez <?php echo ($lign_exp['companyName_3']) ?></div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Fondateurs & Team</div>
                                <div class="bloc-team">
                                    <?php
                                    $row_total_fondateur = getListFondateurByIdStartup($startup[0]["id"]);
                                    foreach ($row_total_fondateur as $personne) {
                                        $fonction = mysqli_fetch_array(mysqli_query($link, "select * from fonction where id=" . $personne["fonction"]));
                                        ?>
                                        <div style="cursor: pointer" class="bloc-team__people" onclick="getinfofounder(<?php echo $personne["id"]; ?>,<?php echo $startup[0]['id']; ?>)">
                                            <div class="bloc-team__people__avatar">
                                                <div class="avatar">
                                                    <i class="ico-avatar-white"></i>
                                                </div>
                                            </div>
                                            <div class="bloc-team__people__name">
                                                <?php echo stripslashes(utf8_encode($personne['prenom'] . " " . $personne['nom'])); ?>
                                                <span class="fonction"><?php echo stripslashes($fonction['nom_fr']); ?></span>
                                            </div>
                                            <div class="bloc-team__people__link">
                                                <?php if ($personne['linkedin'] != "") { ?>
                                                    <a target="_blank" href="https://<?php echo str_replace(array("https://"), array(""), $personne['linkedin']) ?>" title=""><i class="ico-linkedin"></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module module--fondateurs">
                        <div class="module__title">Localisation</div>
                        <?php
                        if (isset($_SESSION['data_login'])) {

                            $sql_recru = mysqli_query($link, "select * from startup_emploi where id_startup=" . $startup[0]['id']);
                            $nb_recru = mysqli_num_rows($sql_recru);
                            if ($nb_recru > 0) {
                                $cs = "ctg--3_3";
                            } else {
                                $cs = "ctg--2_2";
                            }
                        } else {
                            $cs = "ctg--2_2";
                        }
                        ?>
                        <div class="ctg <?php echo $cs; ?>">
                            <div class="bloc jc text-center">
                                <div class="datasbloc">
                                    <div class="datasbloc__val">
                                        <?php
                                        if ($startup[0]['region_new'] != "") {
                                            ?>
                                            <div class="tags">
                                                <a target="_blank" href="<?php echo URL . '/region/' . generate_id($get_id_region['id']) . "/" . urlWriting(strtolower(utf8_encode($get_id_region['region_new']))) ?>" title="" class="tags__el tags__el--pinky">
                                                    <span class="ico-chart"></span>  <?php echo utf8_encode($startup[0]['region_new']) ?>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Adresse</div>
                                    <div class="datasbloc__val small">
                                        <?php if ($startup[0]['adresse'] != '') { ?>
                                            <?php echo utf8_encode(stripslashes($startup[0]['adresse'])) . ", " . $startup[0]['cp'] . " " . utf8_encode($startup[0]['ville']); ?>
                                        <?php } else { ?>
                                            -
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Tél</div>
                                    <div class="datasbloc__val small">
                                        <?php
                                        if (isset($_SESSION['data_login'])) {
                                            if ($startup[0]['tel'] != "")
                                                echo $startup[0]['tel'];
                                            else
                                                echo "-";
                                        } else {
                                            ?>
                                            <a href="<?php echo URL ?>/connexion" class="link-not-connected">Connectez-vous</a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Website</div>
                                    <div class="datasbloc__val small">
                                        <a href="https://www.<?php echo str_replace(array("https://", "http://", "www."), array("", "", ""), $startup[0]['url']); ?>" target="_blank"><?php echo $startup[0]['url'] ?></a>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Email</div>
                                    <div class="datasbloc__val small">
                                        <?php
                                        if (isset($_SESSION['data_login'])) {
                                            echo $startup[0]['email'];
                                        } else {
                                            ?>
                                            <a href="<?php echo URL ?>/connexion" class="link-not-connected">Connectez-vous</a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Réseaux sociaux</div>
                                    <div class="datasbloc__val small">
                                        <ul class="list list--socialnetworks">
                                            <?php if ($startup[0]['linkedin'] != "") { ?> <li><a target="_blank" href="<?php echo $startup[0]['linkedin']; ?>" title="LinkedIn"><span class="ico-linkedin"></span></a></li><?php } ?>
                                            <?php if ($startup[0]['facebook'] != "") { ?>  <li><a target="_blank" href="<?php echo $startup[0]['facebook']; ?>" title="Facebook"><span class="ico-facebook"></span></a></li><?php } ?>
                                            <?php if ($startup[0]['twitter'] != "") { ?> <li><a target="_blank" href="https://www.twitter.com/<?php echo str_replace('https://twitter.com/', '', $startup[0]['twitter']); ?>" title="Twitter"><span class="ico-twitter"></span></a></li><?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Startups du secteur par département <a href="">Voir la liste &raquo;</a></div>
                                <div class="tablebloc">
                                    <?php
                                    if ($startup[0]['region_new'] == "") {
                                        ?>
                                        <div class="alert alert--infos">Pas de données.</div>
                                        <?php
                                    } else {
                                        ?>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th class="iconCell">Évol.</th>
                                                    <th>Départements</th>
                                                    <th class="text-center">Startups</th>
                                                </tr>
                                            </tbody>
                                            <tbody>
                                                <?php
                                                $tab_list_deps = explode(";", $ligne_secteur_activite['list_dep']);
                                                $tab_list_nbr_deps = explode(";", $ligne_secteur_activite['list_nbr_dep']);
                                                $tab_list_evo_nbr_dep = explode(";", $ligne_secteur_activite['list_evo_nbr_dep']);
                                                $b = count($tab_list_deps);
                                                for ($i = 0; $i < 10; $i++) {
                                                    if ($tab_list_deps[$i] != "") {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                if ($tab_list_evo_nbr_dep[$i] == -1) {
                                                                    ?>
                                                                    <span class="ico-decrease"></span>
                                                                <?php } ?>
                                                                <?php
                                                                if ($tab_list_evo_nbr_dep[$i] == 1) {
                                                                    ?>
                                                                    <span class="ico-raise"></span>
                                                                <?php } ?>
                                                                <?php
                                                                if ($tab_list_evo_nbr_dep[$i] == 0) {
                                                                    ?>
                                                                    <span class="ico-stable"></span>
                                                                <?php } ?>
                                                            </td>
                                                            <td><?php
                                                                if ($tab_list_deps[$i] != '')
                                                                    echo ($tab_list_deps[$i]);
                                                                else
                                                                    echo "NC";
                                                                ?></td>
                                                            <td class="text-center"><?php echo $tab_list_nbr_deps[$i]; ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php
                            if (isset($_SESSION['data_login'])) {
                                if ($nb_recru > 0) {
                                    ?>
                                    <div class="bloc">
                                        <div class="bloc__title">La startup <?php echo $startup[0]['nom'] ?> recrute</div>
                                        <div class="tablebloc">

                                            <?php
                                            if ($nb_recru == 0) {
                                                ?>
                                                <div class="alert alert--infos">Pas de données.</div>
                                                <?php
                                            } else {
                                                while ($data_recru = mysqli_fetch_array($sql_recru)) {
                                                    ?>
                                                    <div style="font-size: 13px;padding: 10px 0px;"><a style="color: #798a9a;" href="<?php echo $data_recru['link_offre']; ?>" target="_blank"><?php echo $data_recru['titre_offre']; ?></a></div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </section>

                    <section class="module module--legals">
                        <div class="module__title">Legals & tech stack</div>
                        <div class="ctg ctg--1_3">
                            <div class="bloc text-center">
                                <div class="bloc__title text-center">Légales</div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Raison sociale</div>
                                    <div class="datasbloc__val small"><?php
                                        if ($startup[0]['societe'] != "")
                                            echo utf8_encode($startup[0]['societe']);
                                        else
                                            echo "-";
                                        ?></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Siret</div>
                                    <div class="datasbloc__val small"><?php
                                        if ($startup[0]['siret'] != "")
                                            echo $startup[0]['siret'];
                                        else
                                            echo "-";
                                        ?></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Forme juridique</div>
                                    <div class="datasbloc__val small"><?php
                                        if ($startup[0]['juridique'] != "")
                                            echo utf8_encode($startup[0]['juridique']);
                                        else
                                            echo "-";
                                        ?></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Code Naf</div>
                                    <div class="datasbloc__val small"><?php
                                        if ($startup[0]['naf'] != "")
                                            echo $startup[0]['naf'];
                                        else
                                            echo "-";
                                        ?></div>
                                </div>
                                <?php
                                $dernier = mysqli_fetch_array(mysqli_query($link, "select * from startup_depot where id_startup=" . $startup[0]['id'] . " order by dt desc limit 1"));
                                ?>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Dernier mouvement Bodacc</div>
                                    <div class="datasbloc__val small"> <?php if ($dernier['depot'] != "") { ?>
                                            <?php echo ($dernier['depot']); ?><br><?php echo change_date_fr_chaine_related($dernier['dt']); ?>
                                        <?php } else echo "-"; ?></div>
                                </div>
                            </div>

                            <div class="bloc text-center">
                                <div class="bloc__title text-center">Tech Stack</div>
                                <?php
                                $nb_stack = mysqli_query($link, "select * from startup_builtwith where id_startup=" . $startup[0]['id']);
                                $startup_builtwith = mysqli_fetch_array($nb_stack);
                                ?>

                                <?php
                                if ($startup_builtwith['Analytics_and_Tracking_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Analytics and Tracking</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Analytics_and_Tracking_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Widgets_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Widgets</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Widgets_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['eCommerce_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">eCommerce</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['eCommerce_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Frameworks_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Frameworks</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Frameworks_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Content_Delivery_Network_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Content Delivery Network</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Content_Delivery_Network_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Mobile_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Mobile</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Mobile_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Payment_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Payment</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Payment_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Audio_Video_Media_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Audio Video Media</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Audio_Video_Media_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Content_Management_System_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Content Management System</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Content_Management_System_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['JavaScript_Libraries_and_Functions_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">JavaScript Libraries and Functions</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['JavaScript_Libraries_and_Functions_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Advertising_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Advertising</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Advertising_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Verified_Link_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Verified Link</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Verified_Link_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Shipping_Providers_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Shipping Providers</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Shipping_Providers_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Email_Hosting_Providers_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Email Hosting Providers</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Email_Hosting_Providers_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Name_Servers_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Name Servers</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Name_Servers_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Web_Hosting_Providers_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Web Hosting Providers</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Web_Hosting_Providers_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['SSL_Certificates_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">SSL Certificates</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['SSL_Certificates_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Operating_Systems_and_Servers_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Operating Systems and Servers</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Operating_Systems_and_Servers_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Web_Servers_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Web Servers</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Web_Servers_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Verified_CDN_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Verified CDN</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Verified_CDN_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Web_Master_Registration_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Web Master Registration</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Web_Master_Registration_items'] ?></div>
                                    </div>
                                <?php } ?>
                                <?php
                                if ($startup_builtwith['Domain_Parking_items'] != '') {
                                    ?>
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Domain Parking</div>
                                        <div class="datasbloc__val small"><?php echo $startup_builtwith['Domain_Parking_items'] ?></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>

        <script src="https://assets.poool.fr/audit.min.js"></script>
        <script src="https://assets.poool.fr/access.min.js"></script>
        <script>
                                            window.addEventListener('load', () => {
                                                Audit
                                                        .init('UB0IB-OS245-ZNO3H-EH4IZ')
                                                        .config({cookies_enabled: true}, {gtm_auto_tracking_enabled: true})
                                                        .sendEvent('page-view', {type: 'premium'});
                                                const access = Access.init('UB0IB-OS245-ZNO3H-EH4IZ');
                                                access.config({
                                                    cookies_enabled: true,
                                                })
                                                        .on('release', function () {
                                                            access.destroy();
                                                            document.querySelector('#fiche_startup').remove();
                                                            document.querySelector('#bloc_radar').remove();
                                                            document.querySelector('#bloc_maturite').remove();
                                                        })



                                                Access
                                                        .init('UB0IB-OS245-ZNO3H-EH4IZ')
                                                        .config({
                                                            context: 'radar',
                                                            cookies_enabled: true,
                                                        })
                                                        .createPaywall({
                                                            target: '#bloc_radar',
                                                            content: '.content_radar',
                                                            mode: 'hide',
                                                            percent: 100,
                                                            pageType: 'premium',
                                                            cookies_enabled: true,
                                                        });
                                                Access
                                                        .init('UB0IB-OS245-ZNO3H-EH4IZ')
                                                        .config({
                                                            context: 'maturite',
                                                            cookies_enabled: true,
                                                        })
                                                        .createPaywall({
                                                            target: '#bloc_maturite',
                                                            content: '.content_maturite',
                                                            mode: 'hide',
                                                            percent: 100,
                                                            pageType: 'premium',
                                                            cookies_enabled: true,
                                                        });
                                                Access
                                                        .init('UB0IB-OS245-ZNO3H-EH4IZ')
                                                        .config({
                                                            context: 'finance',
                                                            cookies_enabled: true,
                                                        })
                                                        .createPaywall({
                                                            target: '#fiche_startup',
                                                            content: '.content_finance',
                                                            mode: 'hide',
                                                            percent: 80,
                                                            pageType: 'premium',
                                                            cookies_enabled: true,
                                                        });





                                            }
                                            )

        </script>



        <script>
                    (function (i, s, o, g, r, a, m) {
                        i['GoogleAnalyticsObject'] = r;
                        i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                        a = s.createElement(o),
                                m = s.getElementsByTagName(o)[0];
                        a.async = 1;
                        a.src = g;
                        m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>
            function getinfofounder(id, startup) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL ?>/bloc_founder.php?id=" + id + "&startup=" + startup,
                    success: function (o) {
                        document.getElementById("bloc_autre_founder").innerHTML = o;
                    }

                }
                );
            }
            function revendiquer() {
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL ?>/revendiquer_sup.php",
                    success: function (o) {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo URL ?>/revendique_email.php",
                            success: function (o) {
                                document.getElementById("grid").innerHTML = o;
                            }

                        }
                        );
                    }

                }
                );
            }


            $(document).ready(function () {
                $("#voir_plus").click(function () {
                    $("#long_fr_bloc2").show();
                    $("#voir_plus").hide();
                });
                $("#voir_moins").click(function () {
                    $("#long_fr_bloc2").hide();
                    $("#voir_plus").show();
                });
            });


        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectRegion(val) {
                const words = val.split('/');
                // $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function memoriser(id) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL ?>/memoriser.php",
                    data: 'id=' + id,

                    success: function (data) {
                        alert("Votre startup est mémorisée dans votre liste favoris");
                    }
                });
            }
        </script>
        <div class="modal fade" id="exampleModal_filtre" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="document" style="width: 800px !important;max-width: 604px !important;min-height: 400px;">
                <div class="modal-content" style="font-size: 14px;border-radius: 5px;width: 100%;min-height: 400px;">
                    <div id="grid">
                        <div class="zoom-anim-dialog" >




                            <!-- Login -->
                            <div class="tab-content" id="tab1" style="border-top-width: 0px;padding: 8px;">
                                <div style="text-align:center;">
                                    <h2 class="sub_title" style="line-height: 24px;font-size: 2.6rem;text-align: center;margin-top: 8px;margin-bottom: 16px;">Revendiquer<br>gratuitement cette startup</h2>
                                    <h5 style="text-align:center;font-size:16px;color: #425466;font-family: 'Euclid';line-height: 24px;margin-top: 24px;margin-bottom: 24px;">Prenez la main sur votre fiche startup et valorisez votre image auprès de toutes l’écosystème</h5>
                                </div>
                                <form method="post" action="<?php echo url ?>revendiquer-startup-validation" class="register">

                                    <div class="colgridform">
                                        <div class="gridformtext" style="margin-top: 8px;">
                                            <div>
                                                <label>Email</label>
                                                <input type="email" name="email" id="email" required="" value="<?php echo $_SESSION['data_login']; ?>" >
                                            </div>
                                            <div>
                                                <label>Startup</label>
                                                <input type="text" name="societe" required="" id="societe" value="<?php echo $_SESSION['startup_nom_revendiquer']; ?>" >
                                            </div>
                                        </div>
                                        <h5 style="text-align:left;font-size:16px;color: #425466;font-family: 'EuclidBold';line-height: 24px;margin-top: 16px;margin-bottom: 16px;">Demande de liaison</h5>
                                        <div style="margin-top: 16px;">
                                            <p style="font-family: 'EuclidLight';line-height: 16px!important"><strong>Utilisez une adresse e-mail de votre établissement</strong> : lorsque vous indiquez une adresse e-mail de votre établissement au moment de votre demande de liaison, vos données sont vérifiées rapidement.
                                                <br><br>Si vous ne disposez pas d’une adresse e-mail de l’établissement ou si votre adresse ne peut être vérifiée, des justificatifs supplémentaires pourront vous être demandés.</p>

                                        </div>
                                        <h5 style="text-align:left;font-size:16px;color: #425466;font-family: 'EuclidBold';line-height: 24px;margin-top: 16px;margin-bottom: 16px;">Les prochaines étapes</h5>
                                        <p style="font-family: 'EuclidLight';line-height: 24px!important">
                                            1. Sélectionnez votre startup et saisissez votre adresse e-mail professionnelle<br> 
                                            2. Vous recevez un e-mail de confirmation (sous 24/48h) avec un lien vers l’espace de mise à jour startup<br> 
                                            3. Retrouvez votre startup dans votre Dashboard, éditez votre fiche et accédez à vos statistiques.<br> 
                                        </p>                                     
                                        <div style="text-align:center;margin-top: 16px;margin-left: 224px;margin-right: 224px;height: 38px;margin-bottom: 32px;">
                                            <input type="button" onclick="revendiquer()" style="background: #00CCFF;border-radius: 4px;padding: 8px;width: 89px;font-family: 'Roboto';height: 38px;font-size: 12px;line-height: 12px;margin-right: 0px;margin-bottom: 0px;" name="register" value="Valider" />
                                        </div>
                                    </div>
                                </form>
                            </div>



                        </div>
                    </div>


                </div>
            </div>
        </div>
        <script type="text/javascript">window.gdprAppliesGlobally = true;
            (function () {
                function a(e) {
                    if (!window.frames[e]) {
                        if (document.body && document.body.firstChild) {
                            var t = document.body;
                            var n = document.createElement("iframe");
                            n.style.display = "none";
                            n.name = e;
                            n.title = e;
                            t.insertBefore(n, t.firstChild)
                        } else {
                            setTimeout(function () {
                                a(e)
                            }, 5)
                        }
                    }
                }
                function e(n, r, o, c, s) {
                    function e(e, t, n, a) {
                        if (typeof n !== "function") {
                            return
                        }
                        if (!window[r]) {
                            window[r] = []
                        }
                        var i = false;
                        if (s) {
                            i = s(e, t, n)
                        }
                        if (!i) {
                            window[r].push({command: e, parameter: t, callback: n, version: a})
                        }
                    }
                    e.stub = true;
                    function t(a) {
                        if (!window[n] || window[n].stub !== true) {
                            return
                        }
                        if (!a.data) {
                            return
                        }
                        var i = typeof a.data === "string";
                        var e;
                        try {
                            e = i ? JSON.parse(a.data) : a.data
                        } catch (t) {
                            return
                        }
                        if (e[o]) {
                            var r = e[o];
                            window[n](r.command, r.parameter, function (e, t) {
                                var n = {};
                                n[c] = {returnValue: e, success: t, callId: r.callId};
                                a.source.postMessage(i ? JSON.stringify(n) : n, "*")
                            }, r.version)
                        }
                    }
                    if (typeof window[n] !== "function") {
                        window[n] = e;
                        if (window.addEventListener) {
                            window.addEventListener("message", t, false)
                        } else {
                            window.attachEvent("onmessage", t)
                        }
                    }
                }
                e("__tcfapi", "__tcfapiBuffer", "__tcfapiCall", "__tcfapiReturn");
                a("__tcfapiLocator");
                (function (e) {
                    var t = document.createElement("script");
                    t.id = "spcloader";
                    t.type = "text/javascript";
                    t.async = true;
                    t.src = "https://sdk.privacy-center.org/" + e + "/loader.js?target=" + document.location.hostname;
                    t.charset = "utf-8";
                    var n = document.getElementsByTagName("script")[0];
                    n.parentNode.insertBefore(t, n)
                })("f0c52c20-b8cf-485b-a4b4-c222da28676d")
            })();</script>
    </body>
</html>