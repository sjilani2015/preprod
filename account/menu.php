<ul>
    <li <?php echo ($currentPage == "datas") ? 'class="active"' : ""; ?>><a href="<?php echo URL ?>/mon-compte" title="Informations personnelles">Informations personnelles</a></li>
    <li <?php echo ($currentPage == "lists") ? 'class="active"' : ""; ?>><a href="<?php echo URL ?>/mes-listes" title="Mes listes">Gestion des listes</a></li>
    <li <?php echo ($currentPage == "startups") ? 'class="active"' : ""; ?>><a href="<?php echo URL ?>/mes-startups" title="Mes startups">Mes startups</a></li>
    <li <?php echo ($currentPage == "notification") ? 'class="active"' : ""; ?>><a href="<?php echo URL ?>/mes-notifications" title="Mes notifications">Mes notifications</a></li>
    <li <?php echo ($currentPage == "invoices") ? 'class="active"' : ""; ?>><a href="<?php echo URL ?>/mes-factures" title="Mes factures">Mes factures</a></li>
    <li><a href="<?php echo URL ?>/deconnexion">Déconnexion</a></li>
</ul>