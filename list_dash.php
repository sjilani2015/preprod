<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');


//if(!isset($_SESSION['data_login'])){
//  echo "<script>window.location='".url."nos-offres'</script>";
//}
$monUrl = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$_SESSION['utm_source'] = $monUrl;
//$id_dash = degenerate_id($_GET['id']);
$id_dash = degenerate_id(12219);
$dashboard = mysqli_fetch_array(mysqli_query($link, "select * from user_save_list where id=" . $id_dash));
$ligne_france = mysqli_fetch_array(mysqli_query($link, "select * from indice_france where id=1"));


$year = date("Y");
$previousyear = $year - 1;

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}

$date1 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d'), date('y')));
$date2 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d') - 30, date('y')));
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">

        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/maps.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/franceLow.min.js"></script>

        <script>
            const   themeColorBlue = am4core.color('#00bff0'),
                    themeColorFill = am4core.color('#E1F2FA'),
                    themeColorGrey = am4core.color('#dadada'),
                    themeColorLightGrey = am4core.color('#F5F5F5'),
                    themeColorDarkgrey = am4core.color("#33333A"),
                    themeColorLine = am4core.color("#87D6E3"),
                    themeColorLineRed = am4core.color("#FF7C7C"),
                    themeColorLineGreen = am4core.color("#21D59B"),
                    themeColorColumns = am4core.color("#E1F2FA"),
                    labelColor = am4core.color('#798a9a');
        </script>
        <style>
            a{
                color:#323239;
            }
        </style>
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">

                    <section class="module">
                        <div class="module__title">Étude segment <?php echo utf8_encode($dashboard['nom']) ?></div>
                        <div class="ctg ctg--1_3">
                            <aside class="bloc text-center">
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Startups françaises</div>
                                    <div class="datasbloc__val"><?php
                                        echo number_format($dashboard['nbr_startups'], 0, ".", " ");
                                        ?>
                                    </div>
                                    <div class="datagrid__subtitle">
                                        <?php echo str_replace(',0', '', number_format($dashboard['taux_creation'], 1, ",", "")); ?>%                    
                                        <?php
                                        if ($dashboard['taux_creation'] < 0) {
                                            ?>
                                            <span class="ico-decrease"></span>
                                        <?php } ?>
                                        <?php
                                        if ($dashboard['taux_creation'] > 0) {
                                            ?>
                                            <span class="ico-raise"></span>
                                        <?php } ?>
                                        <?php
                                        if ($dashboard['taux_creation'] == 0) {
                                            ?>
                                            <span class="ico-stable"></span>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Emplois créés</div>
                                    <div class="datasbloc__val"><?php
                                        echo number_format($dashboard['emploi'], 0, ".", " ");
                                        ?>
                                    </div>
                                    <div class="datagrid__subtitle">
                                        <?php echo str_replace(',0', '', number_format($dashboard['taux_emploi'], 1, ",", "")); ?>%                    
                                        <?php
                                        if ($dashboard['taux_emploi'] < 0) {
                                            ?>
                                            <span class="ico-decrease"></span>
                                        <?php } ?>
                                        <?php
                                        if ($dashboard['taux_emploi'] > 0) {
                                            ?>
                                            <span class="ico-raise"></span>
                                        <?php } ?>
                                        <?php
                                        if ($dashboard['taux_emploi'] == 0) {
                                            ?>
                                            <span class="ico-stable"></span>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Total fonds levés France</div>
                                    <div class="datasbloc__val"><?php echo number_format(($dashboard['total_invest'] / 1000), 1, ",", " ") ?> M€</div>
                                    <div class="datagrid__subtitle">
                                        <?php echo str_replace(',0', '', number_format($dashboard['taux_leve'], 1, ",", "")); ?>%                    
                                        <?php
                                        if ($dashboard['taux_leve'] < 0) {
                                            ?>
                                            <span class="ico-decrease"></span>
                                        <?php } ?>
                                        <?php
                                        if ($dashboard['taux_leve'] > 0) {
                                            ?>
                                            <span class="ico-raise"></span>
                                        <?php } ?>
                                        <?php
                                        if ($dashboard['taux_leve'] == 0) {
                                            ?>
                                            <span class="ico-stable"></span>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Financements France</div>
                                    <div class="datasbloc__val"><?php echo str_replace(',0', '', number_format($dashboard['invest_france'], 1, ",", " ")) ?>%</div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Startups France</div>
                                    <div class="datasbloc__val"><?php echo str_replace(',0', '', number_format($dashboard['nb_france'], 1, ",", " ")) ?>%</div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Nombre d'investisseurs</div>
                                    <div class="datasbloc__val"><?php echo str_replace(',0', '', number_format(($dashboard['nb_investisseurs']), 0, ",", " ")) ?></div>
                                </div>
                            </aside>

                            <div class="bloc">
                                <div class="ctg ctg--2_2 ctg--fullheight">
                                    <div class="chart chart--radar">
                                        <div id="chart-radar"></div>
                                    </div>
                                    <script>
                                        am4core.ready(function () {


                                            /* Create chart instance */
                                            var chart = am4core.create("chart-radar", am4charts.RadarChart);
                                            /* Add data */
                                            chart.data = [{
                                                    "date": "A",
                                                    "value": <?php echo number_format($ligne_france['maturite'], 1, ".", ""); ?>,
                                                    "value2": <?php echo $dashboard['maturite']; ?>
                                                }, {
                                                    "date": "B",
                                                    "value": <?php echo number_format($ligne_france['transparence'], 1, ".", ""); ?>,
                                                    "value2": <?php echo $dashboard['transparence'] ?>
                                                }, {
                                                    "date": "C",
                                                    "value": <?php echo number_format($ligne_france['anciennete'], 1, ".", ""); ?>,
                                                    "value2": <?php echo $dashboard['anciennete'] ?>
                                                }, {
                                                    "date": "D",
                                                    "value": <?php echo number_format($ligne_france['traction_likedin'], 1, ".", ""); ?>,
                                                    "value2": <?php echo $dashboard['traction_likedin'] ?>
                                                }];
                                            chart.paddingTop = 0;
                                            chart.paddingBottom = 0;
                                            chart.paddingLeft = 0;
                                            chart.paddingRight = 0;
                                            /* Create axes */
                                            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                            categoryAxis.dataFields.category = "date";
                                            categoryAxis.renderer.ticks.template.strokeWidth = 2;
                                            categoryAxis.renderer.labels.template.fontSize = 11;
                                            categoryAxis.renderer.labels.template.fill = labelColor;
                                            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                            valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
                                            valueAxis.renderer.axisFills.template.fillOpacity = 0.05;
                                            valueAxis.renderer.labels.template.fontSize = 11;
                                            valueAxis.renderer.gridType = "polygons"


                                            /* Create and configure series */
                                            function createSeries(field, name, themeColor) {
                                                var series = chart.series.push(new am4charts.RadarSeries());
                                                series.dataFields.valueY = field;
                                                series.dataFields.categoryX = "date";
                                                series.strokeWidth = 2;
                                                series.fill = themeColor;
                                                series.fillOpacity = 0.3;
                                                series.stroke = themeColor;
                                                series.name = name;
                                                // Show bullets ?
                                                //let circleBullet = series.bullets.push(new am4charts.CircleBullet());
                                                //circleBullet.circle.stroke = am4core.color('#fff');
                                                //circleBullet.circle.strokeWidth = 2;
                                            }


                                            createSeries("value", "Ma liste", themeColorBlue);
                                            createSeries("value2", "France", themeColorGrey);
                                            chart.legend = new am4charts.Legend();
                                            chart.legend.position = "top";
                                            chart.legend.useDefaultMarker = true;
                                            chart.legend.fontSize = "11";
                                            chart.legend.color = themeColorGrey;
                                            chart.legend.labels.template.fill = labelColor;
                                            chart.legend.labels.template.textDecoration = "none";
                                            chart.legend.valueLabels.template.textDecoration = "none";
                                            let as = chart.legend.labels.template.states.getKey("active");
                                            as.properties.textDecoration = "line-through";
                                            as.properties.fill = themeColorDarkgrey;
                                            let marker = chart.legend.markers.template.children.getIndex(0);
                                            marker.cornerRadius(12, 12, 12, 12);
                                            marker.width = 20;
                                            marker.height = 20;
                                        });
                                    </script>
                                    <div class="datagrid">
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">A :</span> Maturité
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $dashboard['maturite']); ?> 

                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['maturite'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">B :</span> Transarence
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $dashboard['transparence']); ?> 

                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['transparence'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">C :</span> Ancienneté
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $dashboard['anciennete']); ?> 

                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo number_format($ligne_france['anciennete'], 1, ",", ""); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">D :</span> Traction Linkedin
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $dashboard['traction_likedin']); ?> 

                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo number_format($ligne_france['traction_likedin'], 1, ",", ""); ?></div>
                                        </div>


                                    </div>

                                    <div class="bloc-actions">
                                        <div class="bloc-actions__ico">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Financement segment <?php echo utf8_encode($dashboard['nom']) ?></div>

                        <div class="ctg ctg--3_3">
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financements Seed</div>
                                        <div class="datasbloc__val nowrap">Moy. <?php echo str_replace(",0", "", number_format($dashboard['moy_1'] / 1000, 1, ",", " ")) ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($dashboard['nb_1']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($dashboard['min_1'] / 1000, 1, ",", " ")) ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($dashboard['max_1'] / 1000, 1, ",", " ")) ?> M€ </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financements Croissance</div>
                                        <div class="datasbloc__val nowrap">Moy. <?php echo str_replace(",0", "", number_format($dashboard['moy_2'] / 1000, 1, ",", " ")) ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($dashboard['nb_2']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($dashboard['min_2'] / 1000, 1, ",", " ")) ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($dashboard['max_2'] / 1000, 1, ",", " ")) ?> M€</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financements Développement</div>
                                        <div class="datasbloc__val nowrap">Moy. <?php echo str_replace(",0", "", number_format($dashboard['moy_3'] / 1000, 1, ",", " ")) ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($dashboard['nb_3']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($dashboard['min_3'] / 1000, 1, ",", " ")) ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($dashboard['max_3'] / 1000, 1, ",", " ")) ?> M€</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Nouvelles statups ajoutées</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="dateCell text-center">Création</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $list_prob = explode(",", $dashboard['last_startup']);
                                            $nbrs = count($list_prob);
                                            for ($i = 0; $i < $nbrs; $i++) {

                                                $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation,date_complete from startup where id=" . $list_prob[$i]));
                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="logoNameCell">
                                                            <div class="logo">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt="<?php echo utf8_encode($ma_startup['nom']); ?>" /></a>
                                                            </div>
                                                            <div class="name"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo utf8_encode($ma_startup['nom']); ?></a></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($ma_startup['date_complete']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Derniers deals</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="moneyCell text-center">Montant</th>
                                                <th class="dateCell text-center">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $list_prob = explode(",", $dashboard['last_lf']);
                                            $nbrs = count($list_prob);
                                            for ($i = 0; $i < $nbrs; $i++) {

                                                $ma_startup = mysqli_fetch_array(mysqli_query($link, "select startup.id,startup.nom,startup.logo,startup.creation,startup.date_complete,lf.date_ajout,lf.montant from startup inner join lf on lf.id_startup=startup.id where lf.id=" . $list_prob[$i]));
                                                ?>


                                                <tr>
                                                    <td>
                                                        <div class="logoNameCell">
                                                            <div class="logo">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt="<?php echo utf8_encode($ma_startup['nom']); ?>" /></a>
                                                            </div>
                                                            <div class="name"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo utf8_encode($ma_startup['nom']); ?></a></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center dateCell"><?php
                                                        if ($ma_startup['montant'] != 0)
                                                            echo str_replace(",0", "", number_format($ma_startup['montant'] / 1000, 1, ",", " ")) . " M€";
                                                        else
                                                            echo 'NC';
                                                        ?></td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($ma_startup['date_ajout']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                         <div class="module__title">Localisation</div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Géographie du segment</div>

                                <div class="chart chart--map">
                                    <div id="chart-map"></div>
                                </div>

                                <script>

                                    // Create map instance
                                    var chart = am4core.create("chart-map", am4maps.MapChart);
                                    chart.paddingTop = 0;
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    // Set map definition
                                    chart.geodata = am4geodata_franceLow;
                                    // Set projection
                                    chart.projection = new am4maps.projections.Miller();
                                    // Create map polygon series
                                    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
                                    //Set min/max fill color for each area
                                    polygonSeries.heatRules.push({
                                        property: "fill",
                                        target: polygonSeries.mapPolygons.template,
                                        //min: chart.colors.getIndex(1).brighten(1),
                                        //max: chart.colors.getIndex(1).brighten(-0.3),
                                        logarithmic: true
                                    });
                                    // Make map load polygon data (state shapes and names) from GeoJSON
                                    polygonSeries.useGeodata = true;
                                    // Set heatmap values for each state
<?php
$region_Bretagne = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=17 and id_secteur=" . $id_dash));
$region_PaysdelaLoire = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=12 and id_secteur=" . $id_dash));
$region_GrandEst = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=18 and id_secteur=" . $id_dash));
$region_Nouvelle_Aquitaine = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=1 and id_secteur=" . $id_dash));
$region_Hauts_de_France = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=7 and id_secteur=" . $id_dash));
$region_azur = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=2 and id_secteur=" . $id_dash));
$region_Occitanie = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=4 and id_secteur=" . $id_dash));
$region_Auvergne_Rhone_Alpes = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=5 and id_secteur=" . $id_dash));
$region_Corse = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=3 and id_secteur=" . $id_dash));
$region_Bourgogne_Franche_Comte = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=11 and id_secteur=" . $id_dash));
$region_ile_e_France = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=8 and id_secteur=" . $id_dash));
$region_Centre_Val_Loire = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=19 and id_secteur=" . $id_dash));
$region_Normandie = mysqli_fetch_array(mysqli_query($link, "select nbr_startups from secteur_region where id_region=15 and id_secteur=" . $id_dash));
?>
                                    polygonSeries.data = [
                                        {
                                            "id": "FR-BRE",
                                            "value": <?php echo $region_Bretagne['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        },
                                        {
                                            "id": "FR-IDF",
                                            "value": <?php echo $region_ile_e_France['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        },
                                        {
                                            "id": "FR-HDF",
                                            "value": <?php echo $region_Hauts_de_France['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        },
                                        {
                                            "id": "FR-GES",
                                            "value": <?php echo $region_GrandEst['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        },
                                        {
                                            "id": "FR-NOR",
                                            "value": <?php echo $region_Normandie['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        }, {
                                            "id": "FR-PDL",
                                            "value": <?php echo $region_PaysdelaLoire['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        }, {
                                            "id": "FR-CVL",
                                            "value": <?php echo $region_Centre_Val_Loire['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        }, {
                                            "id": "FR-BFC",
                                            "value": <?php echo $region_Bourgogne_Franche_Comte['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        }, {
                                            "id": "FR-ARA",
                                            "value": <?php echo $region_Auvergne_Rhone_Alpes['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        }, {
                                            "id": "FR-PAC",
                                            "value": <?php echo $region_azur['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        }, {
                                            "id": "FR-NAQ",
                                            "value": <?php echo $region_Nouvelle_Aquitaine['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        }, {
                                            "id": "FR-OCC",
                                            "value": <?php echo $region_Occitanie['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        }, {
                                            "id": "FR-COR",
                                            "value": <?php echo $region_Corse['nbr_startups'] ?>,
                                            //"fill": am4core.color("#ff0000"),
                                        },
                                    ];
                                    // Configure series tooltip
                                    var polygonTemplate = polygonSeries.mapPolygons.template;
                                    polygonTemplate.tooltipText = "[#fff font-size: 11px]{name}: {value}[/]";
                                    polygonTemplate.nonScalingStroke = true;
                                    polygonTemplate.strokeWidth = 0.7;
                                    polygonTemplate.propertyFields.fill = "fill";
                                    polygonTemplate.stroke = themeColorGrey;
                                    polygonTemplate.properties.fill = themeColorLightGrey;
                                    // Create hover state and set alternative fill color
                                    var hs = polygonTemplate.states.create("hover");
                                    hs.properties.fill = themeColorBlue;
                                    hs.properties.color = am4core.color("#ff0000");
                                    chart.seriesContainer.draggable = false;
                                    chart.seriesContainer.resizable = false;
                                    chart.maxZoomLevel = 1;
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Startups du segment par département</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="iconCell">Évol.</th>
                                                <th>Départements</th>
                                                <th class="text-center">Startups</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_last_startup_region = explode(',', $dashboard['list']);
                                            $nb_region = count($sql_last_startup_region);
                                            $ch = "";
                                            for ($i = 0; $i < $nb_region; $i++) {
                                                if ($sql_last_startup_region[$i] != "") {
                                                    $ch .= " or id=" . $sql_last_startup_region[$i] . " ";
                                                }
                                            }
                                            $xx = mysqli_query($link, "select count(*) as nb,region_new from startup where status=1 and (id=0 " . $ch . " ) group by region_new order by nb desc");
                                            while ($data_region = mysqli_fetch_array($xx)) {
                                                ?>
                                                <tr>

                                                    <td><span class="ico-raise"></span></td>

                                                    <td><?php if($data_region['region_new']!='')echo utf8_encode($data_region['region_new']); else echo 'NC'; ?></td>
                                                    <td class="text-center"><?php echo $data_region['nb']; ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Marché et activités secteur <?php echo utf8_encode($dashboard['nom']) ?></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Dynamiques du secteur </div>
                                <div class="chart chart--xy">
                                    <div id="chart-dynamiquesecteur"></div>
                                </div>
                                <script>
                                    am4core.ready(function () {
                                        var chart = am4core.create("chart-dynamiquesecteur", am4charts.XYChart);
                                        chart.paddingTop = 0;
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;
                                        chart.data = [
<?php
for ($i = 2011; $i <= 2021; $i++) {
    $nb_creation = mysqli_num_rows((mysqli_query($link, "select startup.id from startup inner join activite on activite.id_startup=startup.id where startup.status=1 and sup_radie=0 and activite.secteur=" . $id_dash . " and year(startup.date_add)='" . $i . "'")));
    $nb_defaillance = mysqli_num_rows((mysqli_query($link, "select startup.id from startup inner join activite on activite.id_startup=startup.id where startup.status=1 and sup_radie=0 and activite.secteur=" . $id_dash . " and year(startup.dt_radie)='" . $i . "'")));
    $nb_aquisition = mysqli_num_rows((mysqli_query($link, "select startup.id from startup inner join activite on activite.id_startup=startup.id inner join lf on lf.id_startup=startup.id where startup.status=1 and sup_radie=0 and activite.secteur=" . $id_dash . " and year(lf.date_ajout)='" . $i . "' and (lf.rachat=1 or rachat=2)")));
    ?>
                                                {date: new Date(<?php echo $i; ?>, 5, 12), value1: <?php echo $nb_creation; ?>, value2: <?php echo $nb_defaillance; ?>, value3: <?php echo $nb_aquisition; ?>, previousDate: new Date(<?php echo $i; ?>, 5, 5)},
    <?php
}
?>

                                        ]

                                        // Create axes
                                        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                                        dateAxis.renderer.minGridDistance = 50;
                                        dateAxis.renderer.labels.template.fontSize = 11;
                                        dateAxis.renderer.labels.template.fill = labelColor;
                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.renderer.labels.template.fill = labelColor;
                                        /* Create and configure series */
                                        function createSeries(field, name, themeColor) {
                                            var series = chart.series.push(new am4charts.LineSeries());
                                            series.dataFields.valueY = field;
                                            series.dataFields.dateX = "date";
                                            series.strokeWidth = 2;
                                            series.stroke = themeColor;
                                            series.name = name;
                                            series.tensionX = 0.77;
                                            series.fill = themeColor;
                                        }


                                        createSeries("value1", "Créations", themeColorBlue);
                                        createSeries("value2", "Défaillances", themeColorLineGreen);
                                        createSeries("value3", "Acquisitions - IPO", themeColorLineRed);
                                        chart.zoomOutButton.disabled = true;
                                        chart.legend = new am4charts.Legend();
                                        chart.legend.position = "top";
                                        chart.legend.useDefaultMarker = true;
                                        chart.legend.fontSize = "11";
                                        chart.legend.color = themeColorGrey;
                                        chart.legend.labels.template.fill = labelColor;
                                        chart.legend.labels.template.textDecoration = "none";
                                        chart.legend.valueLabels.template.textDecoration = "none";
                                        let as = chart.legend.labels.template.states.getKey("active");
                                        as.properties.textDecoration = "line-through";
                                        as.properties.fill = themeColorDarkgrey;
                                        let marker = chart.legend.markers.template.children.getIndex(0);
                                        marker.cornerRadius(12, 12, 12, 12);
                                        marker.width = 20;
                                        marker.height = 20;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                            <div class="bloc">

                                <?php
                                $sql_region = mysqli_query($link, "select count(*) as nb, activite.sous_secteur from startup inner join activite on activite.id_startup=startup.id where startup.status=1 and sup_radie=0 and activite.sous_secteur!=0 and activite.secteur=" . $id_dash . " group by activite.sous_secteur order by nb desc limit 10");
                                $nbs = mysqli_num_rows($sql_region);
                                if ($nbs > 0) {
                                    ?>
                                    <div class="bloc__title">Répartition des startups par sous-secteurs <a href="">Voir la liste &raquo;</a></div>
                                    <div class="tablebloc">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th class="iconCell">Évol.</th>
                                                    <th>Sous secteur</th>
                                                    <th class="text-center">Startups</th>
                                                </tr>
                                            </tbody>
                                            <tbody>
                                                <?php
                                                $i = 1;
                                                while ($data = mysqli_fetch_array($sql_region)) {
                                                    $sql_dep_tot33 = mysqli_num_rows(mysqli_query($link, "select startup.id from startup inner join activite on activite.id_startup=startup.id where startup.status=1 and sup_radie=0 and activite.sous_secteur!=0 and activite.secteur=" . $id_dash . " and date(startup.date_add)>'" . $date1 . "'"));
                                                    $sql_dep33 = mysqli_fetch_array(mysqli_query($link, "select count(*) as nb from startup inner join activite on activite.id_startup=startup.id where startup.status=1 and sup_radie=0 and activite.sous_secteur!=0 and activite.secteur=" . $id_dash . " and date(startup.date_add)>'" . $date2 . "'"));
                                                    $diff33 = $sql_dep33['nb'] - $sql_dep_tot33;
                                                    $sss = mysqli_fetch_array(mysqli_query($link, "select * from sous_secteur where id=" . $data['sous_secteur']));
                                                    ?>
                                                    <tr>
                                                        <?php
                                                        if ($diff33 < 0) {
                                                            ?>
                                                            <td><span class="ico-decrease"></span></td>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($diff33 > 0) {
                                                            ?>
                                                            <td><span class="ico-raise"></span></td>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if ($diff33 == 0) {
                                                            ?>
                                                            <td><span class="ico-stable"></span></td>
                                                            <?php
                                                        }
                                                        ?>
                                                        <td><?php echo utf8_encode($sss['nom_sous_secteur']) ?></td>
                                                        <td class="text-center"><?php echo $data['nb'] ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Dernières créations de startups <?php echo utf8_encode($dashboard['nom']) ?> <a href="">Voir la liste &raquo;</a></div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="dateCell text-center">Création</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_naf = mysqli_query($link, "select startup.logo,startup.nom,startup.creation,activite.activite,startup.date_complete,startup.id from startup inner join activite on activite.id_startup=startup.id where startup.status=1 and sup_radie=0 and activite.secteur=" . $id_dash . " order by date_complete desc limit 5");
                                            while ($data_naf = mysqli_fetch_array($sql_naf)) {
                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="logoNameCell">
                                                            <div class="logo">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($data_naf['id']) . "/" . urlWriting(strtolower($data_naf["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $data_naf['logo']) ?>" alt="<?php echo utf8_encode($data_naf['nom']); ?>" /></a>
                                                            </div>
                                                            <div class="name"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($data_naf['id']) . "/" . urlWriting(strtolower($data_naf["nom"])) ?>" target="_blank"><?php echo utf8_encode($data_naf['nom']); ?></a></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($data_naf['date_complete']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Dernières acquisitions de startups <?php echo utf8_encode($dashboard['nom']) ?> <a href="">Voir la liste &raquo;</a></div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="moneyCell text-center">Montant</th>
                                                <th class="dateCell text-center">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_secteur2 = mysqli_query($link, "SELECT startup.logo,startup.id,startup.nom,lf.date_ajout,lf.montant FROM `startup` inner join activite on startup.id=activite.id_startup inner join lf on lf.id_startup=startup.id where startup.status=1 and sup_radie=0 and activite.secteur=" . $id_dash . " and lf.rachat=1 order by date_ajout desc limit 5");
                                            while ($data_secteur2 = mysqli_fetch_array($sql_secteur2)) {
                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="logoNameCell">
                                                            <div class="logo">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($data_secteur2['id']) . "/" . urlWriting(strtolower($data_secteur2["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $data_secteur2['logo']) ?>" alt="<?php echo utf8_encode($data_secteur2['nom']); ?>" /></a>
                                                            </div>
                                                            <div class="name"> <a href="<?php echo URL . '/fr/startup-france/' . generate_id($data_secteur2['id']) . "/" . urlWriting(strtolower($data_secteur2["nom"])) ?>" target="_blank"><?php echo utf8_encode($data_secteur2['nom']); ?></a></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center moneyCell"><?php
                                                        if ($data_secteur2['montant'] != 0)
                                                            echo str_replace(",0", "", number_format($data_secteur2['montant'] / 1000, 1, ",", " ")) . " M€";
                                                        else
                                                            echo 'NC';
                                                        ?></td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($data_secteur2['date_ajout']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>



                    <section class="module module--fundraising">
                        <div class="module__title">Dernières levées de fonds secteur <?php echo utf8_encode($dashboard['nom']) ?></div>
                        <div class="carousel" data-flickity='{"groupCells": true, "contain": true, "prevNextButtons": false}'>
                            <?php
                            $list_last_lf = explode(",", $dashboard['last_lf']);
                            $nbrs = count($list_last_lf);
                            for ($i = 0; $i < $nbrs; $i++) {

                                $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation from startup where id=" . $list_last_lf[$i]));
                                $data_capital = mysqli_fetch_array(mysqli_query($link, "select lf.montant from lf  where id_startup=" . $list_last_lf[$i]));
                                $score = mysqli_fetch_array(mysqli_query($link, "SELECT secteur_activite.`competition_secteur` FROM `secteur_activite` inner join startup_score on secteur_activite.id=startup_score.id_secteur WHERE startup_score.id_startup=" . $list_last_lf[$i]));
                                ?>
                                <div class="carousel-cell">
                                    <div class="carousel-cell__content cards__bloc">
                                        <div class="cards__bloc__thumb"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt=""></div>
                                        <div class="cards__bloc__infos"><?php echo utf8_encode($ma_startup['nom']); ?></div>
                                        <div class="cards__bloc__subtitle">Dernier montant levé</div>
                                        <div class="cards__bloc__amount"><?php echo str_replace(",0", "", number_format($data_capital['montant'] / 1000, 1, ",", " ")); ?> M€</div>
                                        <div class="cards__bloc__subtitle">Création</div>
                                        <div class="cards__bloc__amount"><?php echo $ma_startup['creation']; ?></div>
                                        <div class="cards__bloc__small"><?php echo utf8_encode($ma_startup['short_fr']); ?></div>
                                        <div class="cards__bloc__cta"><a href="" title="">Voir plus</a></div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                        </div>
                    </section>

                    <section class="module module--fundraising">
                        <div class="module__title">Dernières augmentations de capital secteur <?php echo utf8_encode($dashboard['nom']) ?></div>
                        <div class="carousel" data-flickity='{"groupCells": true, "contain": true, "prevNextButtons": false}'>

                            <?php
                            $list_capital = explode(",", $dashboard['last_capital']);
                            $nbrs = count($list_capital);
                            for ($i = 0; $i < $nbrs; $i++) {

                                $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation from startup where id=" . $list_capital[$i]));
                                $dernier_capital = mysqli_fetch_array(mysqli_query($link, "select * from startup_capital where id_startup=" . $list_capital[$i] . " order by dt desc limit 1"));
                                $score = mysqli_fetch_array(mysqli_query($link, "SELECT secteur_activite.`competition_secteur` FROM `secteur_activite` inner join startup_score on secteur_activite.id=startup_score.id_secteur WHERE startup_score.id_startup=" . $list_capital[$i]));
                                ?>

                                <div class="carousel-cell">
                                    <div class="carousel-cell__content cards__bloc">
                                        <div class="cards__bloc__thumb"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt=""></div>
                                        <div class="cards__bloc__infos"><?php echo $ma_startup['nom']; ?></div>
                                        <div class="cards__bloc__subtitle">Augmentation du capital</div>
                                        <div class="cards__bloc__amount"><?php echo str_replace(",0", "", number_format($dernier_capital['capital'] / 1000, 1, ",", " ")); ?> k€</div>
                                        <div class="cards__bloc__subtitle">Création</div>
                                        <div class="cards__bloc__amount"><?php echo $ma_startup['creation']; ?></div>
                                        <div class="cards__bloc__small"><?php echo utf8_encode($ma_startup['short_fr']); ?></div>
                                        <div class="cards__bloc__cta"><a href="" title="">Voir plus</a></div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </section>

                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
                                    (function (i, s, o, g, r, a, m) {
                                        i['GoogleAnalyticsObject'] = r;
                                        i[r] = i[r] || function () {
                                            (i[r].q = i[r].q || []).push(arguments)
                                        }, i[r].l = 1 * new Date();
                                        a = s.createElement(o),
                                                m = s.getElementsByTagName(o)[0];
                                        a.async = 1;
                                        a.src = g;
                                        m.parentNode.insertBefore(a, m)
                                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                    ga('create', 'UA-36251023-1', 'auto');
                                    ga('send', 'pageview');
        </script>
    </body>
</html>