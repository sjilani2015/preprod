<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="<?=CSS_PATH;?>app.css?<?=time();?>">
<meta name="theme-color" content="#ffffff">
<link rel="shortcut icon" href="<?=IMG_PATH;?>myfs.ico">
<!--[if lt IE 10]><script async src="<?=JS_PATH;?>vendor/classlist-polyfill.min.js"></script><![endif]-->