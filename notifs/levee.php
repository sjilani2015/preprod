<?php

$base = dirname(dirname(__FILE__));
include($base . "/include/db.php");
require $base . '/phpmailer/PHPMailerAutoload.php';
ini_set("display_errors", 1);
error_reporting(1);

function generate_id($id) {
    $nb = ($id * 11) + 5124;
    return $nb;
}

function urlWriting($str, $utf8 = true) {
    $str = (string) $str;
    if (is_null($utf8)) {
        if (!function_exists('mb_detect_encoding')) {
            $utf8 = (strtolower(mb_detect_encoding($str)) == 'utf-8');
        } else {
            $length = strlen($str);
            $utf8 = true;
            for ($i = 0; $i < $length; $i++) {
                $c = ord($str[$i]);
                if ($c < 0x80)
                    $n = 0;# 0bbbbbbb
                elseif (($c & 0xE0) == 0xC0)
                    $n = 1;# 110bbbbb
                elseif (($c & 0xF0) == 0xE0)
                    $n = 2;# 1110bbbb
                elseif (($c & 0xF8) == 0xF0)
                    $n = 3;# 11110bbb
                elseif (($c & 0xFC) == 0xF8)
                    $n = 4;# 111110bb
                elseif (($c & 0xFE) == 0xFC)
                    $n = 5;# 1111110b
                else
                    return false;# Does not match any model
                for ($j = 0; $j < $n; $j++) { # n bytes matching 10bbbbbb follow ?
                    if (( ++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80)) {
                        $utf8 = false;
                        break;
                    }
                }
            }
        }
    }

    if (!$utf8)
        $str = utf8_encode($str);

    $transliteration = array(
        'Ĳ' => 'I', 'Ö' => 'O', 'Œ' => 'O', 'Ü' => 'U', 'ä' => 'a', 'æ' => 'a',
        'ĳ' => 'i', 'ö' => 'o', 'œ' => 'o', 'ü' => 'u', 'ß' => 's', 'ſ' => 's',
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A',
        'Æ' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Ç' => 'C', 'Ć' => 'C',
        'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D', 'È' => 'E',
        'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E', 'Ę' => 'E', 'Ě' => 'E',
        'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G', 'Ġ' => 'G', 'Ģ' => 'G',
        'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
        'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I', 'İ' => 'I', 'Ĵ' => 'J',
        'Ķ' => 'K', 'Ľ' => 'K', 'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ł' => 'L',
        'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N', 'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O',
        'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O',
        'Ŏ' => 'O', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Ş' => 'S',
        'Ŝ' => 'S', 'Ș' => 'S', 'Š' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
        'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ū' => 'U', 'Ů' => 'U',
        'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U', 'Ŵ' => 'W', 'Ŷ' => 'Y',
        'Ÿ' => 'Y', 'Ý' => 'Y', 'Ź' => 'Z', 'Ż' => 'Z', 'Ž' => 'Z', 'à' => 'a',
        'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
        'å' => 'a', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
        'ď' => 'd', 'đ' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
        'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e', 'ƒ' => 'f',
        'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h', 'ħ' => 'h',
        'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i', 'ĩ' => 'i',
        'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĵ' => 'j', 'ķ' => 'k', 'ĸ' => 'k',
        'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l', 'ŀ' => 'l', 'ñ' => 'n',
        'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n', 'ŋ' => 'n', 'ò' => 'o',
        'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o',
        'ŏ' => 'o', 'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'ś' => 's', 'š' => 's',
        'ť' => 't', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ū' => 'u', 'ů' => 'u',
        'ű' => 'u', 'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ÿ' => 'y',
        'ý' => 'y', 'ŷ' => 'y', 'ż' => 'z', 'ź' => 'z', 'ž' => 'z', 'Α' => 'A',
        'Ά' => 'A', 'Ἀ' => 'A', 'Ἁ' => 'A', 'Ἂ' => 'A', 'Ἃ' => 'A', 'Ἄ' => 'A',
        'Ἅ' => 'A', 'Ἆ' => 'A', 'Ἇ' => 'A', 'ᾈ' => 'A', 'ᾉ' => 'A', 'ᾊ' => 'A',
        'ᾋ' => 'A', 'ᾌ' => 'A', 'ᾍ' => 'A', 'ᾎ' => 'A', 'ᾏ' => 'A', 'Ᾰ' => 'A',
        'Ᾱ' => 'A', 'Ὰ' => 'A', 'ᾼ' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D',
        'Ε' => 'E', 'Έ' => 'E', 'Ἐ' => 'E', 'Ἑ' => 'E', 'Ἒ' => 'E', 'Ἓ' => 'E',
        'Ἔ' => 'E', 'Ἕ' => 'E', 'Ὲ' => 'E', 'Ζ' => 'Z', 'Η' => 'I', 'Ή' => 'I',
        'Ἠ' => 'I', 'Ἡ' => 'I', 'Ἢ' => 'I', 'Ἣ' => 'I', 'Ἤ' => 'I', 'Ἥ' => 'I',
        'Ἦ' => 'I', 'Ἧ' => 'I', 'ᾘ' => 'I', 'ᾙ' => 'I', 'ᾚ' => 'I', 'ᾛ' => 'I',
        'ᾜ' => 'I', 'ᾝ' => 'I', 'ᾞ' => 'I', 'ᾟ' => 'I', 'Ὴ' => 'I', 'ῌ' => 'I',
        'Θ' => 'T', 'Ι' => 'I', 'Ί' => 'I', 'Ϊ' => 'I', 'Ἰ' => 'I', 'Ἱ' => 'I',
        'Ἲ' => 'I', 'Ἳ' => 'I', 'Ἴ' => 'I', 'Ἵ' => 'I', 'Ἶ' => 'I', 'Ἷ' => 'I',
        'Ῐ' => 'I', 'Ῑ' => 'I', 'Ὶ' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M',
        'Ν' => 'N', 'Ξ' => 'K', 'Ο' => 'O', 'Ό' => 'O', 'Ὀ' => 'O', 'Ὁ' => 'O',
        'Ὂ' => 'O', 'Ὃ' => 'O', 'Ὄ' => 'O', 'Ὅ' => 'O', 'Ὸ' => 'O', 'Π' => 'P',
        'Ρ' => 'R', 'Ῥ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Ύ' => 'Y',
        'Ϋ' => 'Y', 'Ὑ' => 'Y', 'Ὓ' => 'Y', 'Ὕ' => 'Y', 'Ὗ' => 'Y', 'Ῠ' => 'Y',
        'Ῡ' => 'Y', 'Ὺ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'P', 'Ω' => 'O',
        'Ώ' => 'O', 'Ὠ' => 'O', 'Ὡ' => 'O', 'Ὢ' => 'O', 'Ὣ' => 'O', 'Ὤ' => 'O',
        'Ὥ' => 'O', 'Ὦ' => 'O', 'Ὧ' => 'O', 'ᾨ' => 'O', 'ᾩ' => 'O', 'ᾪ' => 'O',
        'ᾫ' => 'O', 'ᾬ' => 'O', 'ᾭ' => 'O', 'ᾮ' => 'O', 'ᾯ' => 'O', 'Ὼ' => 'O',
        'ῼ' => 'O', 'α' => 'a', 'ά' => 'a', 'ἀ' => 'a', 'ἁ' => 'a', 'ἂ' => 'a',
        'ἃ' => 'a', 'ἄ' => 'a', 'ἅ' => 'a', 'ἆ' => 'a', 'ἇ' => 'a', 'ᾀ' => 'a',
        'ᾁ' => 'a', 'ᾂ' => 'a', 'ᾃ' => 'a', 'ᾄ' => 'a', 'ᾅ' => 'a', 'ᾆ' => 'a',
        'ᾇ' => 'a', 'ὰ' => 'a', 'ᾰ' => 'a', 'ᾱ' => 'a', 'ᾲ' => 'a', 'ᾳ' => 'a',
        'ᾴ' => 'a', 'ᾶ' => 'a', 'ᾷ' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd',
        'ε' => 'e', 'έ' => 'e', 'ἐ' => 'e', 'ἑ' => 'e', 'ἒ' => 'e', 'ἓ' => 'e',
        'ἔ' => 'e', 'ἕ' => 'e', 'ὲ' => 'e', 'ζ' => 'z', 'η' => 'i', 'ή' => 'i',
        'ἠ' => 'i', 'ἡ' => 'i', 'ἢ' => 'i', 'ἣ' => 'i', 'ἤ' => 'i', 'ἥ' => 'i',
        'ἦ' => 'i', 'ἧ' => 'i', 'ᾐ' => 'i', 'ᾑ' => 'i', 'ᾒ' => 'i', 'ᾓ' => 'i',
        'ᾔ' => 'i', 'ᾕ' => 'i', 'ᾖ' => 'i', 'ᾗ' => 'i', 'ὴ' => 'i', 'ῂ' => 'i',
        'ῃ' => 'i', 'ῄ' => 'i', 'ῆ' => 'i', 'ῇ' => 'i', 'θ' => 't', 'ι' => 'i',
        'ί' => 'i', 'ϊ' => 'i', 'ΐ' => 'i', 'ἰ' => 'i', 'ἱ' => 'i', 'ἲ' => 'i',
        'ἳ' => 'i', 'ἴ' => 'i', 'ἵ' => 'i', 'ἶ' => 'i', 'ἷ' => 'i', 'ὶ' => 'i',
        'ῐ' => 'i', 'ῑ' => 'i', 'ῒ' => 'i', 'ῖ' => 'i', 'ῗ' => 'i', 'κ' => 'k',
        'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => 'k', 'ο' => 'o', 'ό' => 'o',
        'ὀ' => 'o', 'ὁ' => 'o', 'ὂ' => 'o', 'ὃ' => 'o', 'ὄ' => 'o', 'ὅ' => 'o',
        'ὸ' => 'o', 'π' => 'p', 'ρ' => 'r', 'ῤ' => 'r', 'ῥ' => 'r', 'σ' => 's',
        'ς' => 's', 'τ' => 't', 'υ' => 'y', 'ύ' => 'y', 'ϋ' => 'y', 'ΰ' => 'y',
        'ὐ' => 'y', 'ὑ' => 'y', 'ὒ' => 'y', 'ὓ' => 'y', 'ὔ' => 'y', 'ὕ' => 'y',
        'ὖ' => 'y', 'ὗ' => 'y', 'ὺ' => 'y', 'ῠ' => 'y', 'ῡ' => 'y', 'ῢ' => 'y',
        'ῦ' => 'y', 'ῧ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'p', 'ω' => 'o',
        'ώ' => 'o', 'ὠ' => 'o', 'ὡ' => 'o', 'ὢ' => 'o', 'ὣ' => 'o', 'ὤ' => 'o',
        'ὥ' => 'o', 'ὦ' => 'o', 'ὧ' => 'o', 'ᾠ' => 'o', 'ᾡ' => 'o', 'ᾢ' => 'o',
        'ᾣ' => 'o', 'ᾤ' => 'o', 'ᾥ' => 'o', 'ᾦ' => 'o', 'ᾧ' => 'o', 'ὼ' => 'o',
        'ῲ' => 'o', 'ῳ' => 'o', 'ῴ' => 'o', 'ῶ' => 'o', 'ῷ' => 'o', 'А' => 'A',
        'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E',
        'Ж' => 'Z', 'З' => 'Z', 'И' => 'I', 'Й' => 'I', 'К' => 'K', 'Л' => 'L',
        'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S',
        'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'K', 'Ц' => 'T', 'Ч' => 'C',
        'Ш' => 'S', 'Щ' => 'S', 'Ы' => 'Y', 'Э' => 'E', 'Ю' => 'Y', 'Я' => 'Y',
        'а' => 'A', 'б' => 'B', 'в' => 'V', 'г' => 'G', 'д' => 'D', 'е' => 'E',
        'ё' => 'E', 'ж' => 'Z', 'з' => 'Z', 'и' => 'I', 'й' => 'I', 'к' => 'K',
        'л' => 'L', 'м' => 'M', 'н' => 'N', 'о' => 'O', 'п' => 'P', 'р' => 'R',
        'с' => 'S', 'т' => 'T', 'у' => 'U', 'ф' => 'F', 'х' => 'K', 'ц' => 'T',
        'ч' => 'C', 'ш' => 'S', 'щ' => 'S', 'ы' => 'Y', 'э' => 'E', 'ю' => 'Y',
        'я' => 'Y', 'ð' => 'd', 'Ð' => 'D', 'þ' => 't', 'Þ' => 'T', 'ა' => 'a',
        'ბ' => 'b', 'გ' => 'g', 'დ' => 'd', 'ე' => 'e', 'ვ' => 'v', 'ზ' => 'z',
        'თ' => 't', 'ი' => 'i', 'კ' => 'k', 'ლ' => 'l', 'მ' => 'm', 'ნ' => 'n',
        'ო' => 'o', 'პ' => 'p', 'ჟ' => 'z', 'რ' => 'r', 'ს' => 's', 'ტ' => 't',
        'უ' => 'u', 'ფ' => 'p', 'ქ' => 'k', 'ღ' => 'g', 'ყ' => 'q', 'შ' => 's',
        'ჩ' => 'c', 'ც' => 't', 'ძ' => 'd', 'წ' => 't', 'ჭ' => 'c', 'ხ' => 'k',
        'ჯ' => 'j', 'ჰ' => 'h'
    );
    $str = str_replace(array_keys($transliteration), array_values($transliteration), $str);
    $str = trim(preg_replace('/([^ a-z0-9]+)/i', '', $str));

    $str = str_replace(' ', '_', $str);

    return $str;
}

set_time_limit(0);

$date = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d') - 3, date('y')));

$sql = mysqli_query($link, "select startup.id,startup.nom,startup.short_fr,startup.logo,lf.montant,lf.date_ajout,lf.de,lf.id as idlf from startup inner join lf on lf.id_startup=startup.id where startup.status=1 and lf.rachat=0 and lf.notif=0 and date(lf.date_add)>'" . $date . "' order by lf.date_add desc limit 2 offset 0")or die(mysqli_error($link));
$sql2 = mysqli_query($link, "select startup.id,startup.nom,startup.short_fr,startup.logo,lf.montant,lf.date_ajout,lf.de,lf.id as idlf from startup inner join lf on lf.id_startup=startup.id where startup.status=1 and lf.rachat=0 and lf.notif=0 and date(lf.date_add)>'" . $date . "' order by lf.date_add desc limit 10 offset 2")or die(mysqli_error($link));
$nbrs = mysqli_num_rows($sql);
if ($nbrs > 0) {


    setlocale(LC_TIME, 'fr_FR');
    date_default_timezone_set('Europe/Paris');

    $nb_total_lf = mysqli_num_rows(mysqli_query($link, "select lf.id from lf inner join startup on startup.id=lf.id_startup where startup.status=1 and lf.rachat=0"));
    $somme_total_lf = mysqli_fetch_array(mysqli_query($link, "select sum(lf.montant) as somme from lf inner join startup on startup.id=lf.id_startup where startup.status=1 and lf.rachat=0"));

    $message = '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>myfrenchstartup</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
      <style type="text/css">
         table {
         border-collapse: separate;
         }
         a,
         a:link,
         a:visited {
         text-decoration: none;
         color: #000000;
         }
         a:hover {
         text-decoration: underline;
         font-weight: bold;
         }
         .ExternalClass p,
         .ExternalClass p,
         .ExternalClass font,
         .ExternalClass td {
         line-height: 100%
         }
         .ExternalClass {
         width: 100%;
         }
         .hmfix img {
         width: 10px !important;
         height: 10px !important;
         }
        
        
      </style>
   </head>
   <body bgcolor="#a5a2a2">
      <table width="600" style="width:600px;" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" >
         <tr>
            <td>
               <table width="600" style="width:600px;" class="resp_tab" cellspacing="0" cellpadding="0" border="0"  align="center">
                  <tr>
                     <td>
                       
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <table width="600" style="width:600px;" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
         <tr>
            <td width="23"></td>
            <td >
               <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:20px;color:#425466;line-height:20px;font-weight:bold;">
                  &nbsp;
               </p>
               <table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                  <tr>
                     <td class="resp_col" align="left">
                        <a href="https://www.myfrenchstartup.com" target="_blank" style="text-decoration: none;"><img src="https://www.myfrenchstartup.com/notifs/images2/lg.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></a>
                     </td>
                   
                  </tr>
               </table>
                <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:20px;color:#425466;line-height:20px;font-weight:bold;">
                  &nbsp;
               </p>
               
            </td>
            <td width="23"></td>
         </tr>
      </table>
        <table width="600" style="width:600px;" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#f6f9fc">
         <tr>
            <td width="23"></td>
            <td >
         
                <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                  &nbsp;
               </p>
                <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#425466;line-height:28px;font-weight: bold">
                          Vos mises à jour pour le ' . utf8_encode(strftime('%A %d %B %Y')) . '
                </p>
               <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                  &nbsp;
               </p>
            </td>
            <td width="23"></td>
         </tr>
      </table>
      <table width="600" style="width:600px;" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#f6f9fc">
         <tr>
            <td ><img src="https://www.myfrenchstartup.com/notifs/images2/1.jpeg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></td>
            <td valign="top" bgcolor="#ffffff" >
               <table width="534"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#00abc8">
                  <tr>
                     <td  width="25"> &nbsp; </td>
                     <td class="resp_col" >
                        <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:13px;color:#ffffff;line-height:34px;font-weight: bold">
                           Dernières levées de fonds
                        </p>
                     </td>
                  </tr>
               </table>
               <table width="534"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                  <tr>
                     <td width="20"></td>
                     <td bgcolor="ffffff" >
                      <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:15px;color:#425466;line-height:15px;font-weight:bold;">
                                    &nbsp;
                                 </p>';

    while ($lf_new = mysqli_fetch_array($sql)) {
        $message .= '<table width="514"   class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                           <tr>
                              <td class="resp_col" align="left" width="80">
                                 <a href="' . url . 'fr/startup-france/' . generate_id($lf_new['id']) . "/" . urlWriting(strtolower($lf_new["nom"])) . '" target="_blank"><img src="https://www.myfrenchstartup.com/' . str_replace("../", "", $lf_new['logo']) . '" width="50" height="50" alt="myfrenchstartup" /></a>
                              </td>
                              <td class="resp_col" align="left">
                                 <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:16px;color:#00ABC8;line-height:20px;font-weight: bold">
                                    <a href="' . url . 'fr/startup-france/' . generate_id($lf_new['id']) . "/" . urlWriting(strtolower($lf_new["nom"])) . '" target="_blank" style="text-decoration: :none;font-size:16px;color:#00ABC8;line-height:20px;font-weight: bold">' . $lf_new['nom'] . '</a>
                                 </p>
                                
                                 <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#425466;line-height:15px;font-weight: bold">
                                    ' . number_format($lf_new['montant'], 0, ",", " ") . ' k&euro; / <span style="color: #00ABC8"> Series ' . $lf_new['serie'] . '</span>
                                 </p>
                                
                              </td>
                           </tr>
                        </table>

                        <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                                    &nbsp;
                                 </p>
                                  <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#425466;line-height:15px;">
                                  ' . strtoupper(utf8_encode($lf_new['de'])) . '
                                 </p>
                       <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                                    &nbsp;
                                 </p>
                       
                               

                        
                       <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                                    &nbsp;
                                 </p>';
    }
    $message .= ' <table  width="492"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                           <tr>
                              <td class="resp_col" align="left" width="80">
                                 <img src="https://www.myfrenchstartup.com/notifs/images/c1.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" />
                              </td>
                           </tr>
                           <tr>
                              <td class="resp_col" align="left" width="80">
                                       
                                 <table  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                                    <tr>
                                       <td class="resp_col" align="left"   >
                                          <img src="https://www.myfrenchstartup.com/notifs/images/c2.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" />
                                       </td>
                                        <td class="resp_col" width="121">
                                             <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:24px;color:#425466;line-height:25px;font-weight: bold">
                                               ' . $nb_total_lf . '
                                             </p>
                                            
                                             <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:12px;color:#00ABC8;line-height:15px;font-weight: bold">
                                                Levées de fonds
                                             </p>
                                           
                                          </td>
                                       <td class="resp_col" align="left" >
                                          <img src="https://www.myfrenchstartup.com/notifs/images/c3.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" />
                                       </td>
                                       <td class="resp_col" width="120">
                                             <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:24px;color:#425466;line-height:25px;font-weight: bold">
                                               ' . number_format(($somme_total_lf['somme'] / 1000000), 1, ",", "") . ' Mds&euro;
                                             </p>
                                            
                                             <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:12px;color:#00ABC8;line-height:15px;font-weight: bold">
                                                Montant levé
                                             </p>
                                           
                                          </td>
                                       <td class="resp_col" align="left" >
                                          <img src="https://www.myfrenchstartup.com/notifs/images/c4.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" />
                                       </td>
                                       <td class="resp_col" width="122">
                                             <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:24px;color:#425466;line-height:25px;font-weight: bold">
                                              00
                                             </p>
                                            
                                             <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:12px;color:#00ABC8;line-height:15px;font-weight: bold">
                                                lorem ipsum <br> lorem ipsum
                                             </p>
                                           
                                          </td>
                                       <td class="resp_col" align="left" >
                                          <img src="https://www.myfrenchstartup.com/notifs/images2/c5.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" />
                                       </td>
                                      
                                      
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                            <tr>
                              <td class="resp_col" align="left" width="80">
                                 <img src="https://www.myfrenchstartup.com/notifs/images2/c6.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" />
                              </td>
                           </tr>
                        </table>
                                
                       
                         <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:15px;color:#425466;line-height:15px;font-weight:bold;">
                                    &nbsp;
                                 </p>';

    while ($lf_new2 = mysqli_fetch_array($sql2)) {
        $message .= '<table width="514"   class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                           <tr>
                              <td class="resp_col" align="left" width="80">
                                 <a href="' . url . 'fr/startup-france/' . generate_id($lf_new2['id']) . "/" . urlWriting(strtolower($lf_new2["nom"])) . '" target="_blank"><img src="https://www.myfrenchstartup.com/' . str_replace("../", "", $lf_new2['logo']) . '" width="50" height="50" alt="myfrenchstartup"  style="" /></a>
                              </td>
                              <td class="resp_col" align="left">
                                 <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:16px;color:#00ABC8;line-height:20px;font-weight: bold">
                                    <a href="' . url . 'fr/startup-france/' . generate_id($lf_new2['id']) . "/" . urlWriting(strtolower($lf_new2["nom"])) . '" target="_blank" style="text-decoration: :none;font-size:16px;color:#00ABC8;line-height:20px;font-weight: bold">' . $lf_new2['nom'] . '</a>
                                 </p>
                                
                                 <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#425466;line-height:15px;font-weight: bold">
                                    ' . number_format($lf_new2['montant'], 0, ",", " ") . ' k&euro; / <span style="color: #00ABC8"> Series ' . $lf_new2['serie'] . '</span>
                                 </p>
                                
                              </td>
                           </tr>
                        </table>

                        <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                                    &nbsp;
                                 </p>
                                  <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#425466;line-height:15px;">
                                   ' . strtoupper(utf8_encode($lf_new2['de'])) . '
                                 </p>
                       <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                                    &nbsp;
                                 </p>        
                                 
                       <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                                    &nbsp;
                                 </p> ';
    }


    $message .= '</td>
                     <td width="20"></td>
                  </tr>
               </table>
               
             
               
               
           
            </td>
            <td ><img src="https://www.myfrenchstartup.com/notifs/images2/2.jpeg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></td>
         </tr>
      </table>
         <table width="600" style="width:600px;" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
         <tr>
           
            <td >
               <table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                  <tr>
                     <td class="resp_col" align="left">
                        <a href="https://www.myfrenchstartup.com" target="_blank" style="text-decoration: none;"><img src="https://www.myfrenchstartup.com/notifs/images2/bt.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></a>
                     </td>
                   
                  </tr>
               </table>
              
               
            </td>
       
         </tr>
      </table>

       
        <table width="600" style="width:600px;" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#f6f9fc">
         <tr>
            <td width="20"></td>
            <td align="left">
               <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                  &nbsp;
               </p>
               <table  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="left" bgcolor="#f6f9fc">
                  <tr>
                     <td  align="left">
                        <a href="https://www.myfrenchstartup.com" target="_blank" style="text-decoration: none;"><img src="https://www.myfrenchstartup.com/notifs/images2/lg2.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></a>
                         <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                  &nbsp;
               </p>
                     </td>
                     
                  </tr>
               </table>
               
            </td>
             <td align="right">
                         <table   class="resp_tab" border="0" cellpadding="0" cellspacing="0"  bgcolor="#f6f9fc">
                          <tr>
                             <td  align="left">
                                <a href="https://twitter.com/myfrenchstartup" target="_blank" style="text-decoration: none;"><img src="https://www.myfrenchstartup.com/notifs/images2/tw.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></a>
                             </td>
                             <td width="10"></td>
                             <td align="left">
                                <a href="https://www.linkedin.com/company/myfrenchstartup/about/" target="_blank" style="text-decoration: none;"><img src="https://www.myfrenchstartup.com/notifs/images2/in.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></a>
                             </td>
                             <td width="10"></td>
                             <td  align="left">
                                <a href="https://www.facebook.com/MyFrenchStartup" target="_blank" style="text-decoration: none;"><img src="https://www.myfrenchstartup.com/notifs/images2/fb.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></a>
                             </td>
                          </tr>
                       </table>
                     </td>
            <td width="20"></td>
         </tr>
      </table>
   </body>
</html>';


    echo $message;

    $mail = new PHPMailer();

    $mail->IsSMTP();
    $mail->Host = 'ssl0.ovh.net';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'info@myfrenchstartup.com';                 // SMTP username
    $mail->Password = '04Betterway';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                               // TCP port to connect to

    $mail->setFrom('info@myfrenchstartup.com');
    $mail->addAddress('samijilani@live.com');     // Add a recipient
    $mail->addBCC('contact@myfrenchstartup.com');
    $mail->addBCC('samijilani@live.com');


    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = utf8_decode("MyFrenchStartup - Dernières levées de fonds");

    $mail->Body = utf8_decode($message);

    if (!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        
    }


    //mysqli_query($link, "update lf set notif=1 where lf.rachat=0 and lf.notif=0 and date(lf.date_add)>'" . $date . "' ");
}
?>