<?php

$base = dirname(dirname(__FILE__));
include($base . "/include/db.php");
require $base . '/phpmailer/PHPMailerAutoload.php';
ini_set("display_errors", 1);
error_reporting(1);

function generate_id($id) {
    $nb = ($id * 11) + 5124;
    return $nb;
}

function urlWriting($str, $utf8 = true) {
    $str = (string) $str;
    if (is_null($utf8)) {
        if (!function_exists('mb_detect_encoding')) {
            $utf8 = (strtolower(mb_detect_encoding($str)) == 'utf-8');
        } else {
            $length = strlen($str);
            $utf8 = true;
            for ($i = 0; $i < $length; $i++) {
                $c = ord($str[$i]);
                if ($c < 0x80)
                    $n = 0;# 0bbbbbbb
                elseif (($c & 0xE0) == 0xC0)
                    $n = 1;# 110bbbbb
                elseif (($c & 0xF0) == 0xE0)
                    $n = 2;# 1110bbbb
                elseif (($c & 0xF8) == 0xF0)
                    $n = 3;# 11110bbb
                elseif (($c & 0xFC) == 0xF8)
                    $n = 4;# 111110bb
                elseif (($c & 0xFE) == 0xFC)
                    $n = 5;# 1111110b
                else
                    return false;# Does not match any model
                for ($j = 0; $j < $n; $j++) { # n bytes matching 10bbbbbb follow ?
                    if (( ++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80)) {
                        $utf8 = false;
                        break;
                    }
                }
            }
        }
    }

    if (!$utf8)
        $str = utf8_encode($str);

    $transliteration = array(
        'Ĳ' => 'I', 'Ö' => 'O', 'Œ' => 'O', 'Ü' => 'U', 'ä' => 'a', 'æ' => 'a',
        'ĳ' => 'i', 'ö' => 'o', 'œ' => 'o', 'ü' => 'u', 'ß' => 's', 'ſ' => 's',
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A',
        'Æ' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Ç' => 'C', 'Ć' => 'C',
        'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D', 'È' => 'E',
        'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E', 'Ę' => 'E', 'Ě' => 'E',
        'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G', 'Ġ' => 'G', 'Ģ' => 'G',
        'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
        'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I', 'İ' => 'I', 'Ĵ' => 'J',
        'Ķ' => 'K', 'Ľ' => 'K', 'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ł' => 'L',
        'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N', 'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O',
        'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O',
        'Ŏ' => 'O', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Ş' => 'S',
        'Ŝ' => 'S', 'Ș' => 'S', 'Š' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
        'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ū' => 'U', 'Ů' => 'U',
        'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U', 'Ŵ' => 'W', 'Ŷ' => 'Y',
        'Ÿ' => 'Y', 'Ý' => 'Y', 'Ź' => 'Z', 'Ż' => 'Z', 'Ž' => 'Z', 'à' => 'a',
        'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
        'å' => 'a', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
        'ď' => 'd', 'đ' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
        'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e', 'ƒ' => 'f',
        'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h', 'ħ' => 'h',
        'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i', 'ĩ' => 'i',
        'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĵ' => 'j', 'ķ' => 'k', 'ĸ' => 'k',
        'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l', 'ŀ' => 'l', 'ñ' => 'n',
        'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n', 'ŋ' => 'n', 'ò' => 'o',
        'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o',
        'ŏ' => 'o', 'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'ś' => 's', 'š' => 's',
        'ť' => 't', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ū' => 'u', 'ů' => 'u',
        'ű' => 'u', 'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ÿ' => 'y',
        'ý' => 'y', 'ŷ' => 'y', 'ż' => 'z', 'ź' => 'z', 'ž' => 'z', 'Α' => 'A',
        'Ά' => 'A', 'Ἀ' => 'A', 'Ἁ' => 'A', 'Ἂ' => 'A', 'Ἃ' => 'A', 'Ἄ' => 'A',
        'Ἅ' => 'A', 'Ἆ' => 'A', 'Ἇ' => 'A', 'ᾈ' => 'A', 'ᾉ' => 'A', 'ᾊ' => 'A',
        'ᾋ' => 'A', 'ᾌ' => 'A', 'ᾍ' => 'A', 'ᾎ' => 'A', 'ᾏ' => 'A', 'Ᾰ' => 'A',
        'Ᾱ' => 'A', 'Ὰ' => 'A', 'ᾼ' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D',
        'Ε' => 'E', 'Έ' => 'E', 'Ἐ' => 'E', 'Ἑ' => 'E', 'Ἒ' => 'E', 'Ἓ' => 'E',
        'Ἔ' => 'E', 'Ἕ' => 'E', 'Ὲ' => 'E', 'Ζ' => 'Z', 'Η' => 'I', 'Ή' => 'I',
        'Ἠ' => 'I', 'Ἡ' => 'I', 'Ἢ' => 'I', 'Ἣ' => 'I', 'Ἤ' => 'I', 'Ἥ' => 'I',
        'Ἦ' => 'I', 'Ἧ' => 'I', 'ᾘ' => 'I', 'ᾙ' => 'I', 'ᾚ' => 'I', 'ᾛ' => 'I',
        'ᾜ' => 'I', 'ᾝ' => 'I', 'ᾞ' => 'I', 'ᾟ' => 'I', 'Ὴ' => 'I', 'ῌ' => 'I',
        'Θ' => 'T', 'Ι' => 'I', 'Ί' => 'I', 'Ϊ' => 'I', 'Ἰ' => 'I', 'Ἱ' => 'I',
        'Ἲ' => 'I', 'Ἳ' => 'I', 'Ἴ' => 'I', 'Ἵ' => 'I', 'Ἶ' => 'I', 'Ἷ' => 'I',
        'Ῐ' => 'I', 'Ῑ' => 'I', 'Ὶ' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M',
        'Ν' => 'N', 'Ξ' => 'K', 'Ο' => 'O', 'Ό' => 'O', 'Ὀ' => 'O', 'Ὁ' => 'O',
        'Ὂ' => 'O', 'Ὃ' => 'O', 'Ὄ' => 'O', 'Ὅ' => 'O', 'Ὸ' => 'O', 'Π' => 'P',
        'Ρ' => 'R', 'Ῥ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Ύ' => 'Y',
        'Ϋ' => 'Y', 'Ὑ' => 'Y', 'Ὓ' => 'Y', 'Ὕ' => 'Y', 'Ὗ' => 'Y', 'Ῠ' => 'Y',
        'Ῡ' => 'Y', 'Ὺ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'P', 'Ω' => 'O',
        'Ώ' => 'O', 'Ὠ' => 'O', 'Ὡ' => 'O', 'Ὢ' => 'O', 'Ὣ' => 'O', 'Ὤ' => 'O',
        'Ὥ' => 'O', 'Ὦ' => 'O', 'Ὧ' => 'O', 'ᾨ' => 'O', 'ᾩ' => 'O', 'ᾪ' => 'O',
        'ᾫ' => 'O', 'ᾬ' => 'O', 'ᾭ' => 'O', 'ᾮ' => 'O', 'ᾯ' => 'O', 'Ὼ' => 'O',
        'ῼ' => 'O', 'α' => 'a', 'ά' => 'a', 'ἀ' => 'a', 'ἁ' => 'a', 'ἂ' => 'a',
        'ἃ' => 'a', 'ἄ' => 'a', 'ἅ' => 'a', 'ἆ' => 'a', 'ἇ' => 'a', 'ᾀ' => 'a',
        'ᾁ' => 'a', 'ᾂ' => 'a', 'ᾃ' => 'a', 'ᾄ' => 'a', 'ᾅ' => 'a', 'ᾆ' => 'a',
        'ᾇ' => 'a', 'ὰ' => 'a', 'ᾰ' => 'a', 'ᾱ' => 'a', 'ᾲ' => 'a', 'ᾳ' => 'a',
        'ᾴ' => 'a', 'ᾶ' => 'a', 'ᾷ' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd',
        'ε' => 'e', 'έ' => 'e', 'ἐ' => 'e', 'ἑ' => 'e', 'ἒ' => 'e', 'ἓ' => 'e',
        'ἔ' => 'e', 'ἕ' => 'e', 'ὲ' => 'e', 'ζ' => 'z', 'η' => 'i', 'ή' => 'i',
        'ἠ' => 'i', 'ἡ' => 'i', 'ἢ' => 'i', 'ἣ' => 'i', 'ἤ' => 'i', 'ἥ' => 'i',
        'ἦ' => 'i', 'ἧ' => 'i', 'ᾐ' => 'i', 'ᾑ' => 'i', 'ᾒ' => 'i', 'ᾓ' => 'i',
        'ᾔ' => 'i', 'ᾕ' => 'i', 'ᾖ' => 'i', 'ᾗ' => 'i', 'ὴ' => 'i', 'ῂ' => 'i',
        'ῃ' => 'i', 'ῄ' => 'i', 'ῆ' => 'i', 'ῇ' => 'i', 'θ' => 't', 'ι' => 'i',
        'ί' => 'i', 'ϊ' => 'i', 'ΐ' => 'i', 'ἰ' => 'i', 'ἱ' => 'i', 'ἲ' => 'i',
        'ἳ' => 'i', 'ἴ' => 'i', 'ἵ' => 'i', 'ἶ' => 'i', 'ἷ' => 'i', 'ὶ' => 'i',
        'ῐ' => 'i', 'ῑ' => 'i', 'ῒ' => 'i', 'ῖ' => 'i', 'ῗ' => 'i', 'κ' => 'k',
        'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => 'k', 'ο' => 'o', 'ό' => 'o',
        'ὀ' => 'o', 'ὁ' => 'o', 'ὂ' => 'o', 'ὃ' => 'o', 'ὄ' => 'o', 'ὅ' => 'o',
        'ὸ' => 'o', 'π' => 'p', 'ρ' => 'r', 'ῤ' => 'r', 'ῥ' => 'r', 'σ' => 's',
        'ς' => 's', 'τ' => 't', 'υ' => 'y', 'ύ' => 'y', 'ϋ' => 'y', 'ΰ' => 'y',
        'ὐ' => 'y', 'ὑ' => 'y', 'ὒ' => 'y', 'ὓ' => 'y', 'ὔ' => 'y', 'ὕ' => 'y',
        'ὖ' => 'y', 'ὗ' => 'y', 'ὺ' => 'y', 'ῠ' => 'y', 'ῡ' => 'y', 'ῢ' => 'y',
        'ῦ' => 'y', 'ῧ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'p', 'ω' => 'o',
        'ώ' => 'o', 'ὠ' => 'o', 'ὡ' => 'o', 'ὢ' => 'o', 'ὣ' => 'o', 'ὤ' => 'o',
        'ὥ' => 'o', 'ὦ' => 'o', 'ὧ' => 'o', 'ᾠ' => 'o', 'ᾡ' => 'o', 'ᾢ' => 'o',
        'ᾣ' => 'o', 'ᾤ' => 'o', 'ᾥ' => 'o', 'ᾦ' => 'o', 'ᾧ' => 'o', 'ὼ' => 'o',
        'ῲ' => 'o', 'ῳ' => 'o', 'ῴ' => 'o', 'ῶ' => 'o', 'ῷ' => 'o', 'А' => 'A',
        'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E',
        'Ж' => 'Z', 'З' => 'Z', 'И' => 'I', 'Й' => 'I', 'К' => 'K', 'Л' => 'L',
        'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S',
        'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'K', 'Ц' => 'T', 'Ч' => 'C',
        'Ш' => 'S', 'Щ' => 'S', 'Ы' => 'Y', 'Э' => 'E', 'Ю' => 'Y', 'Я' => 'Y',
        'а' => 'A', 'б' => 'B', 'в' => 'V', 'г' => 'G', 'д' => 'D', 'е' => 'E',
        'ё' => 'E', 'ж' => 'Z', 'з' => 'Z', 'и' => 'I', 'й' => 'I', 'к' => 'K',
        'л' => 'L', 'м' => 'M', 'н' => 'N', 'о' => 'O', 'п' => 'P', 'р' => 'R',
        'с' => 'S', 'т' => 'T', 'у' => 'U', 'ф' => 'F', 'х' => 'K', 'ц' => 'T',
        'ч' => 'C', 'ш' => 'S', 'щ' => 'S', 'ы' => 'Y', 'э' => 'E', 'ю' => 'Y',
        'я' => 'Y', 'ð' => 'd', 'Ð' => 'D', 'þ' => 't', 'Þ' => 'T', 'ა' => 'a',
        'ბ' => 'b', 'გ' => 'g', 'დ' => 'd', 'ე' => 'e', 'ვ' => 'v', 'ზ' => 'z',
        'თ' => 't', 'ი' => 'i', 'კ' => 'k', 'ლ' => 'l', 'მ' => 'm', 'ნ' => 'n',
        'ო' => 'o', 'პ' => 'p', 'ჟ' => 'z', 'რ' => 'r', 'ს' => 's', 'ტ' => 't',
        'უ' => 'u', 'ფ' => 'p', 'ქ' => 'k', 'ღ' => 'g', 'ყ' => 'q', 'შ' => 's',
        'ჩ' => 'c', 'ც' => 't', 'ძ' => 'd', 'წ' => 't', 'ჭ' => 'c', 'ხ' => 'k',
        'ჯ' => 'j', 'ჰ' => 'h'
    );
    $str = str_replace(array_keys($transliteration), array_values($transliteration), $str);
    $str = trim(preg_replace('/([^ a-z0-9]+)/i', '', $str));

    $str = str_replace(' ', '_', $str);

    return $str;
}

set_time_limit(0);



$sql = mysqli_query($link, "select * from user_save_list where notif_update=1 group by user")or die(mysqli_error($link));
while ($data_user = mysqli_fetch_array($sql)) {

    mysqli_query($link, "INSERT INTO `notifications` ( `user`, `nom`, `dt`, `etat`, `liste`,list_sup,list_lf,list_verif) VALUES ( " . $data_user['user'] . ", '" . $data_user['nom'] . "', '" . date('Y-m-d H:i:s') . "', '0', " . $data['id'] . ",'".$data_user['list_new']."','".$data_user['list_new_lf']."','".$data_user['list_new_verif']."');");


    $user = mysqli_fetch_array(mysqli_query($link, "select * from user where id=" . $data_user['user']));


    $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>myfrenchstartup</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
      <style type="text/css">
         table {
         border-collapse: separate;
         }
         a,
         a:link,
         a:visited {
         text-decoration: none;
         color: #000000;
         }
         a:hover {
         text-decoration: underline;
         font-weight: bold;
         }
         .ExternalClass p,
         .ExternalClass p,
         .ExternalClass font,
         .ExternalClass td {
         line-height: 100%
         }
         .ExternalClass {
         width: 100%;
         }
         .hmfix img {
         width: 10px !important;
         height: 10px !important;
         }
      </style>
   </head>
   <body bgcolor="#EDF0F2">
    
      <table width="600" style="width:600px;" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#edf0f2">
         <tr>
            <td width="20"></td>
            <td >
               <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                  &nbsp;
               </p>
               <table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#edf0f2">
                  <tr>
                     <td class="resp_col" align="left">
                        <a href="https://www.myfrenchstartup.com" target="_blank" style="text-decoration: none;"><img src="https://www.myfrenchstartup.com/notifs/images3/lg1.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></a>
                     </td>
                     <td class="resp_col" align="right" valign="bottom">
                        <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:right;font-size:11px;color:#425466;line-height:15px;">
                           Daily news
                        </p>
                     </td>
                  </tr>
               </table>
               <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                  &nbsp;
               </p>
            </td>
            <td width="20"></td>
         </tr>
      </table>';









    $sql3 = mysqli_query($link, "select * from user_save_list where notif_update=1 and user=" . $user['id'])or die(mysqli_error($link));
    while ($data = mysqli_fetch_array($sql3)) {

        if (($data['nb_new'] != 0) || ($data['nb_new_lf'] != 0) || ($data['nb_new_verif'] != 0)) {


            $message .= '<table width="600" style="width:600px;" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#edf0f2">
         <tr>
            <td width="20"></td>
            <td >
               <table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                  <tr>
                     <td class="resp_col" >
                        <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:15px;color:#425466;line-height:15px;font-weight:bold;">
                           &nbsp;
                        </p>
                        <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:18px;color:#425466;line-height:28px;font-weight: bold;text-transform: uppercase;">
                           LISTE : ' . strtoupper($data['nom']) . '
                        </p>
                        <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:15px;color:#425466;line-height:15px;font-weight:bold;">
                           &nbsp;
                        </p>
                     </td>
                  </tr>
               </table>
               <!-- bloc1 -->';

            if (($data['nb_new'] != "") && ($data['nb_new'] != 0)) {
                $message .= '<table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#6ACCDC">
                  <tr>
                     <td  width="45"> &nbsp; </td>
                     <td class="resp_col" >
                        <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#ffffff;line-height:21px;">
                           Nouvelles Startups
                        </p>
                     </td>
                  </tr>
               </table>';




                $list_sup_new_tab = explode(",", $data['list_new']);
                $nb_sup_new = count($list_sup_new_tab);
                for ($i1 = 0; $i1 < $nb_sup_new; $i1++) {
                    if ($list_sup_new_tab[$i1] != "") {
                        $sup_new = mysqli_fetch_array(mysqli_query($link, "select startup.short_fr,startup.nom,startup.logo,startup.id,activite.secteur,startup.creation from startup inner join activite on activite.id_startup=startup.id where startup.id=" . $list_sup_new_tab[$i1]));
                        $secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur where id=" . $sup_new['secteur']));



                        $message .= '  <table   class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                  <tr>
                     <td width="45"></td>
                     <td width="470" bgcolor="ffffff" >
                        <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:20px;color:#425466;line-height:20px;font-weight:bold;">
                           &nbsp;
                        </p>
                        <table width="470" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                           <tbody>
                              <tr>
                                 <td class="resp_col" align="left" width="108">
                                    <a href="' . url . 'fr/startup-france/' . generate_id($sup_new['id']) . "/" . urlWriting(strtolower($sup_new["nom"])) . '" target="_blank"><img src="https://www.myfrenchstartup.com/' . str_replace("../", "", $sup_new['logo']) . '" width="50" height="50" alt="myfrenchstartup" style=""></a>
                                 </td>
                                 <td class="resp_col" align="left">
                                    <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:16px;color:#425466;line-height:20px;font-weight: bold">
                                       <a href="' . url . 'fr/startup-france/' . generate_id($sup_new['id']) . "/" . urlWriting(strtolower($sup_new["nom"])) . '" target="_blank" style="text-decoration: :none;font-size:16px;color:#425466;line-height:20px;font-weight: bold;text-transform: uppercase;">' . stripslashes($sup_new['nom']) . '</a>
                                    </p>
                                    <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:5px;color:#425466;line-height:5px;font-weight:bold;">
                                       &nbsp;
                                    </p>
                                    <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#425466;line-height:13px;">
                                       ' . utf8_encode($secteur['nom_secteur']) . '
                                    </p>
                                    <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:5px;color:#425466;line-height:5px;font-weight:bold;">
                                       &nbsp;
                                    </p>
                                    <table class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="left" bgcolor="#ffffff">
                                       <tbody>
                                          <tr>
                                             <td class="resp_col" align="left" width="30">
                                                <a href="" target="_blank"><img src="https://www.myfrenchstartup.com/notifs/images3/cal.jpg" width="auto" height="auto" alt="myfrenchstartup" style=""></a>
                                             </td>
                                             <td class="resp_col" align="left">
                                                <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#425466;line-height:13px;">
                                                   ' . utf8_encode($sup_new['creation']) . '
                                                </p>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:20px;color:#425466;line-height:20px;font-weight:bold;">
                           &nbsp;
                        </p>
                     </td>
                     <td width="45"></td>
                  </tr>
               </table>
               <!-- sp -->
               <table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                  <tr>
                     <td width="45"></td>
                     <td class="resp_col" width="470" bgcolor="#e6e8eb" height="1">
                        <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:1px;color:#e6e8eb;line-height:1px;">
                           &nbsp;
                        </p>
                     </td>
                     <td width="45"></td>
                  </tr>
               </table>';
                    }
                }

                $message .= ' <!-- Voir plus -->
               <table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="right" bgcolor="#ffffff">
                  <tr>
                     <td width="45"></td>
                     <td class="resp_col" width="470" bgcolor="#ffffff" >
                        <p style="font-size:12px;color:#6ACCDC;line-height:12px;text-align: right;">
                       <a href="' . url . 'dashboard/' . generate_id($data['id']) . "/" . urlWriting(strtolower($data["nom"])) . '" target="_blank" style="text-decoration:underline;font-size:12px;color:#6ACCDC;line-height:13px;text-align: right;">Voir plus</a>
                    </p>
                     </td>
                     <td width="45"></td>
                  </tr>
               </table>';
            }




            if (($data['nb_new_lf'] != "") && ($data['nb_new_lf'] != 0)) {
                $message .= '    <!-- bloc2  -->
               <table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#6ACCDC">
                  <tr>
                     <td  width="45"> &nbsp; </td>
                     <td class="resp_col" >
                        <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#ffffff;line-height:21px;">
                           Dernières levées de fonds
                        </p>
                     </td>
                  </tr>
               </table>
               <!-- lg1 -->';
                $list_lf_new_tab = explode(",", $data['list_new_lf']);
                $nb_lf_new = count($list_lf_new_tab);
                for ($i2 = 0; $i2 < $nb_lf_new; $i2++) {

                    if ($list_lf_new_tab[$i2] != "") {
                        $lf_new = mysqli_fetch_array(mysqli_query($link, "select startup.nom,startup.logo,startup.id,lf.montant,lf.de,lf.serie,lf.id as idlf from startup inner join lf on lf.id_startup=startup.id where startup.id=" . trim($list_lf_new_tab[$i2]) . " order by lf.date_ajout desc limit 1"));
                        $sup_news = mysqli_fetch_array(mysqli_query($link, "select startup.short_fr,startup.nom,startup.logo,startup.id,activite.secteur,startup.creation from startup inner join activite on activite.id_startup=startup.id where startup.id=" . trim($list_lf_new_tab[$i2])));
                        $secteurs = mysqli_fetch_array(mysqli_query($link, "select * from secteur where id=" . $sup_news['secteur']));


                        $message .= ' <table   class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                  <tr>
                     <td width="45"></td>
                     <td width="470" bgcolor="ffffff" >
                        <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:20px;color:#425466;line-height:20px;font-weight:bold;">
                           &nbsp;
                        </p>
                        <table width="470" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                           <tbody>
                              <tr>
                                 <td class="resp_col" align="left" width="108">
                                    <a href="' . url . 'levee-de-fonds/' . generate_id($lf_new['idlf']) . "/" . urlWriting(strtolower($lf_new["nom"])) . '" target="_blank"><img src="https://www.myfrenchstartup.com/' . str_replace("../", "", $lf_new['logo']) . '" width="50" height="50" alt="myfrenchstartup" style=""></a>
                                 </td>
                                 <td class="resp_col" align="left">
                                    <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:16px;color:#425466;line-height:20px;font-weight: bold">
                                       <a href="' . url . 'levee-de-fonds/' . generate_id($lf_new['idlf']) . "/" . urlWriting(strtolower($lf_new["nom"])) . '" target="_blank" style="text-decoration: :none;font-size:16px;color:#425466;line-height:20px;font-weight: bold;text-transform: uppercase;">' . stripslashes($lf_new['nom']) . '</a>
                                    </p>
                                    <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:5px;color:#425466;line-height:5px;font-weight:bold;">
                                       &nbsp;
                                    </p>
                                    <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#425466;line-height:13px;">
                                       ' . stripslashes($secteurs['nom_secteur']) . '
                                    </p>
                                    <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:5px;color:#425466;line-height:5px;font-weight:bold;">
                                       &nbsp;
                                    </p>
                                 </td>
                                 <td class="resp_col" align="right">
                                                <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:right;font-size:24px;color:#425466;line-height:30px;text-transform: uppercase;font-weight: bold;">
                                                   ' . str_replace(",0", "", number_format($lf_new['montant'] / 1000, 1, ",", " ")) . ' M&euro;
                                                </p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:20px;color:#425466;line-height:20px;font-weight:bold;">
                           &nbsp;
                        </p>
                        <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#425466;line-height:13px;text-transform: uppercase;">
                                       ' . strtoupper($lf_new['de']) . '
                        </p>
                        <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:20px;color:#425466;line-height:20px;font-weight:bold;">
                           &nbsp;
                        </p>
                     </td>
                     <td width="45"></td>
                  </tr>
               </table>
               <!-- sp -->
               <table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                  <tr>
                     <td width="45"></td>
                     <td class="resp_col" width="470" bgcolor="#e6e8eb" height="1">
                        <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:1px;color:#e6e8eb;line-height:1px;">
                           &nbsp;
                        </p>
                     </td>
                     <td width="45"></td>
                  </tr>
               </table>';
                    }
                }

                $message .= ' <!-- Voir plus -->
               <table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="right" bgcolor="#ffffff">
                  <tr>
                     <td width="45"></td>
                     <td class="resp_col" width="470" bgcolor="#ffffff" >
                        <p style="font-size:12px;color:#6ACCDC;line-height:12px;text-align: right;">
                       <a href="' . url . 'dashboard/' . generate_id($data['id']) . "/" . urlWriting(strtolower($data["nom"])) . '" target="_blank" style="text-decoration:underline;font-size:12px;color:#6ACCDC;line-height:13px;text-align: right;">Voir plus</a>
                    </p>
                     </td>
                     <td width="45"></td>
                  </tr>
               </table>';
            }





            if (($data['nb_new_verif'] != "") && ($data['nb_new_verif'] != 0)) {

                $message .= '  <!-- bloc3  -->
               <table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#6ACCDC">
                  <tr>
                     <td  width="45"> &nbsp; </td>
                     <td class="resp_col" >
                        <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#ffffff;line-height:21px;">
                           Dernières publications Bodacc
                        </p>
                     </td>
                  </tr>
               </table>';

                $list_verif_new_tab = explode(",", $data['list_new_verif']);
                $nb_verif_new = count($list_verif_new_tab);
                for ($i3 = 0; $i3 < 3; $i3++) {

                    if ($list_verif_new_tab[$i3] != "") {
                        $verif_new = mysqli_fetch_array(mysqli_query($link, "select startup_depot.date_depot, startup_depot.lib_depot, startup.nom,startup.logo,startup.id,startup_depot.depot,startup_depot.dt from startup inner join startup_depot on startup_depot.id_startup=startup.id where startup.id=" . trim($list_verif_new_tab[$i3]) . " order by startup_depot.dt desc limit 1"));




                        $message .= '    <!-- lg1 -->
               <table   class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                  <tr>
                     <td width="45"></td>
                     <td width="470" bgcolor="ffffff" >
                        <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:20px;color:#425466;line-height:20px;font-weight:bold;">
                           &nbsp;
                        </p>
                        <table width="470" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
                           <tbody>
                              <tr>
                                 <td class="resp_col" align="left" width="108">
                                    <a href="' . url . 'fr/startup-france/' . generate_id($verif_new['id']) . "/" . urlWriting(strtolower($verif_new["nom"])) . '" target="_blank"><img src="https://www.myfrenchstartup.com/' . str_replace("../", "", $verif_new['logo']) . '" width="50" height="50" alt="myfrenchstartup" style=""></a>
                                 </td>
                                 <td class="resp_col" align="left">
                                    <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:16px;color:#425466;line-height:20px;font-weight: bold">
                                       <a href="' . url . 'fr/startup-france/' . generate_id($verif_new['id']) . "/" . urlWriting(strtolower($verif_new["nom"])) . '" target="_blank" style="text-decoration: :none;font-size:16px;color:#425466;line-height:20px;font-weight: bold;text-transform: uppercase;">' . stripslashes($verif_new['nom']) . '</a>
                                    </p>
                                    <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:5px;color:#425466;line-height:5px;font-weight:bold;">
                                       &nbsp;
                                    </p>
                                    <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#425466;line-height:16px;">
                                        ' . utf8_encode($verif_new['lib_depot']) . '
                                    </p>
                                    <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:5px;color:#425466;line-height:5px;font-weight:bold;">
                                       &nbsp;
                                    </p>
                                    
                                    <p class="txt_mb" style="font-family:arial;padding:0;Margin:0;text-align:left;font-size:12px;color:#425466;line-height:16px;">
                                        ' . $verif_new['date_depot'] . ' : ' . utf8_encode($verif_new['depot']) . '
                                    </p>
                                    <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:5px;color:#425466;line-height:5px;font-weight:bold;">
                                       &nbsp;
                                    </p>
                              
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:20px;color:#425466;line-height:20px;font-weight:bold;">
                           &nbsp;
                        </p>
                     </td>
                     <td width="45"></td>
                  </tr>
               </table>';
                    }
                }


                $message .= '  <!-- Voir plus -->
               <table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="right" bgcolor="#ffffff">
                  <tr>
                     <td width="45"></td>
                     <td class="resp_col" width="470" bgcolor="#ffffff" >
                        <p style="font-size:12px;color:#6ACCDC;line-height:12px;text-align: right;">
                       <a href="' . url . 'dashboard/' . generate_id($data['id']) . "/" . urlWriting(strtolower($data["nom"])) . '" target="_blank" style="text-decoration:underline;font-size:12px;color:#6ACCDC;line-height:13px;text-align: right;">Voir plus</a>
                    </p>
                     </td>
                     <td width="45"></td>
                  </tr>
               </table>
               

            </td>
            <td width="20"></td>
         </tr>
      </table>';
            }





            $message .= ' <!-- sp gris -->
      <table width="600" style="width:600px;" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#edf0f2">
         <tr>
            <td height="40">
               <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:40px;color:#edf0f2;line-height:40px;font-weight:bold;">
                  &nbsp;
               </p>
              
            </td>
         </tr>
      </table>';
        }
    }













    $message .= '  <!-- pub -->
      <table width="600" style="width:600px;" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#edf0f2">
         <tr>
            <td width="30"></td>
            <td >
               <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                  &nbsp;
               </p>
               <table   class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#edf0f2">
                  <tr>
                     <td class="resp_col" align="left">
                        <a href="https://www.myfrenchstartup.com" target="_blank" style="text-decoration: none;"><img src="https://www.myfrenchstartup.com/notifs/images3/pub.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></a>
                     </td>
                     
                  </tr>
               </table>
               <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:10px;color:#425466;line-height:10px;font-weight:bold;">
                  &nbsp;
               </p>
            </td>
            <td width="30"></td>
         </tr>
      </table>

      <table width="600" style="width:600px;" class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#edf0f2">
         <tr>
            <td width="20"></td>
            <td >
               <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:20px;color:#425466;line-height:20px;font-weight:bold;">
                  &nbsp;
               </p>
               <table width="560"  class="resp_tab" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#edf0f2">
                  <tr>
                     <td  align="left">
                        <a href="https://www.myfrenchstartup.com" target="_blank" style="text-decoration: none;"><img src="https://www.myfrenchstartup.com/notifs/images3/lg2.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></a>
                     </td>
                     <td class="resp_col" width="200"></td>
                     <td align="right">
                        <table   class="resp_tab" border="0" cellpadding="0" cellspacing="0"  bgcolor="#edf0f2">
                           <tr>
                              <td  align="left">
                                 <a href="https://twitter.com/myfrenchstartup" target="_blank" style="text-decoration: none;"><img src="https://www.myfrenchstartup.com/notifs/images3/tw.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></a>
                              </td>
                              <td width="10"></td>
                              <td align="left">
                                 <a href="https://www.linkedin.com/company/myfrenchstartup/about/" target="_blank" style="text-decoration: none;"><img src="https://www.myfrenchstartup.com/notifs/images3/in.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></a>
                              </td>
                              <td width="10"></td>
                              <td  align="left">
                                 <a href="https://www.facebook.com/MyFrenchStartup" target="_blank" style="text-decoration: none;"><img src="https://www.myfrenchstartup.com/notifs/images3/fb.jpg" width="auto" height="auto" alt="myfrenchstartup"  style="" /></a>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
               <p style="font-family:arial;padding:0;Margin:0;text-align:center;font-size:20px;color:#425466;line-height:20px;font-weight:bold;">
                  &nbsp;
               </p>
            </td>
            <td width="20"></td>
         </tr>
      </table>
      <center>
         <p style="font-family: Arial, Helvetica, sans-serif; text-align: center; font-size: 10px;height:10px;color: #000;Margin:0;padding:0">&nbsp;      Si vous souhaitez vous désabonner,   <a style="color:#000;" target="_blank" href=""> cliquez ici</a>
         </p>
      </center>
   </body>
</html>';

    $mail = new PHPMailer();

    $mail->IsSMTP();
    $mail->Host = 'ssl0.ovh.net';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'info@myfrenchstartup.com';                 // SMTP username
    $mail->Password = '04Betterway';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                               // TCP port to connect to

    $mail->setFrom('info@myfrenchstartup.com');
    //$mail->addAddress($user['email']);     // Add a recipient
    $mail->addAddress('samijilani@live.com');     // Add a recipient
    $mail->addBCC('contact@myfrenchstartup.com');
    $mail->addBCC('samijilani@live.com');


    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = utf8_decode("MyFrenchStartup - Mise à jour de la liste " . addslashes($data['nom'])) . " ";

    $mail->Body = utf8_decode($message);

    if (!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        mysqli_query($link, "insert into save_list_traking(user,dt,nb_new,nb_new_lf,nb_new_verif)values(" . $data['user'] . ",'" . date('Y-m-d') . "','" . $data['nb_new'] . "','" . $data['nb_new_lf'] . "','" . $data['nb_new_verif'] . "')");
    }

    mysqli_query($link, "update user_save_list set notif_update=0,nb_new=0,list_new='',nb_new_lf=0,list_new_lf='',nb_new_verif=0,list_new_verif='' where id=" . $data['id']);
}
?>