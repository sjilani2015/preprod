<?php
require   '../phpmailer/PHPMailerAutoload.php';
$message = '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

	<title>PETIT BATEAU</title>

	<style type="text/css">
	html {width:100%;}
	body, table, td, div, p, a, li, blockquote {-ms-text-size-adjust:none;-webkit-text-size-adjust:none;text-size-adjust:none;font-family:arial;}
	body {margin:0;padding:0;word-wrap:normal;}
	table {mso-table-lspace:0pt;mso-table-rspace:0pt;}
	table td {border-collapse:collapse;font-size:0;}
	a {line-height:normal;}
	a img {border:0;}
	img {-ms-interpolation-mode:bicubic;}
	.ReadMsgBody {width:100%;}
	.ExternalClass {width:100%;}
	.ExternalClass * {line-height:100%}
	#outlook a {padding:0;}
	.gmailfix {display:none !important;}
	.preheader { display:none !important; visibility:hidden; opacity:0; color:transparent; height:0; width:0; }

	a[href^=tel] {color:inherit;text-decoration:none;}
	a[x-apple-data-detectors] {color: inherit !important;text-decoration: none !important;font-size: inherit !important;font-family: inherit !important;font-weight: inherit !important;line-height: inherit !important;}

	@media only screen and (max-width: 480px) {
		.actu-mob { padding: 11px 10px !important; }
		.nav-ft-mob { font-size: 12px !important; }
		.divider-footer-mob { height: 2px !important; width: 100% !important; line-height: 2px !important;font-size: 2px !important; }
		.divider-footer-pd-mob { padding: 20px !important; }
		.nav-item-with-mob { width: 25% !important; }
		.text-center { text-align: center !important; margin: 0 auto !important; }
		.pd-b-div-footer { padding-bottom: 20px !important; }
		.footer-i-size { width: 150px !important; height: auto !important; }
		.mobile-only {overflow:visible !important;float:none !important;display:block !important;line-height:normal !important;max-height:none !important;width:100% !important;margin:0 auto !important;}
		.hide-mobile {display:none !important;}
		.mobile-d-block {display:block !important;}
		.mobile-d-iblock {display: inline-block !important;}
		.w100pc {width:100% !important;}
		table.w100pc {float:none !important;}
		td.w100pc {display:block !important;}
		img.w100pc {display:block;width:100% !important;height:auto !important;}
	}
	@media only screen and (max-width: 480px) {
        /* gabarit */                        
        .mobile_320{ width: 320px !important;margin: 0 auto !important; }
        .mobile_display_none{ display: none !important; }           
        /* android gmail 6 */            
        .mobile_320 {width: 320px !important;}
        .blk {display: block !important;}                        
        .height_122 {height: 122px !important;}
        .no {display: none !important;width:0px !important;height:0px !important;}
        .show {overflow:visible !important; float:none !important;display:block !important;line-height:normal !important;max-height:none !important;width:100% !important;margin:0 auto !important;}
    }
	
	@media only screen and (min-width: 481px) {
        .show {display:none;}
    }
</style>
    <!--[if gte mso 9]>
    <style type="text/css">
        sup { font-size: 100% !important; }
    </style>
    <![endif]-->
    
    <!--[if gte mso 9]><xml>
    <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml><![endif]-->
</head>
<body bgcolor="#FFFFFF" link="#000000" style="width:100%;margin:0;padding:0;-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;" vlink="#000000">
	<table class="w100pc" width="640" align="center" bgcolor="#ffffff" cellspacing="0" cellpadding="0" border="0" style="margin:0 auto;padding:0;mso-cellspacing:0;mso-padding-alt:0 0 0 0;">
		<tr>
			<td align="center">
				<table class="w100pc" width="640" align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0">

					<!-- PRE-HEADER SOUS OBJET -->
					<tr>
						<td style="display: none !important;font-size: 0 !important;line-height: 0 !important;float:left; mso-hide: all; overflow: hidden; max-height: 0; width: 0;">
							<span class="preheader" style="color:transparent; display:none !important; height:0; opacity:0; visibility:hidden; width:0">
								Profitez-vite de votre offre en magasin d\'usine !  
							</span>
							<div style="display: none; max-height: 0px; overflow: hidden;">
								&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
								&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
								&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
								&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
								&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
								&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
								&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
								&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
								&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
								&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
							</div>
						</td>
					</tr>

					<!-- /PRE-HEADER SOUS OBJET -->

					<!-- PRE-HEADER -->
					<tr>
						<td valign="top" align="center" style="padding: 13px 0;text-align: center;">
							<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
								<tr>
									<td align="center" style="padding: 0 20px 0;font-family: Arial, Helvetica, sans-serif;font-size: 11px;color: #15284b;" class="mobile-d-iblock">
										Profitez-vite de votre offre en magasin d\'usine !  
									</td>
									<td align="center" style="padding: 0 20px 0;font-family: Arial, Helvetica, sans-serif;font-size: 11px;color: #aeaeae;" class="mobile-d-iblock">
										<a href="<%@ include option=\'NmsServer_MirrorPageUrl\' %>/nl/jsp/m.jsp?c=<%=escapeUrl(cryptString(message.deliveryPartId.toString(16)+'|'+message.id.toString(16)))%>" target="_blank" title="Version en ligne" style="font-family: Arial, Helvetica, sans-serif;font-size: 11px;color: #aeaeae;">Version en ligne</a>&nbsp;/&nbsp;<a href="http://abonnes.news.crm-petitbateau.com/nms/jsp/webForm.jsp?fo=DesaboPetitbateau_INT&id=%40O0pnN5ApGYFh7frxyAFLqw%3D%3D" target="_blank" title="Se d&eacute;sinscrire" style="font-family: Arial, Helvetica, sans-serif;font-size: 11px;color: #aeaeae;">Se d&eacute;sinscrire</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<!-- /PRE-HEADER -->
					<!-- HEADER -->
					<tr>
						<td height="20"></td>
					</tr>
					<tr class="no">
						<td align="center" bgcolor="#FFFFFF" style="">
							<img src="https://www.myfrenchstartup.com/notifs/img/mu_logo.png" width="640" height="147" alt="PETIT BATEAU MAGASIN D\'USINE" title="PETIT BATEAU MAGASIN D\'USINE" border="0" style="display:block;margin:0 auto;" />
						</td>
					</tr>
					<tr>
						<td>
							<!--[if !mso]><!-->
								<div class="mobile-only" style="float:left; font-size: 0; mso-hide: all; overflow: hidden; max-height: 0; width: 0; display: none; line-height: 0;">
									<table cellspacing="0" cellpadding="0" border="0" align="center">
										<tr>
											<td align="center" bgcolor="#FFFFFF" style="">
												<img src="https://www.myfrenchstartup.com/notifs/img/mu_logo_mob.png" width="320" height="147" alt="PETIT BATEAU MAGASIN D\'USINE" title="PETIT BATEAU MAGASIN D\'USINE" border="0" style="display:block;margin:0 auto;" />
											</td>
										</tr>										
									</table>
								</div>
								<!--<![endif]-->  
						</td>
					</tr>
					<!-- /HEADER -->

					   <!-- CONTENT -->

               <tr>
                  <td valign="top" align="center" style="padding: 10px 0;">
                      <table cellpadding="0" cellspacing="0" border="0" style="margin:0 auto; padding:0;" width="" align="center">
                          <tr>
                             <td class="mobile_320" align="center">
                                 <table width="640" cellpadding="0" cellspacing="0" border="0" align="center" class="mobile_320">
                                     <tr>
                                         <td align="center">
                                             <table cellpadding="0" cellspacing="0" border="0" class="mobile_320"  width="640">
                                                 <tr>
                                                     <td>
                                                         <table  bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" class="mobile_320">
                                                             <tr class="no">
                                                                 <td align="center">
                                                                     <table cellpadding="0" cellspacing="0" border="0"  width="640">
                                                                         <tr>
                                                                             <td width="215" height="106">
                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/star_time_left.gif" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                             </td>
                                                                             <td width="17"></td>
                                                                             <td bgcolor="#FFFFFF">
                                                                              <table bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0">
                                                                                 <tr>
                                                                                    <td bgcolor="#FFFFFF" width="250" height="86" align="left" valign="bottom">
                                                                                          <span style="color:#002e5e;font-size:15px;line-height:21px;;vertical-align: bottom;">
                                                                                             Du 25 au 28 novembre uniquement 
                                                                                             dans votre magasin d\'usine 
                                                                                          </span>
                                                                                      </td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td height="20"></td>
                                                                                 </tr>
                                                                              </table>
                                                                             </td>
                                                                             <td width="160" height="106">
                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/star_time_right.gif" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                             </td>
                                                                         </tr>                     
                                                                     </table>
                                                                 </td>
                                                             </tr>
                                                             <tr>
                                                               <td  bgcolor="#FFFFFF" width="640" height="68" style="font-size:17px;line-height:19px;vertical-align:middle;text-align:center;color:#111d3a;" valign="middle" class="no">
                                                                       <span style="color:#002e5e;">
                                                                        <span style="color:#dcb93e">(Julie),</span> <br>
                                                                        comme vous n\'&ecirc;tes pas une cliente comme les autres, <br>b&eacute;n&eacute;ficiez de votre
                                                                       </span>
                                                                  </td>
                                                             </tr>
                                                             <tr class="no">
                                                                 <td align="center">
                                                                     <table cellpadding="0" cellspacing="0" border="0" width="640">
                                                                         <tr>
                                                                             <td width="160" height="95">
                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/day_star_left.png" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                             </td>
                                                                             <td bgcolor="#FFFFFF" >
                                                                              <table bgcolor="#FFFFFF"  cellpadding="0" cellspacing="0" border="0">
                                                                                 <tr>
                                                                                    <td bgcolor="#FFFFFF"  width="320" height="85" style="font-size:30px;line-height:32px;vertical-align:bottom;text-align:center;color:#dcb93e;" valign="bottom">
                                                                                         <span style="color:#dcb93e;">
                                                                                          JOURN&Eacute;E EN OR
                                                                                         </span>
                                                                                    </td> 
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td height="10"></td>
                                                                                 </tr>
                                                                              </table>
                                                                             </td>
                                                                             <td width="160" height="95">
                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/day_star_right.png" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                             </td>
                                                                         </tr>                     
                                                                     </table>
                                                                 </td>
                                                             </tr>
                                                             <tr class="no">
                                                                 <td align="center">
                                                                     <table cellpadding="0" cellspacing="0" border="0" width="640">
                                                                         <tr valign="top">
                                                                             <td width="160" height="145">
                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/per_star_left.gif" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                             </td>
                                                                             <td>
                                                                                <table cellpadding="0" cellspacing="0" border="0" class="mobile_320" width="320">
                                                                                    <tr>
                                                                                        <td width="320" height="91">
                                                                                            <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/thirty.png" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                          <td bgcolor="#FFFFFF" width="320" height="54" style="font-size:20px;line-height:20px;vertical-align:middle;text-align:center;color:#dcb93e;" valign="middle">
                                                                                         <span style="color:#dcb93e;">
                                                                                          sur tout*<br>d&egrave;s 5 articles achet&eacute;s
                                                                                         </span>
                                                                                   </td>
                                                                                    </tr>
                                                                                </table>
                                                                             </td>
                                                                             <td width="160" height="145">
                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/per_star_left.gif" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                             </td>
                                                                         </tr>
                                                                     </table>
                                                                 </td>
                                                             </tr>
                                                             <tr class="no">
                                                                 <td align="center">
                                                                     <table bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" width="640">
                                                                         <tr >
                                                                             <td width="160" height="54">
                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/quick_star_left.gif" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                             </td>
                                                                             <td bgcolor="#FFFFFF" width="320" height="54" style="font-size:17px;line-height:19px;vertical-align:middle;text-align:center;color:#00235a;" valign="middle">
                                                                                <span style="color:#00235a;">
                                                                                 <b>Vite ! J\'en profite en magasin d\'usine :</b>
                                                                                </span>
                                                                           </td>
                                                                             <td width="160" height="54">
                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/quick_star_right.gif" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                             </td>
                                                                         </tr>
                                                                     </table>
                                                                 </td>
                                                             </tr>
                                                             <tr class="no">
                                                                 <td align="center">
                                                                     <table cellpadding="0" cellspacing="0" border="0" width="640">
                                                                         <tr>
                                                                             <td width="157" height="150">
                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/box_star_left.gif"width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                             </td>
                                                                             <td>
                                                                                 <table cellpadding="0" cellspacing="0" border="0" width="321">
                                                                                     <tr>
                                                                                         <td width="7" height="150">
                                                                                             <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/fram_left.jpg" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                                         </td>
                                                                                         <td>
                                                                                            <table cellpadding="0" cellspacing="0" border="0"  width="308">
                                                                                                 <tr>
                                                                                                     <td width="308" height="6">
                                                                                                         <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/frame_top.jpg" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                                                     </td>
                                                                                                 </tr>
                                                                                                 <tr>
                                                                                                   <td  bgcolor="#FFFFFF" width="308" height="37" style="font-size:16px;line-height:17px;vertical-align:bottom;text-align:center;color:#002e5e;" valign="bottom">
                                                                                                  <span style="color:#002e5e;">
                                                                                                   <b>PETIT BATEAU ROUBAIX</b>
                                                                                                  </span>
                                                                                             </td>
                                                                                                 </tr>
                                                                                                 <tr>
                                                                                                   <td height="10"></td>
                                                                                                 </tr>
                                                                                                 <tr>
                                                                                                   <td  bgcolor="#FFFFFF" width="308" height="59" style="font-size:16px;line-height:17px;vertical-align:top;text-align:center;color:#002e5e;" valign="top">
                                                                                                  <span style="color:#002e5e;">
                                                                                                   <b>62 mail Delannoy<br>
                                                                                                      Mac Arthur Glen<br>
                                                                                                      59100 Roubaix
                                                                                                   </b>
                                                                                                  </span>
                                                                                             </td>
                                                                                                 </tr>
                                                                                                 <tr>
                                                                                                   <td width="308" height="32" style="font-size:16px;line-height:17px;vertical-align:top;text-align:center;color:#002e5e;" valign="top">
                                                                                                  <span style="color:#002e5e;">
                                                                                                   <b>03 20 20 90 40</b>
                                                                                                  </span>
                                                                                             </td>
                                                                                                 </tr>
                                                                                                 <tr>
                                                                                                     <td width="308" height="6">
                                                                                                         <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/frame_btm.jpg" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                                                     </td>
                                                                                                 </tr>
                                                                                            </table> 
                                                                                         </td>
                                                                                         <td width="8" height="150">
                                                                                             <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/frame_right.jpg" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                                         </td>
                                                                                     </tr>
                                                                                 </table>
                                                                             </td>
                                                                             <td width="160" height="150">
                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/box_star_right.gif" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                             </td>
                                                                         </tr>
                                                                     </table>
                                                                 </td>
                                                             </tr>
                                                             <tr class="no">
                                                                 <td align="center" width="640" height="63" >
                                                                     <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/btm.gif" width="100%" height="100%" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                 </td>
                                                             </tr>
                                                             <tr>
                                                                 <td height="12" align="center">
                                                                     <!--[if !mso 9]><!-->
                                                                     <div style="overflow:hidden; float:left; display:none; line-height:0px; max-height: 0px; font-size: 0px;" class="show">                           
                                                                     </div>
                                                                     <!--<![endif]-->    
                                                                 </td>
                                                             </tr>
                                                             <tr align="center">
                                                                 <td align="center">
                                                                     <!--[if !mso 9]><!-->
                                                                     <div style="overflow:hidden; float:left; display:none; line-height:0px; max-height: 0px; font-size: 0px;" class="show">
                                                                         <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/mobile_clock.png" width="320" height="59" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />                              
                                                                     </div>
                                                                     <!--<![endif]-->    
                                                                 </td>
                                                             </tr>
                                                             <tr>
                                                                 <td style="text-align:center;" align="center">
                                                                     <!--[if !mso 9]><!-->
                                                                     <div style="overflow:hidden; float:left; display:none; line-height:0px; max-height: 0px; font-size: 0px;" class="show">
                                                                         <table  bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" class="mobile_320" width="320">
                                                                             <tr>
                                                                                 <td  bgcolor="#FFFFFF" height="68">
                                                                                     <span style="color:#002e5e;font-size:19px!important;line-height:21px!important;-webkit-text-size-adjust:none!important;white-space:nowrap!important;">Du 25 au 28 novembre uniquement <br>dans votre magasin d\'usine </span>
                                                                                 </td>
                                                                             </tr>
                                                                         </table>
                                                                     </div>
                                                                     <!--<![endif]-->    
                                                                 </td>
                                                             </tr>
                                                             <tr>
                                                                 <td valign="top" align="center">
                                                                     <!--[if !mso 9]><!-->
                                                                     <div style="overflow:hidden; float:left; display:none; line-height:0px; max-height: 0px; font-size: 0px;" class="show">
                                                                         <table cellpadding="0" cellspacing="0" border="0" class="mobile_320" width="320">
                                                                             <tr>
                                                                                 <td>
                                                                                     <table cellpadding="0" cellspacing="0" border="0" class="mobile_320" width="320">
                                                                                         <tr>
                                                                                             <td>
                                                                                                <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/julie_star_right.png" width="110" height="40" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" /> 
                                                                                             </td>
                                                                                             <td width="100" height="40" align="center" valign="bottom">
                                                                                                 <span style="color:#dcb93e;font-size:17px!important;line-height:22px!important;-webkit-text-size-adjust:none!important;white-space:nowrap!important;text-align:center;">(Julie),</span>
                                                                                             </td>
                                                                                             <td>
                                                                                                <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/julie_star_right.gif" width="110" height="40" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" /> 
                                                                                             </td>
                                                                                         </tr>
                                                                                     </table>
                                                                                 </td>
                                                                             </tr>
                                                                             <tr>
                                                                              <td width="320" height="52" style="font-size:17px;line-height:18px;vertical-align:middle;text-align:center;color:#002e5e;" valign="top">
                                                                                   <span style="color:#002e5e;">
                                                                                    Comme vous n\'&ecirc;tes pas une cliente <br>comme les autres, b&eacute;n&eacute;ficiez de votre
                                                                                   </span>
                                                                              </td>
                                                                             </tr>
                                                                             <tr>
                                                                                 <td>
                                                                                     <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/mobile_star.png" width="320" height="26" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" /> 
                                                                                 </td>
                                                                             </tr>
                                                                             <tr bgcolor="#FFFFFF" >
                                                                                 <td bgcolor="#FFFFFF"  style="text-align:center;">
                                                                                     <span style="color:#dcb93e;font-size:30px!important;line-height:22px!important;-webkit-text-size-adjust:none!important;white-space:nowrap!important;text-align:center;">JOURN&Eacute;E EN OR</span>
                                                                                 </td>
                                                                             </tr>
                                                                             <tr>
                                                                                 <td>
                                                                                     <table cellpadding="0" cellspacing="0" border="0" class="mobile_320" width="320">
                                                                                         <tr>
                                                                                             <td>
                                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/mobile_thirty.png" width="256" height="110" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" /> 
                                                                                             </td>
                                                                                             <td>
                                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/mobile_per_star.gif" width="64" height="110" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" /> 
                                                                                             </td>
                                                                                         </tr>
                                                                                     </table>
                                                                                 </td>
                                                                             </tr>
                                                                             <tr>
                                                                                <td bgcolor="#FFFFFF" align="center" valign="middle" width="320" height="41">
                                                                                    <span style="color:#dcb93e;font-size:20px!important;line-height:22px!important;-webkit-text-size-adjust:none!important;white-space:nowrap!important;text-align:center;">sur tout*<br>d&egrave;s 5 articles achet&eacute;s</span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                 <td>
                                                                                     <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/text_btm.gif" width="320" height="61" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" /> 
                                                                                 </td>
                                                                             </tr>
                                                                             <tr>
                                                                                 <td bgcolor="#FFFFFF" style="text-align:center;" height="53" width="320">
                                                                                     <span style="color:#00235a;font-size:17px;line-height:18px!important;-webkit-text-size-adjust:none!important;white-space:nowrap!important;text-align:center;"><b>Vite ! J\'en profite <br>en magasin d\'usine :</b></span>
                                                                                 </td>
                                                                             </tr>
                                                                         </table>
                                                                     </div>
                                                                     <!--<![endif]-->    
                                                                 </td>
                                                             </tr>
                                                             <tr>
                                                                 <td align="center">
                                                                     <!--[if !mso 9]><!-->
                                                                     <div style="overflow:hidden; float:left; display:none; line-height:0px; max-height: 0px; font-size: 0px;" class="show">                             
                                                                         <table cellpadding="0" cellspacing="0" border="0" class="mobile_320" width="320">
                                                                             <tr>
                                                                                 <td>
                                                                                     <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/fram_left.jpg" width="7" height="150" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                                 </td>
                                                                                 <td>
                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="308">
                                                                                         <tr>
                                                                                             <td>
                                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/frame_top.jpg" width="308" height="6" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                                             </td>
                                                                                         </tr>
                                                                                         <tr>
                                                                                             <td  bgcolor="#FFFFFF" width="308" height="37" style="font-size:16px;line-height:17px;vertical-align:bottom;text-align:center;color:#002e5e;" valign="bottom">
                                                                                            <span style="color:#002e5e;">
                                                                                             <b>PETIT BATEAU ROUBAIX</b>
                                                                                            </span>
                                                                                       </td>
                                                                                           </tr>
                                                                                           <tr>
                                                                                             <td height="10"></td>
                                                                                           </tr>
                                                                                           <tr>
                                                                                             <td  bgcolor="#FFFFFF" width="308" height="59" style="font-size:16px;line-height:17px;vertical-align:top;text-align:center;color:#002e5e;" valign="top">
                                                                                            <span style="color:#002e5e;">
                                                                                             <b>62 mail Delannoy<br>
                                                                                                Mac Arthur Glen<br>
                                                                                                59100 Roubaix
                                                                                             </b>
                                                                                            </span>
                                                                                       </td>
                                                                                           </tr>
                                                                                           <tr>
                                                                                             <td width="308" height="32" style="font-size:16px;line-height:17px;vertical-align:top;text-align:center;color:#002e5e;" valign="top">
                                                                                            <span style="color:#002e5e;">
                                                                                             <b>03 20 20 90 40</b>
                                                                                            </span>
                                                                                       </td>
                                                                                           </tr>
                                                                                         <tr>
                                                                                             <td>
                                                                                                 <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/frame_btm.jpg" width="308" height="6" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                                             </td>
                                                                                         </tr>
                                                                                    </table> 
                                                                                 </td>
                                                                                 <td valign="left">
                                                                                     <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/frame_right.jpg" width="6" height="150" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" />
                                                                                 </td>
                                                                             </tr>
                                                                         </table>                                                                        
                                                                     </div>
                                                                     <!--<![endif]-->    
                                                                 </td>
                                                             </tr>
                                                             <tr>
                                                                 <td align="center">
                                                                     <!--[if !mso 9]><!-->
                                                                     <div style="overflow:hidden; float:left; display:none; line-height:0px; max-height: 0px; font-size: 0px;" class="show">
                                                                         <img style="display:block;border:0px;" src="https://www.myfrenchstartup.com/notifs/img/mobile_btm.png" width="320" height="89" border="0" alt="-30% d&egrave;s 5 articles achet&eacute;s dans votre magasin d\'usine" /> 
                                                                     </div>
                                                                     <!--<![endif]-->  
                                                                 </td>
                                                             </tr>
                                                         </table>
                                                     </td>
                                                 </tr>
                                             </table>
                                         </td>
                                     </tr>
                                 </table>
                             </td>
                         </tr>
                     </table>
                  </td>
               </tr>

               <!-- /CONTENT -->
              

					<!-- FOOTER -->
					<tr>
						<td class="no">
							<table cellpadding="0" cellspacing="0" border="0" align="center">
								<tr>
									<td>
										<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#f7f7f7">
											<tr>
												<td width="20"></td>
												<td> 
			                                        <img src="https://www.myfrenchstartup.com/notifs/img/lilboat.jpg" width="92" style="width: 100%; max-width:92px; font-family: sans-serif; color: #ffffff; font-size:20px; display: block; border: 0px;" border="0" alt="Preheader">
			                                    </td>
												<td>
													<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#f7f7f7">
														<tr>
															<td width="209" height="91" style="font-size:14px;line-height:17px;vertical-align:middle;text-align:left;color:#00235a" valign="middle">
																<span style="color:#00235a;">
																	Votre n&deg; client : <br>
																	021891000415
																</span>
				                                            </td> 
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td> 
                                        <img src="https://www.myfrenchstartup.com/notifs/img/middleman.jpg" width="2" style="width: 100%; max-width:2px; font-family: sans-serif; color: #ffffff; font-size:20px; display: block; border: 0px;" border="0" alt="Preheader">
                                    </td>
									<td>
										<table cellpadding="0" cellspacing="0" border="0" align="center">
											<tr>
												<td> 
													<a href="https://stores.petit-bateau.com/fr/search" target="_blank">
														<img src="https://www.myfrenchstartup.com/notifs/img/point.jpg" width="69" style="width: 100%; max-width:69px; font-family: sans-serif; color: #ffffff; font-size:20px; display: block; border: 0px;" border="0" alt="Preheader">
													</a>
			                                    </td>
												<td>
													<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#f7f7f7">
														<tr>
															<td width="248" height="91" style="font-size:14px;line-height:17px;vertical-align:middle;text-align:left;color:#00235a;" valign="middle">
																<a href="https://stores.petit-bateau.com/fr/search" target="_blank" style="font-size:14px;line-height:17px;vertical-align:middle;text-align:left;color:#00235a; text-decoration:none; vertical-align:middle;">
																	<span style="color:#00235a;">
				                                                        Trouvez votre magasin d\'usine <br>
				                                                        le plus proche
				                                                    </span>
																</a>
				                                                </a>
				                                            </td> 
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
                        <td>
                        <!--[if !mso 9]><!-->
                            <div style="overflow:hidden; float:left; display:none; line-height:0px; max-height: 0px; font-size: 0px;" class="show">
                                <table cellpadding="0" cellspacing="0" border="0"  style="margin:0 auto; padding:0;width:100%;" align="center" bgcolor="#f7f7f7"> 
                                    <tr>
				                		<td>
				                			<table cellpadding="0" cellspacing="0" border="0" align="center">
				                				<tr>
				                					<td>
				                						<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#f7f7f7">
				                							<tr>
				                								<td> 
							                                        <img src="https://www.myfrenchstartup.com/notifs/img/m_lilboat.jpg" width="149" style="width: 100%; max-width:149px; font-family: sans-serif; color: #ffffff; font-size:20px; display: block; border: 0px;" border="0" alt="Preheader">
							                                    </td>
				                							</tr>
				                							<tr>
				                								<td height="4"></td>
				                							</tr>
				                							<tr>
				                								<td>
																	<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#f7f7f7">
																		<tr>
																			<td width="148" height="72" style="font-size:14px;line-height:17px;vertical-align:top;text-align:center;color:#00235a" valign="top">
																					<span style="color:#00235a;">
								                                                        Votre n&deg; client : <br>
								                                                        021891000415
								                                                    </span>
								                                                </a>
								                                            </td> 
																		</tr>
																	</table>
																</td>
				                							</tr>
				                						</table>
				                					</td>
				                					<td> 
				                                        <img src="https://www.myfrenchstartup.com/notifs/img/m_middleman.jpg" width="2" style="width: 100%; max-width:2px; font-family: sans-serif; color: #ffffff; font-size:20px; display: block; border: 0px;" border="0" alt="Preheader">
				                                    </td>
				                					<td>
				                						<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#f7f7f7">
				                							<tr>
				                								<td> 
																	<a href="https://stores.petit-bateau.com/fr/search" target="_blank">
																		<img src="https://www.myfrenchstartup.com/notifs/img/m_point.jpg" width="150" style="width: 100%; max-width:150px; font-family: sans-serif; color: #ffffff; font-size:20px; display: block; border: 0px;" border="0" alt="Preheader">
																	</a>
							                                    </td>
				                							</tr>
				                							<tr>
				                								<td height="4"></td>
				                							</tr>
				                							<tr>
				                								<td>
																	<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#f7f7f7">
																		<tr>
																			
																				<td width="150" height="72" style="font-size:14px;line-height:17px;vertical-align:top;text-align:center;color:#00235a" valign="top">
																					<a href="https://stores.petit-bateau.com/fr/search" target="_blank" style=" font-size:14px;line-height:17px;vertical-align:top;text-align:center;color:#00235a; text-decoration:none; vertical-align:top;">
																						<span style="color:#00235a;">
																							Trouvez votre <br>
																							magasin d\'usine <br>
																							le plus proche
																						</span>
																					</a>
																				</td> 
																		</tr>
																	</table>
																</td>
				                							</tr>
				                						</table>
				                					</td>
				                				</tr>
				                			</table>
				                		</td>
				                	</tr>
                                </table>
                            </div>
                        <!--<![endif]--> 
                        </td>
                    </tr>
					<tr>
						<td height="2"></td>
					</tr>
					<!-- /FOOTER -->


					<!-- RESEAU -->

					<tr>
						<td valign="top" align="center" bgcolor="#f7f7f7" style="padding: 18px 0;">
							<table cellspacing="0" cellpadding="0" align="center" border="0"  width="100%">
								<tr>
									<td valign="top" align="center" style="font-size: 12px;color: #15284b;font-family: Arial, Helvetica, sans-serif;text-decoration: none;padding-bottom: 13px;">
										Partagez vos photos avec <b><a href="https://www.instagram.com/petitbateau" target="_blank" title="#petitbateaubigfamily" style="text-decoration: none;font-size: 12px;color: #15284b;font-family: Arial, Helvetica, sans-serif;">#petitbateaubigfamily</a></b><i class="mobile-d-block"></i> et suivez toute l\'actu de la marque
									</td>
								</tr>
								<tr>
									<td valign="top" align="center">
										<table cellspacing="0" cellpadding="0" border="0" align="center">
											<tr>
												<td valign="top" align="center">
													<table cellspacing="0" cellpadding="0" border="0" align="center">
														<tr>

															<td valign="top" align="center">
																<a href="https://www.instagram.com/petitbateau" target="_blank" title="instagram">
																	<img src="https://www.myfrenchstartup.com/notifs/img/instagram.png" width="18" height="22" alt="instagram" title="instagram" border="0" style="display:block;" />
																</a>
															</td>
															<td valign="top" align="center" style="padding: 0 10px;">
																<table cellspacing="0" cellpadding="0" border="0" align="center">
																	<tr>
																		<td width="2" height="22" bgcolor="#FFFFFF">
																			&nbsp;
																		</td>
																	</tr>
																</table>
															</td>
															<td valign="top" align="center">
																<a href="https://www.facebook.com/petitbateau" target="_blank" title="facebook">
																	<img src="https://www.myfrenchstartup.com/notifs/img/facebook.png" width="10" height="22" alt="facebook" title="facebook" border="0" style="display:block;" />
																</a>
															</td>
															<td valign="top" align="center" style="padding: 0 10px;">
																<table cellspacing="0" cellpadding="0" border="0" align="center">
																	<tr>
																		<td width="2" height="22" bgcolor="#FFFFFF">
																			&nbsp;
																		</td>
																	</tr>
																</table>
															</td>
															<td valign="top" align="center">
																<a href="https://www.youtube.com/PetitBateau" target="_blank" title="youtube">
																	<img src="https://www.myfrenchstartup.com/notifs/img/youtube.png" width="10" height="22" alt="youtube" title="youtube" border="0" style="display:block;" />
																</a>
															</td>
															<td valign="top" align="center" style="padding: 0 10px;">
																<table cellspacing="0" cellpadding="0" border="0" align="center">
																	<tr>
																		<td width="2" height="22" bgcolor="#FFFFFF">
																			&nbsp;
																		</td>
																	</tr>
																</table>
															</td>
															<td valign="top" align="center">
																<a href="https://www.pinterest.fr/petitbateau" target="_blank" title="pinterest">
																	<img src="https://www.myfrenchstartup.com/notifs/img/pinterest.png" width="15" height="22" alt="pinterest" title="pinterest" border="0" style="display:block;" />
																</a>
															</td>
															<td valign="top" align="center" style="padding: 0 10px;">
																<table cellspacing="0" cellpadding="0" border="0" align="center">
																	<tr>
																		<td width="2" height="22" bgcolor="#FFFFFF">
																			&nbsp;
																		</td>
																	</tr>
																</table>
															</td>
															<td valign="top" align="center">
																<a href="https://blog.petit-bateau.fr/" target="_blank" title="le petit blog">
																	<img src="https://www.myfrenchstartup.com/notifs/img/blog.png" width="95" height="22" alt="le petit blog" title="le petit blog" border="0" style="display:block;" />
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="center">
										<!--[if !mso]><!-->
										<div class="mobile-only" style="float:left; font-size: 0; mso-hide: all; overflow: hidden; max-height: 0; width: 0; display: none; line-height: 0;">
											<table border="0" cellpadding="0" cellspacing="0" style="mso-cellspacing: 0px; mso-padding-alt: 0 0 0 0" width="100%">
												<!-- DIVIDER -->
												<tr>
													<td valign="top" style="padding: 20px 40px;">
														<table cellspacing="0" cellpadding="0" border="0" class="w100pc">
															<tr>
																<td valign="top" align="center" width="100%" height="2" bgcolor="#FFFFFF" style="line-height: 2px;font-size: 2px; height: 2px;">
																	&nbsp;
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<!-- /DIVIDER -->
												<tr>
													<td valign="top" align="center" style="font-size: 12px;color: #15284b;font-family: Arial, Helvetica, sans-serif;text-decoration: none;padding-bottom: 13px;">
														Hop, par ici <b>l\'appli mobile</b>
													</td>
												</tr>
												<tr>
													<td valign="top" align="center">
														<table cellspacing="0" cellpadding="0" align="center" border="0">
															<tr>
																<td valign="top" align="center">
																	<a href="https://play.google.com/store/apps/details" target="_blank" title="android">
																		<img src="https://www.myfrenchstartup.com/notifs/img/android.png" width="19" height="22" alt="android" title="android" border="0" style="display:block;" />
																	</a>
																</td>
																<td valign="top" align="center" style="padding: 0 10px;">
																	<table cellspacing="0" cellpadding="0" border="0" align="center">
																		<tr>
																			<td width="2" height="22" bgcolor="#FFFFFF">
																				&nbsp;
																			</td>
																		</tr>
																	</table>
																</td>
																<td valign="top" align="center">
																	<a href="https://itunes.apple.com/fr/app/petit-bateau/id1164833506" target="_blank" title="apple">
																		<img src="https://www.myfrenchstartup.com/notifs/img/apple.png" width="16" height="22" alt="apple" title="apple" border="0" style="display:block;" />
																	</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</div>
										<!--<![endif]-->
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<!-- /RESEAU -->

					<!-- MENTIONS LEGALES -->
					<tr>
						<td align="center" style="padding:20px;font-family:Arial,sans-serif;font-size:11px;color:#aeaeae;">
							 *Offre valable du 25 au 28 novembre 2021 inclus : -30% sur la collection Jour, les sous-v&ecirc;tements, les articles de nuit et les accessoires (hors articles de seconde main pour les magasins concern&eacute;s), sous r&eacute;serve de l\'achat d\'au moins 5 articles &eacute;ligibles en un seul acte d\'achat. Offre r&eacute;serv&eacute;e aux adh&eacute;rents et nouveaux adh&eacute;rents du programme fid&eacute;lit&eacute; des magasins d\'usine Petit Bateau. Offre valable dans les magasins d\'usine Petit Bateau et le magasin d\'usine Petit Bateau partenaire de Roanne, ouverts le dimanche le cas &eacute;ch&eacute;ant. Offre non cumulable avec d\'autres offres promotionnelles en cours. R&eacute;duction applicable en caisse. Liste des magasins d\'usine consultable sur le site internet :  <a href="https://www.petit-bateau.fr" target="_blank" title="en cliquant ici" style="font-family:Arial,sans-serif;font-size:11px;color:#aeaeae;">www.petit-bateau.fr</a>.<br><br>
							
							Vos donn&eacute;es font l\'objet d\'un traitement informatis&eacute; sous la responsabilit&eacute; de Petit Bateau RCS 542 880 125, et sont collect&eacute;es suite &agrave; votre adh&eacute;sion &agrave; la newsletter de Petit Bateau.<br>
							Vos donn&eacute;es sont collect&eacute;es et trait&eacute;es selon la politique de protection des donn&eacute;es &agrave; caract&egrave;re personnel mise en ouvre par Petit Bateau que vous trouverez <a href="https://www.petit-bateau.fr/c/c6/cookies.html" target="_blank" title="en cliquant ici" style="font-family:Arial,sans-serif;font-size:11px;color:#aeaeae;">en cliquant ici</a>.<br>
							Pour exercer vos droits tels que le droit d\'acc&egrave;s, de rectification, d\'opposition et d\'effacement, vous pouvez nous &eacute;crire &agrave; l\'adresse postale &laquo; Petit Bateau Service Consommateurs 15 rue du Lieutenant Pierre Murard, BP 525, 10081 Troyes Cedex &raquo; ou encore par email &agrave; l\'adresse <a href="mailto:privacy@fr.petitbateau.com" title="privacy@fr.petitbateau.com" style="font-family:Arial,sans-serif;font-size:11px;color:#aeaeae;">privacy@fr.petitbateau.com</a>.<br>
							Si vous ne souhaitez plus recevoir d\'email de la part de Petit Bateau, vous pouvez vous d&eacute;sinscrire <a href="http://abonnes.news.crm-petitbateau.com/nms/jsp/webForm.jsp?fo=DesaboPetitbateau_INT&id=%40O0pnN5ApGYFh7frxyAFLqw%3D%3D" style="color:#aeaeae;" target="_blank">ici</a>.<br>							
						</td>
					</tr>
					<!-- /MENTIONS LEGALES -->
				</table>
			</td>
		</tr>
	</table>
	
	<img src="https://er.cloud-media.fr/r/<%= recipient.email %>/d6746642-ed1f-4c6b-b158-7a7c3a3430d8" width="1" height="1" />

	<div class="gmailfix" style="white-space:nowrap;font:15px courier;line-height:0;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	</div>
</body>
</html>';


$mail = new PHPMailer();

    $mail->IsSMTP();
    $mail->Host = 'ssl0.ovh.net';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'info@myfrenchstartup.com';                 // SMTP username
    $mail->Password = '04Betterway';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                               // TCP port to connect to

    $mail->setFrom('info@myfrenchstartup.com');
    $mail->addAddress('sami.jilani.dev@gmail.com');     // Add a recipient
    $mail->addAddress('samijilani@live.com');     // Add a recipient
   


    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = "Test ";

    $mail->Body = utf8_decode($message);

    if (!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {

        }
?>