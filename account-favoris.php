<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
if (!isset($_SESSION['data_login'])) {
    header("location:" . URL . "/connexion");
    exit();
}
$mon_user = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}

$currentPage = "favoris";

function change_date_fr_chaine_related($date) {
    $split1 = explode(" ", $date);
    $split = explode("-", $split1[0]);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Mes favoris - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>

        <div class="page-wrapper">
            <?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">            
                    <div class="account">
                        <div class="account__aside">
                            <div class="nav-vertical">
                                <div class="nav-vertical__title">
                                    <span class="title">Mon profil</span>
                                </div>
                                <div class="nav-vertical__list">
                                    <?php include ('account/menu.php'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="account__right">

                            <div class="section-title">
                                Mes startups favoris
                            </div>
                            <div class="tablebloc">
                                <table id="mine2" class="table table-search">
                                    <thead>
                                        <tr>
                                            <th>Startup</th>
                                            <th>Marché</th>
                                            <th>Maturité</th>
                                            <th>Total fonds levés</th>
                                            <th>Région</th>
                                            <th>Traction</th>
                                            <th>Création</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $dataas = mysqli_fetch_array(mysqli_query($link, "select * from user_favoris where user=" . $mon_user['id']));
                                        $details = explode(",", $dataas['liste']);
                                        $nbs = count($details);
                                        for ($i = 0; $i < $nbs; $i++) {
                                            if ($details[$i] != '') {
                                                $favoris = mysqli_fetch_array(mysqli_query($link, "select * from startup where id=" . $details[$i]));
                                                $secteurs = mysqli_fetch_array(mysqli_query($link, "Select secteur.nom_secteur,secteur.id,secteur.nom_secteur_en From   secteur Inner Join  activite    On activite.secteur = secteur.id Where  activite.id_startup =" . $favoris['id']));
                                                $score = mysqli_fetch_array(mysqli_query($link, "Select  * From   startup_score where  id_startup=" . $favoris['id']));
                                                $tot_lf = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $favoris['id']));

                                                $ssecteurs = mysqli_fetch_array(mysqli_query($link, "Select  * From  activite  Where  activite.id_startup =" . $favoris['id']));
                                                $ligne_secteur_activite = mysqli_fetch_array(mysqli_query($link, "select * from secteur_activite where id_secteur=" . $secteurs['id'] . " and id_sous_secteur=" . $idsoussecteur . " and id_activite=" . $idactivite . " and id_sous_activite=" . $idsousactivite));

                                                if ($ssecteurs['sous_activite'] == "")
                                                    $idsousactivite = 0;
                                                else
                                                    $idsousactivite = $ssecteurs['sous_activite'];

                                                if ($ssecteurs['sous_secteur'] == "")
                                                    $idsoussecteur = 0;
                                                else
                                                    $idsoussecteur = $ssecteurs['sous_secteur'];

                                                if ($ssecteurs['activite'] == "")
                                                    $idactivite = 0;
                                                else
                                                    $idactivite = $ssecteurs['activite'];
                                                ?>
                                                <tr>
                                                    <th class="table-search__startup">
                                                        <div class="startupInfos">
                                                            <div class="startupInfos__image">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($favoris['id']) . "/" . urlWriting(strtolower($favoris["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $favoris['logo']) ?>" alt="<?php echo "Startup " . $favoris["nom"]; ?>"></a>
                                                            </div>
                                                            <div class="startupInfos__desc">
                                                                <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($favoris['id']) . "/" . urlWriting(strtolower($favoris["nom"])) ?>" target="_blank"><?php echo stripslashes($favoris['nom']); ?></a></div>
                                                                <div class="companyLabel"><?php echo stripslashes($favoris['short_fr']); ?></div>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <td class="table-search__position center">
                                                        <?php echo ($secteurs['nom_secteur']); ?>
                                                    </td>
                                                    <td class="table-search__position center">
                                                        <?php echo $score['maturite']; ?> 
                                                        <?php if ($score['maturite'] == $ligne_secteur_activite['moy_maturite']) { ?>
                                                            <span class="ico-stable"></span> 
                                                        <?php } ?>
                                                        <?php if ($score['maturite'] > $ligne_secteur_activite['moy_maturite']) { ?>
                                                            <span class="ico-raise"></span> 
                                                        <?php } ?>
                                                        <?php if ($score['maturite'] < $ligne_secteur_activite['moy_maturite']) { ?>
                                                            <span class="ico-decrease"></span> 
                                                        <?php } ?>
                                                    </td>
                                                    <td class="table-search__position center"><?php echo str_replace(",0", "", number_format(($tot_lf['somme'] / 1000), 1, ",", " ")); ?></td>
                                                    <td class="table-search__position center"><?php echo ($favoris['region_new']); ?></td>
                                                    <td class="table-search__position center"><?php echo ($score['traction_likedin']); ?></td>
                                                    <td class="table-search__position center"><?php echo change_date_fr_chaine_related($favoris['date_complete']); ?></td>

                                                </tr>

                                                <?php
                                                $l++;
                                            }
                                        }
                                        ?>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>