<?php
//include("include/db.php");
include("functions/functions.php");
include ('config.php');
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Offre payante - <?= SITENAME; ?></title>
        <meta name="description" content="Foire aux questions <?= SITENAME; ?>">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php include ('layout/header-simple.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">
                    <div class="section-title section-title--fat">
                        Votre commande est validée !
                    </div>
                    <div class="alert alert--success">
                        Merci Laurent ! Nous vous confirmons que <strong>votre commande a été validée</strong> avec succès.
                    </div>
                    <div class="paiement-stripe-confirm">
                        <h2>Récapitulatif de votre commande</h2>
                        <div class="tablebloc">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Désignation</th>
                                        <th>Date</th>
                                        <th>Montant</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Abonnement <strong>Premium 1an</strong> &laquo; Une offre simple qui grandit en même temps que vous &raquo;
                                            <br />
                                            Expire le 03/12/2023 - Renouvellement automatique
                                        </td>
                                        <td>
                                            01/01/22
                                        </td>
                                        <td>
                                            89,00€ HT
                                        </td>
                                        <td>
                                            106,80€ TTC
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="paiement-stripe-confirm__cta">
                                <a href="<?php echo URL ?>/mon-compte" title="Mon compte" class="btn btn-primary">Accéder à mon compte &raquo;</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/animated.min.js"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>