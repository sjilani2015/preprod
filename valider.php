<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
if (isset($_SESSION['data_login'])) {
    header('location:' . URL . '/recherche-startups');
}

if (isset($_GET['num'])) {
    if ($_GET['num'] !== '') {
        $token = $_GET['num'];
        $oo = mysqli_query($link, "select * from user WHERE token = '" . $token . "'");
        $verif = mysqli_num_rows($oo);
        if ($verif == 1) {

            $new_token = uniqid();

            mysqli_query($link, "UPDATE user SET status= 1 ,token= '" . $new_token . "' WHERE token = '" . $token . "'");

            $users_list = mysqli_fetch_array($oo);

            $type = $users_list['type'];
            $mytype = "";
            if ($type == 0)
                $mytype = "Entrepreneur";
            if ($type == 1)
                $mytype = "Investisseur";
            if ($type == 3)
                $mytype = "Media";
            if ($type == 4)
                $mytype = "Institutionnel";
            if ($type == 5)
                $mytype = "Prestataire";
            if ($type == 6)
                $mytype = "Etudiant";
            if ($type == 7)
                $mytype = "Enseignant";
            if ($type == 10)
                $mytype = "Corporate";

            $email = $users_list['email'];
            $nom = $users_list['nom'];
            $prenom = $users_list['prenom'];
            $societe = $users_list['societe'];
            $fonction = $users_list['fonction'];
            $tel = $users_list['tel'];

////////////////////////
// API to mailchimp ########################################################
            $list_id = 'c58d4d1c92';
            $authToken = '972ffbc478dfe67f276f8999271bbe2e-us6';
// The data to send to the API
            $postData = array(
                "email_address" => "$email",
                "status" => "subscribed",
                "merge_fields" => array(
                    "LNAME" => "$nom",
                    "FNAME" => "$prenom",
                    "TYPE" => $mytype,
                    "COMPANY" => "$societe",
                    "JOB" => "$fonction",
                    "PHONE" => "$tel")
            );

// Setup cURL
            $ch = curl_init('https://us6.api.mailchimp.com/3.0/lists/' . $list_id . '/members/');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: apikey ' . $authToken,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));
// Send the request
            $response = curl_exec($ch);
// #######################################################################
//////////////////////////////////
            $body = "Inscription<br>
	Type : " . $mytype . "<br>
	Prénom : " . $prenom . "<br>
	Nom : " . $nom . "<br>
	Company : " . $societe . "<br>
	Fonction : " . $fonction . "<br>
	Tél : " . $tel . "<br>
	Email : " . $email . "<br>
	Date : " . date('Ym-d H:i:s');


            require 'phpmailer/PHPMailerAutoload.php';

            $mail = new PHPMailer();

            $mail->IsSMTP();
            $mail->Host = 'ssl0.ovh.net';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'inscription@myfrenchstartup.com';                 // SMTP username
            $mail->Password = '04Betterway#p';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                               // TCP port to connect to

            $mail->setFrom('inscription@myfrenchstartup.com','myFrenchStartup');
            $mail->addAddress('contact@myfrenchstartup.com');


            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = "Bienvenue Sur Myfrenchstartup !";

            $mail->Body = utf8_decode($body);

            if (!$mail->send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                
            }
        } else {
            echo '<script>window.location="' . URL . '"</script>';
        }
    } else {
        echo '<script>window.location="' . URL . '"</script>';
    }
} else {
    echo '<script>window.location="' . URL . '"</script>';
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>

        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">            
                    Votre compte est désormais actif. Vous pouvez désormais accéder à tous les services offerts par Myfrenchstartup.<br>

                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');

            function checkEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            function connexion() {
                document.getElementById("msg_error_mail").style.display = "none";
                document.getElementById("msg_error_user").style.display = "none";
                document.getElementById("msg_error_user_block").style.display = "none";
                var login = document.getElementById("login_connect").value;
                var pwd = document.getElementById("pwd").value;
                if (checkEmail(login)) {

                    $.ajax({
                        type: "POST",
                        url: "<?php echo URL ?>/verif_user.php?login=" + login + "&pwd=" + pwd,

                        success: function (data) {
                            var t = eval(data);
                            if (t[0].result == 0) {
                                document.getElementById("msg_error_user").style.display = "block";
                            } else {
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo URL ?>/verif_user_block.php?login=" + login + "&pwd=" + pwd,

                                    success: function (data1) {
                                        var t = eval(data1);
                                        if (t[0].result == 0) {
                                            document.getElementById("msg_error_user_block").style.display = "block";
                                        } else {
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo URL ?>/redirection.php?login=" + login + "&pwd=" + pwd,

                                                success: function (data2) {

                                                    window.location = data2;


                                                }
                                            });
                                        }


                                    }
                                });
                            }


                        }
                    });






                } else {
                    document.getElementById("msg_error_mail").style.display = "block";
                }

            }
        </script>
    </body>
</html>