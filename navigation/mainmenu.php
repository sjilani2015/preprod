<?php include ('../config.php'); ?>
<div class="mainmenu__shutter main">
    <ul class="mainmenu__categories">
        <?php foreach ($mainmenu as $menu) { ?>
            <li>
                <a href="<?=URL;?>/<?php echo $menu['link'];?>"  title="<?=$menu['title'];?>"><?=$menu['title'];?></a>
            </li>
        <?php } ?>
    </ul>
</div>