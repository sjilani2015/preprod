<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (!isset($_SESSION['data_login'])) {
    header("location:" . URL . "/connexion");
     exit();
}

// Include the configuration file  
require_once 'paiement-securise/config.php';

// Include the database connection file  
require_once 'paiement-securise/dbConnect.php';





$payment_id = $statusMsg = '';
$status = 'error';
$_SESSION['utm_source'] = 'https://www.myfrenchstartup.com/fr/';
// Check whether the subscription ID is not empty 
if (!empty($_GET['sid'])) {
    $subscr_id = base64_decode($_GET['sid']);

    // Fetch subscription info from the database 
    $sqlQ = "SELECT S.*, P.name as plan_name, P.price as plan_amount, U.first_name, U.last_name, U.email FROM user_subscriptions as S LEFT JOIN users_premium as U On U.id = S.user_id LEFT JOIN plans as P On P.id = S.plan_id WHERE S.id = ?";
    $stmt = $db->prepare($sqlQ);
    $stmt->bind_param("i", $db_id);
    $db_id = $subscr_id;
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        // Subscription and transaction details 
        $subscrData = $result->fetch_assoc();
        $stripe_subscription_id = $subscrData['stripe_subscription_id'];
        $paid_amount = $subscrData['paid_amount'];
        $paid_amount_currency = $subscrData['paid_amount_currency'];
        $plan_interval = $subscrData['plan_interval'];
        $plan_period_start = $subscrData['plan_period_start'];
        $plan_period_end = $subscrData['plan_period_end'];
        $subscr_status = $subscrData['status'];

        $plan_name = $subscrData['plan_name'];
        $plan_amount = $subscrData['plan_amount'];

        $customer_name = $subscrData['first_name'] . ' ' . $subscrData['last_name'];
        $customer_email = $subscrData['payer_email'];

        $status = 'success';
        $statusMsg = 'Transaction réussie';
        
         $pp = mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "' limit 1");
        $mon_user = mysqli_fetch_array($pp);
        
         $body = '<html>
    <head>
        <title>Inscription à MyFrenchStartup</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>
    <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <table align="center" id="Table_01" width="600" height="394" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <img src="http://www.myfrenchstartup.com/mailing/images/myfs_01.jpg" width="600" height="81" alt=""></td>
            </tr>
            <tr>
                <td>
                    <img src="http://www.myfrenchstartup.com/mailing/images/myfs_02.jpg" width="600" height="30" alt=""></td>
            </tr>
            <tr>
                <td width="600" bgcolor="#ffffff">';
      

        $body .= 'Bonjour ,<br /><br />Le client ' . stripslashes($mon_user['prenom']." ".$mon_user['nom']) ." (".$mon_user['email']. ') a fait un paiement de 89 EUR';

        $body .= '</td></tr>
            
            
            <tr>
                <td>
                    <img src="http://www.myfrenchstartup.com/mailing/images/myfs_04.jpg" width="600" height="59" alt=""></td>
            </tr>
            <tr>
                <td>
                    <img src="http://www.myfrenchstartup.com/mailing/images/myfs_05.jpg" width="600" height="48" alt=""></td>
            </tr>
        </table>
    </body>
</html>';
        
        
        
        
        
        
        
        
        
        
        require 'phpmailer/PHPMailerAutoload.php';

        $mail = new PHPMailer();

        $mail->IsSMTP();
        $mail->Host = 'ssl0.ovh.net';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
         $mail->Username = 'inscription@myfrenchstartup.com';                 // SMTP username
        $mail->Password = '04Betterway#p';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                               // TCP port to connect to

        $mail->setFrom('info@myfrenchstartup.com');
        $mail->addAddress('contact@myfrenchstartup.com');     // Add a recipient
        $mail->addBCC('samijilani@live.com');


        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = "Paiement Client MYFRENCHSTARTUP";

        $mail->Body = utf8_decode($body);

        if (!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    } else {
        $statusMsg = "Transaction has been failed!";
    }
} else {
    header("Location: index.php");
    exit;
}
?>
<!DOCTYPE html>
<head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
    </head>

<body class="preload page">
    <div id="mainmenu" class="mainmenu">
        <div class="mainmenu__wrapper"></div>
    </div>

    <div class="page-wrapper">
        <?php
        if (!isset($_SESSION['data_login'])) {
            include ('layout/header-simple.php');
        } else {
            include ('layout/header-connected.php');
        }
        ?>
        <div class="page-content" id="page-content">
            <div class="container" style="padding-left:0px;padding-right: 0px; padding-top: 25px; padding-bottom: 25px">
                <div class="colgrid1-2-1" style="margin-bottom:32px">
                    <?php if (!empty($subscr_id)) { ?>
                        <div style="margin-top:16px;background-color: #FFFFFF;padding: 25px;">
                            <h1 style="text-align: center" class="pricing-h1">Transaction réussie</h1>
                            <div style="text-align: center;line-height: 30px;">Merci<br>
                                Paiement effectué avec succès<br>
                                Veuillez noter le N° de la transaction : <b><?php echo $stripe_subscription_id; ?></b><br>
                                Votre transaction est terminée et vous allez recevoir par email un avis accusant réception de votre achat. Vous pouvez connecter à votre compte sur <a href="https://www.myfrenchstartup.com/mes-factures" target="_blank">myfrenchstartup.com/mes-factures</a> pour consulter les détails de cette transaction.


                            </div>




                        </div>
                    <?php } else { ?>
                        <h1 class="error">Your Transaction been failed!</h1>
                        <p class="error"><?php echo $statusMsg; ?></p>
                    <?php } ?>
                </div>
                <div><?php include('footer1.php'); ?></div>
            </div>
        </div>
    </div>

    <script src="<?php echo url; ?>jquery.min.js"></script>

    <script src="<?php echo url; ?>bootstrap.min.js"></script>
    <script src="<?php echo url; ?>jquery.knob.js"></script>
    <!-- Sparkline -->

    <script>
        $(document).ready(function () {
            $("#search-box").keyup(function () {
                $.ajax({
                    type: "POST",
                    url: "<?php echo url ?>readCountry.php",
                    data: 'keyword=' + $(this).val(),
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            });
        });
        function selectCountry(val) {
            const words = val.split('/');
            $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo url ?>' + val;
        }
        function selectInvest(val) {
            const words = val.split('/');
            $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo url ?>' + val;
        }
        function selectEntrepreneur(val) {
            const words = val.split('/');
            $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo url ?>' + val;
        }
        function selectTags(val) {
            const words = val.split('/');
            $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo url ?>' + val;
        }
        function selectRegion(val) {
            const words = val.split('/');
            $("#search-box").val(words[2]);
            $("#suggesstion-box").hide();
            window.location = '<?php echo url ?>' + val;
        }




    </script>
</body>
</html>