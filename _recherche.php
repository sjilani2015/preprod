<?php 
    include ('config.php'); 
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
<head>
    <?php include ('metaheaders.php'); ?>
    <title><?=SITENAME;?></title>
    <meta name="description" content="<?=METADESC;?>">
</head>

<?php 
/*
    sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
 */
 ?>
<body class="preload page">
<div id="mainmenu" class="mainmenu">
    <div class="mainmenu__wrapper"></div>
</div>
<div class="page-wrapper">
    <?php include ('layout/header-connected.php'); ?>
    <?php include ('recherche/filters.php'); ?>
    <?php include ('recherche/columns.php'); ?>
    <?php include ('recherche/save.php'); ?>
    <div class="page-content" id="page-content">
        <div class="table-search-wrapper">
            <div class="container-fluid">
                <div class="banner-partner">
                    <a href="https://www.myfrenchstartup.com/channel/" target="_blank"> <img src="https://www.myfrenchstartup.com/images/banner.png" alt=""></a>
                </div>
                <div class="searchHeader">
                    <div class="searchHeader__title">10 startups affichées / 19824 startups trouvées</div>
                    <div class="searchHeader__actions">
                        <button class="btn btn-sm btn-bordered btn-searchfilters btn-filter">Filtrer</button>
                        <button class="btn btn-sm btn-primary btn-bookmark btn-saveSearch">Enregistrer</button>
                        <button class="btn btn-sm btn-primary btn-reset">Réinitialiser</button>
                    </div>
                </div>
                <div class="searchFilters">
                    <div class="searchFilters__tags tags tags--left">
                        <a href="#" title="" class="tags__el tags__el--pinky">
                            <span class="ico-suitcase"></span> Électronique & Composants
                        </a>
                        <a href="#" title="" class="tags__el tags__el--pinky">
                            <span class="ico-chart"></span> Occitanie
                        </a>
                        <a href="#" title="" class="tags__el">
                            <span class="ico-chart"></span> Labège
                        </a>
                        <a href="#" title="" class="tags__el">
                            <span class="ico-tag"></span> B2B
                        </a>
                        <a href="#" title="" class="tags__el">
                            <span class="ico-tag"></span> M2M
                        </a>
                    </div>
                    <div class="searchFilters__tableOpt">
                        <ul>
                            <li class="columns">
                                <a href="#" title="#" class="btn-searchcolumns"><span class="ico-columns"></span> Voir plus de colonnes</a>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div class="table-search-wrapper">
                    <table class="table table-search">
                        <thead>
                            <tr>
                                <?php foreach ($searchColumns as $col) { ?>
                                    <th data-column="<?= $col['id']; ?>" class="<?= $col['class']; ?>" data-sortable="<?= $col['sortable']; ?>"><?= $col['name']; ?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th class="table-search__startup">
                                    <div class="startupInfos">
                                        <div class="startupInfos__image">
                                            <img src="https://www.myfrenchstartup.com/logo/shift.jpg" alt="">
                                        </div>
                                        <div class="startupInfos__desc">
                                            <div class="companyName">SHIFT TECHNOLOGY</div>
                                            <div class="companyLabel">La détection de la fraude à l'assurance</div>
                                        </div>
                                    </div>
                                </th>
                                <td class="table-search__marche">
                                    <div class="table-search__link"><a href="">Transport & logistique</a></div>
                                    <div class="table-search__link small"><a href="">Voiture</a></div>
                                    <div class="table-search__link small"><a href="">C2C</a></div>
                                </td>
                                <td class="table-search__maturite center"><span class="ico-raise"></span> 40</td>
                                <td class="table-search__totalfondsleves center">200</td>
                                <td class="table-search__region center">
                                    <div class="table-search__link small"><a href="">Île-de-france</a></div>
                                </td>
                                <td class="table-search__traction center"><span class="ico-decrease"></span> 0,9%</td>
                                <td class="table-search__ville center">Paris</td>
                                <td class="table-search__date center">2010</td>
                                <td class="table-search__position center">Active</td>
                                <td class="table-search__nblevees center">2</td>
                                <td class="table-search__totaldernierelevee center">2k</td>
                                <td class="table-search__datedernierelevee center">Juil. 2021</td>
                                <td class="table-search__datedernieremodificationcapital center">Juil. 2021</td>
                                <td class="table-search__effectif center"><span class="ico-raise"></span> 30</td>
                                <td class="table-search__competitionsecteur center"><span class="ico-raise"></span> 6,4/10</td>
                                <td class="table-search__modeleeconomique center">B2B</td>
                                <td class="table-search__capaciteadelivrer center"><span class="ico-raise"></span> 6,4/10</td>
                                <td class="table-search__derniereactualite center">4 sem.</td>
                                <td class="table-search__tags">
                                    <div class="table-search__link"><a href="">covoiturage</a></div>
                                    <div class="table-search__link"><a href="">plateformes collaboratives</a></div>
                                    <div class="table-search__link"><a href="">autopartage</a>&nbsp;...</div>
                                </td>
                            </tr>
    
                            <tr>
                                <th class="table-search__startup">
                                    <div class="startupInfos">
                                        <div class="startupInfos__image">
                                            <img src="https://www.myfrenchstartup.com/logo/533bbad43c2a2_DEEZER.jpg" alt="">
                                        </div>
                                        <div class="startupInfos__desc">
                                            <div class="companyName">Deezer</div>
                                            <div class="companyLabel">Ecouter de la musique à la demande</div>
                                        </div>
                                    </div>
                                </th>
                                <td class="table-search__marche">
                                    <div class="table-search__link"><a href="">Art, culture, loisirs, sport</a></div>
                                    <div class="table-search__link small"><a href="">Musique</a></div>
                                    <div class="table-search__link small"><a href="">B2C</a></div>
                                </td>
                                <td class="table-search__maturite center"><span class="ico-raise"></span> 30</td>
                                <td class="table-search__totalfondsleves center">100</td>
                                <td class="table-search__region center">
                                    <div class="table-search__link small"><a href="">Provence-Alpes-Côte d'Azur</a></div>
                                </td>
                                <td class="table-search__traction center"><span class="ico-raise"></span> 22,3%</td>
                                <td class="table-search__ville center">Paris</td>
                                <td class="table-search__date center">2010</td>
                                <td class="table-search__position center">Active</td>
                                <td class="table-search__nblevees center">2</td>
                                <td class="table-search__totaldernierelevee center">4k</td>
                                <td class="table-search__datedernierelevee center">Aoû. 2021</td>
                                <td class="table-search__datedernieremodificationcapital center">Juil. 2021</td>
                                <td class="table-search__effectif center"><span class="ico-raise"></span> 42</td>
                                <td class="table-search__competitionsecteur center"><span class="ico-raise"></span> 3,4/10</td>
                                <td class="table-search__modeleeconomique center">B2B</td>
                                <td class="table-search__capaciteadelivrer center"><span class="ico-raise"></span> 2,8/10</td>
                                <td class="table-search__derniereactualite center">1 sem.</td>
                                <td class="table-search__tags">
                                    <div class="table-search__link"><a href="">covoiturage</a></div>
                                    <div class="table-search__link"><a href="">plateformes collaboratives</a></div>
                                    <div class="table-search__link"><a href="">autopartage</a>&nbsp;...</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="search-actions">
                    <a href="" class="btn btn-sm btn-primary btn-alarm btn-searchAlert">Créer une alerte</a>
                    <a href="" class="btn btn-sm btn-saveSearch btn-primary btn-bookmark">Enregistrer la recherche</a>
                </div>
                <ul class="pagination pagination--center">
                    <li class="prev"><a href="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="10.278" height="18.556" viewBox="0 0 10.278 18.556"><path d="M16.864,6,9,13.864l7.864,7.864" transform="translate(-8 -4.586)" fill="none" stroke="#0cf" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></svg></a></li>
                    <li class="active"><a href="" title="">1</a></li>
                    <li><a href="" title="">2</a></li>
                    <li><a href="" title="">3</a></li>
                    <li class="next"><a href="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="9.995" height="17.991" viewBox="0 0 9.995 17.991"><path d="M9,6l7.581,7.581L9,21.162" transform="translate(-7.586 -4.586)" fill="none" stroke="#0cf" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></svg></a></li>
                </ul>
            </div>
        </div>

        <script type="module">
            // https://github.com/fiduswriter/Simple-DataTables/wiki/columns
            import {DataTable} from "<?=JS_PATH;?>tablefilter.min.js"
            const table = new DataTable("table", {
                "searchable": false,
                "nextPrev": false,
                "paging": false,
                //"fixedColumns": false,
                
            });
                    
            let columns = table.columns();
           
            //https://github.com/fiduswriter/Simple-DataTables/wiki/columns()
            columns.hide([7,8,9,10,11,12,13,14,15,16,17,18,19]);

            //var visibleColumns = columns.visible();
            let columnsLinks = document.querySelectorAll('.columnsFilter [data-columnId]');
            if (columnsLinks !== null) {
                columnsLinks.forEach(e => {
                    e.addEventListener('click', function() {
                        if (this.classList.contains('seeAll')) {
                            columns.show([7,8,9,10,11,12,13,14,15,16,17,18,19]);
                        } else {
                            let column = parseInt(this.getAttribute("data-columnId"));
                            columns.show([column]);
                        }
                    })
                });
            }
        </script>
    </div>
</div>

<?php include ('layout/footer.php'); ?>

<script async src="<?=JS_PATH;?>flickity.min.js?<?=time(); ?>"></script>
<script async src="<?=JS_PATH;?>app.min.js?<?=time(); ?>"></script>

<noscript>
    <script src="<?=JS_PATH;?>app.min.js?<?=time(); ?>"></script>
    <script src="<?=JS_PATH;?>flickity.min.js?<?=time(); ?>"></script>
</noscript>

<script async="" src="//www.google-analytics.com/analytics.js"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-36251023-1', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>