<?php
//$base = dirname(dirname(__FILE__));
include("include/db.php");
//On déclare la fonction Php :


/*  Nous allons générer notre fichier XML d'un seul coup. Pour cela, nous allons stocker tout notre
  fichier dans une variable php : $xml.
  On commence par déclarer le fichier XML puis la version du flux RSS 2.0.
  Puis, on ajoute les éléments d'information sur le channel. Notez que nous avons volontairement
  omit quelques balises :
 */

$xml = '<?xml version="1.0" encoding="UTF-8"?>';
$xml .= '<rss version="2.0">';
$xml .= '<channel>';
$xml .= ' <title>Selection News Startups : MyFrenchStartup RSS</title>';
$xml .= ' <link>https://www.myfrenchstartup.com/en/french-startups-news</link>';
$xml .= ' <description>MyFrenchStartup a sélectionné pour vous les dernières news Startups en France. </description>';
$xml .= ' <language>fr</language>';
$xml .= ' <copyright>MyFrenchStartup</copyright>';
$xml .= ' <category>Startup France News</category>';
$sql = mysqli_query($link,"select * from cron_news where  dt='" . date("Y-m-d") . "' order by dt,RAND() desc ")or die(mysqli_error($link));
while ($donnees = mysqli_fetch_array($sql)) {
    $xml .= '<item>';
    $xml .= '<title>' . (html_entity_decode(stripcslashes(addslashes($donnees['titre'])))) . '</title>';
    $xml .= '<link>' . addslashes($donnees['lien']) . '</link>';
    $xml .= '<guid isPermaLink="true">' . addslashes($donnees['lien']) . '</guid>';
    $xml .= '<pubDate>' . (date("D, d M Y H:i:s O", strtotime($donnees['dt']))) . '</pubDate>';
    $xml .= '<description>' . (html_entity_decode(stripcslashes(addslashes($donnees['description'])))) . '</description>';
    $xml .= '<source>' . (html_entity_decode(stripcslashes(addslashes($donnees['source'])))) . '</source>';
    $xml .= '</item>';
}
$xml .= '</channel>';
$xml .= '</rss>';
$xml=preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $xml);
$fp = fopen("flux-rss.xml", 'w+');
fputs($fp, $xml);
fclose($fp);
?>