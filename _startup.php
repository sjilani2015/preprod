<?php 
    include ('config.php'); 
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
<head>
    <?php include ('metaheaders.php'); ?>
    <title><?=SITENAME;?></title>
    <meta name="description" content="<?=METADESC;?>">

    <!-- AM Charts components -->
    <script src="<?=JS_PATH;?>amcharts/core.min.js"></script>
    <script src="<?=JS_PATH;?>amcharts/charts.min.js"></script>

    <script>
        const   themeColorBlue       = am4core.color('#00bff0'),
                themeColorFill       = am4core.color('#E1F2FA'),
                themeColorGrey       = am4core.color('#dadada'),
                themeColorLightGrey  = am4core.color('#F5F5F5'),
                themeColorDarkgrey   = am4core.color("#33333A"),
                themeColorLine       = am4core.color("#87D6E3"),
                themeColorLineRed    = am4core.color("#FF7C7C"),
                themeColorLineGreen  = am4core.color("#21D59B"),
                themeColorColumns    = am4core.color("#E1F2FA"),
                labelColor           = am4core.color('#798a9a');
    </script>
</head>

<?php 
/*
    sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
 */
 ?>
<body class="preload page">
<div id="mainmenu" class="mainmenu">
    <div class="mainmenu__wrapper"></div>
</div>
<div class="page-wrapper">
    <?php include ('layout/header-connected.php'); ?>
    <div class="page-content" id="page-content">
        <div class="container">

            <div class="banner-partner">
                <a href="https://www.myfrenchstartup.com/channel/" target="_blank"> <img src="https://www.myfrenchstartup.com/images/banner.png" alt=""></a>
            </div>
            
            <section class="module">
                <div class="module__title">ALAN</div>
                <div class="tags">
                    <a href="#" title="" class="tags__el tags__el--pinky">
                        <span class="ico-suitcase"></span> Électronique & Composants
                    </a>
                    <a href="#" title="" class="tags__el tags__el--darkblue">
                        <span class="ico-chart"></span> Étude
                    </a>
                    <a href="#" title="" class="tags__el tags__el--pinky">
                        <span class="ico-pin"></span> Occitanie
                    </a>
                    <a href="#" title="" class="tags__el">
                        <span class="ico-tag"></span> Labège
                    </a>
                    <a href="#" title="" class="tags__el">
                        <span class="ico-tag"></span> B2B
                    </a>
                    <a href="#" title="" class="tags__el">
                        <span class="ico-tag"></span> M2M
                    </a>
                </div>
                
                <div class="ctg ctg--1_3">
                    <aside class="bloc text-center">
                        <div class="companyLogo">
                            <img src="https://www.myfrenchstartup.com/logo/alan.png" alt="Alan" />
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Total des fonds levés</div>
                            <div class="datasbloc__val">310 M€</div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Effectifs</div>
                            <div class="datasbloc__val">> 100</div>
                        </div>
                        <ul class="companyInfos">
                            <li>Création : 2016</li>
                            <li>Capital : 972,3 K€</li>
                            <li>6 fondateurs</li>
                        </ul>
                        <div class="bloc-actions bloc-actions--darkgrey">
                            <div class="bloc-actions__ico">
                                <a href="#" title="">
                                    <span class="icon-wrapper">
                                        <span class="ico-growup"></span>
                                    </span>
                                    Revendiquer cette startup
                                </a>
                            </div>
                        </div>
                    </aside>
                    
                    <div class="bloc">
                        <div class="ctg ctg--2_2 ctg--fullheight">
                            <div class="startupIntro">
                                <h2>L'assurance santé simple</h2>
                                <p class="intro-short">Alan est une assurance complémentaire santé agréée par la Banque de France - ACPR. Nous sommes les premiers à avoir cet agrément depuis 1986 ! Avec nous, vous êtes en ... <span class="intro-seeMore">voir plus</span></p>
                                <p class="intro-long">Alan est une assurance complémentaire santé agréée par la Banque de France - ACPR. Nous sommes les premiers à avoir cet agrément depuis 1986 ! Avec nous, vous êtes en lien direct avec l'entreprise qui vous couvre; nous ne sommes pas un intermédiaire ou un courtier. Cela nous donne la latitude de créer le produit qui nous semble le meilleur pour vous, de concevoir une expérience utilisateur incroyable qu'aucun autre assureur ne pourra apporter. <span class="intro-seeLess">voir moins</span></p>
                                
                            </div>
                            <div class="chart chart--radar">
                                <div id="chart-radar"></div>
                            </div>
                            <script>
                                am4core.ready(function() {


                                    /* Create chart instance */
                                    var chart = am4core.create("chart-radar", am4charts.RadarChart);

                                    /* Add data */
                                    chart.data = [{
                                        "date": "A",
                                        "value": 5,
                                        "value2": 7
                                    }, {
                                        "date": "B",
                                        "value": 10,
                                        "value2": 4
                                    }, {
                                        "date": "C",
                                        "value": 8,
                                        "value2": 3
                                    }, {
                                        "date": "D",
                                        "value": 7,
                                        "value2": 10
                                    }, {
                                        "date": "E",
                                        "value": 1,
                                        "value2": 4
                                    }, {
                                        "date": "F",
                                        "value": 4,
                                        "value2": 7
                                    }];

                                    chart.paddingTop = 0;
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;

                                    /* Create axes */
                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "date";
                                    categoryAxis.renderer.ticks.template.strokeWidth = 2;
                                    categoryAxis.renderer.labels.template.fontSize = 11;
                                    categoryAxis.renderer.labels.template.fill = labelColor;

                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
                                    valueAxis.renderer.axisFills.template.fillOpacity = 0.05;
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.gridType = "polygons"
                                    

                                    /* Create and configure series */
                                    function createSeries(field, name, themeColor) {
                                        var series = chart.series.push(new am4charts.RadarSeries());
                                        series.dataFields.valueY = field;
                                        series.dataFields.categoryX = "date";
                                        series.strokeWidth = 2;
                                        series.fill = themeColor;
                                        series.fillOpacity = 0.3;
                                        series.stroke = themeColor;
                                        series.name = name;

                                        // Show bullets ?
                                        //let circleBullet = series.bullets.push(new am4charts.CircleBullet());
                                        //circleBullet.circle.stroke = am4core.color('#fff');
                                        //circleBullet.circle.strokeWidth = 2;
                                    }


                                    createSeries("value", "Secteur", themeColorBlue);
                                    createSeries("value2", "France", themeColorGrey);

                                    chart.legend = new am4charts.Legend();
                                    chart.legend.position = "top";
                                    chart.legend.useDefaultMarker = true;
                                    chart.legend.fontSize = "11";
                                    chart.legend.color = themeColorGrey;
                                    chart.legend.labels.template.fill = labelColor;

                                    chart.legend.labels.template.textDecoration = "none";
                                    chart.legend.valueLabels.template.textDecoration = "none";

                                    let as = chart.legend.labels.template.states.getKey("active");
                                    as.properties.textDecoration = "line-through";
                                    as.properties.fill = themeColorDarkgrey;

                                    let marker = chart.legend.markers.template.children.getIndex(0);
                                    marker.cornerRadius(12, 12, 12, 12);
                                    marker.width = 20;
                                    marker.height = 20;
                                });
                            </script>
                            <div class="datagrid">
                                <div class="datagrid__bloc">
                                    <div class="datagrid__key">
                                        <span class="datagrid__key__label">A :</span> Compétition secteur
                                    </div>
                                    <div class="datagrid__val">4,8 <span class="ico-decrease"></span></div>
                                    <div class="datagrid__subtitle">% France 5.1</div>
                                </div>
                                <div class="datagrid__bloc">
                                    <div class="datagrid__key">
                                        <span class="datagrid__key__label">B :</span> Maturité
                                    </div>
                                    <div class="datagrid__val">8,3 <span class="ico-decrease"></span></div>
                                    <div class="datagrid__subtitle">% France 5.1</div>
                                </div>
                                <div class="datagrid__bloc">
                                    <div class="datagrid__key">
                                        <span class="datagrid__key__label">C :</span> Capacité à délivrer
                                    </div>
                                    <div class="datagrid__val">4,8 <span class="ico-decrease"></span></div>
                                    <div class="datagrid__subtitle">% France 5.1</div>
                                </div>
                                <div class="datagrid__bloc">
                                    <div class="datagrid__key">
                                        <span class="datagrid__key__label">D :</span> Nombre de levées
                                    </div>
                                    <div class="datagrid__val">8,3</div>
                                    <div class="datagrid__subtitle">% France 5.1</div>
                                </div>
                                <div class="datagrid__bloc">
                                    <div class="datagrid__key">
                                        <span class="datagrid__key__label">E :</span> Ancienneté
                                    </div>
                                    <div class="datagrid__val">4,8</div>
                                    <div class="datagrid__subtitle">% France 5.1</div>
                                </div>
                                <div class="datagrid__bloc">
                                    <div class="datagrid__key">
                                        <span class="datagrid__key__label">F :</span> Traction LinkedIn
                                    </div>
                                    <div class="datagrid__val">8,3 <span class="ico-decrease"></span></div>
                                    <div class="datagrid__subtitle">% France 5.1</div>
                                </div>
                            </div>
                            
                            <div class="bloc-actions">
                                <div class="bloc-actions__ico">
                                    <a href="#" title="">
                                        <span class="icon-wrapper">
                                            <span class="ico-bookmark"></span>
                                        </span>
                                        Mémoriser
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="module module--finances">
                <div class="module__title">Finances : capitaux et levées de fonds</div>
                <div class="bloc">
                    <div class="row">
                        <div class="col datasbloc text-center">
                            <div class="datasbloc__key">Montant global levé</div>
                            <div class="datasbloc__val">310 M€</div>
                            <div class="datasbloc__subtitle">+3200% /moy. secteur</div>
                        </div>
                        <div class="col datasbloc text-center">
                            <div class="datasbloc__key">Nombre de levées</div>
                            <div class="datasbloc__val">5</div>
                            <div class="datasbloc__subtitle">14 VC</div>
                        </div>
                        <div class="col datasbloc text-center">
                            <div class="datasbloc__key">Fréquence</div>
                            <div class="datasbloc__val">4,2 mois</div>
                            <div class="datasbloc__subtitle" style="white-space: nowrap;">Délai moyen entre 2 levées</div>
                        </div>
                        <div class="col datasbloc text-center">
                            <div class="datasbloc__key"style="white-space: nowrap;">Probabilité de future levée</div>
                            <div class="datasbloc__val">50%</div>
                            <div class="datasbloc__subtitle">à 6 mois</div>
                        </div>
                        <div class="col datasbloc text-center">
                            <div class="datasbloc__key">Capital social</div>
                            <div class="datasbloc__val">930 K€</div>
                            <div class="datasbloc__subtitle">25 évolutions</div>
                        </div>
                        <div class="col datasbloc text-center">
                            <div class="datasbloc__key">CA</div>
                            <div class="datasbloc__val">-</div>
                            <div class="datasbloc__subtitle"></div>
                        </div>
                    </div>
                    
                    <br />
                    
                    <div class="bloc__title">Détails des levées de fonds de Alan</div>
                    <div class="tablebloc">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center">Montant</th>
                                    <th class="text-center">Date</th>
                                    <th>Investisseurs</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center moneyCell">337 M€</td>
                                    <td class="text-center dateCell">Juil. 2021</td>
                                    <td class="">
                                        <a href="#" class="label">Index ventures</a>
                                        <a href="#" class="label">Coatue management</a>
                                        <a href="#" class="label">temasek</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center moneyCell">50 M€</td>
                                    <td class="text-center dateCell">Avr. 2021</td>
                                    <td class="">
                                        <a href="#" class="label">investisseurs privés</a>
                                        <a href="#" class="label">dst global</a>
                                        <a href="#" class="label">temasek</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center moneyCell">337 M€</td>
                                    <td class="text-center dateCell">Juil. 2021</td>
                                    <td class="">
                                        <a href="#" class="label">Index ventures</a>
                                        <a href="#" class="label">Coatue management</a>
                                        <a href="#" class="label">temasek</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center moneyCell">337 M€</td>
                                    <td class="text-center dateCell">Juil. 2021</td>
                                    <td class="">
                                        <a href="#" class="label">Index ventures</a>
                                        <a href="#" class="label">Coatue management</a>
                                        <a href="#" class="label">temasek</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>

            <section class="module">
                <div class="module__title">Marché & secteur</div>
                <div class="ctg ctg--1_2_1">
                    <div class="bloc text-center">
                        <!--<div class="bloc__title">Compétition secteur</div>-->
                        <div class="datasbloc">
                            <div class="datasbloc__key">Compétition secteur <a href="#" class="datasbloc__key__ico"><span class="ico-chart"></span></a></div>
                            <div class="datasbloc__val">1,3 <span class="ico-raise"></span></div>
                            <div class="datasbloc__subtitle">Moy. France 3,1</div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Création secteur</div>
                            <div class="datasbloc__val">5,8% <span class="ico-raise"></span></div>
                            <div class="datasbloc__subtitle">Moy. France 3,1</div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Code Naf</div>
                            <div class="datasbloc__val">6201Z</div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Modèle</div>
                            <div class="datasbloc__val">B2B, B2C</div>
                        </div>
                    </div>
                    <div class="bloc">
                        <div class="bloc__title">Maturité des startups du secteur</div>
                        <div class="chart chart--bubbles">
                            <div id="chart-bubbles-startups"></div>
                        </div>
                        <script>
                            am4core.ready(function() {


                                var chart = am4core.create("chart-bubbles-startups", am4charts.XYChart);
                                chart.paddingBottom = 0;
                                chart.paddingLeft = 0;
                                chart.paddingRight = 0;

                                chart.numberFormatter.numberFormat = "#.";

                                
                                var valueAxisX = chart.xAxes.push(new am4charts.ValueAxis());
                                valueAxisX.renderer.ticks.template.disabled = true;
                                valueAxisX.renderer.axisFills.template.disabled = true;
                                valueAxisX.renderer.fontSize = 11;
                                valueAxisX.renderer.color = labelColor;
                                valueAxisX.renderer.labels.template.fill = labelColor;


                                // Title left
                                valueAxisX.title.text = "Maturité";
                                valueAxisX.title.align = "right";
                                //valueAxisX.title.valign = "top";
                                valueAxisX.title.dy = 0;
                                valueAxisX.title.fontSize = 11;
                                valueAxisX.title.fill = labelColor;
                                
                                var valueAxisY = chart.yAxes.push(new am4charts.ValueAxis());
                                valueAxisY.renderer.ticks.template.disabled = true;
                                valueAxisY.renderer.axisFills.template.disabled = true;

                                // Title left
                                valueAxisY.title.text = "Années";
                                valueAxisY.title.rotation = -90;
                                valueAxisY.title.align = "left";
                                valueAxisY.title.valign = "top";
                                valueAxisY.title.dy = 0;
                                valueAxisY.title.fontSize = 11;
                                valueAxisY.title.fill = labelColor;

                                valueAxisY.renderer.fontSize = 11;
                                valueAxisY.renderer.color = labelColor;
                                valueAxisY.renderer.labels.template.fill = labelColor;
                                
                                var series = chart.series.push(new am4charts.LineSeries());
                                series.dataFields.valueX = "x";
                                series.dataFields.valueY = "y";
                                series.dataFields.value = "value";
                                series.strokeOpacity = 0;
                                series.sequencedInterpolation = true;
                                series.tooltip.pointerOrientation = "vertical";

                                var bullet = series.bullets.push(new am4core.Circle());
                                bullet.fill = themeColorLightGrey;
                                bullet.propertyFields.fill = "color";
                                bullet.strokeOpacity = 0;
                                bullet.strokeWidth = 2;
                                bullet.fillOpacity = 0.5;
                                bullet.stroke = am4core.color("#ffffff");
                                bullet.hiddenState.properties.opacity = 0;
                                bullet.tooltipText = "[bold]{title}[/]\nAnnée: {valueX.value}\nLevée : {valueY.value}";

                                var outline = chart.plotContainer.createChild(am4core.Circle);
                                outline.fillOpacity = 0;
                                outline.strokeOpacity = 0.8;
                                outline.stroke = themeColorBlue;
                                outline.strokeWidth = 2;
                                outline.hide(0);

                                var blurFilter = new am4core.BlurFilter();
                                outline.filters.push(blurFilter);

                                bullet.events.on("over", function(event) {
                                    var target = event.target;
                                    outline.radius = target.pixelRadius + 2;
                                    outline.x = target.pixelX;
                                    outline.y = target.pixelY;
                                    outline.show();
                                })

                                bullet.events.on("out", function(event) {
                                    outline.hide();
                                })

                                var hoverState = bullet.states.create("hover");
                                hoverState.properties.fillOpacity = 1;
                                hoverState.properties.strokeOpacity = 1;

                                series.heatRules.push({ target: bullet, min: 2, max: 60, property: "radius" });

                                bullet.adapter.add("tooltipY", function (tooltipY, target) {
                                    return -target.radius;
                                })

                                chart.data = [
                                    {
                                        "title": "EDF",
                                        "color": themeColorGrey,
                                        "x": "2015",
                                        "y": 1,
                                        "value": 60
                                    },
                                    {
                                        "title": "La poste",
                                        "color": themeColorGrey,
                                        "x": "2016",
                                        "y": 2,
                                        "value": 60
                                    },
                                    {
                                        "title": "GDF",
                                        "color": themeColorGrey,
                                        "x": 2017,
                                        "y": 5,
                                        "value": 60
                                    },
                                    {
                                        "title": "Zoomalia",
                                        "color": themeColorGrey,
                                        "continent": "africa",
                                        "x": 2018,
                                        "y": 7,
                                        "value": 60
                                    },
                                    {
                                        "title": "IT Facto",
                                        "id": "AT",
                                        "color": themeColorGrey,
                                        "x": 2018,
                                        "y": 9,
                                        "value": 60
                                    },
                                    {
                                        "title": "Orange",
                                        "color": themeColorGrey,
                                        "x": 2019,
                                        "y": 10,
                                        "value": 60
                                    },
                                    {
                                        "title": "SFR",
                                        "color": themeColorGrey,
                                        "x": 2020,
                                        "y": 12,
                                        "value": 60
                                    },
                                    {
                                        "title": "Alan",
                                        "color": themeColorBlue,
                                        "x": "2021",
                                        "y": 11,
                                        "value": 60
                                    },
                                    
                                ];
                            });
                        </script>
                    </div>
                    <div class="bloc">
                        <div class="bloc__title">Startups à suivre</div>
                        <div class="tablebloc">
                            <table class="table table--condensed">
                                <thead>
                                <tr>
                                    <th>Startup</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <a href="" title="" class="companyNameCell"><span>Lafinbox</span></a>
                                    </td>
                                    <td>Nov. 2015</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="" title="" class="companyNameCell"><span>Nousassurons.co</span></a>
                                    </td>
                                    <td>Nov. 2015</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="" title="" class="companyNameCell"><span>Otherwise</span></a>
                                    </td>
                                    <td>Oct. 2021</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="" title="" class="companyNameCell"><span>manyformoney</span></a>
                                    </td>
                                    <td>Sept. 2013</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            
            <section class="module module--fondateurs">
                <div class="module__title">Fondateurs & team</div>

                <div class="ctg ctg--3_1">
                    <div class="bloc">
                        <div class="bloc__title">Fondateurs</div>
                        <div class="tablebloc tablebloc--light">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center">Fondateur</th>
                                    <th class="text-center">Âge</th>
                                    <th class="text-center">École</th>
                                    <th class="text-center">Diplôme</th>
                                    <th class="text-center" width="40%">Compétences</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center">
                                        <div class="avatar">
                                            <i class="ico-avatar"></i>
                                        </div>
                                        <strong>Jean-Charles Samuelian</strong>
                                        <div class="fonction"><?php echo strtoupper('Ceo'); ?></div>
                                    </td>
                                    <td class="text-center">35</td>
                                    <td class="text-center">Institut des Actuaires</td>
                                    <td class="text-center">Member</td>
                                    <td class="text-center">Business Strategy, Project Management, Management, Strategy, Business Development,</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="historyList">
                            <div class="historyList__item">
                                <div class="historyList__item__date">2016 - Aujourd'hui</div>
                                <div class="historyList__item__infos">Co-founder & CEO chez Alan</div>
                            </div>
                            <div class="historyList__item">
                                <div class="historyList__item__date">2010 - 2015</div>
                                <div class="historyList__item__infos">Co-founder chez Expliseat</div>
                            </div>
                        </div>
                    </div>
                    <div class="bloc">
                        <div class="bloc__title">Fondateurs & Team</div>
                        <div class="bloc-team">
                            <div class="bloc-team__people">
                                <div class="bloc-team__people__avatar">
                                    <div class="avatar">
                                        <i class="ico-avatar-white"></i>
                                    </div>
                                </div>
                                <div class="bloc-team__people__name">
                                    Jean-Charles  Samuelian
                                    <span class="fonction">CEO</span>
                                </div>
                                <div class="bloc-team__people__link">
                                    <a href="" title=""><i class="ico-linkedin"></i></a>
                                </div>
                            </div>
                            <div class="bloc-team__people">
                                <div class="bloc-team__people__avatar">
                                    <div class="avatar">
                                        <i class="ico-avatar-white"></i>
                                    </div>
                                </div>
                                <div class="bloc-team__people__name">
                                    Charles Gorintin
                                    <span class="fonction">CEO</span>
                                </div>
                                <div class="bloc-team__people__link">
                                    <a href="" title=""><i class="ico-linkedin"></i></a>
                                </div>
                            </div>
                            <div class="bloc-team__people">
                                <div class="bloc-team__people__avatar">
                                    <div class="avatar">
                                        <i class="ico-avatar-white"></i>
                                    </div>
                                </div>
                                <div class="bloc-team__people__name">
                                    STAAD Fabrice
                                    <span class="fonction">CEO</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <section class="module module--fondateurs">
                <div class="module__title">Localisation</div>
                <div class="ctg ctg--2_2">
                    <div class="bloc jc text-center">
                        <div class="datasbloc">
                            <div class="datasbloc__val">
                                <div class="tags">
                                    <a href="#" title="" class="tags__el tags__el--darkblue">
                                        <span class="ico-chart"></span> Étude
                                    </a>
                                    <a href="#" title="" class="tags__el tags__el--pinky">
                                        <span class="ico-pin"></span> île-de-france
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Adresse</div>
                            <div class="datasbloc__val small">
                                30 Rue des Boulangers, 75005 Paris
                            </div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Tél</div>
                            <div class="datasbloc__val small">
                                06 61 31 56 55
                            </div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Website</div>
                            <div class="datasbloc__val small">
                                <a href="">Alan.eu</a>
                            </div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Email</div>
                            <div class="datasbloc__val small">
                                <a href="mailto:john@doe.com">john@doe.com</a> <br/>
                                <a href="" class="link-not-connected">Connectez-vous</a>
                            </div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Réseaux sociaux</div>
                            <div class="datasbloc__val small">
                                <ul class="list list--socialnetworks">
                                    <li><a href="" title="LinkedIn"><span class="ico-linkedin"></span></a></li>
                                    <li><a href="" title="Facebook"><span class="ico-facebook"></span></a></li>
                                    <li><a href="" title="Twitter"><span class="ico-twitter"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="bloc">
                        <div class="bloc__title">Startups du secteur par département <a href="">Voir la liste &raquo;</a></div>
                        <div class="tablebloc">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th class="iconCell">Évol.</th>
                                    <th>Départements</th>
                                    <th class="text-center">Startups</th>
                                </tr>
                                </tbody>
                                <tbody>
                                <tr>
                                    <td><span class="ico-stable"></span></td>
                                    <td>Paris</td>
                                    <td class="text-center">1053</td>
                                </tr>
                                <tr>
                                    <td><span class="ico-raise"></span></td>
                                    <td>Hauts-de-Seine</td>
                                    <td class="text-center">332</td>
                                </tr>
                                <tr>
                                    <td><span class="ico-decrease"></span></td>
                                    <td>Yvelines</td>
                                    <td class="text-center">75</td>
                                </tr>
                                <tr>
                                    <td><span class="ico-stable"></span></td>
                                    <td>Val-de-Marne</td>
                                    <td class="text-center">68</td>
                                </tr>
                                <tr>
                                    <td><span class="ico-stable"></span></td>
                                    <td>Seine-Saint-Denis</td>
                                    <td class="text-center">63</td>
                                </tr>
                                <tr>
                                    <td><span class="ico-raise"></span></td>
                                    <td>Essonne</td>
                                    <td class="text-center">62</td>
                                </tr>
                                <tr>
                                    <td><span class="ico-decrease"></span></td>
                                    <td>Seine-et-Marne</td>
                                    <td class="text-center">25</td>
                                </tr>
                                <tr>
                                    <td><span class="ico-decrease"></span></td>
                                    <td>Val-d'Oise</td>
                                    <td class="text-center">21</td>
                                </tr>
                                <tr>
                                    <td><span class="ico-decrease"></span></td>
                                    <td>NC</td>
                                    <td class="text-center">1</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <section class="module module--legals">
                <div class="module__title">Legals & tech stack</div>
                <div class="ctg ctg--1_3">
                    <div class="bloc text-center">
                        <div class="bloc__title text-center">Légales</div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Raison sociale</div>
                            <div class="datasbloc__val small">ALAN</div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Siret</div>
                            <div class="datasbloc__val small">81835307000019</div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Forme juridique</div>
                            <div class="datasbloc__val small">SA à conseil d'administration</div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Code Naf</div>
                            <div class="datasbloc__val small">6201Z</div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Dernier mouvement Bodacc</div>
                            <div class="datasbloc__val small">Augmentation du capital social 10/09/2021</div>
                        </div>
                    </div>
                    
                    <div class="bloc text-center">
                        <div class="bloc__title text-center">Tech Stack</div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Analytics and Tracking</div>
                            <div class="datasbloc__val small">BuzzBuilder</div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Frameworks</div>
                            <div class="datasbloc__val small">FotoGraphy , Distincta</div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Audio Video Media</div>
                            <div class="datasbloc__val small">Goom player</div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Content Management System</div>
                            <div class="datasbloc__val small">GlobalBluePoint , RedX Web Design</div>
                        </div>
                        <div class="datasbloc">
                            <div class="datasbloc__key">Web Hosting Providers</div>
                            <div class="datasbloc__val small">Tierpoint , MidPhase</div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="module module--companyinfos">
                <div class="bloc">
                    ALAN est une startup de la région Paris - Ile De France, basée à Paris – France. Créée en 2016 sous le nom ALAN , ALAN est une startup du secteur Finance - Banque, Assurance
                    Les fondateurs de ALAN sont Jean-Charles Samuelian et Charles Gorintin et STAAD Fabrice et CHAN Connie et SARKOZY DE NAGY BOCSA Guillaume et HAMMER Jan
                    La startup ALAN a réalisé une levée des fonds le 20 avril 2021.
                </div>
            </section>
        </div>
    </div>
</div>

<?php include ('layout/footer.php'); ?>

<script async src="<?=JS_PATH;?>flickity.min.js?<?=time(); ?>"></script>
<script async src="<?=JS_PATH;?>app.min.js?<?=time(); ?>"></script>

<noscript>
    <script src="<?=JS_PATH;?>app.min.js?<?=time(); ?>"></script>
    <script src="<?=JS_PATH;?>flickity.min.js?<?=time(); ?>"></script>
</noscript>

<script async="" src="//www.google-analytics.com/analytics.js"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-36251023-1', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>