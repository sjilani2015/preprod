<div class="searchFilters__tableOpt">
    <ul>
        <li class="columns">
            <a href="#" title="#" class="btn-searchcolumns"><span class="ico-columns"></span> Voir plus de colonnes</a>
        </li>
    </ul>
</div>

<script>
    /**
     * Open filters for column modal
     */
    let searchColumnsBtn = document.querySelector('.btn-searchcolumns'),
        modalSearchColumns = document.getElementById('modal-columns');

    if (modalSearchColumns !== null && searchColumnsBtn !== null) {
        searchColumnsBtn.addEventListener('click', function () {
            document.body.classList.add ("has-door");
            modalSearchColumns.classList.add ("shown");
        });
    }
</script>