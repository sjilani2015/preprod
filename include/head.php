<?php
require_once("include/db.php");
require_once("include/util.php");

$langage = stripos($_SERVER['SCRIPT_URI'], '/en/');

if ($langage !== false)
	$lang="en";
else 
	$lang="fr";	
	
require_once("lang/" . getLanguage($lang) . ".php");

require_once("include/query.php");
?>