<?php
session_start();
header('Content-type: text/html; charset=utf-8');
ini_set('session.use_trans_sid', 0);
ini_set('session.use_only_cookies', 1);

//error_reporting(0);
Header("Cache-Control: must-revalidate");

$offset = 60 * 60 * 24 * 3;
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
Header($ExpStr);


$dbroot = 'localhost';
$dbuser = 'myfrenchuser';
$dbpass = 'My#2fsDb3';
$dbname = 'myfrenchdata';

$link = mysqli_connect($dbroot, $dbuser, $dbpass) or die('<h1>Impossible de trouver le serveur</h1>');
mysqli_select_db($link, $dbname) or die('<h1>Impossible de trouver la base de données</h1>');
mysqli_set_charset($link, "utf8mb4");

define("url", "https://www.myfrenchstartup.com/");

function nottoie($ch) {
    $cc = trim(str_replace(array(" ", "-", "é", "è", "ê","à","&",',',"Î","'","ô"), array("_", "__", "e", "e", "e","a","..","","i","","o"), trim(utf8_encode($ch))));
    return $cc;
}

?>