<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');


if (isset($_SESSION['data_login'])) {
    $users = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));
    $ma_liste_colonne = explode(",", $users['colonne']);
} else {
    $ma_liste_colonne = array("6,7,8,9,10,11,12,13,14,15,16,17,18");
}

if (isset($_SESSION['data_login'])) {
    if ($users['premium'] == 0) {
        $quota = "10";
    }
    if ($users['premium'] == 1) {
        $quota = "100";
    }
} else {
    $quota = 10;
}
$id = addslashes(str_replace(array("__", "_", ".."), array("-", " ", "&"), $_GET['tags']));
$_SESSION['ma_base_url'] = $monUrl;
$totas = mysqli_num_rows(mysqli_query($link, "select * from startup where startup.status=1 and (ville like '%" . $id . "%' or concat_tags like '%" . $id . "%')"));
$nbr_result = mysqli_num_rows(mysqli_query($link, "select * from startup where startup.status=1 and  (ville like '%" . $id . "%' or concat_tags like '%" . $id . "%')  limit $quota"));

$tag_tags = '<a href="" title="" class="tags__el"><span class="ico-tag"></span>' . ($id) . '</a>';

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}
?>
<div class="searchHeader">
    <div class="searchHeader__title"><?php echo $nbr_result ?> startups affichées / <?php echo $totas ?> startups sélectionnées</div>
    <div class="searchHeader__actions">
        <button class="btn btn-sm btn-bordered btn-searchfilters btn-filter">Filtrer</button>
        <?php
        if (isset($_SESSION['data_login'])) {
            ?>
            <button class="btn btn-sm btn-primary btn-bookmark btn-saveSearch">Enregistrer</button>
            <?php
        }
        ?>
        <a href="<?php echo URL ?>/recherche-startups" class="btn btn-sm btn-primary btn-reset">Réinitialiser</a>
    </div>
</div>
<div class="searchFilters">
    <div class="searchFilters__tags tags tags--left tags--hascross">
        <?php echo $tag_tags; ?>
    </div>
    <div class="searchFilters__tableOpt">
        <?php
        if (isset($_SESSION['data_login'])) {
            ?>
            <ul>
                <li class="columns">
                    <a href="#" title="#" class="btn-searchcolumns"><span class="ico-columns"></span> Voir plus de colonnes</a>
                </li>
            </ul>
            <?php
        }
        ?>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-search" id="mine">
        <thead>
            <tr>
                <?php foreach ($searchColumns as $col) { ?>
                    <th data-column="<?= $col['id']; ?>" class="<?= $col['class']; ?>" data-sortable="<?= $col['sortable']; ?>"><?= $col['name']; ?></th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $count = 1;
            $sql_sim = mysqli_query($link, "select * from startup where (ville like '%" . $id . "%' or concat_tags like '%" . $id . "%') order by id desc limit $quota")or die(mysqli_error($link));
            while ($sups = mysqli_fetch_array($sql_sim)) {

                $secteurs = mysqli_fetch_array(mysqli_query($link, "Select  secteur.nom_secteur,secteur.id,secteur.nom_secteur_en From   secteur Inner Join  activite    On activite.secteur = secteur.id Where  activite.id_startup =" . $sups['id']));
                $ssecteurs = mysqli_fetch_array(mysqli_query($link, "Select  sous_secteur.nom_sous_secteur,sous_secteur.id,sous_secteur.nom_sous_secteur_en From   sous_secteur Inner Join  activite    On activite.sous_secteur = sous_secteur.id Where  activite.id_startup =" . $sups['id']));
                $activite = mysqli_fetch_array(mysqli_query($link, "Select last_activite.nom_activite,last_activite.id From   last_activite Inner Join  activite    On activite.activite = last_activite.id Where  activite.id_startup =" . $sups['id']));
                $sactivite = mysqli_fetch_array(mysqli_query($link, "Select  last_sous_activite.nom_sous_activite,last_sous_activite.id From   last_sous_activite Inner Join  activite    On activite.sous_activite = last_sous_activite.id Where  activite.id_startup =" . $sups['id']));

                $tags_list = mysqli_fetch_array(mysqli_query($link, "Select  activite.tags From  activite  Where  activite.id_startup =" . $sups['id']));
                $lf_somme = mysqli_fetch_array(mysqli_query($link, "Select  sum(montant) as somme From  lf inner join startup on lf.id_startup=startup.id  Where startup.status=1 and lf.rachat=0 and startup.id =" . $sups['id']));
                $lf_nb = mysqli_fetch_array(mysqli_query($link, "Select  count(*) as nb From  lf inner join startup on lf.id_startup=startup.id  Where startup.status=1 and lf.rachat=0 and startup.id =" . $sups['id']));
                $rachat_nb = mysqli_num_rows(mysqli_query($link, "Select  id From  lf inner join startup on lf.id_startup=startup.id  Where startup.status=1 and lf.rachat=1 and startup.id =" . $sups['id']));
                $ipo_nb = mysqli_num_rows(mysqli_query($link, "Select id From  lf inner join startup on lf.id_startup=startup.id  Where startup.status=1 and lf.rachat=2 and startup.id =" . $sups['id']));
                $sup_ca = mysqli_fetch_array(mysqli_query($link, "Select  startup_ca.ca,startup_ca.annee From  startup_ca inner join startup on startup_ca.id_startup=startup.id  Where startup.status=1 and  startup.id =" . $sups['id'] . " order by annee desc limit 1"));
                $ch = "";

                $chiffre = mysqli_fetch_array(mysqli_query($link, "select * from startup_score where id_startup=" . $sups['id']));

                if ($sactivite['id'] == "")
                    $idsousactivite = 0;
                else
                    $idsousactivite = $sactivite['id'];

                if ($ssecteurs['id'] == "")
                    $idsoussecteur = 0;
                else
                    $idsoussecteur = $ssecteurs['id'];

                if ($activite['id'] == "")
                    $idactivite = 0;
                else
                    $idactivite = $activite['id'];



                $ligne_secteur_activite = mysqli_fetch_array(mysqli_query($link, "select * from secteur_activite where id_secteur=" . $secteurs['id'] . " and id_sous_secteur=" . $idsoussecteur . " and id_activite=" . $idactivite . " and id_sous_activite=" . $idsousactivite));


                $somme_global_competition = mysqli_fetch_array(mysqli_query($link, "select sum(competition_secteur) as somme from secteur_activite"));
                $nb_global_competition = mysqli_num_rows(mysqli_query($link, "select competition_secteur from secteur_activite where competition_secteur!=0"));
                $moy_competition_secteur = $somme_global_competition['somme'] / $nb_global_competition;
                $_SESSION['list_id'] = $_SESSION['list_id'] . $sups['id'] . ",";

                $tabs = explode(",", $sups['concat_tags']);
                $nbrs = count($tabs);
                for ($i = 0; $i < 6; $i++) {
                    if (trim($tabs[$i]) != '') {
                        $ch = $ch . ' <div style="background-color: #D1ECF0;color: #000;padding: 5px;border-radius: 5px;font-size: 10px;margin-bottom:5px;float: left;margin-right: 5px;">' . $tabs[$i] . '</div>';
                    }
                }
                if ($sups['status'] == 1 && $sups['bilan'] == 0) {
                    $status = 'Active';
                }
                if ($sups['status'] == 1 && $sups['bilan'] == 1) {
                    $status = 'Fermée';
                }
                if ($rachat_nb > 0) {
                    $status = 'Rachetée';
                }
                if ($ipo_nb > 0) {
                    $status = 'En bourse';
                }
                $ssh = '';
                $tabs1 = ($sups['short_fr']);
                $bnrs = strlen(($sups['short_fr']));
                for ($o = 0; $o < 50; $o++) {
                    $ssh .= $tabs1[$o];
                }
                if ($bnrs >= 50) {
                    $ssh = $ssh . "...";
                }

                $tabs22 = explode(",", $sups['concat_tags']);
                $nbs_tag = count($tabs22);
                $tagss = "";
                for ($j = 0; $j < 3; $j++) {

                    $val = trim(str_replace(array(" ", "-", "é", "è", "ê"), array("_", "__", "e", "e", "e"), trim(utf8_encode($tabs22[$j]))));
                    $tagss .= '<div  class="table-search__link"><a style="cursor:pointer" id="' . addslashes(utf8_encode($tabs22[$j])) . '" onclick="afficher_par_tag(\'' . str_replace(array(" ", "-", "é", "è", "ê"), array("_", "__", "e", "e", "e"), utf8_encode($val)) . '\')">' . addslashes(($tabs22[$j])) . '</a></div> ';
                }
                $tab_site = explode("/", str_replace(array("https://", "http://", "www."), array("", "", ""), $sups['url']));
                include("body.php");
            }
            ?>


        </tbody>
    </table>
</div>