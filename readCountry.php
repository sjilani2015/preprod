<?php
include("include/db.php");
include("functions/functions.php");

if (!empty($_POST["keyword"])) {
    $query = mysqli_query($link, "SELECT id,nom,logo FROM startup WHERE nom like '" . addslashes((str_replace(array("é", "è", "ê", "ç", "î", "ï"), array("e", "e", "e", "c", "i", "i"), $_POST["keyword"]))) . "%' and status=1 ORDER BY nom LIMIT 0,4");
    $query2 = mysqli_query($link, "SELECT * FROM new_name_list WHERE new_name like '" . addslashes((str_replace(array("é", "è", "ê", "ç", "î", "ï"), array("e", "e", "e", "c", "i", "i"), $_POST["keyword"]))) . "%' ORDER BY new_name LIMIT 0,4");
    $query3 = mysqli_query($link, "SELECT startup.id, startup.nom as sup, personnes.prenom,personnes.nom FROM personnes inner join startup on startup.id=personnes.id_startup WHERE startup.status=1 and (personnes.prenom like '" . addslashes((str_replace(array("é", "è", "ê", "ç", "î", "ï"), array("e", "e", "e", "c", "i", "i"), $_POST["keyword"]))) . "%' or personnes.nom like '" . addslashes((str_replace(array("é", "è", "ê", "ç", "î", "ï"), array("e", "e", "e", "c", "i", "i"), $_POST["keyword"]))) . "%')  ORDER BY personnes.prenom LIMIT 0,4");
    $query4 = mysqli_query($link, "SELECT * FROM tags_list WHERE tag like '" . addslashes((str_replace(array("é", "è", "ê", "ç", "î", "ï"), array("e", "e", "e", "c", "i", "i"), $_POST["keyword"]))) . "%' group by tag ORDER BY tag LIMIT 0,4");
    $query5 = mysqli_query($link, "SELECT * FROM region_new WHERE region_new like '" . addslashes((str_replace(array("é", "è", "ê", "ç", "î", "ï"), array("e", "e", "e", "c", "i", "i"), $_POST["keyword"]))) . "%'  ORDER BY region_new LIMIT 0,4");
    $nb_sup = mysqli_num_rows($query);
    $nb_inv = mysqli_num_rows($query2);
    $nb_per = mysqli_num_rows($query3);
    $nb_tags = mysqli_num_rows($query4);
    $nb_ville = mysqli_num_rows($query5);
    ?>
    <ul id="country-list" style=" list-style-type: none;">
        <?php
        if ($nb_sup > 0) {
            ?>
            <li style="font-weight: 400;border-bottom: 1px solid #eee;font-size: 15px;color: #00abc9;padding-top: 5px;padding-bottom: 10px; padding-left: 10px">Startups</li>
            <?php
            while ($country = mysqli_fetch_array($query)) {
                ?>
                <li style="padding-top: 5px;padding-bottom: 5px;cursor: pointer;border-bottom: 1px solid #eee; font-size: 11px; color: #425565; padding-left: 20px; clear: both; height: 35px" onClick="selectCountry('<?php echo 'fr/startup-france/' . generate_id($country['id']) . "/" . urlWriting(strtolower($country["nom"])) ?>');">
                    <div style="float: left"> <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $country['logo']) ?>" style="max-width: 25px;margin-right: 10px;"></div>
                    <div style="float: left"> <?php echo strtoupper(utf8_encode($country["nom"])); ?></div>
                </li>
                <?php
            }
            ?>
        <?php } ?>
        <?php
        if ($nb_inv > 0) {
            ?>
            <li style="font-weight: 400;border-bottom: 1px solid #eee;font-size: 15px;color: #00abc9;padding-top: 5px;padding-bottom: 10px; padding-left: 10px">Investisseurs</li>
            <?php
            while ($country2 = mysqli_fetch_array($query2)) {
                ?>
                <li  style="padding-top: 5px;padding-bottom: 5px;cursor: pointer;border-bottom: 1px solid #eee; font-size: 11px; color: #425565; padding-left: 20px; clear: both; height: 25px" onClick="selectInvest('<?php echo 'startups-investisseur/' . generate_id($country2['id']) . "/" . urlWriting(strtolower($country2["new_name"])) ?>');"><?php echo strtoupper($country2["new_name"]); ?></li>
                <?php
            }
            ?>
        <?php } ?>
        <?php
        if ($nb_per > 0) {
            ?>
            <li style="font-weight: 400;border-bottom: 1px solid #eee;font-size: 15px;color: #00abc9;padding-top: 5px;padding-bottom: 10px; padding-left: 10px">Entrepreneurs</li>
            <?php
            while ($country3 = mysqli_fetch_array($query3)) {
                ?>
                <li  style="padding-top: 5px;padding-bottom: 5px;cursor: pointer;border-bottom: 1px solid #eee; font-size: 11px; color: #425565; padding-left: 20px; clear: both; height: 25px" onClick="selectEntrepreneur('<?php echo 'fr/startup-france/' . generate_id($country3['id']) . "/" . urlWriting(strtolower($country3["sup"])) ?>');"><?php echo ucfirst(strtolower($country3["prenom"])) . " " . ucfirst(strtolower($country3["nom"])); ?></li>
                <?php
            }
            ?>
        <?php } ?>
        <?php
        if ($nb_tags > 0) {
            ?>
            <li style="font-weight: 400;border-bottom: 1px solid #eee;font-size: 15px;color: #00abc9;padding-top: 5px;padding-bottom: 10px; padding-left: 10px">Tags</li>
            <?php
            while ($country4 = mysqli_fetch_array($query4)) {
                ?>
                <li  style="padding-top: 5px;padding-bottom: 5px;cursor: pointer;border-bottom: 1px solid #eee; font-size: 11px; color: #425565; padding-left: 20px; clear: both; height: 25px" onClick="selectTags('<?php echo 'recherche-startups/tags/' . nottoie(utf8_decode($country4['tag'])) ?>');"><?php echo strtoupper($country4["tag"]); ?></li>
                <?php
            }
            ?>
        <?php } ?>
        <?php
        if ($nb_ville > 0) {
            ?>
            <li style="font-weight: 400;border-bottom: 1px solid #eee;font-size: 15px;color: #00abc9;padding-top: 5px;padding-bottom: 10px; padding-left: 10px">Ville / Région</li>
            <?php
            while ($country5 = mysqli_fetch_array($query5)) {
                ?>
                <li  style="padding-top: 5px;padding-bottom: 5px;cursor: pointer;border-bottom: 1px solid #eee; font-size: 11px; color: #425565; padding-left: 20px; clear: both; height: 25px" onClick="selectRegion('<?php echo 'recherche-startups/location/' . generate_id($country5['id']) . '/' . urlWriting($country5["region_new"]) ?>');"><?php echo strtoupper($country5["region_new"]); ?></li>
                    <?php
                }
                ?>
            <?php } ?>
    </ul>
    <?php
}
?>