<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
if (!isset($_SESSION['data_login'])) {
    header("location:" . URL . "/connexion");
     exit();
}
$user_compte = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}

$currentPage = "password";
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Changer mon mot de passe - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <?php
        if (isset($_POST['pwd2'])) {
            $pwd = md5($_POST['pwd']);
            $pwd2 = md5($_POST['pwd2']);
            if ($pwd == $pwd2) {
                mysqli_query($link, "update user set password='" . $pwd . "' where email='" . $_SESSION['data_login'] . "'");
                echo "<script>alert('Mot de passe modifié')</script>";
                echo "<script>window.location='modifier-mot-de-passe'</script>";
            } else {
                echo "<script>alert('Mots de passe non identiques!')</script>";
                echo "<script>window.location='modifier-mot-de-passe'</script>";
            }
        }
        ?>
        <div class="page-wrapper">
            <?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">
                    <div class="account">
                        <div class="account__aside">
                            <div class="nav-vertical">
                                <div class="nav-vertical__title">
                                    <span class="title">Mon profil</span>
                                </div>
                                <div class="nav-vertical__list">
                                    <?php include ('account/menu.php'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="account__right">
                            <form method="post" action="">
                                <div class="form-group form-group--half">
                                    <label>Votre adresse e-mail</label>
                                    <input type="text" disabled="" value="<?php echo $_SESSION['data_login']; ?>" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <div class="formgrid">
                                        <div class="formgrid__item">
                                            <label>Nouveau mot de passe</label>
                                            <input type="password" value="" name="pwd" class="form-control" />
                                        </div>
                                        <div class="formgrid__item">
                                            <label>Confirmer le nouveau mot de passe</label>
                                            <input type="password" value="" name="pwd2" class="form-control" />
                                        </div>
                                    </div>
                                </div>                    
                                <div class="form-group form-group--btn">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>