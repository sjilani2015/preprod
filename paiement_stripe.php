<?php
include("include/db.php");
include("functions/functions.php");
if (!isset($_SESSION['data_login'])) {
    echo "<script>window.location='" . url . "connexion?utm_source=nos-offres'</script>";
}

$myuser = mysqli_fetch_array(mysqli_query($link, "select* from user where email='" . $_SESSION['data_login'] . "'"));

if (isset($_POST['prix']) && (!empty($_POST['prix']))) {
    require_once('vendor/autoload.php');
    $prix = (float) $_POST['prix'];

    // \Stripe\Stripe::setApiKey("sk_test_51J17OXFAJfjJET3VsjjEFbwAj01SoVdHqr226yiUGulObkmdDaL12IStdCCmABg9E0uBgGWtNWhBloScU2MyzYyk00wKJ9qBCx");
    \Stripe\Stripe::setApiKey("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");

    $intent = \Stripe\PaymentIntent::create([
                'amount' => $prix * 100,
                'currency' => 'eur'
                
    ]);
    

    $stripe = new \Stripe\Customer('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');
   
    $stripe->create([
        'description' => $myuser['prenom'] . " " . $myuser['nom'],
        'email' =>  $_SESSION['data_login'],
        'id' =>  $intent->id,
    ]);
} else {
    header("location:index.php");
}
?>
<!DOCTYPE html>
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>Pricing</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    
    ================================================== -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/main-color.css" id="colors">
    <link rel="stylesheet" href="css/design_system.css" id="colors">
    <link rel="stylesheet" href="file.css" id="colors">

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <style>
        th{
            background: #F6F9FC;
            color: #7E84A3;
            font-size: 1.2rem;
            line-height: 16px;
            border: none !important;
        }
        td{

            font-size: 1.2rem;
            text-align: left;
            line-height: 1.6rem;
            font-family: 'Roboto';
            border: none !important;

        }
        .table-ditance{
            margin-top: 24px;
            margin-left: 8px;
        }
        .pricing-title{
            font-size: 2.6rem;
            line-height: 2.8rem;
            color: #C5D1DC;
            font-family: 'EuclidLight';
            text-align: center;
            margin-bottom: 1.6rem;

        }
        .pricing-h3{
            text-align: center;
            font-size: 1.6rem;
            line-height: 2.4rem;
            color:#2D3B48;
            margin-top: 0px;
            margin-bottom: 48px;
            font-family: 'Euclid';
        }
        .pricing-h1{
            text-align: center;
            font-family: 'EuclidBold';
            font-size:4.8rem;
            line-height: 4.8rem;
            margin-top: 40px;
            margin-bottom: 24px;
            color:#2D3B48;
        }
        .pricing-montant{
            text-align: center;
            font-family: 'EuclidBold';
            font-size: 6.4rem;

            color:#2D3B48;
        }
        .pricing-devise{
            font-family: 'EuclidLight';
        }
        .pricing-cart{
            font-family: 'EuclidBold';
            font-size: 1.2rem;
            line-height: 1.6rem;
            color: #425466;
            text-align: center;
        }
        .pricing-text{
            text-align: center;
            font-family: 'Roboto';
            font-size: 1.6rem;
            line-height: 2rem;
            color: #425466;
            margin-top: 28px;
        }
        .pricing-puce{
            margin-left: 5em;
            text-align: left;
            margin-top: 16px;
            margin-bottom: 24px;
            color: #425466;
            font-size: 1.2rem;
            font-family: 'Roboto';
            font-weight: 100;
            line-height: 24px;
            height: 144px;
        }
        .pricing-check{
            width: 1.2rem;
            margin-right: 8px;
        }
        .pricing-btn{

            background-color: #00CCFF;
            font-weight: 300;
            font-family: 'Roboto';
            border-radius: 5px;
            color: #FFFFFF;
            padding-left: 1.6rem;
            padding-right: 1.6rem;
            padding-top: 1.2rem;
            padding-bottom: 1.2rem;
            font-size: 1.2rem;
            line-height: 1.6rem;
        }
        .bloc-contact{
            padding: 24px;
        }
        .contact-text{
            text-align: center;
            color: #425466;
            font-size: 1.2rem;
            line-height: 1.6rem;
            font-family: 'Roboto';
        }
        .bloc-information{
            background: #EDF1F6;
            margin-top: 40px;
            margin-bottom: 72px;
        }
        .contact-text-span{
            font-family: 'EuclidBold';
            font-size: 1.2rem;
        }
        .contact-grand-question{
            text-align: center;
            padding-top: 24px;
            font-size: 3.2rem;
            color: #2D3B48;
            line-height: 2.4rem;
            font-family: 'EuclidBold';
            margin-bottom: 40px;
            input[type='text']{
                top: 283px;
                left: 300px;
                width: 255px;
                height: 40px;
                background: var(--arrière-plan) 0% 0% no-repeat padding-box;
                border: 1px solid var(--background-1);
                background: #F6F9FC 0% 0% no-repeat padding-box;
                border: 1px solid #D8E2ED;
                opacity: 1;
            }

        </style>


    </head>

    <body style="background: #F6F9FC;
        font-family: 'Roboto';">

        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Header Container
            ================================================== -->
<?php include('header.php'); ?>
            <div class="wrapper">

                <div class="clearfix"></div>

                <h1 class="pricing-h1">Premium</h1>

                <h3 class="pricing-h3">Une offre simple qui grandit en même temps que vous</h3>



                <div class="blob2-courbe" style="margin-bottom: 30px">
                        <div class="espace-bloc-interieur">
                            <div class="titre-overview" style="text-align: left;">Récapitualitif de la commande</div>

                        <div class="montant-overview">89 €</div>
                        <div class="montant-overview" style="font-size:11px">Par mois / Par utilisateur</div>
                            <div class="pricing-text">Une offre simple qui grandit en même<br>temps que vous</div>
                            <div class="pricing-puce" style="margin-bottom: 12px;">
                            <div>
                                <div><span><img class="pricing-check" src="assets/images/check.svg"></span> <span class="tout_text_index">Dashboard personnalisé</span> </div>
                                <div><span><img class="pricing-check" src="assets/images/check.svg"></span> <span class="tout_text_index">Recherches illimitées</span> </div>
                                <div><span><img class="pricing-check" src="assets/images/check.svg"></span> <span class="tout_text_index">100 résultats par recherche</span></div>
                                <div><span><img class="pricing-check" src="assets/images/check.svg"></span> <span class="tout_text_index">Alertes personnalisées</span></div>
                                <div><span><img class="pricing-check" src="assets/images/check.svg"></span> <span class="tout_text_index">Accès dealflow</span></div>
                                <div><span><img class="pricing-check" src="assets/images/check.svg"></span> <span class="tout_text_index">Abonnement individuel</span> </div>
                            </div>
                        </div>

                    </div>
                    <div class="espace-bloc-interieur">
                        <div class="titre-overview" style="text-align: left;">Paiement sécurisé via Stripe</div>
                        <form method="post">
                            <div id="errors" style="color: #f34d4d;
                            margin-bottom: 8px;"></div>
                            <input type="text" id='cardholder-name' autocomplete="off" placeholder="Titulaire de la carte" style="margin-bottom: 24px">
                                <div id="card-elements" style="border: 1px solid #dbdbdb;
                            padding: 8px;
                            border-radius: 4px; "></div>
                            <div id="card-errors" style="height: 25px;
                            color:#f34d4d " role="alert"></div>
                                <div style="text-align: center;
                            margin-top: 30px;"><button class="btn pricing-btn" style="color:#fff" type="button" id="card-button" data-secret="<?php echo $intent['client_secret'] ?>">Procéder au paiement (89 €)</button></div>
                            </form>
                            <div style="text-align: center;
                            margin-top: 30px;"><img src="images/logo-stripe-secure.png" alt="" style="width: 170px;"></div>
                    </div>
                </div>



                <div class="bloc-chart">




                </div>









                <div id="backtotop"><a href="#"></a></div>
<?php include('footer1.php'); ?>
            </div>

        </div>
        <!-- Wrapper / End -->












        <script src="https://js.stripe.com/v3/"></script>
        <script src="js/scripts.js"></script>
        <script src="jquery.min.js"></script>

        <script src="bootstrap.min.js"></script>
        <script src="jquery.knob.js"></script>
        <script>
            $(document).ready(function () {
                $("#search-box").keyup(function () {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo url ?>readCountry.php",
                        data: 'keyword=' + $(this).val(),
                        beforeSend: function () {
                            $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function (data) {
                            $("#suggesstion-box").show();
                            $("#suggesstion-box").html(data);
                            $("#search-box").css("background", "#FFF");
                        }
                    });
                });


            });

            function selectCountry(val) {
                const words = val.split('/');
                $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }
            function selectRegion(val) {
                const words = val.split('/');
                $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo url ?>' + val;
            }




        </script>
    </body>
</html>