<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
if (isset($_SESSION['data_login'])) {
    header('location:' . URL . '/recherche-startups');
    exit();
}

define('type', 'Je suis');
define('type_user_0', 'Entrepreneur');
define('type_user_1', 'Investisseur');
define('type_user_2', 'Autre');
define('type_user_3', 'Media');
define('type_user_4', 'Institutionnel');
define('type_user_5', 'Prestataire');
define('type_user_6', 'Etudiant');
define('type_user_7', 'Enseignant');
define('type_user_10', 'Corporate');
define('type3', 'Autre');
if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>

        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">            
                    <div class="signin">
                        <ul class="signin__tabs">
                            <li class="logintab active"><span>Déjà inscrit ?</span></li>
                            <li class="registertab"><span>Créer un compte</span></li> 
                        </ul>
                        <div class="signin__content">
                            <div class="signin__login shown">
                                <form method="post" action="connect.php">
                                    <h1>Bienvenue !<br />Connectez-vous</h1>
                                    <div class="alert alert--error" id="msg_error_mail" style="display: none">
                                        Fomrat mail non valide
                                    </div>
                                    <div class="alert alert--error" id="msg_error_user" style="display: none">
                                        Veuillez vérifier vos coordonnées !
                                    </div>
                                    <div class="alert alert--error" id="msg_error_user_block" style="display: none">
                                        Votre compte est inactif !
                                    </div>
                                    <div class="alert alert--success" style="display: none">
                                        Success
                                    </div>
                                    <div class="form-group ">
                                        <label>Email</label>
                                        <input class="form-control" id="login_connect" type="email" value="" required="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Mot de passe</label>
                                        <input class="form-control" id="pwd" type="password" value="" required="" />
                                    </div>
                                    <div class="form-group">
                                        <input class="inp-cbx" id="cbx" type="checkbox" style="display: none" />
                                        <label class="cbx" for="cbx">
                                            <span>
                                                <svg width="12px" height="10px" viewbox="0 0 12 10">
                                                <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                                </svg>
                                            </span>
                                            <span>Se souvenir de moi</span>
                                        </label>
                                    </div>
                                    <div class="form-group text-center">
                                        <button class="btn btn-primary" type="button" onclick="connexion()">Confirmer</button>
                                    </div>
                                    <div class="form-group text-center">
                                        <a href="<?php echo url; ?>mot-de-passe-oublie">Mot de passe oublié ?</a>
                                    </div>
                                </form>
                            </div>
                            <div class="signin__register">
                                <form method="post" action="<?php echo URL ?>/inscription-terminee">
                                    <h1>Inscrivez-vous<br />gratuitement pour continuer</h1>
                                    <div class="form-group">
                                        <label>Civilité</label>
                                        <div class="custom-radio-wrapper">
                                            <label class="custom-radio">
                                                Monsieur
                                                <input type="radio" required="" checked="checked" value="0" name="f1_civilite">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="custom-radio">
                                                Madame
                                                <input type="radio" required="" value="2" name="f1_civilite">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="register-grid">
                                        <div class="register-grid__col">
                                            <div class="form-group">
                                                <label>Nom</label>
                                                <input class="form-control" name="nom" required="" type="text" value="" />
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input class="form-control" name="email" type="email" autocomplete="off" required="" value="" />
                                            </div>
                                            <div class="form-group">
                                                <label>Téléphone professionel</label>
                                                <input class="form-control" name="tel" type="text" required="" value="" />
                                            </div>
                                            <div class="form-group">
                                                <label>Fonction</label>
                                                <input class="form-control" name="fonction" type="text" required="" value="" />
                                            </div>
                                            <div class="form-group">
                                                <label>Mot de passe</label>
                                                <input class="form-control" name="password1" minlength="6" autocomplete="off" type="password" required="" value="" />
                                            </div>
                                        </div>
                                        <div class="register-grid__col">
                                            <div class="form-group">
                                                <label>Prénom</label>
                                                <input class="form-control" name="prenom"  type="text" required="" value="" />
                                            </div>
                                            <div class="form-group">
                                                <label>Société</label>
                                                <input class="form-control" name="societe" type="text" value="" />
                                            </div>
                                            <div class="form-group">
                                                <label>Ville</label>
                                                <input class="form-control" name="ville" type="text" value="" />
                                            </div>
                                            <div class="form-group">
                                                <label>Vous êtes...</label>
                                                <div class="custom-select">
                                                    <select class="form-control" name="f1_type" required="">
                                                        <option value="" selected=""></option>
                                                        <option value="10"><?php echo type_user_10; ?></option>
                                                        <option value="7"><?php echo type_user_7; ?></option>
                                                        <option value="0"><?php echo type_user_0; ?></option>
                                                        <option value="6"><?php echo type_user_6; ?></option>
                                                        <option value="4"><?php echo type_user_4; ?></option>
                                                        <option value="1"><?php echo type_user_1; ?></option>
                                                        <option value="3"> <?php echo type_user_3; ?></option>
                                                        <option value="5"><?php echo type_user_5; ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Confirmez votre mot de passe</label>
                                                <input class="form-control" name="password2" minlength="6" autocomplete="off" required="" type="password" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="register-mentions">
                                        <p>En ouvrant un compte, vous avez la possibilité d’accéder à des services supplémentaires. Les données que vous nous communiquez nous permettront par ailleurs de vous proposer des newsletters et des services en lien avec votre activité. Elles nous permettront également de vous proposer des interviews, des vidéos, des événements, des produits ou services au contenu au plus près de vos attentes. <span class="btn-seemoreRegister">Lire la suite</span></span></p>
                                        <p class="hide">
                                            L'accès à nos contenus est soit gratuit soit payant, selon l'offre que vous choisissez. Si vous souhaitez accéder gratuitement à nos contenus, vous nous permettez de partager vos données avec nos partenaires commerciaux pour recevoir des offres dans votre cadre professionnel en lien avec les thématiques choisies. Il s'agit de la seule contrepartie que nous vous demandons et qui est nécessaire pour nous permettre de continuer à vous proposer gratuitement des services et du contenu de qualité. Si vous ne souhaitez pas que vos données soient partagées avec nos partenaires commerciaux et bénéficier de leurs offres, vous pouvez souscrire à notre formule payante. La formule payante propose les mêmes contenus que la formule gratuite mais sans partage de données. La liste de nos partenaires commerciaux est régulièrement mise à jour et consultable à tout moment.
                                            Conformément à notre politique générale en matière de données personnelles, vous disposez d'un droit d'accès vous permettant à tout moment de connaître la nature des données collectées vous concernant, de demander leur rectification ou leur effacement. Ce droit s'exerce par simple envoi d'un email à <a href="mailto:privacy@myfrenchstartup.com" target="_blank">privacy@myfrenchstartup.com</a>. Pour tout savoir sur la manière dont MYFRENCHSTARTUP ère les données personnelles, vous pouvez vous reporter à notre Charte de confidentialité.
                                        </p>
                                        En cliquant sur « Inscription », vous acceptez nos Conditions Générales et notre <a href="<?php echo URL ?>/charte-de-confidentialite" target="_blank">Charte de confidentialité</a>.
                                    </div>
                                    <div class="text-center">
                                        <button class="btn btn-primary">Inscription</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
                                            (function (i, s, o, g, r, a, m) {
                                                i['GoogleAnalyticsObject'] = r;
                                                i[r] = i[r] || function () {
                                                    (i[r].q = i[r].q || []).push(arguments)
                                                }, i[r].l = 1 * new Date();
                                                a = s.createElement(o),
                                                        m = s.getElementsByTagName(o)[0];
                                                a.async = 1;
                                                a.src = g;
                                                m.parentNode.insertBefore(a, m)
                                            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                                            ga('create', 'UA-36251023-1', 'auto');
                                            ga('send', 'pageview');

                                            function checkEmail(email) {
                                                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                                return re.test(email);
                                            }
                                            function connexion() {
                                                document.getElementById("msg_error_mail").style.display = "none";
                                                document.getElementById("msg_error_user").style.display = "none";
                                                document.getElementById("msg_error_user_block").style.display = "none";
                                                var login = document.getElementById("login_connect").value;
                                                var pwd = escape(document.getElementById("pwd").value);
                                                if (checkEmail(login)) {

                                                    $.ajax({
                                                        type: "POST",
                                                        url: "<?php echo URL ?>/verif_user.php?login=" + login + "&pwd=" + pwd,

                                                        success: function (data) {
                                                            var t = eval(data);
                                                            if (t[0].result == 0) {
                                                                document.getElementById("msg_error_user").style.display = "block";
                                                            } else {
                                                                $.ajax({
                                                                    type: "POST",
                                                                    url: "<?php echo URL ?>/verif_user_block.php?login=" + login + "&pwd=" + pwd,

                                                                    success: function (data1) {
                                                                        var t = eval(data1);
                                                                        if (t[0].result == 0) {
                                                                            document.getElementById("msg_error_user_block").style.display = "block";
                                                                        } else {
                                                                            $.ajax({
                                                                                type: "POST",
                                                                                url: "<?php echo URL ?>/redirection.php?login=" + login + "&pwd=" + pwd,

                                                                                success: function (data2) {

                                                                                    window.location = data2;


                                                                                }
                                                                            });
                                                                        }


                                                                    }
                                                                });
                                                            }


                                                        }
                                                    });






                                                } else {
                                                    document.getElementById("msg_error_mail").style.display = "block";
                                                }

                                            }
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>