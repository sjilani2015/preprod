<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
if (!isset($_SESSION['data_login'])) {
    header("location:" . URL . "/connexion");
    exit();
}
$user_compte = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));
if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
mysqli_query($link, "insert into user_dashboard(user,dt)values(" . $user_compte['id'] . ",'" . date('Y-m-d H:i:s') . "')");
if ($user_compte['type'] == 10)
    $val = "Corporate";
if ($user_compte['type'] == 7)
    $val = "Enseignant";
if ($user_compte['type'] == 0)
    $val = "Entrepreneur";
if ($user_compte['type'] == 6)
    $val = "Etudiant";
if ($user_compte['type'] == 4)
    $val = "Institutionnel";
if ($user_compte['type'] == 1)
    $val = "Investisseur";
if ($user_compte['type'] == 3)
    $val = "Media";
if ($user_compte['type'] == 5)
    $val = "Prestataire";
$currentPage = "datas";
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Informations personnelles - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
        <style>
            .account__right{
                padding-top: 35px;
                background: #f7fafc !important;
                border: 1px solid #eef2f6 !important;
            }
            .form-control{
                background-color: #fff !important;
                border-color: #eef2f6 !important;
            }
        </style>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>

        <div class="page-wrapper">
            <?php include ('layout/header-connected.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">            
                    <div class="account">
                        <div class="account__aside">
                            <div class="nav-vertical">
                                <div class="nav-vertical__title">
                                    <span class="title">Mon profil</span>
                                </div>
                                <div class="nav-vertical__list">
                                    <?php include ('account/menu.php'); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        if (isset($_POST['f1_civilite'])) {
                            $civ = $_POST['f1_civilite'];
                            $nom = addslashes($_POST['nom']);
                            $prenom = addslashes($_POST['prenom']);

                            $societe = addslashes($_POST['societe']);
                            $fonction = addslashes($_POST['fonction']);
                            $types = addslashes($_POST['types']);
                            $tel = addslashes($_POST['tel']);
                            $ville = addslashes($_POST['ville']);

                            mysqli_query($link, "update user set civilite=" . $civ . ",nom='" . $nom . "',prenom='" . $prenom . "',fonction='" . $fonction . "', societe='" . $societe . "',type=" . $types . ",tel='" . $tel . "',ville='" . $ville . "' where email='" . $_SESSION['data_login'] . "' ");

                            echo "<script>alert('Informations personnelles modifiées')</script>";
                            echo "<script>window.location='mon-compte'</script>";
                        }
                        ?>

                        <?php
                        if (isset($_POST['pwd2'])) {
                            $pwd = md5($_POST['pwd']);
                            $pwd2 = md5($_POST['pwd2']);
                            if ($pwd == $pwd2) {
                                mysqli_query($link, "update user set password='" . $pwd . "' where email='" . $_SESSION['data_login'] . "'");
                                echo "<script>alert('Mot de passe modifié')</script>";
                                echo "<script>window.location='modifier-mot-de-passe'</script>";
                            } else {
                                echo "<script>alert('Mots de passe non identiques!')</script>";
                                echo "<script>window.location='mon-compte'</script>";
                            }
                        }
                        ?>



                        <div class="account__right">
                            <form method="post" action="">
                                <div style="font-family: 'EuclidBold';margin-bottom: 32px;color: #8694A1;">Mes coordonnées</div>
                                <div class="form-group">
                                    <label>Civilité</label>
                                    <div class="custom-radio-wrapper">
                                        <label class="custom-radio">
                                            Monsieur
                                            <input <?php if ($user_compte['civilite'] == 0) echo "checked"; ?> type="radio" value="0" name="f1_civilite">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="custom-radio">
                                            Madame
                                            <input type="radio" <?php if ($user_compte['civilite'] == 2) echo "checked"; ?> value="2" name="f1_civilite">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="formgrid">
                                        <div class="formgrid__item">
                                            <label>Nom</label>
                                            <input type="text" value="<?php echo $user_compte['nom']; ?>" name="nom"  class="form-control" />
                                        </div>
                                        <div class="formgrid__item">
                                            <label>Prénom</label>
                                            <input type="text" value="<?php echo $user_compte['prenom']; ?>" name="prenom" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="formgrid">
                                        <div class="formgrid__item">
                                            <label>Email</label>
                                            <input type="text" value="<?php echo $user_compte['email']; ?>" disabled="" style="cursor: not-allowed;" class="form-control" />
                                        </div>
                                        <div class="formgrid__item">
                                            <label>Société</label>
                                            <input type="text" value="<?php echo $user_compte['societe']; ?>" name="societe" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="formgrid">
                                        <div class="formgrid__item">
                                            <label>Fonction</label>
                                            <input type="text" value="<?php echo $user_compte['fonction']; ?>" name="fonction" class="form-control" />
                                        </div>
                                        <div class="formgrid__item">
                                            <label>Vous êtes</label>
                                            <select name="types" id="types" class="form-control"  required="">
                                                <option value="<?php echo $user_compte['type']; ?>" selected=""><?php echo $val; ?></option>
                                                <option value="10">Corporate</option>
                                                <option value="7">Enseignant</option>
                                                <option value="0">Entrepreneur</option>
                                                <option value="6">Etudiant</option>
                                                <option value="4">Institutionnel</option>
                                                <option value="1">Investisseur</option>
                                                <option value="3">Media</option>
                                                <option value="5">Prestataire</option>



                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="formgrid">
                                        <div class="formgrid__item">
                                            <label>Téléphone</label>
                                            <input type="text" value="<?php echo $user_compte['tel']; ?>" name="tel" class="form-control" />
                                        </div>
                                        <div class="formgrid__item">
                                            <label>Ville</label>
                                            <input type="text" value="<?php echo $user_compte['ville']; ?>" name="ville" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-group--cdc" style="text-align: right;font-style: italic;margin-bottom: 0px;">
                                    <a href="<?php echo URL; ?>/charte-de-confidentialite" target="_blank">Voir la charte de confidentialité myfrenchstartup.com</a>
                                </div>
                                <div class="form-group form-group--btn" style="text-align: left;margin-top: 0px;margin-bottom: 8px;">
                                    <button type="submit" class="btn btn-primary" style="padding-left: 88px;padding-right: 88px;padding-top: 8px;padding-bottom: 8px;font-size: 16px;">Enregistrer mes coordonnnées</button>
                                </div>
                            </form>
                            <div class="form-group form-group--cdc">
                                <div style="margin-top: 32px;margin-bottom: 32px;background: #d9e3ed;height: 1px;"></div>
                            </div>
                            <form method="post" action="">
                                <div style="font-family: 'EuclidBold';margin-bottom: 32px;color: #8694A1;">Modifier mot de passe</div>
                                <div class="form-group">
                                    <div class="formgrid">
                                        <div class="formgrid__item">
                                            <label>Nouveau mot de passe</label>
                                            <input type="password" value="" required="" name="pwd" class="form-control" />
                                        </div>
                                        <div class="formgrid__item">
                                            <label>Confirmer le nouveau mot de passe</label>
                                            <input type="password" value="" required="" name="pwd2" class="form-control" />
                                        </div>
                                    </div>
                                </div>                    
                                <div class="form-group form-group--btn" style="text-align: left; margin-top: 8px;margin-bottom: 8px;">
                                    <button type="submit" class="btn btn-primary" style="padding-left: 88px;padding-right: 88px;padding-top: 8px;padding-bottom: 8px;font-size: 16px;">Modifier le mot de passe</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>