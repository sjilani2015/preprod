<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');


//if(!isset($_SESSION['data_login'])){
//  echo "<script>window.location='".url."nos-offres'</script>";
//}
$monUrl = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$_SESSION['utm_source'] = $monUrl;
$id_region = degenerate_id($_GET['id']);
//$id_region = degenerate_id(5168);
$ma_region = mysqli_fetch_array(mysqli_query($link, "select * from region_new where id=" . $id_region));
$ligne_region = mysqli_fetch_array(mysqli_query($link, "select * from startup_region where id_region=" . $id_region));
$ligne_france = mysqli_fetch_array(mysqli_query($link, "select * from startup_region_france where 1"));


$year = date("Y");
$previousyear = $year - 1;

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}

$date1 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d'), date('y')));
$date2 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d') - 30, date('y')));
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Études région <?php echo ($ma_region['region_new']) ?> - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">

        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/maps.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/franceLow.min.js"></script>
        <script>am4core.addLicense("CH303325752");</script>
        <script>
            const   themeColorBlue = am4core.color('#00bff0'),
                    themeColorFill = am4core.color('#E1F2FA'),
                    themeColorGrey = am4core.color('#dadada'),
                    themeColorLightGrey = am4core.color('#F5F5F5'),
                    themeColorDarkgrey = am4core.color("#33333A"),
                    themeColorLine = am4core.color("#87D6E3"),
                    themeColorLineRed = am4core.color("#FF7C7C"),
                    themeColorLineGreen = am4core.color("#21D59B"),
                    themeColorColumns = am4core.color("#E1F2FA"),
                    labelColor = am4core.color('#798a9a');
        </script>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">

                    <div class="banner-partner">
                        <a href="https://www.myfrenchstartup.com/channel/" target="_blank"> <img src="https://www.myfrenchstartup.com/images/banner.png" alt=""></a>
                    </div>

                    <section class="module">
                        <div class="module__title">Études région <?php echo ($ma_region['region_new']) ?></div>
                        <div class="ctg ctg--1_3">
                            <aside class="bloc text-center">
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Startups dans la région</div>
                                    <div class="datasbloc__val"><?php
                                        echo number_format($ligne_region['nbr_startups'], 0, ".", " ");
                                        ?></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Emplois créés</div>
                                    <div class="datasbloc__val"><?php
                                        echo number_format($ligne_region['emplois_generes'], 0, ".", " ");
                                        ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Total fonds levés région</div>
                                    <div class="datasbloc__val"><?php echo number_format(($ligne_region['total_invest'] / 1000), 1, ",", " ") ?> M€ <span class="ico-raise"></span></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">% Financements France</div>
                                    <div class="datasbloc__val"><?php echo str_replace(',0', '', number_format($ligne_region['perc_total_invest'], 1, ",", " ")) ?>% <span class="ico-raise"></span></div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">% Startups France</div>
                                    <div class="datasbloc__val"><?php echo str_replace(',0', '', number_format($ligne_region['perc_nbr_startups'], 1, ",", " ")) ?>% <span class="ico-raise"></span></div>
                                </div>
                            </aside>

                            <div class="bloc">
                                <div class="ctg ctg--2_2 ctg--fullheight">
                                    <div class="chart chart--radar">
                                        <div id="chart-radar"></div>
                                    </div>
                                    <script>
                                        am4core.ready(function () {


                                        /* Create chart instance */
                                        var chart = am4core.create("chart-radar", am4charts.RadarChart);
                                        /* Add data */
                                        chart.data = [{
                                        "date": "A",
                                                "value": <?php echo $ligne_region['densite_startups']; ?>,
                                                "value2": <?php echo number_format($ligne_france['densite_startups'], 1, ".", ""); ?>
                                        }, {
                                        "date": "B",
                                                "value": <?php echo $ligne_region['densite_investisseurs'] ?>,
                                                "value2": <?php echo number_format($ligne_france['densite_investisseurs'], 1, ".", ""); ?>
                                        }, {
                                        "date": "C",
                                                "value": <?php echo $ligne_region['anciennete_startup'] ?>,
                                                "value2": <?php echo number_format($ligne_france['anciennete_startup'], 1, ".", ""); ?>
                                        }, {
                                        "date": "D",
                                                "value": <?php echo $ligne_region['maturite_startup'] ?>,
                                                "value2": <?php echo number_format($ligne_france['maturite_startup'], 1, ".", ""); ?>
                                        }, {
                                        "date": "E",
                                                "value": <?php echo $ligne_region['montant_moyen_financement'] ?>,
                                                "value2": <?php echo number_format($ligne_france['montant_moyen_financement'], 1, ".", ""); ?>
                                        }, {
                                        "date": "F",
                                                "value": <?php echo $ligne_region['indice_defaillances']; ?>,
                                                "value2": <?php echo number_format($ligne_france['indice_defaillances'], 1, ".", ""); ?>
                                        }];
                                        chart.paddingTop = 0;
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;
                                        /* Create axes */
                                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.dataFields.category = "date";
                                        categoryAxis.renderer.ticks.template.strokeWidth = 2;
                                        categoryAxis.renderer.labels.template.fontSize = 11;
                                        categoryAxis.renderer.labels.template.fill = labelColor;
                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
                                        valueAxis.renderer.axisFills.template.fillOpacity = 0.05;
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.renderer.gridType = "polygons"


                                                /* Create and configure series */
                                                        function createSeries(field, name, themeColor) {
                                                        var series = chart.series.push(new am4charts.RadarSeries());
                                                        series.dataFields.valueY = field;
                                                        series.dataFields.categoryX = "date";
                                                        series.strokeWidth = 2;
                                                        series.fill = themeColor;
                                                        series.fillOpacity = 0.3;
                                                        series.stroke = themeColor;
                                                        series.name = name;
                                                        // Show bullets ?
                                                        //let circleBullet = series.bullets.push(new am4charts.CircleBullet());
                                                        //circleBullet.circle.stroke = am4core.color('#fff');
                                                        //circleBullet.circle.strokeWidth = 2;
                                                        }


                                                createSeries("value", "Région", themeColorBlue);
                                                createSeries("value2", "France", themeColorGrey);
                                                chart.legend = new am4charts.Legend();
                                                chart.legend.position = "top";
                                                chart.legend.useDefaultMarker = true;
                                                chart.legend.fontSize = "11";
                                                chart.legend.color = themeColorGrey;
                                                chart.legend.labels.template.fill = labelColor;
                                                chart.legend.labels.template.textDecoration = "none";
                                                chart.legend.valueLabels.template.textDecoration = "none";
                                                let as = chart.legend.labels.template.states.getKey("active");
                                                as.properties.textDecoration = "line-through";
                                                as.properties.fill = themeColorDarkgrey;
                                                let marker = chart.legend.markers.template.children.getIndex(0);
                                                marker.cornerRadius(12, 12, 12, 12);
                                                marker.width = 20;
                                                marker.height = 20;
                                                });
                                    </script>
                                    <div class="datagrid">
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">A :</span> Densité startups
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_region['densite_startups']); ?> 
                                                <?php
                                                if ($ligne_region['evo_competition_secteur'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_competition_secteur'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_competition_secteur'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['densite_startups'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">B :</span> Densité investisseurs
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_region['densite_investisseurs']); ?> 
                                                <?php
                                                if ($ligne_region['evo_maturite'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_maturite'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_maturite'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['densite_investisseurs'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">C :</span> Ancienneté startups
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_region['anciennete_startup']); ?> 
                                                <?php
                                                if ($ligne_region['evo_transparence'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_transparence'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_transparence'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo number_format($ligne_france['anciennete_startup'], 1, ",", ""); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">D :</span> Maturité startups
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_region['maturite_startup']); ?> 
                                                <?php
                                                if ($ligne_region['evo_anciennete'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_anciennete'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_anciennete'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo number_format($ligne_france['maturite_startup'], 1, ",", ""); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">E :</span> Montant moyen financements
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_region['montant_moyen_financement']); ?> 
                                                <?php
                                                if ($ligne_region['evo_montant_investissement'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_montant_investissement'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_montant_investissement'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(',0', '', number_format($ligne_france['montant_moyen_financement'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">F :</span> Taux<br>défaillance
                                            </div>
                                            <div class="datagrid__val"><?php echo $ligne_region['indice_defaillances']; ?> 
                                                <?php
                                                if ($ligne_region['evo_financement'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_financement'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_financement'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo number_format($ligne_france['indice_defaillances'], 1, ",", ""); ?></div>
                                        </div>
                                    </div>

                                    <div class="bloc-actions">
                                        <div class="bloc-actions__ico">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Indicateurs économiques région <?php echo ($ma_region['region_new']) ?></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Startups de la région par effectifs</div>
                                <div class="chart chart--columns">
                                    <div id="chart-startups-effectifs"></div>
                                </div>
                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                        var chart = am4core.create("chart-startups-effectifs", am4charts.XYChart);
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;
                                        // will use this to store colors of the same items
                                        var colors = {};
                                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.dataFields.category = "category";
                                        categoryAxis.renderer.minGridDistance = 10;
                                        categoryAxis.renderer.fontSize = "10px";
                                        categoryAxis.renderer.labels.template.fill = labelColor;
                                        categoryAxis.dataItems.template.text = "{realName}";
                                        categoryAxis.renderer.grid.disabled = true;
                                        // Title Bottom
                                        categoryAxis.title.text = "Tranches d'effectifs";
                                        categoryAxis.title.rotation = 0;
                                        categoryAxis.title.align = "center";
                                        categoryAxis.title.valign = "bottom";
                                        categoryAxis.title.dy = - 5;
                                        categoryAxis.title.fontSize = 11;
                                        categoryAxis.title.fill = labelColor;
                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.renderer.labels.template.fill = labelColor;
                                        // Title left
                                        valueAxis.title.text = "Nombre de startups";
                                        valueAxis.title.rotation = - 90;
                                        valueAxis.title.align = "left";
                                        valueAxis.title.valign = "top";
                                        valueAxis.title.dy = 0;
                                        valueAxis.title.fontSize = 11;
                                        valueAxis.title.fill = labelColor;
                                        // single column series for all data
                                        var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                        columnSeries.fill = themeColorColumns;
                                        columnSeries.stroke = 0;
                                        columnSeries.columns.template.width = am4core.percent(80);
                                        columnSeries.dataFields.categoryX = "category";
                                        columnSeries.dataFields.valueY = "value";
                                        // second value axis for quantity
                                        var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis2.renderer.opposite = true;
                                        valueAxis2.syncWithAxis = valueAxis;
                                        valueAxis2.tooltip.disabled = true;
                                        valueAxis2.renderer.labels.template.fontSize = 11;
                                        valueAxis2.renderer.labels.template.fill = labelColor;
                                        // Title right
                                        valueAxis2.title.text = "Financement total (M€)";
                                        valueAxis2.title.rotation = - 90;
                                        valueAxis2.title.align = "left";
                                        valueAxis2.title.valign = "top";
                                        valueAxis2.title.dy = 0;
                                        valueAxis2.title.fontSize = 11;
                                        valueAxis2.title.fill = labelColor;
                                        // quantity line series
                                        var lineSeries = chart.series.push(new am4charts.LineSeries());
                                        lineSeries.dataFields.categoryX = "category";
                                        lineSeries.dataFields.valueY = "financement";
                                        lineSeries.yAxis = valueAxis2;
                                        lineSeries.bullets.push(new am4charts.CircleBullet());
                                        lineSeries.fill = themeColorLine;
                                        lineSeries.strokeWidth = 2;
                                        lineSeries.fillOpacity = 0;
                                        lineSeries.stroke = themeColorLine;
                                        // when data validated, adjust location of data item based on count
                                        lineSeries.events.on("datavalidated", function () {
                                            lineSeries.dataItems.each(function (dataItem) {
                                                // if count divides by two, location is 0 (on the grid)
                                                if (dataItem.dataContext.count / 2 == Math.round(dataItem.dataContext.count / 2)) {
                                                    dataItem.setLocation("categoryX", 0);
                                                }
                                                // otherwise location is 0.5 (middle)
                                                else {
                                                    dataItem.setLocation("categoryX", 0.5);
                                                }
                                            })
                                        })

                                        ///// DATA
                                        var chartData = [];
                                        var lineSeriesData = [];
                                        var data =
                                            {
                                                <?php
                                                for ($i = 2014; $i <= date('Y'); $i++) {
                                                $nbr_somme = 0;
                                                $nbr_tot = 0;
                                                $verif_somme = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf inner join startup on startup.id=lf.id_startup inner join region_new on region_new.region_new=startup.region_new where rachat=0 and year(date_ajout)='" . $i . "' and region_new.id=" . $id_region));
                                                $nbr_somme = $nbr_somme + $verif_somme['somme'];

                                                $verif_nb = mysqli_num_rows(mysqli_query($link, "select lf.id from lf inner join startup on startup.id=lf.id_startup inner join region_new on region_new.region_new=startup.region_new where rachat=0 and year(date_ajout)='" . $i . "' and region_new.id=" . $id_region));
                                                $nbr_tot = $nbr_tot + $verif_nb;
                                                ?>
                                                "<?php echo $i; ?>": {
                                                    "<?php echo $i; ?>": <?php echo $nbr_tot; ?>,
                                                    "financement": <?php echo $nbr_somme / 1000; ?>
                                                },
                                                <?php
                                                }
                                                ?>

                                            }

                                        // process data and prepare it for the chart
                                        for (var providerName in data) {
                                            var providerData = data[providerName];
                                            // add data of one provider to temp array
                                            var tempArray = [];
                                            var count = 0;
                                            // add items
                                            for (var itemName in providerData) {
                                                if (itemName != "financement") {
                                                    count++;
                                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                                }
                                            }
                                            // sort temp array
                                            tempArray.sort(function (a, b) {
                                                if (a.value > b.value) {
                                                    return 1;
                                                } else if (a.value < b.value) {
                                                    return - 1
                                                } else {
                                                    return 0;
                                                }
                                            })

                                            // add quantity and count to middle data item (line series uses it)
                                            var lineSeriesDataIndex = Math.floor(count / 2);
                                            tempArray[lineSeriesDataIndex].financement = providerData.financement;
                                            tempArray[lineSeriesDataIndex].count = count;
                                            // push to the final data
                                            am4core.array.each(tempArray, function (item) {
                                                chartData.push(item);
                                            })
                                        }

                                        valueAxis.renderer.grid.template.strokeWidth = 0;
                                        //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                        categoryAxis.renderer.grid.template.strokeWidth = 0;
                                        chart.data = chartData;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Maturité des startups financées</div>
                                <div class="chart chart--columns">
                                    <div id="chart-maturite"></div>
                                </div>
                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                        var chart = am4core.create("chart-maturite", am4charts.XYChart);
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;
                                        // will use this to store colors of the same items
                                        var colors = {};
                                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.dataFields.category = "category";
                                        categoryAxis.renderer.minGridDistance = 10;
                                        categoryAxis.renderer.fontSize = "10px";
                                        categoryAxis.renderer.labels.template.fill = labelColor;
                                        categoryAxis.dataItems.template.text = "{realName}";
                                        categoryAxis.renderer.grid.disabled = true;
                                        // Title Bottom
                                        categoryAxis.title.text = "Tranches d'âges";
                                        categoryAxis.title.rotation = 0;
                                        categoryAxis.title.align = "center";
                                        categoryAxis.title.valign = "bottom";
                                        categoryAxis.title.dy = - 5;
                                        categoryAxis.title.fontSize = 11;
                                        categoryAxis.title.fill = labelColor;
                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.renderer.labels.template.fill = labelColor;
                                        // Title left
                                        valueAxis.title.text = "Nombre de startups";
                                        valueAxis.title.rotation = - 90;
                                        valueAxis.title.align = "left";
                                        valueAxis.title.valign = "top";
                                        valueAxis.title.dy = 0;
                                        valueAxis.title.fontSize = 11;
                                        valueAxis.title.fill = labelColor;
                                        // single column series for all data
                                        var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                        columnSeries.fill = themeColorColumns;
                                        columnSeries.stroke = 0;
                                        columnSeries.columns.template.width = am4core.percent(80);
                                        columnSeries.dataFields.categoryX = "category";
                                        columnSeries.dataFields.valueY = "value";
                                        // second value axis for quantity
                                        var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis2.renderer.opposite = true;
                                        valueAxis2.syncWithAxis = valueAxis;
                                        valueAxis2.tooltip.disabled = true;
                                        valueAxis2.renderer.labels.template.fontSize = 11;
                                        valueAxis2.renderer.labels.template.fill = labelColor;
                                        // Title right
                                        valueAxis2.title.text = "Financement total (M€)";
                                        valueAxis2.title.rotation = - 90;
                                        valueAxis2.title.align = "left";
                                        valueAxis2.title.valign = "top";
                                        valueAxis2.title.dy = 0;
                                        valueAxis2.title.fontSize = 11;
                                        valueAxis2.title.fill = labelColor;
                                        // quantity line series
                                        var lineSeries = chart.series.push(new am4charts.LineSeries());
                                        lineSeries.dataFields.categoryX = "category";
                                        lineSeries.dataFields.valueY = "financement";
                                        lineSeries.yAxis = valueAxis2;
                                        lineSeries.bullets.push(new am4charts.CircleBullet());
                                        lineSeries.fill = themeColorLine;
                                        lineSeries.strokeWidth = 2;
                                        lineSeries.fillOpacity = 0;
                                        lineSeries.stroke = themeColorLine;
                                        // when data validated, adjust location of data item based on count
                                        lineSeries.events.on("datavalidated", function () {
                                            lineSeries.dataItems.each(function (dataItem) {
                                                // if count divides by two, location is 0 (on the grid)
                                                if (dataItem.dataContext.count / 2 == Math.round(dataItem.dataContext.count / 2)) {
                                                    dataItem.setLocation("categoryX", 0);
                                                }
                                                // otherwise location is 0.5 (middle)
                                                else {
                                                    dataItem.setLocation("categoryX", 0.5);
                                                }
                                            })
                                        })

                                        ///// DATA
                                        var chartData = [];
                                        var lineSeriesData = [];
                                        var data =
                                            {
                                                <?php
                                                for ($i = 2014; $i <= date('Y'); $i++) {
                                                $nbr_somme = 0;
                                                $nbr_tot = 0;
                                                $verif_somme = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf inner join startup on startup.id=lf.id_startup inner join region_new on region_new.region_new=startup.region_new where rachat=0 and year(startup.date_complete)='" . $i . "' and region_new.id=" . $id_region));
                                                $nbr_somme = $nbr_somme + $verif_somme['somme'];

                                                $verif_nb = mysqli_num_rows(mysqli_query($link, "select lf.id from lf inner join startup on startup.id=lf.id_startup inner join region_new on region_new.region_new=startup.region_new where rachat=0 and year(startup.date_complete)='" . $i . "' and region_new.id=" . $id_region));
                                                $nbr_tot = $nbr_tot + $verif_nb;
                                                ?>
                                                "<?php echo $i; ?>": {
                                                    "<?php echo $i; ?>": <?php echo $nbr_tot; ?>,
                                                    "financement": <?php echo $nbr_somme / 1000; ?>
                                                },
                                                <?php
                                                }
                                                ?>

                                            }

                                        // process data and prepare it for the chart
                                        for (var providerName in data) {
                                            var providerData = data[providerName];
                                            // add data of one provider to temp array
                                            var tempArray = [];
                                            var count = 0;
                                            // add items
                                            for (var itemName in providerData) {
                                                if (itemName != "financement") {
                                                    count++;
                                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                                }
                                            }
                                            // sort temp array
                                            tempArray.sort(function (a, b) {
                                                if (a.value > b.value) {
                                                    return 1;
                                                } else if (a.value < b.value) {
                                                    return - 1
                                                } else {
                                                    return 0;
                                                }
                                            })

                                            // add quantity and count to middle data item (line series uses it)
                                            var lineSeriesDataIndex = Math.floor(count / 2);
                                            tempArray[lineSeriesDataIndex].financement = providerData.financement;
                                            tempArray[lineSeriesDataIndex].count = count;
                                            // push to the final data
                                            am4core.array.each(tempArray, function (item) {
                                                chartData.push(item);
                                            })
                                        }

                                        valueAxis.renderer.grid.template.strokeWidth = 0;
                                        //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                        categoryAxis.renderer.grid.template.strokeWidth = 0;
                                        chart.data = chartData;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                        </div>
                    </section>
                    
                    <section class="module">
                        <div class="ctg ctg--3_3">
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Emplois générés</div>
                                        <div class="datasbloc__val"><?php echo number_format($ligne_region['emplois_generes'], 0, "", " ") ?></div>
                                        <div><?php echo ceil($ligne_region['creations_emploi_12']); ?>% sur 12 mois</div>
                                        <div><?php echo $year . " vs " . $previousyear; ?></div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = ceil($ligne_region['creations_emploi_12']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min">% emplois créés en 12 mois</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Défaillances</div>
                                        <div class="datasbloc__val"><?php echo number_format($ligne_region['defaillances'], 0, "", " "); ?></div>
                                        <div><?php echo ceil($ligne_region['defaillances_12_perc']); ?>% sur 12 mois</div>
                                        <div><?php echo $year . " vs " . $previousyear; ?></div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($ligne_region['defaillances_perc']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min">% startups radiées par rapport à radiées France</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Emplois détruits</div>
                                        <div class="datasbloc__val"><?php echo number_format($ligne_region['emplois_detruits'], 0, "", " ") ?></div>
                                        <div><?php echo ceil($ligne_region['detruits_12']); ?>% sur 12 mois</div>
                                        <div><?php echo $year . " vs " . $previousyear; ?></div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($ligne_region['emplois_detruits_generes']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min">% emplois détruits / emplois générés</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="module__title">Financements startups <?php echo ($ma_region['region_new']) ?></div>
                        <div class="ctg ctg--3_3">
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Emplois générés</div>
                                        <div class="datasbloc__val"><?php echo number_format($ligne_region['emplois_generes'], 0, "", " ") ?></div>
                                        <div><?php echo ceil($ligne_region['creations_emploi_12']); ?>% sur 12 mois</div>
                                        <div><?php echo $year . " vs " . $previousyear; ?></div>
                                    </div>
                                    <?php
                                    // Calcul pour le donut avec Pourcentage de commentaires positifs
                                    // CSS Variables
                                    // Variable à toucher
                                    $valToShow = ceil($ligne_region['creations_emploi_12']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
                                    // Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

                                    // HACK CSS
                                    //Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min">% emplois créés en 12 mois</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Défaillances</div>
                                        <div class="datasbloc__val"><?php echo number_format($ligne_region['defaillances'], 0, "", " "); ?></div>
                                        <div><?php echo ceil($ligne_region['defaillances_12_perc']); ?>% sur 12 mois</div>
                                        <div><?php echo $year . " vs " . $previousyear; ?></div>
                                    </div>
                                    <?php
                                    // Calcul pour le donut avec Pourcentage de commentaires positifs
                                    // CSS Variables
                                    // Variable à toucher
                                    $valToShow = round($ligne_region['defaillances_perc']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
                                    // Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

                                    // HACK CSS
                                    //Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min">% startups radiées par rapport à radiées France</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Emplois détruits</div>
                                        <div class="datasbloc__val"><?php echo number_format($ligne_region['emplois_detruits'], 0, "", " ") ?></div>
                                        <div><?php echo ceil($ligne_region['detruits_12']); ?>% sur 12 mois</div>
                                        <div><?php echo $year . " vs " . $previousyear; ?></div>
                                    </div>
                                    <?php
                                    // Calcul pour le donut avec Pourcentage de commentaires positifs
                                    // CSS Variables
                                    // Variable à toucher
                                    $valToShow = round($ligne_region['emplois_detruits_generes']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
                                    // Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

                                    // HACK CSS
                                    //Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min">% emplois détruits / emplois générés</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="module__title">Indicateurs économiques <?php echo ($ma_region['region_new']) ?></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Historique financements du secteur</div>
                                <div class="chart chart--columns">
                                    <div id="chart-financement"></div>
                                </div>
                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                        var chart = am4core.create("chart-financement", am4charts.XYChart);
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;
                                        // will use this to store colors of the same items
                                        var colors = {};
                                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.dataFields.category = "category";
                                        categoryAxis.renderer.minGridDistance = 10;
                                        categoryAxis.renderer.fontSize = "10px";
                                        categoryAxis.renderer.labels.template.fill = labelColor;
                                        categoryAxis.dataItems.template.text = "{realName}";
                                        categoryAxis.renderer.grid.disabled = true;
                                        // Title Bottom
                                        categoryAxis.title.text = "Années";
                                        categoryAxis.title.rotation = 0;
                                        categoryAxis.title.align = "center";
                                        categoryAxis.title.valign = "bottom";
                                        categoryAxis.title.dy = - 5;
                                        categoryAxis.title.fontSize = 11;
                                        categoryAxis.title.fill = labelColor;
                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.renderer.labels.template.fill = labelColor;
                                        // Title left
                                        valueAxis.title.text = "Nombre de startups financées";
                                        valueAxis.title.rotation = - 90;
                                        valueAxis.title.align = "left";
                                        valueAxis.title.valign = "top";
                                        valueAxis.title.dy = 0;
                                        valueAxis.title.fontSize = 11;
                                        valueAxis.title.fill = labelColor;
                                        // single column series for all data
                                        var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                        columnSeries.fill = themeColorColumns;
                                        columnSeries.stroke = 0;
                                        columnSeries.columns.template.width = am4core.percent(80);
                                        columnSeries.dataFields.categoryX = "category";
                                        columnSeries.dataFields.valueY = "value";
                                        // second value axis for quantity
                                        var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis2.renderer.opposite = true;
                                        valueAxis2.syncWithAxis = valueAxis;
                                        valueAxis2.tooltip.disabled = true;
                                        valueAxis2.renderer.labels.template.fontSize = 11;
                                        valueAxis2.renderer.labels.template.fill = labelColor;
                                        // Title right
                                        valueAxis2.title.text = "Financement total (M€)";
                                        valueAxis2.title.rotation = - 90;
                                        valueAxis2.title.align = "left";
                                        valueAxis2.title.valign = "top";
                                        valueAxis2.title.dy = 0;
                                        valueAxis2.title.fontSize = 11;
                                        valueAxis2.title.fill = labelColor;
                                        // quantity line series
                                        var lineSeries = chart.series.push(new am4charts.LineSeries());
                                        lineSeries.dataFields.categoryX = "category";
                                        lineSeries.dataFields.valueY = "financement";
                                        lineSeries.yAxis = valueAxis2;
                                        lineSeries.bullets.push(new am4charts.CircleBullet());
                                        lineSeries.fill = themeColorLine;
                                        lineSeries.strokeWidth = 2;
                                        lineSeries.fillOpacity = 0;
                                        lineSeries.stroke = themeColorLine;
                                        // when data validated, adjust location of data item based on count
                                        lineSeries.events.on("datavalidated", function () {
                                            lineSeries.dataItems.each(function (dataItem) {
                                                // if count divides by two, location is 0 (on the grid)
                                                if (dataItem.dataContext.count / 2 == Math.round(dataItem.dataContext.count / 2)) {
                                                    dataItem.setLocation("categoryX", 0);
                                                }
                                                // otherwise location is 0.5 (middle)
                                                else {
                                                    dataItem.setLocation("categoryX", 0.5);
                                                }
                                            })
                                        })

                                        ///// DATA
                                        var chartData = [];
                                        var lineSeriesData = [];
                                        var data =
                                            {
                                                <?php
                                                for ($i = 2014; $i <= date('Y'); $i++) {
                                                $nbr_somme = 0;
                                                $nbr_tot = 0;
                                                $verif_somme = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf inner join startup on startup.id=lf.id_startup inner join region_new on region_new.region_new=startup.region_new where rachat=0 and year(date_ajout)='" . $i . "' and region_new.id=" . $id_region));
                                                $nbr_somme = $nbr_somme + $verif_somme['somme'];

                                                $verif_nb = mysqli_num_rows(mysqli_query($link, "select lf.id from lf inner join startup on startup.id=lf.id_startup inner join region_new on region_new.region_new=startup.region_new where rachat=0 and year(date_ajout)='" . $i . "' and region_new.id=" . $id_region));
                                                $nbr_tot = $nbr_tot + $verif_nb;
                                                ?>
                                                "<?php echo $i; ?>": {
                                                    "<?php echo $i; ?>": <?php echo $nbr_tot; ?>,
                                                    "financement": <?php echo $nbr_somme / 1000; ?>
                                                },
                                                <?php
                                                }
                                                ?>

                                            }

                                        // process data and prepare it for the chart
                                        for (var providerName in data) {
                                            var providerData = data[providerName];
                                            // add data of one provider to temp array
                                            var tempArray = [];
                                            var count = 0;
                                            // add items
                                            for (var itemName in providerData) {
                                                if (itemName != "financement") {
                                                    count++;
                                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                                }
                                            }
                                            // sort temp array
                                            tempArray.sort(function (a, b) {
                                                if (a.value > b.value) {
                                                    return 1;
                                                } else if (a.value < b.value) {
                                                    return - 1
                                                } else {
                                                    return 0;
                                                }
                                            })

                                            // add quantity and count to middle data item (line series uses it)
                                            var lineSeriesDataIndex = Math.floor(count / 2);
                                            tempArray[lineSeriesDataIndex].financement = providerData.financement;
                                            tempArray[lineSeriesDataIndex].count = count;
                                            // push to the final data
                                            am4core.array.each(tempArray, function (item) {
                                                chartData.push(item);
                                            })
                                        }

                                        valueAxis.renderer.grid.template.strokeWidth = 0;
                                        //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                        categoryAxis.renderer.grid.template.strokeWidth = 0;
                                        chart.data = chartData;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Dernières levées de fonds startups région</div>
                                Tableau
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Startups de la région en recherche de fonds <a href="">Voir la liste &raquo;</a></div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="moneyCell text-center">Montant recherché</th>
                                                <th class="dateCell text-center">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $list_prob = explode(",", $ligne_region['last_deal']);
                                            $nbrs = count($list_prob);
                                            for ($i = 0; $i < $nbrs; $i++) {

                                                $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation,date_complete from startup where id=" . $list_prob[$i]));
                                                $data_secteur4 = mysqli_fetch_array(mysqli_query($link, "SELECT * FROM  startup_deal_flow  where id_startup=" . $list_prob[$i] . " order by startup_deal_flow.dt desc limit 1"));
                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="logoNameCell">
                                                            <div class="logo">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt="<?php echo ($ma_startup['nom']); ?>" /></a>
                                                            </div>
                                                            <div class="name"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo ($ma_startup['nom']); ?></a></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center moneyCell"><?php echo str_replace(",0", "", number_format($data_secteur4['montant'] / 1000, 1, ",", " ")); ?> M€</td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($data_secteur4['dt']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Top 10 probabilité levée de fonds à 12 (ou 6) mois <a href="">Voir la liste &raquo;</a></div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="dateCell text-center">Création</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $list_prob = explode(",", $ligne_region['last_prob']);
                                            $nbrs = count($list_prob);
                                            for ($i = 0; $i < $nbrs; $i++) {

                                                $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation,date_complete from startup where id=" . $list_prob[$i]));
                                                $tot_lev_prob = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $list_prob['id']));
                                                ?>


                                                <tr>
                                                    <td>
                                                        <div class="logoNameCell">
                                                            <div class="logo">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt="<?php echo ($ma_startup['nom']); ?>" /></a>
                                                            </div>
                                                            <div class="name"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo ($ma_startup['nom']); ?></a></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($ma_startup['date_complete']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Actualités startups région <?php echo ($ma_region['region_new']) ?></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Dernières startups créées dans la région</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Startup</th>
                                            <th class="moneyCell">Secteur</th>
                                            <th class="dateCell text-center">Création</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="" title="" class="companyNameCell"><span>Lafinbox</span></a>
                                                </td>
                                                <td class="text-center">Éducation, Apprentissage</td>
                                                <td class="text-center dateCell">Oct. 2021</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="" title="" class="companyNameCell"><span>ALAN</span></a>
                                                </td>
                                                <td class="text-center">Éducation, Apprentissage</td>
                                                <td class="text-center dateCell">Oct. 2021</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="" title="" class="companyNameCell"><span>LA FOURCHETTE</span></a>
                                                </td>
                                                <td class="text-center">Cleantech, Énergie</td>
                                                <td class="text-center dateCell">Aoû. 2021</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="" title="" class="companyNameCell"><span>DEEZER</span></a>
                                                </td>
                                                <td class="text-center">Éducation, Apprentissage</td>
                                                <td class="text-center dateCell">Jui. 2021</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Dernières acquisitions ou IPO dans la région</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="moneyCell">Secteur</th>
                                                <th class="dateCell text-center">Création</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <a href="" title="" class="companyNameCell"><span>Lafinbox</span></a>
                                            </td>
                                            <td class="text-center">Éducation, Apprentissage</td>
                                            <td class="text-center dateCell">Oct. 2021</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="" title="" class="companyNameCell"><span>ALAN</span></a>
                                            </td>
                                            <td class="text-center">Services aux entreprises</td>
                                            <td class="text-center dateCell">Oct. 2021</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="" title="" class="companyNameCell"><span>LA FOURCHETTE</span></a>
                                            </td>
                                            <td class="text-center">Éducation, Apprentissage</td>
                                            <td class="text-center dateCell">Aoû. 2021</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="" title="" class="companyNameCell"><span>DEEZER</span></a>
                                            </td>
                                            <td class="text-center">Éducation, Apprentissage</td>
                                            <td class="text-center dateCell">Jui. 2021</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Marchés et secteurs <?php echo ($ma_region['region_new']) ?></div>
                        <div class="bloc">
                            <div class="tablebloc">
                                <table class="table" id="table-marches-secteurs">
                                    <thead>
                                    <tr>
                                        <th>Secteur</th>
                                        <th class="text-center">Startups</th>
                                        <th class="text-center">Créations sur 12 mois</th>
                                        <th class="text-center">Ancienneté moy.</th>
                                        <th class="text-center">Effectifs moy.</th>
                                        <th class="text-center">Score maturité</th>
                                        <th class="text-center">Financements cumulés</th>
                                        <th class="text-center">Levées de fonds sur 12 mois</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Restauration, Cuisine, Alimentation</td>
                                            <td class="text-center moneyCell">76</td>
                                            <td class="text-center moneyCell">5,6%</td>
                                            <td class="text-center moneyCell">6 ans</td>
                                            <td class="text-center moneyCell">618</td>
                                            <td class="text-center moneyCell">3,8</td>
                                            <td class="text-center moneyCell">40,2 M€</td>
                                            <td class="text-center moneyCell">24%</td>
                                        </tr>
                                        <tr>
                                            <td>Cleantech,Énergie</td>
                                            <td class="text-center moneyCell">79</td>
                                            <td class="text-center moneyCell">19,7%</td>
                                            <td class="text-center moneyCell">8,1 ans</td>
                                            <td class="text-center moneyCell">908</td>
                                            <td class="text-center moneyCell">4,4</td>
                                            <td class="text-center moneyCell">317,7 M€ M€</td>
                                            <td class="text-center moneyCell">33%</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <script>
                                    $(document).ready(function() {
                                        $('#table-marches-secteurs').DataTable();
                                    } );
                                </script>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Géographie <?php echo ($ma_region['region_new']) ?></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Dynamiques du région</div>
                            </div>
                            <div class="bloc">
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="bloc">
                            <div class="bloc__title">Dernières levées de fonds de la région <?php echo ($ma_region['region_new']) ?></div>
                            <div class="table-responsive">
                                <table class="table table-search">
                                    <thead>
                                        <tr>

                                            <th></th>
                                            <th>Startups</th>
                                            <th>Dernier montant levé (M€)</th>
                                            <th>Date</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $list_last_lf = explode(",", $ligne_region['last_lf']);
                                        $nbrs = count($list_last_lf);
                                        for ($i = 0; $i < $nbrs; $i++) {

                                            $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation from startup where id=" . $list_last_lf[$i]));
                                            $data_lf_last = mysqli_fetch_array(mysqli_query($link, "select lf.montant,lf.date_ajout from lf  where id_startup=" . $list_last_lf[$i]));
                                            ?>
                                            <tr>
                                                <td class="table-search__startup">

                                                    <div class="startupInfos__image">
                                                        <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt="<?php echo "Startup " . $ma_startup["nom"]; ?>"></a>
                                                    </div>


                                                </td>
                                                <td class="table-search__position center">

                                                    <div class="startupInfos__desc">
                                                        <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo stripslashes($ma_startup['nom']); ?></a></div>
                                                        <div class="companyLabel"><?php echo stripslashes($ma_startup['short_fr']); ?></div>
                                                    </div>

                                                </td>
                                                <td class="table-search__position center"><?php echo str_replace(",0", "", number_format($data_lf_last['montant'] / 1000, 1, ",", " ")); ?> M€</td>
                                                <td class="table-search__position center"><?php echo change_date_fr_chaine_related($data_lf_last['date_ajout']); ?></td>

                                            </tr>

                                            <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="bloc">
                            <div class="bloc__title">Dernières augmentations de capital secteur <?php echo ($ma_region['region_new']) ?></div>
                            <div class="table-responsive">
                                <table class="table table-search">
                                    <thead>
                                        <tr>

                                            <th></th>
                                            <th>Startups</th>
                                            <th>Augmentation du capital (k€)</th>
                                            <th>Date</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $list_capital = explode(",", $ligne_region['last_capital']);
                                        $nbrs = count($list_capital);
                                        for ($i = 0; $i < $nbrs; $i++) {

                                            $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation from startup where id=" . $list_capital[$i]));
                                            $dernier_capital = mysqli_fetch_array(mysqli_query($link, "select * from startup_capital where id_startup=" . $list_capital[$i] . " order by dt desc limit 1"));
                                            ?>
                                            <tr>
                                                <td class="table-search__startup">

                                                    <div class="startupInfos__image">
                                                        <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt="<?php echo "Startup " . $ma_startup["nom"]; ?>"></a>
                                                    </div>


                                                </td>
                                                <td class="table-search__position center">

                                                    <div class="startupInfos__desc">
                                                        <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo stripslashes($ma_startup['nom']); ?></a></div>
                                                        <div class="companyLabel"><?php echo stripslashes($ma_startup['short_fr']); ?></div>
                                                    </div>

                                                </td>
                                                <td class="table-search__position center"><?php echo str_replace(",0", "", number_format($dernier_capital['capital'] / 1000, 1, ",", " ")); ?> k€</td>
                                                <td class="table-search__position center"><?php echo change_date_fr_chaine_related($dernier_capital['dt']); ?></td>

                                            </tr>

                                            <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>




                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
                                    (function (i, s, o, g, r, a, m) {
                                    i['GoogleAnalyticsObject'] = r;
                                    i[r] = i[r] || function () {
                                    (i[r].q = i[r].q || []).push(arguments)
                                    }, i[r].l = 1 * new Date();
                                    a = s.createElement(o),
                                            m = s.getElementsByTagName(o)[0];
                                    a.async = 1;
                                    a.src = g;
                                    m.parentNode.insertBefore(a, m)
                                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                    ga('create', 'UA-36251023-1', 'auto');
                                    ga('send', 'pageview');
        </script>
    </body>
</html>