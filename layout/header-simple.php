<header class="header" id="header">
    <div class="header__wrapper" id="header-wrapper">
        <div class="burger" id="toggleMainMenu">
            <span></span>
            <span></span>
            <span></span>
        </div>
        
        <div class="logo">
            <a href="<?=URL;?>" title="MyFrenchStartup">
                <img src="<?=URL;?>/static/images/logos/logo.svg?1" alt="<?=SITENAME;?>" width="170">
            </a>
        </div>
        
        <div class="searchbar">
            <input class="searchbar__field form-control" onkeyup="chercher()" id="search-box" type="text" value="" placeholder="Trouver une startup, un investisseur..." name="searchbar" />
            <span class="searchbar__btn">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.6 44.3"><path d="M24.3 38.6C13.6 38.6 5 29.9 5 19.3 5 8.7 13.6 0 24.3 0c10.6 0 19.3 8.7 19.3 19.3C43.6 29.9 34.9 38.6 24.3 38.6zM24.3 5.2c-7.8 0-14.1 6.3-14.1 14.1 0 7.8 6.3 14.1 14.1 14.1 7.8 0 14.1-6.3 14.1-14.1C38.4 11.5 32 5.2 24.3 5.2z"></path><path d="M3.2 44.3c-0.8 0-1.6-0.3-2.3-0.9 -1.3-1.3-1.3-3.3 0-4.5l8.2-8.2c1.3-1.3 3.3-1.3 4.5 0 1.3 1.3 1.3 3.3 0 4.5l-8.2 8.2C4.9 44 4 44.3 3.2 44.3z"></path></svg>
            </span>
            <div id="suggesstion-box" style="position: absolute;background: #fff;font-weight: 100;border: none;border-radius: 5px;padding-right: 25px;width: 100%;text-align: left;z-index: 99999999; top:47px"></div>
        </div>
        
        <div class="headernav">
            <ul class="headernav__links">
                <li><a href="<?php echo URL ?>/recherche-startups" title="Startups">Startups</a></li>
                <li><a href="<?php echo URL ?>/investisseurs" title="Investisseurs">Investisseurs</a></li>
                <li class="headernav__links__more" data-dropdown="univers">
                    <a href="#" title="">Plus <i class="caret-d"></i></a>
                    <div class="dropdown" id="univers">
                        <div class="dropdown__content">
                            <ul>
                                <li><a href="<?php echo URL ?>/entreprises" title="Entreprises">Entreprises</a></li>
                                <li><a href="<?php echo URL ?>/secteur-public" title="Secteur public">Secteur Public</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="headernav__btn">
                <li><a href="<?php echo URL; ?>/connexion" title="Connexion" class="btn btn-bordered">S'identifier</a></li>
                <li><a href="<?php echo URL; ?>/connexion" title="S'inscrire" class="btn btn-primary">S'inscrire</a></li>
            </ul>
        </div>

        <div class="searchbarIcon" id="toggleSearchbar">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.6 44.3"><path d="M24.3,38.6C13.6,38.6,5,29.9,5,19.3S13.6,0,24.3,0c10.6,0,19.3,8.7,19.3,19.3S34.9,38.6,24.3,38.6z M24.3,4.2c-7.8,0-15.1,7.3-15.1,15.1s7.3,15.1,15.1,15.1s15.1-7.3,15.1-15.1S32,4.2,24.3,4.2z"/><path d="M3.2,44.3c-0.8,0-1.6-0.3-2.3-0.9c-1.3-1.3-1.3-3.3,0-4.5l8.2-8.2c1.3-1.3,3.3-1.3,4.5,0c1.3,1.3,1.3,3.3,0,4.5l-8.2,8.2C4.9,44,4,44.3,3.2,44.3z"/></svg>
        </div>
        <div class="loginIcon">
            <a href="<?php echo URL; ?>/connexion" title="Connexion">
                <svg height="30.737" viewBox="0 0 34.703 30.737" width="34.703" xmlns="http://www.w3.org/2000/svg"><g fill="none" stroke="#6a7b8a" stroke-linecap="round" stroke-linejoin="round" stroke-width="3.5" transform="translate(.717 .5)"><circle cx="16.779" cy="8.309" r="7.809"/><path d="m.5 29.02s1.941-7.588 16.28-7.588 16.014 7.588 16.014 7.588"/></g></svg>
            </a>
        </div>

        </div>
</header>