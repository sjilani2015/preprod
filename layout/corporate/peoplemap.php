<div class="hpSection hpSection--peopleMap">
    <div class="container">
        <div class="peopleMap">
            <div class="peopleMap__title">Votre outil de travail est fin prêt </div>
            <div class="peopleMap__subtitle">
                <p>Retrouvez l’écosystème au complet.<br>Commencez à travailler.</p>
                <br />
                <a href="https://www.myfrenchstartup.com/recherche-startups" class="btn btn-primary">Échangez</a>
            </div>

            <div class="peopleMap__people peopleMap__people--1">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/CORP3.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Comment surveiller les startups disruptives pour l’activité de mon groupe ?
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--2">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/CORP4.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                       Existe-t-il une innovation technologique qui pourrait être repositionnée dans mon environnement ?
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--3">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/CORP5.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Comment comparer la maturité et la capacité à délivrer entre plusieurs startups à approcher ?
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--4">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/CORP6.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                       Je dois être informé(e) quand une startup se crée sur l’un de mes marchés
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--5">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/CORP7.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Quel serait le partenaire technologique idéal pour mon projet interne ?
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>