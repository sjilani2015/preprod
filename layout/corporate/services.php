<div class="hpSection hpSection--services">
    <div class="container">
        <h2>Les services que vous attendez. Sans attendre</h2>
        <div class="carousel" data-flickity='{ "groupCells": true, "contain": true, "pageDots": false, "prevNextButtons": false}'>
            <div class="carousel-cell">
                <div class="services__col">
                    <div class="services__icon">
                        <img src="<?=IMG_PATH;?>home/icons/speaker.svg" alt="">
                    </div>
                    <div class="services__title">Innovation</div>
                    <div class="services__desc">Identifiez les startups disruptives de votre secteur. Invitez-les à répondre à vos appels à projets.</div>
                </div>
            </div>
            <div class="carousel-cell">
                <div class="services__col">
                    <div class="services__icon">
                        <img src="<?=IMG_PATH;?>home/icons/money.svg" alt="">
                    </div>
                    <div class="services__title">Financez</div>
                    <div class="services__desc">Identifiez vos futurs investissements, parcourez les anciens deals, contactez directement des fondateurs.</div>
                </div>
            </div>
            <div class="carousel-cell">
                <div class="services__col">

                    <div class="services__icon">
                        <img src="<?=IMG_PATH;?>home/icons/aim.svg" alt="">
                    </div>
                    <div class="services__title">Veille</div>
                    <div class="services__desc">Créez des listes de startups ciblées. Soyez notifiés de chaque mouvement ou création de startup.</div>
                </div>
            </div>
        </div>
        <div class="services__cta">
            <a href="<?php echo URL ?>/recherche-startups" title="" class="btn btn-primary">Démarrer</a>
        </div>
    </div>
</div>