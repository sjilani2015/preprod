<div class="hpSection hpSection--overview">
    <div class="container">
        <h1>L’écosystème complet des startups françaises.<br>Accessible comme jamais.</h1>
        <div class="hpSection__subtitle">
            <p>Disposez de la data nécessaire pour votre innovation.<br>Anticipez et collaborez avec les startups disruptives de votre secteur.
</p>
        </div>
        <!--<div class="searchbarWrapper">
            <div class="searchbar">
                <input class="searchbar__field form-control" type="text" value="" placeholder="Trouver une startup, un investisseur..." name="searchbar">
                <button type="submit" class="btn btn-primary">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.6 44.3"><path d="M24.3 38.6C13.6 38.6 5 29.9 5 19.3 5 8.7 13.6 0 24.3 0c10.6 0 19.3 8.7 19.3 19.3C43.6 29.9 34.9 38.6 24.3 38.6zM24.3 5.2c-7.8 0-14.1 6.3-14.1 14.1 0 7.8 6.3 14.1 14.1 14.1 7.8 0 14.1-6.3 14.1-14.1C38.4 11.5 32 5.2 24.3 5.2z"></path><path d="M3.2 44.3c-0.8 0-1.6-0.3-2.3-0.9 -1.3-1.3-1.3-3.3 0-4.5l8.2-8.2c1.3-1.3 3.3-1.3 4.5 0 1.3 1.3 1.3 3.3 0 4.5l-8.2 8.2C4.9 44 4 44.3 3.2 44.3z"></path></svg>
                </button>
            </div>
        </div>-->
        <div class="hpSection__cta">
           
            <a href="<?php echo URL ?>/recherche-startups" class="btn btn-primary">Démarrer</a>
        </div>
        <div class="decor">
            <div class="decor__laptop">
                <img src="<?=IMG_PATH;?>home/laptop_secteurEntreprise.png" alt="" height="545">
            </div>
            <div class="decor__people decor__people--1">
                <img src="<?=IMG_PATH;?>home/people/CORP2.png" alt="" width="170">
            </div>
            <div class="decor__people decor__people--2">
                <img src="<?=IMG_PATH;?>home/people/CORP1.png" alt="" width="170">
            </div>
        </div>
    </div>
</div>