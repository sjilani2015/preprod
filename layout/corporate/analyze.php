<div class="hpSection hpSection--analyze">
    <div class="decor-bgdiag"></div>
    <div class="container">
        <h2>Études &amp; analyses</h2>
        <div class="hpSection__subtitle">
            <p>La puissance d’un bigdata allié à des scores, indicateurs, comparables et probabilités. Pour des études référentes pré-définies ou à la demande.</p>
        </div>
        <div class="analyzeBloc-container">
            <div class="analyzeBloc">
                <div class="analyzeBloc__thumb">
                    <img class=" lazyloaded" src="<?=IMG_PATH;?>home/analyze/03.png" data-src="<?=IMG_PATH;?>home/analyze/03.png" alt="">
                </div>
                <div class="analyzeBloc__content">
                    <div class="analyzeBloc__label">Études &amp; Analyse</div>
                    <div class="analyzeBloc__title">Analyse des startups fintech</div>
                </div>
            </div>
            <div class="analyzeBloc">
                <div class="analyzeBloc__thumb">
                    <img class=" lazyloaded" src="<?=IMG_PATH;?>home/analyze/01.png" data-src="<?=IMG_PATH;?>home/analyze/01.png" alt="">
                </div>
                <div class="analyzeBloc__content">
                    <div class="analyzeBloc__label">Data room</div>
                    <div class="analyzeBloc__title">Les startups et les GAFA</div>
                </div>
            </div>
            <div class="analyzeBloc">
                <div class="analyzeBloc__thumb">
                    <img class=" lazyloaded" src="<?=IMG_PATH;?>home/analyze/02.png" data-src="<?=IMG_PATH;?>home/analyze/02.png" alt="">
                </div>
                <div class="analyzeBloc__content">
                    <div class="analyzeBloc__label">Work the data</div>
                    <div class="analyzeBloc__title">Les startups et brevetées</div>
                </div>
            </div>
        </div>
    </div>
</div>