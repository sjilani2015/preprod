<div class="hpSection hpSection--demo">
    <div class="decor-bgdiag"></div>
    <div class="container">
        <div class="blocDemo">
            <div class="blocDemo__container">
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__1">Pensé pour les acteurs de l’innovation</div>
                        <div class="blocDemo__title__2">Naviguez dans l’écosystème et identifiez vos futurs partenaires</div>
                    </div>
                    <p class="blocDemo__intro">Au-delà des chiffres, identifiez les innovations émergentes et les risques de rupture. Diffusez vos appels à projets pour nouer des partenariats avec les startups qui transformeront votre activité.</p>
                    <p class="blocDemo__intro">Découvrez les startups de tout un secteur ou marché, nos indicateurs et scores de maturité ou de capacité à délivrer.</p>
                    <p class="blocDemo__intro">Rendez visible le dynamisme de votre groupe, sa culture de l’innovation et engagez la relation.</p>

                </div>
                <div class="blocDemo__screen">
                    <img class="lazyload" src="<?= IMG_PATH; ?>home/screens/screen_01_enterprise.png" data-src="<?= IMG_PATH; ?>home/screens/screen_01.png" alt="My french startup : screenshot d'utilisation 1 de l'outil">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="hpSection hpSection--demo">
    <div class="container">
        <div class="blocDemo">
            <div class="blocDemo__container">
                <div class="blocDemo__screen">
                    <img class="lazyload" src="<?= IMG_PATH; ?>home/screens/screen_02_entreprise.png" data-src="<?= IMG_PATH; ?>home/screens/screen_04.png" alt="My french startup : screenshot d'utilisation 4 de l'outil">
                </div>
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__1">Le meilleur de la data</div>
                        <div class="blocDemo__title__2">Une data room qualifiée et validée chaque jour</div>
                    </div>
                    <p class="blocDemo__intro">Des données enrichies en temps réel : toutes les actualités, mises à jour par les fondateurs, données open data françaises, croisées, normalisées, validées par nos soins.</p>
                    <ul class="blocDemo__list">
                        <li>Retrouvez chaque aventure et tendance de l’innovation made in France.</li>
                        <li>Data crunching par secteurs, métiers, régions, types de technologies associés à nos indices, comparables et dynamiques</li>
                        <li>Tous les financements et pool d’investisseurs associés pour évaluer les capacités à se développer</li>
                    </ul>
                    <a href="<?php echo URL ?>/recherche-startups" class="btn btn-primary btn-caret-right">Démarrer</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hpSection hpSection--demo">
    <div class="decor-bgdiag"></div>
    <div class="container">
        <h2>Le groupe du mois</h2>
        <div class="blocDemo blocDemo--startupoftheweek">
            <div class="blocDemo__container">
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__2">TF1</div>
                    </div>
                    <p class="blocDemo__intro">Leader français des media, TF1 prend en compte de manière active la dynamique startups en extension de son écosystème : site de ventes privées pour le second œuvre du bâtiment avec Batiwiz, site emploi avec Cornerjob, plateforme d’engagement des téléspectateurs avec Synchronized, services aux particuliers avec SeFaireAider, site de location d’appartements meublés avec Sejourning. Plus de 20 M€ d’investissements et co-investissements dont certains avec Xavier Niel et d’autres media tels que Mediaset ou TV Azteca. </p>
                   
                </div>
                <div class="blocDemo__screen">
                    <img class=" lazyload" src="<?= IMG_PATH; ?>home/screens/screen_03_enterprise.png" data-src="<?= IMG_PATH; ?>home/screens/screen_05.png" alt="My french startup : groupe du mois">
                </div>
            </div>
        </div>
    </div>
</div>