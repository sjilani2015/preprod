<footer id="footer" class="footer">
    <div class="container">
        <div class="footer__wrapper">
            <div class="footer__bloc">
                <div class="footer__title">MyFrenchStartup</div>
                <p class="companyDesc">MyFrenchStartup est la solution SaaS de référence pour l’écosystème des startups en France. Nous déployons une gamme de services sans équivalent pour faciliter l’essor de l’innovation française, en général, et les relations entrepreneurs, investisseurs, corporate et institutionnels, en particulier. Data actualisées en temps réel, analyses dynamiques de la startup jusqu’aux secteurs, market place pour les rencontres de talents et les partenariats… Impossible n’est pas MyFrenchStartup.</p>
            </div>
            <div class="footer__bloc">
                <div class="footer__title">Liens rapides</div>
                <ul>
                    <li><a href="<?php echo URL ?>/contact" title="">Contact</a></li>
                    <li><a href="<?php echo URL ?>/nos-offres" title="">Nos offres</a></li>
                    <li><a href="<?php echo URL ?>/faq" title="Foire aux questions">FAQ</a></li>
                    <li><a href="<?php echo URL ?>/mentions-legales" title="">Mentions légales</a></li>
                    <li><a href="<?php echo URL ?>/conditions-generales-de-ventes" title="CGV">CGV</a></li>
                    <li> <a href="javascript:Didomi.preferences.show()">Préférences</a></li>
                </ul>
            </div>
            
            <div class="footer__bloc">
                <div class="footer__title">Inscrivez-vous à notre newsletter</div>
                <form class="nl-form" method="post" action="<?php echo URL ?>/newsletter">
                    <input type="email" name="email_news" id="email_news" value="" required="" placeholder="Inscrivez-vous à notre newsletter" class="form-control">
                    <input type="submit" class="btn btn-block btn-primary" name="signup" value="S'inscrire maintenant">
                </form>
            </div>
        </div>
    </div>
</footer>