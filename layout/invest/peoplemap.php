<div class="hpSection hpSection--peopleMap">
    <div class="container">
        <div class="peopleMap">
            <div class="peopleMap__title">Votre outil de travail est prêt.</div>
            <div class="peopleMap__subtitle">
                <p>Retrouvez l’écosystème au complet.<br>Commencez à travailler.</p>
                <br />
                <a href="https://www.myfrenchstartup.com/recherche-startups" class="btn btn-primary">Échangez</a>
            </div>

            <div class="peopleMap__people peopleMap__people--1">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/INVEST3.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Comment se comportent mes secteurs d’investissement par rapport aux autres ?
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--2">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/INVEST4.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                       Quel historique pour les startups concurrentes à mon projet d’investissement ?
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--3">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/INVEST5.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Comment se situent mes participations par rapport à leurs comparables ?
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--4">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/INVEST6.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                       Je dois être informé(e) quand une startup se crée sur un de mes secteurs d’investissement
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--5">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/INVEST7.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Quel serait le co-investisseur idéal pour ce dossier ?
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>