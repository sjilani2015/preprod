<div class="hpSection hpSection--demo">
    <div class="decor-bgdiag"></div>
    <div class="container">
        <div class="blocDemo">
            <div class="blocDemo__container">
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__1">Pensé pour le métier d’investisseur</div>
                        <div class="blocDemo__title__2">Surveillez & analysez : startups, secteurs, innovation</div>
                    </div>
                    <p class="blocDemo__intro">Exploitez nos data, indicateurs et scores : maturité, historique de financement, comparables secteur/marché...</p>
                    <ul class="blocDemo__list">
                        <li>Accédez à une sélection exclusive de startups en recherche de fonds</li>
                        <li>Profitez de nos données exploitables, historiques et angles d’analyses variés</li>
                        <li>Gagnez en visibilité. Attirez les startups sensibles à vos caractéristiques et votre portefeuille.</li>
                        
                    </ul>
                    <a href="<?php echo URL ?>/" class="btn btn-primary btn-caret-right">J'ajoute ma startup</a>
                </div>
                <div class="blocDemo__screen">
                    <img class="lazyload" src="<?=IMG_PATH;?>home/screens/screen_01_invest.png" data-src="<?=IMG_PATH;?>home/screens/screen_01.png" alt="My french startup : screenshot d'utilisation 1 de l'outil">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="hpSection hpSection--demo">
    <div class="container">
        <div class="blocDemo">
            <div class="blocDemo__container">
                <div class="blocDemo__screen">
                    <img class="lazyload" src="<?=IMG_PATH;?>home/screens/screen_02_invest.png" data-src="<?=IMG_PATH;?>home/screens/screen_04.png" alt="My french startup : screenshot d'utilisation 4 de l'outil">
                </div>
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__1">Le meilleur de la data</div>
                        <div class="blocDemo__title__2">Une data room qualifiée, validée et scorée chaque jour</div>
                    </div>
                    <p class="blocDemo__intro">Des données enrichies en temps réel : toutes les actualités et les mises à jour, données open data françaises, croisées, normalisées, validées par nos soins.</p>
                    <ul class="blocDemo__list">
                        <li>Retrouvez chaque aventure et tendance de l’innovation made in France.</li>
                        <li>Data crunching par secteurs et métiers en référent pour tous les indicateurs avec les dynamiques spécifiques</li>
                        <li>Toutes les positions des fonds, investisseurs privés et institutionnels avec l’analyse de leurs sensibilités d’investissement</li>
                    </ul>
                    <a href="<?php echo URL  ?>/recherche-startups" class="btn btn-primary btn-caret-right">Démarrer</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hpSection hpSection--demo">
    <div class="decor-bgdiag"></div>
    <div class="container">
        <h2>L’investisseur du mois</h2>
        <div class="blocDemo blocDemo--startupoftheweek">
            <div class="blocDemo__container">
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__2">PARTECH VENTURES</div>
                    </div>
                    <p class="blocDemo__intro">Début octobre 2021, Partech Venture International a participé à la levée de fonds de 5 M€ de PLUM KITCHEN, la startup première plateforme créative dans le domaine de l'architecture intérieure. Il s’agit ici de la 4ème participation dans le secteur Aménagement-architecture de cet investisseur de poids sur le marché français avec plus de 2 Mds investis en France pour 135 financements référencés. Partech Venture est significativement investi dans le B2B avec 18% sur les NTIC et SSII, 13% dans les services aux entreprises et 12% dans l’électronique et les composants. Comme pour le financement de PLUM KITCHEN, Kima Ventures est un co-investisseur récurrent pour Partech Venture avec 17 participations communes.</p>
                     <a href="<?php echo URL; ?>/startups-investisseur/5630/bpifrance" class="btn btn-primary btn-caret-right">Étudier cet investisseur</a>
                </div>
                <div class="blocDemo__screen">
                    <img class=" lazyload" src="<?=IMG_PATH;?>home/screens/screen_03_invest.png" data-src="<?=IMG_PATH;?>home/screens/screen_05.png" alt="My french startup : investisseur du mois">
                </div>
            </div>
        </div>
    </div>
</div>