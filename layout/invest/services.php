<div class="hpSection hpSection--services">
    <div class="container">
        <h2>Les services que tout investisseur attend. Sans attendre.</h2>
        <div class="carousel" data-flickity='{ "groupCells": true, "contain": true, "pageDots": false, "prevNextButtons": false}'>
            <div class="carousel-cell">
                <div class="services__col">
                    <div class="services__icon">
                        <img src="<?=IMG_PATH;?>home/icons/speaker.svg" alt="">
                    </div>
                    <div class="services__title">Dealflow</div>
                    <div class="services__desc">Accédez à une sélection exclusive de Startups en recherche de fonds, déclarée et prévisionnelle.</div>
                </div>
            </div>
            <div class="carousel-cell">
                <div class="services__col">
                    <div class="services__icon">
                        <img src="<?=IMG_PATH;?>home/icons/money.svg" alt="">
                    </div>
                    <div class="services__title">Analyses de deals</div>
                    <div class="services__desc">Suivez et analysez les deals et les trends de vos marchés, grâce à plus de 7 ans d’historique data.</div>
                </div>
            </div>
            <div class="carousel-cell">
                <div class="services__col">

                    <div class="services__icon">
                        <img src="<?=IMG_PATH;?>home/icons/aim.svg" alt="">
                    </div>
                    <div class="services__title">Veille marché</div>
                    <div class="services__desc">Configurez votre veille, recevez les deals du jour, personnalisez votre monitoring et vos alertes.</div>
                </div>
            </div>
        </div>
        <div class="services__cta">
            <a href="<?php echo URL ?>/recherche-startups" title="" class="btn btn-primary">Démarrer</a>
        </div>
    </div>
</div>