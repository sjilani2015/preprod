<div class="hpSection hpSection--peopleMap">
    <div class="container">
        <div class="peopleMap">
            <div class="peopleMap__title">Votre territoire est ici </div>
            <div class="peopleMap__subtitle">
                <p>Retrouvez tout l’écosystème de votre région.<br>Commencez à travailler</p>
                <br />
                <a href="https://www.myfrenchstartup.com/recherche-startups" class="btn btn-primary">Échangez</a>
            </div>

            <div class="peopleMap__people peopleMap__people--1">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?= IMG_PATH; ?>home/people/PUB3.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Quelles sont les startups les plus avancées et les moins financées de mon territoire ?
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--2">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?= IMG_PATH; ?>home/people/PUB4.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Comment identifier et faire échanger des communautés technologiques similaires ?
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--3">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?= IMG_PATH; ?>home/people/PUB5.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Comment construire un tableau de bord et évaluer trimestriellement les tendances comparatives de mon territoire ?
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--4">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?= IMG_PATH; ?>home/people/PUB6.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Où positionner les prochains incubateurs ?
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--5">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?= IMG_PATH; ?>home/people/PUB7.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Comment mettre en place un flux d’information sectoriel national pour chaque famille de startups sur mon territoire ?
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>