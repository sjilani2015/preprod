<div class="hpSection hpSection--demo">
    <div class="decor-bgdiag"></div>
    <div class="container">
        <div class="blocDemo">
            <div class="blocDemo__container">
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__1">Pensé pour les acteurs publics et parapublics </div>

                    </div>
                    <div class="blocDemo__title__2">Figurez au coeur de l'action</div>
                    <p class="blocDemo__intro">Identifiez les startups de votre territoire. Rendez-les visibles. Positionnez-les, grâce à nos comparables, analyses big data et angles de vues variés.</p>

                    <div class="blocDemo__title__2">Tous les fonds, investisseurs privés et institutionnels</div>
                    <p class="blocDemo__intro">Grâce à 8 ans de données actualisées, découvrez les niveaux d’investissement et les choix des financeurs de votre région.</p>

                    <div class="blocDemo__title__2">Étudiez les données de votre région</div>
                    <p class="blocDemo__intro">Réalisez des études poussées sur votre écosystème territorial. Identifiez les clés d’attractivité et les synergies.</p>

                </div>
                <div class="blocDemo__screen">
                    <img class="lazyload" src="<?= IMG_PATH; ?>home/screens/screen_01_secteurPublic.png" data-src="<?= IMG_PATH; ?>home/screens/screen_01.png" alt="My french startup : screenshot d'utilisation 1 de l'outil">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="hpSection hpSection--demo">
    <div class="container">
        <div class="blocDemo">
            <div class="blocDemo__container">
                <div class="blocDemo__screen">
                    <img class="lazyload" src="<?= IMG_PATH; ?>home/screens/screen_02_secteurPublic.png" data-src="<?= IMG_PATH; ?>home/screens/screen_04.png" alt="My french startup : screenshot d'utilisation 4 de l'outil">
                </div>
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__1">Le meilleur de la data</div>
                        <div class="blocDemo__title__2">Une data room qualifiée et validée chaque jour</div>
                    </div>
                    <p class="blocDemo__intro">Des données enrichies en temps réel : toutes les actualités et les mises à jour, données open data françaises, croisées, normalisées, validées par nos soins.</p>
                    <ul class="blocDemo__list">
                        <li>Retrouvez chaque aventure et tendance de l’innovation made in France.</li>
                        <li>Data crunching par géographie, secteurs et métiers. L’intégralité des indicateurs et des dynamiques spécifiques comparées.</li>
                        <li>Toutes les positions des fonds, investisseurs privés et institutionnels. Analyse de leurs engagements et priorités.</li>
                    </ul>
                    <a href="<?php echo URL ?>/recherche-startups" class="btn btn-primary btn-caret-right">Démarrer</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hpSection hpSection--demo">
    <div class="decor-bgdiag"></div>
    <div class="container">
        <h2>La région du mois</h2>
        <div class="blocDemo blocDemo--startupoftheweek">
            <div class="blocDemo__container">
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__2">Région Pays de la Loire</div>
                    </div>
                    <p>La région Pays de la Loire réunit 538 startups, implantées pour 83% d’entre elles en Loire-Atlantique. Leur nombre a augmenté de 1% entre septembre 2020 et octobre 2021. L’ensemble a contribué à la création de 8 800 emplois. Paradoxalement, la région réunit 3,5% des startups françaises mais ne capte que 1,2% des financements, en relation avec la jeunesse relative de son écosystème.
                        Le secteur le plus représenté est le service aux entreprises, pour 20% d’entre elles, qui a retenu 32% des investissements des 12 derniers mois. 
                        On remarque toujours sur les 12 derniers mois une forte création de plus de 20% dans les secteurs Santé, Biotech, Chimie et SSII-NTIC, ainsi qu’un niveau de financement dédié proche de 140 M€ en 12 mois.
                    </p>
                </div>
                <div class="blocDemo__screen">
                    <img class=" lazyload" src="<?= IMG_PATH; ?>home/screens/screen_03_secteurPublic.png" data-src="<?= IMG_PATH; ?>home/screens/screen_05.png" alt="My french startup : la startup de la semaine">
                </div>
            </div>
        </div>
    </div>
</div>