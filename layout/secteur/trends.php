<div class="hpSection hpSection--trends">
    <div class="decor-bgdiag"></div>
    <div class="container">
        <h2>Tendances fortes, signaux faibles</h2>
        <p class="hpSection__subtitle">Monitorez l’évolution de votre secteur en temps réel.  Paramétrez vos alertes marchés, exploitez nos scores, indicateurs et anticipations.</p>
        <div class="carousel cards cards--md" data-flickity='{ "groupCells": true, "contain": true, "pageDots": false, "prevNextButtons": false}'>
            <div class="carousel-cell">
                <div class="carousel-cell__content cards__bloc">
                    <div class="cards__bloc__icon">
                        <div class="cards__bloc__icon__wrapper">
                            <img src="<?= IMG_PATH; ?>icons/suitcase.svg" alt="">
                        </div>
                    </div>
                    <div class="cards__bloc__infos">
                        Secteur<span>Services aux entreprises</span>
                    </div>
                    <?php
                    $tendance = mysqli_fetch_array(mysqli_query($link, "select * from tendances where id=1"));
                    ?>
                    <div class="cards__bloc__raise">
                        Nombre de levées <span>30j</span>
                        <div class="cards__bloc__raise__iconUp"><?php echo $tendance['lf_services_entreprises_30']; ?>
                            <?php
                            if ($tendance['evo_lf_services_entreprises'] == -1) {
                                ?>
                                <span class="ico-decrease"></span>
                                <?php
                            }
                            ?>
                            <?php
                            if ($tendance['evo_lf_services_entreprises'] == 1) {
                                ?>
                                <span class="ico-raise"></span>
                                <?php
                            }
                            ?>
                            <?php
                            if ($tendance['evo_lf_services_entreprises'] == 0) {
                                ?>
                                <span class="ico-stable"></span>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="cards__bloc__small">Moy. France <strong><?php echo str_replace(",0", "", number_format($tendance['moy_fr_secteur'], 1, ",", "")); ?></strong></div>
                    <div class="cards__bloc__small mtop">
                        <a href="<?php echo URL ?>/etude-secteur/5234/Services_aux_entreprises" title="" class="btn btn-pinky btn-suitcase">Fiche de secteur</a>
                    </div>
                </div>
            </div>
            <div class="carousel-cell">
                <div class="carousel-cell__content cards__bloc">
                    <div class="cards__bloc__icon">
                        <div class="cards__bloc__icon__wrapper">
                            <img src="<?= IMG_PATH; ?>icons/pin.svg" alt="">
                        </div>
                    </div>
                    <div class="cards__bloc__infos">
                        Région<span>Ile de france</span>
                    </div>
                    
                    <div class="cards__bloc__raise">
                        Nombre de levées <span>30j</span>
                        <div class="cards__bloc__raise__iconUp">
                            <?php echo $tendance['lf_ile_de_france_30']; ?>
                            <?php
                            if ($tendance['evo_lf_ile_de_france'] == -1) {
                                ?>
                                <span class="ico-decrease"></span>
                                <?php
                            }
                            ?>
                            <?php
                            if ($tendance['evo_lf_ile_de_france'] == 1) {
                                ?>
                                <span class="ico-raise"></span>
                                <?php
                            }
                            ?>
                            <?php
                            if ($tendance['evo_lf_ile_de_france'] == 0) {
                                ?>
                                <span class="ico-stable"></span>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="cards__bloc__small">Moy. France <strong><?php echo str_replace(",0", "", number_format($tendance['moy_fr_region'], 1, ",", "")); ?></strong></div>
                    <div class="cards__bloc__small mtop">
                        <a href="<?php echo URL; ?>/region/5212/ile__de__France" title="" class="btn btn-pinky btn-pin">Fiche de la région</a>
                    </div>
                </div>
            </div>
            <div class="carousel-cell">
                <div class="carousel-cell__content cards__bloc">
                    <div class="cards__bloc__icon">
                        <div class="cards__bloc__icon__wrapper">
                            <img src="<?= IMG_PATH; ?>icons/rising.svg" alt="">
                        </div>
                    </div>
                    <div class="cards__bloc__infos">
                        Fond<span>BPI France</span>
                    </div>
                    
                    <div class="cards__bloc__raise">
                        Nombre de levées <span>30j</span>
                        <div class="cards__bloc__raise__iconUp"><?php echo $tendance['lf_bpi_30']; ?>
                        <?php
                            if ($tendance['evo_lf_bpi'] == -1) {
                                ?>
                                <span class="ico-decrease"></span>
                                <?php
                            }
                            ?>
                            <?php
                            if ($tendance['evo_lf_bpi'] == 1) {
                                ?>
                                <span class="ico-raise"></span>
                                <?php
                            }
                            ?>
                            <?php
                            if ($tendance['evo_lf_bpi'] == 0) {
                                ?>
                                <span class="ico-stable"></span>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="cards__bloc__small">Moy. France <strong><?php echo str_replace(",0", "", number_format($tendance['moy_fr_fond'], 1, ",", "")); ?></strong></div>
                    <div class="cards__bloc__small mtop">
                        <a href="<?php echo URL; ?>/startups-investisseur/5630/bpifrance" title="" class="btn btn-pinky btn-rising">Fiche des fonds</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>