<div class="hpSection hpSection--services">
    <div class="container">
        <h2>Les services que vous attendez sans attendre</h2>
        <div class="carousel" data-flickity='{ "groupCells": true, "contain": true, "pageDots": false, "prevNextButtons": false}'>
            <div class="carousel-cell">
                <div class="services__col">
                    <div class="services__icon">
                        <img src="<?=IMG_PATH;?>home/icons/speaker.svg" alt="">
                    </div>
                    <div class="services__title">Communiquez</div>
                    <div class="services__desc">Toute la data startups de votre région. Développez votre plateforme de communication startups.</div>
                </div>
            </div>
            <div class="carousel-cell">
                <div class="services__col">
                    <div class="services__icon">
                        <img src="<?=IMG_PATH;?>home/icons/money.svg" alt="">
                    </div>
                    <div class="services__title">Analysez</div>
                    <div class="services__desc">Réalisez des études régionales comparées et évolutives grâce aux nombreux filtres sur tous types de données.</div>
                </div>
            </div>
            <div class="carousel-cell">
                <div class="services__col">

                    <div class="services__icon">
                        <img src="<?=IMG_PATH;?>home/icons/aim.svg" alt="">
                    </div>
                    <div class="services__title">Collaborez</div>
                    <div class="services__desc">Valorisez votre territoire, gagnez en visibilité et faites des données un outil d’attractivité pour votre écosystème.</div>
                </div>
            </div>
        </div>
        <div class="services__cta">
            <a href="<?php echo URL ?>/recherche-startups" title="" class="btn btn-primary">Démarrer</a>
        </div>
    </div>
</div>