<div class="hpSection hpSection--services">
    <div class="container">
        <h2>Les services que vous attendez sans attendre</h2>
        <div class="carousel" data-flickity='{ "groupCells": true, "contain": true, "pageDots": false, "prevNextButtons": false}'>
            <div class="carousel-cell">
                <div class="services__col">
                    <div class="services__icon">
                        <img src="<?=IMG_PATH;?>home/icons/speaker.svg" alt="">
                    </div>
                    <div class="services__title">Communiquez</div>
                    <div class="services__desc">Créez et actualisez votre fiche startup. Valorisez votre projet auprès de tout l’écosystème.</div>
                </div>
            </div>
            <div class="carousel-cell">
                <div class="services__col">
                    <div class="services__icon">
                        <img src="<?=IMG_PATH;?>home/icons/money.svg" alt="">
                    </div>
                    <div class="services__title">Financez</div>
                    <div class="services__desc">Donnez des repères à vos futurs partenaires financiers, industriels ou institutionnels.</div>
                </div>
            </div>
            <div class="carousel-cell">
                <div class="services__col">

                    <div class="services__icon">
                        <img src="<?=IMG_PATH;?>home/icons/aim.svg" alt="">
                    </div>
                    <div class="services__title">Identifiez</div>
                    <div class="services__desc">Positionnez-vous en connaissance de cause. Identifiez vos prochains partenaires et concurrents.</div>
                </div>
            </div>
        </div>
        <div class="services__cta">
            <a href="<?php echo URL ?>/recherche-startups" title="" class="btn btn-primary">Démarrer</a>
        </div>
    </div>
</div>