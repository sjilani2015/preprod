<div class="hpSection hpSection--peopleMap">
    <div class="container">
        <div class="peopleMap">
            <div class="peopleMap__title">Vous y êtes déjà !</div>
            <div class="peopleMap__subtitle">
                <p>Retrouvez tout l’écosystème au complet.<br>Commencez à interagir.</p>
                <br />
                <a href="https://www.myfrenchstartup.com/recherche-startups" class="btn btn-primary">Échangez</a>
            </div>

            <div class="peopleMap__people peopleMap__people--1">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/STARTUP1.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Je dois être prévenue quand une nouvelle startup se lance sur mon secteur
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--2">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/STARTUP2.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Il me faut une perception plus claire sur les secteurs qui tirent le marché
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--3">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/people_3.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        J’ai besoin de contacter un des fondateurs
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--4">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/people_4.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Quels sont les acteurs en vue sur ma région ?
                    </div>
                </div>
            </div>
            <div class="peopleMap__people peopleMap__people--5">
                <div class="peopleMap__wrapper">
                    <div class="peopleMap__avatar">
                        <img src="<?=IMG_PATH;?>home/people/STARTUP3.png" alt="" width="170">
                    </div>
                    <div class="peopleMap__bubble">
                        Qui cibler pour ma recherche de fonds ?
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>