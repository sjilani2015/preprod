<div class="hpSection hpSection--fundraising">
    <div class="container">
        <div style="text-align: center;padding-top: 30px;margin-bottom: 40px;"><a href="https://www.myfrenchstartup.com/etude-dealflow" target="_blank"><img  src="https://www.myfrenchstartup.com/martech.png" alt="" /></a></div>

        <h2>Dernières levées de fonds</h2>
        <p class="hpSection__subtitle">Chaque mouvement de marché en temps réel. Actualités normalisées, vérifiées ou validées. Idéal pour vous évaluer.</p>
        <div class="carousel cards" data-flickity='{"groupCells": true, "contain": true, "prevNextButtons": false}'>

            <?php
            $sql_lf_list = mysqli_query($link, "select startup.nom,lf.montant,startup.id from startup inner join lf on lf.id_startup=startup.id where startup.status=1 and lf.rachat=0 order by lf.date_ajout desc limit 6 offset 0")or die(mysqli_error($link));
            while ($datas = mysqli_fetch_array($sql_lf_list)) {
                $secteurs = mysqli_fetch_array(mysqli_query($link, "Select  secteur.nom_secteur,secteur.id,secteur.nom_secteur_en From   secteur Inner Join  activite    On activite.secteur = secteur.id Where  activite.id_startup =" . $datas['id']));
                ?>

                <div class="carousel-cell">
                    <div class="carousel-cell__content cards__bloc">
                        <div class="cards__bloc__title"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($datas['id']) . "/" . urlWriting(strtolower($datas["nom"])) ?>"><?php echo stripslashes($datas["nom"]); ?></a></div>
                        <div class="cards__bloc__amount"><?php
                                                if ($datas['montant'] != 0) {
                                                    echo str_replace(",0", "", number_format($datas['montant'] / 1000, 1, ",", ""));
                                                    ?>M€ <?php } else echo "NC"; ?></div>
                        <div class="cards__bloc__small"><?php echo ($secteurs['nom_secteur']); ?></div>
                    </div>
                </div>
                <?php
            }
            ?>
            
        </div>
    </div>
</div>