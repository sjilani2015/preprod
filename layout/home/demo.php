<div class="hpSection hpSection--demo">
    <div class="decor-bgdiag"></div>
    <div class="container">
        <div class="blocDemo">
            <div class="blocDemo__container">
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__1">Pensé pour les startups</div>
                        <div class="blocDemo__title__2">Figurez au cœur de l’action</div>
                    </div>
                    <p class="blocDemo__intro">Donnez de la visibilité à votre startup. Positionnez-là avec pertinence, grâce à nos comparables de tendances, analyses big data et angles de vues multiples.</p>
                    <ul class="blocDemo__list">
                        <li>Donnez de la visibilité à votre startup. Positionnez-là avec pertinence, grâce à nos comparables de tendances, analyses big data et angles de vues multiples.</li>
                        <li>Avec 8 ans de données actualisées et normalisées, découvrez les participations et les choix de vos financeurs potentiels.</li>
                        <li>Inscrivez votre startup gratuitement Renseignez et actualisez le profil de votre entreprise ou de votre projet.</li>
                        <li>Captez l’attention, annoncez vos recherches de fonds.</li>
                    </ul>
                    <a href="<?php echo URL ?>/ajouter-ma-startup" class="btn btn-primary btn-caret-right">J'ajoute ma startup</a>
                </div>
                <div class="blocDemo__screen">
                    <img class="lazyload" src="<?=IMG_PATH;?>home/screens/screen_01.png" data-src="<?=IMG_PATH;?>home/screens/screen_01.png" alt="My french startup : screenshot d'utilisation 1 de l'outil">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hpSection hpSection--demo">
    <div class="container">
        <div class="blocDemo">
            <div class="blocDemo__container">
                <div class="blocDemo__screen">
                    <img class="lazyload" src="<?=IMG_PATH;?>home/screens/screen_02.png" data-src="<?=IMG_PATH;?>home/screens/screen_02.png" alt="My french startup : screenshot d'utilisation 2 de l'outil">
                </div>
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__1">Pensé pour les investisseurs</div>
                        <div class="blocDemo__title__2">Surveillez les startups, les  secteurs et l'innovation</div>
                    </div>
                    <ul class="blocDemo__list">
                        <li>Sélectionnez vos opportunités, avec nos indicateurs et scores : maturité, historique de financement, secteur/marché, comparables…</li>
                        <li>Accédez à une sélection exclusive de startups en quête de fonds.</li>
                        <li>Bénéficiez d'analyses sectorielles et retrouvez l’historique des deals.</li>
                        <li>Gagnez en visibilité. Attirez les startups sensibles à vos caractéristiques.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hpSection hpSection--demo">
    <div class="decor-bgdiag"></div>
    <div class="container">
        <div class="blocDemo">
            <div class="blocDemo__container">
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__1">Pensé pour les corporate</div>
                        <div class="blocDemo__title__2">Naviguez dans l’écosystème et identifiez vos futurs partenaires</div>
                    </div>
                    <ul class="blocDemo__list">
                        <li>Au-delà des chiffres, identifiez les innovations émergentes.  Diffusez vos appels à projets pour nouer des partenariats avec les startups qui transformeront votre activité.</li>
                        <li>Découvrez les startups de tout un secteur ou marché.</li>
                        <li>Rendez visible le dynamisme de votre groupe et sa culture de l’innovation.</li>
                    </ul>
                </div>
                <div class="blocDemo__screen">
                    <img class="lazyload" src="<?=IMG_PATH;?>home/screens/screen_03.png" data-src="<?=IMG_PATH;?>home/screens/screen_03.png" alt="My french startup : screenshot d'utilisation 3 de l'outil">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hpSection hpSection--demo">
    <div class="container">
        <div class="blocDemo">
            <div class="blocDemo__container">
                <div class="blocDemo__screen">
                    <img class="lazyload" src="<?=IMG_PATH;?>home/screens/screen_04.png" data-src="<?=IMG_PATH;?>home/screens/screen_04.png" alt="My french startup : screenshot d'utilisation 4 de l'outil">
                </div>
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__1">Le meilleur de la data</div>
                        <div class="blocDemo__title__2">Une data room qualifiée et validée chaque jour</div>
                    </div>
                    <p class="blocDemo__intro">Des données enrichies en temps réel : toutes les actualités, mises à jour par les fondateurs, données open data françaises, croisées, normalisées et validées par nos soins.</p>
                    <ul class="blocDemo__list">
                        <li>Retrouvez chaque aventure et tendance de l’innovation made in France.</li>
                        <li>Data crunching par secteurs et métiers en référent pour tous les indicateurs avec les dynamiques spécifiques et les consolidations.</li>
                        <li>Toutes les positions des fonds, investisseurs privés et institutionnels avec l’analyse de leurs sensibilités.</li>
                    </ul>
                    <a href="<?php echo URL  ?>/recherche-startups" class="btn btn-primary btn-caret-right">Démarrer</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hpSection hpSection--demo">
    <div class="decor-bgdiag"></div>
    <div class="container">
        <h2>La startup de la semaine</h2>
        <div class="blocDemo blocDemo--startupoftheweek">
            <div class="blocDemo__container">
                <div class="blocDemo__content">
                    <div class="blocDemo__title">
                        <div class="blocDemo__title__2">ALAN</div>
                    </div>
                    <p class="blocDemo__intro">L’assurance santé qui fait simple.</p>
                    <p><strong>Secteur</strong><br>Avec 21 startups de l’Assurtech et 181 M€ levés sur les 6 derniers mois, le secteur reste concentré et moins concurrentiel que les autres (54 startups : moyenne par secteur dans l’écosystème français)</p>
                    <p><strong>Innovation</strong><br>3 startups créées sur le même marché depuis 12 mois, 6 fois moins de création dans ce secteur que dans le reste de l’écosystème français.</p>
                    <a href="<?php echo URL; ?>/fr/startup-france/157122/alan" class="btn btn-primary btn-caret-right">Étudier cette startup</a>
                </div>
                <div class="blocDemo__screen">
                    <img class=" lazyload" src="<?=IMG_PATH;?>home/screens/screen_05.png" data-src="<?=IMG_PATH;?>home/screens/screen_05.png" alt="My french startup : la startup de la semaine">
                </div>
            </div>
        </div>
    </div>
</div>