<div class="hpSection hpSection--lateststartups">
    <div class="decor-bgdiag"></div>
    <div class="container">
        <h2>Nouvelles Startups</h2>
        <p class="hpSection__subtitle">Entrepreneurs, investisseurs, acteurs publics : affinez vos décisions, avec nos indicateurs actualisés et nos études secteurs. Gratuitement.</p>
        <p class="hpSection__cta">
            <a href="https://www.myfrenchstartup.com/ajouter-ma-startup" class="btn btn-primary">Actualiser ma startup</a>
        </p>
        <div class="carousel cards cards--md" data-flickity='{ "groupCells": true, "contain": true, "prevNextButtons": false}'>
            <?php
            $ss = mysqli_query($link, "select * from startup where status=1 and siret!='' order by creation desc,id desc limit 4");
            while ($last_startup = mysqli_fetch_array($ss)) {
                $secteurs = mysqli_fetch_array(mysqli_query($link, "Select  secteur.nom_secteur,secteur.id,secteur.nom_secteur_en From   secteur Inner Join  activite On activite.secteur = secteur.id Where  activite.id_startup =" . $last_startup['id']));
                $secteur = $secteurs['nom_secteur'];
                if ($secteur == "Restauration, Cuisine, Alimentation")
                    $secteur = "Restauration, Cuisine";
                ?>
                <div class="carousel-cell">
                    <div class="carousel-cell__content cards__bloc">
                        <div class="cards__bloc__thumb"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $last_startup['logo']) ?>" alt=""></div>
                        <div class="cards__bloc__title"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($last_startup['id']) . "/" . urlWriting(strtolower($last_startup["nom"])) ?>" target="_blank"><?php echo $last_startup['nom'] ?></a></div>
                        <div class="cards__bloc__note"><?php echo $last_startup['creation'] ?></div>
                        <div class="cards__bloc__small"><?php echo ($secteur); ?></div>
                        <div class="cards__bloc__small"><?php echo ($last_startup['short_fr']) ?> <a href="<?php echo URL . '/fr/startup-france/' . generate_id($last_startup['id']) . "/" . urlWriting(strtolower($last_startup["nom"])) ?>" title="">Voir plus</a></div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>