<header class="header" id="header">
    <div class="header__wrapper" id="header-wrapper">
        <div class="burger" id="toggleMainMenu">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <?php
        $mon_user = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));
        $sql_notif = mysqli_query($link, "select * from notifications inner join user_save_list on user_save_list.id=notification.liste where notifications.user=" . $mon_user['id'] . " order by dt desc limit 3");
        $nb_notif = mysqli_num_rows($sql_notif);
        if($nb_notif=='')
            $nb_notif=0;
        ?>
        <div class="logo">
            <a href="<?= URL; ?>" title="MyFrenchStartup">
                <img src="<?= URL; ?>/static/images/logos/logo.svg?1" alt="<?= SITENAME; ?>" width="170">
            </a>
        </div>

        <div class="searchbar">
            <input class="searchbar__field form-control" onkeyup="chercher()" id="search-box" type="text" value="" placeholder="Trouver une startup, un investisseur..." name="searchbar" />
            <span class="searchbar__btn">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.6 44.3"><path d="M24.3 38.6C13.6 38.6 5 29.9 5 19.3 5 8.7 13.6 0 24.3 0c10.6 0 19.3 8.7 19.3 19.3C43.6 29.9 34.9 38.6 24.3 38.6zM24.3 5.2c-7.8 0-14.1 6.3-14.1 14.1 0 7.8 6.3 14.1 14.1 14.1 7.8 0 14.1-6.3 14.1-14.1C38.4 11.5 32 5.2 24.3 5.2z"></path><path d="M3.2 44.3c-0.8 0-1.6-0.3-2.3-0.9 -1.3-1.3-1.3-3.3 0-4.5l8.2-8.2c1.3-1.3 3.3-1.3 4.5 0 1.3 1.3 1.3 3.3 0 4.5l-8.2 8.2C4.9 44 4 44.3 3.2 44.3z"></path></svg>
            </span>
            <div id="suggesstion-box" style="position: absolute;background: #fff;font-weight: 100;border: none;border-radius: 5px;padding-right: 25px;width: 100%;text-align: left;z-index: 99999999; top:47px"></div>
        </div>

        <div class="searchbarIcon" id="toggleSearchbar">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.6 44.3"><path d="M24.3,38.6C13.6,38.6,5,29.9,5,19.3S13.6,0,24.3,0c10.6,0,19.3,8.7,19.3,19.3S34.9,38.6,24.3,38.6z M24.3,4.2c-7.8,0-15.1,7.3-15.1,15.1s7.3,15.1,15.1,15.1s15.1-7.3,15.1-15.1S32,4.2,24.3,4.2z"/><path d="M3.2,44.3c-0.8,0-1.6-0.3-2.3-0.9c-1.3-1.3-1.3-3.3,0-4.5l8.2-8.2c1.3-1.3,3.3-1.3,4.5,0c1.3,1.3,1.3,3.3,0,4.5l-8.2,8.2C4.9,44,4,44.3,3.2,44.3z"/></svg>
        </div>

        <div class="notifications" data-dropdown="notifications-dd">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M439.4,362.3c-19.3-20.8-55.5-52-55.5-154.3c0-77.7-54.5-139.9-127.9-155.2V32c0-17.7-14.3-32-32-32s-32,14.3-32,32v20.8 C118.6,68.1,64.1,130.3,64.1,208c0,102.3-36.2,133.5-55.5,154.3C2.6,368.7,0,376.4,0,384c0.1,16.4,13,32,32.1,32h383.8 c19.1,0,32-15.6,32.1-32C448,376.4,445.4,368.7,439.4,362.3L439.4,362.3z M224,512c35.3,0,64-28.6,64-64H160 C160,483.4,188.7,512,224,512z"></path></svg>
            <span class="notifications__nb"><?php echo $nb_notif; ?></span>
            <div class="dropdown" id="notifications-dd">
                <div class="dropdown__title">Notifications</div>
                <div class="dropdown__content">
                    <?php
                    while ($data_notif = mysqli_fetch_array($sql_notif)) {

                        $ma_list = mysqli_fetch_array(mysqli_query($link, "select * from user_save_list where id=" . $data_notif['liste']));
                        ?>
                        <div class="notifications__row">
                            <strong>Mise à jour</strong>
                            <br>
                                Mise à jour de votre liste <?php echo $ma_list['nom']; ?>
                                <div class="notifications__row__action">
                                    <a href="#">Ignorer</a>
                                    <a href="<?php echo URL; ?>/recherche-startups/list/<?php echo generate_id($ma_list['id']); ?>">Détails</a>
                                </div>
                        </div>
    <?php
}
?>

                </div>
            </div>
        </div>
        <div class="user" data-dropdown="userinfos-dd">
            <div class="user__infos">
                <div class="user__name"><?php echo $mon_user['prenom'] . " " . $mon_user['nom']; ?></div>
<?php
if ($mon_user['premium'] == 1) {
    ?>
                    <div class="user__subscription">Abonné premium</div>
                    <?php
                } else {
                    ?>
                    <div class="user__subscription">Abonné gratuit</div>
                    <?php
                }
                ?>
            </div>
            <div class="user__avatar">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 729 592.6">
                    <path class="st0" d="M363.5,338.7c92.8-0.1,168-75.3,168.1-168.1c0-92.8-75.3-168.1-168.1-168.1S195.4,77.8,195.4,170.6 S270.7,338.7,363.5,338.7z M363.5,36.2c74.2,0,134.3,60.2,134.4,134.4c0,74.2-60.2,134.4-134.4,134.4s-134.4-60.2-134.4-134.4 S289.3,36.2,363.5,36.2z"></path>
                    <path class="st0" d="M628.3,567.4c-10-30.5-27.1-58.3-50-80.9c-34.9-35.7-100.1-78.1-212.8-78.6h-2.4 c-217.9,1-262.3,157.9-262.8,159.5c-2.1,8.7,3,17.5,11.6,20.1c8.9,2.7,18.3-2.4,20.9-11.3c1.5-5.5,39.2-134.3,231.5-134.6 c192,0.4,229.8,129,231.4,134.6c1.9,7.3,8.6,12.4,16.2,12.4c1.6,0,3-0.2,4.5-0.6C625.4,585.6,630.7,576.4,628.3,567.4z"></path>
                </svg>
            </div>
            <div class="dropdown" id="userinfos-dd">
                <div class="dropdown__content">
                    <ul>
                        <li><a href="<?php echo URL ?>/mon-compte" title="Mon compte">Mon compte</a></li>
                        <li><a href="<?php echo URL ?>/ajouter-ma-startup" title="Ajouter ma startup">Ajouter ma startup</a></li>
                        <li><a href="<?php echo URL ?>/nos-offres" title="Ajouter ma startup">Nos offres</a></li>
                        <li><a href="<?php echo URL ?>/deconnexion" title="Déconnexion">Déconnexion</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>