
<tr>
    <th class="table-search__startup">
        <div class="startupInfos">
            <div class="startupInfos__image">
                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($sups['id']) . "/" . urlWriting(strtolower($sups["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $sups['logo']) ?>" alt=""></a>
            </div>
            <div class="startupInfos__desc">
                <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($sups['id']) . "/" . urlWriting(strtolower($sups["nom"])) ?>" target="_blank"><?php echo (stripslashes($sups['nom'])); ?></a></div>
                <div class="companyLabel"><?php echo ($ssh); ?></div>
            </div>
        </div>
    </th>
    <td class="table-search__marche">
        <div class="table-search__link small"><a href="#" onclick="afficher_cible('<?php echo str_replace(",", "_", $sups['cible']) ?>')"><?php echo ($sups['cible']) ?></a> - <a href="#" onclick="afficher_par_secteur(<?php echo $secteurs['id'] ?>, '<?php echo urlWriting($secteurs['nom_secteur']) ?>')"><?php echo ($secteurs['nom_secteur']) ?></a></div>
        <div class="table-search__link small"><a href="#" onclick="afficher_par_ssecteur(<?php echo $ssecteurs['id']; ?>, '<?php echo urlWriting($ssecteurs['nom_sous_secteur']) ?>')"><?php echo ($ssecteurs['nom_sous_secteur']) ?></a></div>
    </td>
    <td class="table-search__maturite center">
        <?php if ($chiffre['maturite'] == $ligne_secteur_activite['moy_maturite']) { ?>
            <span class="ico-stable"></span> 
        <?php } ?>
        <?php if ($chiffre['maturite'] > $ligne_secteur_activite['moy_maturite']) { ?>
            <span class="ico-raise"></span> 
        <?php } ?>
        <?php if ($chiffre['maturite'] < $ligne_secteur_activite['moy_maturite']) { ?>
            <span class="ico-decrease"></span> 
        <?php } ?>
        <?php echo str_replace(".", ",", $chiffre['maturite']) ?></td>
    <td class="table-search__totalfondsleves center"><?php
        if ($lf_somme['somme'] != '') {
            $ssm = ($lf_somme['somme'] + 0) / 1000;
            echo str_replace(",0", "", number_format($ssm + 0, 1, ",", " "));
            echo "&nbsp;M€";
        } else {
            echo "-";
        }
        ?></td>
    <?php
    $get_id_region = mysqli_fetch_array(mysqli_query($link, "select * from region_new where region_new='" . addslashes($sups['region_new']) . "'"));
    $nb_tour = mysqli_fetch_array(mysqli_query($link, "select tour_serie from startup where id=" . addslashes($sups['id'])));
    ?>
    <td class="table-search__region center">
        <div class="table-search__link small"><a href="#" onclick="afficher_par_ville(<?php echo generate_id($get_id_region['id']); ?>, '<?php echo urlWriting($sups['region_new']); ?>')"><?php echo ($sups['region_new']) ?></a></div>
    </td>
    <td class="table-search__traction center">
        <?php if ($chiffre['traction_likedin'] == $ligne_secteur_activite['moy_traction_likedin']) { ?>
            <span class="ico-stable"></span> 
        <?php } ?>
        <?php if ($chiffre['traction_likedin'] > $ligne_secteur_activite['moy_traction_likedin']) { ?>
            <span class="ico-raise"></span> 
        <?php } ?>
        <?php if ($chiffre['traction_likedin'] < $ligne_secteur_activite['moy_traction_likedin']) { ?>
            <span class="ico-decrease"></span> 
        <?php } ?> <?php
        if ($chiffre['traction_likedin'] != "0") {
            echo str_replace(".", ",", $chiffre['traction_likedin']);
        } else
            echo '-';
        ?></td>
    <td class="table-search__date center"><?php echo $sups['creation']; ?></td>
    <td class="table-search__ville center small"><?php echo (ucfirst(strtolower($sups['ville']))) ?></td>

    <td class="table-search__position center"><?php echo $status; ?></td>
    <td class="table-search__nblevees center"><?php echo $nb_tour['tour_serie']; ?></td>
    <?php
    if ($lf_somme['somme'] != '') {
        $sql_derniere_leve = mysqli_fetch_array(mysqli_query($link, "select * from lf where id_startup=" . $sups['id'] . " order by date_ajout desc limit 1 "));
        $derniere_leve = $sql_derniere_leve['montant'];
        $derniere_date = $sql_derniere_leve['date_ajout'];
    } else {
        $derniere_leve = "-";
        $derniere_date = "-";
    }
    ?>
    <td class="table-search__totaldernierelevee center">
        <?php
        echo str_replace(",0", "", number_format($derniere_leve / 1000, 1, ",", " "));
        ?>
        M€</td>
    <td class="table-search__datedernierelevee center"><?php echo change_date_fr_chaine_related($derniere_date); ?></td>
    <?php
    $sql_derniere_capital = mysqli_fetch_array(mysqli_query($link, "select * from startup_capital where id_startup=" . $sups['id'] . " order by dt desc limit 1 "));
    $derniere_capital_date = $sql_derniere_capital['dt'];
    ?>
    <td class="table-search__datedernieremodificationcapital center"><?php echo change_date_fr_chaine_related($derniere_capital_date); ?></td>
    <td class="table-search__effectif center"> <?php echo $sups['rang_effectif']; ?></td>
    <td class="table-search__competitionsecteur center"><span class="ico-raise"></span> <?php if ($ligne_secteur_activite['competition_secteur'] != "")
        echo str_replace(".", ",", $ligne_secteur_activite['competition_secteur']);
    else
        echo "-";
    ?>/10</td>
    <td class="table-search__modeleeconomique center">B2B</td>
    <td class="table-search__capaciteadelivrer center"><span class="ico-raise"></span> 6,4/10</td>
    <td class="table-search__derniereactualite center">4 sem.</td>
    <td class="table-search__tags">
<?php echo stripslashes($tagss) . "<span title='" . utf8_encode($sups['concat_tags']) . "'>...</span>"; ?>
    </td>
</tr>