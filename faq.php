<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Foire aux questions - <?= SITENAME; ?></title>
        <meta name="description" content="Foire aux questions <?= SITENAME; ?>">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php include ('layout/header-simple.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">

                    <div class="edito">
                        <div class="page-title"><h1>FAQ</h1></div>
                        <div class="page-intro">Une question avant de démarrer ? Vous trouverez ici les réponses aux questions les plus fréquemment posées par notre communauté d'entrepreneurs.</div>

                        <div class="faq">
                            <div class="faq__title">
                                <h2>Est-ce qu'ajouter sa startup est payant ?</h2>
                            </div>
                            <div class="faq__answer">
                                <p>Le référencement des startups est totalement gratuit et le sera toujours. Il suffit de créer un compte et d'ajouter votre startup via votre espace personnel.</p>
                            </div>
                        </div>

                        <div class="faq">
                            <div class="faq__title">
                                <h2>Est-ce que l'offre levée de fonds est payante ?</h2>
                            </div>
                            <div class="faq__answer">
                                Non ! Nous ne facturons pas la mise en relation des startups avec les investisseurs. Il suffit de renseigner le formulaire de recherche de fonds et vous serez directement sollicités par les investisseurs.
                            </div>
                        </div>
                        <div class="faq">
                            <div class="faq__title">
                                <h2>Je veux mettre à jour ma fiche.</h2>
                            </div>
                            <div class="faq__answer">
                                Pour prendre la main sur votre fiche startup, il suffit de faire une demande de liaison via votre espace personnel. Un modérateur la validera et vous pourrez par la suite la mettre à jour via ce même espace.
                            </div>
                        </div>
                        <div class="faq">
                            <div class="faq__title">
                                <h2>Est-ce que le recrutement de stagiaires est payant ?</h2>
                            </div>
                            <div class="faq__answer">
                                Non ! Il suffit de renseigner votre offre et nous réalisons mensuellement des emailings pour communiquer sur les offres de stages auprès de notre communauté de stagiaires.
                            </div>
                        </div>
                        <div class="faq">
                            <div class="faq__title">
                                <h2>Comment diffuser un communiqué de presse ?</h2>
                            </div>
                            <div class="faq__answer">
                                C'est très simple ! Remplissez le formulaire via votre espace personnel et votre article sera visible sur votre fiche ainsi que sur l'espace dédié aux communiqués de presse.
                            </div>
                        </div>
                        <div class="faq">
                            <div class="faq__title">
                                <h2>Ma startup n'existe plus, comment l'indiquer ?</h2>
                            </div>
                            <div class="faq__answer">
                                Si vous souhaitez que votre startup n'apparaissent plus ou bien qu'elle soit désinscrite, il suffit de nous envoyer un email via notre formulaire de contact et nous ferosn le nécessaire.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/animated.min.js"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>