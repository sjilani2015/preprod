<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (isset($_SESSION['data_login'])) {
    $users = mysqli_fetch_array(mysqli_query($link, "select * from user where email='" . $_SESSION['data_login'] . "'"));
}


$sql100 = mysqli_query($link, "select lf.id from lf inner join startup on lf.id_startup=startup.id where lf.rachat=0 and startup.status=1 and startup.sup_radie=0");
$sql1000 = mysqli_num_rows($sql100);

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}
?>
<div class="searchHeader">
    <div class="searchHeader__title">10 startups affichées / <?php echo $sql1000 ?> startups sélectionnées</div>

    <div class="searchHeader__actions">
        <button class="btn btn-sm btn-bordered btn-searchfilters btn-filter">Filtrer</button>

        <a href="<?php echo URL ?>/levees-fonds" class="btn btn-sm btn-primary btn-reset">Réinitialiser</a>
    </div>
</div>


<div class="searchFilters">
    <div class="searchFilters__tags tags tags--left"></div>
    <div class="searchFilters__tableOpt"></div>
</div>
<div class="table-responsive">
    <table class="table table-search" id="mine">
        <thead>
            <tr>
                <th class="table-search__startup">Startup</th>
                <th class="table-search__ranking text-center">Montant levé</th>
                <th class="table-search__participations text-center">Date levé</th>
                <th class="table-search__yearInvests text-center">Investisseurs</th>
                <th class="table-search__ticketmoy text-center">Série</th>
                <th class="table-search__secteursInvests text-center">Total levé</th>

            </tr>
        </thead>
        <tbody>
            <?php
            $_SESSION['ma_base_url'] = $monUrl;
            $_SESSION['ma_requete'] = "select * from startup inner join lf on startup.id=lf.id_startup where startup.status=1 and lf.rachat=0 group by startup.id order by startup.id desc limit 10";
            $sql100 = mysqli_query($link, "select startup.id,lf.id as idf from startup inner join lf on startup.id=lf.id_startup where startup.status=1 and lf.rachat=0 group by startup.id order by startup.id desc limit 10");
            $nbr_result = mysqli_num_rows($sql100);
            $_SESSION['nbr_result'] = $nbr_result;
            $_SESSION['list_id'] = "";


            while ($sups1 = mysqli_fetch_array($sql100)) {
                $sups = mysqli_fetch_array(mysqli_query($link, "Select * From   startup Where  id =" . $sups1['id']));
                $lfs = mysqli_fetch_array(mysqli_query($link, "Select * From   lf Where  id =" . $sups1['idf']));
                $secteurs = mysqli_fetch_array(mysqli_query($link, "Select secteur.nom_secteur,secteur.id,secteur.nom_secteur_en From   secteur Inner Join  activite    On activite.secteur = secteur.id Where  activite.id_startup =" . $sups['id']));
                $ssecteurs = mysqli_fetch_array(mysqli_query($link, "Select  sous_secteur.nom_sous_secteur,sous_secteur.id,sous_secteur.nom_sous_secteur_en From   sous_secteur Inner Join  activite    On activite.sous_secteur = sous_secteur.id Where  activite.id_startup =" . $sups['id']));
                $tags_list = mysqli_fetch_array(mysqli_query($link, "Select  activite.tags From  activite  Where  activite.id_startup =" . $sups['id']));
                $lf_somme = mysqli_fetch_array(mysqli_query($link, "Select  sum(montant+0) as somme From  lf inner join startup on lf.id_startup=startup.id  Where startup.status=1 and lf.rachat=0 and startup.id =" . $sups['id']));
                $lf_nb = mysqli_fetch_array(mysqli_query($link, "Select  count(*) as nb From  lf inner join startup on lf.id_startup=startup.id  Where startup.status=1 and lf.rachat=0 and startup.id =" . $sups['id']));
                $sup_ca = mysqli_fetch_array(mysqli_query($link, "Select  startup_ca.ca,startup_ca.annee From  startup_ca inner join startup on startup_ca.id_startup=startup.id  Where startup.status=1 and  startup.id =" . $sups['id'] . " order by annee desc limit 1"));
                $ch = "";
                $ssh = '';
                $tabs1 = ($sups['short_fr']);
                $bnrs = strlen(($sups['short_fr']));
                for ($o = 0; $o < 50; $o++) {
                    $ssh .= $tabs1[$o];
                }
                if ($bnrs >= 50) {
                    $ssh = $ssh . "...";
                }
                ?>


                <tr>
                    <th class="table-search__startup">
                        <div class="startupInfos">
                            <div class="startupInfos__image">
                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($sups['id']) . "/" . urlWriting(strtolower($sups["nom"])) ?>" target="_blank"><img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $sups['logo']) ?>" alt=""></a>
                            </div>
                            <div class="startupInfos__desc">
                                <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($sups['id']) . "/" . urlWriting(strtolower($sups["nom"])) ?>" target="_blank"><?php echo (stripslashes($sups['nom'])); ?></a></div>
                                <div class="companyLabel"><?php echo ($ssh); ?></div>
                            </div>
                        </div>
                    </th>
                    <td class="table-search__ranking text-center">
                        <?php echo str_replace(",0", "", number_format($lfs['montant'] / 1000, 1, ",", " ")); ?> M€
                    </td>
                    <td class="table-search__participations text-center">
                        <?php echo $lfs['date_ajout']; ?>
                    </td>
                    <td class="table-search__yearInvests text-center">
                        <?php echo strtoupper(str_replace(",", ", ", $lfs['de'])); ?>
                    </td>
                    <td class="table-search__ticketmoy text-center">
                        <?php echo $sups['tour_serie']; ?>
                    </td>
                    <td class="table-search__secteursInvests text-center">
                        <?php echo str_replace(",0", "", number_format($lf_somme['somme'] / 1000, 1, ",", " ")); ?> M€
                    </td>
                </tr>

                <?php
            }
            ?>
        </tbody>
    </table>
</div>