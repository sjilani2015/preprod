<?php
include('db.php');
$menu = 100;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Etude des deals</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="../backofficemyfs/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="../backofficemyfs/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../backofficemyfs/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="../backofficemyfs/assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../backofficemyfs/assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="../backofficemyfs/assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="../backofficemyfs/css/datepicker.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="../backofficemyfs/global_assets/js/main/jquery.min.js"></script>
        <script src="../backofficemyfs/global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="../backofficemyfs/global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="../backofficemyfs/global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="../backofficemyfs/global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="../backofficemyfs/global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="../backofficemyfs/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="../backofficemyfs/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="../backofficemyfs/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="../backofficemyfs/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="../backofficemyfs/global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="../backofficemyfs/global_assets/js/plugins/pickers/daterangepicker.js"></script>

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style>
            .datepicker{
                z-index: 1200 !important
            }
            .barre_modif{
                position:fixed;
                bottom:30px;
                width:100%;
            }
        </style>
    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->
        <?php
        if (isset($_GET['idm'])) {
            mysqli_query($link, "update levees_sources set supp=1 where id=" . $_GET['idm']);
            echo '<script>window.location="levee.php"</script>';
        }
        ?>

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">


                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Statistiques</h6>
                                </div>

                                <!-- Numbers -->
                                <div class="card-body py-0">

                                    <div class="col-md-12">

                                        <form method="post" action="">
                                            <div class="row">
                                                <div class="col-lg-1">Date début</div>
                                                <div class="col-lg-4"><input type="date" name="cal1"  autocomplete="off" class=" form-control"></div>

                                                <div class="col-lg-1">Date Fin</div>

                                                <div class="col-lg-4"><input type="date" name="cal2" autocomplete="off" class=" form-control"></div>

                                                <div class="col-lg-2"><button type="submit" class="btn btn-primary">
                                                        <i class="icon-plus-circle2 position-left"></i> Chercher</button>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                        if (isset($_POST['cal1'])) {


                                            $date_deb = $_POST['cal1'];
                                            $date_fin = $_POST['cal2'];

                                            $nombre_total_lf = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on lf.id_startup=startup.id WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.ipo=0"));
                                            $montant_total_lf = mysqli_fetch_array(mysqli_query($link, "SELECT sum(lf.montant) as somme FROM `lf` inner join startup on lf.id_startup=startup.id WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.ipo=0"));
                                            ?>
                                            <div class="row" style="margin-top: 50px;"> <hr style="clear: both"></div>

                                            <h2 style="text-align: center">Resultat entre le <?php echo $date_deb ?> et le <?php echo $date_fin; ?></h2>

                                            <div class="row" style="margin-top: 50px;"> <hr style="clear: both"></div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Année</th>
                                                                <th>Semestre</th>
                                                                <th>Nombre total des levées de fonds</th>
                                                                <th>Montant total levé</th>
                                                                <th>Nombre de Startup rachetées</th>
                                                                <th>Nombre de Startup IPO</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            for ($i = 2010; $i <= date('Y'); $i++) {

                                                                $oo1 = mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on lf.id_startup=startup.id WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $i . "-01-01' and lf.date_ajout<='" . $i . "-06-30' and lf.ipo=0")or die(mysql_error());
                                                                $sql_annee1 = mysqli_fetch_array($oo1);
                                                                $oo2 = mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on lf.id_startup=startup.id WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $i . "-07-01' and lf.date_ajout<='" . $i . "-12-31' and lf.ipo=0")or die(mysql_error());
                                                                $sql_annee2 = mysqli_fetch_array($oo2);
                                                                $oo3 = mysqli_query($link, "SELECT sum(lf.montant) as somme FROM `lf` inner join startup on lf.id_startup=startup.id WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $i . "-01-01' and lf.date_ajout<='" . $i . "-06-30' and lf.ipo=0")or die(mysql_error());
                                                                $sql_annee3 = mysqli_fetch_array($oo3);
                                                                $oo4 = mysqli_query($link, "SELECT sum(lf.montant) as somme FROM `lf` inner join startup on lf.id_startup=startup.id WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $i . "-07-01' and lf.date_ajout<='" . $i . "-12-31' and lf.ipo=0")or die(mysql_error());
                                                                $sql_annee4 = mysqli_fetch_array($oo4);
                                                                $oo5 = mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on lf.id_startup=startup.id WHERE startup.status=1 and lf.rachat=1 and lf.date_ajout>='" . $i . "-01-01' and lf.date_ajout<='" . $i . "-06-30'")or die(mysql_error());
                                                                $sql_annee5 = mysqli_fetch_array($oo5);
                                                                $oo6 = mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on lf.id_startup=startup.id WHERE startup.status=1 and lf.rachat=1 and lf.date_ajout>='" . $i . "-07-01' and lf.date_ajout<='" . $i . "-12-31'")or die(mysql_error());
                                                                $sql_annee6 = mysqli_fetch_array($oo6);
                                                                $oo7 = mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on lf.id_startup=startup.id WHERE startup.status=1 and lf.rachat=2 and lf.date_ajout>='" . $i . "-01-01' and lf.date_ajout<='" . $i . "-06-30'")or die(mysql_error());
                                                                $sql_annee7 = mysqli_fetch_array($oo7);
                                                                $oo8 = mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on lf.id_startup=startup.id WHERE startup.status=1 and lf.rachat=2 and lf.date_ajout>='" . $i . "-07-01' and lf.date_ajout<='" . $i . "-12-31'")or die(mysql_error());
                                                                $sql_annee8 = mysqli_fetch_array($oo8);
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $i; ?></td>
                                                                    <td>S1</td>
                                                                    <td><?php echo $sql_annee1['nb']; ?></td>
                                                                    <td><?php echo $sql_annee3['somme']; ?> K€</td>
                                                                    <td><?php echo $sql_annee5['nb']; ?></td>
                                                                    <td><?php echo $sql_annee7['nb']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><?php echo $i; ?></td>
                                                                    <td>S2</td>
                                                                    <td><?php echo $sql_annee2['nb']; ?> </td>
                                                                    <td><?php echo $sql_annee4['somme']; ?> K€</td>
                                                                    <td><?php echo $sql_annee6['nb']; ?></td>
                                                                    <td><?php echo $sql_annee8['nb']; ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 50px;"> <hr style="clear: both"></div>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Nombre total des levées de fonds : <span><?php echo $nombre_total_lf['nb']; ?></span></h4>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Montant total levé : <span><?php echo $montant_total_lf['somme']; ?> K€</span></h4>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Secteur</th>
                                                                <th>Nombre</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $sql_secteur = mysqli_query($link, "SELECT count(*) as nb,secteur.nom_secteur,secteur.id FROM `lf` inner join startup on lf.id_startup=startup.id inner join activite on activite.id_startup=startup.id inner join secteur on secteur.id=activite.secteur WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "'  and lf.ipo=0 group by secteur.nom_secteur ORDER BY `nb` DESC")or die(mysqli_error($link));
                                                            while ($data_secteur = mysqli_fetch_array($sql_secteur)) {
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php echo utf8_encode($data_secteur['nom_secteur']); ?>
                                                                        <hr>
                                                                        <?php
                                                                        $sql_ssecteur = mysqli_query($link, "SELECT count(*) as nb,sous_secteur.nom_sous_secteur FROM `lf` inner join startup on lf.id_startup=startup.id inner join activite on activite.id_startup=startup.id inner join sous_secteur on sous_secteur.id=activite.sous_secteur WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "'  and lf.ipo=0 and activite.secteur=" . $data_secteur['id'] . " group by sous_secteur.nom_sous_secteur ORDER BY `nb` DESC")or die(mysqli_error($link));
                                                                        while ($data_ssecteur = mysqli_fetch_array($sql_ssecteur)) {
                                                                            ?>
                                                                            <div><?php echo utf8_encode($data_ssecteur['nom_sous_secteur']) ?> : <?php echo $data_ssecteur['nb'] ?> </div>    
                                                                            <?php
                                                                        }
                                                                        ?>


                                                                    </td>
                                                                    <td><?php echo $data_secteur['nb']; ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Secteur</th>
                                                                <th>Montant (K€)</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $sql_secteur_montant = mysqli_query($link, "SELECT sum(lf.montant) as somme,secteur.nom_secteur,secteur.id FROM `lf` inner join startup on lf.id_startup=startup.id inner join activite on activite.id_startup=startup.id inner join secteur on secteur.id=activite.secteur WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.ipo=0 group by secteur.nom_secteur ORDER BY `somme` DESC")or die(mysqli_error($link));
                                                            while ($data_secteur_montant = mysqli_fetch_array($sql_secteur_montant)) {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo utf8_encode($data_secteur_montant['nom_secteur']); ?>
                                                                        <hr>
                                                                        <?php
                                                                        $sql_ssecteur_montant = mysqli_query($link, "SELECT sum(lf.montant) as somme,sous_secteur.nom_sous_secteur FROM `lf` inner join startup on lf.id_startup=startup.id inner join activite on activite.id_startup=startup.id inner join sous_secteur on sous_secteur.id=activite.sous_secteur WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.ipo=0 and activite.secteur=" . $data_secteur_montant['id'] . " group by sous_secteur.nom_sous_secteur ORDER BY `somme` DESC")or die(mysqli_error($link));
                                                                        while ($data_ssecteur_montant = mysqli_fetch_array($sql_ssecteur_montant)) {
                                                                            ?>
                                                                            <div><?php echo utf8_encode($data_ssecteur_montant['nom_sous_secteur']) ?> : <?php echo $data_ssecteur_montant['somme'] ?> </div>    
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                    <td><?php echo $data_secteur_montant['somme']; ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <?php
                                            $round_A = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.tour_serie='1'"));
                                            $round_B = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.tour_serie='2'"));
                                            $round_C = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.tour_serie='3'"));
                                            $round_D = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.tour_serie='4'"));
                                            $round_E = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.tour_serie='5'"));
                                            $round_F = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.tour_serie='6'"));
                                            $round_G = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.tour_serie='7"));
                                            $round_H = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.tour_serie='8'"));
                                            $round_I = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.tour_serie='9'"));
                                            ?>
                                            <div class="row" style="margin-top: 50px;"> <hr style="clear: both"></div>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Nombre de Startup Round A : <span><?php echo $round_A['nb']; ?></span></h4>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Nombre de Startup Round B : <span><?php echo $round_B['nb']; ?></span></h4>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Nombre de Startup Round C : <span><?php echo $round_C['nb']; ?></span></h4>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Nombre de Startup Round D : <span><?php echo $round_D['nb']; ?></span></h4>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Nombre de Startup Round E : <span><?php echo $round_E['nb']; ?></span></h4>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Nombre de Startup Round F : <span><?php echo $round_F['nb']; ?></span></h4>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Nombre de Startup Round G : <span><?php echo $round_G['nb']; ?></span></h4>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Nombre de Startup Round H : <span><?php echo $round_H['nb']; ?></span></h4>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Nombre de Startup Round I : <span><?php echo $round_I['nb']; ?></span></h4>

                                            <div class="row" style="margin-top: 50px;"> <hr style="clear: both"></div>


                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Startup</th>
                                                        <th>Montant (K€)</th>
                                                        <th>Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $top_startup = mysqli_query($link, "SELECT startup.nom,lf.montant,lf.date_ajout,startup.id FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.ipo=0 order by lf.montant desc limit 10");
                                                    while ($data_top_lf = mysqli_fetch_array($top_startup)) {
                                                        ?>
                                                        <tr>
                                                            <td><a href="<?php echo 'https://www.myfrenchstartup.com/fr/startup-france/' . generate_id($data_top_lf['id']) . "/" . urlWriting(strtolower($data_top_lf["nom"])) ?>" target="_blank"><?php echo $data_top_lf['nom']; ?></a></td>
                                                            <td><?php echo $data_top_lf['montant']; ?> K€</td>
                                                            <td><?php echo $data_top_lf['date_ajout']; ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>

                                            <div class="row" style="margin-top: 50px;"> <hr style="clear: both"></div>


                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Montant</th>
                                                        <th>Nombre</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $taille1 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.montant!=0 and lf.montant!='' and lf.montant<'500' and lf.ipo=0 order by lf.montant desc "));
                                                    $taille2 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.montant!=0 and lf.montant!=''  and lf.montant>='500' and lf.ipo=0 and lf.montant<'1000' order by lf.montant desc "));
                                                    $taille3 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.montant!=0 and lf.montant!=''  and lf.montant>='1000' and lf.ipo=0 and lf.montant<'10000' order by lf.montant desc "));
                                                    $taille4 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.montant!=0 and lf.montant!=''  and lf.montant>='10000' and lf.ipo=0 and lf.montant<'100000' order by lf.montant desc "));
                                                    $taille5 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.montant!=0 and lf.montant!=''  and lf.montant>='100000' and lf.ipo=0 order by lf.montant desc "));
                                                    ?>
                                                    <tr>
                                                        <td><500k</td>
                                                        <td><?php echo $taille1['nb']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>>500k à 999k</td>
                                                        <td><?php echo $taille2['nb']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>>1M à 9,9M</td>
                                                        <td><?php echo $taille3['nb']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>>10M à 99M</td>
                                                        <td><?php echo $taille4['nb']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>>100M</td>
                                                        <td><?php echo $taille5['nb']; ?></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                            <div class="row" style="margin-top: 50px;"> <hr style="clear: both"></div>


                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Effectifs</th>
                                                        <th>Nombre</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $effectif1 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.effectif>='1' and startup.effectif<='3' and lf.ipo=0"));
                                                    $effectif2 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.effectif>'3' and startup.effectif<='10' and lf.ipo=0"));
                                                    $effectif3 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.effectif>'10' and startup.effectif<='50' and lf.ipo=0"));
                                                    $effectif4 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.effectif>'50' and startup.effectif<='100' and lf.ipo=0"));
                                                    $effectif5 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.effectif>'100' and lf.ipo=0"));
                                                    ?>
                                                    <tr>
                                                        <td>de 1 à 3</td>
                                                        <td><?php echo $effectif1['nb']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>de 4 à 10</td>
                                                        <td><?php echo $effectif2['nb']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>de 11 à 50</td>
                                                        <td><?php echo $effectif3['nb']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>de 51 à 100</td>
                                                        <td><?php echo $effectif4['nb']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>>100</td>
                                                        <td><?php echo $effectif5['nb']; ?></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                            <div class="row" style="margin-top: 50px;"> <hr style="clear: both"></div>


                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Age</th>
                                                        <th>Nombre</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $age1 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.age>=1 and startup.age<=2 and lf.ipo=0"));
                                                    $age2 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.age>=3 and startup.age<=5 and lf.ipo=0"));
                                                    $age3 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.age>=6 and startup.age<=10 and lf.ipo=0"));
                                                    $age4 = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and startup.age>10 and lf.ipo=0"));
                                                    ?>
                                                    <tr>
                                                        <td>de 0 à 2</td>
                                                        <td><?php echo $age1['nb']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>de 3 à 5</td>
                                                        <td><?php echo $age2['nb']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>de 5 à 10</td>
                                                        <td><?php echo $age3['nb']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>>10</td>
                                                        <td><?php echo $age4['nb']; ?></td>
                                                    </tr>


                                                </tbody>
                                            </table>

                                            <?php
                                            $rachat = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=1 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.ipo=0"));
                                            $ipo = mysqli_fetch_array(mysqli_query($link, "SELECT count(*) as nb FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=2 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.ipo=0"));
                                            ?>
                                            <div class="row" style="margin-top: 50px;"> <hr style="clear: both"></div>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Nombre de Startup rachetées : <span><?php echo $rachat['nb']; ?></span></h4>
                                            <h4 style="padding-left: 20px; padding-top: 5px;">Nombre de Startup IPO : <span><?php echo $ipo['nb']; ?></span></h4>





                                        </div>
                                        <div class="row" style="margin-top: 50px;"> <hr style="clear: both"></div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Région</th>
                                                            <th>Nombre</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $sql_region = mysqli_query($link, "SELECT count(*) as nb,startup.region_new FROM `lf` inner join startup on lf.id_startup=startup.id  WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.ipo=0 group by startup.region_new ORDER BY `nb` DESC")or die(mysql_error());
                                                        while ($data_region = mysqli_fetch_array($sql_region)) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo utf8_encode($data_region['region_new']); ?></td>
                                                                <td><?php echo $data_region['nb']; ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Région</th>
                                                            <th>Nombre</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $sql_region = mysqli_query($link, "SELECT count(*) as nb,startup.region FROM `lf` inner join startup on lf.id_startup=startup.id  WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.ipo=0 group by startup.region ORDER BY `nb` DESC")or die(mysql_error());
                                                        while ($data_region = mysqli_fetch_array($sql_region)) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo utf8_encode($data_region['region']); ?></td>
                                                                <td><?php echo $data_region['nb']; ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-6" style="margin-top: 30px">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Cible</th>
                                                            <th>Nombre</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $top_cible = mysqli_query($link, "SELECT count(*) as nb,startup.cible FROM `lf` inner join startup on startup.id=lf.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.ipo=0 group by startup.cible order by nb desc");
                                                        while ($data_cible = mysqli_fetch_array($top_cible)) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo utf8_encode($data_cible['cible']); ?></td>
                                                                <td><?php echo $data_cible['nb']; ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-6" style="margin-top: 30px">
                                                 <?php
                                                        $top_femme = mysqli_query($link, "SELECT startup.id FROM `lf` inner join startup on startup.id=lf.id_startup inner join personnes on startup.id=personnes.id_startup WHERE startup.status=1 and lf.rachat=0 and lf.date_ajout>='" . $date_deb . "' and lf.date_ajout<='" . $date_fin . "' and lf.ipo=0 and personnes.civilite=1 group by personnes.id_startup");
                                                      $nb_startup= mysqli_num_rows($top_femme);
                                                            ?>
                                                Nombre de startups ayant une Femme : <?php echo $nb_startup;  ?>
                                            </div>
                                        </div>

                                        <?php
                                    }
                                    ?>


                                </div>

                            </div>
                        </div>
                        <!-- Latest posts -->







                    </div>


                </div>
                <!-- /dashboard content -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->


    <!-- Footer -->
    <div class="navbar navbar-expand-lg navbar-light">
        <div class="text-center d-lg-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                <i class="icon-unfold mr-2"></i>
                Footer
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-footer">
            <span class="navbar-text">
                &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
            </span>
        </div>
    </div>
    <script src="js/bootstrap-datepicker.js"></script>
    <script>

        $(document).ready(function () {
            //jq162 = jQuery.noConflict(true);


            $(function () {
                window.prettyPrint && prettyPrint();
                $('#dp1').datepicker({
                    format: 'mm-dd-yyyy'
                });
            });
            $(function () {
                window.prettyPrint && prettyPrint();
                $('#dp2').datepicker({
                    format: 'mm-dd-yyyy'
                });
            });


            $(function () {
                $("#datepicker").datepicker({dateFormat: 'dd/mm/yy'});
                $("#datepicker1").datepicker({dateFormat: 'dd/mm/yy'});
                $("#datepicker2").datepicker({dateFormat: 'dd/mm/yy'});
                $("#datepicker3").datepicker({dateFormat: 'dd/mm/yy'});
                $("#datepicker4").datepicker({dateFormat: 'dd/mm/yy'});
            });

            $(function () {
                $("#geocomplete").geocomplete({
                    map: ".map_canvas",
                    details: "form",
                    types: ["geocode", "establishment"],
                });

                $("#find").click(function () {
                    $("#geocomplete").trigger("geocode");
                });
            });




        });
    </script>
    <script>

        $().ready(function () {
            window.prettyPrint && prettyPrint();

            refrech_datepicker();

        });


        function refrech_datepicker() {
            $(".datepicker").each(function () {
                $(this).datepicker({
                    format: 'mm-dd-yyyy'
                });


            });

        }
    </script>
</body>


</html>
