<div class="page-header page-header-dark bg-indigo">



    <div class="navbar navbar-expand-md navbar-dark bg-indigo border-0 shadow-0">
        <div class="d-md-none w-100">
            <button type="button" class="navbar-toggler d-flex align-items-center w-100" data-toggle="collapse" data-target="#navbar-navigation">
                <i class="icon-menu-open mr-2"></i>
                Main navigation
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-navigation">
            <ul class="nav navbar-nav navbar-nav-material">

                <li class="nav-item">
                    <a href="index.php" class="navbar-nav-link <?php if ($menu == 100) echo 'active'; ?>">
                        <i class="icon-cash3 mr-2"></i>
                        Etude des deals
                    </a>
                </li>
                <li class="nav-item">
                    <a href="keyword.php" class="navbar-nav-link <?php if ($menu == 100) echo 'active'; ?>">
                        <i class="icon-cash3 mr-2"></i>
                        Recherche par keyword    
                    </a>
                </li>

            </ul>


        </div>
    </div>
    <!-- /secondary navbar -->



</div>