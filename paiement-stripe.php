<?php
//include("include/db.php");
include("functions/functions.php");
include ('config.php');
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Offre payante - <?= SITENAME; ?></title>
        <meta name="description" content="Foire aux questions <?= SITENAME; ?>">
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php include ('layout/header-simple.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">
                    <div class="section-title section-title--fat">
                        Premium
                        <span>Une offre simple qui grandit en même temps que vous</span>
                    </div>
                    <div class="paiement-stripe">
                        <div class="paiement-stripe__item">
                            <div class="paiement-stripe__item__title">
                                Récapitulatif de votre commande
                            </div>
                            <div class="paiement-stripe__item__pricing">
                                89<span class="curr">€</span>
                            </div>
                            <div class="paiement-stripe__item__pricingDetails">
                                Par mois / Par utilisateur
                            </div>
                            <div class="paiement-stripe__item__headline">
                                Une offre simple qui grandit en même temps<br />que vous
                            </div>
                            <ul class="formules__item__arg">
                                <li>Dashboard personnalisé</li>
                                <li>Recherches illimitées</li>
                                <li>100 résultats par recherche</li>
                                <li>Alertes personnalisées</li>
                                <li>Accès dealflow</li>
                                <li>Abonnement individuel</li>
                            </ul>
                        </div>
                        <div class="paiement-stripe__item">
                            <div class="paiement-stripe__item__title">
                                Paiement sécurisé via Stripe
                            </div>
                            <div class="paiement-stripe__item__form">
                                <form id="" name="" action="" method="post">
                                    <div class="form-group">
                                        <label>Titulaire de la carte</label>
                                        <input type="text" value="" name="" placeholder="" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>N° de carte</label>
                                        <input type="text" value="" name="" placeholder="" class="form-control" />
                                    </div>
                                    <div class="formgrid formgrid--2col">
                                        <div class="formgrid__item">
                                            <div class="formgrid formgrid--2col">
                                                <div class="formgrid__item">
                                                    <div class="form-group">
                                                        <label>Mois</label>
                                                        <input type="text" value="" name="" maxlength="2" placeholder="MM" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="formgrid__item">
                                                    <div class="form-group">
                                                        <label>Année</label>
                                                        <input type="text" value="" maxlength="2" placeholder="YY" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="formgrid__item">
                                            <div class="form-group">
                                                <label>CVC</label>
                                                <input type="text" maxlength="3" value="" name="" placeholder="CVC" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="paiement-stripe__item__cta">
                                        <a href="<?php echo URL ?>" class="btn btn-primary">Procéder au paiement (89€)</a>
                                    </div>
                                    <div class="paiement-stripe__item__secureImg">
                                        <img src="images/logo-stripe-secure.png?1" alt="" width="180" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/animated.min.js"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>