<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Formules - <?= SITENAME; ?></title>
        <meta name="description" content="Foire aux questions <?= SITENAME; ?>">
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
        <style>
            @media screen and (min-width: 768px){

                .formules {
                    grid-template-columns: 1fr 1fr 1fr 1fr;
                }
            }
        </style>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">
                    <div class="section-title section-title--fat">
                        Tout l’écosystème des startups<br>françaises. Maintenant.
                    </div>
                    <div class="formules-intro">
                        Ne manquez pas ce qui se passe dans l’écosystème des Startups françaises.
                        Data en temps réel, analyses des tendances, maintenant disponibles pour tous.
                    </div>
                    <div class="formules">
                        <div class="formules__item">
                            <div class="formules__item__title">
                                Try free
                            </div>
                            <div class="formules__item__pricing">
                                0<span class="curr">€</span>
                            </div>
                            <div class="formules__item__pricingDetails">Sans CB</div>
                            <div class="formules__item__headline">
                                S'inscrire et tester gratuitement,<br>sans engagement !
                            </div>
                            <ul class="formules__item__arg">
                                <li>Les startups et leurs indicateurs</li>
                                <li>10 résultats par recherche</li>
                                <li>10 consultations de startups par jour</li>
                                <li>Filtres génériques</li>
                                <li>Editer ma startup</li>
                                <li>Etudes secteurs et régions</li>
                            </ul>


                            <div class="formules__item__cta">
                                <a href="<?php echo URL ?>/connexion" class="btn btn-primary">S'inscrire</a>
                            </div>
                        </div>
                        <div class="formules__item">
                            <div class="formules__item__title">
                                Premium
                            </div>
                            <div class="formules__item__pricing">
                                89<span class="curr">€</span>
                            </div>
                            <div class="formules__item__pricingDetails">Par mois / Par utilisateur</div>
                            <div class="formules__item__headline">
                                Une offre simple qui grandit en même<br>temps que vous
                            </div>
                            <ul class="formules__item__arg">
                                <li>Dashboard personnalisé</li>
                                <li>Recherches illimitées</li>
                                <li>100 résultats par recherche</li>
                                <li>Alertes personnalisées</li>
                                <li>Accès dealflow</li>
                                <li>Abonnement individuel</li>
                            </ul>


                            <div class="formules__item__cta">
                                <a href="<?php echo URL ?>/paiement-stripe" class="btn btn-primary">S'abonner</a>

                            </div>
                        </div>
                        <div class="formules__item">
                            <div class="formules__item__title">
                                Premium
                            </div>
                            <div class="formules__item__pricing">
                                900<span class="curr">€</span>
                            </div>
                            <div class="formules__item__pricingDetails">Par an / Par utilisateur</div>
                            <div class="formules__item__headline">
                                Une offre simple qui grandit en même<br>temps que vous
                            </div>
                            <ul class="formules__item__arg">
                                <li>Dashboard personnalisé</li>
                                <li>Recherches illimitées</li>
                                <li>100 résultats par recherche</li>
                                <li>Alertes personnalisées</li>
                                <li>Accès dealflow</li>
                                <li>Abonnement individuel</li>
                            </ul>


                            <div class="formules__item__cta">
                                <a href="<?php echo URL ?>/paiement-stripe" class="btn btn-primary">S'abonner</a>

                            </div>
                        </div>
                        <div class="formules__item">
                            <div class="formules__item__title">
                                Business
                            </div>
                            <div class="formules__item__pricing" style="font-size: 48px">
                                Sur devis
                            </div>
                            <div class="formules__item__pricingDetails">Nous contacter</div>
                            <div class="formules__item__headline">
                                Conçu pour les investisseurs,<br>les corporate et les institutionnels.
                            </div>
                            <ul class="formules__item__arg">
                                <li>Etudes de marché à la carte</li>
                                <li>Analyses d'écosystèmes</li>
                                <li>API data</li>
                                <li>Abonnement multi-utilisateurs Entreprise</li>

                            </ul>


                            <div class="formules__item__cta">
                                <a href="<?php echo URL ?>/contact" class="btn btn-primary">Nous contacter</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/animated.min.js"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>