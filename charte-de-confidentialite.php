<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (isset($_GET['utm_source'])) {
    $_SESSION['utm_source'] = $_GET['utm_source'];
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="edito">

                    <div class="page-title"><h1>Charte de confidentialité myfrenchstartup.com</h1></div>
                        <p>Myfrenchstartup édite un site d'informations sur les startups françaises, accessible à l’adresse suivante <a href=" https://www.myfrenchstartup.com/"><u>https://www.myfrenchstartup.com/</u></a> (ci-après, le « <b>Site</b> »).</p>
                        <p>Les termes commençant par une majuscule renvoient aux définitions des Conditions Générales du Site.</p>
                        <p>Le Site permet aux cadres et dirigeants de startups, aux investisseurs, aux cadres d’entreprises ou du secteur public et à toute personne intéressée de consulter des informations relatives au développement des startups référencées et d’être tenue informé de l’actualité du secteur par le biais de de collecte d’informations en continu, de newsletters, de publications et d’articles (ci-après, les « <b>Services</b> »). Il permet aussi aux dirigeants de startups de mettre à jour les informations sur leur société de déposer des communiqués de presse ainsi que d’annoncer des recherches de levée de fonds.</p>
                        <p>Les données collectées par la Société Editrice sont collectées, enregistrées et stockées en conformité avec les dispositions de la loi relative à l’informatique, aux fichiers et aux libertés du 6 janvier 1978 dans sa version en vigueur à la date des présentes, ainsi qu’avec les dispositions du Règlement Général sur la Protection des Données (RGPD).</p>


                        <h2 class="sub_title">Données collectées et Finalités</h2>
                        <p>Les principales données collectées sur le Site sont les suivantes : nom, prénom, adresse email et numéro de téléphone, entreprise et fonction au sein de l’entreprise, centres d’intérêts, contenu des messages et commentaires, nature des Contenus téléchargés et des Services utilisés.</p>
                        <p>Le traitement des données collectées lors de l’utilisation des Services est nécessaire pour permettre la fourniture des services concernés, notamment la création du Compte, l’envoi de newsletter, la promotion d’une startup ainsi que la mise en relation entre investisseurs et dirigeants de startup et le téléchargement de livres blancs.</p>
                        <p>Si l’Utilisateur ne souhaite pas communiquer ces informations, la Société Editrice ne pourra pas fournir les Services concernés.</p>
                        <p>Lorsque la Société Editrice est amenée à publier des offres de stages et d’emplois, elle ne collecte pas elle-même les données issues des candidatures. Seuls les tiers à l’origine de ces offres collectent, traitent et accèdent aux données ainsi collectées.</p>



                        <h2 class="sub_title">Partage des données via le Site</h2>
                        <p>Lorsque l’utilisation des Services nécessite le transfert de données personnelles vers des tiers (fonds d’investissement, etc.), ceux-ci agissent en qualité de responsable de traitement des données ainsi collectées et transférées. Ils s’engagent à respecter les dispositions légales et réglementaires en matière de protection des données personnelles. Le détail de leurs politiques en matière de protection des données personnelles peut être consulté directement sur leurs sites Internet respectifs.</p>
                        <p>Par ailleurs, les Contenus et Services du Site ont été construits en partenariat avec les Partenaires de la Société qui peuvent les avoir financés.</p>
                        <p>L’accès à ces Contenus et Services peut se faire gratuitement, ou en contrepartie du paiement de la Contribution dans le cadre d’un Abonnement.</p>
                        <p>Si un Utilisateur souhaite accéder gratuitement auxdits Contenus, il autorise la Société à partager ses données avec ses Partenaires pour recevoir, dans un cadre professionnel, des offres en lien avec les Services choisis. L’accord de l’Utilisateur permet de continuer à lui proposer gratuitement des services et du contenu de qualité. Lorsque ses données sont communiquées aux Partenaires, elles le sont uniquement pour proposer des services en lien avec son activité professionnelle et ses centres d’intérêts, en relation avec les newsletters et évènements auxquels il est inscrit. Aucune communication n’est faite sans son accord exprès et sans qu’il n’en ait préalablement été informé.</p>
                        <p>L’Utilisateur qui ne souhaite pas que ses données soient partagées avec les Partenaires doit souscrire un <a href="https://www.myfrenchstartup.com/offre-payante" target="_blank">Abonnement payant</a>, donnant accès aux mêmes contenus, et payer la Contribution. Le détail et les modalités de la souscription des Abonnements sont accessibles depuis les Conditions Générales.</p>
                        <p>Lorsque l’Utilisateur choisit de payer la Contribution, les seuls destinataires des données personnelles collectées seront les personnes en charge de la gestion des Abonnements au sein de la Société.</p>

                        <h2 class="sub_title">Durée de conservation des données</h2>
                        <p>Lorsqu’un Utilisateur ouvre un Compte sur le Site, ses données et les données liées à son utilisation du Site sont conservées tant qu’il n’a pas résilié son Compte.</p>
                        <p>Après clôture du Compte, l’adresse email de l’Utilisateur concerné est cependant conservée tant qu’il n’a pas demandé à être désinscrit des listes de diffusion, afin de le tenir informé des évènements et des publications de la Société Editrice. Dans tous les cas, l’Utilisateur peut se désinscrire à tout moment en cliquant sur le lien présent dans les newsletters.</p>
                        <p>L’adresse email et le contenu des messages des personnes qui ont soumis une demande via le Site sont conservés pendant une durée de deux (2) ans à compter de l’envoi du message.</p>
                        <p>En cas de Compte inactif pendant une durée de vingt-quatre (24) mois consécutifs, les données relatives à l’utilisation du Compte par l’Utilisateur sont effacées par la Société Editrice.</p>
                        <p>Une fois ces délais écoulés, les données sont archivées et restituables exclusivement dans le cadre d’un contentieux, pendant la durée de la prescription légale. </p>
                        <p>Les données de fréquentation et de traçabilité des Utilisateurs et des Visiteurs sont conservées pour une durée de 13 mois à compter de la date du dépôt du cookie ou du traceur sur le terminal de la personne concernée.</p>
                        <p>Les données personnelles sont ensuite archivées et restituables exclusivement dans le cadre d’un contentieux et pendant la durée de la prescription légale.</p>
                        <p>La Société Editrice ne conserve pas les données de paiement.</p>

                        <h2 class="sub_title">Droit des personnes concernées</h2>
                        <p>Les personnes concernées disposent d’un droit d'accès, de modification, de rectification, d’effacement et, le cas échéant, de portabilité des données personnelles les concernant, conformément aux dispositions de la loi relative à l'informatique, aux fichiers et aux libertés du 6 janvier 1978 dans sa version en vigueur à la date des présentes, et conformément à la règlementation communautaire. </p>
                        <p>Le droit d’accès, de modification, de rectification et d’effacement prévu au paragraphe précédent, s’exerce auprès du service client à l’adresse email suivante : <u>privacy@myfrenchstartup.com</u>. </p>
                        <p>En cas de difficulté en lien avec la gestion de ses données personnelles, la personne concernée a le droit d’introduire une réclamation auprès de la CNIL ou auprès de toute autorité de contrôle compétente.</p>
                        <p>Par ailleurs, les Utilisateurs du Site comprennent et reconnaissent qu’en cas de transfert de leurs données vers un tiers, l’exercice de leurs droits afférents aux données transférées doit s’exercer directement auprès du tiers concerné.</p>


                        <h2 class="sub_title">Cookies</h2>
                        <p>Des cookies peuvent s’installer automatiquement sur votre logiciel de navigation ou votre terminal, lors de votre accès à notre Site. Un cookie est un élément qui ne permet pas d’identifier une personne mais sert à enregistrer des informations relatives à la navigation de celle-ci sur le site myfrenchstartup.com.</p>


                        <h2 class="sub_title">Quels sont les cookies susceptibles d’être installés lorsque vous visitez notre Site ?</h2>
                        <p>Les cookies susceptibles d’être installés sur votre navigateur lorsque vous visitez le Site de myfrenchstartup.com visent essentiellement à disposer d’outils de mesure d’audience, à améliorer l’ergonomie du Site et à déterminer le parcours des visiteurs.</p>


                        <h2 class="sub_title">Quelle est la durée de vie des cookies ?</h2>
                        <p>Il existe deux types de cookies : les cookies de session et les cookies persistants. Les cookies de session sont créés de manière temporaire dans le sous-dossier de votre navigateur au moment où vous visitez un site web. Une fois que vous quittez le Site, le cookie de session est supprimé.</p>
                        <p>Les fichiers du cookie persistants restent dans le sous-dossier de votre navigateur et ils sont réactivés une fois que vous consultez le site web qui a créé ce cookie spécifique. Un cookie persistant demeure dans le sous-dossier du navigateur pour la durée définie dans le fichier du cookie.</p>
                        <p>D’une manière générale, la durée de validité d’un cookie est de 13 mois maximum.</p>


                        <h2 class="sub_title">Puis-je refuser le dépôt de cookies sur mon ordinateur ?</h2>
                        <p>Le paramétrage de votre navigateur permet éventuellement de refuser les cookies selon la procédure décrite dans l’onglet « Option Internet » de votre navigateur. </p>
                        <p>Vous naviguez sur Internet avec le navigateur Microsoft Edge ou Internet Explorer, visitez
                            https://support.microsoft.com/fr-fr/kb/278835<br>
                            Vous naviguez sur Internet avec le navigateur Mozilla Firefox, visitez
                            https://support.mozilla.org/fr/kb/activer-desactiver-cookies<br>
                            Vous naviguez sur Internet avec le navigateur Google Chrome, visitez
                            https://support.google.com/chrome/answer/95647?hl=fr<br>
                            Vous naviguez sur Internet avec le navigateur Apple Safari, visitez
                            https://support.apple.com/fr-fr/HT201265<br>
                        </p>
                        <p>Les principaux émetteurs de cookies que nous utilisons sont les suivants : </p>
                        <div>
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th style="width: 170px!important;"><p style="margin-bottom: 0px!important;"><b>Nom du cookie</b></p></th>
                                    <th style="width: 190px!important;"><p style="margin-bottom: 0px!important;"><b>Finalité</b></p></th>
                                    <th><p style="margin-bottom: 0px!important;"><b>Désactivation</b></p></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><p style="margin-bottom: 0px!important;"><b>Google Analytics</b></p></td>
                                    <td><p style="margin-bottom: 0px!important;">Mesure d’audience</p></td>
                                    <td><p style="margin-bottom: 0px!important;">https://policies.google.com/technologies/managing?hl=fr</p></td>
                                </tr>

                                <tr>
                                </tr><tr>
                                    <td><p style="margin-bottom: 0px!important;"><b>Facebook</b></p></td>
                                    <td><p style="margin-bottom: 0px!important;">Mesure d’audience / suivi des connexions/ personnalisation des contenus proposés</p></td>
                                    <td><p style="margin-bottom: 0px!important;">https://fr-fr.facebook.com/policies/cookies/</p></td>
                                </tr>

                                <tr>
                                    <td><p style="margin-bottom: 0px!important;"><b>Twitter</b></p></td>
                                    <td><p style="margin-bottom: 0px!important;">Mesure d’audience / suivi des connexions/ personnalisation des contenus proposés</p></td>
                                    <td><p style="margin-bottom: 0px!important;">https://help.twitter.com/fr/using-twitter/tailored-suggestions#</p></td>
                                </tr>
                                <tr>
                                    <td><p style="margin-bottom: 0px!important;"><b>Linkedin</b></p></td>
                                    <td><p style="margin-bottom: 0px!important;">Mesure d’audience / suivi des connexions/ personnalisation des contenus proposés</p></td>
                                    <td><p style="margin-bottom: 0px!important;">https://www.linkedin.com/legal/cookie-policy?_l=fr_FR</p></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

         <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/animated.min.js"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectRegion(val) {
                const words = val.split('/');
                // $("#search-box").val(words[2]);
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>