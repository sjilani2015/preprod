<?php

include("../include/db.php");
include("../fonction/fonction.php");
header('Content-Type: text/xml');
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 10 Jul 1990 05:00:00 GMT");

$sql = mysqli_query($link, "select * from startup where status=1 and sup_radie=0 order by id desc");
$xml = '<?xml version="1.0" encoding="UTF-8"?>';
$xml .= '<urlset xmlns="http://www.google.com/schemas/sitemap/0.84">';
while ($data = mysqli_fetch_array($sql)) {



    $xml .= '<url><loc>';
    $xml .= "https://www.myfrenchstartup.com/fr/startup-france/" . generate_id($data['id']) . "/" . urlWriting(strtolower($data["nom"]));
    $xml .= '</loc></url>';
}
$xml .= '</urlset>';

echo $xml;
?>