<div class="page-header page-header-dark bg-indigo">



    <div class="navbar navbar-expand-md navbar-dark bg-indigo border-0 shadow-0">
        <div class="d-md-none w-100">
            <button type="button" class="navbar-toggler d-flex align-items-center w-100" data-toggle="collapse" data-target="#navbar-navigation">
                <i class="icon-menu-open mr-2"></i>
                Main navigation
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-navigation">
            <ul class="nav navbar-nav navbar-nav-material">
                <li class="nav-item">
                    <a href="index.php" class="navbar-nav-link <?php if ($menu == 1) echo 'active'; ?>">
                        <i class="icon-home2 mr-2"></i>
                        Dashboard
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-add mr-2"></i>
                        Ajouter Startup
                    </a>

                    <div class="dropdown-menu">
                        <div class="dropdown-header">Startup</div>
                        <div class="">
                            <a href="add_startup.php" class="dropdown-item"><i class="icon-rocket"></i> Nouvelle Startup</a>
                        </div>
                        <div >
                            <a href="attente.php" class="dropdown-item "><i class="icon-rocket"></i> En attente</a>
                        </div>
                        <div >
                            <a href="lf.php" class="dropdown-item "><i class="icon-rocket"></i> From LF</a>
                        </div>
                        <div >
                            <a href="aqui.php" class="dropdown-item "><i class="icon-rocket"></i> From Aquisition</a>
                        </div>
                        <div >
                            <a href="stage.php" class="dropdown-item "><i class="icon-rocket"></i> From Stage</a>
                        </div>
                        <div >
                            <a href="news.php" class="dropdown-item "><i class="icon-rocket"></i> From News</a>
                        </div>
                        <div >
                            <a href="emploi.php" class="dropdown-item "><i class="icon-rocket"></i> From Offre d'emploi</a>
                        </div>
                        <div >
                            <a href="user.php" class="dropdown-item "><i class="icon-rocket"></i> From Inscrits</a>
                        </div>
                        <div >
                            <a href="siret.php" class="dropdown-item "><i class="icon-rocket"></i> Sans SIRET</a>
                        </div>

                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-add mr-2"></i>
                        Modifier Deals
                    </a>

                    <div class="dropdown-menu">
                        <div class="dropdown-header">Deals</div>
                        <div class="">
                            <a href="modif_lf.php" class="dropdown-item"><i class="icon-rocket"></i> Levées de fonds</a>
                        </div>
                        <div>
                            <a href="" class="dropdown-item "><i class="icon-rocket"></i> Rachats</a>
                        </div>
                        <div>
                            <a href="modif_ipo.php" class="dropdown-item "><i class="icon-rocket"></i> IPO</a>
                        </div>
                    </div>
                </li>

                <li class="nav-item">
                    <a href="list.php" class="navbar-nav-link <?php if ($menu == 16) echo 'active'; ?>">
                        <i class="icon-list mr-2"></i>
                        Liste Startup
                    </a>
                </li>




                <li class="nav-item">
                    <a href="clients_potentiels.php" class="navbar-nav-link <?php if ($menu == 140) echo 'active'; ?>">
                        <i class="icon-cash3 mr-2"></i>
                        Clients potentiels
                    </a>
                </li>
                <li class="nav-item">
                    <a href="investisseur.php" class="navbar-nav-link <?php if ($menu == 10) echo 'active'; ?>">
                        <i class="icon-user mr-2"></i>
                        Investisseurs
                    </a>
                </li>
                <li class="nav-item">
                    <a href="corriger_linkedin.php" class="navbar-nav-link <?php if ($menu == 10) echo 'active'; ?>">
                        <i class="icon-cash3 mr-2"></i>
                        Corriger Fondateurs
                    </a>
                </li>
                <li class="nav-item">
                    <a href="levee.php" class="navbar-nav-link <?php if ($menu == 10) echo 'active'; ?>">
                        <i class="icon-cash3 mr-2"></i>
                        Levée de fonds
                    </a>
                </li>
                <li class="nav-item">
                    <a href="aquisition.php" class="navbar-nav-link <?php if ($menu == 109) echo 'active'; ?>">
                        <i class="icon-cash3 mr-2"></i>
                       Aquisitions
                    </a>
                </li>
                <li class="nav-item">
                    <a href="rapport.php" class="navbar-nav-link <?php if ($menu == 100) echo 'active'; ?>">
                        <i class="icon-cash3 mr-2"></i>
                        Etude des deals
                    </a>
                </li>
                <li class="nav-item">
                    <a href="qualif_activite.php" class="navbar-nav-link <?php if ($menu == 101) echo 'active'; ?>">
                        <i class="icon-cash3 mr-2"></i>
                        Qualifier Activité
                    </a>
                </li>
                <li class="nav-item">
                    <a href="users.php" class="navbar-nav-link <?php if ($menu == 101) echo 'active'; ?>">
                        <i class="icon-cash3 mr-2"></i>
                        Inscrit au sites
                    </a>
                </li>
                <li class="nav-item">
                    <a href="flow.php" class="navbar-nav-link <?php if ($menu == 11) echo 'active'; ?>">
                        <i class="icon-cash3 mr-2"></i>
                        Deal Flow
                    </a>
                </li>
                <li class="nav-item">
                    <a href="newsletter.php" class="navbar-nav-link <?php if ($menu == 131) echo 'active'; ?>">
                        <i class="icon-mail-forward mr-2"></i>
                        Newsletter
                    </a>
                </li>
                <li class="nav-item">
                    <a href="cmit.php" class="navbar-nav-link <?php if ($menu == 1331) echo 'active'; ?>">
                        <i class="icon-mail-forward mr-2"></i>
                        CMIT
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-search4 mr-2"></i>
                        Recherche Startup
                    </a>
                    <div class="dropdown-menu">
                        <div class="dropdown-header">Recherche</div>
                        <div >
                            <a href="recherhche.php" class="dropdown-item "><i class="icon-grid2"></i> Par Nom</a>
                        </div>
                        <div >
                            <a href="keyword.php" class="dropdown-item "><i class="icon-paragraph-justify3"></i> Par Keyword</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-user mr-2"></i>
                        Fondateurs
                    </a>

                    <div class="dropdown-menu">
                        <div class="dropdown-header">Fondateurs</div>
                        <div >
                            <a href="qualif_founder.php" class="dropdown-item "><i class="icon-grid2"></i> Qualifier Linkedin</a>

                        </div>
                        <div >
                            <a href="cookie.php" class="dropdown-item "><i class="icon-grid2"></i> Modifier Cookies</a>

                        </div>
                        <div >
                            <a href="fondateur.php" class="dropdown-item "><i class="icon-grid2"></i> Désinscription</a>

                        </div>
                        <div >
                            <a href="lier_startup.php" class="dropdown-item "><i class="icon-paragraph-justify3"></i> Demande de liaison</a>

                        </div>
                        <div >
                            <a href="manuelle.php" class="dropdown-item "><i class="icon-paragraph-justify3"></i> Liaison manuelle</a>

                        </div>
                     

                    </div>
                </li>
                <li class="nav-item">
                    <a href="premium.php" class="navbar-nav-link <?php if ($menu == 101) echo 'active'; ?>">
                        <i class="icon-cash3 mr-2"></i>
                      Clients Premium
                    </a>
                </li>
            </ul>


        </div>
    </div>
    <!-- /secondary navbar -->



</div>