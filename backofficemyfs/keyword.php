<?php
include('db.php');
$menu = 13;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Recherche Startup</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">


        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />
        <style>
            .form-inline {
                display: block !important;

            }
        </style>
    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->


        <!-- Page content -->
        <div >

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">



                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Startups from Keyword</h6>

                                </div>

                                <!-- Numbers -->
                                <div class="card-body py-0" style="margin-bottom: 50px;">
                                    <form method="post" action="">
                                        <div class="row">
                                            <div class="col-lg-4">  <input type="text" class="form-control" name="keyword">
                                            </div>
                                            <div class="col-lg-4">
                                                <button type="submit" name="chercher" class="btn btn-primary">Trouver</button>
                                            </div>
                                        </div>
                                    </form>
                                    <?php
                                    if (isset($_POST['keyword'])) {


                                        $aDoor = explode(",", $_POST['keyword']);
                                        $N = count($aDoor);

                                        for ($i = 0; $i < $N; $i++) {

                                            $etat.=" startup.short_fr like '%" . $aDoor[$i] . "%' or startup.long_fr like '%" . $aDoor[$i] . "%' or startup.short_en like '%" . $aDoor[$i] . "%' or startup.long_en like '%" . $aDoor[$i] . "%' or startup.technologie like '%" . $aDoor[$i] . "%' or activite.tags like '%" . $aDoor[$i] . "%' or startup.concat_tags like '%" . $aDoor[$i] . "%' or ";
                                        }
                                        $etat = substr($etat, 0, -3);  // retourne "abcde"



                                        $sql33 = mysqli_query($link, "SELECT startup.`id`, `nom`, `societe`, `url`, `date_complete`, `effectif`, `email`, `tel`, `siret`,activite.tags, `naf`, `adresse`, `cp`, `ville`, `short_fr`,long_fr,concat_tags FROM `startup` inner join activite on activite.id_startup=startup.id WHERE status=1 and (" . $etat . " )");
                                        $nbr = mysqli_num_rows($sql33);
                                        echo "<h1 style='margin-top: 20px; font-size: 20px;'>" . $nbr . " résultats</h1>";
                                        ?>
                                        <div class="col-md-12">

                                            <div class="table-responsive mb-4">
                                                <table id="example" class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Nom</th>
                                                            <th>Societe</th>
                                                            <th>Url</th>
                                                            <th>Date</th>
                                                            <th>Effectif</th>
                                                            <th>Email</th>
                                                            <th>Tel</th>
                                                            <th>Siret</th>
                                                            <th>NAF</th>
                                                            <th>Adresse</th>
                                                            <th>CP</th>
                                                            <th>Ville</th>
                                                            <th>Short fr</th>
                                                            <th>Tags</th>
                                                            <th>Secteur</th>
                                                            <th>Sous secteur</th>
                                                            <th>Marché</th>
                                                            <th>Sous marché</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        while ($data = mysqli_fetch_array($sql33)) {
                                                            $activite = mysqli_fetch_array(mysqli_query($link, "select tags from activite where activite.id_startup=" . $data['id']));
                                                            $secteur = mysqli_fetch_array(mysqli_query($link, "select nom_secteur from secteur inner join activite on activite.secteur=secteur.id where activite.id_startup=" . $data['id']));
                                                            $sous_secteur = mysqli_fetch_array(mysqli_query($link, "select nom_sous_secteur from sous_secteur inner join activite on activite.sous_secteur=sous_secteur.id where activite.id_startup=" . $data['id']));
                                                            $marche = mysqli_fetch_array(mysqli_query($link, "select nom_activite from last_activite inner join activite on activite.activite=last_activite.id where activite.id_startup=" . $data['id']));
                                                            $sous_marche = mysqli_fetch_array(mysqli_query($link, "select nom_sous_activite from last_sous_activite inner join activite on activite.sous_activite=last_sous_activite.id where activite.id_startup=" . $data['id']));
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $data['nom'] ?></td>
                                                                <td><?php echo $data['societe'] ?></td>
                                                                <td><?php echo $data['url'] ?></td>
                                                                <td><?php echo $data['date_complete'] ?></td>
                                                                <td><?php echo $data['effectif'] ?></td>
                                                                <td><?php echo $data['email'] ?></td>
                                                                <td><?php echo $data['tel'] ?></td>
                                                                <td><?php echo $data['siret'] ?></td>
                                                                <td><?php echo $data['naf'] ?></td>
                                                                <td><?php echo utf8_encode($data['adresse']) ?></td>
                                                                <td><?php echo $data['cp'] ?></td>
                                                                <td><?php echo utf8_encode($data['ville']) ?></td>
                                                                <td><?php echo utf8_encode($data['short_fr']) ?></td>
                                                                <td><?php echo utf8_encode($data['concat_tags']) ?></td>
                                                                <td><?php echo utf8_encode($secteur['nom_secteur']) ?></td>
                                                                <td><?php echo utf8_encode($sous_secteur['nom_sous_secteur']) ?></td>
                                                                <td><?php echo utf8_encode($marche['nom_activite']) ?></td>
                                                                <td><?php echo utf8_encode($sous_marche['nom_sous_activite']) ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>


                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>
                            </div>

                        </div>


                    </div>
                    <!-- /dashboard content -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    "paging": true,
            "order": [[0, "asc"]],
                "searching": true,
                    "bLengthChange": true,
                    "aoColumnDefs": [
                    {"sType": "numeric"}
                    ]
                    });
            });

                    function change_texte(id) {
                    var val = document.getElementById("sup_" + id).value;
            $.ajax({
            type: "POST",
                url: "change_text_stage.php",
                    data: {
                id: id,
                    val: val
                    },
                    success: function (o) {
                        var h = JSON.parse(o);
                    var t = eval(h);


                    }
                });

                        }         </script>
    </body>
</html>
