<?php
include('db.php');
$menu = 140;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Personnes accédant à la page de paiement</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style>
            .form-inline {
                display: block !important;

            }
        </style>
    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->


        <!-- Page content -->
        <div >

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">


                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Personnes ayant accès à la page de paiement</h6>
                                </div>

                                <!-- Numbers -->
                                <div class="card-body py-0">

                                    <div class="col-md-12">

                                        <div class="table-responsive mb-4">
                                            <table id="example" class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>User</th>
                                                        <th>Email MYFS</th>
                                                        <th>Email STRIPE</th>
                                                        <th>Société</th>
                                                        <th>Fonction</th>
                                                        <th>Téléphone</th>
                                                        <th>Ville</th>
                                                        <th>Inscrit le </th>
                                                        <th>Payé le</th>
                                                        <th>Statut</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $attente = mysqli_query($link, "select * from user where premium=1");
                                                    while ($uss = mysqli_fetch_array($attente)) {
                                                        $data = mysqli_fetch_array(mysqli_query($link, "select * from user_subscriptions where id=" . $uss['subscription_id']));
                                                      
                                                        if($data['plan_id']==1) $dd="Mensuel";
                                                        if($data['plan_id']==2) $dd="Mensuel";
                                                        
                                                        ?>
                                                        <tr>
                                                            <td><?php echo utf8_encode($uss['prenom'] . " " . $uss['nom']); ?></td>
                                                            <td><?php echo $uss['email']; ?></td>
                                                            <td><?php echo $data['payer_email']; ?></td>
                                                            <td><?php echo $uss['societe']; ?></td>
                                                            <td><?php echo $uss['fonction']; ?></td>
                                                            <td><?php echo $uss['tel']; ?></td>
                                                            <td><?php echo $uss['ville']; ?></td>
                                                            <td><?php echo $uss['date_add']; ?></td>
                                                            <td><?php echo $data['plan_period_start']; ?></td>
                                                             <td><?php echo $uss['status']; ?></td>

                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>


                                    </div>

                                </div>
                            </div>
                            <!-- Latest posts -->







                        </div>


                    </div>
                    <!-- /dashboard content -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    "paging": true,
                    "order": [[2, "desc"]],
                    "searching": true,
                    "bLengthChange": true,
                    "aoColumnDefs": [
                        {"sType": "numeric"}
                    ]
                });
            });
        </script>
    </body>
</html>
