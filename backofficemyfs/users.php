<?php
include('db.php');
$menu = 10;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Users</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <link rel = "stylesheet" href = "https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css">
        <link rel = "stylesheet" href = "https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">


        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style>
            tfoot input {
                width: 100%;
                padding: 3px;
                box-sizing: border-box;
            }
            .col-lg-4{
                text-align: left;
                margin-bottom: 25px;
            }
            label{
                font-weight: bold;
            }
        </style>
    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->


        <!-- Page content -->
        <div class="">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">


                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title" style="font-size: 18px;font-style: italic;color: #3a61e4;font-weight: bold;text-decoration: underline;">Liste des inscrits MyFrenchStartup</h6>
                                </div>

                                <!-- Numbers -->
                                <div class="card-body py-0">

                                    <div class="col-md-12">
                                        <form method="post" action="">
                                            <div style="text-align: center">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <label>Civilité</label>
                                                        <select name="civ" class="form-control">
                                                            <option></option>
                                                            <option value="0">Monsieur</option>
                                                            <option value="2">Madame</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Type d'utilisateur</label>
                                                        <select name="types" class="form-control">
                                                            <option></option>
                                                            <option value="10">Corporate</option>
                                                            <option value="7">Enseignant</option>
                                                            <option value="0">Entrepreneur</option>
                                                            <option value="6">Etudiant</option>
                                                            <option value="4">Institutionnel</option>
                                                            <option value="1">Investisseur</option>
                                                            <option value="3">Media</option>
                                                            <option value="5">Prestataire</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <label>Société</label>
                                                        <input type="text" name="societe" class="form-control">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Fonction</label>
                                                        <input type="text" name="fonction" class="form-control">
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <label>Téléphone</label><br>
                                                        <input type="radio" name="tel" value="1"> Oui
                                                        <input type="radio" name="tel" value="0"> Non
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <label>S'est Connecté ?</label><br>
                                                        <input type="radio" name="connect" value="1"> Oui
                                                        <input type="radio" name="connect" value="0"> Non
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <label>Lié à une Startup</label><br>
                                                        <input type="radio" name="lier" value="1"> Oui
                                                        <input type="radio" name="lier" value="0"> Non
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Connecté de </label>
                                                        <input type="date" name="dt1" class="form-control">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Connecté à </label>
                                                        <input type="date" name="dt2" class="form-control">
                                                    </div>
                                                    <div class="col-lg-12">&nbsp;</div>
                                                    <div class="col-lg-4">
                                                        <label>Ajouté de </label>
                                                        <input type="date" name="dt3" class="form-control">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Ajouté à </label>
                                                        <input type="date" name="dt4" class="form-control">
                                                    </div>
                                                    <div class="col-lg-12" style="margin-top: 25px; margin-bottom: 15px">
                                                        <input type="submit" name="chercher" class="btn btn-primary" value="Trouver">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                        if (isset($_POST['chercher'])) {
                                            $types = $_POST['types'];
                                            if ($types != '') {
                                                $sql_types = " and type='" . $types . "' ";
                                            } else {
                                                $sql_types = ' ';
                                            }

                                            $civ = $_POST['civ'];
                                            if ($civ != '') {
                                                if ($civ == 0)
                                                    $sql_civ = " and civilite=0 ";
                                                if ($civ == 2)
                                                    $sql_civ = " and (civilite=1 or civilite=2) ";
                                            } else {
                                                $sql_civ = ' ';
                                            }

                                            $societe = addslashes($_POST['societe']);
                                            if ($societe != '') {
                                                $sql_societe = " and societe like '%" . $societe . "%' ";
                                            } else {
                                                $sql_societe = ' ';
                                            }

                                            $fonction = addslashes($_POST['fonction']);
                                            if ($fonction != '') {
                                                $sql_fonction = " and fonction like '%" . $fonction . "%' ";
                                            } else {
                                                $sql_fonction = ' ';
                                            }
                                            $tel = addslashes($_POST['tel']);
                                            if ($tel != '') {
                                                if ($tel == 1) {
                                                    $sql_tel = " and tel!='' ";
                                                }
                                                if ($tel == 0) {
                                                    $sql_tel = " and tel='' ";
                                                }
                                            } else {
                                                $sql_tel = ' ';
                                            }
                                            $cnx = addslashes($_POST['connect']);
                                            if ($cnx != '') {

                                                $sql_cnx = " and nb_cnx>=1 ";
                                            } else {
                                                $sql_cnx = ' ';
                                            }
                                            $lier = addslashes($_POST['lier']);
                                            if ($lier != '') {

                                                $sql_lier = " and lier=1 ";
                                            } else {
                                                $sql_lier = ' ';
                                            }
                                            $de = addslashes($_POST['dt1']);
                                            if ($de != '') {

                                                $sql_de = " and date(last_connection)>='" . $de . "' ";
                                            } else {
                                                $sql_de = ' ';
                                            }
                                            $a = addslashes($_POST['dt2']);
                                            if ($a != '') {

                                                $sql_a = " and date(last_connection)<='" . $a . "' ";
                                            } else {
                                                $sql_a = ' ';
                                            }
                                            $add_de = addslashes($_POST['dt3']);
                                            if ($add_de != '') {

                                                $sql_add_de = " and date(date_add)>='" . $add_de . "' ";
                                            } else {
                                                $sql_add_de = ' ';
                                            }
                                            $add_a = addslashes($_POST['dt4']);
                                            if ($add_a != '') {

                                                $sql_add_a = " and date(date_add)<='" . $add_a . "' ";
                                            } else {
                                                $sql_add_a = ' ';
                                            }
                                            ?>
                                            <div class="table-responsive mb-4">
                                                <table id="example" class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Type</th>
                                                            <th>Civilté</th>
                                                            <th>Utilisateur</th>
                                                            <th>Société</th>
                                                            <th>Fonction</th>
                                                            <th>Téléphone</th>
                                                            <th>Email</th>
                                                            <th>Date ajout</th>
                                                            <th>Dernière connexion</th>
                                                            <th>Nombre de connexion</th>
                                                            <th>Statut</th>
                                                            <th>Ville</th>
                                                            <th>Startups vu</th>
                                                            <th>Commentaire</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $sql33 = mysqli_query($link, "SELECT * FROM `user` where email !='' $sql_types $sql_civ $sql_societe $sql_fonction  $sql_tel $sql_cnx $sql_de $sql_a $sql_add_a $sql_add_de $sql_lier ");
                                                        while ($data = mysqli_fetch_array($sql33)) {
                                                            $premium = "";
                                                            $type = "";
                                                            $civilite = "";
                                                            if ($data['status'] == 0)
                                                                $etat = "<i class='fa fa-circle' style='color:red'></i> Inactif";
                                                            if ($data['status'] == 1)
                                                                $etat = "<i class='fa fa-circle' style='color:green'></i> Actif";
                                                            if ($data['type'] == 10) {
                                                                $type = "Corporate";
                                                            }
                                                            if ($data['type'] == 7) {
                                                                $type = "Enseignant";
                                                            }
                                                            if ($data['type'] == 0) {
                                                                $type = "Entrepreneur";
                                                            }
                                                            if ($data['type'] == 6) {
                                                                $type = "Etudiant";
                                                            }
                                                            if ($data['type'] == 4) {
                                                                $type = "Institutionnel";
                                                            }
                                                            if ($data['type'] == 1) {
                                                                $type = "Investisseur";
                                                            }
                                                            if ($data['type'] == 3) {
                                                                $type = "Media";
                                                            }
                                                            if ($data['type'] == 5) {
                                                                $type = "Prestataire";
                                                            }
                                                            if ($data['civilite'] == 2) {
                                                                $civilite = "Mme";
                                                            }
                                                            if ($data['civilite'] == 0) {
                                                                $civilite = "M.";
                                                            }
                                                            if ($data['civilite'] == 1) {
                                                                $civilite = "Mme";
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $type; ?></td>
                                                                <td><?php echo $civilite; ?></td>
                                                                <td><?php echo utf8_encode($data['prenom']) . " " . utf8_encode($data['nom']); ?></td>
                                                                <td><?php echo utf8_encode($data['societe']); ?></td>
                                                                <td><?php echo utf8_encode($data['fonction']); ?></td>
                                                                <td><?php echo $data['tel']; ?></td>
                                                                <td><?php echo $data['email']; ?></td>
                                                                <td><?php echo $data['date_add']; ?></td>
                                                                <td><?php echo $data['last_connection']; ?></td>
                                                                <td><?php echo $data['nb_cnx']; ?></td>
                                                                <td><?php echo $etat; ?></td>
                                                                <td><?php echo utf8_encode($data['ville']); ?></td>
                                                                <td><?php echo $data['vu']; ?></td>
                                                                <td><textarea name="comment" class="form-control" onblur="saves(<?php echo $data['id']; ?>)" id="<?php echo $data['id']; ?>"><?php echo utf8_encode($data['msg']); ?></textarea></td>

                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Type</th>
                                                            <th>Civilté</th>
                                                            <th>Utilisateur</th>
                                                            <th>Société</th>
                                                            <th>Fonction</th>
                                                            <th>Téléphone</th>
                                                            <th>Email</th>
                                                            <th>Date ajout</th>
                                                            <th>Dernière connexion</th>
                                                            <th>Nombre de connexion</th>
                                                            <th>Statut</th>
                                                            <th>Ville</th>
                                                            <th>Startups vu</th>
                                                            <th>Commentaire</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                    </div>

                                </div>
                            </div>
                            <!-- Latest posts -->







                        </div>


                    </div>
                    <!-- /dashboard content -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>

    </body>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>

    <script>

                                                                    $(document).ready(function () {
                                                                        // Setup - add a text input to each footer cell
                                                                        $('#example tfoot th').each(function () {
                                                                            var title = $(this).text();
                                                                            $(this).html('<input type="text" placeholder="' + title + '" />');
                                                                        });

                                                                        // DataTable
                                                                        var table = $('#example').DataTable({
                                                                            initComplete: function () {
                                                                                // Apply the search
                                                                                this.api()
                                                                                        .columns()
                                                                                        .every(function () {
                                                                                            var that = this;

                                                                                            $('input', this.footer()).on('keyup change clear', function () {
                                                                                                if (that.search() !== this.value) {
                                                                                                    that.search(this.value).draw();
                                                                                                }
                                                                                            });
                                                                                        });
                                                                            },
                                                                        });
                                                                    });
                                                                    function saves(id) {
                                                                        var texte = document.getElementById(id).value;

                                                                        $.ajax({
                                                                            url: 'saves_msg.php?texte=' + texte + '&id=' + id,

                                                                            success: function (data) {



                                                                            }
                                                                        });

                                                                    }
    </script>
</html>
