<?php
include('db.php');
$menu = 1;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
$j = 0;
$nb_teams = mysqli_num_rows(mysqli_query($link, "select id from startup_teams"));
$nb_teams_distinct = mysqli_num_rows(mysqli_query($link, "select id from startup_teams group by linkedInProfileUrl"));
//$nb_mails = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where (email!='' or email_p !='' or email_ph !='' or email_genere!='')"));
$email_sup1 = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where email!=''  group by email"));
$email_personnes1 = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where email_p!=''   group by email_p"));
$email_linkedin1 = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where email_ph!=''  group by email_ph"));
$email_genere1 = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where email_genere!=''  group by email_genere"));
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Analytics</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style>
            div.dataTables_wrapper {
                width: 800px;
                margin: 0 auto;
            }
        </style>
    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->


        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">


                            <div class="card">


                                <!-- Numbers -->
                                <div class="card-body py-0">
                                    <div class="row" style="padding-top: 30px">
                                        <div class="col-lg-4">

                                            <div class="card bg-teal-400">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $nb_teams; ?></h3>
                                                        <span class="badge bg-teal-800 badge-pill align-self-center ml-auto" style="font-size: 20px;"></span>
                                                    </div>
                                                    <div>
                                                        Profils
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-lg-4">
                                            <div class="card bg-teal-400">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $nb_teams_distinct; ?></h3>
                                                        <span class="badge bg-teal-800 badge-pill align-self-center ml-auto" style="font-size: 20px;"></span>
                                                    </div>
                                                    <div>
                                                        Profils uniques
                                                    </div>
                                                </div>
                                                <div id="server-load"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="card bg-teal-400">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $email_sup1 + $email_personnes1 + $email_linkedin1 + $email_genere1; ?></h3>
                                                        <span class="badge bg-teal-800 badge-pill align-self-center ml-auto" style="font-size: 20px;"></span>
                                                    </div>
                                                    <div>
                                                        Emails uniques
                                                    </div>
                                                </div>
                                                <div id="server-load"></div>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title"><?php echo $email_sup1 + $email_personnes1; ?> emails qualifiés</h6>
                                </div>
                                <div class="card-body py-0">
                                    <div class="row">
                                        <div style="text-align: center;width: 100%;padding: 30px;font-size: 25px;color: #309be7;"><?php echo $email_sup1 + $email_personnes1; ?></div>




                                        <table id="myTable" class="display nowrap" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Secteur</th>
                                                    <th>Total</th>
                                                    <th>Contact</th>
                                                    <th>Management</th>
                                                    <th>Finance</th>
                                                    <th>IT</th>
                                                    <th>RH</th>
                                                    <th>Mark. Commer.</th>
                                                    <th>logistics</th>


                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $nbs = 0;
                                                $sup = 0;
                                                $per = 0;
                                                $phan = 0;
                                                $gen = 0;
                                                $sql_secteur = mysqli_query($link, "select count(*) as nb,secteur.nom_secteur from secteur inner join startup_teams on startup_teams.startup_secteur=secteur.nom_secteur where profile='myfs' and (email!='' or email_p!='') group by startup_teams.startup_secteur order by nb desc");
                                                while ($data_secteur = mysqli_fetch_array($sql_secteur)) {
                                                    $email_sup = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where email!='' and startup_secteur='" . $data_secteur['nom_secteur'] . "' group by email"));
                                                    $email_personnes = mysqli_num_rows(mysqli_query($link, "SELECT id, email_p FROM `startup_teams` WHERE email_p != '' AND `startup_secteur` = '" . $data_secteur['nom_secteur'] . "' GROUP BY email_p"));

                                                    $ceo = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where management=1 and (email_p!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "'  and profile='myfs' group by email_p"));
                                                    $cto = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where IT=1 and (email_p!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "'  and profile='myfs' group by email_p"));
                                                    $cfo = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where finance=1 and ( email_p!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "'  and profile='myfs' group by email_p"));
                                                    $cpo = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where RH=1 and ( email_p!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "'  and profile='myfs' group by email_p"));
                                                    $it = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where IT=1 and ( email_p!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "'  and profile='myfs' group by email_p"));
                                                    $marketing = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where marketing_commercial=1 and ( email_p!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "'  and profile='myfs' group by email_p"));
                                                    $logistics = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where logistics=1 and ( email_p!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "'  and profile='myfs' group by email_p"));
                                                    ?>
                                                    <tr>
                                                        <td><?php echo utf8_encode($data_secteur['nom_secteur']) ?></td>
                                                        <td><?php echo $email_sup + $email_personnes; ?></td>
                                                        <td><?php echo $email_sup ?></td>
                                                        <td><?php echo $ceo; ?></td>
                                                        <td><?php echo $cfo; ?></td>
                                                        <td><?php echo $cto; ?></td>
                                                        <td><?php echo $cpo; ?></td>
                                                        <td><?php echo $marketing; ?></td>
                                                        <td><?php echo $logistics; ?></td>

                                                    </tr>
                                                    <?php
                                                }
                                                ?>

                                            </tbody>

                                        </table>
                                    </div>





                                </div>
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title"><?php echo $email_linkedin1 + $email_genere1; ?> emails générés </h6>
                                </div>
                                <div class="card-body py-0">
                                    <div class="row">
                                        <div style="text-align: center;width: 100%;padding: 30px;font-size: 25px;color: #309be7;"><?php echo $email_linkedin1 + $email_genere1; ?></div>
                                        <table id="myTable2" class="display nowrap" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Secteur</th>
                                                    <th>Total</th>
                                                    <th>Management</th>
                                                    <th>Finance</th>
                                                    <th>IT</th>
                                                    <th>RH</th>
                                                    <th>Mark. Commer.</th>
                                                    <th>logistics</th>


                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $nbs = 0;
                                                $sup = 0;
                                                $per = 0;
                                                $phan = 0;
                                                $gen = 0;
                                                $sql_secteur = mysqli_query($link, "select count(*) as nb,secteur.nom_secteur from secteur inner join startup_teams on startup_teams.startup_secteur=secteur.nom_secteur where profile='linkedin' and (email_ph!='' or email_genere!='') group by startup_teams.startup_secteur order by nb desc");
                                                while ($data_secteur = mysqli_fetch_array($sql_secteur)) {
                                                    $email_sup = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where email_ph!='' and startup_secteur='" . $data_secteur['nom_secteur'] . "' and profile='linkedin' group by email_ph"));
                                                    $email_sup2 = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where email_genere!='' and startup_secteur='" . $data_secteur['nom_secteur'] . "' and profile='linkedin' group by email_genere"));

                                                    $ceo = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where management=1 and (email_ph!='' or email_genere!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "' and profile='linkedin'"));
                                                    $cto = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where IT=1 and (email_ph!='' or email_genere!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "' and profile='linkedin' "));
                                                    $cfo = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where finance=1 and (email_ph!='' or email_genere!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "' and profile='linkedin' "));
                                                    $cpo = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where RH=1 and (email_ph!='' or email_genere!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "' and profile='linkedin' "));
                                                    $it = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where IT=1 and (email_ph!='' or email_genere!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "' and profile='linkedin' "));
                                                    $marketing = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where marketing_commercial=1 and (email_ph!='' or email_genere!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "' and profile='linkedin' "));
                                                    $logistics = mysqli_num_rows(mysqli_query($link, "select id from startup_teams where logistics=1 and (email_ph!='' or email_genere!='') and startup_secteur='" . $data_secteur['nom_secteur'] . "' and profile='linkedin' "));
                                                    ?>
                                                    <tr>
                                                        <td><?php echo utf8_encode($data_secteur['nom_secteur']) ?></td>
                                                        <td><?php echo $email_sup + $email_sup2 ?></td>
                                                        <td><?php echo $ceo; ?></td>
                                                        <td><?php echo $cfo; ?></td>
                                                        <td><?php echo $cto; ?></td>
                                                        <td><?php echo $cpo; ?></td>
                                                        <td><?php echo $marketing; ?></td>
                                                        <td><?php echo $logistics; ?></td>

                                                    </tr>
                                                    <?php
                                                }
                                                ?>

                                            </tbody>

                                        </table>
                                    </div>





                                </div>

                            </div>
                            <!-- Latest posts -->

                        </div>


                    </div>
                    <!-- /dashboard content -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('#myTable').DataTable({
                    "scrollX": true,
                    "bPaginate": false
                });
            });
            $(document).ready(function () {
                $('#myTable2').DataTable({
                    "scrollX": true,
                    "bPaginate": false
                });
            });
            $(document).ready(function () {
                $('#myTable3').DataTable({
                    "scrollX": true,
                    "bPaginate": false
                });
            });
        </script>
    </body>
</html>
