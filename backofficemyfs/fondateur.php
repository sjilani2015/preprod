<?php
include('db.php');
$menu = 14;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Fondateurs</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />

        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />


        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style>
            .form-inline {
                display: block !important;

            }
        </style>
    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->
        <?php
        if (isset($_GET['idm'])) {
            mysqli_query($link, "update new_startups set sup=1 where id=" . $_GET['idm']);
            echo '<script>window.location="news.php"</script>';
        }
        ?>

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->
                    <?php
                    if (isset($_GET['ids'])) {

                        mysqli_query($link, "update personnes set etat=0 where id=" . $_GET['ids']);
                        echo '<script>alert("Fondateur désinscrit");</script>';
                        echo '<script>window.location="fondateur.php"</script>';
                    }
                    ?>

                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">


                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Statistiques</h6>
                                </div>

                                <!-- Numbers -->
                                <div class="card-body py-0" style="margin-bottom: 50px;">

                                    <div class="col-md-12">

                                        <form method="post" action="">
                                            <div style="text-align: center">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <input type="text" name="nom" class="form-control" placeholder="Nom">
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <input type="text" name="prenom" class="form-control" placeholder="Prénom">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <input type="submit" name="chercher" class="btn btn-primary" value="Trouver">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                        if (isset($_POST['nom'])) {


                                            $nom = addslashes($_POST['nom']);
                                            $prenom = addslashes($_POST['prenom']);

                                            if (($prenom != '') && ($nom != '')) {
                                                $sql33 = mysqli_query($link, "select personnes.id,personnes.nom,personnes.prenom,personnes.fonction,personnes.email,personnes.tel,personnes.id_startup from personnes inner join startup on startup.id=personnes.id_startup where personnes.etat=1 and startup.status=1 and (personnes.nom like '%" . $nom . "%' or personnes.prenom like '%" . $prenom . "%')")or die(mysql_error());
                                            }
                                            if (($prenom != '') && ($nom == '')) {
                                                $sql33 = mysqli_query($link, "select personnes.id,personnes.nom,personnes.prenom,personnes.fonction,personnes.email,personnes.tel,personnes.id_startup from personnes inner join startup on startup.id=personnes.id_startup where personnes.etat=1 and startup.status=1 and (personnes.prenom like '%" . $prenom . "%')")or die(mysql_error());
                                            }
                                            if (($prenom == '') && ($nom != '')) {
                                                $sql33 = mysqli_query($link, "select personnes.id,personnes.nom,personnes.prenom,personnes.fonction,personnes.email,personnes.tel,personnes.id_startup from personnes inner join startup on startup.id=personnes.id_startup where personnes.etat=1 and startup.status=1 and (personnes.nom like '%" . $nom . "%')")or die(mysql_error());
                                            }





                                            $nbr = mysqli_num_rows($sql33);
                                            echo "<h1 style='margin-top: 20px; font-size: 20px;'>" . $nbr . " résultats</h1>";
                                            ?>
                                            <div class="table-responsive mb-4">
                                                <table id="example" class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Nom</th>
                                                            <th>Fonction</th>
                                                            <th>Startup</th>
                                                            <th>Email</th>
                                                            <th>Tel</th>
                                                            <th>Désinscrire</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        while ($data = mysqli_fetch_array($sql33)) {
                                                            $startup = mysqli_fetch_array(mysqli_query($link, "select * from startup where id=" . $data['id_startup']));

                                                            if ($data['fonction'] == '1')
                                                                $type = "Fondateur";
                                                            if ($data['fonction'] == '2')
                                                                $type = "Co-Fondateur";
                                                            if ($data['fonction'] == '6')
                                                                $type = "CEO";
                                                            if ($data['fonction'] == '9')
                                                                $type = "CTO";
                                                            if ($data['fonction'] == '10')
                                                                $type = "COO";
                                                            if ($data['fonction'] == '11')
                                                                $type = "CMO";
                                                            if ($data['fonction'] == '12')
                                                                $type = "CFO";
                                                            if ($data['fonction'] == '13')
                                                                $type = "Business Developper";
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $data['prenom'] . " " . $data['nom']; ?></td>
                                                                <td><?php echo $type; ?></td>
                                                                <td><?php echo $startup['nom']; ?></td>
                                                                <td><?php echo $data['email']; ?></td>
                                                                <td><?php echo $data['tel']; ?></td>
                                                                <td><a href="fondateur.php?ids=<?php echo $data['id']; ?>" class="btn btn-warning">Désinscrire</a></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                    </div>

                                </div>
                            </div>
                            <!-- Latest posts -->







                        </div>


                    </div>
                    <!-- /dashboard content -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    "paging": true,
                    "order": [[0, "asc"]],
                    "searching": true,
                    "bLengthChange": true,
                    "aoColumnDefs": [
                        {"sType": "numeric"}
                    ]
                });
            });
        </script>
    </body>
</html>
