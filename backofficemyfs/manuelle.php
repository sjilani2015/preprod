<?php
include('db.php');
$menu = 15;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Liaison manuelle</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style>
            .form-inline {
                display: block !important;

            }
        </style>
    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->
        <?php
        $sql = '';
        if (isset($_POST["submitx"])) {
            if ($_POST['userx'] != '' && $_POST['startupx'] != '')
                mysqli_query($link, "insert into user_startup (id, iduser, id_startup,dt,etat,source) values(NULL,'" . $_POST['userx'] . "', '" . $_POST['startupx'] . "','" . date("Y-m-d H:i:s") . "',1,0)");
        }

        if (isset($_GET['act']) && $_GET['act'] == 'del')
            mysqli_query($link, "delete from user_startup where id='" . $_GET['id'] . "'");
        ?>

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">


                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Liaison Startup / User</h6>
                                </div>

                                <!-- Numbers -->
                                <div class="card-body py-0">

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="bordered-justified-pill1">
                                            <div class="col-md-12">
                                                <div style="height: 125px;"><?php
                                                    if ($sql != '')
                                                        echo '<br /><br /><br />Votre demande est ajoutée [id :' . $id . ']';
                                                    else {
                                                        ?>
                                                        <form method="post" enctype="multipart/form-data">
                                                            <h1 id="startup">Demande de liaison</h1>
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <label>Compte</label>
                                                                    <select name="userx" class="form-control">
                                                                        <option value="">---</option>
                                                                        <?php
                                                                        $act = mysqli_query($link, "select * from user where status!=0 and email!='' order by email");
                                                                        while ($actt = mysqli_fetch_object($act)) {
                                                                            echo '<option value="' . $actt->id . '">' . $actt->email . '</option>';
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <label>Startup</label>
                                                                    <select name="startupx" class="form-control">
                                                                        <option value="">---</option>
                                                                        <?php
                                                                        $act = mysqli_query($link, "select * from startup where status='1' order by nom asc");
                                                                        while ($actt = mysqli_fetch_object($act)) {
                                                                            echo '<option value="' . $actt->id . '">' . $actt->nom . '</option>';
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <input type="submit" name="submitx" class="btn btn-primary" value="Ajouter Startup">
                                                                </div>
                                                            </div>
                                                        </form>
                                                    <?php } ?>
                                                </div>
                                                <div class="col-md-12">
                                                    <hr>
                                                </div>

                                                <div class="col-md-12">
                                                    <form method="post" action="">
                                                        <h1 id="startup">Rechercher</h1>
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <strong>Email</strong>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="email" name="email_search" class="form-control" />
                                                            </div>
                                                            <div class="col-md-2">
                                                                <strong>Startup</strong>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="text" name="startup_search" class="form-control" />
                                                            </div>
                                                            <div class="col-md-2"><input type="submit" name="search" class="btn btn-primary" value="Rechercher">
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-md-12">
                                                    <hr>
                                                </div>
                                                <?php
                                                if (isset($_POST['search'])) {
                                                    if ($_POST['email_search'] != "") {
                                                        ?>
                                                        <div class="col-md-12">
                                                            <?php
                                                            $sql1 = mysqli_query($link, "select id from user where email='" . $_POST['email_search'] . "'");
                                                            $data_email = mysqli_fetch_array($sql1);
                                                            $nb1 = mysqli_num_rows($sql1);
                                                            if ($nb1 == 0) {
                                                                ?>
                                                                <div>Pas de résultat.</div>
                                                                <?php
                                                            } else {
                                                                $sql_list_startup = mysqli_query("select startup.nom from user_startup inner join startup on startup.id=user_startup.id_startup where user_startup.iduser=" . $data_email["id"]);
                                                                while ($data = mysqli_fetch_array($sql_list_startup)) {
                                                                    ?>
                                                                    <h3><?php echo $data['nom']; ?></h3>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                        <?php
                                                    }if ($_POST['startup_search'] != "") {
                                                        ?>
                                                        <div class="col-md-12">
                                                            <?php
                                                            $sql1 = mysqli_query($link, "select id from startup where nom='" . $_POST['startup_search'] . "'");

                                                            $nb1 = mysqli_num_rows($sql1);
                                                            if ($nb1 == 0) {
                                                                ?>
                                                                <div>Pas de résultat.</div>
                                                                <?php
                                                            } else {
                                                                while ($data_email = mysqli_fetch_array($sql1)) {

                                                                    $sql_list_startup = mysqli_query($link, "select user.email,user.nom,user.prenom from user_startup inner join user on user.id=user_startup.iduser where user_startup.id_startup=" . $data_email["id"]);
                                                                    $data = mysqli_fetch_array($sql_list_startup);
                                                                    if ($data['email'] != "") {
                                                                        ?>
                                                                        <h3><?php echo $data['prenom'] . " " . $data['nom'] . " :: " . $data['email']; ?></h3>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    "paging": true,
                    "order": [[2, "desc"]],
                    "searching": true,
                    "bLengthChange": true,
                    "aoColumnDefs": [
                        {"sType": "numeric"}
                    ]
                });
            });
        </script>
        <script>


            function valider(id) {


                $.ajax({
                    url: 'ajax/valider_liaison.php?id=' + id,
                    success: function (data) {
                        var t = eval(data);

                        alert("Demande de liaison effectuée");



                    }
                });

            }

            function supprimer(id) {

                $.ajax({
                    url: 'ajax/supprimer_liaison.php?id=' + id,
                    success: function (data) {
                        var t = eval(data);

                        alert("Demande de liaison supprimée");


                    }
                });

            }

        </script>
    </body>
</html>
