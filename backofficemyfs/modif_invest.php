<?php
include('db.php');
$menu = 6;

if (isset($_GET['idi'])) {
    $investisseur = mysqli_fetch_array(mysqli_query($link, "select * from new_name_list where id=" . $_GET['idi']));
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Modifier investisseur</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        <link rel="stylesheet" href="assets/css/datepicker.css">
        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>

        <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>


        <link rel="stylesheet" href="textext/css/textext.core.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.tags.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.autocomplete.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.focus.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.prompt.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.arrow.css" type="text/css" />
        <script src="textext/js/textext.core.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.tags.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.autocomplete.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.suggestions.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.filter.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.focus.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.prompt.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.ajax.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.arrow.js" type="text/javascript" charset="utf-8"></script>

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style type="text/css" media="screen">
            .map_canvas {
                float: left;
            }
            .text-wrap{
                position: relative !important
            }
            .big_titre{
                padding: 10px;
                font-size: 20px;
                cursor: pointer;
                margin-bottom: 10px;
                margin-top: 10px;
            }
            .btn-primary{
                width:120px
            }
        </style>



    </head>

    <body>

        <?php
        if (isset($_POST['new_name'])) {
            $new_name = addslashes($_POST['new_name']);
            $group_name = addslashes($_POST['group_name']);
            $email = addslashes($_POST['email']);
            $tel = addslashes($_POST['tel']);
            $siteweb = addslashes($_POST['siteweb']);
            $adresse = addslashes($_POST['adresse']);
            $cp = addslashes($_POST['postal_code']);
            $ville = addslashes($_POST['locality']);
            $pays = addslashes($_POST['country']);
            $lat = addslashes($_POST['lat']);
            $lng = addslashes($_POST['lng']);
            $id = addslashes($_POST['idi']);
            $twitter = addslashes($_POST['twitter']);
            $linkedin = addslashes($_POST['linkedin']);
            $creation = addslashes($_POST['creation']);

            $target_path = "/var/www/html/invest/";

/////
            $uid = uniqid();
            $tf = basename($_FILES['logo']['name']);

            $target_path = $target_path . $uid . '.' . pathinfo($tf, PATHINFO_EXTENSION);

            $chemin = "../invest/" . $uid . '.' . pathinfo($tf, PATHINFO_EXTENSION);

            if (move_uploaded_file($_FILES['logo']['tmp_name'], $target_path)) {
                mysqli_query($link, "update `new_name_list` set  logo='" . $chemin . "' where id=" . $id)or die(mysqli_error($link));
            }

            mysqli_query($link, "update new_name_list set new_name='" . $new_name . "',group_name='" . $group_name . "',adresse='" . $adresse . "',cp='" . $cp . "',ville='" . $ville . "',pays='" . $pays . "',lat='" . $lat . "',lng='" . $lng . "',tel='" . $tel . "',email='" . $email . "',url='" . $siteweb . "',twitter='".$twitter."',linkedin='".$linkedin."',creation='".$creation."' where id=" . $id);

            echo "<script>alert('Investisseur modifié')</script>";
            echo "<script>window.location='investisseur.php'</script>";
        }
        ?>
        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->


        <div class="page-content" id="page-content">

            <!-- Main sidebar -->

            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <div class="card">


                    <div class="card-body">
                        <div class="tabbable">


                            <div class="tab-content">
                                <div class="tab-pane active" id="bordered-justified-pill1">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form method="post"  enctype="multipart/form-data">
                                                <div class=" bg-indigo-400 big_titre" id="info">Informations générales</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="map_canvas"></div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">New Name</div>
                                                        <div class="col-md-4"><input type="text" class="form-control" value="<?php echo $investisseur['new_name'] ?>" name="new_name" /></div>
                                                        <input type="hidden" name="idi" value="<?php echo $investisseur['id'] ?>">
                                                        <div class="col-md-2">Group Name</div>
                                                        <div class="col-md-4"><input type="text" value="<?php echo $investisseur['group_name'] ?>" class="form-control" name="group_name" /></div>

                                                    </div>


                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">

                                                        <div class="col-md-2">Email</div>
                                                        <div class="col-md-4"><input type="text" class="form-control" value="<?php echo $investisseur['email'] ?>" name="email" id="email" /></div>
                                                        <div class="col-md-2">T&eacute;l&eacute;phone</div>
                                                        <div class="col-md-4"><input type="text" name="tel" value="<?php echo $investisseur['tel'] ?>" class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">

                                                    </div>

                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Site web</div>
                                                        <div class="col-md-4"><input type="text" name="siteweb"  value="<?php echo $investisseur['url'] ?>"  class="form-control" /></div>
                                                        <div class="col-md-2">Twitter</div>
                                                        <div class="col-md-4"><input type="text" name="twitter"  value="<?php echo $investisseur['twitter'] ?>"  class="form-control" /></div>
                                                        <div class="col-md-2">Linkedin</div>
                                                        <div class="col-md-4"><input type="text" name="linkedin"  value="<?php echo $investisseur['linkedin'] ?>"  class="form-control" /></div>
                                                        <div class="col-md-2">Année de création</div>
                                                        <div class="col-md-4"><input type="text" name="creation"  value="<?php echo $investisseur['creation'] ?>"  class="form-control" /></div>

                                                    </div>



                                                </div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">

                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Adresse</div>
                                                        <div class="col-md-10"><input type="text" class="form-control" name="adresse" value="<?php echo $investisseur['adresse'] ?>"  placeholder="Entrez votre adresse" id="geocomplete" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Code Postal</div>
                                                        <div class="col-md-4"><input type="text" value="<?php echo $investisseur['cp'] ?>" name="postal_code" class="form-control" /></div>
                                                        <div class="col-md-2">Ville</div>
                                                        <div class="col-md-4"><input type="text" value="<?php echo $investisseur['ville'] ?>" name="locality" class="form-control" /></div>
                                                        <div class="col-md-2">Pays</div>
                                                        <div class="col-md-4"><input type="text" value="<?php echo $investisseur['pays'] ?>" name="country" class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">LAT</div>
                                                        <div class="col-md-4"><input type="text" name="lat" value="<?php echo $investisseur['lat'] ?>" class="form-control" /></div>
                                                        <div class="col-md-2">LNG</div>
                                                        <div class="col-md-4"><input type="text" name="lng" value="<?php echo $investisseur['lng'] ?>" class="form-control" /></div>
                                                    </div>
                                                </div>


                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">


                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Logo</div>
                                                        <div class="col-md-4"><input type="file" name="logo"  class="form-control" /></div>

                                                    </div>
                                                </div>



                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <table width="1134" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="center" colspan="2"><button type="submit" class="btn btn-success">Modifier</button></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </form>

                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- /main content -->





        </div>

        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcCIjgD7FA1zVC6EQwS5V6I-6xuUuwMiQ&sensor=false&amp;libraries=places"></script>
        <script src="assets/js/jquery.geocomplete.js"></script>
        <script src="assets/js/bootstrap-datepicker.js"></script>
        <script src="script/function.js"></script>
        <script src="script/site.js"></script>
        <script src="script/siret.js"></script>
        <script src="script/verif.js"></script>
        <script src="script/verifphontom.js"></script>
        <script src="script/google_linkedin_company.js"></script>
        <script src="script/qualif_linkedin_company.js"></script>
        <script src="script/verif_seul.js"></script>
        <script>


            function remplire_tags() {
                $('.list_invest_tag')
                        .textext({
                            plugins: 'tags autocomplete'
                        })
                        .bind('getSuggestions', function (e, data)
                        {
                            var list = [
<?php
$sql_inves = mysqli_query($link, "select id,new_name from temp_lf group by new_name order by new_name");
while ($data_invest = mysqli_fetch_array($sql_inves)) {
    ?>
                                    '<?php echo addslashes($data_invest['new_name']); ?>',
    <?php
}
?>
                            ],
                                    textext = $(e.target).textext()[0],
                                    query = (data ? data.query : '') || '';

                            $(this).trigger(
                                    'setSuggestions',
                                    {result: textext.itemManager().filter(list, query)}
                            );
                        })
                        ;
            }
            $(document).ready(function () {

                remplire_tags();
            }
            );

            function verif_verif() {
                var siren = document.getElementById("verif").value;
                if (siren != 'https://www.verif.com/societe/') {
                    $.ajax({
                        url: 'getverifcom.php?field=' + siren,
                        beforeSend: function () {
                            $('#loader1').show();
                            document.getElementById("page-content").style.opacity = 0.5;
                        },
                        complete: function () {
                            $('#loader1').hide();
                            document.getElementById("page-content").style.opacity = 1;
                        },
                        success: function (data) {

                            var t = eval(data);
                            document.getElementById("geocomplete").value = t[0].adresse;
                            document.getElementById("siret").value = t[0].siret;
                            document.getElementById("dp1").value = t[0].creation;
                            document.getElementById("capital").value = t[0].capital;
                            document.getElementById("siret").value = t[0].siret;
                            document.getElementById("naf").value = t[0].naf;
                            document.getElementById("libnaf").value = t[0].libelle;
                            document.getElementById("juridique").value = t[0].juridique;
                            document.getElementById("nom").value = t[0].startup;
                            document.getElementById("societe").value = t[0].startup;
                            document.getElementById("prenomf1").value = t[0].dirigeant1;



                            google_linkedin_company();


                        }
                    });
                }
            }



        </script>
    </body>
</html>
