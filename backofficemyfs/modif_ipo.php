<?php
include('db.php');
$menu = 166;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Modifier LF</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">


        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />
        <style>
            .form-inline {
                display: block !important;

            }
        </style>
    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->


        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">



                            <div class="card">


                                <!-- Numbers -->
                                <div class="card-body py-0" style="margin-bottom: 50px;">
                                    <form method="post" action="">
                                        <div style="text-align: center; margin-top: 20px">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <input type="text" name="keyword" class="form-control" placeholder="Nom du startup">
                                                </div>
                                                <div class="col-lg-4">
                                                    <input type="submit" name="chercher" class="btn btn-primary" value="Trouver">
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <?php
                                    if (isset($_POST['keyword'])) {

                                        $etat = '';
                                        $aDoor = addslashes($_POST['keyword']);


                                        $etat .= " startup.nom like '%" . $aDoor . "%' or startup.juridique like '%" . $aDoor . "%' or startup.url like '%" . $aDoor . "%' or ";

                                        $etat = substr($etat, 0, -3);  // retourne "abcde"
                                        // echo "SELECT * FROM `startup` WHERE status!=100 and (" . $etat . " )";

                                        $sql33 = mysqli_query($link, "SELECT lf.id as idl,startup.id,startup.nom,startup.societe,startup.url,lf.date_ajout,lf.de,lf.rachat FROM `startup` inner join lf on lf.id_startup=startup.id WHERE startup.status=1 and rachat=2  and (" . $etat . " )")or die(mysql_error());
                                        $nbr = mysqli_num_rows($sql33);
                                        echo "<h1 style='margin-top: 20px; font-size: 20px;'>" . $nbr . " résultats</h1>";
                                        ?>
                                        <div class="col-md-12">

                                            <div class="table-responsive mb-4">
                                                <table id="example" class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Nom</th>
                                                            <th>Investisseurs</th>
                                                            <th>Date</th>          
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        while ($data = mysqli_fetch_array($sql33)) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $data['nom'] ?></td>
                                                                <td><?php echo $data['de'] ?></td>
                                                                <td><?php echo $data['date_ajout'] ?></td>

                                                                <td><a href="modifier_ipo.php?sup=<?php echo $data['idl']; ?>" class="btn btn-sm btn-success" target="_blank">Modifier</a></td>
                                                                <td><a href="<?php echo 'https://www.myfrenchstartup.com/fr/startup-france/' . generate_id($data['id']) . "/" . urlWriting(strtolower($data["nom"])) ?>" class="btn btn-sm btn-primary">Fiche</a></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>


                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>
                            </div>

                        </div>


                    </div>
                    <!-- /dashboard content -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    "paging": true,
                    "order": [[0, "asc"]],
                    "searching": true,
                    "bLengthChange": true,
                    "aoColumnDefs": [
                        {"sType": "numeric"}
                    ]
                });
            });

            function change_texte(id) {
                var val = document.getElementById("sup_" + id).value;
                $.ajax({
                    type: "POST",
                    url: "change_text_stage.php",
                    data: {
                        id: id,
                        val: val
                    },
                    success: function (o) {
                        var h = JSON.parse(o);
                        var t = eval(h);


                    }
                });

            }
        </script>
    </body>
</html>
