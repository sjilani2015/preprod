<?php
include('db.php');
$menu = 4;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Startups à partir des offres de stage</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">


        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />
        <style>
            .form-inline {
                display: block !important;

            }
        </style>
    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->
        <?php
        if (isset($_GET['idm'])) {
            mysqli_query($link, "update offre_stage set sup=1 where id=" . $_GET['idm']);
            echo '<script>window.location="stage.php"</script>';
        }
        ?>

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">


                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Startups à partir des offres de stage</h6>
                                    <div><a class="btn btn-success" href="../cron/qualif_id_startup_stage.php" target="_blank">Identifier les startups</a></div>

                                </div>

                                <!-- Numbers -->
                                <div class="card-body py-0">

                                    <div class="col-md-12">

                                        <div class="table-responsive mb-4">
                                            <table id="example" class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Startup</th>
                                                        <th>Email</th>
                                                        <th>Date d'ajout</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $attente = mysqli_query($link, "SELECT * FROM `offre_stage` WHERE id_startup=0 and sup=0 group by startup order by date_add desc");
                                                    while ($data = mysqli_fetch_array($attente)) {
                                                        ?>
                                                        <tr>
                                                            <td><input type="text" value="<?php echo utf8_encode(stripcslashes($data['startup'])); ?>" id="sup_<?php echo $data['id']; ?>" onblur="change_texte(<?php echo $data['id']; ?>)"></td>
                                                            <td><?php echo $data['email']; ?></td>
                                                            <td><code><?php echo $data['date_add']; ?></code></td>
                                                            <td>
                                                                <a href="modif_stage.php?sup=<?php echo $data['id']; ?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a>
                                                                <a href="stage.php?idm=<?php echo $data['id']; ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>


                                    </div>

                                </div>
                            </div>
                            <!-- Latest posts -->







                        </div>


                    </div>
                    <!-- /dashboard content -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

        <script>
                                                            $(document).ready(function () {
                                                                $('#example').DataTable({
                                                                    "paging": true,
                                                                    "order": [[0, "asc"]],
                                                                    "searching": true,
                                                                    "bLengthChange": true,
                                                                    "aoColumnDefs": [
                                                                        {"sType": "numeric"}
                                                                    ]
                                                                });
                                                            });

                                                            function change_texte(id) {
                                                                var val = document.getElementById("sup_" + id).value;
                                                                $.ajax({
                                                                    type: "POST",
                                                                    url: "change_text_stage.php",
                                                                    data: {
                                                                        id: id,
                                                                        val: val
                                                                    },
                                                                    success: function (o) {
                                                                        var h = JSON.parse(o);
                                                                        var t = eval(h);


                                                                    }
                                                                });

                                                            }
        </script>
    </body>
</html>
