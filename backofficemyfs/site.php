<?php

include("config.php");
include("simple_html_dom.php");
ini_set("display_errors", 0);
error_reporting(0);

function slugify($text) {
    // replace all non letters or digits by -
    $text = preg_replace('/\W+/', '-', $text);

    // trim and lowercase
    $text = strtolower(trim($text, '-'));

    return $text;
}

function page_title($url) {
    $fp = file_get_contents($url);
    if (!$fp)
        return null;

    $res = preg_match("/<title>(.*)<\/title>/siU", $fp, $title_matches);
    if (!$res)
        return null;

    // Clean up title: remove EOL's and excessive whitespace.
    $title = preg_replace('/\s+/', ' ', $title_matches[1]);
    $title = trim($title);
    return $title;
}

$website = "http://" . str_replace(array("http://","https://", "www."), array("","", ""), $_GET['field']);



$my_siret = "";
$tags_en = "";
$description = "";
$titre = page_title($website);
$facebook = "";
$twitter = "";
$linkedin = "";
$scoop = "";
$google = "";
$youtube = "";
$pinterest = "";
$rss = "";
$blog = "";
$email = "";
$legal1 = "";
$mailto = "";
// Create DOM from URL or file
$html = file_get_html($website);
$tags = get_meta_tags($website);
// Notez que les clés sont en minuscule, et
// le . a été remplacé par _ dans la clé

/* print_r($html);
  exit(); */

$tags_en = $tags['keywords'];     // documentation php
$description = $tags['Description'];  // n manuel PHP
// Find all links 

foreach ($html->find('a') as $element) {
    /*
     * Facebook
     */
    $pos_facebook = stripos($element->href, "facebook.com");
    if ($pos_facebook !== false) {
        $tab_facebook = explode("?", $element->href);
        $facebook = $tab_facebook[0];
    }
    /*
     * Twitter
     */
    $pos_twitter = stripos($element->href, "twitter.com");
    if ($pos_twitter !== false) {
        $tab_twitter = explode("?", $element->href);
        $twitter = $tab_twitter[0];
    }
    /*
     * Linkedin
     */
    $pos_linkedin = stripos($element->href, "linkedin.com");
    if ($pos_linkedin !== false) {
        $tab_linkedin = explode("?", $element->href);
        $linkedin = $tab_linkedin[0];
    }
    /*
     * Scoop.it
     */
    $pos_scoop = stripos($element->href, "scoop.it");
    if ($pos_scoop !== false) {
        $tab_scoop = explode("?", $element->href);
        $scoop = $tab_scoop[0];
    }
    /*
     * Google Plus
     */
    $pos_google = stripos($element->href, "google.com/");
    if ($pos_google !== false) {
        $tab_google = explode("?", $element->href);
        $google = $tab_google[0];
    }
    /*
     * Youtube
     */
    $pos_youtube = stripos($element->href, "youtube.com/");
    if ($pos_youtube !== false) {
        $tab_youtube = explode("?", $element->href);
        $youtube = $tab_youtube[0];
    }
    /*
     * Pinterest
     */
    $pos_pinterest = stripos($element->href, "pinterest.com");
    if ($pos_pinterest !== false) {
        $tab_pinterest = explode("?", $element->href);
        $pinterest = $tab_pinterest[0];
    }
    /*
     * RSS FEED
     */
    $pos_feed = stripos($element->href, "feed");
    if ($pos_feed !== false) {
        $tab_feed = explode("?", $element->href);
        $feed = $tab_feed[0];
    }

    /*
     * Blog
     */
    $pos_blog = stripos($element->href, "blog");
    if ($pos_blog !== false) {
        $tab_blog = explode("?", $element->href);
        $blog1 = $tab_blog[0];


        $blog = $element->href;
    }

    /*
     * Email
     */
    $pos_mailto = stripos($element->href, "mailto");
    if ($pos_mailto !== false) {
        $mailto = str_replace("mailto:", "", $element->href);
    }
    /*
     * Mentions legales
     */
    $pos_legal1 = stripos($element->href, "legal");
    $pos_legal2 = stripos($element->href, "conditions-generales");
    $pos_legal3 = stripos($element->href, "cgv");
    $pos_legal4 = stripos($element->href, "cgu");
    $pos_legal5 = stripos($element->href, "terms");
    if (($pos_legal1 !== false) || ($pos_legal2 !== false) || ($pos_legal3 !== false) || ($pos_legal4 !== false) || ($pos_legal5 !== false)) {
        $legal_link = stripos($element->href, $domaine);
        if (($legal_link !== false))
            $legal1 = $element->href;
        else
            $legal1 = "http://" . $domaine . $element->href;


        $html_legal = file_get_html($legal1);
        /*
         * Numéro de SIRET
         */
        preg_match_all("/\b\d{14}\b/", $html_legal, $matches_siret_all, PREG_PATTERN_ORDER);
        preg_match_all("/\b\d{3}[\s]?\d{3}[\s]?\d{3}\b/", $html_legal, $matches_siret, PREG_PATTERN_ORDER);
        $my_siret2 = $matches_siret[0][0];
        $my_siret1 = $matches_siret_all[0][0];
        if ($my_siret2 != '')
            $my_siret = $my_siret2;
        else
            $my_siret = $my_siret1;
    }
}


$rs = "[";


$rs.="{facebook:'" . ltrim(rtrim(addslashes($facebook))) . "',twitter:'" . ltrim(rtrim(addslashes($twitter))) . "',youtube:'" . ltrim(rtrim(addslashes($youtube))) . "',linkedin:'" . ltrim(rtrim(addslashes($linkedin))) . "',scoop:'" . ltrim(rtrim(addslashes($scoop))) . "',google:'" . ltrim(rtrim(addslashes($google))) . "',pinterest:'" . ltrim(rtrim(addslashes($pinterest))) . "',rss:'" . ltrim(rtrim(addslashes($feed))) . "',blog:'" . ltrim(rtrim(addslashes($blog))) . "',email:'" . ltrim(rtrim(addslashes($mailto))) . "',siret:'" . ltrim(rtrim(addslashes($my_siret))) . "',title:'" . ltrim(rtrim(addslashes($tags_en))) . "',description:'" . ltrim(rtrim(addslashes($description))) . "',short_fr:'" . ltrim(rtrim(addslashes($titre))) . "'},";



$rs.="]";
echo $rs;
?>
