<?php

include("db.php");
include("simple_html_dom.php");
ini_set("display_errors", 1);
error_reporting(1);

function slugify($text) {
    // replace all non letters or digits by -
    $text = preg_replace('/\W+/', '-', $text);

    // trim and lowercase
    $text = strtolower(trim($text, '-'));

    return $text;
}

$html = file_get_html($_GET['field']);
//print_r($html);

foreach ($html->find('div.accroche') as $article) {

    $d1 = $article->find('p', 0)->plaintext . "<br>";
    $d2 = $article->find('p', 1)->plaintext . "<br>";
    $d3 = $article->find('p', 2)->plaintext . "<br>";
    $description = $d1 . $d2 . $d3;
    $t1 = explode(",", $article->find('p', 0)->plaintext);
    $t2 = explode("au capital", $t1[1]);
    $forme_juridique = $t2[0];
}

foreach ($html->find('#fiche_entreprise') as $article1) {
    $startup = $article1->find('h1', 0)->plaintext;
}


foreach ($html->find('table.infoGen') as $article) {
    $adr = explode("Voir la carte", $article->find('div[itemprop=address]', 0)->plaintext);
   
    $adresse = str_replace("&nbsp;", " ", $adr[0]);
}

foreach ($html->find('tr.dateCreation') as $article) {
    $datec1 = explode("le", $article->find('td', 1)->plaintext);
    $datec2 = explode("/", trim($datec1[1]));
    $datec = trim($datec2[1]) . "-" . trim($datec2[0]) . "-" . trim($datec2[2]);
}

foreach ($html->find('tr.ca') as $article) {
    $ca1 = explode("+", $article->find('td', 1)->plaintext);
    $ca = $ca1[0];
}


foreach ($html->find('tr.capital') as $article) {
    $capital_social = str_replace(array("&nbsp;&euro;", " "), array("", ""), $article->find('td', 1)->plaintext);
}

foreach ($html->find('tr.siret') as $article) {
    $siret = $article->find('td', 1)->plaintext;
}
foreach ($html->find('div.company-desc') as $article2) {
    $text = $article2->find('span[id=verif_fiche.code.naf]', 0)->plaintext . "<br>";

    $tab = explode("/", $text);
    $code_naf = strip_tags($tab[0]);
    $libelle_naf = strip_tags($tab[1]);
}

//Dirigeant 1
if ($html->find('table.dirigeants tr', 0)) {
    $dir1 = $html->find('table.dirigeants tr', 0);
    $dir1_val1 = $dir1->find('td', 1);
    $dir1_val = trim($dir1_val1->find('a', 0)->plaintext);
} else {
    $dir1_val = "";
}

//Dirigeant 2
if ($html->find('table.dirigeants tr', 1)) {
    $dir2 = $html->find('table.dirigeants tr', 1);
    $dir2_val1 = $dir2->find('td', 1);
    $dir2_val = trim($dir2_val1->find('a', 0)->plaintext);
} else {
    $dir2_val = "";
}

//Dirigeant 3
if ($html->find('table.dirigeants tr', 2)) {
    $dir3 = $html->find('table.dirigeants tr', 2);
    $dir3_val1 = $dir3->find('td', 1);
    $dir3_val = trim($dir3_val1->find('a', 0)->plaintext);
} else {
    $dir3_val = "";
}

$rs = "[";


$rs.="{description:'" . ltrim(rtrim(addslashes($description))) . "',adresse:'" . str_replace(array("  ", "\n", "\r", "\t"), array("", "", "", ""), ltrim(rtrim(addslashes($adresse)))) . "',creation:'" . ltrim(rtrim(addslashes($datec))) . "',ca:'" . ltrim(rtrim(addslashes($ca))) . "',capital:'" . ltrim(rtrim(addslashes($capital_social))) . "',siret:'" . ltrim(rtrim(addslashes($siret))) . "',naf:'" . ltrim(rtrim(addslashes($code_naf))) . "',libelle:'" . ltrim(rtrim(addslashes($libelle_naf))) . "',juridique:'" . ltrim(rtrim(addslashes($forme_juridique))) . "',startup:'" . ltrim(rtrim(addslashes($startup))) . "',dirigeant1:'" . ltrim(rtrim(addslashes($dir1_val))) . "',dirigeant2:'" . ltrim(rtrim(addslashes($dir2_val))) . "',dirigeant3:'" . ltrim(rtrim(addslashes($dir3_val))) . "'},";



$rs.="]";
echo $rs;
?>
