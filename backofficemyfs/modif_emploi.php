<?php
include('db.php');
$menu = 6;


if (isset($_GET['sup'])) {
    $startup = mysqli_fetch_array(mysqli_query($link, "select * from offre_emploi where id=" . $_GET['sup']));
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ajouter Startup</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        <link rel="stylesheet" href="assets/css/datepicker.css">
        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>

        <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>


        <link rel="stylesheet" href="textext/css/textext.core.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.tags.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.autocomplete.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.focus.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.prompt.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.arrow.css" type="text/css" />
        <script src="textext/js/textext.core.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.tags.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.autocomplete.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.suggestions.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.filter.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.focus.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.prompt.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.ajax.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.arrow.js" type="text/javascript" charset="utf-8"></script>

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style type="text/css" media="screen">
            .map_canvas {
                float: left;
            }
            .text-wrap{position: relative !important}
            .big_titre{
                padding: 10px;
                font-size: 20px;
                cursor: pointer;
                margin-bottom: 10px;
                margin-top: 10px;
            }
            .btn-primary{
                width:120px
            }
        </style>



    </head>

    <body>
        <div id="loader" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/site.gif"  /></div>
        <div id="loader2" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/siret.gif"  /></div>
        <div id="loader1" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/verif.gif"  /></div>
        <div id="loader3" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/Linkedin.gif"  /></div>
        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->


        <div class="page-content" id="page-content">

            <!-- Main sidebar -->

            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <div class="card">


                    <div class="card-body">
                        <div class="tabbable">


                            <div class="tab-content">
                                <div class="tab-pane active" id="bordered-justified-pill1">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form method="post" action="add.php" enctype="multipart/form-data">
                                                <div class=" bg-indigo-400 big_titre" id="info">Informations générales</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="map_canvas"></div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Lien de verif.com</div>
                                                        <div class="col-md-3"><input type="text" class="form-control" value="https://www.verif.com/societe/" name="verif" id="verif" placeholder="https://www.verif.com/societe/STARTUP-798203774/" /></div>
                                                        <div class="col-md-1"><button type="button" onclick="verif_verif()" class="btn btn-success" >V&eacute;rifier</button></div>
                                                        <div class="col-md-2">Site web</div>
                                                        <div class="col-md-3"><input type="text" value="<?php echo $startup['siteweb'] ?>" class="form-control" name="site" id="site" /></div>
                                                        <div class="col-md-1"> <button type="button" class="btn btn-success" onclick="verifsite()">V&eacute;rifier</button></div>

                                                    </div>

                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Nom Startup</div>
                                                        <div class="col-md-4"><input type="text" class="form-control"  name="nom" value="<?php echo stripslashes($startup['startup']); ?>" id="nom" required /></div>
                                                        <div class="col-md-2">Nom juridique</div>
                                                        <div class="col-md-4"><input type="text" class="form-control" name="societe" value="" id="societe" required /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Date de cr&eacute;ation</div>
                                                        <div class="col-md-4"><input type="text" name="cal" id="dp1" autocomplete="off" value="" class="datepicker form-control" onclick="refrech_datepicker()"></div>
                                                        <div class="col-md-2">Effectif</div>
                                                        <div class="col-md-4"><input type="text" class="form-control" name="effectif" value="<?php echo $startup['effectif'] ?>" id="effectif" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">

                                                        <div class="col-md-2">Email</div>
                                                        <div class="col-md-4"><input type="text" class="form-control" value="<?php echo $startup['email'] ?>" name="email" id="email" /></div>
                                                        <div class="col-md-2">T&eacute;l&eacute;phone</div>
                                                        <div class="col-md-4"><input type="text" name="tel" id="telephone" value="<?php echo $startup['tel'] ?>" class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">

                                                    </div>

                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Twitter</div>
                                                        <div class="col-md-4"><input type="text" name="twitter" id="twitter" value="<?php echo $startup['twitter'] ?>"  class="form-control" /></div>

                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Youtube</div>
                                                        <div class="col-md-4"><input type="text" name="youtube" id="youtube" value="<?php echo $startup['youtube'] ?>" class="form-control" /></div>
                                                        <div class="col-md-2">Facebook</div>
                                                        <div class="col-md-4"><input type="text" name="facebook" id="facebook" value="<?php echo $startup['facebook'] ?>"  class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Viadeo</div>
                                                        <div class="col-md-4"><input type="text" name="viadeo" id="viadeo"  class="form-control" /></div>
                                                        <div class="col-md-2">Linkedin</div>
                                                        <div class="col-md-3"><input type="text" name="linkedin" id="linkedin" value="<?php echo $startup['linkedin'] ?>"  class="form-control" /></div>
                                                        <div class="col-md-1"> <button type="button" class="btn btn-success" onclick="verifphontom()">V&eacute;rifier</button></div>                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Blog</div>
                                                        <div class="col-md-4"><input type="text" name="blog" id="blog" class="form-control" /></div>
                                                        <div class="col-md-2">Google Plus</div>
                                                        <div class="col-md-4"><input type="text" name="google" id="google" class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Pinterest</div>
                                                        <div class="col-md-4"><input type="text" name="pinterest" id="pinterest" class="form-control"/></div>
                                                        <div class="col-md-2">RSS</div>
                                                        <div class="col-md-4"><input type="text" name="rss" id="rss"  class="form-control" /></div>
                                                    </div>

                                                </div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">

                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Adresse</div>
                                                        <div class="col-md-10"><input type="text" class="form-control" name="adresse" value="<?php echo $startup['adress_startup'] ?>"  placeholder="Entrez votre adresse" id="geocomplete" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Code Postal</div>
                                                        <div class="col-md-4"><input type="text" name="postal_code" class="form-control" /></div>
                                                        <div class="col-md-2">Ville</div>
                                                        <div class="col-md-4"><input type="text" name="locality" class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">LAT</div>
                                                        <div class="col-md-4"><input type="text" name="lat" class="form-control" /></div>
                                                        <div class="col-md-2">LNG</div>
                                                        <div class="col-md-4"><input type="text" name="lng" class="form-control" /></div>
                                                    </div>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="jurid">Informations juridique</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Siret</div>
                                                        <div class="col-md-4"><input type="text" name="siret" value="" id="siret" class="form-control" /></div>
                                                        <div class="col-md-2">Capital Social</div>
                                                        <div class="col-md-4"><input type="text" name="capital" id="capital" class="form-control"  /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">NAF</div>
                                                        <div class="col-md-4"><input type="text" name="naf" id="naf" class="form-control" /></div>
                                                        <div class="col-md-2">Libell&eacute; NAF</div>
                                                        <div class="col-md-4"><input type="text" name="libnaf" id="libnaf" class="form-control"  /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Forme Juridique</div>
                                                        <div class="col-md-4"><input type="text" name="juridique" id="juridique" class="form-control" /></div>
                                                    </div>
                                                </div>


                                                <div class=" bg-indigo-400 big_titre" id="list_sect" >Secteur d'activité</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Accroche FR</div>
                                                        <div class="col-md-8"><input type="text" name="short_fr" value="<?php echo utf8_encode($startup['short_desc']) ?>" class="form-control" id="short_fr" required onblur="copier()" /></div>
                                                        <div class="col-md-2"><button type="button" class="btn btn-info" onclick="traduire_accroche()" >Traduire</button></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Description FR</div>
                                                        <div class="col-md-8"><textarea name="long_fr" id="long_fr" class="form-control" required><?php echo utf8_encode($startup['long_desc']) ?></textarea>
                                                            <script type="text/javascript">CKEDITOR.replace('long_fr');</script></div>
                                                        <div class="col-md-2"><button type="button" class="btn btn-info" onclick="traduire_long()" >Traduire</button></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Accroche EN</div>
                                                        <div class="col-md-10"><input type="text" name="short_en" class="form-control" value="" id="short_en" required onblur="copier()" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Description EN</div>
                                                        <div class="col-md-10"><textarea name="long_en" id="long_en" ></textarea>
                                                            <script type="text/javascript">CKEDITOR.replace('long_en');</script></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Tags</div>
                                                        <div class="col-md-4"><input type="text" name="tags" id="tags" value="<?php echo $startup['Tags'] ?>" class="form-control" required /></div>
                                                        <div class="col-md-2">Cible</div>
                                                        <div class="col-md-4">
                                                            <input type="checkbox" value="B2C" name="cible[]" style="margin-top: -3px; margin-right: 5px; width: 20px;" />
                                                            B2C &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <input type="checkbox" value="B2B" name="cible[]" style="margin-top: -3px; margin-right: 5px; width: 20px;" />
                                                            B2B &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <input type="checkbox" value="C2C" name="cible[]" style="margin-top: -3px; margin-right: 5px; width: 20px;" />
                                                            C2C
                                                        </div>
                                                    </div>



                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Secteur</div>
                                                        <div class="col-md-4"><select  name="secteur" class="form-control" id="secteur" onChange="getListSSecteur(this)" required>

                                                                <option></option>

                                                                <?php
                                                                $sql = mysqli_query($link, "select * from secteur order by nom_secteur");
                                                                while ($data = mysqli_fetch_array($sql)) {
                                                                    ?>
                                                                    <option value="<?php echo $data['id']; ?>"><?php echo $data['nom_secteur']; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select></div>
                                                        <div class="col-md-2">Sous Secteur</div>

                                                        <div class="col-md-4"><select name="sous_secteur" class="form-control" id="sous_secteur"></select></div>


                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Activite</div>
                                                        <div class="col-md-4">
                                                            <select  name="activ" id="activ" class="form-control" onChange="getListSActivite(this)" required>
                                                                <option></option>
                                                                <?php
                                                                $sql = mysqli_query($link, "select * from last_activite order by nom_activite");
                                                                while ($data = mysqli_fetch_array($sql)) {
                                                                    ?>
                                                                    <option value="<?php echo $data['id']; ?>"><?php echo $data['nom_activite']; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">Sous Activit&eacute;</div>
                                                        <div class="col-md-4"><select name="sous_activ" class="form-control" id="sous_activ"></select></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Logo</div>
                                                        <div class="col-md-4"><input type="file" name="logo"  class="form-control" /></div>
                                                        <div class="col-md-2">JEI</div>
                                                        <div class="col-md-4">
                                                            <input type="radio" value="1" name="jei" id="jei" style="width:20px; margin-left:20px;">
                                                            Oui
                                                            <input type="radio" checked="checked" value="0" name="jei" id="jei" style="width:20px; margin-left:20px;">
                                                            Non
                                                        </div>
                                                    </div>
                                                </div>
                                                <div  class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="col-md-12" id="bloc_ca">
                                                        <div id="bloc_min_ca" class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Chiffre Affaire</div>
                                                            <div class="col-md-4"><input type="text" name="ca[]" class="form-control" /></div>
                                                            <div class="col-md-2">Année</div>
                                                            <div class="col-md-4"><input type="text" name="anneeca[]" class="form-control" /></div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div id="add_ca">
                                                            <div class="col-lg-3">
                                                                <button onclick="add_ca();" type="button" class="btn btn-primary" >
                                                                    <i class="icon-plus-circle2 position-left"></i> Ajouter</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class=" bg-indigo-400 big_titre" id="fond1" > <i class="fa fa-plus-circle"> </i> Fondateur 1</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond1_bloc">
                                                    <table class="table"  border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td> Civilite </td>
                                                            <td ><select class="form-control" name="civf1" style="width: 200px;">
                                                                    <option value="0">M</option>
                                                                    <option value="1">Mme</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Pr&eacute;nom </td>
                                                            <td><input type="text" class="form-control" name="prenomf1" value="<?php echo $startup['prenom']; ?>" id="prenomf1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Nom </td>
                                                            <td><input type="text" class="form-control" name="nomf1" value="<?php echo $startup['nom'] ?>" id="nomf1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><select name="fonctionf1" id="fonctionf1" class="form-control" style="width: 200px;">
                                                                    <option value="">--Choisir fonction--</option>
                                                                    <option value="6"> CEO </option>
                                                                    <option value="9"> CTO </option>
                                                                    <option value="10"> COO </option>
                                                                    <option value="11"> CMO </option>
                                                                    <option value="12"> CFO </option>
                                                                    <option value="13"> Business Developper </option>
                                                                    <option value="2"> Co-fondateur </option>
                                                                    <option value="1"> Fondateur </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Email </td>
                                                            <td><input type="text" class="form-control" value="<?php echo $startup['email'] ?>" name="emailf1"  id="emailf1"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Tel </td>
                                                            <td><input type="text" class="form-control" value="<?php echo $startup['tel'] ?>" name="telf1" id="telf1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Mobile </td>
                                                            <td><input type="text" class="form-control" name="mobf1" id="mobf1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Linkedin </td>
                                                            <td><input type="text" class="form-control" name="linkedinf1" id="linkedinf1" style="display:inline;width:50%;" />
                                                                <button type="button" style="width:45%;float:right" class="btn btn-success form-control-inline" onclick="veriflinkedinFond1()">V&eacute;rifier</button></div>

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Viadeo </td>
                                                            <td><input type="text" class="form-control" name="viadeof1" id="viadeof1"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Image </td>
                                                            <td><input type="text" name="imgf1" class="form-control" id="imgf1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Experiences Professionnelles</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" class="form-control" name="societef11" id="societef11"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" class="form-control" name="fonctionf11" id="fonctionf11"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" class="form-control" name="societe_def11" id="societe_def11" ></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" class="form-control" name="societe_af11" id="societe_af11"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" class="form-control" name="societef12" id="societef12"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" class="form-control" name="fonctionf12" id="fonctionf12"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" class="form-control" name="societe_def12" id="societe_def12"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" class="form-control" name="societe_af12" id="societe_af12"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" class="form-control" name="societef13" id="societef13"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" class="form-control" name="fonctionf13" id="fonctionf13"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" class="form-control" name="societe_def13" id="societe_def13"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" class="form-control" name="societe_af13" id="societe_af13"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" class="form-control" name="societef14" id="societef14"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" class="form-control" name="fonctionf14" id="fonctionf14"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" class="form-control" name="societe_def14" id="societe_def14"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" class="form-control" name="societe_af14" id="societe_af14"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" class="form-control" name="societef15" id="societef15"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" class="form-control" name="fonctionf15" id="fonctionf15"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" class="form-control" name="societe_def15" id="societe_def15"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" class="form-control" name="societe_af15" id="societe_af15"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Formation Acad&eacute;mique</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" class="form-control" name="ecolef11" id="ecolef11"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" class="form-control" name="diplomef11" id="diplomef11"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" class="form-control" name="ecole_def11" id="ecole_def11"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" class="form-control" name="ecole_af11" id="ecole_af11"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" class="form-control" name="ecolef12" id="ecolef12"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" class="form-control" name="diplomef12" id="diplomef12"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" class="form-control" name="ecole_def12" id="ecole_def12"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" class="form-control" name="ecole_af12" id="ecole_af12"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" class="form-control" name="ecolef13" id="ecolef13"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" class="form-control" name="diplomef13" id="diplomef13"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" class="form-control" name="ecole_def13" id="ecole_def13"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" class="form-control" name="ecole_af13" id="ecole_af13"></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="fond2" > <i class="fa fa-plus-circle"> </i> Fondateur 2</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond2_bloc">
                                                    <table class="table"  border="0" cellspacing="0" cellpadding="0">

                                                        <tr>
                                                            <td> Civilite </td>
                                                            <td >
                                                                <select name="civf2" class="form-control" style="width: 200px">
                                                                    <option value="0">M</option>
                                                                    <option value="1">Mme</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Pr&eacute;nom </td>
                                                            <td><input type="text" name="prenomf2" id="prenomf2" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Nom </td>
                                                            <td><input type="text" name="nomf2" id="nomf2" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td>
                                                                <select name="fonctionf2" id="fonctionf2" class="form-control" style="width: 200px;">
                                                                    <option value="">--Choisir fonction--</option>
                                                                    <option value="6"> CEO </option>
                                                                    <option value="9"> CTO </option>
                                                                    <option value="10"> COO </option>
                                                                    <option value="11"> CMO </option>
                                                                    <option value="12"> CFO </option>
                                                                    <option value="13"> Business Developper </option>
                                                                    <option value="2"> Co-fondateur </option>
                                                                    <option value="1"> Fondateur </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Email </td>
                                                            <td><input type="text" name="emailf2" id="emailf2" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Tel </td>
                                                            <td><input type="text" value="" name="telf2" id="telf2" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Mobile </td>
                                                            <td><input type="text" name="mobf2" id="mobf2" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Linkedin </td>
                                                            <td><input type="text" name="linkedinf2" id="linkedinf2" class="form-control" style="display:inline;width:50%;" />
                                                                <button type="button" style="width:45%;float:right" class="btn btn-success form-control-inline" onclick="veriflinkedinFond2()">V&eacute;rifier</button></div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Viadeo </td>
                                                            <td><input type="text" value="" name="viadeof2" id="viadeof2" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Image </td>
                                                            <td><input type="text" name="imgf2" id="imgf2" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Experiences Professionnelles</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef21" id="societef21" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf21" id="fonctionf21" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def21" id="societe_def21" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af21" id="societe_af21" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef22" id="societef22" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf22" id="fonctionf22" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def22" id="societe_def22" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af22" id="societe_af22" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef23" id="societef23" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf23" id="fonctionf23" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def23" id="societe_def23" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af23" id="societe_af23" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef24" id="societef24" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf24" id="fonctionf24" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def24" id="societe_def24" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af24" id="societe_af24" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef25" id="societef25" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf25" id="fonctionf25" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def25" id="societe_def25" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af25" id="societe_af25" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Formation Acad&eacute;mique</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef21" id="ecolef21" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef21" id="diplomef21" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def21" id="ecole_def21" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af21" id="ecole_af21" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef22" id="ecolef22" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef22" id="diplomef22" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def22" id="ecole_def22" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af22" id="ecole_af22" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef23" id="ecolef23" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef23" id="diplomef23" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def23" id="ecole_def23" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af23" id="ecole_af23" class="form-control"></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="fond3" > <i class="fa fa-plus-circle"> </i> Fondateur 3</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond3_bloc">
                                                    <table class="table" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td colspan="2"><strong>Fondateurs 3</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Civilite </td>
                                                            <td>
                                                                <select name="civf3" class="form-control" style="width: 200px;">
                                                                    <option value="0">M</option>
                                                                    <option value="1">Mme</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Pr&eacute;nom </td>
                                                            <td><input type="text" name="prenomf3" id="prenomf3" class="form-control"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Nom </td>
                                                            <td><input type="text" name="nomf3" id="nomf3" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td>
                                                                <select name="fonctionf3" id="fonctionf3" class="form-control" style="width: 200px;">
                                                                    <option value="">--Choisir fonction--</option>
                                                                    <option value="6"> CEO </option>
                                                                    <option value="9"> CTO </option>
                                                                    <option value="10"> COO </option>
                                                                    <option value="11"> CMO </option>
                                                                    <option value="12"> CFO </option>
                                                                    <option value="13"> Business Developper </option>
                                                                    <option value="2"> Co-fondateur </option>
                                                                    <option value="1"> Fondateur </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Email </td>
                                                            <td><input type="text" name="emailf3" id="emailf3" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Tel </td>
                                                            <td><input type="text" value="" name="telf3" id="telf3" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Mobile </td>
                                                            <td><input type="text" name="mobf3" id="mobf3" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Linkedin </td>
                                                            <td><input type="text" name="linkedinf3" id="linkedinf3" class="form-control" style="display:inline;width:50%;" />
                                                                <button type="button" style="width:45%;float:right" class="btn btn-success form-control-inline" onclick="veriflinkedinFond3()">V&eacute;rifier</button></div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Viadeo </td>
                                                            <td><input type="text" value="" name="viadeof3" id="viadeof3" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Image </td>
                                                            <td><input type="text" name="imgf3" id="imgf3" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Experiences Professionnelles</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef31" id="societef31" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf31" id="fonctionf31" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def31" id="societe_def31" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af31" id="societe_af31" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef32" id="societef32" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf32" id="fonctionf32" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def32" id="societe_def32" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af32" id="societe_af32" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef33" id="societef33" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf33" id="fonctionf33" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def33" id="societe_def33" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af33" id="societe_af33" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef34" id="societef34" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf34" id="fonctionf34" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def34" id="societe_def34" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af34" id="societe_af34" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef35" id="societef35" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf35" id="fonctionf35" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def35" id="societe_def35" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af35" id="societe_af35" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Formation Acad&eacute;mique</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef31" id="ecolef31" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef31" id="diplomef31" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def31" id="ecole_def31" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af31" id="ecole_af31" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef32" id="ecolef32" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef32" id="diplomef32" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def32" id="ecole_def32" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af32" id="ecole_af32" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef33" id="ecolef33" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef33" id="diplomef33" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def33" id="ecole_def33" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af33"  id="ecole_af33" class="form-control"></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="fond4"> </i> Fondateur 4</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond4_bloc">
                                                    <table class="table" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td colspan="2"><strong>Fondateurs 4</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Civilite </td>
                                                            <td>
                                                                <select name="civf4" class="form-control" style="width: 200px;">
                                                                    <option value="0">M</option>
                                                                    <option value="1">Mme</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Pr&eacute;nom </td>
                                                            <td><input type="text" name="prenomf4" id="prenomf4" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Nom </td>
                                                            <td><input type="text" name="nomf4" id="nomf4" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td>
                                                                <select id="fonctionf4" name="fonctionf4" class="form-control" style="width: 200px;">
                                                                    <option value="">--Choisir fonction--</option>
                                                                    <option value="6"> CEO </option>
                                                                    <option value="9"> CTO </option>
                                                                    <option value="10"> COO </option>
                                                                    <option value="11"> CMO </option>
                                                                    <option value="12"> CFO </option>
                                                                    <option value="13"> Business Developper </option>
                                                                    <option value="2"> Co-fondateur </option>
                                                                    <option value="1"> Fondateur </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Email </td>
                                                            <td><input type="text" name="emailf4" id="emailf4" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Tel </td>
                                                            <td><input type="text" value="" name="telf4" id="telf4" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Mobile </td>
                                                            <td><input type="text" name="mobf4" id="mobf4" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Linkedin </td>
                                                            <td><input type="text" name="linkedinf4" id="linkedinf4" class="form-control" style="display:inline;width:50%;" />
                                                                <button type="button" style="width:45%;float:right" class="btn btn-success form-control-inline" onclick="veriflinkedinFond4()">V&eacute;rifier</button></div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Viadeo </td>
                                                            <td><input type="text" value="" name="viadeof4" id="viadeof4" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Image </td>
                                                            <td><input type="text" name="imgf4" id="imgf4" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Experiences Professionnelles</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef41" id="societef41" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf41" id="fonctionf41" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def41" id="societe_def41" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af41" id="societe_af41" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef42" id="societef42" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf42" id="fonctionf42" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def42" id="societe_def42" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af42" id="societe_af42" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef43" id="societef43" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf43" id="fonctionf43" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def43" id="societe_def43" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af43" id="societe_af43" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef44" id="societef44" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf44" id="fonctionf44" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def44" id="societe_def44" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af44" id="societe_af44" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef45" id="societef45" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf45" id="fonctionf45" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def45" id="societe_def45" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af45" id="societe_af45" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Formation Acad&eacute;mique</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef41" id="ecolef41" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef41" id="diplomef41" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def41" id="ecole_def41" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af41" id="ecole_af41" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef42" id="ecolef42" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef42" id="diplomef42" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def42" id="ecole_def42" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af42" id="ecole_af42" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef43" id="ecolef43" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef43" id="diplomef43" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def43" id="ecole_def43" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af43" id="ecole_af43" class="form-control"></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="fond5"> <i class="fa fa-plus-circle"> </i> Fondateur 5</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond5_bloc">
                                                    <table class="table" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td colspan="2"><strong>Fondateurs 5</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Civilite </td>
                                                            <td>
                                                                <select name="civf5" class="form-control" style="width: 200px;">
                                                                    <option value="0">M</option>
                                                                    <option value="1">Mme</option>
                                                                </select></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Pr&eacute;nom </td>
                                                            <td><input type="text" name="prenomf5" id="prenomf5"  class="form-control"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Nom </td>
                                                            <td><input type="text" name="nomf5" id="nomf5" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><select name="fonctionf5" id="fonctionf5" class="form-control" style="width: 200px;">
                                                                    <option value="">--Choisir fonction--</option>
                                                                    <option value="6"> CEO </option>
                                                                    <option value="9"> CTO </option>
                                                                    <option value="10"> COO </option>
                                                                    <option value="11"> CMO </option>
                                                                    <option value="12"> CFO </option>
                                                                    <option value="13"> Business Developper </option>
                                                                    <option value="2"> Co-fondateur </option>
                                                                    <option value="1"> Fondateur </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Email </td>
                                                            <td><input type="text" name="emailf5" id="emailf5" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Tel </td>
                                                            <td><input type="text" value="" name="telf5" id="telf5" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Mobile </td>
                                                            <td><input type="text" name="mobf5" id="mobf5"  class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Linkedin </td>
                                                            <td><input type="text" name="linkedinf5" id="linkedinf5" class="form-control" style="display:inline;width:50%;" />
                                                                <button type="button" style="width:45%;float:right" class="btn btn-success form-control-inline" onclick="veriflinkedinFond5()">V&eacute;rifier</button></div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Viadeo </td>
                                                            <td><input type="text" value="" name="viadeof5" id="viadeof5" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Image </td>
                                                            <td><input type="text" name="imgf5" id="imgf5" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Experiences Professionnelles</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef51" id="societef51" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf51" id="fonctionf51" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def51" id="societe_def51" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af51" id="societe_af51" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef52" id="societef52" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf52" id="fonctionf52" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def52" id="societe_def52" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af52" id="societe_af52" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef53" id="societef53" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf53" id="fonctionf53" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def53" id="societe_def53" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af53" id="societe_af53" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef54" id="societef54" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf54" id="fonctionf54" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def54" id="societe_def54" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af54" id="societe_af54" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef55" id="societef55" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf55"  id="fonctionf55" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def55" id="societe_def55" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af55" id="societe_af55" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Formation Acad&eacute;mique</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef51" id="ecolef51" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef51" id="diplomef51" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def51" id="ecole_def51" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af51" id="ecole_af51" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef52" id="ecolef52" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef52" id="diplomef52" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def52" id="ecole_def52" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af52" id="ecole_af52" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef53" id="ecolef53" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef53" id="diplomef53" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def53" id="ecole_def53" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af53" id="ecole_af53" class="form-control"></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="fond6"> <i class="fa fa-plus-circle"> </i> Fondateur 6</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond6_bloc">
                                                    <table class="table" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td colspan="2"><strong>Fondateurs 6</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Civilite </td>
                                                            <td>
                                                                <select name="civf6" class="form-control" style="width: 200px;">
                                                                    <option value="0">M</option>
                                                                    <option value="1">Mme</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Pr&eacute;nom </td>
                                                            <td><input type="text" name="prenomf6" id="prenomf6"  class="form-control"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Nom </td>
                                                            <td><input type="text" name="nomf6" id="nomf6" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td>
                                                                <select name="fonctionf6" id="fonctionf6" class="form-control" style="width: 200px;">
                                                                    <option value="">--Choisir fonction--</option>
                                                                    <option value="6"> CEO </option>
                                                                    <option value="9"> CTO </option>
                                                                    <option value="10"> COO </option>
                                                                    <option value="11"> CMO </option>
                                                                    <option value="12"> CFO </option>
                                                                    <option value="13"> Business Developper </option>
                                                                    <option value="2"> Co-fondateur </option>
                                                                    <option value="1"> Fondateur </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Email </td>
                                                            <td><input type="text" name="emailf6" id="emailf6" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Tel </td>
                                                            <td><input type="text" value="" name="telf6" id="telf6" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Mobile </td>
                                                            <td><input type="text" name="mobf6" id="mobf6"  class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Linkedin </td>
                                                            <td><input type="text" name="linkedinf6" id="linkedinf6"  class="form-control" style="display:inline;width:50%;" />
                                                                <button type="button" style="width:45%;float:right" class="btn btn-success form-control-inline" onclick="veriflinkedinFond6()">V&eacute;rifier</button></div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Viadeo </td>
                                                            <td><input type="text" value="" name="viadeof6" id="viadeof6" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Image </td>
                                                            <td><input type="text" name="imgf6" id="imgf6" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Experiences Professionnelles</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef61" id="societef61"  class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf61" id="fonctionf61" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def61" id="societe_def61" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af61" id="societe_af61" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef62" id="societef62" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf62" id="fonctionf62" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def62"  id="societe_def62" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af62"  id="societe_af62" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef63" id="societef63" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf63" id="fonctionf63" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def63" id="societe_def63" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af63" id="societe_af63" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef64" id="societef64" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf64" id="fonctionf64" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def64" id="societe_def64" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af64" id="societe_af64"  class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Soci&eacute;t&eacute; </td>
                                                            <td><input type="text" value="" name="societef65" id="societef65" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><input type="text" value="" name="fonctionf65"  id="fonctionf65" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" value="" name="societe_def65" id="societe_def65" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" value="" name="societe_af65"  id="societe_af65" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Formation Acad&eacute;mique</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef61" id="ecolef61" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef61" id="diplomef61" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def61" id="ecole_def61" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af61" id="ecole_af61" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef62" id="ecolef62" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef62" id="diplomef62" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def62" id="ecole_def62" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af62" id="ecole_af62" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"> *********************** </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ecole </td>
                                                            <td><input type="text" name="ecolef63" id="ecolef63" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Diplome </td>
                                                            <td><input type="text" name="diplomef63" id="diplomef63" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> De </td>
                                                            <td><input type="text" name="ecole_def63" id="ecole_def63" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&agrave; </td>
                                                            <td><input type="text" name="ecole_af63" id="ecole_af63" class="form-control"></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="lf">Deals</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="col-md-12" id="bloc_lf">
                                                        <div id="bloc_min_lf" class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Deal</div>
                                                            <div class="col-md-4">
                                                                <select name="rachat[]" class="form-control">
                                                                    <option value="" selected="selected"></option>
                                                                    <option value="0">Lev&eacute;e de fonds</option>
                                                                    <option value="1">Rachat</option>
                                                                    <option value="2">IPO</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">Montant</div>
                                                            <div class="col-md-4"><input type="text" name="montant_lf[]" class="form-control"></div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Date</div>
                                                            <div class="col-md-4">
                                                                <input type="text" autocomplete="off" class="datepicker form-control" onclick="refrech_datepicker()" name="date_lf[]">
                                                            </div>
                                                            <div class="col-md-2">Investisseurs</div>
                                                            <div class="col-md-4">

                                                                <textarea class="example list_invest_tag" name="de[]" style="width: 320px"></textarea>
                                                                <input name="de1[]" class="form-control" type="text" placeholder="Autres" />



                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a class="delete_lf_ligne">
                                                                <i style="color:#EC407A" class="fa fa-minus-circle fa-2x"></i>
                                                            </a>
                                                        </div>

                                                    </div>

                                                    <div class="row">
                                                        <div id="add_lf">
                                                            <div class="col-lg-3">
                                                                <button onclick="add_lf();" type="button" class="btn btn-primary">
                                                                    <i class="icon-plus-circle2 position-left"></i> Ajouter</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="deal_flow">Recherche de fonds</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Montant recherché (k€)</div>
                                                        <div class="col-md-4"><input type="text" name="montant_rech"  class="form-control" /></div>
                                                        <div class="col-md-2">Date de clotûre</div>
                                                        <div class="col-md-4"><input type="text" name="date_rech"  autocomplete="off" class="datepicker form-control" onclick="refrech_datepicker()" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Type d'investisseurs recherchés :</div>
                                                        <div class="col-md-10">
                                                            <div class="col-md-3 col-sm-4">
                                                                <input type="checkbox" value="Business Angels" name="rech[]" style="width: 20px;">
                                                                Business Angels
                                                            </div>
                                                            <div class="col-md-3 col-sm-4">
                                                                <input type="checkbox" value="Fonds d'investissements" name="rech[]" style="width: 20px;">
                                                                Fonds d'investissements
                                                            </div>
                                                            <div class="col-md-3 col-sm-4">
                                                                <input type="checkbox" value="Crowdfunding" name="rech[]" style="width: 20px;">
                                                                Crowdfunding
                                                            </div>
                                                            <div class="col-md-3 col-sm-4">
                                                                <input type="checkbox" value="Fonds publics" name="rech[]" style="width: 20px;">
                                                                Fonds publics</div>
                                                        </div>
                                                        <div class="col-md-2">Objectif :</div>
                                                        <div class="col-md-10">

                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="Recrutement IT" name="objectif[]" style="width: 20px;">
                                                                Recrutement IT
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="Recrutement BizDev" name="objectif[]" style="width: 20px;">
                                                                Recrutement BizDev
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="R&amp;D" name="objectif[]" style="width: 20px;">
                                                                R&amp;D
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="Acquisition matériels" name="objectif[]" style="width: 20px;">
                                                                Acquisition matériels
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="International" name="objectif[]" style="width: 20px;">
                                                                International
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="Autres" name="objectif[]" style="width: 20px;">
                                                                Autres
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre">Défaillance</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">


                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Depot de bilan</div>
                                                        <div class="col-md-4">
                                                            <input type="radio" value="1" name="bilan" style="width:20px; margin-left:20px;">
                                                            Oui
                                                            <input type="radio" checked="checked" value="0" name="bilan" style="width:20px; margin-left:20px;">
                                                            Non
                                                        </div>
                                                        <div class="col-md-2">Date de dep&ocirc;t de bilan</div>
                                                        <div class="col-md-4"><input type="text" name="datebilan"  autocomplete="off" class="datepicker form-control" onclick="refrech_datepicker()" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Statut</div>
                                                        <div class="col-md-4">
                                                            <select  class="form-control">
                                                                <option></option>
                                                                <option value="Entreprise en défaillance">Entreprise en défaillance</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">Motif</div>
                                                        <div class="col-md-4">
                                                            <select  class="form-control" name="motif">
                                                                <option></option>
                                                                <option value="Annulation de la procédure de redressement juidiciaire">Annulation de la procédure de redressement juidiciaire</option>
                                                                <option value="Arrêt du plan de sauvegarde">Arrêt du plan de sauvegarde</option>
                                                                <option accesskey="Changement de liquidateur judiciaire">Changement de liquidateur judiciaire</option>
                                                                <option value="Clôture pour insuffisance d'actif">Clôture pour insuffisance d'actif</option>
                                                                <option value="Conciliation">Conciliation</option>
                                                                <option value="Conversion en liquidation judiciaire">Conversion en liquidation judiciaire</option>
                                                                <option value="Dépôt de l'état des créances">Dépôt de l'état des créances</option>
                                                                <option value="Dépôt des créances nées après jugement">Dépôt des créances nées après jugement</option>
                                                                <option value="Liquidation judiciaire">Liquidation judiciaire</option>
                                                                <option value="Liquidation judiciaire simplifiée">Liquidation judiciaire simplifiée</option>
                                                                <option value="Plan de cession totale">Plan de cession totale</option>
                                                                <option value="Procédure de sauvegarde">Procédure de sauvegarde</option>
                                                                <option value="Redressement Judiciaire">Redressement Judiciaire</option>
                                                                <option value="Règlement amiable">Règlement amiable</option>
                                                                <option value="Rétractation de redressement judiciaire sur tierce opposition">Rétractation de redressement judiciaire sur tierce opposition</option>
                                                                <option value="Sauvegarde Jugement">Sauvegarde Jugement</option>

                                                            </select>
                                                        </div>

                                                    </div>


                                                </div>

                                                <div class=" bg-indigo-400 big_titre" id="actualite">News</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="col-md-12" id="bloc_actualite">
                                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Lien</div>
                                                            <div class="col-md-4"><input type="text" name="lien[]" id="news_link" class="form-control"></div>
                                                            <div class="col-md-4"><button type="button" id="" class="btn btn-info" onclick="getNews()">Get NEWS</button></div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Titre</div>
                                                            <div class="col-md-4"><input type="text" id="titre_news" name="titre[]" class="form-control"></div>
                                                            <div class="col-md-2">Description</div>
                                                            <div class="col-md-4"><input type="text" id="description_news" name="description[]" class="form-control"></div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-1">Date</div>
                                                            <div class="col-md-3"><input type="text" id="date_news" autocomplete="off" class="datepicker form-control" onclick="refrech_datepicker()"  name="dateactu[]"></div>

                                                            <div class="col-md-1">Source</div>
                                                            <div class="col-md-3"><input type="text" id="source_news" name="source[]" class="form-control"></div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a class="delete_actualite_ligne">
                                                                <i style="color:#EC407A" class="fa fa-minus-circle fa-2x"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div id="add_actualite">
                                                            <div class="col-lg-3">
                                                                <button onclick="add_actualite();" type="button" class="btn btn-primary">
                                                                    <i class="icon-plus-circle2 position-left"></i> Ajouter</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <table width="1134" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="center" colspan="2"><button type="submit" class="btn btn-success">Ajouter</button></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </form>

                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- /main content -->





        </div>

        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcCIjgD7FA1zVC6EQwS5V6I-6xuUuwMiQ&sensor=false&amp;libraries=places"></script>
        <script src="assets/js/jquery.geocomplete.js"></script>
        <script src="assets/js/bootstrap-datepicker.js"></script>
        <script src="script/function.js"></script>
        <script src="script/site.js"></script>
        <script src="script/siret.js"></script>
        <script src="script/verif.js"></script>
        <script src="script/verifphontom.js"></script>
        <script src="script/google_linkedin_company.js"></script>
        <script src="script/qualif_linkedin_company.js"></script>
        <script src="script/verif_seul.js"></script>
        <script>


                                                                    function remplire_tags() {
                                                                        $('.list_invest_tag')
                                                                                .textext({
                                                                                    plugins: 'tags autocomplete'
                                                                                })
                                                                                .bind('getSuggestions', function (e, data)
                                                                                {
                                                                                    var list = [
<?php
$sql_inves = mysqli_query($link, "select id,new_name from temp_lf group by new_name order by new_name");
while ($data_invest = mysqli_fetch_array($sql_inves)) {
    ?>
                                                                                            '<?php echo addslashes($data_invest['new_name']); ?>',
    <?php
}
?>
                                                                                    ],
                                                                                            textext = $(e.target).textext()[0],
                                                                                            query = (data ? data.query : '') || '';

                                                                                    $(this).trigger(
                                                                                            'setSuggestions',
                                                                                            {result: textext.itemManager().filter(list, query)}
                                                                                    );
                                                                                })
                                                                                ;
                                                                    }
                                                                    $(document).ready(function () {

                                                                        remplire_tags();
                                                                    }
                                                                    );

                                                                    function verif_verif() {
                                                                        var siren = document.getElementById("verif").value;
                                                                        if (siren != 'https://www.verif.com/societe/') {
                                                                            $.ajax({
                                                                                url: 'getverifcom.php?field=' + siren,
                                                                                beforeSend: function () {
                                                                                    $('#loader1').show();
                                                                                    document.getElementById("page-content").style.opacity = 0.5;
                                                                                },
                                                                                complete: function () {
                                                                                    $('#loader1').hide();
                                                                                    document.getElementById("page-content").style.opacity = 1;
                                                                                },
                                                                                success: function (data) {

                                                                                    var t = eval(data);
                                                                                    document.getElementById("geocomplete").value = t[0].adresse;
                                                                                    document.getElementById("siret").value = t[0].siret;
                                                                                    document.getElementById("dp1").value = t[0].creation;
                                                                                    document.getElementById("capital").value = t[0].capital;
                                                                                    document.getElementById("siret").value = t[0].siret;
                                                                                    document.getElementById("naf").value = t[0].naf;
                                                                                    document.getElementById("libnaf").value = t[0].libelle;
                                                                                    document.getElementById("juridique").value = t[0].juridique;
                                                                                    document.getElementById("nom").value = t[0].startup;
                                                                                    document.getElementById("societe").value = t[0].startup;
                                                                                    document.getElementById("prenomf1").value = t[0].dirigeant1;



                                                                                    google_linkedin_company();


                                                                                }
                                                                            });
                                                                        }
                                                                    }



        </script>
    </body>
</html>
