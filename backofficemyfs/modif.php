<?php
include('db.php');
$menu = 2;

$idstartup = $_GET['sup'];

$startup = mysqli_fetch_array(mysqli_query($link, 'select * from startup where id=' . $idstartup));

function getfonction($id) {
    $f1 = mysqli_fetch_array(mysqli_query($link, 'select nom_fr from fonction where id=' . $id . ' limit 1'));
    return $f1['nom_fr'];
}

function getTags($id) {
    $f1 = mysqli_fetch_array(mysqli_query($link, 'select tags from activite where id_startup=' . $id . ' limit 1'));
    return $f1['tags'];
}

function convert_date($date) {
    if ($date != "") {
        $split = explode("-", $date);
        $ma_date = $split[1] . "-" . $split[2] . "-" . $split[0];
    } else {
        $ma_date = "";
    }
    return $ma_date;
}

$rang_effectif = mysqli_fetch_array(mysqli_query($link, "select * from startup_effectif where id_startup=" . $idstartup . " order by annee desc limit 1"));
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Modifier Startup</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        <link rel="stylesheet" href="assets/css/datepicker.css">
        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>

        <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>


        <link rel="stylesheet" href="textext/css/textext.core.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.tags.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.autocomplete.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.focus.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.prompt.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.arrow.css" type="text/css" />
        <script src="textext/js/textext.core.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.tags.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.autocomplete.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.suggestions.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.filter.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.focus.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.prompt.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.ajax.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.arrow.js" type="text/javascript" charset="utf-8"></script>

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style type="text/css" media="screen">
            .map_canvas {
                float: left;
            }
            .text-wrap{
                position: relative !important
            }
            .big_titre{
                padding: 10px;
                font-size: 20px;
                cursor: pointer;
                margin-bottom: 10px;
                margin-top: 10px;
            }
            .btn-primary{
                width:120px
            }
        </style>



    </head>

    <body>
        <div id="loader" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/site.gif"  /></div>
        <div id="loader2" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/siret.gif"  /></div>
        <div id="loader1" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/verif.gif"  /></div>
        <div id="loader3" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/Linkedin.gif"  /></div>
        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->


        <div id="page-content" class="page-content">


            <!-- Main content -->
            <div class="content-wrapper">

                <div class="card">


                    <div class="card-body">
                        <div class="tabbable">

                            <form method="post" action="edit.php" enctype="multipart/form-data">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="bordered-justified-pill1">

                                        <div class="row">
                                            <div class="col-md-12">

                                                <input type="hidden" name="idstartupmodif" value="<?php echo $_GET["sup"]; ?>" />
                                                <div class=" bg-indigo-400 big_titre" id="historique" >Historique</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">

                                                    <?php
                                                    $histo = mysqli_query($link, "select * from historique where id_startup=" . $startup['id'] . " order by dt desc");
                                                    while ($historique = mysqli_fetch_array($histo)) {
                                                        ?>
                                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-12"><?php echo $historique['dt'] . " : " . ($historique['module']); ?></div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="info" >Informations générales</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="map_canvas"></div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Status</div>
                                                        <div class="col-md-10">
                                                            <label style="width:110px">
                                                                <input type="radio" onClick="updateNom3(this)" name="<?php echo $startup["id"]; ?>" <?php if ($startup["status"] == "1") echo ' checked="checked"'; ?> value="1" id="status" style="width:20px">
                                                                Actif</label>
                                                            <label style="width:110px">
                                                                <input type="radio" onClick="updateNom3(this)" name="<?php echo $startup["id"]; ?>" <?php if ($startup["status"] == "0") echo ' checked="checked"'; ?> value="0" id="status" style="width:20px">
                                                                Inactif</label>
                                                            <label style="width:110px">
                                                                <input type="radio" onClick="updateNom3(this)" name="<?php echo $startup["id"]; ?>" <?php if (($startup["status"] == "2") || ($startup["status"] == "3")) echo ' checked="checked"'; ?> value="3" id="status" style="width:20px">
                                                                En attente</label>
                                                            <label style="width:110px">
                                                                <input type="radio" onClick="updateNom3(this)" name="<?php echo $startup["id"]; ?>" <?php if ($startup["status"] == "4") echo ' checked="checked"'; ?> value="4" id="status" style="width:20px">
                                                                Désinscrit</label>
                                                            <label style="width:110px">
                                                                <input type="radio" onClick="updateNom3(this)" name="<?php echo $startup["id"]; ?>" <?php if ($startup["status"] == "5") echo ' checked="checked"'; ?> value="5" id="status" style="width:20px">
                                                                Hors cible</label>
                                                            <label style="width:110px">
                                                                <input type="radio" onClick="updateNom3(this)" name="<?php echo $startup["id"]; ?>" <?php if ($startup["status"] == "6") echo ' checked="checked"'; ?> value="6" id="status" style="width:20px">
                                                                Doublon</label>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Lien de verif.com</div>
                                                        <div class="col-md-3"><input type="text" class="form-control" name="verif" id="verif" placeholder="https://www.verif.com/societe/STARTUP-798203774/" value="<?php echo utf8_encode($startup['verif_link']); ?>" /></div>
                                                        <div class="col-md-1"><button type="button" onclick="verif_verif()" class="btn btn-success" >V&eacute;rifier</button></div>
                                                        <div class="col-md-2">Site web</div>
                                                        <div class="col-md-3"><input type="text" class="form-control" name="site" id="site" value="<?php echo $startup['url']; ?>" /></div>
                                                        <div class="col-md-1"> <button type="button" class="btn btn-success" onclick="verifsite()">V&eacute;rifier</button></div>

                                                    </div>

                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Nom Startup</div>
                                                        <div class="col-md-4"><input type="text" class="form-control" name="nom" id="nom" value="<?php echo utf8_encode($startup['nom']); ?>" required /></div>
                                                        <div class="col-md-2">Nom juridique</div>
                                                        <div class="col-md-4"><input type="text" class="form-control" name="societe" id="societe" value="<?php echo utf8_encode($startup['societe']); ?>" required /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Date de cr&eacute;ation</div>
                                                        <div class="col-md-4"><input type="text" name="cal" id="dp1" value="<?php echo convert_date($startup['date_complete']); ?>" class="datepicker form-control" onclick="refrech_datepicker()"></div>
                                                        <div class="col-md-2">T&eacute;l&eacute;phone</div>
                                                        <div class="col-md-4"><input type="text" name="tel" class="form-control" id="telephone" value="<?php echo $startup['tel']; ?>" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">

                                                        <div class="col-md-2">Email</div>
                                                        <div class="col-md-4"><input type="text" class="form-control" name="email" value="<?php echo $startup['email']; ?>" id="email" /></div>
                                                        <div class="col-md-2">Effectif (<?php echo date('Y'); ?>)</div>
                                                        <div class="col-md-4"><input type="text" class="form-control" name="effectif" value="<?php echo $rang_effectif['effectif']; ?>" id="effectif" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">

                                                        <div class="col-md-6">
                                                            <table class="table table-bordered" style="width: 100%">
                                                                <tr>
                                                                    <th>Effectif</th>
                                                                    <th>Année</th>
                                                                </tr>
                                                                <?php
                                                                $sql_effectif = mysqli_query($link, "select * from startup_effectif where id_startup=" . $idstartup);
                                                                while ($data_effectif = mysqli_fetch_array($sql_effectif)) {
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $data_effectif['effectif']; ?></td>
                                                                        <td><?php echo $data_effectif['annee']; ?></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </table>
                                                        </div>

                                                    </div>
                                                    <hr>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Accroche FR</div>
                                                        <div class="col-md-8"><input type="text" name="short_fr" value="<?php echo stripslashes(utf8_encode($startup['short_fr'])); ?>" class="form-control" id="short_fr" required onblur="copier()" /></div>
                                                        <?php /* ?> <div class="col-md-2"><button type="button" class="btn btn-info" onclick="traduire_accroche()" >Traduire</button></div><?php */ ?>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Description FR</div>
                                                        <div class="col-md-8"><textarea name="long_fr" id="long_fr" class="form-control" required> <?php echo stripslashes(utf8_encode($startup['long_fr'])); ?></textarea>
                                                            <script type="text/javascript">CKEDITOR.replace('long_fr');</script></div>
                                                        <?php /* ?> <div class="col-md-2"><button type="button" class="btn btn-info" onclick="traduire_long()" >Traduire</button></div><?php */ ?>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Accroche EN</div>
                                                        <div class="col-md-10"><input type="text" name="short_en" value="<?php echo stripslashes($startup['short_en']); ?>" class="form-control" id="short_en" required onblur="copier()" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Description EN</div>
                                                        <div class="col-md-10"><textarea name="long_en" id="long_en" ><?php echo stripslashes($startup['long_en']); ?></textarea>
                                                            <script type="text/javascript">CKEDITOR.replace('long_en');</script></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Twitter</div>
                                                        <div class="col-md-4"><input type="text" name="twitter" value="<?php echo $startup['twitter']; ?>" id="twitter" class="form-control" /></div>

                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Youtube</div>
                                                        <div class="col-md-4"><input type="text" name="youtube" id="youtube" value="<?php echo $startup['youtube']; ?>" class="form-control" /></div>
                                                        <div class="col-md-2">Facebook</div>
                                                        <div class="col-md-4"><input type="text" name="facebook" value="<?php echo $startup['facebook']; ?>" id="facebook" class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Viadeo</div>
                                                        <div class="col-md-4"><input type="text" name="viadeo" id="viadeo" value="<?php echo $startup['viadeo']; ?>"  class="form-control" /></div>
                                                        <div class="col-md-2">Linkedin</div>
                                                        <div class="col-md-3"><input type="text" name="linkedin" id="linkedin" value="<?php echo $startup['linkedin']; ?>"  class="form-control" /></div>
                                                        <div class="col-md-1"> <button type="button" class="btn btn-success" onclick="verifphontom()">V&eacute;rifier</button></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Blog</div>
                                                        <div class="col-md-4"><input type="text" name="blog" id="blog" value="<?php echo $startup['blog']; ?>" class="form-control" /></div>
                                                        <div class="col-md-2">Google Plus</div>
                                                        <div class="col-md-4"><input type="text" name="google" id="google" value="<?php echo $startup['google']; ?>" class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Pinterest</div>
                                                        <div class="col-md-4"><input type="text" name="pinterest" id="pinterest" value="<?php echo $startup['pinterest']; ?>" class="form-control"/></div>
                                                        <div class="col-md-2">RSS</div>
                                                        <div class="col-md-4"><input type="text" name="rss" id="rss" value="<?php echo $startup['rss']; ?>"  class="form-control" /></div>
                                                    </div>

                                                </div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">

                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Adresse</div>
                                                        <div class="col-md-10"><input type="text" class="form-control" value="<?php echo stripslashes(utf8_encode($startup['adresse'])); ?>" name="adresse" placeholder="Entrez votre adresse" id="geocomplete" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Code Postal</div>
                                                        <div class="col-md-4"><input type="text" name="postal_code" value="<?php echo $startup['cp']; ?>" class="form-control" /></div>
                                                        <div class="col-md-2">Ville</div>
                                                        <div class="col-md-4"><input type="text" name="locality" value="<?php echo stripslashes(utf8_encode($startup['ville'])); ?>" class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">LAT</div>
                                                        <div class="col-md-4"><input type="text" name="lat" value="<?php echo $startup['lat']; ?>" class="form-control" /></div>
                                                        <div class="col-md-2">LNG</div>
                                                        <div class="col-md-4"><input type="text" name="lng" value="<?php echo $startup['lng']; ?>" class="form-control" /></div>
                                                    </div>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="jurid" >Informations juridique</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Siret</div>
                                                        <div class="col-md-4"><input type="text" name="siret" value="<?php echo utf8_encode($startup['siret']); ?>" id="siret" class="form-control" /></div>
                                                        <div class="col-md-2">Forme Juridique</div>
                                                        <div class="col-md-4"><input type="text" name="juridique" value="<?php echo stripslashes(str_replace("\\", "", utf8_encode($startup['juridique']))); ?>" id="juridique" class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">NAF</div>
                                                        <div class="col-md-4"><input type="text" name="naf" value="<?php echo $startup['naf']; ?>" id="naf" class="form-control" /></div>
                                                        <div class="col-md-2">Libell&eacute; NAF</div>
                                                        <div class="col-md-4"><input type="text" name="libnaf" id="libnaf" value="<?php echo utf8_encode($startup['libnaf']); ?>" class="form-control"  /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">

                                                        <div class="col-md-2">Capital Social</div>
                                                        <div class="col-md-4"><input type="text" name="capital" value="<?php echo $startup['capital']; ?>" id="capital" class="form-control"  /></div>
                                                        <div class="col-md-6">
                                                            <table class="table table-bordered" style="width: 100%">
                                                                <tr>
                                                                    <th>Capital Social</th>
                                                                    <th>Date</th>

                                                                </tr>
                                                                <?php
                                                                $sql_capital = mysqli_query($link, "select * from startup_capital where id_startup=" . $idstartup . " order by dt desc");
                                                                while ($data_capital = mysqli_fetch_array($sql_capital)) {
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $data_capital['capital']; ?></td>
                                                                        <td><?php echo $data_capital['dt']; ?></td>

                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class=" bg-indigo-400 big_titre" id="list_sect" >Secteur d'activité</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Tags</div>
                                                        <?php
                                                        $f6 = mysqli_fetch_array(mysqli_query($link, 'select tags from activite where id_startup=' . $idstartup . ' limit 1'));
                                                        ?>
                                                        <div class="col-md-4"><input type="text" name="tags" id="tags" value="<?php echo (utf8_encode($f6['tags'])); ?>" class="form-control" required /></div>
                                                        <div class="col-md-2">Cible</div>
                                                        <div class="col-md-4">

                                                            <?php $tab_cibles = $startup['cible']; ?>
                                                            <input type="checkbox" value="B2C" name="cible[]" <?php
                                                            $B2C = strstr($tab_cibles, 'B2C');
                                                            if ($B2C != '')
                                                                echo ' checked';
                                                            ?> style="margin-top: -3px; margin-right: 5px; width: 20px;" />
                                                            B2C &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <input type="checkbox" value="B2B" name="cible[]" <?php
                                                            $B2B = strstr($tab_cibles, 'B2B');
                                                            if ($B2B != '')
                                                                echo ' checked';
                                                            ?> style="margin-top: -3px; margin-right: 5px; width: 20px;" />
                                                            B2B &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                                            <input type="checkbox" value="C2C" name="cible[]" <?php
                                                            $C2C = strstr($tab_cibles, 'C2C');
                                                            if ($C2C != '')
                                                                echo ' checked';
                                                            ?> style="margin-top: -3px; margin-right: 5px; width: 20px;" />
                                                            C2C
                                                        </div>
                                                    </div>



                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Secteur</div>
                                                        <div class="col-md-4">
                                                            <?php
                                                            $poi = mysqli_query($link, 'select * from activite where id_startup=' . $idstartup)or die(mysqli_error($link));
                                                            $last_activ = mysqli_fetch_array($poi);
                                                            $f2 = mysqli_fetch_array(mysqli_query($link, 'select nom_secteur from secteur where id=' . $last_activ['secteur'] . ' limit 1'));
                                                            $f3 = mysqli_fetch_array(mysqli_query($link, 'select nom_sous_secteur from sous_secteur where id=' . $last_activ['sous_secteur'] . ' limit 1'));
                                                            $f4 = mysqli_fetch_array(mysqli_query($link, 'select * from last_activite where id=' . $last_activ['activite'] . ' limit 1'));
                                                            $f5 = mysqli_fetch_array(mysqli_query($link, 'select * from last_sous_activite where id=' . $last_activ['sous_activite'] . ' limit 1'));
                                                            ?>
                                                            <select  name="secteur" class="form-control" id="secteur" onChange="getListSSecteur(this)">
                                                                <?php if ($last_activ['secteur'] != "") { ?>
                                                                    <option selected="selected" value="<?php echo $last_activ['secteur']; ?>"><?php echo utf8_encode($f2['nom_secteur']); ?></option>
                                                                <?php } else { ?>
                                                                    <option selected="selected"></option>
                                                                <?php } ?>
                                                                <?php
                                                                $sql = mysqli_query($link, "select * from secteur order by nom_secteur");
                                                                while ($data = mysqli_fetch_array($sql)) {
                                                                    ?>
                                                                    <option value="<?php echo $data['id']; ?>"><?php echo utf8_encode($data['nom_secteur']); ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">Sous Secteur</div>
                                                        <div class="col-md-4"><select class="form-control" name="sous_secteur" id="sous_secteur">
                                                                <?php if ($last_activ['sous_secteur'] != "") { ?>
                                                                    <option selected="selected" value="<?php echo $last_activ['sous_secteur']; ?>"><?php echo utf8_encode($f3['nom_sous_secteur']); ?></option>
                                                                <?php } ?>
                                                            </select></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Activite</div>
                                                        <div class="col-md-4">
                                                            <select  name="activ" id="activ" class="form-control" onChange="getListSActivite(this)">
                                                                <?php if ($last_activ['activite'] != "") { ?>
                                                                    <option selected="selected" value="<?php echo $last_activ['activite']; ?>"><?php echo utf8_encode($f4['nom_activite']); ?></option>
                                                                <?php } else { ?>
                                                                    <option selected="selected"></option>
                                                                <?php } ?>
                                                                <?php
                                                                $sql = mysqli_query($link, "select * from last_activite order by nom_activite");
                                                                while ($data = mysqli_fetch_array($sql)) {
                                                                    ?>
                                                                    <option value="<?php echo $data['id']; ?>"><?php echo utf8_encode($data['nom_activite']); ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">Sous Activit&eacute;</div>
                                                        <div class="col-md-4"><select class="form-control" name="sous_activ" id="sous_activ">
                                                                <?php if ($last_activ['sous_activite'] != "") { ?>
                                                                    <option selected="selected" value="<?php echo $last_activ['sous_activite']; ?>"><?php echo utf8_encode($f5['nom_sous_activite']); ?></option>
                                                                <?php } ?>
                                                            </select></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Logo</div>
                                                        <div class="col-md-2"><input type="file" name="logo"  class="form-control" />
                                                            <img src="<?php echo "https://www.myfrenchstartup.com/" . str_replace('/home/myfrench/www/', '', $startup['logo']); ?>" width="50" />
                                                        </div>
                                                        <div class="col-md-2"><img src="" style="width:150px" id="logos"></div>
                                                        <div class="col-md-2">JEI</div>
                                                        <div class="col-md-4">
                                                            <input type="radio" value="1" name="jei" id="jei" <?php if ($startup['jei'] == 1) echo 'checked="checked"'; ?> style="width:20px; margin-left:20px">
                                                            Oui
                                                            <input type="radio" value="0" name="jei" id="jei" <?php if ($startup['jei'] == 0) echo 'checked="checked"'; ?> style="width:20px">
                                                            Non
                                                        </div>
                                                    </div>
                                                </div>
                                                <div  class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="col-md-6" >
                                                        <table class="table table-bordered" style="width: 100%">
                                                            <tr>
                                                                <th>Chiffre d'affaire</th>
                                                                <th>Année</th>

                                                            </tr>
                                                            <?php
                                                            $sql_ca = mysqli_query($link, "select * from startup_ca where id_startup=" . $idstartup . " order by annee desc");
                                                            while ($data_ca = mysqli_fetch_array($sql_ca)) {
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $data_ca['ca']; ?></td>
                                                                    <td><?php echo $data_ca['annee']; ?></td>

                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </table>
                                                        <br><br>
                                                    </div>
                                                    <div class="col-md-12" id="bloc_ca">
                                                        <div id="bloc_min_ca" class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Chiffre Affaire</div>
                                                            <div class="col-md-4"><input type="text" name="ca[]" class="form-control" /></div>
                                                            <div class="col-md-2">Année</div>
                                                            <div class="col-md-4"><input type="text" name="anneeca[]" class="form-control" /></div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div id="add_ca">
                                                            <div class="col-lg-3">
                                                                <button onclick="add_ca();" type="button" class="btn btn-primary">
                                                                    <i class="icon-plus-circle2 position-left"></i> Ajouter</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class=" bg-indigo-400 big_titre" id="fond1"> <i class="fa fa-plus-circle"> </i> Fondateur</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond1_bloc">
                                                    <div id="my_bloc_founder_ajax">
                                                        <?php
                                                        $sqlfond = mysqli_query($link, 'select * from personnes where id_startup=' . $idstartup . ' and nom!="" ');
                                                        $nombrefond = mysqli_num_rows($sqlfond);
                                                        $i = 1;
                                                        while ($fondateur = mysqli_fetch_array($sqlfond)) {
                                                            ?>
                                                            <h3>Fondateur <?php echo $i; ?></h3>
                                                            <table class="table" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td> Civilite </td>
                                                                    <td>
                                                                        <input type="hidden" name="idfond<?php echo $i; ?>" value="<?php echo $fondateur['id']; ?>" />
                                                                        <select name="civf<?php echo $i; ?>" class="form-control">
                                                                            <option value="<?php echo $fondateur['civilite']; ?>"><?php
                                                                                if ($fondateur['civilite'] == 0)
                                                                                    echo 'M';
                                                                                if ($fondateur['civilite'] == 1)
                                                                                    echo 'Mme';
                                                                                ?></option>
                                                                            <option value="0">M</option>
                                                                            <option value="1">Mme</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Pr&eacute;nom </td>
                                                                    <td><input type="text" class="form-control" name="prenomf<?php echo $i; ?>" value="<?php echo utf8_encode($fondateur['prenom']) ?>" id="prenomf1<?php echo $i; ?>" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Nom </td>
                                                                    <td><input type="text" class="form-control" name="nomf<?php echo $i; ?>" value="<?php echo utf8_encode($fondateur['nom']) ?>" id="nomf1<?php echo $i; ?>" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Fonction </td>
                                                                    <?php
                                                                    $f1 = mysqli_fetch_array(mysqli_query($link, 'select nom_fr from fonction where id=' . $fondateur['fonction'] . ' limit 1'));
                                                                    ?>
                                                                    <td><select name="fonctionf<?php echo $i; ?>" class="form-control" style="width: 200px;">
                                                                            <option value="<?php echo $fondateur['fonction']; ?>"><?php echo $f1['nom_fr']; ?></option>
                                                                            <option value="6"> CEO </option>
                                                                            <option value="9"> CTO </option>
                                                                            <option value="10"> COO </option>
                                                                            <option value="11"> CMO </option>
                                                                            <option value="12"> CFO </option>
                                                                            <option value="13"> Business Developper </option>
                                                                            <option value="2"> Co-fondateur </option>
                                                                            <option value="1"> Fondateur </option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Email </td>
                                                                    <td><input type="text" class="form-control" name="emailf<?php echo $i; ?>" value="<?php echo $fondateur['email'] ?>" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Tel </td>
                                                                    <td><input type="text" class="form-control" name="telf<?php echo $i; ?>" value="<?php echo $fondateur['tel'] ?>" id="telf1" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Mobile </td>
                                                                    <td><input type="text" class="form-control" name="mobf<?php echo $i; ?>" value="<?php echo $fondateur['mob'] ?>" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Linkedin </td>
                                                                    <td><input type="text" class="form-control" name="linkedinf<?php echo $i; ?>" value="<?php echo $fondateur['linkedin'] ?>" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Viadeo </td>
                                                                    <td><input type="text" class="form-control" name="viadeof<?php echo $i; ?>" value="<?php echo $fondateur['viadeo'] ?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td> Image </td>
                                                                    <td><input type="text" name="imgf<?php echo $i; ?>" value="" class="form-control" id="imgf1<?php echo $i; ?>" /></td>
                                                                </tr>

                                                                <tr>
                                                                    <td colspan="2"><strong> Experiences Professionnelles</strong></td>
                                                                </tr>
                                                                <?php
                                                                $sqlexperience = mysqli_query($link, 'select * from societe_personnes where idpersonne=' . $fondateur['id'] . ' and societe!="" order by de desc');
                                                                $nbfoncsoc = mysqli_num_rows($sqlexperience);
                                                                $j = 1;
                                                                if ($nbfoncsoc > 0) {

                                                                    while ($fondsociete = mysqli_fetch_array($sqlexperience)) {
                                                                        ?>
                                                                        <tr>
                                                                            <td> Soci&eacute;t&eacute; </td>
                                                                            <td>
                                                                                <input type="hidden" name="idsociete<?php echo $i; ?><?php echo $j; ?>" value="<?php echo $fondsociete['id']; ?>" />
                                                                                <input type="text" class="form-control" name="societef<?php echo $i; ?><?php echo $j; ?>" value="<?php echo utf8_encode(stripcslashes($fondsociete['societe'])); ?>">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td> Fonction </td>
                                                                            <td><input type="text" class="form-control" name="fonctionf<?php echo $i; ?><?php echo $j; ?>" value="<?php echo utf8_encode($fondsociete['fonction']); ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td> De </td>
                                                                            <td><input type="text" class="form-control" name="societe_def<?php echo $i; ?><?php echo $j; ?>" value="<?php echo $fondsociete['de']; ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>A </td>
                                                                            <td><input type="text" class="form-control" name="societe_af<?php echo $i; ?><?php echo $j; ?>" value="<?php echo $fondsociete['a']; ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2"> *********************** </td>
                                                                        </tr>
                                                                        <?php
                                                                        $j++;
                                                                    }

                                                                    for ($k = $nbfoncsoc + 1; $k < 6; $k++) {
                                                                        echo "K :" . $k;
                                                                        ?>
                                                                        <tr>
                                                                            <td> Soci&eacute;t&eacute; </td>
                                                                            <td><input type="hidden" class="form-control" name="idsociete<?php echo $i; ?><?php echo $k; ?>" /><input type="text" class="form-control" value="" name="societef<?php echo $i; ?><?php echo $k; ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td> Fonction </td>
                                                                            <td><input type="text" class="form-control" value="" name="fonctionf<?php echo $i; ?><?php echo $k; ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td> De </td>
                                                                            <td><input type="text" class="form-control" value="" name="societe_def<?php echo $i; ?><?php echo $k; ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&agrave; </td>
                                                                            <td><input type="text" class="form-control" value="" name="societe_af<?php echo $i; ?><?php echo $k; ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2"> *********************** </td>
                                                                        </tr>
                                                                        <?php
                                                                    }//fin for
                                                                } else {
                                                                    ?>
                                                                    <tr>
                                                                        <td> Soci&eacute;t&eacute; </td>
                                                                        <td><input type="hidden" class="form-control" name="idsociete<?php echo $i; ?>1" /><input class="form-control" type="text" value="" name="societef<?php echo $i; ?>1"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Fonction </td>
                                                                        <td><input type="text" class="form-control" value="" name="fonctionf<?php echo $i; ?>1"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> De </td>
                                                                        <td><input type="text" class="form-control" value="" name="societe_def<?php echo $i; ?>1"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&agrave; </td>
                                                                        <td><input type="text" class="form-control" value="" name="societe_af<?php echo $i; ?>1"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"> *********************** </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Soci&eacute;t&eacute; </td>
                                                                        <td><input type="hidden" class="form-control" name="idsociete<?php echo $i; ?>2" /><input class="form-control" type="text" value="" name="societef<?php echo $i; ?>2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Fonction </td>
                                                                        <td><input type="text" class="form-control" value="" name="fonctionf<?php echo $i; ?>2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> De </td>
                                                                        <td><input type="text" class="form-control" value="" name="societe_def<?php echo $i; ?>2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&agrave; </td>
                                                                        <td><input type="text" class="form-control"  value="" name="societe_af<?php echo $i; ?>2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"> *********************** </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Soci&eacute;t&eacute; </td>
                                                                        <td><input type="hidden" class="form-control" name="idsociete<?php echo $i; ?>3" /><input class="form-control" type="text" value="" name="societef<?php echo $i; ?>3"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Fonction </td>
                                                                        <td><input type="text"  class="form-control" value="" name="fonctionf<?php echo $i; ?>3"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> De </td>
                                                                        <td><input type="text" class="form-control" value="" name="societe_def<?php echo $i; ?>3"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&agrave; </td>
                                                                        <td><input type="text" class="form-control" value="" name="societe_af<?php echo $i; ?>3"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"> *********************** </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Soci&eacute;t&eacute; </td>
                                                                        <td><input type="hidden" class="form-control" name="idsociete<?php echo $i; ?>4" /><input class="form-control" type="text" value="" name="societef<?php echo $i; ?>4"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Fonction </td>
                                                                        <td><input type="text" class="form-control" value="" name="fonctionf<?php echo $i; ?>4"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> De </td>
                                                                        <td><input type="text" class="form-control" value="" name="societe_def<?php echo $i; ?>4"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&agrave; </td>
                                                                        <td><input type="text" class="form-control" value="" name="societe_af<?php echo $i; ?>4"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"> *********************** </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Soci&eacute;t&eacute; </td>
                                                                        <td><input type="hidden" name="idsociete<?php echo $i; ?>5" /><input type="text" class="form-control" value="" name="societef<?php echo $i; ?>5"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Fonction </td>
                                                                        <td><input type="text" class="form-control" value="" name="fonctionf<?php echo $i; ?>5"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> De </td>
                                                                        <td><input type="text" class="form-control" value="" name="societe_def<?php echo $i; ?>5"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&agrave; </td>
                                                                        <td><input type="text" class="form-control" value="" name="societe_af<?php echo $i; ?>5"></td>
                                                                    </tr>

                                                                    <?php
                                                                }
                                                                ?>


                                                                <tr>
                                                                    <td colspan="2"><strong> Formation Acad&eacute;mique</strong></td>
                                                                </tr>
                                                                <?php
                                                                $sqlecole = mysqli_query($link, 'select distinct(ecole),id, diplome,de,a from ecole_personnes where idpersonne=' . $fondateur['id'] . ' and ecole !="" order by de desc');
                                                                $nbfonecole = mysqli_num_rows($sqlecole);
                                                                $h = 1;
                                                                if ($nbfonecole > 0) {
                                                                    while ($fondecole = mysqli_fetch_array($sqlecole)) {
                                                                        ?>
                                                                        <tr>
                                                                            <td> Ecole </td>
                                                                            <td><input type="hidden" class="form-control" name="idecole<?php echo $i; ?><?php echo $h; ?>" value="<?php echo $fondecole['id']; ?>" /><input class="form-control" type="text" name="ecolef<?php echo $i; ?><?php echo $h; ?>" value="<?php echo stripslashes(utf8_encode($fondecole['ecole'])); ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td> Diplome </td>
                                                                            <td><input type="text"  class="form-control" name="diplomef<?php echo $i; ?><?php echo $h; ?>" value="<?php echo stripslashes(utf8_encode($fondecole['diplome'])); ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td> De </td>
                                                                            <td><input type="text" class="form-control" name="ecole_def<?php echo $i; ?><?php echo $h; ?>" value="<?php echo $fondecole['de'] ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&agrave; </td>
                                                                            <td><input type="text" class="form-control" name="ecole_af<?php echo $i; ?><?php echo $h; ?>" value="<?php echo $fondecole['a'] ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2"> *********************** </td>
                                                                        </tr>

                                                                        <?php
                                                                        $h++;
                                                                    }//fin while

                                                                    for ($oo = $nbfonecole + 1; $oo < 4; $oo++) {
                                                                        ?>
                                                                        <tr>
                                                                            <td> Ecole </td>
                                                                            <td><input type="hidden" class="form-control" name="idecole<?php echo $i; ?><?php echo $oo; ?>" /><input class="form-control" type="text" name="ecolef<?php echo $i; ?><?php echo $oo; ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td> Diplome </td>
                                                                            <td><input type="text" class="form-control" name="diplomef<?php echo $i; ?><?php echo $oo; ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td> De </td>
                                                                            <td><input type="text" class="form-control" name="ecole_def<?php echo $i; ?><?php echo $oo; ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&agrave; </td>
                                                                            <td><input type="text" class="form-control" name="ecole_af<?php echo $i; ?><?php echo $oo; ?>"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2"> *********************** </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    ?>
                                                                    <tr>
                                                                        <td> Ecole </td>
                                                                        <td><input type="hidden" class="form-control" name="idecole<?php echo $i; ?>1" /><input  class="form-control" type="text" name="ecolef<?php echo $i; ?>1"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Diplome </td>
                                                                        <td><input type="text" class="form-control" name="diplomef<?php echo $i; ?>1"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> De </td>
                                                                        <td><input type="text" class="form-control" name="ecole_def<?php echo $i; ?>1"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&agrave; </td>
                                                                        <td><input type="text" class="form-control" name="ecole_af<?php echo $i; ?>1"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"> *********************** </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Ecole </td>
                                                                        <td><input type="hidden" class="form-control" name="idecole<?php echo $i; ?>2" /><input class="form-control" type="text" name="ecolef<?php echo $i; ?>2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Diplome </td>
                                                                        <td><input type="text" class="form-control" name="diplomef<?php echo $i; ?>2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> De </td>
                                                                        <td><input type="text" class="form-control" name="ecole_def<?php echo $i; ?>2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&agrave; </td>
                                                                        <td><input type="text" class="form-control" name="ecole_af<?php echo $i; ?>2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"> *********************** </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Ecole </td>
                                                                        <td><input type="hidden" class="form-control" name="idecole<?php echo $i; ?>3" /><input class="form-control" type="text" name="ecolef<?php echo $i; ?>3"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Diplome </td>
                                                                        <td><input type="text" class="form-control" name="diplomef<?php echo $i; ?>3"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> De </td>
                                                                        <td><input type="text" class="form-control" name="ecole_def<?php echo $i; ?>3"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&agrave; </td>
                                                                        <td><input type="text" class="form-control" name="ecole_af<?php echo $i; ?>3"></td>
                                                                    </tr>


                                                                    <?php
                                                                }
                                                                ?>
                                                            </table>
                                                            <?php
                                                            $i++;
                                                        }
                                                        ?>
                                                    </div>
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal_add_founder">Ajouter un nouveau Fondateur</button>
                                                    <div class="modal fade" id="myModal_add_founder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title" id="myModalLabel">Nouveau Fondateur</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <input type="hidden" id="startup_id_founder" value="<?php echo $startup['id']; ?>">
                                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                        <div class="col-md-2">Civilité</div>
                                                                        <div class="col-md-4">
                                                                            <select class="form-control" id="civf1">
                                                                                <option value="0">M</option>
                                                                                <option value="1">Mme</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-2">Fonction</div>
                                                                        <div class="col-md-4">
                                                                            <select id="fonctionf1" class="form-control">
                                                                                <option value="">--Choisir fonction--</option>
                                                                                <option value="6"> CEO </option>
                                                                                <option value="9"> CTO </option>
                                                                                <option value="10"> COO </option>
                                                                                <option value="11"> CMO </option>
                                                                                <option value="12"> CFO </option>
                                                                                <option value="13"> Business Developper </option>
                                                                                <option value="2"> Co-fondateur </option>
                                                                                <option value="1"> Fondateur </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                        <div class="col-md-2">Prénom</div>
                                                                        <div class="col-md-4"><input type="text" class="form-control" id="prenomf1" /></div>
                                                                        <div class="col-md-2">Nom</div>
                                                                        <div class="col-md-4"><input type="text" class="form-control" id="nomf1" /></div>
                                                                    </div>
                                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                        <div class="col-md-2">Email</div>
                                                                        <div class="col-md-4"><input type="text" class="form-control" id="emailf1" /></div>
                                                                        <div class="col-md-2">Téléphone</div>
                                                                        <div class="col-md-4"><input type="text" class="form-control" id="telf1" /></div>
                                                                    </div>
                                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">

                                                                        <div class="col-md-2">Linkedin</div>
                                                                        <div class="col-md-4"><input type="text" class="form-control" id="linkedinf1" /></div>
                                                                        <div class="col-md-2">Photo</div>
                                                                        <div class="col-md-4"><input type="text" class="form-control" id="imgf1" /></div>
                                                                    </div>

                                                                    <hr>
                                                                    <h4>Experiences professionelles</h4>
                                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societef11" placeholder="Société"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="fonctionf11" placeholder="Fonction"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societe_def11" placeholder="De"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societe_af11" placeholder="A"></div>
                                                                    </div>
                                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societef12" placeholder="Société"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="fonctionf12" placeholder="Fonction"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societe_def12" placeholder="De"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societe_af12" placeholder="A"></div>
                                                                    </div>
                                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societef13" placeholder="Société"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="fonctionf13" placeholder="Fonction"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societe_def13" placeholder="De"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societe_af13" placeholder="A"></div>
                                                                    </div>
                                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societef14" placeholder="Société"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="fonctionf14" placeholder="Fonction"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societe_def14" placeholder="De"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societe_af14" placeholder="A"></div>
                                                                    </div>
                                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societef15" placeholder="Société"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="fonctionf15" placeholder="Fonction"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societe_def15" placeholder="De"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="societe_af15" placeholder="A"></div>
                                                                    </div>
                                                                    <h4>Formations académiques</h4>
                                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="ecolef11" placeholder="Ecole"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="diplomef11" placeholder="Diplôme"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="ecole_def11" placeholder="DE"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="ecole_af11" placeholder="A"></div>
                                                                    </div>
                                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="ecolef12" placeholder="Ecole"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="diplomef12" placeholder="Diplôme"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="ecole_def12" placeholder="DE"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="ecole_af12" placeholder="A"></div>
                                                                    </div>
                                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="ecolef13" placeholder="Ecole"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="diplomef13" placeholder="Diplôme"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="ecole_def13" placeholder="DE"></div>
                                                                        <div class="col-md-3"><input type="text" class="form-control" id="ecole_af13" placeholder="A"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                                    <button type="button" class="btn btn-primary" onclick="add_founder()">Ajouter</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>





                                                <div class=" bg-indigo-400 big_titre" id="lf" >Deals</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="col-md-12" id="bloc_lf_table">
                                                        <table class="table table-bordered" style="width: 100%">
                                                            <tr>
                                                                <th>Investisseurs</th>
                                                                <th>Montant</th>
                                                                <th>Date</th>
                                                                <th>Deal</th>
                                                                <th>Modifier</th>

                                                            </tr>
                                                            <?php
                                                            $sql_lf = mysqli_query($link, "select * from lf where id_startup=" . $idstartup . " order by date_ajout desc");
                                                            while ($datalf = mysqli_fetch_array($sql_lf)) {
                                                                ?>
                                                                <tr>
                                                                    <td height="20"><?php echo $datalf['de']; ?></td>
                                                                    <td><?php echo $datalf['montant']; ?></td>
                                                                    <td><?php echo $datalf['date_ajout']; ?></td>
                                                                    <td><?php
                                                                        if ($datalf['rachat'] == 0)
                                                                            echo "Levée de fonds";
                                                                        if ($datalf['rachat'] == 1)
                                                                            echo "Rachat";
                                                                        if ($datalf['rachat'] == 2)
                                                                            echo "IPO";
                                                                        ?></td>
                                                                    <td><a href="modifier_lf.php?sup=<?php echo $datalf['id'] ?>" target="_blank" >Modifier</a></td>


                                                                </tr>
    <?php
}
?>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-12" id="bloc_lf">
                                                        <div id="bloc_min_lf" class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Deal</div>
                                                            <div class="col-md-4">
                                                                <select name="rachat" class="form-control">
                                                                    <option value="" selected="selected"></option>
                                                                    <option value="0">Lev&eacute;e de fonds</option>
                                                                    <option value="1">Rachat</option>
                                                                    <option value="2">IPO</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">Montant</div>
                                                            <div class="col-md-4"><input type="text" value="NA" name="montant_lf" class="form-control"></div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Date</div>
                                                            <div class="col-md-4">
                                                                <input type="text" class="datepicker form-control" autocomplete="off" onclick="refrech_datepicker()" class="form-control" name="date_lf">
                                                            </div>
                                                            <div class="col-md-2">Investisseurs</div>
                                                            <div class="col-md-4">
                                                                <textarea class="example list_invest_tag" name="de" style="width: 320px"></textarea>
                                                                <input name="de1" class="form-control" type="text" placeholder="Autres" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a class="delete_lf_ligne">
                                                                <i style="color:#EC407A" class="fa fa-minus-circle fa-2x"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div id="add_lf">
                                                            <div class="col-lg-3">
                                                                <button onclick="add_lf();" type="button" class="btn btn-primary">
                                                                    <i class="icon-plus-circle2 position-left"></i> Ajouter</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="deal_flow" >Recherche de fonds</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div id="my_bloc_invest_ajax">
                                                        <table class="table table-bordered" style="width: 100%">
                                                            <tr>
                                                                <th>Montant</th>
                                                                <th>Objectif</th>
                                                                <th>Type d'investissement</th>
                                                                <th>Date de clotûre</th>

                                                                <th>Supprimer</th>

                                                            </tr>
                                                            <?php
                                                            $sql_inv = mysqli_query($link, "select * from invest_search where id_startup=" . $idstartup);
                                                            while ($datainv = mysqli_fetch_array($sql_inv)) {
                                                                ?>
                                                                <tr>
                                                                    <td height="20"><?php echo $datainv['montant']; ?></td>
                                                                    <td><?php echo $datainv['objectif']; ?></td>
                                                                    <td><?php echo $datainv['typeinvest']; ?></td>
                                                                    <td><?php echo $datainv['dt_cloture']; ?></td>

                                                                    <td><a onclick="supp_flow(this)" id="flow_<?php echo $datainv['id']; ?>" target="_blank" >Supprimer</a>

                                                                    </td>

                                                                </tr>
    <?php
}
?>

                                                        </table>

                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Montant recherché (k€)</div>
                                                        <div class="col-md-4"><input type="text" name="montant_rech"  class="form-control" /></div>
                                                        <div class="col-md-2">Date de clotûre</div>
                                                        <div class="col-md-4"><input type="text" name="date_rech"  class="datepicker form-control" onclick="refrech_datepicker()" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Type d'investisseurs recherchés :</div>
                                                        <div class="col-md-10">
                                                            <div class="col-md-3 col-sm-4">
                                                                <input type="checkbox" value="Business Angels" name="rech[]" style="width: 20px;">
                                                                Business Angels
                                                            </div>
                                                            <div class="col-md-3 col-sm-4">
                                                                <input type="checkbox" value="Fonds d'investissements" name="rech[]" style="width: 20px;">
                                                                Fonds d'investissements
                                                            </div>
                                                            <div class="col-md-3 col-sm-4">
                                                                <input type="checkbox" value="Crowdfunding" name="rech[]" style="width: 20px;">
                                                                Crowdfunding
                                                            </div>
                                                            <div class="col-md-3 col-sm-4">
                                                                <input type="checkbox" value="Fonds publics" name="rech[]" style="width: 20px;">
                                                                Fonds publics</div>
                                                        </div>
                                                        <div class="col-md-2">Objectif :</div>
                                                        <div class="col-md-10">

                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="Recrutement IT" name="objectif[]" style="width: 20px;">
                                                                Recrutement IT
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="Recrutement BizDev" name="objectif[]" style="width: 20px;">
                                                                Recrutement BizDev
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="R&amp;D" name="objectif[]" style="width: 20px;">
                                                                R&amp;D
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="Acquisition matériels" name="objectif[]" style="width: 20px;">
                                                                Acquisition matériels
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="International" name="objectif[]" style="width: 20px;">
                                                                International
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="Autres" name="objectif[]" style="width: 20px;">
                                                                Autres
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" >Défaillance</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">


                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Depot de bilan</div>
                                                        <div class="col-md-4">
                                                            <input type="radio" value="1" name="bilan" <?php if ($startup['bilan'] == 1) echo 'checked="checked"'; ?> style="width:20px; margin-left:20px">
                                                            Oui
                                                            <input type="radio" value="0" name="bilan" <?php if ($startup['bilan'] == 0) echo 'checked="checked"'; ?> style="width:20px;">
                                                            Non
                                                        </div>
                                                        <div class="col-md-2">Date de dep&ocirc;t de bilan</div>
                                                        <div class="col-md-4"><input type="text" name="datebilan"  class="datepicker form-control" value="<?php
                                                            $split = explode("-", $startup['date_bilan']);
                                                            $annee = $split[0];
                                                            $jour = $split[2];
                                                            $mois = $split[1];
                                                            $creation = $mois . "-" . $jour . "-" . $annee;
                                                            echo $creation;
                                                            ?>" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Statut</div>
                                                        <div class="col-md-4">
                                                            <select name="statut"  class="form-control">
                                                                <option value="<?php echo utf8_encode($startup['statut']) ?>"><?php echo utf8_encode($startup['statut']) ?></option>
                                                                <option value="Entreprise en défaillance">Entreprise en défaillance</option>
                                                                <option value="Entreprise radiée">Entreprise radiée</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">Motif</div>
                                                        <div class="col-md-4">
                                                            <select  class="form-control" name="motif">
                                                                <option value="<?php echo $startup['depot'] ?>"><?php echo $startup['depot'] ?></option>
                                                                <option value="Annulation de la procédure de redressement juidiciaire">Annulation de la procédure de redressement juidiciaire</option>
                                                                <option value="Arrêt du plan de sauvegarde">Arrêt du plan de sauvegarde</option>
                                                                <option accesskey="Changement de liquidateur judiciaire">Changement de liquidateur judiciaire</option>
                                                                <option value="Clôture pour insuffisance d'actif">Clôture pour insuffisance d'actif</option>
                                                                <option value="Conciliation">Conciliation</option>
                                                                <option value="Conversion en liquidation judiciaire">Conversion en liquidation judiciaire</option>
                                                                <option value="Dépôt de l'état des créances">Dépôt de l'état des créances</option>
                                                                <option value="Dépôt des créances nées après jugement">Dépôt des créances nées après jugement</option>
                                                                <option value="Liquidation judiciaire">Liquidation judiciaire</option>
                                                                <option value="Liquidation judiciaire simplifiée">Liquidation judiciaire simplifiée</option>
                                                                <option value="Plan de cession totale">Plan de cession totale</option>
                                                                <option value="Procédure de sauvegarde">Procédure de sauvegarde</option>
                                                                <option value="Redressement Judiciaire">Redressement Judiciaire</option>
                                                                <option value="Règlement amiable">Règlement amiable</option>
                                                                <option value="Rétractation de redressement judiciaire sur tierce opposition">Rétractation de redressement judiciaire sur tierce opposition</option>
                                                                <option value="Sauvegarde Jugement">Sauvegarde Jugement</option>

                                                            </select>
                                                        </div>

                                                    </div>


                                                </div>



                                                <div class=" bg-indigo-400 big_titre" id="actualite" >News</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div id="bloc_news_table" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                        <table class="table table-bordered" style="width: 100%">
                                                            <tr>
                                                                <th>Titre</th>
                                                                <th>Source</th>
                                                                <th>Date</th>
                                                                <th>Supprimer</th>

                                                            </tr>
                                                            <?php
                                                            $sqlnews = mysqli_query($link, 'select * from actualite where id_startup=' . $startup['id']);
                                                            $nbnews = mysqli_num_rows($sqlnews);
                                                            if ($nbnews > 0) {
                                                                while ($datanews = mysqli_fetch_array($sqlnews)) {
                                                                    ?>
                                                                    <tr>
                                                                        <td height="20"><?php echo utf8_encode($datanews['titre']); ?></td>
                                                                        <td><?php echo utf8_encode($datanews['source']); ?></td>
                                                                        <td><?php echo $datanews['date']; ?></td>
                                                                        <td><a onclick="supp_news(this)" id="news_<?php echo $datanews['id']; ?>" target="_blank" >Supprimer</a></td>

                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </table>

                                                    </div>
                                                    <!-- <div class="col-md-12" id="bloc_actualite">
                                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;" id="subvention_min_bloc">
                                                            <div class="col-md-2">Titre</div>
                                                            <div class="col-md-4"><input type="text" name="titre[]" class="form-control"></div>
                                                            <div class="col-md-2">Description</div>
                                                            <div class="col-md-4"><input type="text" name="description[]" class="form-control"></div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;" id="subvention_min_bloc">
                                                            <div class="col-md-1">Date</div>
                                                            <div class="col-md-3"><input type="text" class="datepicker form-control" onclick="refrech_datepicker()"  name="dateactu[]"></div>
                                                            <div class="col-md-1">Lien</div>
                                                            <div class="col-md-3"><input type="text" name="lien[]" class="form-control"></div>
                                                            <div class="col-md-1">Source</div>
                                                            <div class="col-md-3"><input type="text" name="source[]" class="form-control"></div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a class="delete_actualite_ligne">
                                                                <i style="color:#EC407A" class="fa fa-minus-circle fa-2x"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div id="add_actualite">
                                                            <div class="col-lg-3">
                                                                <button onclick="add_actualite();" type="button" class="btn btn-primary">
                                                                    <i class="icon-plus-circle2 position-left"></i> Ajouter</button>
                                                            </div>
                                                        </div>
                                                    </div> -->

                                                    <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                        <div class="col-md-12" id="bloc_actualite">
                                                            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <div class="col-md-2">Lien</div>
                                                                <div class="col-md-4"><input type="text" name="lien[]" id="news_link" class="form-control"></div>
                                                                <div class="col-md-4"><button type="button" id="" class="btn btn-info" onclick="getNews()">Get NEWS</button></div>
                                                            </div>
                                                            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <div class="col-md-2">Titre</div>
                                                                <div class="col-md-4"><input type="text" id="titre_news" name="titre[]" class="form-control"></div>
                                                                <div class="col-md-2">Description</div>
                                                                <div class="col-md-4"><input type="text" id="description_news" name="description[]" class="form-control"></div>
                                                            </div>
                                                            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <div class="col-md-1">Date</div>
                                                                <div class="col-md-3"><input type="text" id="date_news" class="datepicker form-control" onclick="refrech_datepicker()"  name="dateactu[]"></div>

                                                                <div class="col-md-1">Source</div>
                                                                <div class="col-md-3"><input type="text" id="source_news" name="source[]" class="form-control"></div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <a class="delete_actualite_ligne">
                                                                    <i style="color:#EC407A" class="fa fa-minus-circle fa-2x"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div id="add_actualite">
                                                                <div class="col-lg-3">
                                                                    <button onclick="add
                                                                            _actualite();" type="button" class="btn btn-primary">
                                                                        <i class="icon-plus-circle2 position-left"></i> Ajouter</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>





                                                    <!-- new new -->
                                                </div>



                                            </div>


                                        </div>



                                    </div>
                                </div>
                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                    <table width="1134" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" colspan="2"><button type="submit" class="btn btn-success">Modifier</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
            <!-- /main content -->





        </div>

        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcCIjgD7FA1zVC6EQwS5V6I-6xuUuwMiQ&sensor=false&amp;libraries=places"></script>
        <script src="assets/js/jquery.geocomplete.js"></script>
        <script src="assets/js/bootstrap-datepicker.js"></script>
        <script src="script/function.js"></script>
        <script src="script/site.js"></script>
        <script src="script/siret.js"></script>
        <script src="script/verif.js"></script>
        <script src="script/verifphontom.js"></script>
        <script src="script/google_linkedin_company.js"></script>
        <script src="script/qualif_linkedin_company.js"></script>
        <script src="script/verif_seul.js"></script>
        <script>




                                                                        function verif_verif() {
                                                                            var siren = document.getElementById("verif").value;
                                                                            if (siren != 'https://www.verif.com/societe/') {
                                                                                $.ajax({
                                                                                    url: 'getverifcom.php?field=' + siren,
                                                                                    beforeSend: function () {
                                                                                        $('#loader1').show();
                                                                                        document.getElementById("page-content").style.opacity = 0.5;
                                                                                    },
                                                                                    complete: function () {
                                                                                        $('#loader1').hide();
                                                                                        document.getElementById("page-content").style.opacity = 1;
                                                                                    },
                                                                                    success: function (data) {

                                                                                        var t = eval(data);
                                                                                        document.getElementById("geocomplete").value = t[0].adresse;
                                                                                        document.getElementById("siret").value = t[0].siret;
                                                                                        document.getElementById("dp1").value = t[0].creation;
                                                                                        document.getElementById("capital").value = t[0].capital;
                                                                                        document.getElementById("siret").value = t[0].siret;
                                                                                        document.getElementById("naf").value = t[0].naf;
                                                                                        document.getElementById("libnaf").value = t[0].libelle;
                                                                                        document.getElementById("juridique").value = t[0].juridique;
                                                                                        document.getElementById("nom").value = t[0].startup;
                                                                                        document.getElementById("societe").value = t[0].startup;
                                                                                        document.getElementById("prenomf1").value = t[0].dirigeant1;



                                                                                        //google_linkedin_company();


                                                                                    }
                                                                                });
                                                                            }
                                                                        }



        </script>
        <script>

            $(document).ready(function () {



                $(function () {

                    window.prettyPrint && prettyPrint();
                    $('#dp1').datepicker({
                        format: 'mm-dd-yyyy'
                    });
                });


                $(function () {
                    $("#datepicker").datepicker({dateFormat: 'dd/mm/yy'});
                    $("#datepicker1").datepicker({dateFormat: 'dd/mm/yy'});
                    $("#datepicker2").datepicker({dateFormat: 'dd/mm/yy'});
                    $("#datepicker3").datepicker({dateFormat: 'dd/mm/yy'});
                    $("#datepicker4").datepicker({dateFormat: 'dd/mm/yy'});
                });

                $(function () {
                    $("#geocomplete").geocomplete({
                        map: ".map_canvas",
                        details: "form",
                        types: ["geocode", "establishment"],
                    });

                    $("#find").click(function () {
                        $("#geocomplete").trigger("geocode");
                    });
                });


                $().ready(function () {

                    $("#concurrent").autocomplete('concurrent.php', {
                        multiple: true,
                        mustMatch: false,
                        autoFill: true

                    });





                    $("#concurrent").result(function (event, data, formatted) {

                        var hidden = $(this).parent().next().find(">:input");

                        hidden.val((hidden.val() ? hidden.val() + ";" : hidden.val()) + data[1]);

                    });


                });


            });
        </script>
        <script>

            $().ready(function () {
                window.prettyPrint && prettyPrint();

                refrech_datepicker();

            });


            function refrech_datepicker() {
                $(".datepicker").each(function () {
                    $(this).datepicker({
                        format: 'mm-dd-yyyy'
                    });


                });

            }
            function remplire_tags() {

                $('.list_invest_tag')
                        .textext({
                            plugins: 'tags autocomplete'
                        })
                        .bind('getSuggestions', function (e, data)
                        {
                            var list = [
<?php
$sql_inves = mysqli_query($link, "select id,new_name from new_name_list group by new_name order by new_name");
while ($data_invest = mysqli_fetch_array($sql_inves)) {
    ?>
                                    '<?php echo addslashes(strtoupper($data_invest['new_name'])); ?>',
    <?php
}
?>
                            ],
                                    textext = $(e.target).textext()[0],
                                    query = (data ? data.query : '') || '';

                            $(this).trigger(
                                    'setSuggestions',
                                    {result: textext.itemManager().filter(list, query)}
                            );
                        })
                        ;
            }
            $(document).ready(function () {

                remplire_tags();
            }
            );
        </script>
    </body>
</html>
