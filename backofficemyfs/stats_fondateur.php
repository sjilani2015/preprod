<?php
include('db.php');
$menu = 140;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Statistiques fondateurs</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style>
            .form-inline {
                display: block !important;

            }
        </style>
    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->


        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">


                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Personnes ayant accès à la page de paiement</h6>
                                </div>

                                <!-- Numbers -->
                                <div class="card-body py-0">

                                    <div class="col-md-12">

                                        <div class="table-responsive mb-4">
                                            <?php
                                            $fond= mysqli_fetch_array(mysqli_query($link, "select * from stats_fondateurs where id=1"));
                                            
                                            ?>
                                            fondateurs : <?php echo $fond['fondateurs'] ?><br>
fondateurs_tel : <?php echo $fond['fondateurs_tel'] ?><br>
fondateurs_email_myfs : <?php echo $fond['fondateurs_email_myfs'] ?><br>
fondateurs_email_linkedin : <?php echo $fond['fondateurs_email_linkedin'] ?><br>
CEO : <?php echo $fond['CEO'] ?><br>
CEO_tel : <?php echo $fond['CEO_tel'] ?><br>
CEO_email : <?php echo $fond['CEO_email'] ?><br>
Co_fondateur : <?php echo $fond['Co_fondateur'] ?><br>
Co_fondateur_tel : <?php echo $fond['Co_fondateur_tel'] ?><br>
Co_fondateur_email : <?php echo $fond['Co_fondateur_email'] ?><br>
Fondateur : <?php echo $fond['Fondateur'] ?><br>
Fondateur_tel : <?php echo $fond['Fondateur_tel'] ?><br>
Fondateur_email : <?php echo $fond['Fondateur_email'] ?><br>
COO : <?php echo $fond['COO'] ?><br>
COO_tel : <?php echo $fond['COO_tel'] ?><br>
COO_email : <?php echo $fond['COO_email'] ?><br>
CTO : <?php echo $fond['CTO'] ?><br>
CTO_tel : <?php echo $fond['CTO_tel'] ?><br>
CTO_email : <?php echo $fond['CTO_email'] ?><br>
CMO : <?php echo $fond['CMO'] ?><br>
CMO_tel : <?php echo $fond['CMO_tel'] ?><br>
CMO_email : <?php echo $fond['CMO_email'] ?><br>
CFO : <?php echo $fond['CFO'] ?><br>
CFO_tel : <?php echo $fond['CFO_tel'] ?><br>
CFO_email : <?php echo $fond['CFO_email'] ?><br>
Business_Developper : <?php echo $fond['Business_Developper'] ?><br>
Business_Developper_tel : <?php echo $fond['Business_Developper_tel'] ?><br>
Business_Developper_email : <?php echo $fond['Business_Developper_email'] ?><br>
from_linkedin : <?php echo $fond['from_linkedin'] ?><br>
linkedin_email_genere : <?php echo $fond['linkedin_email_genere'] ?><br>
linkedin_decision_makers : <?php echo $fond['linkedin_decision_makers'] ?><br>
decision_makers_email_generes : <?php echo $fond['decision_makers_email_generes'] ?><br>
linkedin_IT : <?php echo $fond['linkedin_IT'] ?><br>
IT_email_generes : <?php echo $fond['IT_email_generes'] ?><br>
                                        </div>


                                    </div>

                                </div>
                            </div>
                            <!-- Latest posts -->







                        </div>


                    </div>
                    <!-- /dashboard content -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    "paging": true,
                    "order": [[2, "desc"]],
                    "searching": true,
                    "bLengthChange": true,
                    "aoColumnDefs": [
                        {"sType": "numeric"}
                    ]
                });
            });
        </script>
    </body>
</html>
