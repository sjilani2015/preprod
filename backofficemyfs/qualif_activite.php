<?php
include('db.php');
$menu = 101;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Startups From LF</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />


        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->

    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->
        

        <!-- Page content -->
        <div class="">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">


                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Statistiques</h6>
                                </div>

                                <!-- Numbers -->
                                <div class="card-body py-0">

                                    <div class="col-md-12">

                                        <div class="table-responsive mb-4">
                                            <table id="example" class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Startup</th>
                                                        <th>Website</th>
                                                        <th>Siret</th>
                                                        <th>Description</th>
                                                        <th>Secteur</th>
                                                        <th>Sous secteur</th>
                                                        <th>Activite</th>
                                                        <th>Sous activite</th>
                                                        <th></th>
                                                        <th></th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $sql33 = mysqli_query($link, "SELECT startup.short_fr,startup.siret,startup.url,startup.id,startup.nom,activite.secteur,activite.sous_secteur,activite.activite,activite.sous_activite FROM `startup` INNER JOIN activite ON startup.id = activite.id_startup WHERE ( activite.`activite` =0  or activite.`activite` IS NULL ) AND startup.sup_radie =0 AND startup.status =1")or die(mysqli_error($link));
                                                    while ($data = mysqli_fetch_array($sql33)) {
                                                        $secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur where id=" . $data['secteur']));
                                                        $activite = mysqli_fetch_array(mysqli_query($link, "select * from last_activite where id=" . $data['activite']));
                                                        $ssecteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur where id=" . $data['sous_secteur']));
                                                        $sactivite = mysqli_fetch_array(mysqli_query($link, "select * from last_sous_activite where id=" . $data['sous_activite']));
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $data['nom'] ?></td>
                                                            <td><?php echo $data['url'] ?></td>
                                                            
                                                            <td><a href="https://www.verif.com/societe/<?php echo str_replace(" ","",$data['siret']) ?>/" target="_blank"><?php echo $data['siret'] ?></a></td>
                                                           <td><?php echo utf8_encode($data['short_fr']); ?></td>
                                                            <td><?php echo utf8_encode($secteur['nom_secteur']); ?></td>
                                                            <td>
                                                                <select id="s<?php echo $data['id'] ?>" class="form-control">
                                                                    <option value="<?php echo $ssecteur['id'] ?>"><?php echo $ssecteur['nom_sous_secteur'] ?></option>
                                                                    <?php
                                                                    $sqml = mysqli_query($link, "select nom_sous_secteur,id from sous_secteur where id_secteur=" . $secteur['id'] . " order by nom_sous_secteur");
                                                                    while ($dal = mysqli_fetch_array($sqml)) {
                                                                        ?>
                                                                        <option value="<?php echo $dal['id'] ?>"><?php echo utf8_encode($dal['nom_sous_secteur']) ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select  id="<?php echo $data['id'] ?>" class="form-control" onChange="getListSActivite(this)">
                                                                    <option value="<?php echo $activite['id'] ?>"><?php echo $activite['nom_activite'] ?></option>
                                                                    <?php
                                                                    $sqml2 = mysqli_query($link, "select * from last_activite order by nom_activite");
                                                                    while ($dal2 = mysqli_fetch_array($sqml2)) {
                                                                        ?>
                                                                        <option value="<?php echo $dal2['id'] ?>"><?php echo utf8_encode($dal2['nom_activite']) ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select  class="form-control"  id="sous_activ<?php echo $data['id'] ?>">
                                                                    <option value="<?php echo $sactivite['id'] ?>"><?php echo utf8_encode($sactivite['nom_sous_activite']) ?></option>

                                                                </select>
                                                            </td>
                                                            <td><button type="button" class="btn btn-success" id="saves" onclick="saves(<?php echo $data['id'] ?>)">Valider</button></td>
                                                            <td><a href="modif.php?sup=<?php echo $data['id'] ?>" target="_blank" class="btn btn-warning">Modifier</a></td>


                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>


                                    </div>

                                </div>
                            </div>
                            <!-- Latest posts -->







                        </div>


                    </div>
                    <!-- /dashboard content -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>

    </body>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

    <script>
                                                            $(document).ready(function () {
                                                                $('#example').DataTable({
                                                                    "paging": true,
                                                                    "order": [[2, "desc"]],
                                                                    "searching": true,
                                                                    "bLengthChange": true,
                                                                    "aoColumnDefs": [
                                                                        {"sType": "numeric"}
                                                                    ]
                                                                });
                                                            });
                                                            function getListSActivite(el) {
                                                                var id = el.id;
                                                               
                                                                var valeur = el.value;


                                                                $.ajax({
                                                                    url: 'getSousActivite.php?id=' + valeur,
                                                                    success: function (data) {
                                                                        document.getElementById("sous_activ" + id).options.length = 0;
                                                                        var t = eval(data);
                                                                        for (var i = 0; i < t.length; i++) {
                                                                            document.getElementById("sous_activ" + id).options[i + 1] = new Option(t[i].sactivite, t[i].id);
                                                                        }


                                                                    }
                                                                });
                                                            }
                                                            function saves(id) {
                                                                var ssecteur = document.getElementById("s" + id).value;
                                                                var activite = document.getElementById(id).value;
                                                                var sactivite = document.getElementById("sous_activ" + id).value;


                                                                $.ajax({
                                                                    url: 'saves.php?id=' + id + "&ssecteur=" + ssecteur + "&activite=" + activite + "&sactivite=" + sactivite,
                                                                    success: function (data) {
                                                                        alert('OK');


                                                                    }
                                                                });
                                                            }
    </script>
</html>
