<?php
include('db.php');
$menu = 10;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Startups From LF</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />


        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->

    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->
        <?php
        if (isset($_GET['idm'])) {
            mysqli_query($link, "update levees_sources set supp=1 where id=" . $_GET['idm']);
            echo '<script>window.location="levee.php"</script>';
        }
        ?>

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">


                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Statistiques</h6>
                                </div>

                                <!-- Numbers -->
                                <div class="card-body py-0">

                                    <div class="col-md-12">

                                        <div class="table-responsive mb-4">
                                            <table id="example" class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Startup</th>
                                                        <th>Source</th>
                                                        <th>Date Myfs</th>
                                                        <th>Date Crawl</th>
                                                        <th>Investisseur</th>
                                                        <th>Montant</th>
                                                        <th>Nb LF</th>
                                                        <th>Source</th>
                                                        <th>Supprimer</th>



                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $sql33 = mysqli_query($link, "SELECT * FROM `levees_sources` WHERE id_startup!=0 and supp=0 and (investisseurs!='' or montant!='')   order by startup")or die(mysqli_error($link));
                                                    while ($data = mysqli_fetch_array($sql33)) {
                                                        $nom = addslashes($data['startup']);
                                                        $nb_lf = 0;
                                                        $tab = explode("-", $data['publicationDate']);
                                                        $ddd1 = mysqli_query($link, "select * from lf where id_startup=" . $data['id_startup'] . " and year(date_ajout)='" . $tab[0] . "'")or die(mysqli_error($link));
                                                        $nb_lf = mysqli_num_rows($ddd1);
                                                        if ($nb_lf == 0) {
                                                           // echo "select id from startup where id=" . $data['id_startup'] . " and status=1<br>";
                                                            $verif = mysqli_num_rows(mysqli_query($link, "select id from startup where id=" . $data['id_startup'] . " and status=1"));
                                                            if ($verif > 0) {
                                                                ?>
                                                                <tr>
                                                                    <td><a href="modif.php?sup=<?php echo $data['id_startup'] ?>" target="_blank"><?php echo $data['id_startup'] . " / " . $data['startup']; ?></a></td>
                                                                    <td><?php echo $data['source']; ?></td>
                                                                    <td><?php echo $data['publicationDate']; ?></td>
                                                                    <td><?php echo $data['date_add']; ?></td>
                                                                    <td><?php echo utf8_encode($data['investisseurs']); ?></td>
                                                                    <td><?php echo $data['montant']; ?></td>
                                                                    <td><?php echo $nb_lf; ?></td>
                                                                    <td><a href="<?php echo $data['lien'] ?>" class="btn btn-info" target="_blank">Source</a></td>
                                                                    <td><a href="levee.php?idm=<?php echo $data['id'] ?>" class="btn btn-danger">Supprimer</a></td>

                                                                </tr>
                                                                <?php
                                                            }
                                                        } else {
                                                            mysqli_query($link, "update levees_sources set supp=1 where id=" . $data['id']);
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>


                                    </div>

                                </div>
                            </div>
                            <!-- Latest posts -->







                        </div>


                    </div>
                    <!-- /dashboard content -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>

    </body>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                "paging": true,
                "order": [[2, "desc"]],
                "searching": true,
                "bLengthChange": true,
                "aoColumnDefs": [
                    {"sType": "numeric"}
                ]
            });
        });
    </script>
</html>
