<?php
include('db.php');
$menu = 15;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Demande de liaison</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>
        <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style>
            .form-inline {
                display: block !important;

            }
        </style>
    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->
        <?php
        $sql = '';
        if (isset($_POST["submitx"])) {
            if ($_POST['userx'] != '' && $_POST['startupx'] != '')
                mysqli_query($link, "insert into user_startup (id, iduser, id_startup,dt,etat,source) values(NULL,'" . $_POST['userx'] . "', '" . $_POST['startupx'] . "','" . date("Y-m-d H:i:s") . "',1,0)");
        }

        if (isset($_GET['act']) && $_GET['act'] == 'del')
            mysqli_query($link, "delete from user_startup where id='" . $_GET['id'] . "'");
        ?>

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Liaison Startup / User</h6>
                                </div>
                                <div class="card-body py-0">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="bordered-justified-pill1">
                                            <h2>Demande liaison en attente</h2>
                                            <div class="col-md-12">
                                                <table id="example" class="table table-bordered table-hover datatable-highlight" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="10%"><strong>Nom</strong></th>
                                                            <th width="10%"><strong>Poste</strong></th>
                                                            <th width="10%"><strong>Email</strong></th>
                                                            <th width="10%"><strong>Startup</strong></th>
                                                            <th width="10%"><strong>Date</strong></th>
                                                            <th width="10%"><strong>Source</strong></th>
                                                            <th width="10%"><strong>Valider</strong></th>
                                                            <th width="10%"><strong>Supprimer</strong></th>
                                                        </tr>
                                                    </thead>
                                                    <?php
                                                    $s = mysqli_query($link, "select user_startup.id,user_startup.iduser,user_startup.poste,user_startup.id_startup,user_startup.dt from user_startup inner join startup on startup.id=user_startup.id_startup where user_startup.etat=0 and startup.status=1");
                                                    while ($dt = mysqli_fetch_object($s)) {
                                                        $user1 = mysqli_fetch_assoc(mysqli_query($link, "select * from user where id='" . $dt->iduser . "'"));
                                                        $startup1 = mysqli_fetch_assoc(mysqli_query($link, "select * from startup where id='" . $dt->id_startup . "'"));
                                                        ?>
                                                        <tr id="colfactif<?php echo $data1["id"]; ?>">
                                                            <td width="20%"><?php echo utf8_encode($user1['prenom'] . " " . $user1['nom']); ?></td>
                                                            <td width="20%"><?php echo $dt->poste; ?></td>
                                                            <td width="20%"><?php echo $user1['email']; ?></td>
                                                            <td width="20%"><a href="<?php echo $url . "fr/startup-france/" . generate_id($startup1['id']) . "/" . urlWriting(strtolower($startup1["nom"])) ?>" target="_blank"><?php echo strip_tags($startup1['nom']); ?></a></td>
                                                            <td width="20%"><?php echo strip_tags($dt->dt); ?></td>
                                                            <td width="20%"><?php
                                                                if ($dt->source == 1)
                                                                    echo "Ajout Startup";
                                                                else
                                                                    echo "Demande de liaison";
                                                                ?></td>
                                                            <td width="10%" style="text-align: center"><a onclick="valider(<?php echo $dt->id; ?>)" style="cursor:pointer; color:#F00">Valider</a></td>
                                                            <td width="10%" style="text-align: center"><a onclick="supprimer(<?php echo $dt->id; ?>)" style="cursor:pointer; color:#F00">Supprimer</a></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </table>


                                            </div>



                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Latest posts -->







                        </div>


                    </div>
                    <!-- /dashboard content -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

        <script>
                                                            $(document).ready(function () {
                                                                $('#example').DataTable({
                                                                    "paging": true,
                                                                    "order": [[2, "desc"]],
                                                                    "searching": true,
                                                                    "bLengthChange": true,
                                                                    "aoColumnDefs": [
                                                                        {"sType": "numeric"}
                                                                    ]
                                                                });
                                                            });
        </script>
        <script>


            function valider(id) {


                $.ajax({
                    url: 'ajax/valider_liaison.php?id=' + id,
                    success: function (data) {
                        var t = eval(data);

                        alert("Demande de liaison effectuée");



                    }
                });

            }

            function supprimer(id) {

                $.ajax({
                    url: 'ajax/supprimer_liaison.php?id=' + id,
                    success: function (data) {
                        var t = eval(data);

                        alert("Demande de liaison supprimée");


                    }
                });

            }

        </script>
    </body>
</html>
