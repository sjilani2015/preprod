<?php
include('db.php');
$menu = 2;

$idlf = $_GET['sup'];

$startup = mysqli_fetch_array(mysqli_query($link, 'select * from lf where id=' . $idlf));

$prix = $startup['montant'];
$tab = explode("-", $startup['date_ajout']);
$dtt = $tab[1] . '-' . $tab[2] . '-' . $tab[0];
$de = $startup['de'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Modifier LF</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        <link rel="stylesheet" href="assets/css/datepicker.css">
        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>

        <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>


        <link rel="stylesheet" href="textext/css/textext.core.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.tags.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.autocomplete.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.focus.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.prompt.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.arrow.css" type="text/css" />
        <script src="textext/js/textext.core.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.tags.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.autocomplete.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.suggestions.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.filter.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.focus.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.prompt.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.ajax.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.arrow.js" type="text/javascript" charset="utf-8"></script>

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style type="text/css" media="screen">
            .map_canvas {
                float: left;
            }
            .text-wrap{position: relative !important}
            .big_titre{
                padding: 10px;
                font-size: 20px;
                cursor: pointer;
                margin-bottom: 10px;
                margin-top: 10px;
            }
            .btn-primary{
                width:120px
            }
        </style>



    </head>

    <body>
        <div id="loader" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/site.gif"  /></div>
        <div id="loader2" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/siret.gif"  /></div>
        <div id="loader1" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/verif.gif"  /></div>
        <div id="loader3" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/Linkedin.gif"  /></div>
        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->


        <div id="page-content" class="page-content">


            <!-- Main content -->
            <div class="content-wrapper">

                <div class="card">
                    <?php
                    if (isset($_POST['de1'])) {

                        $id = $_POST["id"];
                        $montant_lf = $_POST["montant_lf"];
                        $dt_lf = $_POST["date_lf"];
                        $rachat = $_POST["rachat"];
                        $de_list2 = str_replace(array('[', '"', "]"), array('', '', ''), utf8_decode($_POST["de"]));

                        $de_list_autre2 = utf8_decode($_POST["de1"]);

                        if ($de_list2 != "") {
                            $de_lf = addslashes($de_list2 . "," . $de_list_autre2);
                        } else {
                            $de_lf = addslashes($de_list_autre2);
                        }


                        $split = explode("-", $dt_lf);
                        $annee = $split[2];
                        $jour = $split[1];
                        $mois = $split[0];
                        $datelf = $annee . "-" . $mois . "-" . $jour;

                        if (($rachat != '') && ($montant_lf != "NA")) {

                            mysqli_query($link, 'update lf set de="' . $de_lf . '", montant="' . $montant_lf . '",date_ajout="' . $datelf . '",rachat="'.$rachat.'"  where id=' . $id)or die(mysqli_error($link));
                        }
                        echo '<script>window.location="modif_lf.php"</script>';
                    }
                    ?>
                    <div class="card-body">
                        <div class="tabbable">

                            <form method="post" action="modifier_lf.php">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="bordered-justified-pill1">

                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="col-md-12" id="bloc_lf">
                                                    <div id="bloc_min_lf" class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Type</div>
                                                        <div class="col-md-4">
                                                            <select name="rachat" class="form-control">
                                                                <option value="" selected="selected"></option>
                                                                <option value="0" selected="">Lev&eacute;e de fonds</option>
                                                                <option value="1">Rachat</option>
                                                                <option value="2">IPO</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div id="bloc_min_lf" class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Montant</div>
                                                        <div class="col-md-4"><input type="text" value="<?php echo $prix; ?>" name="montant_lf" class="form-control"></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Date</div>
                                                        <div class="col-md-4">
                                                            <input type="text" value="<?php echo $dtt; ?>" class="datepicker form-control" autocomplete="off" onclick="refrech_datepicker()" class="form-control" name="date_lf">
                                                        </div>
                                                        <div class="col-md-2">Investisseurs</div>
                                                        <div class="col-md-4">
                                                            <textarea class="example list_invest_tag" name="de" style="width: 320px"></textarea>
                                                            <input name="de1" value="<?php echo $de; ?>" class="form-control" type="text" placeholder="Autres" />
                                                            <input name="id" value="<?php echo $startup['id']; ?>" type="hidden" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <a class="delete_lf_ligne">
                                                            <i style="color:#EC407A" class="fa fa-minus-circle fa-2x"></i>
                                                        </a>
                                                    </div>
                                                </div>


                                            </div>





                                        </div>


                                    </div>



                                </div>

                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                    <table width="1134" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" colspan="2"><button type="submit" class="btn btn-success">Modifier</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
            <!-- /main content -->





        </div>

        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcCIjgD7FA1zVC6EQwS5V6I-6xuUuwMiQ&sensor=false&amp;libraries=places"></script>
        <script src="assets/js/jquery.geocomplete.js"></script>
        <script src="assets/js/bootstrap-datepicker.js"></script>
        <script src="script/function.js"></script>
        <script src="script/site.js"></script>
        <script src="script/siret.js"></script>
        <script src="script/verif.js"></script>
        <script src="script/verifphontom.js"></script>
        <script src="script/google_linkedin_company.js"></script>
        <script src="script/qualif_linkedin_company.js"></script>
        <script src="script/verif_seul.js"></script>
        <script>




                                                                function verif_verif() {
                                                                    var siren = document.getElementById("verif").value;
                                                                    if (siren != 'https://www.verif.com/societe/') {
                                                                        $.ajax({
                                                                            url: 'getverifcom.php?field=' + siren,
                                                                            beforeSend: function () {
                                                                                $('#loader1').show();
                                                                                document.getElementById("page-content").style.opacity = 0.5;
                                                                            },
                                                                            complete: function () {
                                                                                $('#loader1').hide();
                                                                                document.getElementById("page-content").style.opacity = 1;
                                                                            },
                                                                            success: function (data) {

                                                                                var t = eval(data);
                                                                                document.getElementById("geocomplete").value = t[0].adresse;
                                                                                document.getElementById("siret").value = t[0].siret;
                                                                                document.getElementById("dp1").value = t[0].creation;
                                                                                document.getElementById("capital").value = t[0].capital;
                                                                                document.getElementById("siret").value = t[0].siret;
                                                                                document.getElementById("naf").value = t[0].naf;
                                                                                document.getElementById("libnaf").value = t[0].libelle;
                                                                                document.getElementById("juridique").value = t[0].juridique;
                                                                                document.getElementById("nom").value = t[0].startup;
                                                                                document.getElementById("societe").value = t[0].startup;
                                                                                document.getElementById("prenomf1").value = t[0].dirigeant1;



                                                                                //google_linkedin_company();


                                                                            }
                                                                        });
                                                                    }
                                                                }



        </script>
        <script>

            $(document).ready(function () {



                $(function () {

                    window.prettyPrint && prettyPrint();
                    $('#dp1').datepicker({
                        format: 'mm-dd-yyyy'
                    });
                });


                $(function () {
                    $("#datepicker").datepicker({dateFormat: 'dd/mm/yy'});
                    $("#datepicker1").datepicker({dateFormat: 'dd/mm/yy'});
                    $("#datepicker2").datepicker({dateFormat: 'dd/mm/yy'});
                    $("#datepicker3").datepicker({dateFormat: 'dd/mm/yy'});
                    $("#datepicker4").datepicker({dateFormat: 'dd/mm/yy'});
                });

                $(function () {
                    $("#geocomplete").geocomplete({
                        map: ".map_canvas",
                        details: "form",
                        types: ["geocode", "establishment"],
                    });

                    $("#find").click(function () {
                        $("#geocomplete").trigger("geocode");
                    });
                });


                $().ready(function () {

                    $("#concurrent").autocomplete('concurrent.php', {
                        multiple: true,
                        mustMatch: false,
                        autoFill: true

                    });





                    $("#concurrent").result(function (event, data, formatted) {

                        var hidden = $(this).parent().next().find(">:input");

                        hidden.val((hidden.val() ? hidden.val() + ";" : hidden.val()) + data[1]);

                    });


                });


            });
        </script>
        <script>

            $().ready(function () {
                window.prettyPrint && prettyPrint();

                refrech_datepicker();

            });


            function refrech_datepicker() {
                $(".datepicker").each(function () {
                    $(this).datepicker({
                        format: 'mm-dd-yyyy'
                    });


                });

            }
            function remplire_tags() {

                $('.list_invest_tag')
                        .textext({
                            plugins: 'tags autocomplete'
                        })
                        .bind('getSuggestions', function (e, data)
                        {
                            var list = [
<?php
$sql_inves = mysqli_query($link, "select id,new_name from temp_lf group by new_name order by new_name");
while ($data_invest = mysqli_fetch_array($sql_inves)) {
    ?>
                                    '<?php echo addslashes($data_invest['new_name']); ?>',
    <?php
}
?>
                            ],
                                    textext = $(e.target).textext()[0],
                                    query = (data ? data.query : '') || '';

                            $(this).trigger(
                                    'setSuggestions',
                                    {result: textext.itemManager().filter(list, query)}
                            );
                        })
                        ;
            }
            $(document).ready(function () {

                remplire_tags();
            }
            );
        </script>
    </body>
</html>
