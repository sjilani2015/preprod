/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $(function () {
        window.prettyPrint && prettyPrint();
        $('#dp1').datepicker({
            format: 'mm-dd-yyyy'
        });
    });


    $(function () {
        $("#datepicker").datepicker({dateFormat: 'dd/mm/yy'});
        $("#datepicker1").datepicker({dateFormat: 'dd/mm/yy'});
        $("#datepicker2").datepicker({dateFormat: 'dd/mm/yy'});
        $("#datepicker3").datepicker({dateFormat: 'dd/mm/yy'});
        $("#datepicker4").datepicker({dateFormat: 'dd/mm/yy'});
    });

    $(function () {
        $("#geocomplete").geocomplete({
            map: ".map_canvas",
            details: "form",
            types: ["geocode", "establishment"],
        });

        $("#find").click(function () {
            $("#geocomplete").trigger("geocode");
        });
    });

});

$().ready(function () {
    window.prettyPrint && prettyPrint();

    refrech_datepicker();

});


function refrech_datepicker() {
    $(".datepicker").each(function () {
        $(this).datepicker({
            format: 'mm-dd-yyyy'
        });


    });

}
function getListSSecteur(el) {
    var id = el.value;
    $.ajax({
        url: 'getSousSecteur.php?id=' + id,
        success: function (data) {
            document.getElementById("sous_secteur").options.length = 0;
            var t = eval(data);
            for (var i = 0; i < t.length; i++) {
                document.getElementById("sous_secteur").options[i + 1] = new Option(t[i].ssecteur, t[i].id);
            }


        }
    });

}
function getListSActivite(el) {
    var id = el.value;
    $.ajax({
        url: 'getSousActivite.php?id=' + id,
        success: function (data) {
            document.getElementById("sous_activ").options.length = 0;
            var t = eval(data);
            for (var i = 0; i < t.length; i++) {
                document.getElementById("sous_activ").options[i + 1] = new Option(t[i].sactivite, t[i].id);
            }


        }
    });
}
function getcreation() {
    var dat = document.getElementById("dp1").value;
    var res = dat.split("-");
    var val = res[2];
    document.getElementById("anneecreation").value = val;
}
function init() {
    $("#fond1_bloc").hide();
    $("#fond2_bloc").hide();
    $("#fond3_bloc").hide();
    $("#fond4_bloc").hide();
    $("#fond5_bloc").hide();
    $("#fond6_bloc").hide();
}
$(document).ready(function () {

    init();

});


$(document).ready(function () {

    $("body").on("click", ".delete_ca_ligne", function (e) {

        var r = confirm("Are you sure!");
        if (r == true) {
            $(this).parent("div").parent("div").remove();
        }
    });

});
$(document).ready(function () {

    $("#fond1").click(function () {
        $("#fond1_bloc").toggle("slow", function () {
        });
    });
    $("#fond2").click(function () {
        $("#fond2_bloc").toggle("slow", function () {
        });
    });
    $("#fond3").click(function () {
        $("#fond3_bloc").toggle("slow", function () {
        });
    });
    $("#fond4").click(function () {
        $("#fond4_bloc").toggle("slow", function () {
        });
    });
    $("#fond5").click(function () {
        $("#fond5_bloc").toggle("slow", function () {
        });
    });
    $("#fond6").click(function () {
        $("#fond6_bloc").toggle("slow", function () {
        });
    });

});
function add_ca() {
    $.ajax({
        type: "POST",
        url: "ajax/get_ca_bloc_form.php",
        success: function (data) {
            $("#bloc_ca").append(data);
        }
    });
}

$(document).ready(function () {

    $("body").on("click", ".delete_lf_ligne", function (e) {

        var r = confirm("Are you sure!");
        if (r == true) {
            $(this).parent("div").parent("div").remove();
        }
    });

});
function add_lf() {
    $.ajax({
        type: "POST",
        url: "ajax/get_bloc_lf_form.php",
        success: function (data) {
            $("#bloc_lf").append(data);
            remplire_tags();
        }
    });
}

$(document).ready(function () {

    $("body").on("click", ".delete_concour_ligne", function (e) {

        var r = confirm("Are you sure!");
        if (r == true) {
            $(this).parent("div").parent("div").remove();
        }
    });

});
function add_concour() {
    $.ajax({
        type: "POST",
        url: "ajax/get_bloc_concour_form.php",
        success: function (data) {
            $("#bloc_concour").append(data);
        }
    });
}

$(document).ready(function () {

    $("body").on("click", ".delete_subvention_ligne", function (e) {

        var r = confirm("Are you sure!");
        if (r == true) {
            $(this).parent("div").parent("div").remove();
        }
    });

});
function add_suvention() {
    $.ajax({
        type: "POST",
        url: "ajax/get_bloc_subvention_form.php",
        success: function (data) {
            $("#bloc_subvention").append(data);
        }
    });
}

$(document).ready(function () {

    $("body").on("click", ".delete_actualite_ligne", function (e) {

        var r = confirm("Are you sure!");
        if (r == true) {
            $(this).parent("div").parent("div").remove();
        }
    });

});
function add_actualite() {
    $.ajax({
        type: "POST",
        url: "ajax/get_bloc_actualite_form.php",
        success: function (data) {
            $("#bloc_actualite").append(data);
        }
    });
}
function getNews() {
    var link = document.getElementById("news_link").value;
    $.ajax({
        type: "POST",
        url: "ajax/getNews.php?link=" + link,
        success: function (data) {
            var t = eval(data);
            document.getElementById("titre_news").value = t[0].titre;
            document.getElementById("description_news").value = t[0].description;
            document.getElementById("date_news").value = t[0].date;
            document.getElementById("source_news").value = t[0].source;
        }
    });
}
function whois_startup() {
    var link = document.getElementById("whois").value;
    $.ajax({
        type: "POST",
        url: "ajax/whois.php?link=" + link,
        success: function (data) {
            document.getElementById("contenu_whois").innerHTML = data;
            $("#foo").trigger("click");
        }
    });
}
function traduire_accroche() {
    var short_fr = document.getElementById("short_fr").value;
    $.ajax({
        type: "POST",
        url: "https://scalitup.com/api_google_translate/translate.php?text_to_translate=" + short_fr,
        success: function (data) {
            document.getElementById("short_en").value = data;

        }
    });
}
function traduire_long() {
    var long_fr_text = document.getElementById("long_fr").value;
    var dtt = CKEDITOR.instances.long_fr.getData();
    var cleanText = dtt.replace(/<\/?[^>]+(>|$)/g, "");
    var cleanText = $("<div/>").html(cleanText).text();
    $.ajax({
        type: "POST",
        url: "https://scalitup.com/api_google_translate/translate.php?text_to_translate=" + cleanText,
        success: function (data) {
            CKEDITOR.instances.long_en.insertText(data);
        }
    });
}
function updateNom3(el) {
    var id = el.name;
    var champs = el.id;
    var valeur = el.value;
    $.ajax({
        url: 'updateNom.php?id=' + id + '&champs=' + champs + '&valeur=' + valeur,
        success: function (data) {
            var t = eval(data);
            document.getElementById(champs).value = t[0].champ;
        }

    });

}

function add_founder() {
        var id_startup = document.getElementById("startup_id_founder").value;
        var civf1 = document.getElementById("civf1").value;
        var fonctionf1 = document.getElementById("fonctionf1").value;
        var prenomf1 = document.getElementById("prenomf1").value;
        var nomf1 = document.getElementById("nomf1").value;
        var emailf1 = document.getElementById("emailf1").value;
        var telf1 = document.getElementById("telf1").value;
        var linkedinf1 = document.getElementById("linkedinf1").value;
        var imgf1 = document.getElementById("imgf1").value;


        var societef11 = document.getElementById("societef11").value;
        var fonctionf11 = document.getElementById("fonctionf11").value;
        var societe_def11 = document.getElementById("societe_def11").value;
        var societe_af11 = document.getElementById("societe_af11").value;

        var societef12 = document.getElementById("societef12").value;
        var fonctionf12 = document.getElementById("fonctionf12").value;
        var societe_def12 = document.getElementById("societe_def12").value;
        var societe_af12 = document.getElementById("societe_af12").value;

        var societef13 = document.getElementById("societef13").value;
        var fonctionf13 = document.getElementById("fonctionf13").value;
        var societe_def13 = document.getElementById("societe_def13").value;
        var societe_af13 = document.getElementById("societe_af13").value;

        var societef14 = document.getElementById("societef14").value;
        var fonctionf14 = document.getElementById("fonctionf14").value;
        var societe_def14 = document.getElementById("societe_def14").value;
        var societe_af14 = document.getElementById("societe_af14").value;

        var societef15 = document.getElementById("societef15").value;
        var fonctionf15 = document.getElementById("fonctionf15").value;
        var societe_def15 = document.getElementById("societe_def15").value;
        var societe_af15 = document.getElementById("societe_af15").value;

        var ecolef11 = document.getElementById("ecolef11").value;
        var diplomef11 = document.getElementById("diplomef11").value;
        var ecole_def11 = document.getElementById("ecole_def11").value;
        var ecole_af11 = document.getElementById("ecole_af11").value;

        var ecolef12 = document.getElementById("ecolef12").value;
        var diplomef12 = document.getElementById("diplomef12").value;
        var ecole_def12 = document.getElementById("ecole_def12").value;
        var ecole_af12 = document.getElementById("ecole_af12").value;

        var ecolef13 = document.getElementById("ecolef13").value;
        var diplomef13 = document.getElementById("diplomef13").value;
        var ecole_def13 = document.getElementById("ecole_def13").value;
        var ecole_af13 = document.getElementById("ecole_af13").value;

        $.ajax({
            url: 'ajax/add_founder.php?id_startup=' + id_startup
                    + '&civf1=' + civf1
                    + '&fonctionf1=' + fonctionf1
                    + '&prenomf1=' + prenomf1
                    + '&nomf1=' + nomf1
                    + '&emailf1=' + emailf1
                    + '&telf1=' + telf1
                    + '&imgf1=' + imgf1
                    + '&linkedinf1=' + linkedinf1
                    + '&societef11=' + societef11
                    + '&fonctionf11=' + fonctionf11
                    + '&societe_def11=' + societe_def11
                    + '&societe_af11=' + societe_af11
                    + '&societef12=' + societef12
                    + '&fonctionf12=' + fonctionf12
                    + '&societe_def12=' + societe_def12
                    + '&societe_af12=' + societe_af12
                    + '&societef13=' + societef13
                    + '&fonctionf13=' + fonctionf13
                    + '&societe_def13=' + societe_def13
                    + '&societe_af13=' + societe_af13
                    + '&societef14=' + societef14
                    + '&fonctionf14=' + fonctionf14
                    + '&societe_def14=' + societe_def14
                    + '&societe_af14=' + societe_af14
                    + '&societef15=' + societef15
                    + '&fonctionf15=' + fonctionf15
                    + '&societe_def15=' + societe_def15
                    + '&societe_af15=' + societe_af15
                    + '&ecolef11=' + ecolef11
                    + '&diplomef11=' + diplomef11
                    + '&ecole_def11=' + ecole_def11
                    + '&ecole_af11=' + ecole_af11
                    + '&ecolef12=' + ecolef12
                    + '&diplomef12=' + diplomef12
                    + '&ecole_def12=' + ecole_def12
                    + '&ecole_af12=' + ecole_af12
                    + '&ecolef13=' + ecolef13
                    + '&diplomef13=' + diplomef13
                    + '&ecole_def13=' + ecole_def13
                    + '&ecole_af13=' + ecole_af13,
            success: function (data) {
                $('#myModal_add_founder').modal('hide');
                document.getElementById("my_bloc_founder_ajax").innerHTML = data;

            }

        });

    }