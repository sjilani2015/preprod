function qualif_linkedin_company() {
    document.getElementById("page-content").style.opacity = 1;
    var linkedin = document.getElementById('linkedin').value;
    $.ajax({
        url: 'https://scalitup.com/qualif_startup/qualif_linkedin_company.php?linkedin=' + linkedin,
        beforeSend: function () {
            $('#loader3').show();
            document.getElementById("page-content").style.opacity = 0.5;
        },
        complete: function () {
            $('#loader3').hide();
            document.getElementById("page-content").style.opacity = 1;
        },
        success: function (data) {

            var t = eval(data);
            JSON.stringify(t);
            /*if (t[0][4] != "") {
                CKEDITOR.instances.long_fr.insertText(t[0][4]);
                $.ajax({
                    type: "POST",
                    url: "https://scalitup.com/api_google_translate/translate.php?text_to_translate=" + t[0][4],
                    success: function (data) {
                        CKEDITOR.instances.long_en.insertText(t[0][4]);
                    }
                });
            }*/
            if (document.getElementById("effectif").value == "")
                document.getElementById("effectif").value = t[0][8];
            if (document.getElementById("tags").value == "")
                document.getElementById("tags").value = t[0][11];
            if (document.getElementById("dp1").value == "") {
                document.getElementById("dp1").value = "01-01-" + t[0][10];
            }
            if (document.getElementById("geocomplete").value == "") {
                document.getElementById("geocomplete").value = t[0][12];
            }
            if (document.getElementById("nom").value == "") {
                document.getElementById("nom").value = t[0][2];
            }
            if (document.getElementById("site").value == "") {
                document.getElementById("site").value = t[0][6];
            }
            document.getElementById("logos").src = t[0][3];
        }
    });
}