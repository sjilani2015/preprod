<?php
include('db.php');
$menu = 1;
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
$j = 0;
$attente = mysqli_num_rows(mysqli_query($link, "select id from startup where (status=2 or status=3)"));
$attente_mnt = mysqli_num_rows(mysqli_query($link, "select id from startup where (status=2 or status=3) and date_add like '" . date('Y-m-d') . "%'"));


$sql33 = mysqli_query($link, "SELECT * FROM `levees_sources` WHERE id_startup!=0 and supp=0");
while ($data = mysqli_fetch_array($sql33)) {
    $tab = explode("-", $data['publicationDate']);
    $ddd1 = mysqli_query($link, "select * from lf where id_startup=" . $data['id_startup'] . " and year(date_ajout)='" . $tab[0] . "'");
    $nb_lf = mysqli_num_rows($ddd1);
    if ($nb_lf == 0) {
        $j++;
    }
}


$new_startup = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `detect_startup1` WHERE id_startup=0 and supp=0"));
$new_startup_mnt = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `detect_startup1` WHERE date_add like '" . date('Y-m-d') . "%'"));
$stage = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `offre_stage` WHERE id_startup=0 and sup=0   group by startup "));
$stage_mnt = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `offre_stage` WHERE id_startup=0 and sup=0 and date_add like '" . date('Y-m-d') . "%' group by startup "));
$flow = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `data_enquete` WHERE id_startup=0 and supp=0"));
$flow_mnt = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `data_enquete` WHERE id_startup=0 and date_add like '" . date('Y-m-d') . "%' and supp=0"));


$news = mysqli_num_rows(mysqli_query($link, "select id from new_startups where id_startup=0 and sup=0"));

$emploi = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `offre_emploi` WHERE id_startup=0 and bloc=0 and sup=0 group by startup"));
$emploi_mnt = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `offre_emploi` WHERE id_startup=0 and bloc=0 and date_add like '" . date('Y-m-d') . "%' and sup=0 group by startup"));

$sup_lf = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `levees_sources` WHERE id_startup=0 and supp=0"));
$sup_lf_mnt = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `levees_sources` WHERE id_startup=0 and date_add like '" . date('Y-m-d') . "%' and supp=0"));
$nb_list = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `user_save_list`"));
$nb_list_favoris = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `user_favoris`"));
$nb_inscrit = mysqli_num_rows(mysqli_query($link, "SELECT id FROM `user` where date(date_add)='" . date('Y-m-d') . "'"));
$nb_user_list = mysqli_num_rows(mysqli_query($link, "SELECT count(*) as nb,user FROM `user_save_list` group by user"));
$nb_user_connecte = mysqli_num_rows(mysqli_query($link, "SELECT count(*) as nb,user FROM `startup_user_connexion`  where date(date_add)='" . date('Y-m-d') . "' group by user"));

function getLatestMonth($dernierMois) {
    $arParMois = array();
    $date_courant = date("Y-m-d");

    for ($i = 0; $i < $dernierMois; $i++) {
        if ($i === 0) {
            $arParMois[$i] = array(
                'month' => date("m"), 'annee' => date('Y')
            );
        } else {
            //- 1 mois à la date du jour
            $mois = date("m", strtotime("-1 month", strtotime($date_courant)));
            $annee = date("Y", strtotime("-1 month", strtotime($date_courant)));
            $arParMois[$i] = array('month' => $mois, 'annee' => $annee);
            $date_courant = date("$annee-" . $mois . "-d");
        }
    }

    return $arParMois;
}

function strposa($haystack, $needles = array(), $offset = 0) {
    $chr = array();
    foreach ($needles as $needle) {
        $res = strpos($haystack, $needle, $offset);
        if ($res !== false)
            $chr[$needle] = $res;
    }
    if (empty($chr))
        return false;
    return min($chr);
}

function change_date($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    $creation = $jour . "/" . $mois . "/" . $annee;
    return $creation;
}

function clean_investors($string) {
    $array = explode(',', $string);
    return implode(", ", $array);
}

function change_date_fr_chaine($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "janvier";
    if (($mois == "02"))
        $mm = "f&eacute;vrier";
    if (($mois == "03"))
        $mm = "mars";
    if (($mois == "04"))
        $mm = "avril";
    if (($mois == "05"))
        $mm = "mai";
    if (($mois == "06"))
        $mm = "juin";
    if (($mois == "07"))
        $mm = "juillet";
    if (($mois == "08"))
        $mm = "ao&ucirc;t";
    if (($mois == "09"))
        $mm = "septembre";
    if (($mois == "10"))
        $mm = "octobre";
    if (($mois == "11"))
        $mm = "novembre";
    if ($mois == "12")
        $mm = "d&eacute;cembre";

    $creation = $jour . " " . $mm . " " . $annee;
    return $creation;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Analytics</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/core.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->

    </head>

    <body>

        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->


        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->


                    <!-- /main charts -->


                    <!-- Dashboard content -->
                    <div class="row">
                        <div class="col-xl-12">


                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h6 class="card-title">Nouvelle Startups détectées à partir de </h6>
                                </div>

                                <!-- Numbers -->
                                <div class="card-body py-0">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <a href="attente.php">
                                                <div class="card bg-teal-400">
                                                    <div class="card-body">
                                                        <div class="d-flex">
                                                            <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $attente; ?></h3>
                                                            <span class="badge bg-teal-800 badge-pill align-self-center ml-auto" style="font-size: 20px;">+<?php echo $attente_mnt; ?></span>
                                                        </div>
                                                        <div>
                                                            Front
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-lg-4">

                                            <a href="stage.php">
                                                <div class="card bg-teal-400">
                                                    <div class="card-body">
                                                        <div class="d-flex">
                                                            <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $stage; ?></h3>
                                                            <span class="badge bg-teal-800 badge-pill align-self-center ml-auto" style="font-size: 20px;">+<?php echo $stage_mnt; ?></span>

                                                        </div>

                                                        <div>
                                                            Offres de Stage

                                                        </div>
                                                    </div>

                                                    <div id="server-load"></div>
                                                </div>
                                            </a>

                                        </div>

                                        <div class="col-lg-4">
                                            <a href="emploi.php">
                                                <div class="card bg-teal-400">
                                                    <div class="card-body">
                                                        <div class="d-flex">
                                                            <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $emploi; ?></h3>
                                                            <span class="badge bg-teal-800 badge-pill align-self-center ml-auto" style="font-size: 20px;">+<?php echo $emploi_mnt; ?></span>
                                                        </div>
                                                        <div>
                                                            Offres d'emploi
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-lg-4">
                                            <a href="lf.php">
                                                <div class="card bg-teal-400">
                                                    <div class="card-body">
                                                        <div class="d-flex">
                                                            <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $sup_lf; ?></h3>
                                                            <span class="badge bg-teal-800 badge-pill align-self-center ml-auto" style="font-size: 20px;">+<?php echo $sup_lf_mnt; ?></span>
                                                        </div>
                                                        <div>
                                                            Levées de fonds
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-header header-elements-inline">
                                        <h6 class="card-title">Nouvelles Levées de fonds </h6>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <a href="levee.php">
                                                <div class="card bg-blue-400">
                                                    <div class="card-body">
                                                        <div class="d-flex">
                                                            <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $j; ?></h3>
                                                        </div>
                                                        <div>
                                                            Nouvelles levées de fonds
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-header header-elements-inline">
                                        <h6 class="card-title">Startups à la recherche de fonds </h6>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">

                                            <a href="flow.php">
                                                <div class="card bg-indigo-400">
                                                    <div class="card-body">
                                                        <div class="d-flex">
                                                            <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $flow; ?></h3>
                                                            <span class="badge bg-indigo-800 badge-pill align-self-center ml-auto" style="font-size: 20px;">+<?php echo $flow_mnt; ?></span>
                                                        </div>

                                                        <div>
                                                            Deal Flow
                                                        </div>
                                                    </div>

                                                    <div id="today-revenue"></div>
                                                </div>
                                            </a>

                                        </div>
                                    </div>
                                    <div class="card-header header-elements-inline">
                                        <h6 class="card-title">Listes enregistrées </h6>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">

                                            <a href="">
                                                <div class="card bg-indigo-400">
                                                    <div class="card-body">
                                                        <div class="d-flex">
                                                            <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $nb_list; ?></h3>

                                                        </div>

                                                        <div>
                                                            Listes enregistrées
                                                        </div>
                                                    </div>

                                                    <div id="today-revenue"></div>
                                                </div>
                                            </a>

                                        </div>
                                        <div class="col-lg-4">

                                            <a href="">
                                                <div class="card bg-indigo-400">
                                                    <div class="card-body">
                                                        <div class="d-flex">
                                                            <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $nb_user_list; ?></h3>

                                                        </div>

                                                        <div>
                                                            Utilisateurs ayant une listes
                                                        </div>
                                                    </div>

                                                    <div id="today-revenue"></div>
                                                </div>
                                            </a>

                                        </div>
                                        <div class="col-lg-4">

                                            <a href="">
                                                <div class="card bg-indigo-400">
                                                    <div class="card-body">
                                                        <div class="d-flex">
                                                            <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $nb_list_favoris; ?></h3>

                                                        </div>

                                                        <div>
                                                            Utilisateurs ayant mémoriser des startups
                                                        </div>
                                                    </div>

                                                    <div id="today-revenue"></div>
                                                </div>
                                            </a>

                                        </div>
                                    </div>
                                    <div class="card-header header-elements-inline">
                                        <h6 class="card-title">Inscrits</h6>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">

                                            <a href="">
                                                <div class="card bg-indigo-400">
                                                    <div class="card-body">
                                                        <div class="d-flex">
                                                            <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $nb_inscrit; ?></h3>

                                                        </div>

                                                        <div>
                                                            Inscrits aujourd'hui
                                                        </div>
                                                    </div>

                                                    <div id="today-revenue"></div>
                                                </div>
                                            </a>

                                        </div>
                                        <div class="col-lg-4">

                                            <a href="">
                                                <div class="card bg-indigo-400">
                                                    <div class="card-body">
                                                        <div class="d-flex">
                                                            <h3 class="font-weight-semibold mb-0" style="font-size: 4em"><?php echo $nb_user_connecte; ?></h3>

                                                        </div>

                                                        <div>
                                                            connecté aujourd'hui
                                                        </div>
                                                    </div>

                                                    <div id="today-revenue"></div>
                                                </div>
                                            </a>

                                        </div>

                                    </div>
                                    <div class="row">
                                        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                                        <div class="col-xl-12 col-lg-12">
                                            <script type="text/javascript">
                                                google.charts.load("current", {packages: ['corechart']});
                                                google.charts.setOnLoadCallback(drawChart2);
                                                function drawChart2() {
                                                    var data = google.visualization.arrayToDataTable([
                                                        ["Element", "Inscrits", {role: "style"}],
<?php
$tot = 0;

for ($i = 30; $i >= 0; $i--) {
    $sql = mysqli_query($link, "select count(*) as nb from user WHERE  date(date_add) = (CURDATE() - INTERVAL " . $i . " DAY)");
    $data = mysqli_fetch_array($sql);
    $tot = $tot + $data['nb'];
    if ($i % 2 == 0) {
        ?>

                                                                ["", <?php echo $data['nb']; ?>, "#B24191"],
        <?php
    } else {
        ?>
                                                                ["", <?php echo $data['nb']; ?>, "#5673BA"],
        <?php
    }
}
?>


                                                    ]);

                                                    var view = new google.visualization.DataView(data);
                                                    view.setColumns([0, 1,
                                                        {calc: "stringify",
                                                            sourceColumn: 1,
                                                            type: "string",
                                                            role: "annotation"},
                                                        2]);

                                                    var options = {
                                                        title: "La moyenne est <?php echo round($tot / 30); ?> inscrits / jour",
                                                        width: 1200,
                                                        height: 400,
                                                        bar: {groupWidth: "95%"},
                                                        legend: {position: "none"},
                                                    };
                                                    var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values44"));
                                                    chart.draw(view, options);
                                                }
                                            </script>
                                         
                                        </div>
                                        <div id="columnchart_values44" style="width: 100%; height: 500px"></div>


                                    </div>
                                    <div class="row">
                                        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                                        <div class="col-xl-12 col-lg-12">
                                            <script type="text/javascript">
                                                google.charts.load("current", {packages: ['corechart']});
                                                google.charts.setOnLoadCallback(drawChart3);
                                                function drawChart3() {
                                                    var data = google.visualization.arrayToDataTable([
                                                        ["Element", "Connexions", {role: "style"}],
<?php
$tot = 0;

for ($i = 30; $i >= 0; $i--) {
    $sql = mysqli_query($link, "select count(*) as nb,user from startup_user_connexion WHERE  date(date_add) = (CURDATE() - INTERVAL " . $i . " DAY)");
    $data = mysqli_fetch_array($sql);
    $tot = $tot + $data['nb'];
    if ($i % 2 == 0) {
        ?>

                                                                ["", <?php echo $data['nb']; ?>, "#B24191"],
        <?php
    } else {
        ?>
                                                                ["", <?php echo $data['nb']; ?>, "#5673BA"],
        <?php
    }
}
?>


                                                    ]);

                                                    var view = new google.visualization.DataView(data);
                                                    view.setColumns([0, 1,
                                                        {calc: "stringify",
                                                            sourceColumn: 1,
                                                            type: "string",
                                                            role: "annotation"},
                                                        2]);

                                                    var options = {
                                                        title: "La moyenne est <?php echo round($tot / 30); ?>  connexions / jour",
                                                        width: 1200,
                                                        height: 400,
                                                        bar: {groupWidth: "95%"},
                                                        legend: {position: "none"},
                                                    };
                                                    var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values99"));
                                                    chart.draw(view, options);
                                                }
                                            </script>
                                            
                                        </div>
                                        <div id="columnchart_values99" style="width: 100%; height: 500px"></div>


                                    </div>
                                </div>
                            </div>
                            <!-- Latest posts -->







                        </div>


                    </div>
                    <!-- /dashboard content -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->


        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="http://themeforest.net/user/Kopyov" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>

    </body>
</html>
