<?php
include('db.php');
$menu = 2;

if (isset($_GET['idd'])) {
    $startup = mysqli_fetch_array(mysqli_query($link, "select * from data_enquete where id=" . $_GET['idd']));
}
if (isset($_GET['id_startup_websummit'])) {
    $startup = mysqli_fetch_array(mysqli_query($link, "select * from websummit where id=" . $_GET['id_startup_websummit']));
}
if (isset($_GET['id_startup'])) {
    $startup = mysqli_fetch_array(mysqli_query($link, "select * from offre_emploi where id=" . $_GET['id_startup']));
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ajouter Startup</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        <link rel="stylesheet" href="assets/css/datepicker.css">
        <!-- Core JS files -->
        <script src="global_assets/js/main/jquery.min.js"></script>
        <script src="global_assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="global_assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="global_assets/js/plugins/ui/slinky.min.js"></script>
        <script src="global_assets/js/plugins/ui/fab.min.js"></script>
        <script src="global_assets/js/plugins/ui/ripple.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script src="global_assets/js/plugins/visualization/d3/d3.min.js"></script>
        <script src="global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
        <script src="global_assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script src="global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script src="global_assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="global_assets/js/plugins/pickers/daterangepicker.js"></script>

        <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>


        <link rel="stylesheet" href="textext/css/textext.core.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.tags.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.autocomplete.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.focus.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.prompt.css" type="text/css" />
        <link rel="stylesheet" href="textext/css/textext.plugin.arrow.css" type="text/css" />
        <script src="textext/js/textext.core.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.tags.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.autocomplete.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.suggestions.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.filter.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.focus.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.prompt.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.ajax.js" type="text/javascript" charset="utf-8"></script>
        <script src="textext/js/textext.plugin.arrow.js" type="text/javascript" charset="utf-8"></script>

        <script src="assets/js/app.js"></script>
        <!-- /theme JS files -->
        <style type="text/css" media="screen">
            .map_canvas {
                float: left;
            }
            .text-wrap{position: relative !important}
            .big_titre{
                padding: 10px;
                font-size: 20px;
                cursor: pointer;
                margin-bottom: 10px;
                margin-top: 10px;
            }
            .btn-primary{
                width:120px
            }
        </style>



    </head>

    <body>
        <div id="loader" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/site.gif"  /></div>
        <div id="loader2" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/siret.gif"  /></div>
        <div id="loader1" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/verif.gif"  /></div>
        <div id="loader3" style="display:none;position: fixed;z-index: 9999;top: 13%;left: 28%;"><img src="assets/img/Linkedin.gif"  /></div>
        <!-- Page header -->
        <?php include('header.php'); ?>
        <!-- /page header -->


        <div class="page-content" id="page-content">

            <div class="content-wrapper">

                <div class="card">


                    <div class="card-body">
                        <div class="tabbable">


                            <div class="tab-content">
                                <div class="tab-pane active" id="bordered-justified-pill1">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form method="post" action="add.php" enctype="multipart/form-data">
                                                <div class=" bg-indigo-400 big_titre" id="info">Informations générales</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="map_canvas"></div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Lien de verif.com</div>
                                                        <div class="col-md-3"><input type="text" class="form-control" value="https://www.verif.com/societe/<?php echo substr(str_replace(" ", "", trim($startup['siret'])), 0, 9); ?>" name="verif" id="verif" placeholder="https://www.verif.com/societe/STARTUP-798203774/" /></div>
                                                        <div class="col-md-1"><button type="button" onclick="verif_verif_seul()" class="btn btn-success" >V&eacute;rifier</button></div>
                                                        <div class="col-md-2">Site web</div>
                                                        <div class="col-md-3"><input type="text" value="<?php echo $startup['siteweb'] ?>" class="form-control" name="site" id="site" /></div>
                                                        <div class="col-md-1"> <button type="button" class="btn btn-success" onclick="verifsite()">V&eacute;rifier</button></div>

                                                    </div>

                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Nom Startup</div>
                                                        <div class="col-md-4"><input type="text" class="form-control"  name="nom" value="<?php echo stripslashes($startup['startup'] . $startup['societe']); ?>" id="nom" required /></div>
                                                        <div class="col-md-2">Nom juridique</div>
                                                        <div class="col-md-4"><input type="text" class="form-control" name="societe" value="<?php echo $startup['juridique'] ?>" id="societe" required /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Date de cr&eacute;ation</div>
                                                        <div class="col-md-4"><input type="text" name="cal" id="dp1" autocomplete="off" value="<?php echo $startup['date_creation']; ?>" class="datepicker form-control" onclick="refrech_datepicker()"></div>
                                                        <div class="col-md-2">Effectif</div>
                                                        <div class="col-md-4"><input type="text" class="form-control" name="effectif" value="<?php echo $startup['effectif'] ?>" id="effectif" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">

                                                        <div class="col-md-2">Email</div>
                                                        <div class="col-md-4"><input type="text" class="form-control" value="<?php echo $startup['email'] ?>" name="email" id="email" /></div>
                                                        <div class="col-md-2">T&eacute;l&eacute;phone</div>
                                                        <div class="col-md-4"><input type="text" name="tel" id="telephone" value="<?php echo $startup['tel'] ?>" class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">

                                                    </div>

                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Twitter</div>
                                                        <div class="col-md-4"><input type="text" name="twitter" id="twitter" value="<?php echo $startup['twitter'] ?>"  class="form-control" /></div>

                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Youtube</div>
                                                        <div class="col-md-4"><input type="text" name="youtube" id="youtube" value="<?php echo $startup['youtube'] ?>" class="form-control" /></div>
                                                        <div class="col-md-2">Facebook</div>
                                                        <div class="col-md-4"><input type="text" name="facebook" id="facebook" value="<?php echo $startup['facebook'] ?>"  class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Viadeo</div>
                                                        <div class="col-md-4"><input type="text" name="viadeo" id="viadeo"  class="form-control" /></div>
                                                        <div class="col-md-2">Linkedin</div>
                                                        <div class="col-md-3"><input type="text" name="linkedin" id="linkedin" value="<?php echo $startup['linkedin'] ?>"  class="form-control" /></div>
                                                        <div class="col-md-1"> <button type="button" class="btn btn-success" onclick="verifphontom()">V&eacute;rifier</button></div>                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Blog</div>
                                                        <div class="col-md-4"><input type="text" name="blog" id="blog" class="form-control" /></div>
                                                        <div class="col-md-2">Google Plus</div>
                                                        <div class="col-md-4"><input type="text" name="google" id="google" class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Pinterest</div>
                                                        <div class="col-md-4"><input type="text" name="pinterest" id="pinterest" class="form-control"/></div>
                                                        <div class="col-md-2">RSS</div>
                                                        <div class="col-md-4"><input type="text" name="rss" id="rss"  class="form-control" /></div>
                                                    </div>

                                                </div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">

                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Adresse</div>
                                                        <div class="col-md-10"><input type="text" class="form-control" name="adresse" value="<?php echo $startup['adress_startup'] ?>"  placeholder="Entrez votre adresse" id="geocomplete" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Code Postal</div>
                                                        <div class="col-md-4"><input type="text" name="postal_code" class="form-control" /></div>
                                                        <div class="col-md-2">Ville</div>
                                                        <div class="col-md-4"><input type="text" name="locality" class="form-control" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">LAT</div>
                                                        <div class="col-md-4"><input type="text" name="lat" class="form-control" /></div>
                                                        <div class="col-md-2">LNG</div>
                                                        <div class="col-md-4"><input type="text" name="lng" class="form-control" /></div>
                                                    </div>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="jurid">Informations juridique</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Siret</div>
                                                        <div class="col-md-4"><input type="text" name="siret" value="<?php echo $startup['siret'] ?>" id="siret" class="form-control" /></div>
                                                        <div class="col-md-2">Capital Social</div>
                                                        <div class="col-md-4"><input type="text" name="capital" id="capital" class="form-control"  /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">NAF</div>
                                                        <div class="col-md-4"><input type="text" name="naf" id="naf" class="form-control" /></div>
                                                        <div class="col-md-2">Libell&eacute; NAF</div>
                                                        <div class="col-md-4"><input type="text" name="libnaf" id="libnaf" class="form-control"  /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Forme Juridique</div>
                                                        <div class="col-md-4"><input type="text" name="juridique" id="juridique" class="form-control" /></div>
                                                    </div>
                                                </div>


                                                <div class=" bg-indigo-400 big_titre" id="list_sect" >Secteur d'activité</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Accroche FR</div>
                                                        <div class="col-md-8"><input type="text" name="short_fr" value="<?php echo $startup['description']; ?>" class="form-control" id="short_fr" required onblur="copier()" /></div>
                                                        <div class="col-md-2"><button type="button" class="btn btn-info" onclick="traduire_accroche()" >Traduire</button></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Description FR</div>
                                                        <div class="col-md-8"><textarea name="long_fr" id="long_fr" class="form-control" required></textarea>
                                                            <script type="text/javascript">CKEDITOR.replace('long_fr');</script></div>
                                                        <div class="col-md-2"><button type="button" class="btn btn-info" onclick="traduire_long()" >Traduire</button></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Accroche EN</div>
                                                        <div class="col-md-10"><input type="text" name="short_en" class="form-control" value="<?php echo $startup['description'] ?>" id="short_en" required onblur="copier()" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Description EN</div>
                                                        <div class="col-md-10"><textarea name="long_en" id="long_en" ></textarea>
                                                            <script type="text/javascript">CKEDITOR.replace('long_en');</script></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Tags</div>
                                                        <div class="col-md-4"><input type="text" name="tags" id="tags" value="<?php echo $startup['Tags'] ?>" class="form-control" required /></div>
                                                        <div class="col-md-2">Cible</div>
                                                        <div class="col-md-4">
                                                            <input type="checkbox" value="B2C" name="cible[]" style="margin-top: -3px; margin-right: 5px; width: 20px;" />
                                                            B2C &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <input type="checkbox" value="B2B" name="cible[]" style="margin-top: -3px; margin-right: 5px; width: 20px;" />
                                                            B2B &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <input type="checkbox" value="C2C" name="cible[]" style="margin-top: -3px; margin-right: 5px; width: 20px;" />
                                                            C2C
                                                        </div>
                                                    </div>



                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Secteur</div>
                                                        <div class="col-md-4"><select  name="secteur" class="form-control" id="secteur" onChange="getListSSecteur(this)" required>
                                                                <?php
                                                                if (isset($_GET['idd'])) {
                                                                    $ooo = mysqli_query($link, "select * from secteur where id=" . $startup['secteur']);
                                                                    $sql_sector = mysqli_fetch_array($ooo);
                                                                    ?>
                                                                    <option value="<?php echo $sql_sector['id'] ?>"><?php echo utf8_encode($sql_sector['nom_secteur']); ?></option>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <option></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <?php
                                                                $sql = mysqli_query($link, "select * from secteur order by nom_secteur");
                                                                while ($data = mysqli_fetch_array($sql)) {
                                                                    ?>
                                                                    <option value="<?php echo $data['id']; ?>"><?php echo utf8_encode($data['nom_secteur']); ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select></div>
                                                        <div class="col-md-2">Sous Secteur</div>
                                                        <?php
                                                        if (isset($_GET['idd'])) {
                                                            $yyy = mysqli_query($link, "select * from sous_secteur where id=" . $startup['sous_secteur']);
                                                            $sql_sous_sector = mysqli_fetch_array($yyy);
                                                            ?>
                                                            <div class="col-md-4">
                                                                <select name="sous_secteur" class="form-control" id="sous_secteur">
                                                                    <option value="<?php echo $sql_sous_sector['id']; ?>"><?php echo utf8_encode($sql_sous_sector['nom_sous_secteur']); ?></option>
                                                                </select>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div class="col-md-4"><select name="sous_secteur" class="form-control" id="sous_secteur"></select></div>
                                                            <?php
                                                        }
                                                        ?>


                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Activite</div>
                                                        <div class="col-md-4">
                                                            <select  name="activ" id="activ" class="form-control" onChange="getListSActivite(this)" required>
                                                                <option></option>
                                                                <?php
                                                                $sql = mysqli_query($link, "select * from last_activite order by nom_activite");
                                                                while ($data = mysqli_fetch_array($sql)) {
                                                                    ?>
                                                                    <option value="<?php echo $data['id']; ?>"><?php echo utf8_encode($data['nom_activite']); ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">Sous Activit&eacute;</div>
                                                        <div class="col-md-4"><select name="sous_activ" class="form-control" id="sous_activ"></select></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Logo</div>
                                                        <div class="col-md-2"><input type="file" name="logo"  class="form-control" /></div>
                                                        <div class="col-md-2"><img src="" style="width:150px" id="logos"></div>
                                                        <div class="col-md-2">JEI</div>
                                                        <div class="col-md-4">
                                                            <input type="radio" value="1" name="jei" id="jei" style="width:20px; margin-left:20px;">
                                                            Oui
                                                            <input type="radio" checked="checked" value="0" name="jei" id="jei" style="width:20px; margin-left:20px;">
                                                            Non
                                                        </div>
                                                    </div>
                                                </div>
                                                <div  class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="col-md-12" id="bloc_ca">
                                                        <div id="bloc_min_ca" class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Chiffre Affaire</div>
                                                            <div class="col-md-4"><input type="text" name="ca[]" class="form-control" /></div>
                                                            <div class="col-md-2">Année</div>
                                                            <div class="col-md-4"><input type="text" name="anneeca[]" class="form-control" /></div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div id="add_ca">
                                                            <div class="col-lg-3">
                                                                <button onclick="add_ca();" type="button" class="btn btn-primary" >
                                                                    <i class="icon-plus-circle2 position-left"></i> Ajouter</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class=" bg-indigo-400 big_titre" id="fond1" > <i class="fa fa-plus-circle"> </i> Fondateur 1</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond1_bloc">
                                                    <table class="table"  border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td> Civilite </td>
                                                            <td ><select class="form-control" name="civf1" style="width: 200px;">
                                                                    <option value="0">M</option>
                                                                    <option value="1">Mme</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Pr&eacute;nom </td>
                                                            <td><input type="text" class="form-control" name="prenomf1" value="<?php echo $startup['prenom']; ?>" id="prenomf1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Nom </td>
                                                            <td><input type="text" class="form-control" name="nomf1" value="<?php echo $startup['nom'] ?>" id="nomf1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><select name="fonctionf1" id="fonctionf1" class="form-control" style="width: 200px;">
                                                                    <option value="">--Choisir fonction--</option>
                                                                    <option value="6"> CEO </option>
                                                                    <option value="9"> CTO </option>
                                                                    <option value="10"> COO </option>
                                                                    <option value="11"> CMO </option>
                                                                    <option value="12"> CFO </option>
                                                                    <option value="13"> Business Developper </option>
                                                                    <option value="2"> Co-fondateur </option>
                                                                    <option value="1"> Fondateur </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Email </td>
                                                            <td><input type="text" class="form-control" value="<?php echo $startup['email'] ?>" name="emailf1"  id="emailf1"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Tel </td>
                                                            <td><input type="text" class="form-control" value="<?php echo $startup['tel'] ?>" name="telf1" id="telf1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Mobile </td>
                                                            <td><input type="text" class="form-control" name="mobf1" id="mobf1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Linkedin </td>
                                                            <td><input type="text" class="form-control" name="linkedinf1" id="linkedinf1" style="display:inline;width:50%;" />
                                                                <button type="button" style="width:45%;float:right" class="btn btn-success form-control-inline" onclick="veriflinkedinFond1()">V&eacute;rifier</button></div>

                                                            </td>
                                                        </tr>                                                   
                                                        <tr>
                                                            <td> Image </td>
                                                            <td><input type="text" name="imgf1" class="form-control" id="imgf1" /></td>
                                                        </tr>                                                    
                                                    </table>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="fond2" > <i class="fa fa-plus-circle"> </i> Fondateur 2</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond2_bloc">
                                                    <table class="table"  border="0" cellspacing="0" cellpadding="0">

                                                        <tr>
                                                            <td> Civilite </td>
                                                            <td >
                                                                <select name="civf2" class="form-control" style="width: 200px">
                                                                    <option value="0">M</option>
                                                                    <option value="1">Mme</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Pr&eacute;nom </td>
                                                            <td><input type="text" name="prenomf2" id="prenomf2" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Nom </td>
                                                            <td><input type="text" name="nomf2" id="nomf2" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td>
                                                                <select name="fonctionf2" id="fonctionf2" class="form-control" style="width: 200px;">
                                                                    <option value="">--Choisir fonction--</option>
                                                                    <option value="6"> CEO </option>
                                                                    <option value="9"> CTO </option>
                                                                    <option value="10"> COO </option>
                                                                    <option value="11"> CMO </option>
                                                                    <option value="12"> CFO </option>
                                                                    <option value="13"> Business Developper </option>
                                                                    <option value="2"> Co-fondateur </option>
                                                                    <option value="1"> Fondateur </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Email </td>
                                                            <td><input type="text" name="emailf2" id="emailf2" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Tel </td>
                                                            <td><input type="text" value="" name="telf2" id="telf2" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Mobile </td>
                                                            <td><input type="text" name="mobf2" id="mobf2" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Linkedin </td>
                                                            <td><input type="text" name="linkedinf2" id="linkedinf2" class="form-control" style="display:inline;width:50%;" />
                                                                <button type="button" style="width:45%;float:right" class="btn btn-success form-control-inline" onclick="veriflinkedinFond2()">V&eacute;rifier</button></div>
                                                            </td>
                                                        </tr>
                                                       
                                                        <tr>
                                                            <td> Image </td>
                                                            <td><input type="text" name="imgf2" id="imgf2" class="form-control" /></td>
                                                        </tr>
                                                        
                                                    </table>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="fond3" > <i class="fa fa-plus-circle"> </i> Fondateur 3</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond3_bloc">
                                                    <table class="table" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td colspan="2"><strong>Fondateurs 3</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Civilite </td>
                                                            <td>
                                                                <select name="civf3" class="form-control" style="width: 200px;">
                                                                    <option value="0">M</option>
                                                                    <option value="1">Mme</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Pr&eacute;nom </td>
                                                            <td><input type="text" name="prenomf3" id="prenomf3" class="form-control"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Nom </td>
                                                            <td><input type="text" name="nomf3" id="nomf3" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td>
                                                                <select name="fonctionf3" id="fonctionf3" class="form-control" style="width: 200px;">
                                                                    <option value="">--Choisir fonction--</option>
                                                                    <option value="6"> CEO </option>
                                                                    <option value="9"> CTO </option>
                                                                    <option value="10"> COO </option>
                                                                    <option value="11"> CMO </option>
                                                                    <option value="12"> CFO </option>
                                                                    <option value="13"> Business Developper </option>
                                                                    <option value="2"> Co-fondateur </option>
                                                                    <option value="1"> Fondateur </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Email </td>
                                                            <td><input type="text" name="emailf3" id="emailf3" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Tel </td>
                                                            <td><input type="text" value="" name="telf3" id="telf3" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Mobile </td>
                                                            <td><input type="text" name="mobf3" id="mobf3" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Linkedin </td>
                                                            <td><input type="text" name="linkedinf3" id="linkedinf3" class="form-control" style="display:inline;width:50%;" />
                                                                <button type="button" style="width:45%;float:right" class="btn btn-success form-control-inline" onclick="veriflinkedinFond3()">V&eacute;rifier</button></div>
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td> Image </td>
                                                            <td><input type="text" name="imgf3" id="imgf3" class="form-control" /></td>
                                                        </tr>
                                                        
                                                    </table>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="fond4"> </i> Fondateur 4</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond4_bloc">
                                                    <table class="table" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td colspan="2"><strong>Fondateurs 4</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Civilite </td>
                                                            <td>
                                                                <select name="civf4" class="form-control" style="width: 200px;">
                                                                    <option value="0">M</option>
                                                                    <option value="1">Mme</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Pr&eacute;nom </td>
                                                            <td><input type="text" name="prenomf4" id="prenomf4" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Nom </td>
                                                            <td><input type="text" name="nomf4" id="nomf4" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td>
                                                                <select id="fonctionf4" name="fonctionf4" class="form-control" style="width: 200px;">
                                                                    <option value="">--Choisir fonction--</option>
                                                                    <option value="6"> CEO </option>
                                                                    <option value="9"> CTO </option>
                                                                    <option value="10"> COO </option>
                                                                    <option value="11"> CMO </option>
                                                                    <option value="12"> CFO </option>
                                                                    <option value="13"> Business Developper </option>
                                                                    <option value="2"> Co-fondateur </option>
                                                                    <option value="1"> Fondateur </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Email </td>
                                                            <td><input type="text" name="emailf4" id="emailf4" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Tel </td>
                                                            <td><input type="text" value="" name="telf4" id="telf4" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Mobile </td>
                                                            <td><input type="text" name="mobf4" id="mobf4" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Linkedin </td>
                                                            <td><input type="text" name="linkedinf4" id="linkedinf4" class="form-control" style="display:inline;width:50%;" />
                                                                <button type="button" style="width:45%;float:right" class="btn btn-success form-control-inline" onclick="veriflinkedinFond4()">V&eacute;rifier</button></div>
                                                            </td>
                                                        </tr>
                                                       
                                                        <tr>
                                                            <td> Image </td>
                                                            <td><input type="text" name="imgf4" id="imgf4" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong> Experiences Professionnelles</strong></td>
                                                        </tr>
                                                        
                                                    </table>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="fond5"> <i class="fa fa-plus-circle"> </i> Fondateur 5</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond5_bloc">
                                                    <table class="table" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td colspan="2"><strong>Fondateurs 5</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Civilite </td>
                                                            <td>
                                                                <select name="civf5" class="form-control" style="width: 200px;">
                                                                    <option value="0">M</option>
                                                                    <option value="1">Mme</option>
                                                                </select></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Pr&eacute;nom </td>
                                                            <td><input type="text" name="prenomf5" id="prenomf5"  class="form-control"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Nom </td>
                                                            <td><input type="text" name="nomf5" id="nomf5" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td><select name="fonctionf5" id="fonctionf5" class="form-control" style="width: 200px;">
                                                                    <option value="">--Choisir fonction--</option>
                                                                    <option value="6"> CEO </option>
                                                                    <option value="9"> CTO </option>
                                                                    <option value="10"> COO </option>
                                                                    <option value="11"> CMO </option>
                                                                    <option value="12"> CFO </option>
                                                                    <option value="13"> Business Developper </option>
                                                                    <option value="2"> Co-fondateur </option>
                                                                    <option value="1"> Fondateur </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Email </td>
                                                            <td><input type="text" name="emailf5" id="emailf5" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Tel </td>
                                                            <td><input type="text" value="" name="telf5" id="telf5" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Mobile </td>
                                                            <td><input type="text" name="mobf5" id="mobf5"  class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Linkedin </td>
                                                            <td><input type="text" name="linkedinf5" id="linkedinf5" class="form-control" style="display:inline;width:50%;" />
                                                                <button type="button" style="width:45%;float:right" class="btn btn-success form-control-inline" onclick="veriflinkedinFond5()">V&eacute;rifier</button></div>
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td> Image </td>
                                                            <td><input type="text" name="imgf5" id="imgf5" class="form-control" /></td>
                                                        </tr>
                                                        
                                                    </table>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="fond6"> <i class="fa fa-plus-circle"> </i> Fondateur 6</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;" id="fond6_bloc">
                                                    <table class="table" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td colspan="2"><strong>Fondateurs 6</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Civilite </td>
                                                            <td>
                                                                <select name="civf6" class="form-control" style="width: 200px;">
                                                                    <option value="0">M</option>
                                                                    <option value="1">Mme</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Pr&eacute;nom </td>
                                                            <td><input type="text" name="prenomf6" id="prenomf6"  class="form-control"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Nom </td>
                                                            <td><input type="text" name="nomf6" id="nomf6" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Fonction </td>
                                                            <td>
                                                                <select name="fonctionf6" id="fonctionf6" class="form-control" style="width: 200px;">
                                                                    <option value="">--Choisir fonction--</option>
                                                                    <option value="6"> CEO </option>
                                                                    <option value="9"> CTO </option>
                                                                    <option value="10"> COO </option>
                                                                    <option value="11"> CMO </option>
                                                                    <option value="12"> CFO </option>
                                                                    <option value="13"> Business Developper </option>
                                                                    <option value="2"> Co-fondateur </option>
                                                                    <option value="1"> Fondateur </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Email </td>
                                                            <td><input type="text" name="emailf6" id="emailf6" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Tel </td>
                                                            <td><input type="text" value="" name="telf6" id="telf6" class="form-control" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Mobile </td>
                                                            <td><input type="text" name="mobf6" id="mobf6"  class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Linkedin </td>
                                                            <td><input type="text" name="linkedinf6" id="linkedinf6"  class="form-control" style="display:inline;width:50%;" />
                                                                <button type="button" style="width:45%;float:right" class="btn btn-success form-control-inline" onclick="veriflinkedinFond6()">V&eacute;rifier</button></div>
                                                            </td>
                                                        </tr>
                                                      
                                                        <tr>
                                                            <td> Image </td>
                                                            <td><input type="text" name="imgf6" id="imgf6" class="form-control" /></td>
                                                        </tr>
                                                        
                                                    </table>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="lf">Deals</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="col-md-12" id="bloc_lf">
                                                        <div id="bloc_min_lf" class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Deal</div>
                                                            <div class="col-md-4">
                                                                <select name="rachat[]" class="form-control">
                                                                    <option value="" selected="selected"></option>
                                                                    <option value="0">Lev&eacute;e de fonds</option>
                                                                    <option value="1">Rachat</option>
                                                                    <option value="2">IPO</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">Montant</div>
                                                            <div class="col-md-4"><input type="text" value="0"  name="montant_lf[]" class="form-control"></div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Date</div>
                                                            <div class="col-md-4">
                                                                <input type="text" autocomplete="off" class="datepicker form-control" onclick="refrech_datepicker()" name="date_lf[]">
                                                            </div>
                                                            <div class="col-md-2">Investisseurs</div>
                                                            <div class="col-md-4">

                                                                <textarea class="example list_invest_tag" name="de[]" style="width: 320px"></textarea>
                                                                <input name="de1[]" class="form-control" type="text" placeholder="Autres" />



                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a class="delete_lf_ligne">
                                                                <i style="color:#EC407A" class="fa fa-minus-circle fa-2x"></i>
                                                            </a>
                                                        </div>

                                                    </div>

                                                    <div class="row">
                                                        <div id="add_lf">
                                                            <div class="col-lg-3">
                                                                <button onclick="add_lf();" type="button" class="btn btn-primary">
                                                                    <i class="icon-plus-circle2 position-left"></i> Ajouter</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class=" bg-indigo-400 big_titre" id="deal_flow">Recherche de fonds</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Montant recherché (k€)</div>
                                                        <div class="col-md-4"><input type="text" name="montant_rech"  class="form-control" /></div>
                                                        <div class="col-md-2">Date de clotûre</div>
                                                        <div class="col-md-4"><input type="text" name="date_rech"  autocomplete="off" class="datepicker form-control" onclick="refrech_datepicker()" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Type d'investisseurs recherchés :</div>
                                                        <div class="col-md-10">
                                                            <div class="col-md-3 col-sm-4">
                                                                <input type="checkbox" value="Business Angels" name="rech[]" style="width: 20px;">
                                                                Business Angels
                                                            </div>
                                                            <div class="col-md-3 col-sm-4">
                                                                <input type="checkbox" value="Fonds d'investissements" name="rech[]" style="width: 20px;">
                                                                Fonds d'investissements
                                                            </div>
                                                            <div class="col-md-3 col-sm-4">
                                                                <input type="checkbox" value="Crowdfunding" name="rech[]" style="width: 20px;">
                                                                Crowdfunding
                                                            </div>
                                                            <div class="col-md-3 col-sm-4">
                                                                <input type="checkbox" value="Fonds publics" name="rech[]" style="width: 20px;">
                                                                Fonds publics</div>
                                                        </div>
                                                        <div class="col-md-2">Objectif :</div>
                                                        <div class="col-md-10">

                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="Recrutement IT" name="objectif[]" style="width: 20px;">
                                                                Recrutement IT
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="Recrutement BizDev" name="objectif[]" style="width: 20px;">
                                                                Recrutement BizDev
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="R&amp;D" name="objectif[]" style="width: 20px;">
                                                                R&amp;D
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="Acquisition matériels" name="objectif[]" style="width: 20px;">
                                                                Acquisition matériels
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="International" name="objectif[]" style="width: 20px;">
                                                                International
                                                            </div>
                                                            <div class="col-md-3 col-sm-4" style="margin-top: 10px; margin-bottom: 10px;">
                                                                <input type="checkbox" value="Autres" name="objectif[]" style="width: 20px;">
                                                                Autres
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=" bg-indigo-400 big_titre">Défaillance</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">


                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Depot de bilan</div>
                                                        <div class="col-md-4">
                                                            <input type="radio" value="1" name="bilan" style="width:20px; margin-left:20px;">
                                                            Oui
                                                            <input type="radio" checked="checked" value="0" name="bilan" style="width:20px; margin-left:20px;">
                                                            Non
                                                        </div>
                                                        <div class="col-md-2">Date de dep&ocirc;t de bilan</div>
                                                        <div class="col-md-4"><input type="text" name="datebilan"  autocomplete="off" class="datepicker form-control" onclick="refrech_datepicker()" /></div>
                                                    </div>
                                                    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                        <div class="col-md-2">Statut</div>
                                                        <div class="col-md-4">
                                                            <select  class="form-control">
                                                                <option></option>
                                                                <option value="Entreprise en défaillance">Entreprise en défaillance</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">Motif</div>
                                                        <div class="col-md-4">
                                                            <select  class="form-control" name="motif">
                                                                <option></option>
                                                                <option value="Annulation de la procédure de redressement juidiciaire">Annulation de la procédure de redressement juidiciaire</option>
                                                                <option value="Arrêt du plan de sauvegarde">Arrêt du plan de sauvegarde</option>
                                                                <option accesskey="Changement de liquidateur judiciaire">Changement de liquidateur judiciaire</option>
                                                                <option value="Clôture pour insuffisance d'actif">Clôture pour insuffisance d'actif</option>
                                                                <option value="Conciliation">Conciliation</option>
                                                                <option value="Conversion en liquidation judiciaire">Conversion en liquidation judiciaire</option>
                                                                <option value="Dépôt de l'état des créances">Dépôt de l'état des créances</option>
                                                                <option value="Dépôt des créances nées après jugement">Dépôt des créances nées après jugement</option>
                                                                <option value="Liquidation judiciaire">Liquidation judiciaire</option>
                                                                <option value="Liquidation judiciaire simplifiée">Liquidation judiciaire simplifiée</option>
                                                                <option value="Plan de cession totale">Plan de cession totale</option>
                                                                <option value="Procédure de sauvegarde">Procédure de sauvegarde</option>
                                                                <option value="Redressement Judiciaire">Redressement Judiciaire</option>
                                                                <option value="Règlement amiable">Règlement amiable</option>
                                                                <option value="Rétractation de redressement judiciaire sur tierce opposition">Rétractation de redressement judiciaire sur tierce opposition</option>
                                                                <option value="Sauvegarde Jugement">Sauvegarde Jugement</option>

                                                            </select>
                                                        </div>

                                                    </div>


                                                </div>


                                                <div class=" bg-indigo-400 big_titre" id="actualite">News</div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <div class="col-md-12" id="bloc_actualite">
                                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Lien</div>
                                                            <div class="col-md-4"><input type="text" name="lien[]" id="news_link" class="form-control"></div>
                                                            <div class="col-md-4"><button type="button" id="" class="btn btn-info" onclick="getNews()">Get NEWS</button></div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-2">Titre</div>
                                                            <div class="col-md-4"><input type="text" id="titre_news" name="titre[]" class="form-control"></div>
                                                            <div class="col-md-2">Description</div>
                                                            <div class="col-md-4"><input type="text" id="description_news" name="description[]" class="form-control"></div>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                                                            <div class="col-md-1">Date</div>
                                                            <div class="col-md-3"><input type="text" id="date_news" autocomplete="off" class="datepicker form-control" onclick="refrech_datepicker()"  name="dateactu[]"></div>

                                                            <div class="col-md-1">Source</div>
                                                            <div class="col-md-3"><input type="text" id="source_news" name="source[]" class="form-control"></div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a class="delete_actualite_ligne">
                                                                <i style="color:#EC407A" class="fa fa-minus-circle fa-2x"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div id="add_actualite">
                                                            <div class="col-lg-3">
                                                                <button onclick="add_actualite();" type="button" class="btn btn-primary">
                                                                    <i class="icon-plus-circle2 position-left"></i> Ajouter</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="" style="margin-left: 60px; margin-right: 60px; margin-top: 10px;">
                                                    <table width="1134" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="center" colspan="2"><button type="submit" class="btn btn-success">Ajouter</button></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </form>

                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- /main content -->





        </div>

        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
                <span class="navbar-text">
                    &copy; <?php echo date('Y'); ?> <a href="#">myFrenchStaryp Pro</a> par <a href="" target="_blank">myFrenchStartup</a>
                </span>
            </div>
        </div>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcCIjgD7FA1zVC6EQwS5V6I-6xuUuwMiQ&sensor=false&amp;libraries=places"></script>
        <script src="assets/js/jquery.geocomplete.js"></script>
        <script src="assets/js/bootstrap-datepicker.js"></script>
        <script src="script/function.js"></script>
        <script src="script/site.js"></script>
        <script src="script/siret.js"></script>
        <script src="script/verif.js"></script>
        <script src="script/google_linkedin_company.js"></script>
        <script src="script/verifphontom.js"></script>
        <script src="script/qualif_linkedin_company.js"></script>
        <script src="script/verif_seul.js"></script>
        <script>


                                                                    function remplire_tags() {
                                                                        $('.list_invest_tag')
                                                                                .textext({
                                                                                    plugins: 'tags autocomplete'
                                                                                })
                                                                                .bind('getSuggestions', function (e, data)
                                                                                {
                                                                                    var list = [
<?php
$sql_inves = mysqli_query($link, "select id,new_name from temp_lf group by new_name order by new_name");
while ($data_invest = mysqli_fetch_array($sql_inves)) {
    ?>
                                                                                            '<?php echo addslashes($data_invest['new_name']); ?>',
    <?php
}
?>
                                                                                    ],
                                                                                            textext = $(e.target).textext()[0],
                                                                                            query = (data ? data.query : '') || '';

                                                                                    $(this).trigger(
                                                                                            'setSuggestions',
                                                                                            {result: textext.itemManager().filter(list, query)}
                                                                                    );
                                                                                })
                                                                                ;
                                                                    }
                                                                    $(document).ready(function () {

                                                                        remplire_tags();
                                                                    }
                                                                    );





        </script>
        <script>

                                                                                                                    $(document).ready(function () {
                                                                                                                        
                                                                                                                        $(function () {
                                                                                                                            window.prettyPrint && prettyPrint();
                                                                                                                            $('#dp1').datepicker({
                                                                                                                                format: 'mm-dd-yyyy'
                                                                                                                            });
                                                                                                                        });


                                                                                                                        $(function () {
                                                                                                                            $("#datepicker").datepicker({dateFormat: 'dd/mm/yy'});
                                                                                                                            $("#datepicker1").datepicker({dateFormat: 'dd/mm/yy'});
                                                                                                                            $("#datepicker2").datepicker({dateFormat: 'dd/mm/yy'});
                                                                                                                            $("#datepicker3").datepicker({dateFormat: 'dd/mm/yy'});
                                                                                                                            $("#datepicker4").datepicker({dateFormat: 'dd/mm/yy'});
                                                                                                                        });

                                                                                                                        $(function () {
                                                                                                                            $("#geocomplete").geocomplete({
                                                                                                                                map: ".map_canvas",
                                                                                                                                details: "form",
                                                                                                                                types: ["geocode", "establishment"],
                                                                                                                            });

                                                                                                                            $("#find").click(function () {
                                                                                                                                $("#geocomplete").trigger("geocode");
                                                                                                                            });
                                                                                                                        });


                                                                                                                        $().ready(function () {

                                                                                                                            $("#concurrent").autocomplete('concurrent.php', {
                                                                                                                                multiple: true,
                                                                                                                                mustMatch: false,
                                                                                                                                autoFill: true

                                                                                                                            });





                                                                                                                            $("#concurrent").result(function (event, data, formatted) {

                                                                                                                                var hidden = $(this).parent().next().find(">:input");

                                                                                                                                hidden.val((hidden.val() ? hidden.val() + ";" : hidden.val()) + data[1]);

                                                                                                                            });


                                                                                                                        });


                                                                                                                    });
                                                                                                    </script>
                                                                                                    <script>

                                                                                                        $().ready(function () {
                                                                                                            window.prettyPrint && prettyPrint();

                                                                                                            refrech_datepicker();

                                                                                                        });


                                                                                                        function refrech_datepicker() {
                                                                                                            $(".datepicker").each(function () {
                                                                                                                $(this).datepicker({
                                                                                                                    format: 'mm-dd-yyyy'
                                                                                                                });


                                                                                                            });

                                                                                                        }
                                                                                                    </script>
    </body>
</html>
