<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');

if (!isset($_SESSION['data_login'])) {
    header("location:" . URL . "/connexion");
    exit();
}
$monUrl = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

$id_dash = degenerate_id($_GET['id']);
$dashboard = mysqli_fetch_array(mysqli_query($link, "select * from user_save_list where id=" . $id_dash));
$ligne_france = mysqli_fetch_array(mysqli_query($link, "select * from indice_france where id=1"));

$year = date("Y");
$previousyear = $year - 1;

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}

$date1 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d'), date('y')));
$date2 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d') - 30, date('y')));
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?php echo ($dashboard['nom']); ?> Détails du segment - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">

        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/maps.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/franceLow.min.js"></script>
        <script>
            am4core.addLicense("CH303325752");
            am4core.addLicense("MP307346529");
        </script>
        <script>
            const   themeColorBlue = am4core.color('#00bff0'),
                    themeColorFill = am4core.color('#E1F2FA'),
                    themeColorGrey = am4core.color('#dadada'),
                    themeColorLightGrey = am4core.color('#F5F5F5'),
                    themeColorDarkgrey = am4core.color("#33333A"),
                    themeColorLine = am4core.color("#87D6E3"),
                    themeColorLineRed = am4core.color("#FF7C7C"),
                    themeColorLineGreen = am4core.color("#21D59B"),
                    themeColorColumns = am4core.color("#E1F2FA"),
                    labelColor = am4core.color('#798a9a');
        </script>
        <style>
            a{
                color:#323239;
            }
        </style>
        <script>
            (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({'gtm.start':
                    new Date().getTime(), event: 'gtm.js'});
            var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>
    </head>

    <?php
    /*
      sur les pages différentes de la HP, on affiche une classe "page" en plus sur le body pour spécifier que le header a un BG blanc.
     */
    ?>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">

                    <section class="module">
                        <div class="module__title">Mon segment : <?php echo ($dashboard['nom']); ?></div>
                        <div class="ctg ctg--1_3">
                            <aside class="bloc text-center">
                                <div class="bloc__title text-center">Segment <?php echo ($dashboard['nom']); ?></div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Total startups</div>
                                    <div class="datasbloc__val"><?php
                                        echo number_format($dashboard['nb'], 0, ".", " ");
                                        ?>
                                        <div class="datasbloc__val small">
                                            <?php echo str_replace(',0', '', number_format(($dashboard['nb_france']), 1, ",", " ")) ?>%
                                            <?php
                                            if ($dashboard['taux_creation'] > 0) {
                                                ?>
                                                <span class="ico-raise"></span>
                                                <?php
                                            }
                                            ?>
                                            <?php
                                            if ($dashboard['taux_creation'] < 0) {
                                                ?>
                                                <span class="ico-decrease"></span>
                                                <?php
                                            }
                                            ?>
                                            <?php
                                            if ($dashboard['taux_creation'] == 0) {
                                                ?>
                                                <span class="ico-stable"></span>
                                                <?php
                                            }
                                            ?>
                                            des startups France
                                        </div>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Emplois créés</div>
                                    <div class="datasbloc__val">
                                        <?php
                                        echo number_format($dashboard['emploi'], 0, ".", " ");
                                        ?>
                                        <?php
                                        if ($dashboard['taux_emploi'] > 0) {
                                            ?>
                                            <span class="ico-raise"></span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if ($dashboard['taux_emploi'] < 0) {
                                            ?>
                                            <span class="ico-decrease"></span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if ($dashboard['taux_emploi'] == 0) {
                                            ?>
                                            <span class="ico-stable"></span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Total fonds levés</div>
                                    <div class="datasbloc__val"><?php echo str_replace(',0', '', number_format(($dashboard['total_invest'] / 1000), 1, ",", " ")) ?> M€</div>
                                    <div class="datasbloc__val small">
                                        <?php echo str_replace(',0', '', number_format(($dashboard['invest_france']), 1, ",", " ")) ?>%
                                        <?php
                                        if ($dashboard['taux_leve'] > 0) {
                                            ?>
                                            <span class="ico-raise"></span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if ($dashboard['taux_leve'] < 0) {
                                            ?>
                                            <span class="ico-decrease"></span>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if ($dashboard['taux_leve'] == 0) {
                                            ?>
                                            <span class="ico-stable"></span>
                                            <?php
                                        }
                                        ?>
                                        financements France
                                    </div>
                                </div>
                                <!--
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Total investisseurs</div>
                                    <div class="datasbloc__val"><?php echo str_replace(',0', '', number_format(($dashboard['nb_investisseurs']), 0, ",", " ")) ?></div>
                                </div>
                                -->
                            </aside>

                            <div class="bloc">
                                <div class="ctg ctg--2_2 ctg--fullheight">
                                    <div class="chart chart--radar">
                                        <div id="chart-radar"></div>
                                    </div>
                                    <script>
                                        am4core.ready(function () {


                                        /* Create chart instance */
                                        var chart = am4core.create("chart-radar", am4charts.RadarChart);
                                        /* Add data */
                                        chart.data = [{
                                        "date": "A",
                                                "value": <?php echo $dashboard['maturite'] ?>,
                                                "value2": <?php echo $ligne_france['maturite']; ?>
                                        }, {
                                        "date": "B",
                                                "value": <?php echo $dashboard['transparence'] ?>,
                                                "value2": <?php echo $ligne_france['transparence']; ?>
                                        }, {
                                        "date": "C",
                                                "value": <?php echo $dashboard['anciennete'] ?>,
                                                "value2": <?php echo $ligne_france['anciennete']; ?>
                                        }, {
                                        "date": "D",
                                                "value": <?php echo $dashboard['traction_likedin'] ?>,
                                                "value2": <?php echo $ligne_france['traction_likedin']; ?>
                                        }];
                                        chart.paddingTop = 0;
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;
                                        /* Create axes */
                                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.dataFields.category = "date";
                                        categoryAxis.renderer.ticks.template.strokeWidth = 2;
                                        categoryAxis.renderer.labels.template.fontSize = 11;
                                        categoryAxis.renderer.labels.template.fill = labelColor;
                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
                                        valueAxis.renderer.axisFills.template.fillOpacity = 0.05;
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.events.on("ready", function (ev) {
                                        ev.target.min = 0;
                                        ev.target.max = 10;
                                        })
                                                valueAxis.renderer.gridType = "polygons"


                                                /* Create and configure series */
                                                        function createSeries(field, name, themeColor) {
                                                        var series = chart.series.push(new am4charts.RadarSeries());
                                                        series.dataFields.valueY = field;
                                                        series.dataFields.categoryX = "date";
                                                        series.strokeWidth = 2;
                                                        series.fill = themeColor;
                                                        series.fillOpacity = 0.3;
                                                        series.stroke = themeColor;
                                                        series.name = name;
                                                        // Show bullets ?
                                                        //let circleBullet = series.bullets.push(new am4charts.CircleBullet());
                                                        //circleBullet.circle.stroke = am4core.color('#fff');
                                                        //circleBullet.circle.strokeWidth = 2;
                                                        }


                                                createSeries("value", "Mon segment", themeColorBlue);
                                                createSeries("value2", "France", themeColorGrey);
                                                chart.legend = new am4charts.Legend();
                                                chart.legend.position = "top";
                                                chart.legend.useDefaultMarker = true;
                                                chart.legend.fontSize = "11";
                                                chart.legend.color = themeColorGrey;
                                                chart.legend.labels.template.fill = labelColor;
                                                chart.legend.labels.template.textDecoration = "none";
                                                chart.legend.valueLabels.template.textDecoration = "none";
                                                let as = chart.legend.labels.template.states.getKey("active");
                                                as.properties.textDecoration = "line-through";
                                                as.properties.fill = themeColorDarkgrey;
                                                let marker = chart.legend.markers.template.children.getIndex(0);
                                                marker.cornerRadius(12, 12, 12, 12);
                                                marker.width = 20;
                                                marker.height = 20;
                                                });
                                    </script>
                                    <div class="datagrid">
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">A :</span> Score maturité
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $dashboard['maturite']); ?>

                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['maturite'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">B :</span> Score transparence
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $dashboard['transparence']); ?>

                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['transparence'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">C :</span> Ancienneté
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $dashboard['anciennete']); ?>  

                                            </div>
                                            <div class="datagrid__subtitle">Moy. France <?php echo str_replace(",0", "", number_format($ligne_france['anciennete'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">D :</span> Traction Linkedin
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $dashboard['traction_likedin']); ?>

                                            </div>
                                            <div class="datagrid__subtitle"><?php echo str_replace(",0", "", number_format($ligne_france['traction_likedin'], 1, ",", "")); ?></div>
                                        </div>

                                    </div>

                                    <div class="bloc-actions">
                                        <div class="bloc-actions__ico">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Financements <?php echo ($dashboard['nom']); ?></div>

                        <div class="ctg ctg--3_3">
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financements Seed</div>
                                        <div class="datasbloc__val">Moy. <?php echo str_replace(",0", "", number_format(($dashboard['moy_1']) / 1000, 1, ",", " ")) ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($dashboard['nb_1']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($dashboard['min_1'] / 1000, 1, ",", " ")) ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($dashboard['max_1'] / 1000, 1, ",", " ")) ?> M€ </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financements Croissance</div>
                                        <div class="datasbloc__val">Moy. <?php echo str_replace(",0", "", number_format(($dashboard['moy_2']) / 1000, 1, ",", " ")) ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($dashboard['nb_2']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($dashboard['min_2'] / 1000, 1, ",", " ")) ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($dashboard['max_2'] / 1000, 1, ",", " ")) ?> M€</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financements Développement</div>
                                        <div class="datasbloc__val nowrap">Moy. <?php echo str_replace(",0", "", number_format($dashboard['moy_3'] / 1000, 1, ",", " ")) ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($dashboard['nb_3']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($dashboard['min_3'] / 1000, 1, ",", " ")) ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($dashboard['max_3'] / 1000, 1, ",", " ")) ?> M€</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Dernières levées de fonds</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="dateCell">Date</th>
                                                <th>Startup</th>
                                                <th class="moneyCell text-center">Montant levé</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_last_lf = explode(',', $dashboard['last_lf']);
                                            $nb = count($sql_last_lf);
                                            $j = 0;
                                            for ($i = 0; $i < 5; $i++) {
                                                if ($j < 5) {
                                                    if ($sql_last_lf[$i] != '') {
                                                        $sql_lf = mysqli_query($link, "select * from lf where rachat=0 and id=" . $sql_last_lf[$i]);
                                                        $verif = mysqli_num_rows($sql_lf);
                                                        if ($verif > 0) {
                                                            $lf = mysqli_fetch_array($sql_lf);
                                                            $nom_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom, date_add from startup where id=" . $lf['id_startup']));
                                                            ?>
                                                            <tr>
                                                                <td class="dateCell">
                                                                    <?php echo change_date_fr_chaine_related($lf['date_ajout']); ?>
                                                                </td>
                                                                <td><a href="<?php echo URL . '/fr/startup-france/' . generate_id($nom_startup['id']) . "/" . urlWriting(strtolower($nom_startup["nom"])) ?>" target="_blank"><?php echo $nom_startup['nom'] ?></a></td>
                                                                <td class="moneyCell text-center"><?php echo str_replace(",0", "", number_format($lf['montant'] / 1000, 1, ",", "")) ?> M€</td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Dernières augmentations de capital</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="dateCell">Date</th>
                                                <th>Startup</th>
                                                <th class="moneyCell text-center">Nouveau capital</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_last_lf1 = explode(',', $dashboard['last_capital']);
                                           
                                            for ($i = 0; $i < 5; $i++) {
                                                if ($sql_last_lf1[$i] != '') {


                                                    
                                                    $dernier_capital = mysqli_fetch_array(mysqli_query($link, "select * from startup_capital where id=" . $sql_last_lf1[$i] . " order by dt desc limit 1"));
                                                    $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom from startup where id=" . $dernier_capital['id_startup']));
                                                    //if (!empty($ma_startup)) {
                                                        ?>
                                                        <tr>
                                                            <td class="dateCell">
                                                                <?php echo change_date_fr_chaine_related($dernier_capital['dt']); ?>
                                                            </td>
                                                            <td class="nowrap"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo $ma_startup['nom'] ?></a></td>
                                                            <td class="moneyCell text-center"><?php echo str_replace(",0", "", number_format($dernier_capital['capital'] / 1000, 1, ",", " ")); ?> K€</td>
                                                        </tr>
                                                    <?php //} ?>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </section>
                    <div class="module__title">Dernières évolutions <?php echo ($dashboard['nom']); ?></div>
                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Nouvelles startups ajoutées</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="dateCell">Création</th>
                                                <th class="nowrap">Startup</th>
                                                <th>Secteur</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_last_startup = explode(',', $dashboard['last_startup']);
                                            for ($i = 0; $i < 5; $i++) {
                                                if ($sql_last_startup[$i] != '') {
                                                    $nom_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom, date_add from startup where id=" . $sql_last_startup[$i]));
                                                    $sector = mysqli_fetch_array(mysqli_query($link, "select icon,nom_secteur from startup inner join activite on activite.id_startup=startup.id inner join secteur on secteur.id=activite.secteur where startup.id=" . $sql_last_startup[$i]));
                                                    ?>
                                                    <tr>
                                                        <td class="dateCell"><?php echo change_date_fr_chaine_related($nom_startup['date_add']) ?></td>
                                                        <td class="nowrap"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($nom_startup['id']) . "/" . urlWriting(strtolower($nom_startup["nom"])) ?>" target="_blank"><?php echo ($nom_startup['nom']); ?></a></td>
                                                        <td><?php echo $sector['nom_secteur']; ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Derniers bodacc</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="dateCell text-center">Date</th>
                                                <th class="nowrap">Startup</th>
                                                <th>Mouvement</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql_last_bod = explode(',', $dashboard['last_bodac']);
                                            $nb = count($sql_last_bod);
                                            $j = 0;
                                            for ($i = 0; $i < 5; $i++) {
                                                if ($j < 5) {
                                                    if ($sql_last_bod[$i] != '') {
                                                        $sql_lf = mysqli_query($link, "select * from startup_depot where id=" . $sql_last_bod[$i] . " limit 1");
                                                        $verif = mysqli_num_rows($sql_lf);
                                                        $startup_legal = mysqli_fetch_array($sql_lf);
                                                        if ($verif > 0) {
                                                            $nom_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom, date_add from startup where id=" . $startup_legal['id_startup']));
                                                            ?>
                                                            <tr>
                                                                <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($startup_legal['dt']); ?></td>
                                                                <td class="nowrap"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($nom_startup['id']) . "/" . urlWriting(strtolower($nom_startup['nom'])) ?>" target="_blank"><?php echo ($nom_startup['nom']); ?></a></td>
                                                                <td><?php echo ($startup_legal['depot']); ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </section>


                    <section class="module">
                        <div class="module__title">Effectif et ancienneté <?php echo ($dashboard['nom']); ?></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Répartition par effectifs</div>
                                <div class="chart chart--columns">
                                    <div id="chart-financement"></div>
                                </div>
                                <script>
                                    am4core.ready(function () {

// Themes begin
// Themes end

// Create chart instance
                                    var chart = am4core.create("chart-financement", am4charts.XYChart);
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
<?php
$sql_last_startup_region = explode(',', $dashboard['list']);
$nb_region = count($sql_last_startup_region);
$ch = "";
for ($i = 0; $i < $nb_region; $i++) {
    if ($sql_last_startup_region[$i] != "") {
        $ch .= " or id=" . $sql_last_startup_region[$i] . " ";
    }
}
$effectif_rang_1 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>=1 and rang_effectif<=2  and (id=0 " . $ch . " )"));
$effectif_rang_2 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>=3 and rang_effectif<=5  and (id=0 " . $ch . " ) "));
$effectif_rang_3 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>=6 and rang_effectif<=10  and (id=0 " . $ch . " )"));
$effectif_rang_4 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>=11 and rang_effectif<=20  and (id=0 " . $ch . " )"));
$effectif_rang_5 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>=21 and rang_effectif<=50  and (id=0 " . $ch . " )"));
$effectif_rang_6 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>=51 and rang_effectif<=100  and (id=0 " . $ch . " )"));
$effectif_rang_7 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>100  and (id=0 " . $ch . " )"));
?>
// Add data
                                    chart.data = [{
                                    "year": "1-2",
                                            "visits": <?php echo $effectif_rang_1; ?>
                                    }, {
                                    "year": "3-5",
                                            "visits": <?php echo $effectif_rang_2; ?>
                                    }, {
                                    "year": "6-10",
                                            "visits": <?php echo $effectif_rang_3; ?>
                                    }, {
                                    "year": "11-20",
                                            "visits": <?php echo $effectif_rang_4; ?>
                                    }, {
                                    "year": "21-50",
                                            "visits": <?php echo $effectif_rang_5; ?>
                                    }, {
                                    "year": "51-100",
                                            "visits": <?php echo $effectif_rang_6; ?>
                                    }, {
                                    "year": ">100",
                                            "visits": <?php echo $effectif_rang_7; ?>
                                    }];
// Create axes

                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "year";
                                    categoryAxis.renderer.grid.template.location = 0;
                                    categoryAxis.renderer.minGridDistance = 10;
                                    categoryAxis.renderer.fontSize = "10px";
                                    categoryAxis.renderer.labels.template.fill = labelColor;
                                    //categoryAxis.dataItems.template.text = "{realName}";
                                    categoryAxis.renderer.grid.disabled = true;
                                    categoryAxis.title.text = "Tranches d'effectifs";
                                    categoryAxis.title.rotation = 0;
                                    categoryAxis.title.align = "center";
                                    categoryAxis.title.valign = "bottom";
                                    categoryAxis.title.dy = - 5;
                                    categoryAxis.title.fontSize = 11;
                                    categoryAxis.title.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    // Title left
                                    valueAxis.title.text = "Nombre de startups";
                                    valueAxis.title.rotation = - 90;
                                    valueAxis.title.align = "left";
                                    valueAxis.title.valign = "top";
                                    valueAxis.title.dy = 0;
                                    valueAxis.title.fontSize = 11;
                                    valueAxis.title.fill = labelColor;
// Create series
                                    var series = chart.series.push(new am4charts.ColumnSeries());
                                    series.dataFields.valueY = "visits";
                                    series.dataFields.categoryX = "year";
                                    series.fill = themeColorColumns;
                                    series.stroke = 0;
                                    var columnTemplate = series.columns.template;
                                    columnTemplate.strokeWidth = 0;
                                    columnTemplate.strokeOpacity = 0;
                                    valueAxis.renderer.grid.template.strokeWidth = 1;
                                    //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                    categoryAxis.renderer.grid.template.strokeWidth = 0;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Répartition par ancienneté</div>
                                <div class="chart chart--columns">
                                    <div id="chart-maturite"></div>
                                </div>
                                <script>
                                    am4core.ready(function () {


// Themes begin
// Themes end

// Create chart instance
                                    var chart = am4core.create("chart-maturite", am4charts.XYChart);
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
<?php
$sql_last_startup_region = explode(',', $dashboard['list']);
$nb_region = count($sql_last_startup_region);
$ch = "";
for ($i = 0; $i < $nb_region; $i++) {
    if ($sql_last_startup_region[$i] != "") {
        $ch .= " or id=" . $sql_last_startup_region[$i] . " ";
    }
}
$annee = date("Y");
$dernier_annee_0_1 = date('Y') - 1;
$dernier_annee_2 = date('Y') - 2;
$dernier_annee_3 = date('Y') - 3;
$dernier_annee_4 = date('Y') - 4;
$dernier_annee_5 = date('Y') - 5;
$dernier_annee_6 = date('Y') - 6;
$dernier_annee_10 = date('Y') - 10;

$creation_1 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and creation<=" . $annee . " and creation>=" . $dernier_annee_0_1 . "  and (id=0 " . $ch . " )"));
$creation_2 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and creation<=" . $dernier_annee_2 . " and creation>=" . $dernier_annee_3 . "  and (id=0 " . $ch . " )"));
$creation_3 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and creation<=" . $dernier_annee_4 . " and creation>=" . $dernier_annee_5 . "  and (id=0 " . $ch . " )"));
$creation_4 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and creation<=" . $dernier_annee_6 . " and creation>=" . $dernier_annee_10 . "  and (id=0 " . $ch . " )"));
$creation_5 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and creation<" . $dernier_annee_10 . "  and (id=0 " . $ch . " )"));
?>                                  chart.paddingRight = 0;
// Add data
                                    chart.data = [{
                                    "year": "0 à 1 an",
                                            "visits": <?php echo $creation_1; ?>
                                    }, {
                                    "year": "2 à 3 ans",
                                            "visits": <?php echo $creation_2; ?>
                                    }, {
                                    "year": "4 à 5 ans",
                                            "visits": <?php echo $creation_3; ?>
                                    }, {
                                    "year": "6 à 10 ans",
                                            "visits": <?php echo $creation_4; ?>
                                    }, {
                                    "year": "sup à 10 ans",
                                            "visits": <?php echo $creation_5; ?>
                                    }];
// Create axes

                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "year";
                                    categoryAxis.renderer.grid.template.location = 0;
                                    categoryAxis.renderer.minGridDistance = 10;
                                    categoryAxis.renderer.fontSize = "10px";
                                    categoryAxis.renderer.labels.template.fill = labelColor;
                                    //categoryAxis.dataItems.template.text = "{realName}";
                                    categoryAxis.renderer.grid.disabled = true;
                                    categoryAxis.title.text = "Tranches d'âges";
                                    categoryAxis.title.rotation = 0;
                                    categoryAxis.title.align = "center";
                                    categoryAxis.title.valign = "bottom";
                                    categoryAxis.title.dy = - 5;
                                    categoryAxis.title.fontSize = 11;
                                    categoryAxis.title.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    // Title left
                                    valueAxis.title.text = "Nombre de startups";
                                    valueAxis.title.rotation = - 90;
                                    valueAxis.title.align = "left";
                                    valueAxis.title.valign = "top";
                                    valueAxis.title.dy = 0;
                                    valueAxis.title.fontSize = 11;
                                    valueAxis.title.fill = labelColor;
// Create series
                                    var series = chart.series.push(new am4charts.ColumnSeries());
                                    series.dataFields.valueY = "visits";
                                    series.dataFields.categoryX = "year";
                                    series.fill = themeColorColumns;
                                    series.stroke = 0;
                                    var columnTemplate = series.columns.template;
                                    columnTemplate.strokeWidth = 0;
                                    columnTemplate.strokeOpacity = 0;
                                    valueAxis.renderer.grid.template.strokeWidth = 1;
                                    //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                    categoryAxis.renderer.grid.template.strokeWidth = 0;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="module__title">Géographie <?php echo ($dashboard['nom']); ?></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Géographie du secteur</div>

                                <div class="chart chart--map">
                                    <div id="chart-map"></div>
                                </div>

                                <script>

                                    // Create map instance
                                    var chart = am4core.create("chart-map", am4maps.MapChart);
                                    chart.paddingTop = 0;
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    // Set map definition
                                    chart.geodata = am4geodata_franceLow;
                                    // Set projection
                                    chart.projection = new am4maps.projections.Miller();
                                    // Create map polygon series
                                    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
                                    //Set min/max fill color for each area
                                    polygonSeries.heatRules.push({
                                    property: "fill",
                                            target: polygonSeries.mapPolygons.template,
                                            //min: chart.colors.getIndex(1).brighten(1),
                                            //max: chart.colors.getIndex(1).brighten(-0.3),
                                            logarithmic: true
                                    });
                                    // Make map load polygon data (state shapes and names) from GeoJSON
                                    polygonSeries.useGeodata = true;
                                    // Set heatmap values for each state
                                    // Bleu 1 le + clair : #E2F4FC
                                    // Bleu 2 : #CEEAF7
                                    // Bleu 3 : #BEE7F8
                                    // Bleu 4 le + foncé : #ACDDF2
<?php

function valueFillColor($val) {
    if ($val < 40) {
        return "#E2F4FC";
    } elseif ($val > 40 && $val <= 60) {
        return "#CEEAF7";
    } elseif ($val > 60 && $val <= 110) {
        return "#BEE7F8";
    } elseif ($val > 110) {
        return "#ACDDF2";
    }
}
?>

                                    polygonSeries.data = [
<?php
$liste_region = explode(",", $dashboard['list_reg']);
$liste_nb_region = explode(",", $dashboard['list_nbr_reg']);
$count_reg = count($liste_region);
for ($i = 0; $i < $count_reg; $i++) {
    $ma_reg = $liste_region[$i];
    $code_reg = mysqli_fetch_array(mysqli_query($link, "select * from region_new where region_new='" . trim($ma_reg) . "'"));
    ?>
                                        {
                                        "id": "<?php echo $code_reg['code'] ?>",
                                                "value": <?php echo $liste_nb_region[$i] ?>,
                                                "fill": am4core.color("<?php echo valueFillColor($liste_nb_region[$i]); ?>")
                                                //"fill": am4core.color("#ff0000"),
                                        },
    <?php
}
?>

                                    ];
                                    // Configure series tooltip
                                    var polygonTemplate = polygonSeries.mapPolygons.template;
                                    polygonTemplate.tooltipText = "[#fff font-size: 11px]{name}: {value}[/]";
                                    polygonTemplate.nonScalingStroke = true;
                                    polygonTemplate.strokeWidth = 1.5;
                                    polygonTemplate.propertyFields.fill = "fill";
                                    polygonTemplate.stroke = themeColorGrey;
                                    polygonTemplate.properties.fill = am4core.color("#E0E0E0");
                                    // Create hover state and set alternative fill color
                                    var hs = polygonTemplate.states.create("hover");
                                    hs.properties.fill = themeColorBlue;
                                    hs.properties.color = am4core.color("#ff0000");
                                    chart.seriesContainer.draggable = false;
                                    chart.seriesContainer.resizable = false;
                                    chart.maxZoomLevel = 1;
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Startups par département</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="iconCell text-center">Évol.</th>
                                                <th>Département</th>
                                                <th class="text-center">Startups</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $list_dept = explode(",", $dashboard['list_dep']);
                                            $nb_dept = explode(",", $dashboard['list_nbr_dep']);
                                            $nbe=count($list_dept);
                                            if($nbe>10)
                                                $nbe=10;
                                            for ($o = 0; $o < $nbe; $o++) {
                                                if ($list_dept[$o] != "NA") {
                                                    ?>
                                                    <tr>

                                                        <td class="iconCell text-center">
                                                            <span class="ico-raise"></span>
                                                        </td>
                                                        <td>
                                                            <?php echo $list_dept[$o]; ?>
                                                        </td>
                                                        <td class="text-center"> 
                                                            <?php echo $nb_dept[$o]; ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>


                    <section class="module">
                        <div class="module__title">Notifications <?php echo ($dashboard['nom']); ?></div>
                        <?php $sql_notif = mysqli_query($link, "select * from notifications where liste=" . $dashboard['id'] . " order by dt desc limit 10")or die(mysqli_error($link)); ?>
                        <?php $totalNotifs = mysqli_num_rows($sql_notif); ?>
                        <?php if ($totalNotifs == 0) { ?>
                            <div class="alert alert--infos">Pas de notification envoyée à ce jour.</div>
                        <?php } else { ?>
                            <div class="bloc">
                                <div class="table-search-wrapper">
                                    <table class="table tablebloc table-search">
                                        <thead>
                                            <tr>
                                                <th class="table-search__startup">Startup</th>
                                                <th class="table-search__date text-center">Date</th>
                                                <th class="table-search__tags">Notification</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            while ($data_notifs = mysqli_fetch_array($sql_notif)) {
                                                $list_sup = $data_notifs['list_sup'];
                                                $list_lf = $data_notifs['list_lf'];
                                                $list_verif = $data_notifs['list_verif'];
                                                ?>
                                                <?php
                                                if ($list_sup != '') {
                                                    $tabs_sup = explode(",", $list_sup);
                                                    $nb_list_sup = count($tabs_sup);
                                                    for ($i = 0; $i < 5; $i++) {
                                                        if ($tabs_sup[$i] != "") {
                                                            $startups = mysqli_fetch_array(mysqli_query($link, "Select * from startup Where id =" . $tabs_sup[$i]));
                                                            ?>

                                                            <tr>
                                                                <th class="table-search__startup">
                                                                    <div class="startupInfos">
                                                                        <div class="startupInfos__image">
                                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $startups['logo']) ?>" alt="<?php echo "Startup " . $startups["nom"]; ?>"></a>
                                                                        </div>
                                                                        <div class="startupInfos__desc">
                                                                            <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a></div>
                                                                            <div class="companyLabel"><?php echo stripslashes($startups['short_fr']); ?></div>
                                                                        </div>
                                                                    </div>
                                                                </th>
                                                                <td class="table-search__date text-center">
                                                                    <?php echo change_date_fr_chaine_related($data_notifs['dt']); ?>
                                                                </td>
                                                                <td class="table-search__tags">Nouvelle startup ajoutée à votre liste <?php echo $data_notifs['nom']; ?>.</td>
                                                            </tr>

                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                                <?php
                                                if ($list_lf != '') {
                                                    $tabs_sup1 = explode(",", $list_lf);
                                                    $nb_list_sup = count($tabs_sup1);
                                                    for ($i = 0; $i < 5; $i++) {
                                                        if ($tabs_sup1[$i] != "") {
                                                            $startups = mysqli_fetch_array(mysqli_query($link, "Select * from startup Where id =" . $tabs_sup1[$i]));
                                                            ?>
                                                            <tr>
                                                                <th class="table-search__startup">
                                                                    <div class="startupInfos">
                                                                        <div class="startupInfos__image">
                                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $startups['logo']) ?>" alt="<?php echo "Startup " . $startups["nom"]; ?>"></a>
                                                                        </div>
                                                                        <div class="startupInfos__desc">
                                                                            <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a></div>
                                                                            <div class="companyLabel"><?php echo stripslashes($startups['short_fr']); ?></div>
                                                                        </div>
                                                                    </div>
                                                                </th>
                                                                <td class="table-search__date text-center">
                                                                    <?php echo change_date_fr_chaine_related($data_notifs['dt']); ?>
                                                                </td>

                                                                <td class="table-search__tags">Nouvelle levée de fonds ajoutée à votre liste <?php echo $data_notifs['nom']; ?>.</td>

                                                            </tr>

                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                                <?php
                                                if ($list_verif != '') {
                                                    $tabs_sup2 = explode(",", $list_verif);
                                                    $nb_list_sup = count($tabs_sup1);
                                                    for ($i = 0; $i < 5; $i++) {
                                                        if ($tabs_sup2[$i] != "") {
                                                            $startups = mysqli_fetch_array(mysqli_query($link, "Select * from startup Where id =" . $tabs_sup2[$i]));
                                                            ?>
                                                            <tr>
                                                                <th class="table-search__startup">
                                                                    <div class="startupInfos">
                                                                        <div class="startupInfos__image">
                                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $startups['logo']) ?>" alt="<?php echo "Startup " . $startups["nom"]; ?>"></a>
                                                                        </div>
                                                                        <div class="startupInfos__desc">
                                                                            <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a></div>
                                                                            <div class="companyLabel"><?php echo stripslashes($startups['short_fr']); ?></div>
                                                                        </div>
                                                                    </div>
                                                                </th>
                                                                <td class="table-search__date text-center">
                                                                    <?php echo change_date_fr_chaine_related($data_notifs['dt']); ?>
                                                                </td>

                                                                <td class="table-search__tags">Nouveau mouvement Bodacc ajouté à votre liste <?php echo $data_notifs['nom']; ?>.</td>

                                                            </tr>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        <?php } ?>
                    </section>


                    <section class="module">
                        <div class="module__title">Startups <?php echo ($dashboard['nom']); ?></div>
                        <div class="bloc">
                            <div class="table-responsive">
                                <table id="mine2" class="table table-search">
                                    <thead>
                                        <tr>
                                            <th class="table-search__startup">Startup</th>
                                            <th class="table-search__marche text-center">Marché</th>
                                            <th class="table-search__maturite text-center">Maturité</th>
                                            <th class="table-search__totalfondsleves text-center">Total<br />fonds levés</th>
                                            <th class="table-search__region text-center">Région</th>
                                            <th class="table-search__traction text-center">Traction</th>
                                            <th class="table-search__creation text-center">Création</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql101 = mysqli_query($link, $dashboard['req']);
                                        $l = 0;
                                        while ($sups = mysqli_fetch_array($sql101)) {
                                            if ($l < 10) {
                                                $startups = mysqli_fetch_array(mysqli_query($link, "Select * From   startup Where id =" . $sups['id']));
                                                $secteurs = mysqli_fetch_array(mysqli_query($link, "Select secteur.nom_secteur,secteur.id,secteur.nom_secteur_en From   secteur Inner Join  activite    On activite.secteur = secteur.id Where  activite.id_startup =" . $sups['id']));
                                                $score = mysqli_fetch_array(mysqli_query($link, "Select  * From   startup_score where  id_startup=" . $startups['id']));

                                                $tab_site = explode("/", str_replace(array("https://", "http://", "www."), array("", "", ""), $startups['url']));
                                                $tot_lf = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $startups['id']));

                                                $ssecteurs = mysqli_fetch_array(mysqli_query($link, "Select  * From  activite  Where  activite.id_startup =" . $favoris['id']));
                                                $ligne_secteur_activite = mysqli_fetch_array(mysqli_query($link, "select * from secteur_activite where id_secteur=" . $secteurs['id'] . " and id_sous_secteur=" . $idsoussecteur . " and id_activite=" . $idactivite . " and id_sous_activite=" . $idsousactivite));

                                                if ($ssecteurs['sous_activite'] == "")
                                                    $idsousactivite = 0;
                                                else
                                                    $idsousactivite = $ssecteurs['sous_activite'];

                                                if ($ssecteurs['sous_secteur'] == "")
                                                    $idsoussecteur = 0;
                                                else
                                                    $idsoussecteur = $ssecteurs['sous_secteur'];

                                                if ($ssecteurs['activite'] == "")
                                                    $idactivite = 0;
                                                else
                                                    $idactivite = $ssecteurs['activite'];
                                                ?>
                                                <tr>
                                                    <th class="table-search__startup">
                                                        <div class="startupInfos">
                                                            <div class="startupInfos__image">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $startups['logo']) ?>" alt="<?php echo "Startup " . $startups["nom"]; ?>"></a>
                                                            </div>
                                                            <div class="startupInfos__desc">
                                                                <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($startups['id']) . "/" . urlWriting(strtolower($startups["nom"])) ?>" target="_blank"><?php echo stripslashes($startups['nom']); ?></a></div>
                                                                <div class="companyLabel"><?php echo stripslashes($startups['short_fr']); ?></div>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <td class="table-search__secteur text-center">
                                                        <?php echo ($secteurs['nom_secteur']); ?>
                                                    </td>
                                                    <td class="table-search__maturite text-center">
                                                        <?php echo $score['maturite']; ?> 
                                                        <?php if ($score['maturite'] == $ligne_secteur_activite['moy_maturite']) { ?>
                                                            <span class="ico-stable"></span> 
                                                        <?php } ?>
                                                        <?php if ($score['maturite'] > $ligne_secteur_activite['moy_maturite']) { ?>
                                                            <span class="ico-raise"></span> 
                                                        <?php } ?>
                                                        <?php if ($score['maturite'] < $ligne_secteur_activite['moy_maturite']) { ?>
                                                            <span class="ico-decrease"></span> 
                                                        <?php } ?>
                                                    </td>
                                                    <td class="table-search__totalfondsleves text-center"><?php echo str_replace(",0", "", number_format(($tot_lf['somme'] / 1000), 1, ",", " ")); ?> M€</td>
                                                    <td class="table-search__region text-center"><?php echo ($startups['region_new']); ?></td>
                                                    <td class="table-search__traction text-center"><?php echo ($score['traction_likedin']); ?></td>
                                                    <td class="table-search__creation text-center"><?php echo change_date_fr_chaine_related($startups['date_complete']); ?></td>
                                                </tr>

                                                <?php
                                                $l++;
                                            }
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                            <div id="paginateTable2"></div>
                        </div>
                    </section>

                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>jquery.dataTables.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>datatables.bootstrap.min.js?<?= time(); ?>"></script>

        <script>
                                    var table = jQuery('#mine').DataTable({
                                    "searching": false,
                                            "nextPrev": false,
                                            "bLengthChange": false,
                                            "bInfo": false,
                                            "bPaginate": true,
                                            "autoWidth": false,
                                            //"fixedColumns": false,
                                            initComplete: (settings, json) => {
                                    $('#paginateTable').empty();
                                    $('#mine_paginate').appendTo('#paginateTable');
                                    },
                                            "language": {
                                            "paginate": {
                                            "previous": "Précédent",
                                                    "next": "Suivant"
                                            }
                                            }
                                    });
                                    var table2 = jQuery('#mine2').DataTable({
                                    "searching": false,
                                            "nextPrev": false,
                                            "bLengthChange": false,
                                            "bInfo": false,
                                            "bPaginate": true,
                                            "autoWidth": false,
                                            //"fixedColumns": false,
                                            initComplete: (settings, json) => {
                                    $('#paginateTable2').empty();
                                    $('#mine2_paginate').appendTo('#paginateTable2');
                                    },
                                            "language": {
                                            "paginate": {
                                            "previous": "Précédent",
                                                    "next": "Suivant"
                                            }
                                            }
                                    });
        </script>
        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
                                    (function (i, s, o, g, r, a, m) {
                                    i['GoogleAnalyticsObject'] = r;
                                    i[r] = i[r] || function () {
                                    (i[r].q = i[r].q || []).push(arguments)
                                    }, i[r].l = 1 * new Date();
                                    a = s.createElement(o),
                                            m = s.getElementsByTagName(o)[0];
                                    a.async = 1;
                                    a.src = g;
                                    m.parentNode.insertBefore(a, m)
                                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                                    ga('create', 'UA-36251023-1', 'auto');
                                    ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
            var $ = jQuery;
            var valeur = document.getElementById("search-box").value;
            $.ajax({
            type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                    $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#search-box").css("background", "#FFF");
                    }
            });
            }

            function selectCountry(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>