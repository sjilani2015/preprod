<?php
include("include/db.php");
include("functions/functions.php");
include ('config.php');
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Deal Flow : ajouter votre demande de levées de fonds - <?= SITENAME; ?></title>
        <meta name="description" content="Deal Flow <?= SITENAME; ?>">
    </head>
    <body class="preload page">
        <?php
        if (isset($_POST["societe"])) {

            // Startup
            $societe = addslashes((($_POST["societe"])));
            $juridique = addslashes((($_POST["juridique"])));
            $siret = addslashes(utf8_decode(($_POST["siret"])));

            $siteweb = addslashes((($_POST["website"])));
            $nom_marche = addslashes((($_POST["nom_marche"])));
            $secteur = addslashes((($_POST["secteur"])));
            $sous_secteur = addslashes((($_POST["sous_secteur"])));
            $concours = addslashes((($_POST["concours"])));
            $subventions = addslashes((($_POST["subventions"])));
            $nom_subvention = addslashes((($_POST["nom_subvention"])));
            $nom_concours = addslashes((($_POST["nom_concours"])));
            $nom_grand_compte = addslashes((($_POST["nom_grand_compte"])));

            // Votre levée de fonds
            $montant = addslashes(utf8_decode(($_POST["montant_rech"])));
            $creation = $_POST["date_rech"];

            $objectifs = $_POST["objectif"];
            $objectif = "";
            for ($i = 0; $i < count($objectifs); $i++) {
                $objectif .= " " . $objectifs[$i] . ",";
            }

            $typerech = $_POST["rech"];
            $rechs = "";
            for ($i = 0; $i < count($typerech); $i++) {
                $rechs .= " " . $typerech[$i] . ",";
            }



            //Marché
            $business1 = $_POST["business"];
            $business = "";
            for ($i = 0; $i < count($business1); $i++) {
                $business .= " " . $business1[$i] . ",";
            }
            $innovation1 = $_POST["innovation"];
            $innovation = "";
            for ($i = 0; $i < count($innovation1); $i++) {
                $innovation .= " " . $innovation1[$i] . ",";
            }
            $protection1 = $_POST["protection"];
            $protection = "";
            for ($i = 0; $i < count($protection1); $i++) {
                $protection .= " " . $protection1[$i] . ",";
            }
            $cible1 = $_POST["cible"];
            $cible = "";
            for ($i = 0; $i < count($cible1); $i++) {
                $cible .= " " . $cible1[$i] . ",";
            }

            $taillem = addslashes($_POST["taillem"]);
            $sourcemarche = addslashes($_POST["sourcemarche"]);
            $volume = addslashes($_POST["volume"]);

            //Informations générales

            $effectif = addslashes(utf8_decode(($_POST["effectif"])));
            
            $creation_dt = $_POST["date_creation"];

            $payant = addslashes($_POST['payant']);
            $grand_compte = addslashes($_POST['partenaire']);
            $autre_objectif = addslashes($_POST['autre_objectif']);
            $autre_bm = addslashes($_POST['autre_bm']);

//Personne à contacter
            $prenom = addslashes((($_POST['prenom'])));
            $nom = addslashes((($_POST['nom'])));
            $fonction = addslashes((($_POST['fonction'])));
            $tel = addslashes((($_POST['tel'])));
            $email = addslashes((($_POST['email_contact'])));

            mysqli_query($link, "INSERT INTO `data_enquete` ( `email`, `societe`,`juridique`,`siret`, `montant`, `dt`, `type_invest`, `objectif`, `business`, `innovation`, `protection`, `taillem`, `volume`, `source`, `date_add`, `cible`, `effectif`, `date_creation`, `payant`, `grand_compte`, `nom`, `prenom`, `fonction`, `tel`, `autre_objectif`, `autre_bm`, `secteur`, `sous_secteur`, `nom_marche`, `siteweb`, `concours`, `subventions`,`nom_subvention`,`nom_concours`,`nom_grand_compte`) VALUES('" . $email . "', '" . $societe . "','" . $juridique . "','" . $siret . "', '" . $montant . "', '" . $creation . "', '" . addslashes($rechs) . "', '" . $objectif . "', '" . $business . "', '" . $innovation . "', '" . $protection . "', '" . $taillem . "', '" . $volume . "', '" . $sourcemarche . "', '" . date('Y-m-d H:i:s') . "', '" . $cible . "', '" . $effectif . "', '" . $creation_dt . "', '" . $payant . "', '" . $grand_compte . "', '" . $nom . "', '" . $prenom . "', '" . $fonction . "', '" . $tel . "', '" . $autre_objectif . "','" . $autre_bm . "','" . $secteur . "','" . $sous_secteur . "','" . $nom_marche . "','" . $siteweb . "','" . $concours . "','" . $subventions . "','" . $nom_subvention . "','" . $nom_concours . "','" . $nom_grand_compte . "');");
            $id_req = mysqli_insert_id($link);
            $prenoms_list = $_POST["prenom_co"];
            $noms_list = $_POST["nom_co"];
            $fonctions_list = $_POST["fonction_co"];
            foreach ($prenoms_list as $key => $investitem) {
                $prenom_co = addslashes($prenoms_list[$key]);
                $nom_co = addslashes($noms_list[$key]);
                $fonction_co = addslashes($fonctions_list[$key]);
                if ($prenom_co != '') {
                    mysqli_query($link, "INSERT INTO `data_enquete_founder` ( `id_enquete`, `nom`, `prenom`, `fonction`) VALUES ( " . $id_req . ", '" . $nom_co . "', '" . $prenom_co . "', '" . $fonction_co . "');");
                }
            }
            echo '<script>window.location="validation-deal-flow"</script>';
        } // Fin isset
        ?>
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php include ('layout/header-simple.php'); ?>
            <div class="page-content" id="page-content">
                <div class="container">
                    <form method="post" action="">
                        <div class="dealflow">
                            <div class="section-title section-title--fat">
                                Communiquez sur votre<br>recherche de fonds
                                <span>Ajoutez gratuitement votre demande de levée de fonds</span>
                            </div>
                            <div class="bloc bloc--dealflow">
                                <div class="bloc__title">Informations générales</div>
                                <div class="bloc__formDealflow">
                                    <div class="formgrid formgrid--2col">
                                        <div class="formgrid__item">
                                            <div class="form-group">
                                                <label>Nom commercial de votre startup</label>
                                                <input type="text" value="" name="societe" required="" placeholder="" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="formgrid__item">
                                            <div class="form-group">
                                                <label>Nom juridique</label>
                                                <input type="text" value="" name="juridique"  placeholder="" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="formgrid__item">
                                            <div class="form-group">
                                                <label>SIRET / SIREN</label>
                                                <input type="text" value="" name="siret" placeholder="" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="formgrid__item">
                                            <div class="form-group">
                                                <label>Site web</label>
                                                <input type="text" value="" name="website" required="" placeholder="" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="formgrid__item">
                                            <div class="form-group">
                                                <label>Nombre d'employés</label>
                                                <input type="text" value="" placeholder="" required="" name="effectif" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="formgrid__item">
                                            <div class="form-group">
                                                <label>Date de création</label>
                                                <input type="date" value="" placeholder="" name="date_creation"  class="form-control" />
                                            </div>
                                        </div>
                                        <div class="formgrid__item">
                                            <div class="form-group">
                                                <label>Secteur d'activité</label>
                                                <div class="custom-select">
                                                    <select name="secteur" id="secteur_select" onchange="reload_sous_secteur_bloc()" required class="form-control">
                                                        <option value="">- Choisir</option>
                                                        <?php
                                                        $req_select_secteur = "select * from secteur order by nom_secteur";
                                                        $res_select_secteur = mysqli_query($link, $req_select_secteur);
                                                        while ($data_secteur = mysqli_fetch_array($res_select_secteur)) {
                                                            ?>
                                                            <option <?php
                                                            if ($data_secteur['id'] == $data['secteur']) {
                                                                echo ' selected="selected" ';
                                                            }
                                                            ?> value="<?php echo $data_secteur['id'] ?>"> <?php echo ($data_secteur['nom_secteur']) ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="formgrid__item">
                                            <div class="form-group">
                                                <label>Sous Secteur</label>
                                                <div class="custom-select" id="sous_secteur_bloc">
                                                    <select name="sous_secteur"  class="form-control">
                                                        <option value=""></option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="bloc bloc--dealflow">
                                <div class="bloc__title">Votre levée de fonds</div>
                                <div class="bloc__formDealflow">
                                    <div class="form-group">
                                        <div class="formgrid formgrid--2col">
                                            <div class="formgrid__item">
                                                <label>Montant recherché en k€</label>
                                                <input type="text" required value="" name="montant_rech" placeholder="" class="form-control" />
                                            </div>
                                            <div class="formgrid__item">
                                                <label>Date de clôture de votre levée de fonds</label>
                                                <input type="date" required name="date_rech" placeholder="" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Objectif de ce tour de table</label>
                                        <div class="dealflow__tableform">
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Marketing & Communication</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="1" name="objectif[]" value="Marketing & communication" type="checkbox" value="" style="display: none" />
                                                    <label class="cbx" for="1">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Recrutement Développeurs</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="2" type="checkbox" name="objectif[]" value="Recrutement Développeurs" style="display: none" />
                                                    <label class="cbx" for="2">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Recrutement Commerciaux</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="0" type="checkbox" value="Recrutement Commerciaux" name="objectif[]" style="display: none" />
                                                    <label class="cbx" for="0">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">R&D</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="3" type="checkbox" value="R&D" name="objectif[]" style="display: none" />
                                                    <label class="cbx" for="3">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Acquisition Matériels</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="4" type="checkbox" value="Acquisition matériels" name="objectif[]" style="display: none" />
                                                    <label class="cbx" for="4">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Internationalisation</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="5" type="checkbox" value="Internationalisation" name="objectif[]"  style="display: none" />
                                                    <label class="cbx" for="5">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Autres</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="objectif_autre"  value="Autres" name="objectif[]" id="objectif_autre" onclick="ajout_autre()" type="checkbox" value="" style="display: none" />
                                                    <label class="cbx" for="objectif_autre">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group hide" id="autre_objectif">
                                        <label><strong>Autres :</strong> merci de préciser</label>
                                        <input type="text" value="" placeholder="" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Types d'investisseurs recherchés</label>
                                        <div class="dealflow__tableform">
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Business Angels</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="ba1" type="checkbox" value="Business Angels" name="rech[]" style="display: none" />
                                                    <label class="cbx" for="ba1">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">VC / Corporate VC</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="vc-corporate" type="checkbox" value="VC / Corporate VC" name="rech[]" style="display: none" />
                                                    <label class="cbx" for="vc-corporate">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Crowdfunding</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="crowdfunding" type="checkbox" value="Crowdfunding" name="rech[]" style="display: none" />
                                                    <label class="cbx" for="crowdfunding">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Fonds Publics</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="fondspublics" type="checkbox" value="Fonds publics" name="rech[]" style="display: none" />
                                                    <label class="cbx" for="fondspublics">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="bloc bloc--dealflow">
                                <div class="bloc__title">Votre marché</div>
                                <div class="bloc__formDealflow " id="dealflowMarche">
                                    <div class="form-group">
                                        <label>Business Modèle</label>
                                        <div class="dealflow__tableform">
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Gratuit</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="bm_gratuit" type="checkbox" name="business[]" value="Gratuit" style="display: none" />
                                                    <label class="cbx" for="bm_gratuit">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Premium / Abonnement</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="bm_abo" type="checkbox" name="business[]" value="Premium/Abonnement" style="display: none" />
                                                    <label class="cbx" for="bm_abo">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Publicité</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="bm_pub" type="checkbox" name="business[]" value="Publicité" style="display: none" />
                                                    <label class="cbx" for="bm_pub">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Vente</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="bm_vente" type="checkbox" name="business[]" value="Ventes" style="display: none" />
                                                    <label class="cbx" for="bm_vente">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Commissions</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="bm_commissions" type="checkbox" name="business[]" value="Commissions" style="display: none" />
                                                    <label class="cbx" for="bm_commissions">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Mise en relation</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="bm_mer" type="checkbox" name="business[]" value="Mise en relation" style="display: none" />
                                                    <label class="cbx" for="bm_mer">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Revenus&nbsp;de Licences</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="bm_licences" type="checkbox" name="business[]" value="Revenus de licences" style="display: none" />
                                                    <label class="cbx" for="bm_licences">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Autres</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="bm_autre" onclick="ajout_bm();" type="checkbox"  value="Autres" id="bm_autre" style="display: none" />
                                                    <label class="cbx" for="bm_autre">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group hide" id="autre_bm">
                                        <label><strong>Autres :</strong> merci de préciser</label>
                                        <input type="text"  name="autre_bm" placeholder="" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Type d'innovation</label>
                                        <div class="dealflow__tableform">
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Usage</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="innov_usage" type="checkbox" name="innovation[]" value="Usage" style="display: none" />
                                                    <label class="cbx" for="innov_usage">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Technologie</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="innov_techno" type="checkbox" name="innovation[]" value="Technologique" style="display: none" />
                                                    <label class="cbx" for="innov_techno">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Modèle économique</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="innov_modeleco" type="checkbox" name="innovation[]" value="Modèle économique" style="display: none" />
                                                    <label class="cbx" for="innov_modeleco">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Positionnement</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="innov_positionnement" type="checkbox" name="innovation[]" value="Positionnement" style="display: none" />
                                                    <label class="cbx" for="innov_positionnement">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Protection industrielle</label>
                                        <div class="dealflow__tableform">
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Brevet</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="protection_brevet" type="checkbox" name="protection[]" value="Brevet" style="display: none" />
                                                    <label class="cbx" for="protection_brevet">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Dessins & Modèles</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="protection_bessins" type="checkbox" name="protection[]" value="Dessins & Modèles" style="display: none" />
                                                    <label class="cbx" for="protection_bessins">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">Soleau</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="protection_soleau" type="checkbox" name="protection[]" value="Soleau" style="display: none" />
                                                    <label class="cbx" for="protection_soleau">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Votre cible</label>
                                        <div class="dealflow__tableform">
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">BtoB</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="cible_BtoB" type="checkbox" name="cible[]" value="BtoB" style="display: none" />
                                                    <label class="cbx" for="cible_BtoB">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">BtoC</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="cible_BtoC" type="checkbox" name="cible[]" value="BtoC" style="display: none" />
                                                    <label class="cbx" for="cible_BtoC">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="dealflow__tableform__item">
                                                <div class="dealflow__tableform__header">CtoC</div>
                                                <div class="dealflow__tableform__child">
                                                    <input class="inp-cbx" id="cible_CtoC" type="checkbox" name="cible[]" value="CtoC" style="display: none" />
                                                    <label class="cbx" for="cible_CtoC">
                                                        <span><svg width="12px" height="10px" viewbox="0 0 12 10"><polyline points="1.5 6 4.5 9 10.5 1"></polyline></svg></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Nom de votre marché</label>
                                        <input type="text" name="nom_marche" placeholder="Fintech, Edtech, Ads, Biotech,..." class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <div class="formgrid formgrid--2col">

                                            <div class="formgrid__item">
                                                <label>Taille du marché</label>
                                                <input type="text" name="taillem" placeholder="" class="form-control" />
                                            </div>

                                            <div class="formgrid__item" style="align-self: flex-end; margin-bottom: 10px;">
                                                <div class="custom-radio-wrapper">
                                                    <label class="custom-radio">
                                                        Volume
                                                        <input type="radio" checked="checked" value="Volume" name="volume">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="custom-radio">
                                                        Montant
                                                        <input type="radio" checked="checked" value="Montant" name="volume">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Source</label>
                                        <input type="text" name="sourcemarche" placeholder="Merci d'indiquer la source de ces données" class="form-control" />
                                    </div>
                                </div>

                               
                            </div>

                            <div class="bloc bloc--dealflow">
                                <div class="bloc__title">Maturité</div>
                                <div class="bloc__formDealflow " id="dealflowMaturite">
                                    <div class="form-group">
                                        <label>Avez-vous un ou plusieurs clients payants?</label>
                                        <div class="custom-radio-wrapper">
                                            <label class="custom-radio">
                                                Oui
                                                <input type="radio" checked="checked" value="Oui" name="payant">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="custom-radio">
                                                Non
                                                <input type="radio" checked="checked" value="Non" name="payant">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Avez-vous un ou plusieurs partenaires Grand Compte?</label>
                                        <div class="custom-radio-wrapper">
                                            <label class="custom-radio">
                                                Oui
                                                <input type="radio" checked="checked" onclick="affiche_grand_compte()" value="Oui" name="partenaire">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="custom-radio">
                                                Non
                                                <input type="radio" checked="checked" onclick="cacher_grand_compte()" value="Non" name="partenaire">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group hide" id="bloc_nom_grand_compte">
                                        <label>Merci de préciser</label>
                                        <input type="text" value="" placeholder="" name="nom_grand_compte" id="nom_grand_compte" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Avez-vous remporté des concours et/ou des distinctions?</label>
                                        <div class="custom-radio-wrapper">
                                            <label class="custom-radio">
                                                Oui
                                                <input type="radio" checked="checked" onclick="affiche_concours()" value="Oui" name="concours">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="custom-radio">
                                                Non
                                                <input type="radio" checked="checked" onclick="cacher_concours()" value="Non" name="concours">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group hide" id="bloc_nom_concours">
                                        <label>Merci de préciser</label>
                                        <input type="text" value="" placeholder="" name="nom_concours" id="nom_concours" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Avez vous bénéficié de subventions?</label>
                                        <div class="custom-radio-wrapper">
                                            <label class="custom-radio">
                                                Oui
                                                <input type="radio" checked="checked" value="Oui" onclick="affiche_subvension()" name="subventions">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="custom-radio">
                                                Non
                                                <input type="radio" checked="checked" value="Non" onclick="cacher_subvension()" name="subventions">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group hide" id="bloc_nom_subvention">
                                        <label>Merci de préciser</label>
                                        <input type="text" value="" placeholder="" name="nom_subvention" id="nom_subvention" class="form-control" />
                                    </div>
                                </div>

                               
                            </div>

                            <div class="bloc bloc--dealflow">
                                <div class="bloc__title">Personne à contacter</div>
                                <div class="bloc__formDealflow">
                                    <div class="form-group">
                                        <div class="formgrid formgrid--2col">
                                            <div class="formgrid__item">
                                                <label>Prénom</label>
                                                <input type="text" name="prenom" placeholder="" required="" class="form-control" />
                                            </div>
                                            <div class="formgrid__item">
                                                <label>Nom</label>
                                                <input type="text" name="nom" placeholder="" required="" class="form-control" />
                                            </div>
                                            <div class="formgrid__item">
                                                <label>Fonction</label>
                                                <input type="text" name="fonction" placeholder="" required="" class="form-control" />
                                            </div>
                                            <div class="formgrid__item">
                                                <label>Téléphone</label>
                                                <input type="phone" name="tel" placeholder="" required="" class="form-control" />
                                            </div>
                                            <div class="formgrid__item">
                                                <label>Email</label>
                                                <input type="email" name="email_contact" required="" placeholder="" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label id="label-cofondateurs">Autres co-fondateurs</label>
                                        <div id="bloc_founder">
                                            <?php $co = 1; ?>
                                            <div class="form-group">
                                                <div class="formgrid formgrid--4col">
                                                    <div class="formgrid__item">
                                                        <label>Prénom</label>
                                                        <input type="text" name="prenom_co[]" placeholder="" class="form-control" />
                                                    </div>
                                                    <div class="formgrid__item">
                                                        <label>Nom</label>
                                                        <input type="text" name="nom_co[]" placeholder="" class="form-control" />
                                                    </div>
                                                    <div class="formgrid__item">
                                                        <label>Fonction</label>
                                                        <input type="text" name="fonction_co[]" placeholder="" class="form-control" />
                                                    </div>
                                                    <div class="formgrid__item" style="align-self: flex-end;">
                                                        <span class="btn btn-block btn-primary addco" onclick="add_co_founder(<?php echo $co + 1; ?>);"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="bloc bloc--dealflow">
                                <button type="submit" class="btn btn-primary btn-valid-dealflow">Valider</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>



        <?php include ('layout/footer.php'); ?>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/animated.min.js"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
                                                            (function (i, s, o, g, r, a, m) {
                                                                i['GoogleAnalyticsObject'] = r;
                                                                i[r] = i[r] || function () {
                                                                    (i[r].q = i[r].q || []).push(arguments)
                                                                }, i[r].l = 1 * new Date();
                                                                a = s.createElement(o),
                                                                        m = s.getElementsByTagName(o)[0];
                                                                a.async = 1;
                                                                a.src = g;
                                                                m.parentNode.insertBefore(a, m)
                                                            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                                                            ga('create', 'UA-36251023-1', 'auto');
                                                            ga('send', 'pageview');
        </script>
        <script>


            function reload_sous_secteur_bloc()
            {
                id_secteur = $('#secteur_select').val();


                $.ajax({
                    method: "POST",
                    url: "ajax_load_sous_secteur.php",
                    data: {id_secteur: id_secteur}
                })
                        .done(function (data) {
                            $('#sous_secteur_bloc').html(data);
                        });




            }

            function affiche_subvension() {
                document.getElementById("bloc_nom_subvention").style.display = "block";
            }
            function cacher_subvension() {
                document.getElementById("bloc_nom_subvention").value = "";
                document.getElementById("bloc_nom_subvention").style.display = "none";
            }
            function affiche_concours() {
                document.getElementById("bloc_nom_concours").style.display = "block";
            }
            function cacher_concours() {
                document.getElementById("bloc_nom_concours").value = "";
                document.getElementById("bloc_nom_concours").style.display = "none";
            }
            function affiche_grand_compte() {
                document.getElementById("bloc_nom_grand_compte").style.display = "block";
            }
            function cacher_grand_compte() {
                document.getElementById("bloc_nom_grand_compte").value = "";
                document.getElementById("bloc_nom_grand_compte").style.display = "none";
            }
            // Dealflow specifics
            function ajout_autre() {

                if (document.getElementById("objectif_autre").checked == true) {
                    document.getElementById("autre_objectif").style.display = "block";
                } else {
                    document.getElementById("autre_objectif").style.display = "none";
                }
            }

            function add_co_founder(id) {
                $("#bloc_founder").append('' +
                        '<div class="form-group capital" id="capital_' + id + '"> <div class="formgrid formgrid--4col"> <div class="formgrid__item"> <label>Prénom</label> <input type="text" name="prenom_co[]"placeholder="" class="form-control" /> </div> <div class="formgrid__item"> <label>Nom</label> <input type="text" name="nom_co[]" placeholder="" class="form-control" /> </div> <div class="formgrid__item"> <label>Fonction</label> <input type="text" name="fonction_co[]" placeholder="" class="form-control" /> </div> <div class="formgrid__item" style="align-self: flex-end;"> <span class="btn btn-block btn-pinky removeco" onclick="del_co_founfer(' + id + ');"></span> </div> </div> </div>' +
                        '');

                //$("#addnbco").html('<div style="margin-top: 10px; margin-bottom: 10px; clear: both" class="col-md-12"><a class="" onclick="add_co_founder(' + (id + 1) + ');"><i class="fa fa-plus-circle fa-2x"></i></a><div class="clr"></div></div>');
            }

            function del_co_founfer(id)
            {
                $("#capital_" + id).remove();
            }

            function show_maturite(el) {
                var parent = el.parentElement;
                parent.style.display = "none"
                $("#dealflowMaturite").slideDown("slow");
            }

            function show_marche(el) {

                var parent = el.parentElement;
                parent.style.display = "none"
                $("#dealflowMarche").slideDown("slow");
            }


            function ajout_bm() {

                if (document.getElementById("bm_autre").checked == true) {
                    document.getElementById("autre_bm").style.display = "block";
                } else {
                    document.getElementById("autre_bm").style.display = "none";
                }
            }


            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }

        </script>

        <!-- Button trigger modal -->

    </button>


</body>
</html>