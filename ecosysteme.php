<?php
include("include/db.php");
include("functions/functions.php");
include('config.php');
$monUrl = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$_SESSION['utm_source'] = $monUrl;

/*
 * Test de quota
 */


$ecosysteme = mysqli_fetch_array(mysqli_query($link, "select * from french_tech where id=1"));
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title>Ecosystème startups France - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">

        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/maps.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/franceLow.min.js"></script>
        <script>
            am4core.addLicense("CH303325752");
            am4core.addLicense("MP307346529");
        </script>
        <script>
            const   themeColorBlue = am4core.color('#00bff0'),
                    themeColorFill = am4core.color('#E1F2FA'),
                    themeColorGrey = am4core.color('#dadada'),
                    themeColorLightGrey = am4core.color('#F5F5F5'),
                    themeColorDarkgrey = am4core.color("#33333A"),
                    themeColorLine = am4core.color("#87D6E3"),
                    themeColorLineRed = am4core.color("#FF7C7C"),
                    themeColorLineGreen = am4core.color("#21D59B"),
                    themeColorColumns = am4core.color("#E1F2FA"),
                    labelColor = am4core.color('#798a9a');
        </script>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <div style="text-align: center;padding-top: 30px;"><a href="https://www.myfrenchstartup.com/etude-dealflow" target="_blank"><img src="martech.png" alt="" /></a></div>

            <?php
            if (!isset($_SESSION['data_login'])) {
                include ('layout/header-simple.php');
            } else {
                include ('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">
                    <section class="module">
                        <div class="module__title">Ecosystème startups France</div>
                        <div class="ctg ctg--3_3">
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Startups France</div>
                                        <div class="datasbloc__val"><?php echo str_replace(',0', '', number_format($ecosysteme['nbr_startups'], 0, '.', ' ')); ?></div>
                                        <div class="datasbloc__subtitle">
                                            <?php
                                            if ($ecosysteme['evo_startups'] == -1) {
                                                ?>
                                                <span class="ico-decrease"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_startups'] == 1) {
                                                ?>
                                                <span class="ico-raise"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_startups'] == 0) {
                                                ?>
                                                <span class="ico-stable"></span>
                                            <?php } ?>

                                            <?php echo str_replace(',0', '', number_format($ecosysteme['startups_12'], 1, ",", "")); ?>% sur 12 mois</div>
                                    </div>
                                    <?php
                                    // Calcul pour le donut avec Pourcentage de commentaires positifs
                                    // CSS Variables
                                    // Variable à toucher
                                    $valToShow = ceil($ecosysteme['actives']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
                                    // Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

                                    // HACK CSS
                                    //Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total"><?php echo ceil($ecosysteme['actives']); ?>%</span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__txt text-center">% de startups actives</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Entrepreneurs</div>
                                        <div class="datasbloc__val"><?php echo number_format($ecosysteme['entrepreneurs'], 0, '.', ' '); ?></div>
                                        <div class="datasbloc__subtitle">
                                            <?php
                                            if ($ecosysteme['evo_entrepreneurs'] == -1) {
                                                ?>
                                                <span class="ico-decrease"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_entrepreneurs'] == 1) {
                                                ?>
                                                <span class="ico-raise"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_entrepreneurs'] == 0) {
                                                ?>
                                                <span class="ico-stable"></span>
                                            <?php } ?> <?php echo str_replace(",0", "", number_format($ecosysteme['entrepreneurs_12'], 1, ",", "")); ?>% sur 12 mois</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = ceil($ecosysteme['femmes']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo ceil($ecosysteme['femmes']); ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__txt text-center">% de femmes entrepreneures</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Investisseurs France</div>
                                        <div class="datasbloc__val"><?php echo number_format($ecosysteme['investisseurs'], 0, '.', ' '); ?></div>
                                        <div class="datasbloc__subtitle"><?php
                                            if ($ecosysteme['evo_investisseurs'] == -1) {
                                                ?>
                                                <span class="ico-decrease"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_investisseurs'] == 1) {
                                                ?>
                                                <span class="ico-raise"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_investisseurs'] == 0) {
                                                ?>
                                                <span class="ico-stable"></span>
                                            <?php } ?> <?php echo number_format($ecosysteme['investisseurs_12'], 1, ",", ""); ?>% sur 12 mois</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = $ecosysteme['participations_50']; // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo ceil($ecosysteme['participations_50']); ?>% 
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__txt text-center text-center">% d'investisseurs avec <span class="nowrap">plus de 50 participations</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Répartition par effectifs</div>
                                <div class="chart chart--columns">
                                    <div id="chart-effectifs"></div>
                                </div>
                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                        var chart = am4core.create("chart-effectifs", am4charts.XYChart);
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;

                                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.dataFields.category = "category";
                                        categoryAxis.renderer.minGridDistance = 10;
                                        categoryAxis.renderer.fontSize = "10px";
                                        categoryAxis.renderer.labels.template.fill = labelColor;
                                        categoryAxis.dataItems.template.text = "{realName}";
                                        categoryAxis.renderer.grid.disabled = true;


                                        // Title Bottom
                                        categoryAxis.title.text = "Tranches d'effectifs";
                                        categoryAxis.title.rotation = 0;
                                        categoryAxis.title.align = "center";
                                        categoryAxis.title.valign = "bottom";
                                        categoryAxis.title.dy = -5;
                                        categoryAxis.title.fontSize = 11;
                                        categoryAxis.title.fill = labelColor;

                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.renderer.labels.template.fill = labelColor;

                                        // Title left
                                        valueAxis.title.text = "Nombre de startups";
                                        valueAxis.title.rotation = -90;
                                        valueAxis.title.align = "left";
                                        valueAxis.title.valign = "top";
                                        valueAxis.title.dy = 0;
                                        valueAxis.title.fontSize = 11;
                                        valueAxis.title.fill = labelColor;


                                        // single column series for all data
                                        var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                        columnSeries.fill = themeColorColumns;
                                        columnSeries.stroke = 0;
                                        columnSeries.columns.template.width = am4core.percent(80);
                                        columnSeries.dataFields.categoryX = "category";
                                        columnSeries.dataFields.valueY = "value";


                                        ///// DATA
                                        var chartData = [];

                                        var data =
                                                {
                                                    "1-2": {
                                                        "1-2": <?php echo $ecosysteme['eff_1_2']; ?>,
                                                    },
                                                    "3-5": {
                                                        "3-5": <?php echo $ecosysteme['eff_3_5']; ?>,
                                                    },
                                                    "6-10": {
                                                        "6-10": <?php echo $ecosysteme['eff_6_10']; ?>,
                                                    },
                                                    "11-20": {
                                                        "11-20": <?php echo $ecosysteme['eff_11_20']; ?>,
                                                    },
                                                    "21-50": {
                                                        "21-50": <?php echo $ecosysteme['eff_21_50']; ?>,
                                                    },
                                                    "51-100": {
                                                        "51-100": <?php echo $ecosysteme['eff_51_100']; ?>,
                                                    },
                                                    "100+": {
                                                        "100+": <?php echo $ecosysteme['eff_100_plus']; ?>,
                                                    },
                                                }

                                        // process data and prepare it for the chart
                                        for (var providerName in data) {
                                            var providerData = data[providerName];

                                            // add data of one provider to temp array
                                            var tempArray = [];
                                            var count = 0;

                                            // add items
                                            for (var itemName in providerData) {
                                                if (itemName != "financement") {
                                                    count++;
                                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                                }
                                            }
                                            // sort temp array
                                            tempArray.sort(function (a, b) {
                                                if (a.value > b.value) {
                                                    return 1;
                                                } else if (a.value < b.value) {
                                                    return -1
                                                } else {
                                                    return 0;
                                                }
                                            })


                                            // push to the final data
                                            am4core.array.each(tempArray, function (item) {
                                                chartData.push(item);
                                            })
                                        }

                                        valueAxis.renderer.grid.template.strokeWidth = 0;
                                        //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                        categoryAxis.renderer.grid.template.strokeWidth = 0;


                                        chart.data = chartData;

                                    }); // end am4core.ready()
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Répartition par ancienneté</div>
                                <div class="chart chart--columns">
                                    <div id="chart-anciennete"></div>
                                </div>
                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                        var chart = am4core.create("chart-anciennete", am4charts.XYChart);
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;

                                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.dataFields.category = "category";
                                        categoryAxis.renderer.minGridDistance = 10;
                                        categoryAxis.renderer.fontSize = "10px";
                                        categoryAxis.renderer.labels.template.fill = labelColor;
                                        categoryAxis.dataItems.template.text = "{realName}";
                                        categoryAxis.renderer.grid.disabled = true;


                                        // Title Bottom
                                        categoryAxis.title.text = "Tranches d'âges";
                                        categoryAxis.title.rotation = 0;
                                        categoryAxis.title.align = "center";
                                        categoryAxis.title.valign = "bottom";
                                        categoryAxis.title.dy = -5;
                                        categoryAxis.title.fontSize = 11;
                                        categoryAxis.title.fill = labelColor;

                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.renderer.labels.template.fill = labelColor;

                                        // Title left
                                        valueAxis.title.text = "Nombre de startups";
                                        valueAxis.title.rotation = -90;
                                        valueAxis.title.align = "left";
                                        valueAxis.title.valign = "top";
                                        valueAxis.title.dy = 0;
                                        valueAxis.title.fontSize = 11;
                                        valueAxis.title.fill = labelColor;


                                        // single column series for all data
                                        var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                        columnSeries.fill = themeColorColumns;
                                        columnSeries.stroke = 0;
                                        columnSeries.columns.template.width = am4core.percent(80);
                                        columnSeries.dataFields.categoryX = "category";
                                        columnSeries.dataFields.valueY = "value";


                                        ///// DATA
                                        var chartData = [];

                                        var data =
                                                {
                                                    "0-1": {
                                                        "0 à 1 an": <?php echo $ecosysteme['age_0_1']; ?>,
                                                    },
                                                    "1-2": {
                                                        "1 à 2 ans": <?php echo $ecosysteme['age_1_2']; ?>,
                                                    },
                                                    "2-3": {
                                                        "2 à 3 ans": <?php echo $ecosysteme['age_2_3']; ?>,
                                                    },
                                                    "3-4": {
                                                        "3 à 4 ans": <?php echo $ecosysteme['age_3_4']; ?>,
                                                    },
                                                    "4-5": {
                                                        "4 à 5 ans": <?php echo $ecosysteme['age_4_5']; ?>,
                                                    },
                                                    "5-6": {
                                                        "5 à 6 ans": <?php echo $ecosysteme['age_5_6']; ?>,
                                                    },
                                                    "6-7": {
                                                        "6 à 7 ans": <?php echo $ecosysteme['age_6_7']; ?>,
                                                    },
                                                    "7+": {
                                                        "7 ans +": <?php echo $ecosysteme['age_7_plus']; ?>,
                                                    }
                                                }



                                        // process data and prepare it for the chart
                                        for (var providerName in data) {
                                            var providerData = data[providerName];

                                            // add data of one provider to temp array
                                            var tempArray = [];
                                            var count = 0;

                                            // add items
                                            for (var itemName in providerData) {
                                                if (itemName != "financement") {
                                                    count++;
                                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                                }
                                            }
                                            // sort temp array
                                            tempArray.sort(function (a, b) {
                                                if (a.value > b.value) {
                                                    return 1;
                                                } else if (a.value < b.value) {
                                                    return -1
                                                } else {
                                                    return 0;
                                                }
                                            })


                                            // push to the final data
                                            am4core.array.each(tempArray, function (item) {
                                                chartData.push(item);
                                            })
                                        }

                                        valueAxis.renderer.grid.template.strokeWidth = 0;
                                        //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                        categoryAxis.renderer.grid.template.strokeWidth = 0;


                                        chart.data = chartData;

                                    }); // end am4core.ready()
                                </script>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="ctg ctg--3_3">
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Emplois générés</div>
                                        <div class="datasbloc__val"><?php echo number_format($ecosysteme['emplois_generes'], 0, '.', ' '); ?></div>
                                        <div class="datasbloc__subtitle"><?php
                                            if ($ecosysteme['evo_emplois_generes'] == -1) {
                                                ?>
                                                <span class="ico-decrease"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_emplois_generes'] == 1) {
                                                ?>
                                                <span class="ico-raise"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_emplois_generes'] == 0) {
                                                ?>
                                                <span class="ico-stable"></span>
                                            <?php } ?> <?php echo number_format($ecosysteme['emplois_generes_12'], 1, ",", ""); ?>% sur 12 mois</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = $ecosysteme['emplois_generes_12']; // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total"><?php echo ceil($ecosysteme['emplois_generes_12']); ?>%</span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__txt text-center">% des emplois créés au cours des 12 derniers mois</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Défaillances</div>
                                        <div class="datasbloc__val"><?php echo number_format($ecosysteme['defaillances'], 0, '.', ' '); ?></div>
                                        <div class="datasbloc__subtitle"><?php
                                            if ($ecosysteme['evo_defaillances'] == -1) {
                                                ?>
                                                <span class="ico-decrease"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_defaillances'] == 1) {
                                                ?>
                                                <span class="ico-raise"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_defaillances'] == 0) {
                                                ?>
                                                <span class="ico-stable"></span>
                                            <?php } ?> <?php echo str_replace(",0", "", number_format($ecosysteme['defaillances_12'], 1, ",", "")); ?>% sur 12 mois</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = ceil($ecosysteme['defaillances_perc']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total"><?php echo ceil($ecosysteme['defaillances_perc']); ?>%</span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__txt text-center">% défaillance historique des startups créées</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Emplois détruits</div>
                                        <div class="datasbloc__val"><?php echo number_format($ecosysteme['emplois_detruits'], 0, '.', ' '); ?></div>
                                        <div class="datasbloc__subtitle"><?php
                                            if ($ecosysteme['evo_emplois_detruits'] == -1) {
                                                ?>
                                                <span class="ico-decrease"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_emplois_detruits'] == 1) {
                                                ?>
                                                <span class="ico-raise"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_emplois_detruits'] == 0) {
                                                ?>
                                                <span class="ico-stable"></span>
                                            <?php } ?> <?php echo number_format($ecosysteme['emplois_detruits_12'], 1, ",", ""); ?>% sur 12 mois</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = ceil($ecosysteme['detruits_generes']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total"><?php echo ceil($ecosysteme['detruits_generes']); ?>%</span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__txt text-center">% emplois détruits<br/> sur emplois générés</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Dynamique de création nette startups</div>
                                <div class="chart chart--columns">
                                    <div id="chart-dynamique-startups"></div>
                                </div>

                                <!-- Chart code -->
                                <script>
                                    am4core.ready(function () {

                                        var chart = am4core.create("chart-dynamique-startups", am4charts.XYChart);
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;


                                        var data = [
                                            {
                                                "date": "2016",
                                                "value": "<?php echo $ecosysteme['net_2016']; ?>",
                                            },
                                            {
                                                "date": "2017",
                                                "value": "<?php echo $ecosysteme['net_2017']; ?>",
                                            },
                                            {
                                                "date": "2018",
                                                "value": "<?php echo $ecosysteme['net_2018']; ?>",
                                            },
                                            {
                                                "date": "2019",
                                                "value": "<?php echo $ecosysteme['net_2019']; ?>",
                                            },
                                            {
                                                "date": "2020",
                                                "value": "<?php echo $ecosysteme['net_2020']; ?>",
                                            },
                                            {
                                                "date": "2021",
                                                "value": "<?php echo $ecosysteme['net_2021']; ?>",
                                            },
                                            {
                                                "date": "2022",
                                                "value": "<?php echo $ecosysteme['net_2022']; ?>",
                                            }
                                        ];

                                        chart.data = data;

                                        // Create axes
                                        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                                        dateAxis.title.text = "Années";
                                        dateAxis.renderer.minGridDistance = 60;
                                        dateAxis.renderer.fontSize = "10px";
                                        dateAxis.renderer.labels.template.fill = labelColor;
                                        dateAxis.dataItems.template.text = "{realName}";
                                        dateAxis.renderer.grid.disabled = true;
                                        dateAxis.title.rotation = 0;
                                        dateAxis.title.align = "center";
                                        dateAxis.title.valign = "bottom";
                                        dateAxis.title.dy = -5;
                                        dateAxis.title.fontSize = 11;
                                        dateAxis.title.fill = labelColor;

                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.fontSize = "10px";
                                        valueAxis.renderer.labels.template.fill = labelColor;
                                        valueAxis.title.text = "Volume net de startups";
                                        valueAxis.title.rotation = -90;
                                        valueAxis.title.align = "left";
                                        valueAxis.title.valign = "top";
                                        valueAxis.title.dy = 0;
                                        valueAxis.title.fontSize = 11;
                                        valueAxis.title.fill = labelColor;

                                        // Create series
                                        var series = chart.series.push(new am4charts.LineSeries());
                                        series.dataFields.valueY = "value";
                                        series.dataFields.dateX = "date";
                                        series.tensionX = 0.8;
                                        series.bullets.push(new am4charts.CircleBullet());
                                        series.fill = themeColorLine;
                                        series.strokeWidth = 2;
                                        series.fillOpacity = 0;
                                        series.stroke = themeColorLine;
                                        series.tooltip.pointerOrientation = "vertical";
                                    });
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Dynamique de création nette d'emplois</div>
                                <div class="chart chart--columns">
                                    <div id="chart-dynamique-emplois"></div>
                                </div>

                                <!-- Chart code -->
                                <script>
                                    am4core.ready(function () {

                                        var chart = am4core.create("chart-dynamique-emplois", am4charts.XYChart);
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;


                                        var data = [
                                            {
                                                "date": "2016",
                                                "value": "<?php echo $ecosysteme['emploi_net_2016']; ?>",
                                            },
                                            {
                                                "date": "2017",
                                                "value": "<?php echo $ecosysteme['emploi_net_2017']; ?>",
                                            },
                                            {
                                                "date": "2018",
                                                "value": "<?php echo $ecosysteme['emploi_net_2018']; ?>",
                                            },
                                            {
                                                "date": "2019",
                                                "value": "<?php echo $ecosysteme['emploi_net_2019']; ?>",
                                            },
                                            {
                                                "date": "2020",
                                                "value": "<?php echo $ecosysteme['emploi_net_2020']; ?>",
                                            },
                                            {
                                                "date": "2021",
                                                "value": "<?php echo $ecosysteme['emploi_net_2021']; ?>",
                                            },
                                            {
                                                "date": "2022",
                                                "value": "<?php echo $ecosysteme['emploi_net_2022']; ?>",
                                            }
                                        ];

                                        chart.data = data;

                                        // Create axes
                                        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                                        dateAxis.title.text = "Années";
                                        dateAxis.renderer.minGridDistance = 60;
                                        dateAxis.renderer.fontSize = "10px";
                                        dateAxis.renderer.labels.template.fill = labelColor;
                                        dateAxis.dataItems.template.text = "{realName}";
                                        dateAxis.renderer.grid.disabled = true;
                                        dateAxis.title.rotation = 0;
                                        dateAxis.title.align = "center";
                                        dateAxis.title.valign = "bottom";
                                        dateAxis.title.dy = -5;
                                        dateAxis.title.fontSize = 11;
                                        dateAxis.title.fill = labelColor;

                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.fontSize = "10px";
                                        valueAxis.renderer.labels.template.fill = labelColor;
                                        valueAxis.title.text = "Emplois nets créés";
                                        valueAxis.title.rotation = -90;
                                        valueAxis.title.align = "left";
                                        valueAxis.title.valign = "top";
                                        valueAxis.title.dy = 0;
                                        valueAxis.title.fontSize = 11;
                                        valueAxis.title.fill = labelColor;

                                        // Create series
                                        var series = chart.series.push(new am4charts.LineSeries());
                                        series.dataFields.valueY = "value";
                                        series.dataFields.dateX = "date";
                                        series.tensionX = 0.8;
                                        series.bullets.push(new am4charts.CircleBullet());
                                        series.fill = themeColorLine;
                                        series.strokeWidth = 2;
                                        series.fillOpacity = 0;
                                        series.stroke = themeColorLine;
                                        series.tooltip.pointerOrientation = "vertical";
                                    });
                                </script>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Analyse des financements France</div>
                        <div class="ctg ctg--3_3">
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financement total</div>
                                        <div class="datasbloc__val"><?php echo str_replace(",0", "", number_format($ecosysteme['financements'] / 1000000, 1, ",", "")) ?> Mds€</div>
                                        <div class="datasbloc__subtitle"><?php
                                            if ($ecosysteme['evo_financements'] == -1) {
                                                ?>
                                                <span class="ico-decrease"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_financements'] == 1) {
                                                ?>
                                                <span class="ico-raise"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_financements'] == 0) {
                                                ?>
                                                <span class="ico-stable"></span>
                                            <?php } ?> <?php echo number_format($ecosysteme['financements_12'], 1, ",", "") ?>% sur 12 mois</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = ceil($ecosysteme['financements_12']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total"><?php echo ceil($ecosysteme['financements_12']); ?>%</span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__txt text-center" title="% des financements réalisés au cours des 12 derniers mois">% des financements réalisés au cours des 12 derniers mois</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Levées de fonds enregistrées</div>
                                        <div class="datasbloc__val"><?php echo number_format($ecosysteme['levees'], 0, '.', ' '); ?></div>
                                        <div class="datasbloc__subtitle"><?php
                                            if ($ecosysteme['evo_levees'] == -1) {
                                                ?>
                                                <span class="ico-decrease"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_levees'] == 1) {
                                                ?>
                                                <span class="ico-raise"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_levees'] == 0) {
                                                ?>
                                                <span class="ico-stable"></span>
                                            <?php } ?> <?php echo number_format($ecosysteme['levees_12'], 1, ",", ""); ?>% sur 12 mois</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = ceil($ecosysteme['levees_12']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total"><?php echo ceil($ecosysteme['levees_12']); ?>%</span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__txt text-center" title="des levées de fonds réalisées au cours des 12 derniers mois">% des levées de fonds réalisées au cours des 12 derniers mois</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Moyenne des fonds levés</div>
                                        <div class="datasbloc__val"><?php echo str_replace(",0", "", number_format($ecosysteme['moy_financement'] / 1000, 1, ",", "")) ?> M€</div>
                                        <div class="datasbloc__subtitle"><?php
                                            if ($ecosysteme['evo_moy_financement'] == -1) {
                                                ?>
                                                <span class="ico-decrease"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_moy_financement'] == 1) {
                                                ?>
                                                <span class="ico-raise"></span>
                                            <?php } ?>
                                            <?php
                                            if ($ecosysteme['evo_moy_financement'] == 0) {
                                                ?>
                                                <span class="ico-stable"></span>
                                            <?php } ?><?php echo ceil($ecosysteme['moy_financement_12']); ?>% sur 12 mois</div>
                                    </div>
                                    <?php
                                    // Calcul pour le donut avec Pourcentage de commentaires positifs
                                    // CSS Variables
                                    // Variable à toucher
                                    $valToShow = ceil($ecosysteme['financement_sup_moy']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
                                    // Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

                                    // HACK CSS
                                    //Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total"><?php echo ceil($ecosysteme['financement_sup_moy']); ?>%</span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__txt text-center">% des financements supérieurs à la moyenne</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Historique des financements France</div>
                                <div class="chart chart--columns">
                                    <div id="chart-historique-financements-fr"></div>
                                </div>

                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                        var chart = am4core.create("chart-historique-financements-fr", am4charts.XYChart);
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;

                                        // will use this to store colors of the same items
                                        var colors = {};

                                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.dataFields.category = "category";
                                        categoryAxis.renderer.minGridDistance = 10;
                                        categoryAxis.renderer.fontSize = "10px";
                                        categoryAxis.renderer.labels.template.fill = labelColor;
                                        categoryAxis.dataItems.template.text = "{realName}";
                                        categoryAxis.renderer.grid.disabled = true;


                                        // Title Bottom
                                        categoryAxis.title.text = "Années";
                                        categoryAxis.title.rotation = 0;
                                        categoryAxis.title.align = "center";
                                        categoryAxis.title.valign = "bottom";
                                        categoryAxis.title.dy = -5;
                                        categoryAxis.title.fontSize = 11;
                                        categoryAxis.title.fill = labelColor;

                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.renderer.labels.template.fill = labelColor;

                                        // Title left
                                        valueAxis.title.text = "Nombre de startups financées";
                                        valueAxis.title.rotation = -90;
                                        valueAxis.title.align = "left";
                                        valueAxis.title.valign = "top";
                                        valueAxis.title.dy = 0;
                                        valueAxis.title.fontSize = 11;
                                        valueAxis.title.fill = labelColor;


                                        // single column series for all data
                                        var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                        columnSeries.fill = themeColorColumns;
                                        columnSeries.stroke = 0;
                                        columnSeries.columns.template.width = am4core.percent(80);
                                        columnSeries.dataFields.categoryX = "category";
                                        columnSeries.dataFields.valueY = "value";



                                        // second value axis for quantity
                                        var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis2.renderer.opposite = true;
                                        valueAxis2.syncWithAxis = valueAxis;
                                        valueAxis2.tooltip.disabled = true;
                                        valueAxis2.renderer.labels.template.fontSize = 11;
                                        valueAxis2.renderer.labels.template.fill = labelColor;

                                        // Title right
                                        valueAxis2.title.text = "Financement total (Mds€)";
                                        valueAxis2.title.rotation = -90;
                                        valueAxis2.title.align = "left";
                                        valueAxis2.title.valign = "top";
                                        valueAxis2.title.dy = 0;
                                        valueAxis2.title.fontSize = 11;
                                        valueAxis2.title.fill = labelColor;

                                        // quantity line series
                                        var lineSeries = chart.series.push(new am4charts.LineSeries());
                                        lineSeries.dataFields.categoryX = "category";
                                        lineSeries.dataFields.valueY = "financement";
                                        lineSeries.yAxis = valueAxis2;
                                        lineSeries.bullets.push(new am4charts.CircleBullet());
                                        lineSeries.fill = themeColorLine;
                                        lineSeries.strokeWidth = 2;
                                        lineSeries.fillOpacity = 0;
                                        lineSeries.stroke = themeColorLine;

                                        // when data validated, adjust location of data item based on count
                                        lineSeries.events.on("datavalidated", function () {
                                            lineSeries.dataItems.each(function (dataItem) {
                                                // if count divides by two, location is 0 (on the grid)
                                                if (dataItem.dataContext.count / 2 == Math.round(dataItem.dataContext.count / 2)) {
                                                    dataItem.setLocation("categoryX", 0);
                                                }
                                                // otherwise location is 0.5 (middle)
                                                else {
                                                    dataItem.setLocation("categoryX", 0.5);
                                                }
                                            })
                                        })

                                        ///// DATA
                                        var chartData = [];
                                        var lineSeriesData = [];
                                        chart.numberFormatter.numberFormat = "#.#";

                                        var data =
                                                {
                                                    "2014": {
                                                        "2014": <?php echo $ecosysteme['nbr_lf_2014']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_2014'] / 1000000); ?>
                                                    },
                                                    "2015": {
                                                        "2015": <?php echo $ecosysteme['nbr_lf_2015']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_2015'] / 1000000); ?>
                                                    },
                                                    "2016": {
                                                        "2016": <?php echo $ecosysteme['nbr_lf_2016']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_2016'] / 1000000); ?>
                                                    },
                                                    "2017": {
                                                        "2017": <?php echo $ecosysteme['nbr_lf_2017']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_2017'] / 1000000); ?>
                                                    },
                                                    "2018": {
                                                        "2018": <?php echo $ecosysteme['nbr_lf_2018']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_2018'] / 1000000); ?>
                                                    },
                                                    "2019": {
                                                        "2019": <?php echo $ecosysteme['nbr_lf_2019']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_2019'] / 1000000); ?>
                                                    },
                                                    "2020": {
                                                        "2020": <?php echo $ecosysteme['nbr_lf_2020']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_2020'] / 1000000); ?>
                                                    },
                                                    "2021": {
                                                        "2021": <?php echo $ecosysteme['nbr_lf_2021']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_2021'] / 1000000); ?>
                                                    },
                                                    "2022": {
                                                        "2022": <?php echo $ecosysteme['nbr_lf_2022']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_2022'] / 1000000); ?>
                                                    }
                                                }

                                        // process data and prepare it for the chart
                                        for (var providerName in data) {
                                            var providerData = data[providerName];

                                            // add data of one provider to temp array
                                            var tempArray = [];
                                            var count = 0;

                                            // add items
                                            for (var itemName in providerData) {
                                                if (itemName != "financement") {
                                                    count++;
                                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                                }
                                            }
                                            // sort temp array
                                            tempArray.sort(function (a, b) {
                                                if (a.value > b.value) {
                                                    return 1;
                                                } else if (a.value < b.value) {
                                                    return -1
                                                } else {
                                                    return 0;
                                                }
                                            })

                                            // add quantity and count to middle data item (line series uses it)
                                            var lineSeriesDataIndex = Math.floor(count / 2);
                                            tempArray[lineSeriesDataIndex].financement = providerData.financement;
                                            tempArray[lineSeriesDataIndex].count = count;
                                            // push to the final data
                                            am4core.array.each(tempArray, function (item) {
                                                chartData.push(item);
                                            })
                                        }

                                        valueAxis.renderer.grid.template.strokeWidth = 0;
                                        //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                        categoryAxis.renderer.grid.template.strokeWidth = 0;


                                        chart.data = chartData;



                                    }); // end am4core.ready()
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Maturité des startups financées</div>
                                <div class="chart chart--columns">
                                    <div id="chart-maturite-startups-financees"></div>
                                </div>

                                <script>
                                    // Demo
                                    // https://jsfiddle.net/api/post/library/pure/
                                    // https://www.amcharts.com/demos/grouped-and-sorted-columns/
                                    am4core.ready(function () {

                                        var chart = am4core.create("chart-maturite-startups-financees", am4charts.XYChart);
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;

                                        // will use this to store colors of the same items
                                        var colors = {};

                                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.dataFields.category = "category";
                                        categoryAxis.renderer.minGridDistance = 10;
                                        categoryAxis.renderer.fontSize = "10px";
                                        categoryAxis.renderer.labels.template.fill = labelColor;
                                        categoryAxis.dataItems.template.text = "{realName}";
                                        categoryAxis.renderer.grid.disabled = true;

                                        chart.numberFormatter.numberFormat = "#.";

                                        // Title Bottom
                                        categoryAxis.title.text = "Ancienneté des startups au moment de l'investissement";
                                        categoryAxis.title.rotation = 0;
                                        categoryAxis.title.align = "center";
                                        categoryAxis.title.valign = "bottom";
                                        categoryAxis.title.dy = -5;
                                        categoryAxis.title.fontSize = 11;
                                        categoryAxis.title.fill = labelColor;

                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.renderer.labels.template.fill = labelColor;

                                        // Title left
                                        valueAxis.title.text = "Nombre de startups financées";
                                        valueAxis.title.rotation = -90;
                                        valueAxis.title.align = "left";
                                        valueAxis.title.valign = "top";
                                        valueAxis.title.dy = 0;
                                        valueAxis.title.fontSize = 11;
                                        valueAxis.title.fill = labelColor;


                                        // single column series for all data
                                        var columnSeries = chart.series.push(new am4charts.ColumnSeries());
                                        columnSeries.fill = themeColorColumns;
                                        columnSeries.stroke = 0;
                                        columnSeries.columns.template.width = am4core.percent(80);
                                        columnSeries.dataFields.categoryX = "category";
                                        columnSeries.dataFields.valueY = "value";



                                        // second value axis for quantity
                                        var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis2.renderer.opposite = true;
                                        valueAxis2.syncWithAxis = valueAxis;
                                        valueAxis2.tooltip.disabled = true;
                                        valueAxis2.renderer.labels.template.fontSize = 11;
                                        valueAxis2.renderer.labels.template.fill = labelColor;
                                        /* columnSeries.cursorTooltipEnabled = true;
                                         columnSeries.columns.template.tooltipPosition = "pointer";
                                         columnSeries.tooltipText = "{valueX}";*/

                                        // Title right
                                        valueAxis2.title.text = "Financement total (Mds€)";
                                        valueAxis2.title.rotation = -90;
                                        valueAxis2.title.align = "left";
                                        valueAxis2.title.valign = "top";
                                        valueAxis2.title.dy = 0;
                                        valueAxis2.title.fontSize = 11;
                                        valueAxis2.title.fill = labelColor;

                                        // quantity line series
                                        var lineSeries = chart.series.push(new am4charts.LineSeries());
                                        lineSeries.dataFields.categoryX = "category";
                                        lineSeries.dataFields.valueY = "financement";
                                        lineSeries.yAxis = valueAxis2;
                                        lineSeries.bullets.push(new am4charts.CircleBullet());
                                        lineSeries.fill = themeColorLine;
                                        lineSeries.strokeWidth = 2;
                                        lineSeries.fillOpacity = 0;
                                        lineSeries.stroke = themeColorLine;

                                        // when data validated, adjust location of data item based on count
                                        lineSeries.events.on("datavalidated", function () {
                                            lineSeries.dataItems.each(function (dataItem) {
                                                // if count divides by two, location is 0 (on the grid)
                                                if (dataItem.dataContext.count / 2 == Math.round(dataItem.dataContext.count / 2)) {
                                                    dataItem.setLocation("categoryX", 0);
                                                }
                                                // otherwise location is 0.5 (middle)
                                                else {
                                                    dataItem.setLocation("categoryX", 0.5);
                                                }
                                            })
                                        })

                                        ///// DATA
                                        var chartData = [];
                                        var lineSeriesData = [];

                                        var data =
                                                {
                                                    "1 an": {
                                                        "1 an": <?php echo $ecosysteme['nbr_lf_1']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_1'] / 1000000); ?>
                                                    },
                                                    "2 ans": {
                                                        "2 ans": <?php echo $ecosysteme['nbr_lf_2']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_2'] / 1000000); ?>
                                                    },
                                                    "3 ans": {
                                                        "3 ans": <?php echo $ecosysteme['nbr_lf_3']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_3'] / 1000000); ?>
                                                    },
                                                    "4 ans": {
                                                        "4 ans": <?php echo $ecosysteme['nbr_lf_4']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_4'] / 1000000); ?>
                                                    },
                                                    "5 ans": {
                                                        "5 ans": <?php echo $ecosysteme['nbr_lf_5']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_5'] / 1000000); ?>
                                                    },
                                                    "6 ans": {
                                                        "6 ans": <?php echo $ecosysteme['nbr_lf_6']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_6'] / 1000000); ?>
                                                    },
                                                    "7 ans": {
                                                        "7 ans": <?php echo $ecosysteme['nbr_lf_7']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_7'] / 1000000); ?>
                                                    },
                                                    "+7 ans": {
                                                        "+7 ans": <?php echo $ecosysteme['nbr_lf_8']; ?>,
                                                        "financement": <?php echo ceil($ecosysteme['total_lf_8'] / 1000000); ?>
                                                    }
                                                }

                                        // process data and prepare it for the chart
                                        for (var providerName in data) {
                                            var providerData = data[providerName];

                                            // add data of one provider to temp array
                                            var tempArray = [];
                                            var count = 0;

                                            // add items
                                            for (var itemName in providerData) {
                                                if (itemName != "financement") {
                                                    count++;
                                                    // we generate unique category for each column (providerName + "_" + itemName) and store realName
                                                    tempArray.push({category: providerName + "_" + itemName, realName: itemName, value: providerData[itemName], provider: providerName})
                                                }
                                            }
                                            // sort temp array
                                            tempArray.sort(function (a, b) {
                                                if (a.value > b.value) {
                                                    return 1;
                                                } else if (a.value < b.value) {
                                                    return -1
                                                } else {
                                                    return 0;
                                                }
                                            })

                                            // add quantity and count to middle data item (line series uses it)
                                            var lineSeriesDataIndex = Math.floor(count / 2);
                                            tempArray[lineSeriesDataIndex].financement = providerData.financement;
                                            tempArray[lineSeriesDataIndex].count = count;
                                            // push to the final data
                                            am4core.array.each(tempArray, function (item) {
                                                chartData.push(item);
                                            })
                                        }

                                        valueAxis.renderer.grid.template.strokeWidth = 0;
                                        //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                        categoryAxis.renderer.grid.template.strokeWidth = 0;


                                        chart.data = chartData;



                                    }); // end am4core.ready()
                                </script>
                            </div>
                        </div>
                    </section>


                    <section class="module">
                        <div class="ctg ctg--3_3">
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financement<br />Seed</div>
                                        <div class="datasbloc__val">Moy. <?php echo str_replace(",0", "", number_format($ecosysteme['financement_seed_moy'] / 1000, 1, ",", "")); ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = ceil($ecosysteme['financement_seed_perc']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total"><?php echo ceil($ecosysteme['financement_seed_perc']); ?>%</span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($ecosysteme['financement_seed_min'] / 1000, 1, ",", "")); ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($ecosysteme['financement_seed_max'] / 1000, 1, ",", "")); ?> M€</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financements<br />Croissance</div>
                                        <div class="datasbloc__val">Moy. <?php echo str_replace(",0", "", number_format($ecosysteme['financement_croissance_moy'] / 1000, 1, ",", "")); ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = ceil($ecosysteme['financement_croissance_perc']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total"><?php echo ceil($ecosysteme['financement_croissance_perc']); ?>%</span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($ecosysteme['financement_croissance_min'] / 1000, 1, ",", "")); ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($ecosysteme['financement_croissance_max'] / 1000, 1, ",", "")); ?> M€</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financements<br />Développement</div>
                                        <div class="datasbloc__val">Moy. <?php echo str_replace(",0", "", number_format($ecosysteme['financement_developpement_moy'] / 1000, 1, ",", "")); ?> M€</div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = ceil($ecosysteme['financement_developpement_perc']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one" style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two" style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total"><?php echo ceil($ecosysteme['financement_developpement_perc']); ?>%</span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min"><?php echo str_replace(",0", "", number_format($ecosysteme['financement_developpement_min'] / 1000, 1, ",", "")); ?> M€</div>
                                            <div class="donut__legend__max"><?php echo str_replace(",0", "", number_format($ecosysteme['financement_developpement_max'] / 1000, 1, ",", "")); ?> M€</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Startups France par marchés et secteurs</div>
                        <div class="bloc">
                            <div class="tablebloc">
                                <table class="table" id="table-marches-secteurs">
                                    <thead>
                                        <tr>
                                            <th>Secteur</th>
                                            <th class="text-center nowrap">Startups</th>
                                            <th class="text-center">Créations sur 12 mois</th>
                                            <th class="text-center">Ancienneté moyenne</th>
                                            <th class="text-center">Effectifs moyens</th>
                                            <th class="text-center">Score maturité</th>
                                            <th class="text-center">Financements cumulés</th>
                                            <th class="text-center">Levées de fonds sur 12 mois</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql_secteur = mysqli_query($link, "SELECT  * from marches_secteurs");
                                        while ($data_secteur = mysqli_fetch_array($sql_secteur)) {
                                            $moys_stat = mysqli_fetch_array(mysqli_query($link, "select * from secteur where id=" . $data_secteur['id_secteur']));
                                            ?>
                                            <tr>
                                                <td><?php echo ($moys_stat['nom_secteur']); ?></td>
                                                <td class="text-center moneyCell"><?php echo $data_secteur['nbr_startups'] ?></td>
                                                <td class="text-center moneyCell"><?php echo str_replace(",0", "", number_format($data_secteur['creations_12'], 1, ",", "")) ?>%</td>
                                                <td class="text-center moneyCell"><?php echo str_replace(",0", "", number_format($data_secteur['moy_anciennete'], 1, ",", "")) ?> ans</td>
                                                <td class="text-center moneyCell"><?php echo $data_secteur['effectifs'] ?></td>
                                                <td class="text-center moneyCell"><?php echo str_replace(",0", "", number_format($data_secteur['score_maturite'], 1, ",", "")) ?></td>
                                                <td class="text-center moneyCell"><?php echo str_replace(",0", "", number_format($data_secteur['financements_cumules'] / 1000, 0, ",", " ")); ?>&nbsp;M€</td>
                                                <td class="text-center moneyCell"><?php echo str_replace(",0", "", number_format($data_secteur['levee_12'], 1, ",", "")) ?>%</td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Géographie</div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Startups par région</div>

                                <div class="chart chart--map">
                                    <div id="chart-map"></div>
                                </div>

                                <script>

                                    // Create map instance
                                    var chart = am4core.create("chart-map", am4maps.MapChart);
                                    chart.paddingTop = 0;
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    // Set map definition
                                    chart.geodata = am4geodata_franceLow;
                                    // Set projection
                                    chart.projection = new am4maps.projections.Miller();
                                    // Create map polygon series
                                    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
                                    //Set min/max fill color for each area
                                    polygonSeries.heatRules.push({
                                        property: "fill",
                                        target: polygonSeries.mapPolygons.template,
                                        //min: chart.colors.getIndex(1).brighten(1),
                                        //max: chart.colors.getIndex(1).brighten(-0.3),
                                        logarithmic: true
                                    });
                                    // Make map load polygon data (state shapes and names) from GeoJSON
                                    polygonSeries.useGeodata = true;
                                    // Set heatmap values for each state
                                    // Bleu 1 le + clair : #E2F4FC
                                    // Bleu 2 : #CEEAF7
                                    // Bleu 3 : #BEE7F8
                                    // Bleu 4 le + foncé : #ACDDF2
<?php

function valueFillColor($val) {
    if ($val < 300) {
        return "#E2F4FC";
    } elseif ($val > 300 && $val <= 600) {
        return "#CEEAF7";
    } elseif ($val > 600 && $val <= 1000) {
        return "#BEE7F8";
    } elseif ($val > 100 && $val <= 2000) {
        return "#9CE0FD";
    } elseif ($val > 2000) {
        return "#64D0FF";
    }

    //echo number_format($tab_list_nbr_deps[$i], 0, '.', ' ');
}
?>

                                    polygonSeries.data = [
<?php
$liste_region = explode(";", $ecosysteme['list_reg']);
$liste_nb_region = explode(";", $ecosysteme['nbr_reg']);
$count_reg = count($liste_region);
for ($i = 0; $i < $count_reg; $i++) {
    $ma_reg = $liste_region[$i];
    $code_reg = mysqli_fetch_array(mysqli_query($link, "select * from region_new where region_new='" . trim(addslashes($ma_reg)) . "'"));
    ?>
                                            {
                                                "id": "<?php echo $code_reg['code'] ?>",
                                                "value": <?php echo $liste_nb_region[$i] ?>,

                                                "fill": am4core.color("<?php echo valueFillColor($liste_nb_region[$i]); ?>")
                                                        //"fill": am4core.color("#ff0000"),
                                            },
    <?php
}
?>

                                    ];
                                    // Configure series tooltip
                                    var polygonTemplate = polygonSeries.mapPolygons.template;
                                    polygonTemplate.tooltipText = "[#fff font-size: 11px]{name}: {value}[/]";
                                    polygonTemplate.nonScalingStroke = true;
                                    polygonTemplate.strokeWidth = 1.5;
                                    polygonTemplate.propertyFields.fill = "fill";
                                    polygonTemplate.stroke = themeColorGrey;
                                    polygonTemplate.properties.fill = am4core.color("#E0E0E0");
                                    // Create hover state and set alternative fill color
                                    var hs = polygonTemplate.states.create("hover");
                                    hs.properties.fill = themeColorBlue;
                                    hs.properties.color = am4core.color("#ff0000");
                                    chart.seriesContainer.draggable = false;
                                    chart.seriesContainer.resizable = false;
                                    chart.maxZoomLevel = 1;
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Emplois créés par département</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="iconCell">Évol.</th>
                                                <th>Département</th>
                                                <th class="text-center">Effectifs</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $tab_list_deps = explode(";", $ecosysteme['list_dep']);
                                            $tab_list_nbr_deps = explode(";", $ecosysteme['nbr_dep']);
                                            $tab_list_evo_nbr_dep = explode(";", $ecosysteme['evo_nbr_dep']);
                                            $b = count($tab_list_deps);
                                            for ($i = 0; $i < 10; $i++) {
                                                if ($tab_list_deps[$i] != "") {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php
                                                            if ($tab_list_evo_nbr_dep[$i] == -1) {
                                                                ?>
                                                                <span class="ico-decrease"></span>
                                                            <?php } ?>
                                                            <?php
                                                            if ($tab_list_evo_nbr_dep[$i] == 1) {
                                                                ?>
                                                                <span class="ico-raise"></span>
                                                            <?php } ?>
                                                            <?php
                                                            if ($tab_list_evo_nbr_dep[$i] == 0) {
                                                                ?>
                                                                <span class="ico-stable"></span>
                                                            <?php } ?>
                                                        </td>
                                                        <td><?php
                                                            if ($tab_list_deps[$i] != '')
                                                                echo ($tab_list_deps[$i]);
                                                            else
                                                                echo "NC";
                                                            ?></td>
                                                        <td class="text-center"><?php echo number_format($tab_list_nbr_deps[$i], 0, '.', ' '); ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <?php include ('layout/footer.php'); ?>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>jquery.dataTables.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>datatables.bootstrap.min.js?<?= time(); ?>"></script>

        <script>
                                    var table = jQuery('#table-marches-secteurs').DataTable({
                                        "searching": false,
                                        "order": [[3, "desc"]],
                                        "nextPrev": false,
                                        "bLengthChange": false,
                                        "bInfo": false,
                                        "bPaginate": false,
                                        "autoWidth": false,
                                        //"fixedColumns": false,
                                        initComplete: (settings, json) => {
                                            $('#paginateTable').empty();
                                            $('#mine_paginate').appendTo('#paginateTable');
                                        },
                                        "language": {
                                            "paginate": {
                                                "previous": "Précédent",
                                                "next": "Suivant"
                                            }
                                        }
                                    });

        </script>




        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>


        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
                                    (function (i, s, o, g, r, a, m) {
                                        i['GoogleAnalyticsObject'] = r;
                                        i[r] = i[r] || function () {
                                            (i[r].q = i[r].q || []).push(arguments)
                                        }, i[r].l = 1 * new Date();
                                        a = s.createElement(o),
                                                m = s.getElementsByTagName(o)[0];
                                        a.async = 1;
                                        a.src = g;
                                        m.parentNode.insertBefore(a, m)
                                    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                                    ga('create', 'UA-36251023-1', 'auto');
                                    ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
                var $ = jQuery;
                var valeur = document.getElementById("search-box").value;
                $.ajax({
                    type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                        $("#search-box").css("background", "#FFF");
                    }
                });
            }

            function selectCountry(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
                const words = val.split('/');
                $("#suggesstion-box").hide();
                window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>