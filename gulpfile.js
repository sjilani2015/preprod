var gulp            = require('gulp');
var sass            = require('gulp-sass');
var sourcemaps      = require('gulp-sourcemaps');
var concat          = require('gulp-concat');
var cleanCss        = require('gulp-clean-css');
var util            = require('gulp-util');
var gulpif          = require('gulp-if');
var minify          = require('gulp-minify');


// Global configuration
var config = {
    assetsDir: './src/',
    sassPattern: 'stylesheets/**/*.scss',
    cssoutputDir: './static/css',
    jsoutputDir: './static/js',
    rename: 'app.css',
    production: true, // !!util.env.production,
    sourceMaps: true, // !util.env.production
};


// Add specific css files
var app = {};
app.addStyle = function(paths, outputFilename, cssoutputDir) {
    gulp.src(paths)
        .pipe(gulpif(config.sourceMaps, sourcemaps.init()))
        .pipe(sass())
        .pipe(concat(outputFilename))
        .pipe(config.production ? cleanCss() : util.noop())
        .pipe(gulpif(config.sourceMaps, sourcemaps.write('.')))
        .pipe(gulp.dest(cssoutputDir));
};


//==============================
// SASS Tasks for css
//==============================

// SASS
gulp.task('base', function() {
    app.addStyle(
        [ config.assetsDir + 'stylesheets/base.scss' ],
        config.rename,
        config.cssoutputDir
    );
});



//==============================
// JavaScript
//==============================

gulp.task('js', function () {
    // Minifications de tous les spécifiques
    gulp.src([
        config.assetsDir + 'js/app.js',
        config.assetsDir + 'js/flickity.js',
        config.assetsDir + 'js/tablefilter.js',
    ]).pipe(minify({
        ext:{min:'.min.js'},
        noSource:true
    })).pipe(gulp.dest(config.jsoutputDir));

    // Amcharts
    gulp.src([
        config.assetsDir + 'js/amcharts/core.js',
        config.assetsDir + 'js/amcharts/charts.js',
        config.assetsDir + 'js/amcharts/maps.js',
        config.assetsDir + 'js/amcharts/franceLow.js',
        config.assetsDir + 'js/amcharts/animated.js',
    ]).pipe(minify({
        ext:{min:'.min.js'},
        noSource:true
    })).pipe(gulp.dest(config.jsoutputDir + '/amcharts/'));
});



//==============================
// WATCHERS
//==============================

gulp.task('watch', function() {
    gulp.watch(config.assetsDir + config.sassPattern, [
        'base'
    ]);
});


//==============================
// Constructors
//==============================
gulp.task('all', [
    'js',
    'base'
    ]
);