<?php
//header('X-Frame-Options: ALLOW FROM 213.186.33.4'); 
header("Content-Security-Policy: frame-ancestors 'self' https://www.disruptunisia.com;");
include("include/db.php");
include("functions/functions.php");
include("config.php");

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $jour . " " . $mm;
    return $creation;
}
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include ('metaheaders.php'); ?>
        <title><?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">

        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>


        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-31711808-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MR2VSZB');</script>

    </head>

    <?php ?>
    <body>
        <div style="width: 320px">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Startup</th>
                    <th>Montant</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql_lf_list = mysqli_query($link, "select lf.date_ajout,startup.nom,lf.montant,startup.id from startup inner join lf on lf.id_startup=startup.id where startup.status=1 and lf.rachat=0 order by lf.date_ajout desc limit 6 offset 0");
                while ($datas = mysqli_fetch_array($sql_lf_list)) {
                    ?>
                    <tr>
                        <td><?php echo change_date_fr_chaine_related($datas['date_ajout']); ?></td>
                        <td><a target="_blank" href="<?php echo URL . '/fr/startup-france/' . generate_id($datas['id']) . "/" . urlWriting(strtolower($datas["nom"])) ?>"><?php echo stripslashes($datas["nom"]); ?></a></td>
                        <td><?php
                if ($datas['montant'] != 0) {
                    echo str_replace(",0", "", number_format($datas['montant'] / 1000, 1, ",", ""));
                        ?>M€ <?php } else echo "NC"; ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <div style="text-align: right;font-size: 10px;">Powered by <a href="https://www.myfrenchstartup.com" target="_blank"><img src="https://www.myfrenchstartup.com/static/images/logos/logo.svg?1" alt="Myfrenchstartup" style="width: 55px" /></a> </div>
        </div>
        </body>
</html>