<?php
include("include/db.php");
include("functions/functions.php");
include('config.php');

if (!isset($_SESSION['data_login'])) {
    echo "<script>window.location='" . url . "nos-offres'</script>";
    exit();
}
$monUrl = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$_SESSION['utm_source'] = $monUrl;
$id_region = degenerate_id($_GET['id']);
//$id_region = degenerate_id(5168);
$ma_region = mysqli_fetch_array(mysqli_query($link, "select * from region_new where id=" . $id_region));
$ligne_region = mysqli_fetch_array(mysqli_query($link, "select * from startup_region where id_region=" . $id_region));
$ligne_france = mysqli_fetch_array(mysqli_query($link, "select * from startup_region_france where 1"));

$users = mysqli_fetch_array(mysqli_query($link, "select premium,id from user where email='" . $_SESSION['data_login'] . "'"));
mysqli_query($link, "insert into user_vu_region(user,email,region,dt)values(" . $users['id'] . ",'".$_SESSION['data_login']."'," . $id_region . ",'" . date('Y-m-d H:i:s') . "')");

$year = date("Y");
$previousyear = $year - 1;

function change_date_fr_chaine_related($date) {
    $split = explode("-", $date);
    $annee = $split[0];
    $mois = $split[1];
    $jour = $split[2];
    if (($mois == "01"))
        $mm = "Jan. ";
    if (($mois == "02"))
        $mm = "Fév. ";
    if (($mois == "03"))
        $mm = "Mar. ";
    if (($mois == "04"))
        $mm = "Avr. ";
    if (($mois == "05"))
        $mm = "Mai";
    if (($mois == "06"))
        $mm = "Jui. ";
    if (($mois == "07"))
        $mm = "Juil. ";
    if (($mois == "08"))
        $mm = "Aou. ";
    if (($mois == "09"))
        $mm = "Sep. ";
    if (($mois == "10"))
        $mm = "Oct. ";
    if (($mois == "11"))
        $mm = "Nov. ";
    if ($mois == "12")
        $mm = "Déc. ";

    $creation = $mm . " " . $annee;
    return $creation;
}

$date1 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d'), date('y')));
$date2 = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d') - 30, date('y')));
?>
<html lang="fr-FR" class="no-js no-svg" prefix="og: https://ogp.me/ns#">
    <head>
        <?php include('metaheaders.php'); ?>
        <title>Études région <?php echo($ma_region['region_new']) ?> - <?= SITENAME; ?></title>
        <meta name="description" content="<?= METADESC; ?>">

        <!-- AM Charts components -->
        <script src="<?= JS_PATH; ?>amcharts/core.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/charts.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/maps.min.js"></script>
        <script src="<?= JS_PATH; ?>amcharts/franceLow.min.js"></script>
        <script>am4core.addLicense("CH303325752");</script>
        <script>
            const themeColorBlue = am4core.color('#00bff0'),
                    themeColorFill = am4core.color('#E1F2FA'),
                    themeColorGrey = am4core.color('#dadada'),
                    themeColorLightGrey = am4core.color('#F5F5F5'),
                    themeColorDarkgrey = am4core.color("#33333A"),
                    themeColorLine = am4core.color("#87D6E3"),
                    themeColorLineRed = am4core.color("#FF7C7C"),
                    themeColorLineGreen = am4core.color("#21D59B"),
                    themeColorColumns = am4core.color("#E1F2FA"),
                    labelColor = am4core.color('#798a9a');
        </script>
    </head>
    <body class="preload page">
        <div id="mainmenu" class="mainmenu">
            <div class="mainmenu__wrapper"></div>
        </div>
        <div class="page-wrapper">
            <?php
            if (!isset($_SESSION['data_login'])) {
                include('layout/header-simple.php');
            } else {
                include('layout/header-connected.php');
            }
            ?>
            <div class="page-content" id="page-content">
                <div class="container">
                    <section class="module">
                        <div class="module__title">Études région <?php echo($ma_region['region_new']) ?></div>
                        <div class="ctg ctg--1_3">
                            <aside class="bloc text-center">
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Startups dans la région</div>
                                    <div class="datasbloc__val"><?php
                                        echo number_format($ligne_region['nbr_startups'], 0, ".", " ");
                                        ?>
                                    </div>
                                    <div class="datasbloc__subtitle">
                                        <?php echo str_replace(",0", "", number_format($ligne_region['perc_nbr_startups'], 1, ",", "")); ?>
                                        %
                                        <?php if ($ligne_region['evo_nbr_startups'] < 0) { ?>
                                            <span class="ico-decrease"></span>
                                        <?php } ?>
                                        <?php if ($ligne_region['evo_nbr_startups'] > 0) { ?>
                                            <span class="ico-raise"></span>
                                        <?php } ?>
                                        <?php if ($ligne_region['evo_nbr_startups'] == 0) { ?>
                                            <span class="ico-stable"></span>
                                        <?php } ?>
                                        France
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Emplois créés</div>
                                    <div class="datasbloc__val"><?php
                                        echo number_format($ligne_region['emplois_generes'], 0, ".", " ");
                                        ?>
                                    </div>
                                    <div class="datasbloc__subtitle">
                                        <?php echo str_replace(",0", "", number_format($ligne_region['creations_emploi_12'], 1, ",", "")); ?>
                                        %
                                        <?php if ($ligne_region['evo_emplois_generes'] < 0) { ?>
                                            <span class="ico-decrease"></span>
                                        <?php } ?>
                                        <?php if ($ligne_region['evo_emplois_generes'] > 0) { ?>
                                            <span class="ico-raise"></span>
                                        <?php } ?>
                                        <?php if ($ligne_region['evo_emplois_generes'] == 0) { ?>
                                            <span class="ico-stable"></span>
                                        <?php } ?>

                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Investisseurs dans la région</div>
                                    <div class="datasbloc__val"><?php
                                        echo $ligne_region['nbr_investisseurs'];
                                        ?>
                                    </div>
                                </div>
                                <div class="datasbloc">
                                    <div class="datasbloc__key">Total fonds levés</div>
                                    <div class="datasbloc__val"><?php echo number_format(($ligne_region['total_invest'] / 1000), 1, ",", " ") ?>
                                        M€
                                    </div>
                                    <div class="datasbloc__subtitle">
                                        <?php echo str_replace(",0", "", number_format($ligne_region['perc_total_invest'], 1, ",", "")); ?>
                                        %
                                        <?php if ($ligne_region['evo_total_invest'] < 0) { ?>
                                            <span class="ico-decrease"></span>
                                        <?php } ?>
                                        <?php if ($ligne_region['evo_total_invest'] > 0) { ?>
                                            <span class="ico-raise"></span>
                                        <?php } ?>
                                        <?php if ($ligne_region['evo_total_invest'] == 0) { ?>
                                            <span class="ico-stable"></span>
                                        <?php } ?>
                                        France
                                    </div>
                                </div>
                            </aside>

                            <div class="bloc">
                                <div class="ctg ctg--2_2 ctg--fullheight">
                                    <div class="chart chart--radar">
                                        <div id="chart-radar"></div>
                                    </div>
                                    <script>
                                        am4core.ready(function () {


                                        /* Create chart instance */
                                        var chart = am4core.create("chart-radar", am4charts.RadarChart);
                                        /* Add data */
                                        chart.data = [{
                                        "date": "A",
                                                "value": <?php echo $ligne_region['densite_startups']; ?>,
                                                "value2": <?php echo number_format($ligne_france['densite_startups'], 1, ".", ""); ?>
                                        }, {
                                        "date": "B",
                                                "value": <?php echo $ligne_region['densite_investisseurs'] ?>,
                                                "value2": <?php echo number_format($ligne_france['densite_investisseurs'], 1, ".", ""); ?>
                                        }, {
                                        "date": "C",
                                                "value": <?php echo $ligne_region['anciennete_startup'] ?>,
                                                "value2": <?php echo number_format($ligne_france['anciennete_startup'], 1, ".", ""); ?>
                                        }, {
                                        "date": "D",
                                                "value": <?php echo $ligne_region['maturite_startup'] ?>,
                                                "value2": <?php echo number_format($ligne_france['maturite_startup'], 1, ".", ""); ?>
                                        }, {
                                        "date": "E",
                                                "value": <?php echo $ligne_region['montant_moyen_financement'] ?>,
                                                "value2": <?php echo number_format($ligne_france['montant_moyen_financement'], 1, ".", ""); ?>
                                        }, {
                                        "date": "F",
                                                "value": <?php echo $ligne_region['indice_defaillances']; ?>,
                                                "value2": <?php echo number_format($ligne_france['indice_defaillances'], 1, ".", ""); ?>
                                        }];
                                        chart.paddingTop = 0;
                                        chart.paddingBottom = 0;
                                        chart.paddingLeft = 0;
                                        chart.paddingRight = 0;
                                        /* Create axes */
                                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.dataFields.category = "date";
                                        categoryAxis.renderer.ticks.template.strokeWidth = 2;
                                        categoryAxis.renderer.labels.template.fontSize = 11;
                                        categoryAxis.renderer.labels.template.fill = labelColor;
                                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.axisFills.template.fill = chart.colors.getIndex(2);
                                        valueAxis.renderer.axisFills.template.fillOpacity = 0.05;
                                        valueAxis.renderer.labels.template.fontSize = 11;
                                        valueAxis.events.on("ready", function (ev) {
                                        ev.target.min = 0;
                                        ev.target.max = 10;
                                        })

                                                valueAxis.renderer.gridType = "polygons"


                                                /* Create and configure series */
                                                        function createSeries(field, name, themeColor) {
                                                        var series = chart.series.push(new am4charts.RadarSeries());
                                                        series.dataFields.valueY = field;
                                                        series.dataFields.categoryX = "date";
                                                        series.strokeWidth = 2;
                                                        series.fill = themeColor;
                                                        series.fillOpacity = 0.3;
                                                        series.stroke = themeColor;
                                                        series.name = name;
                                                        // Show bullets ?
                                                        //let circleBullet = series.bullets.push(new am4charts.CircleBullet());
                                                        //circleBullet.circle.stroke = am4core.color('#fff');
                                                        //circleBullet.circle.strokeWidth = 2;
                                                        }


                                                createSeries("value", "Région", themeColorBlue);
                                                createSeries("value2", "France", themeColorGrey);
                                                chart.legend = new am4charts.Legend();
                                                chart.legend.position = "top";
                                                chart.legend.useDefaultMarker = true;
                                                chart.legend.fontSize = "11";
                                                chart.legend.color = themeColorGrey;
                                                chart.legend.labels.template.fill = labelColor;
                                                chart.legend.labels.template.textDecoration = "none";
                                                chart.legend.valueLabels.template.textDecoration = "none";
                                                let as = chart.legend.labels.template.states.getKey("active");
                                                as.properties.textDecoration = "line-through";
                                                as.properties.fill = themeColorDarkgrey;
                                                let marker = chart.legend.markers.template.children.getIndex(0);
                                                marker.cornerRadius(12, 12, 12, 12);
                                                marker.width = 20;
                                                marker.height = 20;
                                                });
                                    </script>
                                    <div class="datagrid">
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">A :</span> Densité startups
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_region['densite_startups']); ?>
                                                <?php
                                                if ($ligne_region['evo_densite_startups'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_densite_startups'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_densite_startups'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy.
                                                France <?php echo str_replace(",0", "", number_format($ligne_france['densite_startups'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">B :</span> Densité investisseurs
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_region['densite_investisseurs']); ?>
                                                <?php
                                                if ($ligne_region['evo_densite_investisseurs'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_densite_investisseurs'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_densite_investisseurs'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy.
                                                France <?php echo str_replace(",0", "", number_format($ligne_france['densite_investisseurs'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">C :</span> Ancienneté des startups
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_region['anciennete_startup']); ?>
                                                <?php
                                                if ($ligne_region['evo_anciennete'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_anciennete'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_anciennete'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy.
                                                France <?php echo number_format($ligne_france['anciennete_startup'], 1, ",", ""); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">D :</span> Maturité des startups
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_region['maturite_startup']); ?>
                                                <?php
                                                if ($ligne_region['evo_maturite'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_maturite'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_maturite'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy.
                                                France <?php echo number_format($ligne_france['maturite_startup'], 1, ",", ""); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">E :</span> Montant moyen des financements
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(".", ",", $ligne_region['montant_moyen_financement']); ?>
                                                <?php
                                                if ($ligne_region['evo_montant_moyen_financement'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_montant_moyen_financement'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_montant_moyen_financement'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy.
                                                France <?php echo str_replace(',0', '', number_format($ligne_france['montant_moyen_financement'], 1, ",", "")); ?></div>
                                        </div>
                                        <div class="datagrid__bloc">
                                            <div class="datagrid__key">
                                                <span class="datagrid__key__label">F :</span> Taux<br>défaillance
                                            </div>
                                            <div class="datagrid__val"><?php echo str_replace(",0", "", number_format($ligne_region['indice_defaillances'])); ?>
                                                <?php
                                                if ($ligne_region['evo_defaillances'] > 0) {
                                                    ?>
                                                    <span class="ico-raise"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_defaillances'] < 0) {
                                                    ?>
                                                    <span class="ico-decrease"></span>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if ($ligne_region['evo_defaillances'] == 0) {
                                                    ?>
                                                    <span class="ico-stable"></span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="datagrid__subtitle">Moy.
                                                France <?php echo str_replace(",0", "", number_format($ligne_france['indice_defaillances'], 1, ",", "")); ?></div>
                                        </div>
                                    </div>

                                    <div class="bloc-actions">
                                        <div class="bloc-actions__ico">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Indicateurs économiques région <?php echo($ma_region['region_new']) ?></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Startups de la région par effectifs</div>
                                <div class="chart chart--columns">
                                    <div id="chart-startups-effectifs"></div>
                                </div>

                                <script>
                                    am4core.ready(function () {

                                    // Themes begin
                                    // Themes end

                                    // Create chart instance
                                    var chart = am4core.create("chart-startups-effectifs", am4charts.XYChart);
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
<?php
$effectif_rang_1 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>=1 and rang_effectif<=2 and region_new='" . addslashes($ma_region['region_new']) . "'"));
$effectif_rang_2 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>=3 and rang_effectif<=5 and region_new='" . addslashes($ma_region['region_new']) . "'"));
$effectif_rang_3 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>=6 and rang_effectif<=10 and region_new='" . addslashes($ma_region['region_new']) . "'"));
$effectif_rang_4 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>=11 and rang_effectif<=20 and region_new='" . addslashes($ma_region['region_new']) . "'"));
$effectif_rang_5 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>=21 and rang_effectif<=50 and region_new='" . addslashes($ma_region['region_new']) . "'"));
$effectif_rang_6 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>=51 and rang_effectif<=100 and region_new='" . addslashes($ma_region['region_new']) . "'"));
$effectif_rang_7 = mysqli_num_rows(mysqli_query($link, "select id from startup where status=1 and rang_effectif>100"));
?>
                                    // Add data
                                    chart.data = [{
                                    "year": "1-2",
                                            "visits": <?php echo $effectif_rang_1; ?>
                                    }, {
                                    "year": "3-5",
                                            "visits": <?php echo $effectif_rang_2; ?>
                                    }, {
                                    "year": "6-10",
                                            "visits": <?php echo $effectif_rang_3; ?>
                                    }, {
                                    "year": "11-20",
                                            "visits": <?php echo $effectif_rang_4; ?>
                                    }, {
                                    "year": "21-50",
                                            "visits": <?php echo $effectif_rang_5; ?>
                                    }, {
                                    "year": "51-100",
                                            "visits": <?php echo $effectif_rang_6; ?>
                                    }, {
                                    "year": ">100",
                                            "visits": <?php echo $effectif_rang_7; ?>
                                    }];
                                    // Create axes

                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "year";
                                    categoryAxis.renderer.grid.template.location = 0;
                                    categoryAxis.renderer.minGridDistance = 10;
                                    categoryAxis.renderer.fontSize = "10px";
                                    categoryAxis.renderer.labels.template.fill = labelColor;
                                    //categoryAxis.dataItems.template.text = "{realName}";
                                    categoryAxis.renderer.grid.disabled = true;
                                    categoryAxis.title.text = "Tranches d'effectifs";
                                    categoryAxis.title.rotation = 0;
                                    categoryAxis.title.align = "center";
                                    categoryAxis.title.valign = "bottom";
                                    categoryAxis.title.dy = - 5;
                                    categoryAxis.title.fontSize = 11;
                                    categoryAxis.title.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    // Title left
                                    valueAxis.title.text = "Nombre de startups";
                                    valueAxis.title.rotation = - 90;
                                    valueAxis.title.align = "left";
                                    valueAxis.title.valign = "top";
                                    valueAxis.title.dy = 0;
                                    valueAxis.title.fontSize = 11;
                                    valueAxis.title.fill = labelColor;
                                    // Create series
                                    var series = chart.series.push(new am4charts.ColumnSeries());
                                    series.dataFields.valueY = "visits";
                                    series.dataFields.categoryX = "year";
                                    series.fill = themeColorColumns;
                                    series.stroke = 0;
                                    var columnTemplate = series.columns.template;
                                    columnTemplate.strokeWidth = 0;
                                    columnTemplate.strokeOpacity = 0;
                                    valueAxis.renderer.grid.template.strokeWidth = 1;
                                    //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                    categoryAxis.renderer.grid.template.strokeWidth = 0;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Startups de la région par ancienneté</div>
                                <div class="chart chart--columns">
                                    <div id="chart-maturite"></div>
                                </div>
                                <script>
                                    am4core.ready(function () {

                                    // Themes begin
                                    // Themes end

                                    // Create chart instance
                                    var chart = am4core.create("chart-maturite", am4charts.XYChart);
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    // Add data
                                    chart.data = [

<?php
for ($i = date('Y'); $i >= 2014; $i--) {

    $verif_nb = mysqli_num_rows(mysqli_query($link, "select startup.id from startup  where  year(startup.date_complete)='" . $i . "' and startup.region_new='" . addslashes($ma_region['region_new']) . "'"));
    ?>
                                        {
                                        "year": "<?php echo 2021 - $i + 1; ?> ans",
                                                "visits": <?php echo $verif_nb; ?>
                                        },
    <?php
}
?>
                                    ];
                                    // Create axes

                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "year";
                                    categoryAxis.renderer.grid.template.location = 0;
                                    categoryAxis.renderer.minGridDistance = 10;
                                    categoryAxis.renderer.fontSize = "10px";
                                    categoryAxis.renderer.labels.template.fill = labelColor;
                                    //categoryAxis.dataItems.template.text = "{realName}";
                                    categoryAxis.renderer.grid.disabled = true;
                                    categoryAxis.title.text = "Tranches d'âges";
                                    categoryAxis.title.rotation = 0;
                                    categoryAxis.title.align = "center";
                                    categoryAxis.title.valign = "bottom";
                                    categoryAxis.title.dy = - 5;
                                    categoryAxis.title.fontSize = 11;
                                    categoryAxis.title.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    // Title left
                                    valueAxis.title.text = "Nombre de startups";
                                    valueAxis.title.rotation = - 90;
                                    valueAxis.title.align = "left";
                                    valueAxis.title.valign = "top";
                                    valueAxis.title.dy = 0;
                                    valueAxis.title.fontSize = 11;
                                    valueAxis.title.fill = labelColor;
                                    // Create series
                                    var series = chart.series.push(new am4charts.ColumnSeries());
                                    series.dataFields.valueY = "visits";
                                    series.dataFields.categoryX = "year";
                                    series.fill = themeColorColumns;
                                    series.stroke = 0;
                                    var columnTemplate = series.columns.template;
                                    columnTemplate.strokeWidth = 0;
                                    columnTemplate.strokeOpacity = 0;
                                    valueAxis.renderer.grid.template.strokeWidth = 1;
                                    //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                    categoryAxis.renderer.grid.template.strokeWidth = 0;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="ctg ctg--3_3">
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Emplois générés</div>
                                        <div class="datasbloc__val"><?php echo number_format($ligne_region['emplois_generes'], 0, "", " ") ?></div>
                                        <div>+<?php echo ceil($ligne_region['creations_emploi_12']); ?>% sur 12 mois</div>
                                        <div><?php echo $year . " vs " . $previousyear; ?></div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = ceil($ligne_region['creations_emploi_12']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one"
                                                 style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two"
                                                 style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min">% emplois créés en 12 mois</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Défaillances</div>
                                        <div class="datasbloc__val"><?php echo number_format($ligne_region['defaillances'], 0, "", " "); ?></div>
                                        <div>+<?php echo ceil($ligne_region['defaillances_12_perc']); ?>% sur 12 mois</div>
                                        <div><?php echo $year . " vs " . $previousyear; ?></div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($ligne_region['defaillances_perc']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one"
                                                 style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two"
                                                 style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min">% startups radiées par rapport à radiées France
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Emplois détruits</div>
                                        <div class="datasbloc__val"><?php echo number_format($ligne_region['emplois_detruits'], 0, "", " ") ?></div>
                                        <div>+<?php echo ceil($ligne_region['detruits_12']); ?>% sur 12 mois</div>
                                        <div><?php echo $year . " vs " . $previousyear; ?></div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($ligne_region['emplois_detruits_generes']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one"
                                                 style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two"
                                                 style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min">% emplois détruits / emplois générés</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="module__title">Financements startups <?php echo($ma_region['region_new']) ?></div>
                        <div class="ctg ctg--3_3">
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Total levées de fonds</div>
                                        <div class="datasbloc__val"><?php echo $ligne_region['nbr_levee']; ?></div>
                                        <div>+<?php echo ceil($ligne_region['taux_levee_12']); ?>% sur 12 mois</div>
                                        <div><?php echo $year . " vs " . $previousyear; ?></div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = ceil($ligne_region['taux_levee_12']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one"
                                                 style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two"
                                                 style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min">% levées de fonds France sur 12 mois</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Financement total</div>
                                        <div class="datasbloc__val"><?php echo number_format($ligne_region['total_invest'] / 100, 0, "", " "); ?> M€</div>
                                        <div>+<?php echo ceil($ligne_region['perc_total_invest']); ?>% sur 12 mois</div>
                                        <div><?php echo $year . " vs " . $previousyear; ?></div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($ligne_region['perc_total_invest']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one"
                                                 style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two"
                                                 style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min">% des financements France sur 12 mois
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="ctg ctg--2_2 align-center">
                                    <div class="datasbloc">
                                        <div class="datasbloc__key">Total recherches de fonds</div>
                                        <div class="datasbloc__val"><?php echo number_format($ligne_region['nbr_deal_flow'], 0, "", " ") ?> dossiers</div>
                                        <div><?php if($ligne_region['taux_deal_flow_12']>0) echo "+"; ?><?php echo ceil($ligne_region['taux_deal_flow_12']); ?>% sur 12 mois</div>
                                        <div><?php echo $year . " vs " . $previousyear; ?></div>
                                    </div>
                                    <?php
// Calcul pour le donut avec Pourcentage de commentaires positifs
// CSS Variables
// Variable à toucher
                                    $valToShow = round($ligne_region['taux_deal_flow']); // Moyenne sur 100 à récupérer depuis la BDD. Sera exprimée en "degrés" pour le donut.
// Variables css à ne pas toucher
                                    $deg = ($valToShow / 100 * 360);
                                    $deg1 = 90;
                                    $deg2 = $deg;
                                    $class = null;

// HACK CSS
//Ajout d'une classe "reverse" si la valeur est < à 50 pour retourner le donut (bug CSS)
                                    if ($valToShow < 50) {
                                        $deg1 = ($valToShow / 100 * 360 + 90);
                                        $deg2 = 0;
                                        $class = " reverse";
                                    }
                                    ?>
                                    <div class="donut">
                                        <div class="donut__chart donutDatas<?php if ($class) echo $class; ?>">
                                            <div class="slice one"
                                                 style="transform: rotate(<?php echo $deg1; ?>deg); -webkit-transform: rotate(<?php echo $deg1; ?>deg);"></div>
                                            <div class="slice two"
                                                 style="transform: rotate(<?php echo $deg2; ?>deg); -webkit-transform: rotate(<?php echo $deg2; ?>deg);"></div>
                                            <div class="chart-center">
                                                <span class="total">
                                                    <?php echo $valToShow; ?>%
                                                </span>
                                            </div>
                                        </div>
                                        <div class="donut__legend">
                                            <div class="donut__legend__min">% des recherches de fonds France</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="module__title">Indicateurs économiques <?php echo($ma_region['region_new']) ?></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Historique financements du secteur</div>
                                <div class="chart chart--columns">
                                    <div id="chart-financement"></div>
                                </div>
                                <script>
                                    am4core.ready(function () {

                                    // Themes begin
                                    // Themes end

                                    // Create chart instance
                                    var chart = am4core.create("chart-financement", am4charts.XYChart);
                                    chart.paddingBottom = 0;
                                    chart.paddingLeft = 0;
                                    chart.paddingRight = 0;
                                    // Add data
                                    chart.data = [
<?php
for ($i = 2013; $i <= date('Y'); $i++) {
    $nbr_tot = 0;
    $verif_nb = mysqli_num_rows(mysqli_query($link, "select lf.id from lf inner join startup on startup.id=lf.id_startup where lf.rachat=0 and year(lf.date_ajout)='" . $i . "' and startup.region_new='" . addslashes($ma_region['region_new']) . "'"));
    ?>
                                        {
                                        "year": "<?php echo $i; ?>",
                                                "visits": <?php echo $verif_nb ?>
                                        },
    <?php
}
?>
                                    ];
                                    // Create axes

                                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                                    categoryAxis.dataFields.category = "year";
                                    categoryAxis.renderer.grid.template.location = 0;
                                    categoryAxis.renderer.minGridDistance = 10;
                                    categoryAxis.renderer.fontSize = "10px";
                                    categoryAxis.renderer.labels.template.fill = labelColor;
                                    //categoryAxis.dataItems.template.text = "{realName}";
                                    categoryAxis.renderer.grid.disabled = true;
                                    categoryAxis.title.text = "Année";
                                    categoryAxis.title.rotation = 0;
                                    categoryAxis.title.align = "center";
                                    categoryAxis.title.valign = "bottom";
                                    categoryAxis.title.dy = - 5;
                                    categoryAxis.title.fontSize = 11;
                                    categoryAxis.title.fill = labelColor;
                                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                                    valueAxis.renderer.labels.template.fontSize = 11;
                                    valueAxis.renderer.labels.template.fill = labelColor;
                                    // Title left
                                    valueAxis.title.text = "Nombre de startups";
                                    valueAxis.title.rotation = - 90;
                                    valueAxis.title.align = "left";
                                    valueAxis.title.valign = "top";
                                    valueAxis.title.dy = 0;
                                    valueAxis.title.fontSize = 11;
                                    valueAxis.title.fill = labelColor;
                                    // Create series
                                    var series = chart.series.push(new am4charts.ColumnSeries());
                                    series.dataFields.valueY = "visits";
                                    series.dataFields.categoryX = "year";
                                    series.fill = themeColorColumns;
                                    series.stroke = 0;
                                    var columnTemplate = series.columns.template;
                                    columnTemplate.strokeWidth = 0;
                                    columnTemplate.strokeOpacity = 0;
                                    valueAxis.renderer.grid.template.strokeWidth = 1;
                                    //valueAxis2.renderer.grid.template.strokeWidth = 0;
                                    categoryAxis.renderer.grid.template.strokeWidth = 0;
                                    }); // end am4core.ready()
                                </script>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Dernières levées de fonds de la région <?php echo($ma_region['region_new']) ?> <a href="">Voir la liste &raquo;</a></div>
                                <div class="tablebloc">
                                    <table class="table table--geographie">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="moneyCell text-center">Montant</th>
                                                <th class="dateCell text-center">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $list_last_lf = explode(",", $ligne_region['last_lf']);
                                            $nbrs = count($list_last_lf);
                                            for ($i = 0; $i < $nbrs; $i++) {
                                                $data_lf_last = mysqli_fetch_array(mysqli_query($link, "select lf.montant,lf.date_ajout,lf.id_startup from lf  where id=" . $list_last_lf[$i]));
                                                $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation,date_complete,region_new from startup where id=" . $data_lf_last["id_startup"]));
                                                ?>
                                                <tr>
                                                    <td>
                                                        <a target="_blank" href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" title="" class="companyNameCell"><span><?php echo $ma_startup['nom']; ?></span></a>
                                                    </td>
                                                    <td class="text-center"><?php echo str_replace(",0", "", number_format($data_lf_last['montant'] / 1000, 1, ",", " ")); ?> M€</td>
                                                    <td class="text-center"><?php echo change_date_fr_chaine_related($data_lf_last['date_ajout']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            <?php
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Startups de la région en recherche de fonds <a href="">Voir la liste &raquo;</a></div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="moneyCell text-center">Montant recherché</th>
                                                <th class="dateCell text-center">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $list_prob = explode(",", $ligne_region['last_deal']);
                                            $nbrs = count($list_prob);
                                            for ($i = 0; $i < $nbrs; $i++) {

                                                $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation,date_complete from startup where id=" . $list_prob[$i]));
                                                $data_secteur4 = mysqli_fetch_array(mysqli_query($link, "SELECT * FROM  startup_deal_flow  where id_startup=" . $list_prob[$i] . " order by startup_deal_flow.dt desc limit 1"));
                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="logoNameCell">
                                                            <div class="logo">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>"
                                                                   target="_blank"><img
                                                                        src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>"
                                                                        alt="<?php echo($ma_startup['nom']); ?>"/></a>
                                                            </div>
                                                            <div class="name"><a
                                                                    href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>"
                                                                    target="_blank"><?php echo($ma_startup['nom']); ?></a></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center moneyCell"><?php echo str_replace(",0", "", number_format($data_secteur4['montant'] / 1000, 1, ",", " ")); ?>
                                                        M€
                                                    </td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($data_secteur4['dt']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Top 10 probabilité levée de fonds à 6 mois <a href="">Voir la liste &raquo;</a></div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="dateCell text-center">Création</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $list_prob = explode(",", $ligne_region['last_prob']);
                                            $nbrs = count($list_prob);
                                            for ($i = 0; $i < $nbrs; $i++) {

                                                $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation,date_complete from startup where id=" . $list_prob[$i]));
                                                $tot_lev_prob = mysqli_fetch_array(mysqli_query($link, "select sum(montant) as somme from lf where rachat=0 and id_startup=" . $list_prob['id']));
                                                ?>


                                                <tr>
                                                    <td>
                                                        <div class="logoNameCell">
                                                            <div class="logo">
                                                                <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>"
                                                                   target="_blank"><img
                                                                        src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>"
                                                                        alt="<?php echo($ma_startup['nom']); ?>"/></a>
                                                            </div>
                                                            <div class="name"><a
                                                                    href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>"
                                                                    target="_blank"><?php echo($ma_startup['nom']); ?></a></div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($ma_startup['date_complete']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Actualités startups région <?php echo($ma_region['region_new']) ?></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Dernières startups créées dans la région</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="moneyCell">Secteur</th>
                                                <th class="dateCell text-center">Création</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $tabs_last = explode(",", $ligne_region['last_startup']);
                                            for ($t = 0; $t < 4; $t++) {
                                                $ma_start = mysqli_fetch_array(mysqli_query($link, "select startup.date_complete,startup.id,startup.nom,activite.secteur from startup inner join activite on startup.id=activite.id_startup where startup.id=" . $tabs_last[$t]));
                                                $secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur where id=" . $ma_start['secteur']));
                                                ?>
                                                <tr>
                                                    <td>
                                                        <a href="" title="" class="companyNameCell"><span><?php echo $ma_start['nom']; ?></span></a>
                                                    </td>
                                                    <td><?php echo $secteur['nom_secteur']; ?></td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($ma_start['date_complete']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Dernières acquisitions ou IPO dans la région</div>
                                <div class="tablebloc">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Startup</th>
                                                <th class="moneyCell">Secteur</th>
                                                <th class="dateCell text-center">Création</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $tabs_last = explode(",", $ligne_region['last_acqui']);
                                            for ($t = 0; $t < 4; $t++) {
                                                $ma_start = mysqli_fetch_array(mysqli_query($link, "select startup.date_complete,startup.id,startup.nom,activite.secteur from startup inner join activite on startup.id=activite.id_startup where startup.id=" . $tabs_last[$t]));
                                                $secteur = mysqli_fetch_array(mysqli_query($link, "select * from secteur where id=" . $ma_start['secteur']));
                                                ?>
                                                <tr>
                                                    <td>
                                                        <a href="" title="" class="companyNameCell"><span><?php echo $ma_start['nom']; ?></span></a>
                                                    </td>
                                                    <td><?php echo $secteur['nom_secteur']; ?></td>
                                                    <td class="text-center dateCell"><?php echo change_date_fr_chaine_related($ma_start['date_complete']); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Marchés et secteurs <?php echo($ma_region['region_new']) ?></div>
                        <div class="bloc">
                            <div class="tablebloc">
                                <table class="table" id="table-marches-secteurs">
                                    <thead>
                                        <tr>
                                            <th>Secteur</th>
                                            <th class="text-center nowrap">Startups</th>
                                            <th class="text-center">Créations sur 12 mois</th>
                                            <th class="text-center">Ancienneté moy.</th>
                                            <th class="text-center">Effectifs moy.</th>
                                            <th class="text-center">Score maturité</th>
                                            <th class="text-center">Financements cumulés</th>
                                            <th class="text-center">Levées de fonds sur 12 mois</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql_secteur = mysqli_query($link, "SELECT count(*) as nb,secteur.id,secteur.nom_secteur,secteur.nom_secteur_en FROM `activite` inner join startup on startup.id=activite.id_startup inner join secteur on secteur.id=activite.secteur where startup.status=1 and startup.region_new='".addslashes($ma_region['region_new'])."' group by secteur order by nb desc");
                                        while ($data_secteur = mysqli_fetch_array($sql_secteur)) {
                                            $moys_stat = mysqli_fetch_array(mysqli_query($link, "select * from secteur_region where id_secteur=" . $data_secteur['id']." and region='".$ma_region['region_new']."'"));
                                            ?>
                                            <tr>
                                                <td><?php echo ($data_secteur['nom_secteur']); ?></td>
                                                <td class="text-center moneyCell"><?php echo $moys_stat['nbr_startups'] ?></td>
                                                <td class="text-center moneyCell"><?php echo str_replace(",0", "", number_format($moys_stat['creations_12'], 1, ",", "")) ?>%</td>
                                                <td class="text-center moneyCell"><?php echo str_replace(",0", "", number_format($moys_stat['moy_anciennete'], 1, ",", "")) ?> ans</td>
                                                <td class="text-center moneyCell"><?php echo $moys_stat['effectifs'] ?></td>
                                                <td class="text-center moneyCell"><?php echo str_replace(",0", "", number_format($moys_stat['score_maturite'], 1, ",", "")) ?></td>
                                                <td class="text-center moneyCell"><?php echo str_replace(",0", "", number_format($moys_stat['financements_cumules'] / 1000, 1, ",", " ")); ?> M€</td>
                                                <td class="text-center moneyCell"><?php echo $moys_stat['levee_12'] ?>%</td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <script>
                                    $(document).ready(function () {
                                    $('#table-marches-secteurs').DataTable();
                                    });
                                </script>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="module__title">Géographie <?php echo($ma_region['region_new']) ?></div>
                        <div class="ctg ctg--2_2">
                            <div class="bloc">
                                <div class="bloc__title">Startups du secteur par département</div>
                                <div class="tablebloc">
                                    <table class="table table--geographie">
                                        <thead>
                                            <tr>
                                                <th class="text-center" width="60px">Évol.</th>
                                                <th>Département</th>
                                                <th class="text-center">Startups</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $tab_list_deps = explode(";", $ligne_region['list_dep']);
                                            $tab_list_nbr_deps = explode(";", $ligne_region['nbr_dep']);
                                            $tab_list_evo_nbr_dep = explode(";", $ligne_region['evo_nbr_dep']);
                                            $b = count($tab_list_deps);
                                            for ($i = 0; $i < 10; $i++) {
                                                if ($tab_list_deps[$i] != "") {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php
                                                            if ($tab_list_evo_nbr_dep[$i] == -1) {
                                                                ?>
                                                                <span class="ico-decrease"></span>
                                                            <?php } ?>
                                                            <?php
                                                            if ($tab_list_evo_nbr_dep[$i] == 1) {
                                                                ?>
                                                                <span class="ico-raise"></span>
                                                            <?php } ?>
                                                            <?php
                                                            if ($tab_list_evo_nbr_dep[$i] == 0) {
                                                                ?>
                                                                <span class="ico-stable"></span>
                                                            <?php } ?></td>
                                                        <td><?php
                                                            if ($tab_list_deps[$i] != '')
                                                                echo ($tab_list_deps[$i]);
                                                            else
                                                                echo "NC";
                                                            ?></td>
                                                        <td class="text-center"><?php echo $tab_list_nbr_deps[$i]; ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="bloc">
                                <div class="bloc__title">Effectifs créés par département</div>
                                <div class="tablebloc">
                                    <table class="table table--geographie">
                                        <thead>
                                            <tr>
                                                <th class="text-center" width="60px">Évol.</th>
                                                <th>Département</th>
                                                <th class="text-center">Effectif</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $tab_list_deps_eff = explode(";", $ligne_region['effectif_list_dep']);
                                            $tab_list_nbr_deps_eff = explode(";", $ligne_region['effectif_nbr_dep']);
                                            $tab_list_evo_nbr_dep_eff = explode(";", $ligne_region['effectif_evo_nbr_dep']);
                                            $b = count($tab_list_deps_eff);
                                            for ($i = 0; $i < 10; $i++) {
                                                if ($tab_list_deps_eff[$i] != "") {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <?php
                                                            if ($tab_list_evo_nbr_dep_eff[$i] == -1) {
                                                                ?>
                                                                <span class="ico-decrease"></span>
                                                            <?php } ?>
                                                            <?php
                                                            if ($tab_list_evo_nbr_dep_eff[$i] == 1) {
                                                                ?>
                                                                <span class="ico-raise"></span>
                                                            <?php } ?>
                                                            <?php
                                                            if ($tab_list_evo_nbr_dep_eff[$i] == 0) {
                                                                ?>
                                                                <span class="ico-stable"></span>
                                                            <?php } ?></td>
                                                        <td><?php
                                                            if ($tab_list_deps_eff[$i] != '')
                                                                echo ($tab_list_deps_eff[$i]);
                                                            else
                                                                echo "NC";
                                                            ?></td>
                                                        <td class="text-center"><?php echo $tab_list_nbr_deps_eff[$i]; ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="module">
                        <div class="bloc">
                            <div class="bloc__title">Dernières levées de fonds de la
                                région <?php echo($ma_region['region_new']) ?></div>
                            <div class="tablebloc">
                                <table id="mine2" class="table table-search">
                                    <thead>
                                        <tr>
                                            <th class="table-search__startup">Startups</th>
                                            <th class="table-search__totaldernierelevee text-center">Dernière levée de fonds (M€)</th>
                                            <th class="table-search__datedernierelevee text-center">Date dernière levée</th>
                                            <th class="table-search__region text-center">Région</th>
                                            <th class="table-search__maturite text-center">Maturité</th>
                                            <th class="table-search__date text-center">Création</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $list_last_lf = explode(",", $ligne_region['last_lf']);
                                        $nbrs = count($list_last_lf);
                                        for ($i = 0; $i < $nbrs; $i++) {
                                            $data_lf_last = mysqli_fetch_array(mysqli_query($link, "select lf.montant,lf.date_ajout,lf.id_startup from lf  where id=" . $list_last_lf[$i]));
                                            $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation,date_complete,region_new from startup where id=" . $data_lf_last["id_startup"]));

                                            $score = mysqli_fetch_array(mysqli_query($link, "Select  * From   startup_score where  id_startup=" . $ma_startup['id']));
                                            $secteurs = mysqli_fetch_array(mysqli_query($link, "select secteur.nom_secteur from secteur inner join activite on activite.secteur=secteur.id where activite.id_startup=" . $ma_startup['id']));
                                            ?>
                                            <tr>
                                                <th class="table-search__startup">
                                                    <div class="startupInfos">
                                                        <div class="startupInfos__image">
                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt="<?php echo "Startup " . $ma_startup["nom"]; ?>"></a>
                                                        </div>
                                                        <div class="startupInfos__desc">
                                                            <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo stripslashes($ma_startup['nom']); ?></a></div>
                                                            <div class="companyLabel"><?php echo stripslashes($ma_startup['short_fr']); ?></div>
                                                        </div>
                                                    </div>
                                                </th>

                                                <td class="table-search__totaldernierelevee text-center"><?php echo str_replace(",0", "", number_format($data_lf_last['montant'] / 1000, 1, ",", " ")); ?>
                                                    M€
                                                </td>
                                                <td class="table-search__datedernierelevee text-center"><?php echo change_date_fr_chaine_related($data_lf_last['date_ajout']); ?></td>
                                                <td class="table-search__region text-center"><?php echo ($secteurs['nom_secteur']); ?></td>
                                                <td class="table-search__maturite text-center"> <?php echo $score['maturite']; ?> 
                                                    <?php if ($score['maturite'] == $ligne_region['maturite_startup']) { ?>
                                                        <span class="ico-stable"></span> 
                                                    <?php } ?>
                                                    <?php if ($score['maturite'] > $ligne_region['maturite_startup']) { ?>
                                                        <span class="ico-raise"></span> 
                                                    <?php } ?>
                                                    <?php if ($score['maturite'] < $ligne_region['maturite_startup']) { ?>
                                                        <span class="ico-decrease"></span> 
                                                    <?php } ?>
                                                </td>
                                                <td class="table-search__date text-center">
                                                    <?php echo change_date_fr_chaine_related($ma_startup['date_complete']); ?> 
                                                </td>
                                            </tr>

                                            <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    <section class="module">
                        <div class="bloc">
                            <div class="bloc__title">Dernières augmentations de capital de la région <?php echo($ma_region['region_new']) ?></div>
                            <div class="tablebloc">
                                <table id="mine" class="table table-search">
                                    <thead>
                                        <tr>
                                            <th class="table-search__startup">Startups</th>
                                            <th class="table-search__totalfondsleves text-center">Nouveau capital social (k€)</th>
                                            <th class="table-search__date text-center">Date modification de capital</th>
                                            <th class="table-search__region text-center">Région</th>
                                            <th class="table-search__maturite text-center">Maturité</th>
                                            <th class="table-search__date text-center">Création</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $list_capital = explode(",", $ligne_region['last_capital']);
                                        $nbrs = count($list_capital);
                                        for ($i = 0; $i < $nbrs; $i++) {

                                            $ma_startup = mysqli_fetch_array(mysqli_query($link, "select id,nom,logo,short_fr,creation,date_complete,region_new from startup where id=" . $list_capital[$i]));
                                            $dernier_capital = mysqli_fetch_array(mysqli_query($link, "select * from startup_capital where id_startup=" . $list_capital[$i] . " order by dt desc limit 1"));
                                            $score = mysqli_fetch_array(mysqli_query($link, "Select  * From   startup_score where  id_startup=" . $ma_startup['id']));
                                            $secteurs = mysqli_fetch_array(mysqli_query($link, "select secteur.nom_secteur from secteur inner join activite on activite.secteur=secteur.id where activite.id_startup=" . $ma_startup['id']));
                                            ?>
                                            <tr>
                                                <th class="table-search__startup">
                                                    <div class="startupInfos">
                                                        <div class="startupInfos__image">
                                                            <a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank">  <img src="https://www.myfrenchstartup.com/<?php echo str_replace("../", "", $ma_startup['logo']) ?>" alt="<?php echo "Startup " . $ma_startup["nom"]; ?>"></a>
                                                        </div>
                                                        <div class="startupInfos__desc">
                                                            <div class="companyName"><a href="<?php echo URL . '/fr/startup-france/' . generate_id($ma_startup['id']) . "/" . urlWriting(strtolower($ma_startup["nom"])) ?>" target="_blank"><?php echo stripslashes($ma_startup['nom']); ?></a></div>
                                                            <div class="companyLabel"><?php echo stripslashes($ma_startup['short_fr']); ?></div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <td class="table-search__totalfondsleves text-center"><?php echo str_replace(",0", "", number_format($dernier_capital['capital'] / 1000, 1, ",", " ")); ?>
                                                    k€
                                                </td>
                                                <td class="table-search__date text-center"><?php echo change_date_fr_chaine_related($dernier_capital['dt']); ?></td>
                                                <td class="table-search__region text-center"><?php echo ($secteurs['nom_secteur']); ?></td>
                                                <td class="table-search__maturite text-center"><?php echo $score['maturite']; ?> 
                                                    <?php if ($score['maturite'] == $ligne_region['maturite_startup']) { ?>
                                                        <span class="ico-stable"></span> 
                                                    <?php } ?>
                                                    <?php if ($score['maturite'] > $ligne_region['maturite_startup']) { ?>
                                                        <span class="ico-raise"></span> 
                                                    <?php } ?>
                                                    <?php if ($score['maturite'] < $ligne_region['maturite_startup']) { ?>
                                                        <span class="ico-decrease"></span> 
                                                    <?php } ?>
                                                </td>
                                                <td class="table-search__date text-center">
                                                    <?php echo change_date_fr_chaine_related($ma_startup['date_complete']); ?> 
                                                </td>
                                            </tr>

                                            <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>


                </div>
            </div>
        </div>

        <?php include('layout/footer.php'); ?>

        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>jquery.dataTables.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>jquery.1.9.1.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>jquery.dataTables.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>datatables.bootstrap.min.js?<?= time(); ?>"></script>

        <script>
                                    var table = jQuery('#mine').DataTable({
                                    "searching": false,
                                            "nextPrev": false,
                                            "bLengthChange": false,
                                            "bInfo": false,
                                            "bPaginate": true,
                                            "autoWidth": false,
                                            //"fixedColumns": false,
                                            initComplete: (settings, json) => {
                                    $('#paginateTable').empty();
                                    $('#mine_paginate').appendTo('#paginateTable');
                                    },
                                            "language": {
                                            "paginate": {
                                            "previous": "Précédent",
                                                    "next": "Suivant"
                                            }
                                            }
                                    });
                                    var table2 = jQuery('#mine2').DataTable({
                                    "searching": false,
                                            "nextPrev": false,
                                            "bLengthChange": false,
                                            "bInfo": false,
                                            "bPaginate": true,
                                            "autoWidth": false,
                                            //"fixedColumns": false,
                                            initComplete: (settings, json) => {
                                    $('#paginateTable2').empty();
                                    $('#mine2_paginate').appendTo('#paginateTable2');
                                    },
                                            "language": {
                                            "paginate": {
                                            "previous": "Précédent",
                                                    "next": "Suivant"
                                            }
                                            }
                                    });
        </script>
        <script>
            var table = jQuery('#table-marches-secteurs').DataTable({
            "searching": false,
                    "nextPrev": false,
                    "bLengthChange": false,
                    "bInfo": false,
                    "bPaginate": false,
                    "autoWidth": false,
            });
        </script>

        <script async src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        <script async src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>

        <noscript>
        <script src="<?= JS_PATH; ?>app.min.js?<?= time(); ?>"></script>
        <script src="<?= JS_PATH; ?>flickity.min.js?<?= time(); ?>"></script>
        </noscript>

        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-36251023-1', 'auto');
            ga('send', 'pageview');
        </script>
        <script>

            function chercher() {
            var $ = jQuery;
            var valeur = document.getElementById("search-box").value;
            $.ajax({
            type: "POST",
                    url: "<?php echo URL; ?>/readCountry.php",
                    data: 'keyword=' + valeur,
                    beforeSend: function () {
                    $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#search-box").css("background", "#FFF");
                    }
            });
            }

            function selectCountry(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
            function selectInvest(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
            function selectEntrepreneur(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
            function selectTags(val) {
            const words = val.split('/');
            $("#suggesstion-box").hide();
            window.location = '<?php echo URL ?>/' + val;
            }
        </script>
    </body>
</html>