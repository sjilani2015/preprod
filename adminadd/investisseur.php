<?php
session_start();
include('../include/db.php');

$nb = mysqli_num_rows(mysqli_query($link, "select * from de_list where new_name=''"));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8895-1" />
        <title>Investisseur</title>
        <link rel="stylesheet" href="https:////maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
        <script src="//code.jquery.com/jquery-1.12.3.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    </head>
    <body>
        <div style="margin-top: 20px">
            <div>Nous avons <?php echo $nb ?> investisseurs non qualifiés</div>
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" >
                <thead>
                    <tr>
                        <th>Modifier</th>
                         <th>Nbr Invest.</th>
                        <th>Investisseur</th>
                        <th>New</th>
                        <th>New</th>
                        <th>Group</th>
                        <th>Group</th>
                        <th>Type</th>
                        <th>Type</th>
                       

                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Modifier</th>
                        <th>Nbr Invest.</th>
                        <th>Investisseur</th>
                        <th>New</th>
                        <th>New</th>
                        <th>Group</th>
                        <th>Group</th>
                        <th>Type</th>
                        <th>Type</th>
                        

                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $sql = mysqli_query($link, "select * from de_list where nbr_invest>=10 and new_name=''");
                    while ($data = mysqli_fetch_array($sql)) {
                        ?>
                        <tr>
                            <td><button type="button" class="btn btn-success btn-sm" id="<?php echo $data['id']; ?>" onclick="update_invest(this)">Modifier</button></td>
                            <td><?php echo $data['nbr_invest'] ?></td>
                            <td><span id="<?php echo $data['id']; ?>_invest"><?php echo strtoupper($data['investisseur']); ?></span></td>
                            <td><input type="text" id="<?php echo $data['id']; ?>_new_name" value="<?php echo strtoupper($data['new_name']); ?>" /></td>
                            <td><?php echo strtoupper($data['new_name']); ?></td>
                            <td><input type="text" id="<?php echo $data['id']; ?>_name_group" value="<?php echo strtoupper($data['group_name']); ?>" /></td>
                            <td><?php echo strtoupper($data['group_name']); ?></td>
                            <td><select id="type_<?php echo $data['id']; ?>" class="form-control">
                                    <option value="<?php echo utf8_encode($data['type_name']) ?>"><?php echo utf8_encode($data['type_name']) ?></option>
                                    <option value="VC">VC</option>
                                    <option value="CVC">CVC</option>
                                    <option value="Businness Angels">Businness Angels</option>
                                    <option value="Institutionnel">Institutionnel</option>
                                    <option value="Incubateur">Incubateur</option>
                                    <option value="Accélérateur">Accélérateur</option>
                                    <option value="Crowdfunding">Crowdfunding</option>
                                    <option value="Family Office">Family Office</option>
                                    <option value="startup studio">startup studio</option>
                                </select></td>
                            <td><?php echo utf8_encode($data['type_name']) ?></td>
                            

                        </tr>
    <?php
}
?>
                </tbody>
            </table>
        </div>
        <script>
            $(document).ready(function () {
                $('#example').DataTable();
            });

            function update_invest(el) {
                var id = el.id;
                var new_name = document.getElementById(id + "_new_name").value;
                var name_group = document.getElementById(id + "_name_group").value;
                var type_name = document.getElementById( "type_"+id).value;
               

                $.ajax({
                    type: "POST",
                    url: "update_investisseur.php?id=" + id + "&new_name=" + new_name + "&name_group=" + name_group+ "&type_name=" + type_name ,
                    success: function (data) {
                        alert("Done");
                    }
                });

            }










        </script>

    </body>
</html>